# 6721 AI NLP
The results and dumps of models can be found in `results` folder

```
/results/
 |
 +-- basicSetup2Langs       <-- 30 sentences in EN and FR
 +-- basicSetup4Langs       <-- 30 sentences in EN, FR, NL, ES, PT 
 +-- close languages        <-- tests on similar languages
 +-- domain specific        <-- tests on domain specific corpus
 +-- largerTrainingCorpus   <-- tests using much larger corpus
 +-- length, acronims       <-- varying test string lenght, accronims, shared words between languages
 +-- smoothing              <-- smoothing adjustments
```

`TestFiles` contain sentences for test
`TrainingCorpus` contain corpus for training the models

The `driver` file can be configured to create models of various n-grams, languages and smoothing. The alphabet can be increased as needed.
 