John aime Brook.

1GRAM MODEL:

1GRAM: j
ENGLISH: P(j) = -2.944833324319119 ==> log prob of sentence so far: -2.944833324319119
FRENCH: P(j) = -2.2095776682007906 ==> log prob of sentence so far: -2.2095776682007906

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -4.082617084904614
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -3.483920998518445

1GRAM: h
ENGLISH: P(h) = -1.180274162400034 ==> log prob of sentence so far: -5.262891247304648
FRENCH: P(h) = -2.1056297044472685 ==> log prob of sentence so far: -5.589550702965713

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -6.424488184707277
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -6.712188179580218

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -7.511276865749731
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -7.788541274685727

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -8.673802346023642
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -8.923747717354571

1GRAM: m
ENGLISH: P(m) = -1.6111104829831362 ==> log prob of sentence so far: -10.284912829006778
FRENCH: P(m) = -1.5129486053385504 ==> log prob of sentence so far: -10.436696322693122

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -11.194984047066685
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -11.203662137215577

1GRAM: b
ENGLISH: P(b) = -1.7519519887518187 ==> log prob of sentence so far: -12.946936035818503
FRENCH: P(b) = -2.0518881761829366 ==> log prob of sentence so far: -13.255550313398514

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -14.2082089371306
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -14.43957895246897

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -15.345992697716094
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -15.713922282786625

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -16.483776458301588
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -16.98826561310428

1GRAM: k
ENGLISH: P(k) = -2.072697153608919 ==> log prob of sentence so far: -18.556473611910505
FRENCH: P(k) = -3.5365941540489745 ==> log prob of sentence so far: -20.524859767153252

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: jo
ENGLISH: P(o|j) = -0.5261813808304232 ==> log prob of sentence so far: -0.5261813808304232
FRENCH: P(o|j) = -0.8910129977564625 ==> log prob of sentence so far: -0.8910129977564625

2GRAM: oh
ENGLISH: P(h|o) = -1.9262959225061613 ==> log prob of sentence so far: -2.4524773033365843
FRENCH: P(h|o) = -3.298912055764982 ==> log prob of sentence so far: -4.189925053521445

2GRAM: hn
ENGLISH: P(n|h) = -2.586587304671755 ==> log prob of sentence so far: -5.039064608008339
FRENCH: P(n|h) = -2.923084527448938 ==> log prob of sentence so far: -7.113009580970383

2GRAM: na
ENGLISH: P(a|n) = -1.24610849854619 ==> log prob of sentence so far: -6.285173106554529
FRENCH: P(a|n) = -1.1335472623560008 ==> log prob of sentence so far: -8.246556843326383

2GRAM: ai
ENGLISH: P(i|a) = -1.3482554679089638 ==> log prob of sentence so far: -7.633428574463493
FRENCH: P(i|a) = -0.6966031694951295 ==> log prob of sentence so far: -8.943160012821512

2GRAM: im
ENGLISH: P(m|i) = -1.3479869275073166 ==> log prob of sentence so far: -8.98141550197081
FRENCH: P(m|i) = -1.5427023844256251 ==> log prob of sentence so far: -10.485862397247137

2GRAM: me
ENGLISH: P(e|m) = -0.5945354301857667 ==> log prob of sentence so far: -9.575950932156577
FRENCH: P(e|m) = -0.43058576450809083 ==> log prob of sentence so far: -10.916448161755229

2GRAM: eb
ENGLISH: P(b|e) = -1.6930590296644747 ==> log prob of sentence so far: -11.269009961821052
FRENCH: P(b|e) = -2.134431311091517 ==> log prob of sentence so far: -13.050879472846745

2GRAM: br
ENGLISH: P(r|b) = -1.240850356458931 ==> log prob of sentence so far: -12.509860318279983
FRENCH: P(r|b) = -0.7697085984080487 ==> log prob of sentence so far: -13.820588071254793

2GRAM: ro
ENGLISH: P(o|r) = -0.9679454985579867 ==> log prob of sentence so far: -13.47780581683797
FRENCH: P(o|r) = -1.1139096014103482 ==> log prob of sentence so far: -14.934497672665142

2GRAM: oo
ENGLISH: P(o|o) = -1.3567983196477529 ==> log prob of sentence so far: -14.834604136485723
FRENCH: P(o|o) = -2.756524069532728 ==> log prob of sentence so far: -17.69102174219787

2GRAM: ok
ENGLISH: P(k|o) = -1.8483101659233663 ==> log prob of sentence so far: -16.68291430240909
FRENCH: P(k|o) = -3.404715781933021 ==> log prob of sentence so far: -21.09573752413089

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: joh
ENGLISH: P(h|jo) = -1.1990404571266498 ==> log prob of sentence so far: -1.1990404571266498
FRENCH: P(h|jo) = -2.3533390953113047 ==> log prob of sentence so far: -2.3533390953113047

3GRAM: ohn
ENGLISH: P(n|oh) = -1.5072736719497615 ==> log prob of sentence so far: -2.7063141290764112
FRENCH: P(n|oh) = -1.0934216851622351 ==> log prob of sentence so far: -3.44676078047354

3GRAM: hna
ENGLISH: P(a|hn) = -0.9975111995963053 ==> log prob of sentence so far: -3.7038253286727167
FRENCH: P(a|hn) = -1.1026623418971477 ==> log prob of sentence so far: -4.549423122370688

3GRAM: nai
ENGLISH: P(i|na) = -1.8708019698812508 ==> log prob of sentence so far: -5.574627298553968
FRENCH: P(i|na) = -0.743119041517866 ==> log prob of sentence so far: -5.2925421638885535

3GRAM: aim
ENGLISH: P(m|ai) = -1.947719046120967 ==> log prob of sentence so far: -7.522346344674935
FRENCH: P(m|ai) = -2.0012284513314658 ==> log prob of sentence so far: -7.293770615220019

3GRAM: ime
ENGLISH: P(e|im) = -0.5939876538828333 ==> log prob of sentence so far: -8.116333998557767
FRENCH: P(e|im) = -0.4859739967020765 ==> log prob of sentence so far: -7.779744611922096

3GRAM: meb
ENGLISH: P(b|me) = -1.7753850341404915 ==> log prob of sentence so far: -9.891719032698258
FRENCH: P(b|me) = -2.734441147934156 ==> log prob of sentence so far: -10.514185759856252

3GRAM: ebr
ENGLISH: P(r|eb) = -1.118556038914146 ==> log prob of sentence so far: -11.010275071612405
FRENCH: P(r|eb) = -0.6308181202910425 ==> log prob of sentence so far: -11.145003880147295

3GRAM: bro
ENGLISH: P(o|br) = -0.547286288277202 ==> log prob of sentence so far: -11.557561359889608
FRENCH: P(o|br) = -1.5134225042858958 ==> log prob of sentence so far: -12.65842638443319

3GRAM: roo
ENGLISH: P(o|ro) = -1.6010396911813078 ==> log prob of sentence so far: -13.158601051070915
FRENCH: P(o|ro) = -2.8002170270102105 ==> log prob of sentence so far: -15.4586434114434

3GRAM: ook
ENGLISH: P(k|oo) = -0.6708291323860486 ==> log prob of sentence so far: -13.829430183456964
FRENCH: P(k|oo) = -0.7561569566774757 ==> log prob of sentence so far: -16.214800368120876

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: john
ENGLISH: P(n|joh) = -0.24144430567973715 ==> log prob of sentence so far: -0.24144430567973715
FRENCH: P(n|joh) = -0.7781512503836436 ==> log prob of sentence so far: -0.7781512503836436

4GRAM: ohna
ENGLISH: P(a|ohn) = -1.3921104650113136 ==> log prob of sentence so far: -1.6335547706910507
FRENCH: P(a|ohn) = -1.4771212547196624 ==> log prob of sentence so far: -2.255272505103306

4GRAM: hnai
ENGLISH: P(i|hna) = -1.7781512503836436 ==> log prob of sentence so far: -3.4117060210746946
FRENCH: P(i|hna) = -1.4471580313422192 ==> log prob of sentence so far: -3.702430536445525

4GRAM: naim
ENGLISH: P(m|nai) = -1.6092385759550858 ==> log prob of sentence so far: -5.02094459702978
FRENCH: P(m|nai) = -1.6862046569071374 ==> log prob of sentence so far: -5.388635193352663

4GRAM: aime
ENGLISH: P(e|aim) = -0.3449354813630629 ==> log prob of sentence so far: -5.365880078392843
FRENCH: P(e|aim) = -0.4857556242438642 ==> log prob of sentence so far: -5.8743908175965265

4GRAM: imeb
ENGLISH: P(b|ime) = -1.5095463446745085 ==> log prob of sentence so far: -6.875426423067352
FRENCH: P(b|ime) = -2.2895889525425965 ==> log prob of sentence so far: -8.163979770139123

4GRAM: mebr
ENGLISH: P(r|meb) = -1.8532925186295284 ==> log prob of sentence so far: -8.72871894169688
FRENCH: P(r|meb) = -1.0334237554869496 ==> log prob of sentence so far: -9.197403525626074

4GRAM: ebro
ENGLISH: P(o|ebr) = -0.6184504075161318 ==> log prob of sentence so far: -9.347169349213011
FRENCH: P(o|ebr) = -1.1189757896346233 ==> log prob of sentence so far: -10.316379315260697

4GRAM: broo
ENGLISH: P(o|bro) = -1.3335592204909013 ==> log prob of sentence so far: -10.680728569703913
FRENCH: P(o|bro) = -1.0 ==> log prob of sentence so far: -11.316379315260697

4GRAM: rook
ENGLISH: P(k|roo) = -1.0176089450581944 ==> log prob of sentence so far: -11.698337514762107
FRENCH: P(k|roo) = -0.6020599913279624 ==> log prob of sentence so far: -11.91843930658866

According to the 4gram model, the sentence is in English
----------------
