NTSB exhorte l'installation d'altimetres radar.

1GRAM MODEL:

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -1.1615969374026291
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -1.1226374766145046

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -2.1958205751869015
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -2.288604224711258

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -3.366961587915503
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -3.353051939600914

1GRAM: b
ENGLISH: P(b) = -1.7519519887518187 ==> log prob of sentence so far: -5.118913576667321
FRENCH: P(b) = -2.0518881761829366 ==> log prob of sentence so far: -5.404940115783851

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -6.028984794727228
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -6.171905930306307

1GRAM: x
ENGLISH: P(x) = -2.961523357271089 ==> log prob of sentence so far: -8.990508151998316
FRENCH: P(x) = -2.338646988759627 ==> log prob of sentence so far: -8.510552919065933

1GRAM: h
ENGLISH: P(h) = -1.180274162400034 ==> log prob of sentence so far: -10.17078231439835
FRENCH: P(h) = -2.1056297044472685 ==> log prob of sentence so far: -10.616182623513202

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -11.308566074983844
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -11.890525953830856

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -12.56983897629594
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -13.074554592901313

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -13.604062614080213
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -14.240521340998066

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -14.51413383214012
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -15.007487155520522

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -15.86190271756112
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -16.275699635806046

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -17.024428197835032
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -17.410906078474888

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -18.18602513523766
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -18.53354355508939

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -19.35716614796626
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -19.597991269979048

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -20.391389785750533
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -20.7639580180758

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -21.478178466792986
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -21.84031111318131

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -22.825947352213987
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -23.108523593466835

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -24.173716237634988
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -24.376736073752358

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -25.26050491867744
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -25.45308916885787

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -26.294728556461713
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -26.619055916954622

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -27.457254036735623
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -27.754262359623464

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -28.59503779732112
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -29.02860568994112

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -29.756634734723747
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -30.151243166555624

1GRAM: d
ENGLISH: P(d) = -1.396179917541683 ==> log prob of sentence so far: -31.15281465226543
FRENCH: P(d) = -1.4202616779892512 ==> log prob of sentence so far: -31.571504844544876

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -32.23960333330788
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -32.647857939650386

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -33.58737221872889
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -33.916070419935906

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -34.62159585651316
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -35.082037168032656

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -35.784121336787074
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -36.2172436107015

1GRAM: m
ENGLISH: P(m) = -1.6111104829831362 ==> log prob of sentence so far: -37.39523181977021
FRENCH: P(m) = -1.5129486053385504 ==> log prob of sentence so far: -37.73019221604005

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -38.305303037830114
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -38.49715803056251

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -39.33952667561439
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -39.66312477865927

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -40.600799576926484
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -40.84715341772972

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -41.51087079498639
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -41.61411923225218

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -42.68201180771499
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -42.67856694714183

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -43.94328470902709
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -43.86259558621229

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -45.030073390069546
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -44.9389486813178

1GRAM: d
ENGLISH: P(d) = -1.396179917541683 ==> log prob of sentence so far: -46.426253307611226
FRENCH: P(d) = -1.4202616779892512 ==> log prob of sentence so far: -46.35921035930705

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -47.51304198865368
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -47.43556345441256

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -48.77431488996578
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -48.619592093483014

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: nt
ENGLISH: P(t|n) = -0.8262590589426815 ==> log prob of sentence so far: -0.8262590589426815
FRENCH: P(t|n) = -0.6409503859281479 ==> log prob of sentence so far: -0.6409503859281479

2GRAM: ts
ENGLISH: P(s|t) = -1.3509351575782058 ==> log prob of sentence so far: -2.1771942165208875
FRENCH: P(s|t) = -1.2204919099668523 ==> log prob of sentence so far: -1.8614422958950003

2GRAM: sb
ENGLISH: P(b|s) = -1.6901087323341424 ==> log prob of sentence so far: -3.8673029488550297
FRENCH: P(b|s) = -2.0218271461036785 ==> log prob of sentence so far: -3.883269441998679

2GRAM: be
ENGLISH: P(e|b) = -0.6127645620318631 ==> log prob of sentence so far: -4.480067510886893
FRENCH: P(e|b) = -1.0602929037444118 ==> log prob of sentence so far: -4.943562345743091

2GRAM: ex
ENGLISH: P(x|e) = -2.1630835062535545 ==> log prob of sentence so far: -6.643151017140447
FRENCH: P(x|e) = -2.2665710379855373 ==> log prob of sentence so far: -7.210133383728628

2GRAM: xh
ENGLISH: P(h|x) = -1.3903115686159317 ==> log prob of sentence so far: -8.033462585756379
FRENCH: P(h|x) = -1.5290992152969005 ==> log prob of sentence so far: -8.739232599025529

2GRAM: ho
ENGLISH: P(o|h) = -1.1036697663560255 ==> log prob of sentence so far: -9.137132352112404
FRENCH: P(o|h) = -0.8331794160095403 ==> log prob of sentence so far: -9.57241201503507

2GRAM: or
ENGLISH: P(r|o) = -0.9303205281138703 ==> log prob of sentence so far: -10.067452880226275
FRENCH: P(r|o) = -1.0254415297583428 ==> log prob of sentence so far: -10.597853544793413

2GRAM: rt
ENGLISH: P(t|r) = -1.0948700507529459 ==> log prob of sentence so far: -11.16232293097922
FRENCH: P(t|r) = -1.394553913830865 ==> log prob of sentence so far: -11.992407458624278

2GRAM: te
ENGLISH: P(e|t) = -1.0589086136007413 ==> log prob of sentence so far: -12.221231544579961
FRENCH: P(e|t) = -0.674136919284091 ==> log prob of sentence so far: -12.66654437790837

2GRAM: el
ENGLISH: P(l|e) = -1.3277804884685662 ==> log prob of sentence so far: -13.549012033048527
FRENCH: P(l|e) = -1.2044345212229748 ==> log prob of sentence so far: -13.870978899131345

2GRAM: li
ENGLISH: P(i|l) = -0.9281280591492745 ==> log prob of sentence so far: -14.477140092197802
FRENCH: P(i|l) = -1.2029658890830615 ==> log prob of sentence so far: -15.073944788214407

2GRAM: in
ENGLISH: P(n|i) = -0.5177328887875342 ==> log prob of sentence so far: -14.994872980985336
FRENCH: P(n|i) = -0.8962746820270037 ==> log prob of sentence so far: -15.97021947024141

2GRAM: ns
ENGLISH: P(s|n) = -1.2471781899291021 ==> log prob of sentence so far: -16.24205117091444
FRENCH: P(s|n) = -0.9211627926268392 ==> log prob of sentence so far: -16.891382262868248

2GRAM: st
ENGLISH: P(t|s) = -0.7036883326029575 ==> log prob of sentence so far: -16.945739503517398
FRENCH: P(t|s) = -1.237330102985858 ==> log prob of sentence so far: -18.128712365854106

2GRAM: ta
ENGLISH: P(a|t) = -1.1697948806890075 ==> log prob of sentence so far: -18.115534384206406
FRENCH: P(a|t) = -0.8827723186309387 ==> log prob of sentence so far: -19.011484684485044

2GRAM: al
ENGLISH: P(l|a) = -0.961931979448657 ==> log prob of sentence so far: -19.077466363655063
FRENCH: P(l|a) = -1.191827391645171 ==> log prob of sentence so far: -20.203312076130214

2GRAM: ll
ENGLISH: P(l|l) = -0.8384371303603633 ==> log prob of sentence so far: -19.915903494015424
FRENCH: P(l|l) = -1.0860879345809604 ==> log prob of sentence so far: -21.289400010711173

2GRAM: la
ENGLISH: P(a|l) = -1.002845754477944 ==> log prob of sentence so far: -20.91874924849337
FRENCH: P(a|l) = -0.6764237550347996 ==> log prob of sentence so far: -21.965823765745974

2GRAM: at
ENGLISH: P(t|a) = -0.8627525070949107 ==> log prob of sentence so far: -21.78150175558828
FRENCH: P(t|a) = -1.2479713976288138 ==> log prob of sentence so far: -23.213795163374787

2GRAM: ti
ENGLISH: P(i|t) = -1.0855982270227826 ==> log prob of sentence so far: -22.867099982611062
FRENCH: P(i|t) = -0.9860184878476054 ==> log prob of sentence so far: -24.199813651222392

2GRAM: io
ENGLISH: P(o|i) = -1.4835293380988173 ==> log prob of sentence so far: -24.350629320709878
FRENCH: P(o|i) = -1.375630384753757 ==> log prob of sentence so far: -25.575444035976147

2GRAM: on
ENGLISH: P(n|o) = -0.8622085952026249 ==> log prob of sentence so far: -25.212837915912502
FRENCH: P(n|o) = -0.5205456677173079 ==> log prob of sentence so far: -26.095989703693455

2GRAM: nd
ENGLISH: P(d|n) = -0.7555075870204362 ==> log prob of sentence so far: -25.968345502932937
FRENCH: P(d|n) = -1.0271635643855614 ==> log prob of sentence so far: -27.123153268079015

2GRAM: da
ENGLISH: P(a|d) = -1.000023851849847 ==> log prob of sentence so far: -26.968369354782784
FRENCH: P(a|d) = -0.9714919974560264 ==> log prob of sentence so far: -28.094645265535043

2GRAM: al
ENGLISH: P(l|a) = -0.961931979448657 ==> log prob of sentence so far: -27.93030133423144
FRENCH: P(l|a) = -1.191827391645171 ==> log prob of sentence so far: -29.286472657180212

2GRAM: lt
ENGLISH: P(t|l) = -1.4623928779809854 ==> log prob of sentence so far: -29.392694212212426
FRENCH: P(t|l) = -2.3338122094642793 ==> log prob of sentence so far: -31.620284866644493

2GRAM: ti
ENGLISH: P(i|t) = -1.0855982270227826 ==> log prob of sentence so far: -30.478292439235208
FRENCH: P(i|t) = -0.9860184878476054 ==> log prob of sentence so far: -32.6063033544921

2GRAM: im
ENGLISH: P(m|i) = -1.3479869275073166 ==> log prob of sentence so far: -31.826279366742526
FRENCH: P(m|i) = -1.5427023844256251 ==> log prob of sentence so far: -34.14900573891772

2GRAM: me
ENGLISH: P(e|m) = -0.5945354301857667 ==> log prob of sentence so far: -32.42081479692829
FRENCH: P(e|m) = -0.43058576450809083 ==> log prob of sentence so far: -34.57959150342581

2GRAM: et
ENGLISH: P(t|e) = -1.1805749996736075 ==> log prob of sentence so far: -33.6013897966019
FRENCH: P(t|e) = -1.081730001747917 ==> log prob of sentence so far: -35.661321505173724

2GRAM: tr
ENGLISH: P(r|t) = -1.5560569993606441 ==> log prob of sentence so far: -35.15744679596254
FRENCH: P(r|t) = -0.9624973465112773 ==> log prob of sentence so far: -36.623818851685

2GRAM: re
ENGLISH: P(e|r) = -0.6282077999008819 ==> log prob of sentence so far: -35.78565459586343
FRENCH: P(e|r) = -0.49097082634029093 ==> log prob of sentence so far: -37.11478967802529

2GRAM: es
ENGLISH: P(s|e) = -0.944867640322014 ==> log prob of sentence so far: -36.73052223618544
FRENCH: P(s|e) = -0.7261499946209886 ==> log prob of sentence so far: -37.84093967264628

2GRAM: sr
ENGLISH: P(r|s) = -2.2119046616009927 ==> log prob of sentence so far: -38.94242689778643
FRENCH: P(r|s) = -1.7292075293378615 ==> log prob of sentence so far: -39.57014720198414

2GRAM: ra
ENGLISH: P(a|r) = -1.0362656096552068 ==> log prob of sentence so far: -39.97869250744164
FRENCH: P(a|r) = -0.9045486003753557 ==> log prob of sentence so far: -40.4746958023595

2GRAM: ad
ENGLISH: P(d|a) = -1.3608830323254337 ==> log prob of sentence so far: -41.339575539767075
FRENCH: P(d|a) = -1.6547663418439653 ==> log prob of sentence so far: -42.129462144203465

2GRAM: da
ENGLISH: P(a|d) = -1.000023851849847 ==> log prob of sentence so far: -42.33959939161692
FRENCH: P(a|d) = -0.9714919974560264 ==> log prob of sentence so far: -43.10095414165949

2GRAM: ar
ENGLISH: P(r|a) = -0.9854656313970852 ==> log prob of sentence so far: -43.325065023014005
FRENCH: P(r|a) = -1.0331920938948058 ==> log prob of sentence so far: -44.1341462355543

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: nts
ENGLISH: P(s|nt) = -1.2988828455848969 ==> log prob of sentence so far: -1.2988828455848969
FRENCH: P(s|nt) = -0.9920378032111561 ==> log prob of sentence so far: -0.9920378032111561

3GRAM: tsb
ENGLISH: P(b|ts) = -1.5318319541693262 ==> log prob of sentence so far: -2.8307147997542232
FRENCH: P(b|ts) = -2.2000684080509716 ==> log prob of sentence so far: -3.1921062112621277

3GRAM: sbe
ENGLISH: P(e|sb) = -0.5805640611846906 ==> log prob of sentence so far: -3.4112788609389137
FRENCH: P(e|sb) = -1.037995318561433 ==> log prob of sentence so far: -4.2301015298235605

3GRAM: bex
ENGLISH: P(x|be) = -3.206717963375833 ==> log prob of sentence so far: -6.617996824314747
FRENCH: P(x|be) = -3.0342272607705505 ==> log prob of sentence so far: -7.264328790594111

3GRAM: exh
ENGLISH: P(h|ex) = -1.3358836190412018 ==> log prob of sentence so far: -7.953880443355949
FRENCH: P(h|ex) = -2.413634997198556 ==> log prob of sentence so far: -9.677963787792667

3GRAM: xho
ENGLISH: P(o|xh) = -1.1962946451439682 ==> log prob of sentence so far: -9.150175088499918
FRENCH: P(o|xh) = -0.9563354989451687 ==> log prob of sentence so far: -10.634299286737836

3GRAM: hor
ENGLISH: P(r|ho) = -1.039676538764383 ==> log prob of sentence so far: -10.1898516272643
FRENCH: P(r|ho) = -0.6603576718500624 ==> log prob of sentence so far: -11.294656958587899

3GRAM: ort
ENGLISH: P(t|or) = -0.7619272836955264 ==> log prob of sentence so far: -10.951778910959826
FRENCH: P(t|or) = -0.7256695930598063 ==> log prob of sentence so far: -12.020326551647704

3GRAM: rte
ENGLISH: P(e|rt) = -1.1087936599486459 ==> log prob of sentence so far: -12.060572570908471
FRENCH: P(e|rt) = -0.48256588702277664 ==> log prob of sentence so far: -12.50289243867048

3GRAM: tel
ENGLISH: P(l|te) = -1.321842063676577 ==> log prob of sentence so far: -13.382414634585048
FRENCH: P(l|te) = -1.3088633530177163 ==> log prob of sentence so far: -13.811755791688197

3GRAM: eli
ENGLISH: P(i|el) = -0.8560453108083541 ==> log prob of sentence so far: -14.238459945393402
FRENCH: P(i|el) = -1.2614776698913865 ==> log prob of sentence so far: -15.073233461579584

3GRAM: lin
ENGLISH: P(n|li) = -0.5624987091999829 ==> log prob of sentence so far: -14.800958654593384
FRENCH: P(n|li) = -0.8803317223670838 ==> log prob of sentence so far: -15.953565183946667

3GRAM: ins
ENGLISH: P(s|in) = -1.265665140129588 ==> log prob of sentence so far: -16.06662379472297
FRENCH: P(s|in) = -0.9046547686775224 ==> log prob of sentence so far: -16.85821995262419

3GRAM: nst
ENGLISH: P(t|ns) = -0.648375307618374 ==> log prob of sentence so far: -16.714999102341345
FRENCH: P(t|ns) = -1.2224595056954601 ==> log prob of sentence so far: -18.08067945831965

3GRAM: sta
ENGLISH: P(a|st) = -0.8511298782309611 ==> log prob of sentence so far: -17.566128980572305
FRENCH: P(a|st) = -0.7682219323189109 ==> log prob of sentence so far: -18.848901390638563

3GRAM: tal
ENGLISH: P(l|ta) = -0.9227750667747268 ==> log prob of sentence so far: -18.48890404734703
FRENCH: P(l|ta) = -1.0608917336255352 ==> log prob of sentence so far: -19.909793124264098

3GRAM: all
ENGLISH: P(l|al) = -0.4473969486278356 ==> log prob of sentence so far: -18.936300995974868
FRENCH: P(l|al) = -0.9318410009992406 ==> log prob of sentence so far: -20.84163412526334

3GRAM: lla
ENGLISH: P(a|ll) = -1.0933323486667397 ==> log prob of sentence so far: -20.029633344641606
FRENCH: P(a|ll) = -0.9490995347169217 ==> log prob of sentence so far: -21.790733659980262

3GRAM: lat
ENGLISH: P(t|la) = -1.0572861594736456 ==> log prob of sentence so far: -21.08691950411525
FRENCH: P(t|la) = -1.0653686930057091 ==> log prob of sentence so far: -22.85610235298597

3GRAM: ati
ENGLISH: P(i|at) = -0.8471661636477793 ==> log prob of sentence so far: -21.93408566776303
FRENCH: P(i|at) = -0.5129916565502587 ==> log prob of sentence so far: -23.36909400953623

3GRAM: tio
ENGLISH: P(o|ti) = -0.8229915400080453 ==> log prob of sentence so far: -22.757077207771072
FRENCH: P(o|ti) = -0.6092653740555233 ==> log prob of sentence so far: -23.978359383591755

3GRAM: ion
ENGLISH: P(n|io) = -0.18444187343284554 ==> log prob of sentence so far: -22.941519081203918
FRENCH: P(n|io) = -0.045757490560675115 ==> log prob of sentence so far: -24.02411687415243

3GRAM: ond
ENGLISH: P(d|on) = -1.3200306972485392 ==> log prob of sentence so far: -24.26154977845246
FRENCH: P(d|on) = -0.8229868114477428 ==> log prob of sentence so far: -24.84710368560017

3GRAM: nda
ENGLISH: P(a|nd) = -1.0956254971887573 ==> log prob of sentence so far: -25.357175275641215
FRENCH: P(a|nd) = -0.7620399693968312 ==> log prob of sentence so far: -25.609143654997002

3GRAM: dal
ENGLISH: P(l|da) = -1.0245471458737645 ==> log prob of sentence so far: -26.38172242151498
FRENCH: P(l|da) = -1.5763954664532853 ==> log prob of sentence so far: -27.185539121450287

3GRAM: alt
ENGLISH: P(t|al) = -1.4822924209578305 ==> log prob of sentence so far: -27.86401484247281
FRENCH: P(t|al) = -1.9643702258261606 ==> log prob of sentence so far: -29.14990934727645

3GRAM: lti
ENGLISH: P(i|lt) = -1.168530385866463 ==> log prob of sentence so far: -29.032545228339274
FRENCH: P(i|lt) = -1.0 ==> log prob of sentence so far: -30.14990934727645

3GRAM: tim
ENGLISH: P(m|ti) = -1.0 ==> log prob of sentence so far: -30.032545228339274
FRENCH: P(m|ti) = -1.3093265711311841 ==> log prob of sentence so far: -31.459235918407632

3GRAM: ime
ENGLISH: P(e|im) = -0.5939876538828333 ==> log prob of sentence so far: -30.626532882222108
FRENCH: P(e|im) = -0.4859739967020765 ==> log prob of sentence so far: -31.945209915109707

3GRAM: met
ENGLISH: P(t|me) = -0.9010903984534266 ==> log prob of sentence so far: -31.527623280675535
FRENCH: P(t|me) = -1.0327838539396605 ==> log prob of sentence so far: -32.97799376904937

3GRAM: etr
ENGLISH: P(r|et) = -1.4442381972842688 ==> log prob of sentence so far: -32.971861477959806
FRENCH: P(r|et) = -0.8436486436892003 ==> log prob of sentence so far: -33.82164241273857

3GRAM: tre
ENGLISH: P(e|tr) = -0.7361230241355003 ==> log prob of sentence so far: -33.70798450209531
FRENCH: P(e|tr) = -0.2180447270217422 ==> log prob of sentence so far: -34.03968713976031

3GRAM: res
ENGLISH: P(s|re) = -0.8768268614328087 ==> log prob of sentence so far: -34.58481136352812
FRENCH: P(s|re) = -0.617883484477163 ==> log prob of sentence so far: -34.65757062423747

3GRAM: esr
ENGLISH: P(r|es) = -2.479105053133867 ==> log prob of sentence so far: -37.06391641666198
FRENCH: P(r|es) = -1.6094653824910945 ==> log prob of sentence so far: -36.26703600672857

3GRAM: sra
ENGLISH: P(a|sr) = -0.8671424398043457 ==> log prob of sentence so far: -37.93105885646633
FRENCH: P(a|sr) = -0.7187458528597083 ==> log prob of sentence so far: -36.98578185958827

3GRAM: rad
ENGLISH: P(d|ra) = -1.5357623021434352 ==> log prob of sentence so far: -39.46682115860976
FRENCH: P(d|ra) = -1.6650054569312283 ==> log prob of sentence so far: -38.6507873165195

3GRAM: ada
ENGLISH: P(a|ad) = -1.0188106987464118 ==> log prob of sentence so far: -40.485631857356175
FRENCH: P(a|ad) = -1.2299477159194103 ==> log prob of sentence so far: -39.880735032438906

3GRAM: dar
ENGLISH: P(r|da) = -1.0441132692183577 ==> log prob of sentence so far: -41.52974512657453
FRENCH: P(r|da) = -1.7121552369249358 ==> log prob of sentence so far: -41.59289026936384

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: ntsb
ENGLISH: P(b|nts) = -1.3927444716668365 ==> log prob of sentence so far: -1.3927444716668365
FRENCH: P(b|nts) = -2.010078622489823 ==> log prob of sentence so far: -2.010078622489823

4GRAM: tsbe
ENGLISH: P(e|tsb) = -0.8397473216301556 ==> log prob of sentence so far: -2.232491793296992
FRENCH: P(e|tsb) = -1.7781512503836436 ==> log prob of sentence so far: -3.7882298728734662

4GRAM: sbex
ENGLISH: P(x|sbe) = -2.841359470454855 ==> log prob of sentence so far: -5.0738512637518465
FRENCH: P(x|sbe) = -2.113943352306837 ==> log prob of sentence so far: -5.902173225180303

4GRAM: bexh
ENGLISH: P(h|bex) = -1.4771212547196624 ==> log prob of sentence so far: -6.550972518471509
FRENCH: P(h|bex) = -1.4313637641589874 ==> log prob of sentence so far: -7.33353698933929

4GRAM: exho
ENGLISH: P(o|exh) = -1.5141048209728325 ==> log prob of sentence so far: -8.065077339444342
FRENCH: P(o|exh) = -1.4771212547196624 ==> log prob of sentence so far: -8.810658244058953

4GRAM: xhor
ENGLISH: P(r|xho) = -1.0280287236002434 ==> log prob of sentence so far: -9.093106063044585
FRENCH: P(r|xho) = -1.2041199826559248 ==> log prob of sentence so far: -10.014778226714878

4GRAM: hort
ENGLISH: P(t|hor) = -0.7650465433245978 ==> log prob of sentence so far: -9.858152606369183
FRENCH: P(t|hor) = -2.57978359661681 ==> log prob of sentence so far: -12.594561823331688

4GRAM: orte
ENGLISH: P(e|ort) = -1.3766738365189883 ==> log prob of sentence so far: -11.23482644288817
FRENCH: P(e|ort) = -0.32764867100639977 ==> log prob of sentence so far: -12.922210494338088

4GRAM: rtel
ENGLISH: P(l|rte) = -1.9731278535996988 ==> log prob of sentence so far: -13.20795429648787
FRENCH: P(l|rte) = -1.3182098333557524 ==> log prob of sentence so far: -14.24042032769384

4GRAM: teli
ENGLISH: P(i|tel) = -1.6376074404447676 ==> log prob of sentence so far: -14.845561736932638
FRENCH: P(i|tel) = -1.2704810749868392 ==> log prob of sentence so far: -15.51090140268068

4GRAM: elin
ENGLISH: P(n|eli) = -0.6155321074082071 ==> log prob of sentence so far: -15.461093844340844
FRENCH: P(n|eli) = -0.9589889444637866 ==> log prob of sentence so far: -16.469890347144467

4GRAM: lins
ENGLISH: P(s|lin) = -1.8268645728437383 ==> log prob of sentence so far: -17.287958417184583
FRENCH: P(s|lin) = -1.0222051930580862 ==> log prob of sentence so far: -17.492095540202552

4GRAM: inst
ENGLISH: P(t|ins) = -0.40993929817825187 ==> log prob of sentence so far: -17.697897715362835
FRENCH: P(t|ins) = -0.7404992815639037 ==> log prob of sentence so far: -18.232594821766455

4GRAM: nsta
ENGLISH: P(a|nst) = -0.5256016036158109 ==> log prob of sentence so far: -18.223499318978646
FRENCH: P(a|nst) = -0.45334107857328193 ==> log prob of sentence so far: -18.685935900339736

4GRAM: stal
ENGLISH: P(l|sta) = -1.4078826683094643 ==> log prob of sentence so far: -19.631381987288112
FRENCH: P(l|sta) = -1.212224101272075 ==> log prob of sentence so far: -19.89816000161181

4GRAM: tall
ENGLISH: P(l|tal) = -0.3888064072259295 ==> log prob of sentence so far: -20.02018839451404
FRENCH: P(l|tal) = -1.1289414790619738 ==> log prob of sentence so far: -21.027101480673785

4GRAM: alla
ENGLISH: P(a|all) = -1.1371291625612496 ==> log prob of sentence so far: -21.157317557075288
FRENCH: P(a|all) = -0.4893833476813971 ==> log prob of sentence so far: -21.516484828355182

4GRAM: llat
ENGLISH: P(t|lla) = -0.9282870655951246 ==> log prob of sentence so far: -22.085604622670413
FRENCH: P(t|lla) = -1.3900819957379003 ==> log prob of sentence so far: -22.906566824093083

4GRAM: lati
ENGLISH: P(i|lat) = -0.5114492834995558 ==> log prob of sentence so far: -22.59705390616997
FRENCH: P(i|lat) = -0.6517862580373116 ==> log prob of sentence so far: -23.558353082130395

4GRAM: atio
ENGLISH: P(o|ati) = -0.5139360607573906 ==> log prob of sentence so far: -23.11098996692736
FRENCH: P(o|ati) = -0.262780870366712 ==> log prob of sentence so far: -23.821133952497107

4GRAM: tion
ENGLISH: P(n|tio) = -0.025914061708853266 ==> log prob of sentence so far: -23.136904028636213
FRENCH: P(n|tio) = -0.007433315988208825 ==> log prob of sentence so far: -23.828567268485315

4GRAM: iond
ENGLISH: P(d|ion) = -2.1996261341736654 ==> log prob of sentence so far: -25.33653016280988
FRENCH: P(d|ion) = -0.7724141196356854 ==> log prob of sentence so far: -24.600981388121

4GRAM: onda
ENGLISH: P(a|ond) = -1.407269474445642 ==> log prob of sentence so far: -26.74379963725552
FRENCH: P(a|ond) = -1.235842319107602 ==> log prob of sentence so far: -25.8368237072286

4GRAM: ndal
ENGLISH: P(l|nda) = -0.7724875787699674 ==> log prob of sentence so far: -27.51628721602549
FRENCH: P(l|nda) = -1.5583510634425564 ==> log prob of sentence so far: -27.395174770671158

4GRAM: dalt
ENGLISH: P(t|dal) = -2.0024746191278555 ==> log prob of sentence so far: -29.518761835153345
FRENCH: P(t|dal) = -2.2405492482826 ==> log prob of sentence so far: -29.63572401895376

4GRAM: alti
ENGLISH: P(i|alt) = -0.9429614990296359 ==> log prob of sentence so far: -30.46172333418298
FRENCH: P(i|alt) = -1.3263358609287514 ==> log prob of sentence so far: -30.96205987988251

4GRAM: ltim
ENGLISH: P(m|lti) = -0.6059902849563948 ==> log prob of sentence so far: -31.067713619139376
FRENCH: P(m|lti) = -1.792391689498254 ==> log prob of sentence so far: -32.75445156938076

4GRAM: time
ENGLISH: P(e|tim) = -0.08451454142881903 ==> log prob of sentence so far: -31.152228160568196
FRENCH: P(e|tim) = -0.31509744354670716 ==> log prob of sentence so far: -33.06954901292747

4GRAM: imet
ENGLISH: P(t|ime) = -0.975697465505919 ==> log prob of sentence so far: -32.127925626074116
FRENCH: P(t|ime) = -0.8271909546436406 ==> log prob of sentence so far: -33.89673996757111

4GRAM: metr
ENGLISH: P(r|met) = -1.4844512933960639 ==> log prob of sentence so far: -33.61237691947018
FRENCH: P(r|met) = -0.2570907438235675 ==> log prob of sentence so far: -34.153830711394676

4GRAM: etre
ENGLISH: P(e|etr) = -0.7254448998676407 ==> log prob of sentence so far: -34.33782181933782
FRENCH: P(e|etr) = -0.21525795338227585 ==> log prob of sentence so far: -34.36908866477695

4GRAM: tres
ENGLISH: P(s|tre) = -0.9905584437671585 ==> log prob of sentence so far: -35.328380263104975
FRENCH: P(s|tre) = -0.5517857188905029 ==> log prob of sentence so far: -34.92087438366745

4GRAM: resr
ENGLISH: P(r|res) = -3.009167506240904 ==> log prob of sentence so far: -38.33754776934588
FRENCH: P(r|res) = -1.9430703311269109 ==> log prob of sentence so far: -36.863944714794364

4GRAM: esra
ENGLISH: P(a|esr) = -1.188325715472693 ==> log prob of sentence so far: -39.525873484818575
FRENCH: P(a|esr) = -0.6671547760969821 ==> log prob of sentence so far: -37.53109949089134

4GRAM: srad
ENGLISH: P(d|sra) = -1.166331421766525 ==> log prob of sentence so far: -40.6922049065851
FRENCH: P(d|sra) = -2.1760912590556813 ==> log prob of sentence so far: -39.707190749947024

4GRAM: rada
ENGLISH: P(a|rad) = -1.3602729117694186 ==> log prob of sentence so far: -42.05247781835452
FRENCH: P(a|rad) = -0.7560369360930713 ==> log prob of sentence so far: -40.46322768604009

4GRAM: adar
ENGLISH: P(r|ada) = -1.1105209982538713 ==> log prob of sentence so far: -43.162998816608386
FRENCH: P(r|ada) = -2.2405492482826 ==> log prob of sentence so far: -42.70377693432269

According to the 4gram model, the sentence is in French
----------------
