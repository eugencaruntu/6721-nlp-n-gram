Tinctures constitute limited palette of colours.

1GRAM MODEL:

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -1.0342236377842724
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -1.1659667480967535

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -2.1967491180581833
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -2.301173190765597

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -3.3583460554608124
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -3.423810667380102

1GRAM: c
ENGLISH: P(c) = -1.626587631644617 ==> log prob of sentence so far: -4.984933687105429
FRENCH: P(c) = -1.492223661080204 ==> log prob of sentence so far: -4.916034328460306

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -6.019157324889702
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -6.082001076557059

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -7.571645493847616
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -7.28813613773268

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -8.832918395159712
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -8.472164776803137

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -9.742989613219619
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -9.239130591325594

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -10.91413062594822
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -10.30357830621525

1GRAM: c
ENGLISH: P(c) = -1.626587631644617 ==> log prob of sentence so far: -12.540718257592836
FRENCH: P(c) = -1.492223661080204 ==> log prob of sentence so far: -11.795801967295453

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -13.67850201817833
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -13.070145297613108

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -14.84009895558096
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -14.192782774227613

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -16.01123996830956
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -15.25723048911727

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -17.045463606093833
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -16.423197237214023

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -18.207989086367743
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -17.558403679882865

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -19.242212724152015
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -18.72437042797962

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -20.79470089310993
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -19.93050548915524

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -21.828924530894202
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -21.096472237251994

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -22.73899574895411
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -21.86343805177445

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -24.08676463437511
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -23.131650532059975

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -25.24929011464902
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -24.266856974728817

1GRAM: m
ENGLISH: P(m) = -1.6111104829831362 ==> log prob of sentence so far: -26.860400597632157
FRENCH: P(m) = -1.5129486053385504 ==> log prob of sentence so far: -25.779805580067368

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -28.022926077906067
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -26.91501202273621

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -29.05714971569034
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -28.080978770832964

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -29.967220933750248
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -28.84794458535542

1GRAM: d
ENGLISH: P(d) = -1.396179917541683 ==> log prob of sentence so far: -31.36340085129193
FRENCH: P(d) = -1.4202616779892512 ==> log prob of sentence so far: -30.268206263344673

1GRAM: p
ENGLISH: P(p) = -1.7418254600332643 ==> log prob of sentence so far: -33.1052263113252
FRENCH: P(p) = -1.538635710757223 ==> log prob of sentence so far: -31.806841974101896

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -34.19201499236765
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -32.8831950692074

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -35.53978387778865
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -34.15140754949292

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -36.449855095848555
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -34.91837336401538

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -37.48407873363283
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -36.08434011211213

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -38.5183023714171
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -37.25030686020888

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -39.428373589477005
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -38.01727267473134

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -40.5661573500625
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -39.29161600504899

1GRAM: f
ENGLISH: P(f) = -1.660275542753472 ==> log prob of sentence so far: -42.22643289281597
FRENCH: P(f) = -1.9917263037969526 ==> log prob of sentence so far: -41.283342308845945

1GRAM: c
ENGLISH: P(c) = -1.626587631644617 ==> log prob of sentence so far: -43.85302052446059
FRENCH: P(c) = -1.492223661080204 ==> log prob of sentence so far: -42.77556596992615

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -44.99080428504608
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -44.049909300243804

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -46.338573170467086
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -45.318121780529324

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -47.47635693105258
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -46.59246511084698

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -49.02884510001049
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -47.7986001720226

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -50.29011800132259
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -48.98262881109306

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -51.461259014051194
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -50.04707652598271

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: ti
ENGLISH: P(i|t) = -1.0855982270227826 ==> log prob of sentence so far: -1.0855982270227826
FRENCH: P(i|t) = -0.9860184878476054 ==> log prob of sentence so far: -0.9860184878476054

2GRAM: in
ENGLISH: P(n|i) = -0.5177328887875342 ==> log prob of sentence so far: -1.6033311158103167
FRENCH: P(n|i) = -0.8962746820270037 ==> log prob of sentence so far: -1.882293169874609

2GRAM: nc
ENGLISH: P(c|n) = -1.3671670300676497 ==> log prob of sentence so far: -2.9704981458779667
FRENCH: P(c|n) = -1.2369677008574336 ==> log prob of sentence so far: -3.1192608707320426

2GRAM: ct
ENGLISH: P(t|c) = -1.2537180590862311 ==> log prob of sentence so far: -4.224216204964198
FRENCH: P(t|c) = -1.5425616787629628 ==> log prob of sentence so far: -4.661822549495005

2GRAM: tu
ENGLISH: P(u|t) = -1.6185934558292467 ==> log prob of sentence so far: -5.842809660793445
FRENCH: P(u|t) = -1.4956585238034739 ==> log prob of sentence so far: -6.157481073298479

2GRAM: ur
ENGLISH: P(r|u) = -0.9178087681418121 ==> log prob of sentence so far: -6.760618428935257
FRENCH: P(r|u) = -0.7887730706784898 ==> log prob of sentence so far: -6.946254143976969

2GRAM: re
ENGLISH: P(e|r) = -0.6282077999008819 ==> log prob of sentence so far: -7.388826228836139
FRENCH: P(e|r) = -0.49097082634029093 ==> log prob of sentence so far: -7.43722497031726

2GRAM: es
ENGLISH: P(s|e) = -0.944867640322014 ==> log prob of sentence so far: -8.333693869158154
FRENCH: P(s|e) = -0.7261499946209886 ==> log prob of sentence so far: -8.16337496493825

2GRAM: sc
ENGLISH: P(c|s) = -1.566928188574753 ==> log prob of sentence so far: -9.900622057732907
FRENCH: P(c|s) = -1.3370330161780848 ==> log prob of sentence so far: -9.500407981116334

2GRAM: co
ENGLISH: P(o|c) = -0.7820327849639489 ==> log prob of sentence so far: -10.682654842696856
FRENCH: P(o|c) = -0.6556874666871543 ==> log prob of sentence so far: -10.156095447803489

2GRAM: on
ENGLISH: P(n|o) = -0.8622085952026249 ==> log prob of sentence so far: -11.544863437899481
FRENCH: P(n|o) = -0.5205456677173079 ==> log prob of sentence so far: -10.676641115520797

2GRAM: ns
ENGLISH: P(s|n) = -1.2471781899291021 ==> log prob of sentence so far: -12.792041627828583
FRENCH: P(s|n) = -0.9211627926268392 ==> log prob of sentence so far: -11.597803908147636

2GRAM: st
ENGLISH: P(t|s) = -0.7036883326029575 ==> log prob of sentence so far: -13.495729960431541
FRENCH: P(t|s) = -1.237330102985858 ==> log prob of sentence so far: -12.835134011133494

2GRAM: ti
ENGLISH: P(i|t) = -1.0855982270227826 ==> log prob of sentence so far: -14.581328187454323
FRENCH: P(i|t) = -0.9860184878476054 ==> log prob of sentence so far: -13.8211524989811

2GRAM: it
ENGLISH: P(t|i) = -0.9082997757934181 ==> log prob of sentence so far: -15.489627963247742
FRENCH: P(t|i) = -0.7742072823534025 ==> log prob of sentence so far: -14.595359781334501

2GRAM: tu
ENGLISH: P(u|t) = -1.6185934558292467 ==> log prob of sentence so far: -17.108221419076987
FRENCH: P(u|t) = -1.4956585238034739 ==> log prob of sentence so far: -16.091018305137975

2GRAM: ut
ENGLISH: P(t|u) = -0.8001988560731185 ==> log prob of sentence so far: -17.908420275150107
FRENCH: P(t|u) = -1.1016454823301614 ==> log prob of sentence so far: -17.19266378746814

2GRAM: te
ENGLISH: P(e|t) = -1.0589086136007413 ==> log prob of sentence so far: -18.96732888875085
FRENCH: P(e|t) = -0.674136919284091 ==> log prob of sentence so far: -17.86680070675223

2GRAM: el
ENGLISH: P(l|e) = -1.3277804884685662 ==> log prob of sentence so far: -20.295109377219415
FRENCH: P(l|e) = -1.2044345212229748 ==> log prob of sentence so far: -19.071235227975205

2GRAM: li
ENGLISH: P(i|l) = -0.9281280591492745 ==> log prob of sentence so far: -21.223237436368688
FRENCH: P(i|l) = -1.2029658890830615 ==> log prob of sentence so far: -20.274201117058265

2GRAM: im
ENGLISH: P(m|i) = -1.3479869275073166 ==> log prob of sentence so far: -22.571224363876006
FRENCH: P(m|i) = -1.5427023844256251 ==> log prob of sentence so far: -21.81690350148389

2GRAM: mi
ENGLISH: P(i|m) = -1.0704736226754992 ==> log prob of sentence so far: -23.641697986551506
FRENCH: P(i|m) = -1.0435924781504398 ==> log prob of sentence so far: -22.86049597963433

2GRAM: it
ENGLISH: P(t|i) = -0.9082997757934181 ==> log prob of sentence so far: -24.549997762344923
FRENCH: P(t|i) = -0.7742072823534025 ==> log prob of sentence so far: -23.63470326198773

2GRAM: te
ENGLISH: P(e|t) = -1.0589086136007413 ==> log prob of sentence so far: -25.608906375945665
FRENCH: P(e|t) = -0.674136919284091 ==> log prob of sentence so far: -24.308840181271822

2GRAM: ed
ENGLISH: P(d|e) = -1.0374576111626355 ==> log prob of sentence so far: -26.646363987108302
FRENCH: P(d|e) = -1.2790847979127375 ==> log prob of sentence so far: -25.58792497918456

2GRAM: dp
ENGLISH: P(p|d) = -1.8999648261340962 ==> log prob of sentence so far: -28.546328813242397
FRENCH: P(p|d) = -2.546261035950482 ==> log prob of sentence so far: -28.134186015135043

2GRAM: pa
ENGLISH: P(a|p) = -0.9079158130990952 ==> log prob of sentence so far: -29.45424462634149
FRENCH: P(a|p) = -0.6470605885380062 ==> log prob of sentence so far: -28.78124660367305

2GRAM: al
ENGLISH: P(l|a) = -0.961931979448657 ==> log prob of sentence so far: -30.41617660579015
FRENCH: P(l|a) = -1.191827391645171 ==> log prob of sentence so far: -29.97307399531822

2GRAM: le
ENGLISH: P(e|l) = -0.6938243412569666 ==> log prob of sentence so far: -31.110000947047116
FRENCH: P(e|l) = -0.40129432934375414 ==> log prob of sentence so far: -30.374368324661976

2GRAM: et
ENGLISH: P(t|e) = -1.1805749996736075 ==> log prob of sentence so far: -32.29057594672072
FRENCH: P(t|e) = -1.081730001747917 ==> log prob of sentence so far: -31.456098326409894

2GRAM: tt
ENGLISH: P(t|t) = -1.2608815244736162 ==> log prob of sentence so far: -33.55145747119434
FRENCH: P(t|t) = -1.3533095134855202 ==> log prob of sentence so far: -32.809407839895414

2GRAM: te
ENGLISH: P(e|t) = -1.0589086136007413 ==> log prob of sentence so far: -34.61036608479508
FRENCH: P(e|t) = -0.674136919284091 ==> log prob of sentence so far: -33.4835447591795

2GRAM: eo
ENGLISH: P(o|e) = -1.6145530196909663 ==> log prob of sentence so far: -36.22491910448605
FRENCH: P(o|e) = -2.314956123851811 ==> log prob of sentence so far: -35.79850088303131

2GRAM: of
ENGLISH: P(f|o) = -0.958643897502141 ==> log prob of sentence so far: -37.18356300198819
FRENCH: P(f|o) = -1.934132957908779 ==> log prob of sentence so far: -37.73263384094009

2GRAM: fc
ENGLISH: P(c|f) = -2.0553440621031522 ==> log prob of sentence so far: -39.23890706409134
FRENCH: P(c|f) = -2.75238652782667 ==> log prob of sentence so far: -40.48502036876676

2GRAM: co
ENGLISH: P(o|c) = -0.7820327849639489 ==> log prob of sentence so far: -40.02093984905529
FRENCH: P(o|c) = -0.6556874666871543 ==> log prob of sentence so far: -41.14070783545392

2GRAM: ol
ENGLISH: P(l|o) = -1.416314628982505 ==> log prob of sentence so far: -41.437254478037794
FRENCH: P(l|o) = -1.4870835318641464 ==> log prob of sentence so far: -42.62779136731806

2GRAM: lo
ENGLISH: P(o|l) = -1.0313867050170902 ==> log prob of sentence so far: -42.468641183054885
FRENCH: P(o|l) = -1.1639759810063668 ==> log prob of sentence so far: -43.79176734832443

2GRAM: ou
ENGLISH: P(u|o) = -0.9046394432126483 ==> log prob of sentence so far: -43.37328062626753
FRENCH: P(u|o) = -0.6277892700441801 ==> log prob of sentence so far: -44.41955661836861

2GRAM: ur
ENGLISH: P(r|u) = -0.9178087681418121 ==> log prob of sentence so far: -44.29108939440934
FRENCH: P(r|u) = -0.7887730706784898 ==> log prob of sentence so far: -45.2083296890471

2GRAM: rs
ENGLISH: P(s|r) = -1.1637455734200046 ==> log prob of sentence so far: -45.45483496782934
FRENCH: P(s|r) = -1.1996969344915898 ==> log prob of sentence so far: -46.40802662353869

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: tin
ENGLISH: P(n|ti) = -0.5910003398712387 ==> log prob of sentence so far: -0.5910003398712387
FRENCH: P(n|ti) = -1.0179633064247982 ==> log prob of sentence so far: -1.0179633064247982

3GRAM: inc
ENGLISH: P(c|in) = -1.5523553498909464 ==> log prob of sentence so far: -2.143355689762185
FRENCH: P(c|in) = -1.240583205394459 ==> log prob of sentence so far: -2.258546511819257

3GRAM: nct
ENGLISH: P(t|nc) = -1.459673675520367 ==> log prob of sentence so far: -3.603029365282552
FRENCH: P(t|nc) = -1.8313592244586394 ==> log prob of sentence so far: -4.089905736277896

3GRAM: ctu
ENGLISH: P(u|ct) = -0.9972003481973655 ==> log prob of sentence so far: -4.6002297134799175
FRENCH: P(u|ct) = -1.2506204921863808 ==> log prob of sentence so far: -5.340526228464277

3GRAM: tur
ENGLISH: P(r|tu) = -0.42280621296774995 ==> log prob of sentence so far: -5.023035926447667
FRENCH: P(r|tu) = -0.6302226885041013 ==> log prob of sentence so far: -5.970748916968379

3GRAM: ure
ENGLISH: P(e|ur) = -0.6394892164010537 ==> log prob of sentence so far: -5.662525142848721
FRENCH: P(e|ur) = -0.6870282860527741 ==> log prob of sentence so far: -6.657777203021153

3GRAM: res
ENGLISH: P(s|re) = -0.8768268614328087 ==> log prob of sentence so far: -6.53935200428153
FRENCH: P(s|re) = -0.617883484477163 ==> log prob of sentence so far: -7.275660687498316

3GRAM: esc
ENGLISH: P(c|es) = -1.5675944768247967 ==> log prob of sentence so far: -8.106946481106327
FRENCH: P(c|es) = -1.2064269173590478 ==> log prob of sentence so far: -8.482087604857364

3GRAM: sco
ENGLISH: P(o|sc) = -0.6018041477377043 ==> log prob of sentence so far: -8.708750628844031
FRENCH: P(o|sc) = -0.4727604336885343 ==> log prob of sentence so far: -8.954848038545899

3GRAM: con
ENGLISH: P(n|co) = -0.5370740525074478 ==> log prob of sentence so far: -9.245824681351479
FRENCH: P(n|co) = -0.43353626565368436 ==> log prob of sentence so far: -9.388384304199583

3GRAM: ons
ENGLISH: P(s|on) = -0.9343899383661486 ==> log prob of sentence so far: -10.180214619717628
FRENCH: P(s|on) = -0.566167526432315 ==> log prob of sentence so far: -9.954551830631898

3GRAM: nst
ENGLISH: P(t|ns) = -0.648375307618374 ==> log prob of sentence so far: -10.828589927336001
FRENCH: P(t|ns) = -1.2224595056954601 ==> log prob of sentence so far: -11.177011336327357

3GRAM: sti
ENGLISH: P(i|st) = -0.9936760834460768 ==> log prob of sentence so far: -11.822266010782078
FRENCH: P(i|st) = -1.002678083400447 ==> log prob of sentence so far: -12.179689419727804

3GRAM: tit
ENGLISH: P(t|ti) = -1.1078347745787511 ==> log prob of sentence so far: -12.93010078536083
FRENCH: P(t|ti) = -1.2594911681500698 ==> log prob of sentence so far: -13.439180587877875

3GRAM: itu
ENGLISH: P(u|it) = -1.5918100349461568 ==> log prob of sentence so far: -14.521910820306987
FRENCH: P(u|it) = -1.2244578543867268 ==> log prob of sentence so far: -14.663638442264602

3GRAM: tut
ENGLISH: P(t|tu) = -1.9152653034235838 ==> log prob of sentence so far: -16.43717612373057
FRENCH: P(t|tu) = -2.250420002308894 ==> log prob of sentence so far: -16.914058444573495

3GRAM: ute
ENGLISH: P(e|ut) = -1.251330641704196 ==> log prob of sentence so far: -17.688506765434767
FRENCH: P(e|ut) = -0.5472438278785562 ==> log prob of sentence so far: -17.461302272452052

3GRAM: tel
ENGLISH: P(l|te) = -1.321842063676577 ==> log prob of sentence so far: -19.010348829111344
FRENCH: P(l|te) = -1.3088633530177163 ==> log prob of sentence so far: -18.77016562546977

3GRAM: eli
ENGLISH: P(i|el) = -0.8560453108083541 ==> log prob of sentence so far: -19.8663941399197
FRENCH: P(i|el) = -1.2614776698913865 ==> log prob of sentence so far: -20.031643295361157

3GRAM: lim
ENGLISH: P(m|li) = -1.5911079346547992 ==> log prob of sentence so far: -21.4575020745745
FRENCH: P(m|li) = -1.2885998438254571 ==> log prob of sentence so far: -21.320243139186616

3GRAM: imi
ENGLISH: P(i|im) = -1.1340003511073458 ==> log prob of sentence so far: -22.591502425681846
FRENCH: P(i|im) = -1.3690673552777664 ==> log prob of sentence so far: -22.689310494464383

3GRAM: mit
ENGLISH: P(t|mi) = -0.9245437772283411 ==> log prob of sentence so far: -23.516046202910186
FRENCH: P(t|mi) = -1.3437742207570815 ==> log prob of sentence so far: -24.033084715221463

3GRAM: ite
ENGLISH: P(e|it) = -1.0755552368997654 ==> log prob of sentence so far: -24.591601439809953
FRENCH: P(e|it) = -0.726878948523225 ==> log prob of sentence so far: -24.75996366374469

3GRAM: ted
ENGLISH: P(d|te) = -0.6942062145399549 ==> log prob of sentence so far: -25.285807654349906
FRENCH: P(d|te) = -1.2183780656179406 ==> log prob of sentence so far: -25.97834172936263

3GRAM: edp
ENGLISH: P(p|ed) = -1.8825133771586857 ==> log prob of sentence so far: -27.16832103150859
FRENCH: P(p|ed) = -2.859526801309455 ==> log prob of sentence so far: -28.837868530672083

3GRAM: dpa
ENGLISH: P(a|dp) = -0.7672250267610277 ==> log prob of sentence so far: -27.93554605826962
FRENCH: P(a|dp) = -0.48467439261010836 ==> log prob of sentence so far: -29.322542923282192

3GRAM: pal
ENGLISH: P(l|pa) = -1.1972551198780266 ==> log prob of sentence so far: -29.132801178147645
FRENCH: P(l|pa) = -1.745323907050522 ==> log prob of sentence so far: -31.067866830332715

3GRAM: ale
ENGLISH: P(e|al) = -0.6262975319759201 ==> log prob of sentence so far: -29.759098710123563
FRENCH: P(e|al) = -0.5897802699693387 ==> log prob of sentence so far: -31.657647100302054

3GRAM: let
ENGLISH: P(t|le) = -1.030026933967869 ==> log prob of sentence so far: -30.78912564409143
FRENCH: P(t|le) = -1.3254771322560213 ==> log prob of sentence so far: -32.983124232558076

3GRAM: ett
ENGLISH: P(t|et) = -1.2503204049040535 ==> log prob of sentence so far: -32.03944604899549
FRENCH: P(t|et) = -0.9776677697399989 ==> log prob of sentence so far: -33.96079200229808

3GRAM: tte
ENGLISH: P(e|tt) = -0.8915752914384356 ==> log prob of sentence so far: -32.93102134043392
FRENCH: P(e|tt) = -0.22584029542978878 ==> log prob of sentence so far: -34.18663229772787

3GRAM: teo
ENGLISH: P(o|te) = -2.0076142587802663 ==> log prob of sentence so far: -34.93863559921419
FRENCH: P(o|te) = -2.087463951068868 ==> log prob of sentence so far: -36.27409624879674

3GRAM: eof
ENGLISH: P(f|eo) = -0.3439114680548523 ==> log prob of sentence so far: -35.28254706726904
FRENCH: P(f|eo) = -2.023813442896087 ==> log prob of sentence so far: -38.29790969169282

3GRAM: ofc
ENGLISH: P(c|of) = -1.6956045494523704 ==> log prob of sentence so far: -36.97815161672141
FRENCH: P(c|of) = -2.94546858513182 ==> log prob of sentence so far: -41.24337827682464

3GRAM: fco
ENGLISH: P(o|fc) = -0.36457941869780247 ==> log prob of sentence so far: -37.34273103541921
FRENCH: P(o|fc) = -1.6989700043360187 ==> log prob of sentence so far: -42.94234828116066

3GRAM: col
ENGLISH: P(l|co) = -1.3516895453216524 ==> log prob of sentence so far: -38.69442058074087
FRENCH: P(l|co) = -1.4519315176284482 ==> log prob of sentence so far: -44.39427979878911

3GRAM: olo
ENGLISH: P(o|ol) = -1.0665921187396044 ==> log prob of sentence so far: -39.761012699480474
FRENCH: P(o|ol) = -0.9012946372590254 ==> log prob of sentence so far: -45.295574436048135

3GRAM: lou
ENGLISH: P(u|lo) = -1.2265641826303442 ==> log prob of sentence so far: -40.987576882110815
FRENCH: P(u|lo) = -1.1620038964604027 ==> log prob of sentence so far: -46.45757833250854

3GRAM: our
ENGLISH: P(r|ou) = -0.9331113741598649 ==> log prob of sentence so far: -41.92068825627068
FRENCH: P(r|ou) = -0.6948640525655961 ==> log prob of sentence so far: -47.15244238507414

3GRAM: urs
ENGLISH: P(s|ur) = -0.9273444561346499 ==> log prob of sentence so far: -42.84803271240533
FRENCH: P(s|ur) = -0.824853201951343 ==> log prob of sentence so far: -47.97729558702548

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: tinc
ENGLISH: P(c|tin) = -1.3749060793507049 ==> log prob of sentence so far: -1.3749060793507049
FRENCH: P(c|tin) = -0.8054506799722003 ==> log prob of sentence so far: -0.8054506799722003

4GRAM: inct
ENGLISH: P(t|inc) = -0.9953633600669848 ==> log prob of sentence so far: -2.3702694394176897
FRENCH: P(t|inc) = -1.1393065161378098 ==> log prob of sentence so far: -1.94475719611001

4GRAM: nctu
ENGLISH: P(u|nct) = -0.6731233471534077 ==> log prob of sentence so far: -3.0433927865710975
FRENCH: P(u|nct) = -1.3502480183341627 ==> log prob of sentence so far: -3.295005214444173

4GRAM: ctur
ENGLISH: P(r|ctu) = -0.2763825605854131 ==> log prob of sentence so far: -3.3197753471565106
FRENCH: P(r|ctu) = -0.4727121358146074 ==> log prob of sentence so far: -3.7677173502587804

4GRAM: ture
ENGLISH: P(e|tur) = -0.3435585160944578 ==> log prob of sentence so far: -3.663333863250968
FRENCH: P(e|tur) = -0.10905995167022249 ==> log prob of sentence so far: -3.876777301929003

4GRAM: ures
ENGLISH: P(s|ure) = -0.636719293827219 ==> log prob of sentence so far: -4.300053157078187
FRENCH: P(s|ure) = -0.6005215749764382 ==> log prob of sentence so far: -4.477298876905441

4GRAM: resc
ENGLISH: P(c|res) = -1.7154367493184222 ==> log prob of sentence so far: -6.015489906396609
FRENCH: P(c|res) = -1.3257703732422117 ==> log prob of sentence so far: -5.803069250147653

4GRAM: esco
ENGLISH: P(o|esc) = -0.9830142520093493 ==> log prob of sentence so far: -6.998504158405958
FRENCH: P(o|esc) = -0.3856014906457744 ==> log prob of sentence so far: -6.188670740793427

4GRAM: scon
ENGLISH: P(n|sco) = -0.6090729727187977 ==> log prob of sentence so far: -7.6075771311247555
FRENCH: P(n|sco) = -0.5023570781357358 ==> log prob of sentence so far: -6.691027818929163

4GRAM: cons
ENGLISH: P(s|con) = -0.5627578637753148 ==> log prob of sentence so far: -8.17033499490007
FRENCH: P(s|con) = -0.4025503332254633 ==> log prob of sentence so far: -7.093578152154626

4GRAM: onst
ENGLISH: P(t|ons) = -0.6916882496212494 ==> log prob of sentence so far: -8.86202324452132
FRENCH: P(t|ons) = -1.2076749368729678 ==> log prob of sentence so far: -8.301253089027593

4GRAM: nsti
ENGLISH: P(i|nst) = -1.1666934847153012 ==> log prob of sentence so far: -10.02871672923662
FRENCH: P(i|nst) = -1.2920287515491908 ==> log prob of sentence so far: -9.593281840576784

4GRAM: stit
ENGLISH: P(t|sti) = -1.1630484024897056 ==> log prob of sentence so far: -11.191765131726326
FRENCH: P(t|sti) = -1.525353364408193 ==> log prob of sentence so far: -11.118635204984978

4GRAM: titu
ENGLISH: P(u|tit) = -0.8209313286587037 ==> log prob of sentence so far: -12.01269646038503
FRENCH: P(u|tit) = -0.5035961502512842 ==> log prob of sentence so far: -11.622231355236263

4GRAM: itut
ENGLISH: P(t|itu) = -1.1400820751788994 ==> log prob of sentence so far: -13.152778535563929
FRENCH: P(t|itu) = -2.1694224987436668 ==> log prob of sentence so far: -13.79165385397993

4GRAM: tute
ENGLISH: P(e|tut) = -0.6503646709025174 ==> log prob of sentence so far: -13.803143206466446
FRENCH: P(e|tut) = -1.146128035678238 ==> log prob of sentence so far: -14.937781889658169

4GRAM: utel
ENGLISH: P(l|ute) = -1.129873954283812 ==> log prob of sentence so far: -14.933017160750259
FRENCH: P(l|ute) = -1.289471834969262 ==> log prob of sentence so far: -16.22725372462743

4GRAM: teli
ENGLISH: P(i|tel) = -1.6376074404447676 ==> log prob of sentence so far: -16.570624601195025
FRENCH: P(i|tel) = -1.2704810749868392 ==> log prob of sentence so far: -17.49773479961427

4GRAM: elim
ENGLISH: P(m|eli) = -1.5443565162850952 ==> log prob of sentence so far: -18.11498111748012
FRENCH: P(m|eli) = -1.3739622924346044 ==> log prob of sentence so far: -18.871697092048876

4GRAM: limi
ENGLISH: P(i|lim) = -0.7426789322214247 ==> log prob of sentence so far: -18.857660049701543
FRENCH: P(i|lim) = -0.856679912564072 ==> log prob of sentence so far: -19.72837700461295

4GRAM: imit
ENGLISH: P(t|imi) = -0.8875778208136961 ==> log prob of sentence so far: -19.74523787051524
FRENCH: P(t|imi) = -0.42021640338318983 ==> log prob of sentence so far: -20.148593407996138

4GRAM: mite
ENGLISH: P(e|mit) = -1.2922560713564761 ==> log prob of sentence so far: -21.037493941871716
FRENCH: P(e|mit) = -0.2798406965940431 ==> log prob of sentence so far: -20.42843410459018

4GRAM: ited
ENGLISH: P(d|ite) = -0.9655502234787394 ==> log prob of sentence so far: -22.003044165350456
FRENCH: P(d|ite) = -0.9887617119829383 ==> log prob of sentence so far: -21.41719581657312

4GRAM: tedp
ENGLISH: P(p|ted) = -1.6333130451353075 ==> log prob of sentence so far: -23.636357210485762
FRENCH: P(p|ted) = -3.084933574936716 ==> log prob of sentence so far: -24.502129391509836

4GRAM: edpa
ENGLISH: P(a|edp) = -0.8136895757626327 ==> log prob of sentence so far: -24.450046786248397
FRENCH: P(a|edp) = -1.146128035678238 ==> log prob of sentence so far: -25.648257427188074

4GRAM: dpal
ENGLISH: P(l|dpa) = -0.9907593432650873 ==> log prob of sentence so far: -25.440806129513483
FRENCH: P(l|dpa) = -1.9138138523837167 ==> log prob of sentence so far: -27.56207127957179

4GRAM: pale
ENGLISH: P(e|pal) = -0.7962494724764398 ==> log prob of sentence so far: -26.237055601989923
FRENCH: P(e|pal) = -0.3656728303850301 ==> log prob of sentence so far: -27.92774410995682

4GRAM: alet
ENGLISH: P(t|ale) = -1.2073696625680543 ==> log prob of sentence so far: -27.444425264557978
FRENCH: P(t|ale) = -1.2009680130160765 ==> log prob of sentence so far: -29.128712122972896

4GRAM: lett
ENGLISH: P(t|let) = -1.1262584951118004 ==> log prob of sentence so far: -28.570683759669777
FRENCH: P(t|let) = -1.2491209223401984 ==> log prob of sentence so far: -30.377833045313096

4GRAM: ette
ENGLISH: P(e|ett) = -0.5841689016289925 ==> log prob of sentence so far: -29.15485266129877
FRENCH: P(e|ett) = -0.09341330117980409 ==> log prob of sentence so far: -30.4712463464929

4GRAM: tteo
ENGLISH: P(o|tte) = -3.0962145853464054 ==> log prob of sentence so far: -32.25106724664517
FRENCH: P(o|tte) = -1.6914108905601948 ==> log prob of sentence so far: -32.1626572370531

4GRAM: teof
ENGLISH: P(f|teo) = -0.25780484161870115 ==> log prob of sentence so far: -32.50887208826387
FRENCH: P(f|teo) = -1.5705429398818975 ==> log prob of sentence so far: -33.73320017693499

4GRAM: eofc
ENGLISH: P(c|eof) = -1.5194397438565015 ==> log prob of sentence so far: -34.02831183212037
FRENCH: P(c|eof) = -1.5563025007672873 ==> log prob of sentence so far: -35.28950267770228

4GRAM: ofco
ENGLISH: P(o|ofc) = -0.3675100528671869 ==> log prob of sentence so far: -34.39582188498756
FRENCH: P(o|ofc) = -1.4313637641589874 ==> log prob of sentence so far: -36.72086644186127

4GRAM: fcol
ENGLISH: P(l|fco) = -1.1648102486459921 ==> log prob of sentence so far: -35.560632133633554
FRENCH: P(l|fco) = -1.4313637641589874 ==> log prob of sentence so far: -38.152230206020256

4GRAM: colo
ENGLISH: P(o|col) = -0.4856971648977127 ==> log prob of sentence so far: -36.04632929853127
FRENCH: P(o|col) = -0.6234815955555674 ==> log prob of sentence so far: -38.775711801575824

4GRAM: olou
ENGLISH: P(u|olo) = -0.7911162275480113 ==> log prob of sentence so far: -36.83744552607928
FRENCH: P(u|olo) = -1.8169038393756602 ==> log prob of sentence so far: -40.592615640951486

4GRAM: lour
ENGLISH: P(r|lou) = -0.6725376498537088 ==> log prob of sentence so far: -37.50998317593299
FRENCH: P(r|lou) = -0.7645784431943697 ==> log prob of sentence so far: -41.35719408414585

4GRAM: ours
ENGLISH: P(s|our) = -0.6270724515353182 ==> log prob of sentence so far: -38.13705562746831
FRENCH: P(s|our) = -0.7580268341077214 ==> log prob of sentence so far: -42.11522091825358

According to the 4gram model, the sentence is in English
----------------
