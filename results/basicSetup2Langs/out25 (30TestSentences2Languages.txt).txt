The magret de canard is yucky.

1GRAM MODEL:

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -1.0342236377842724
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -1.1659667480967535

1GRAM: h
ENGLISH: P(h) = -1.180274162400034 ==> log prob of sentence so far: -2.214497800184306
FRENCH: P(h) = -2.1056297044472685 ==> log prob of sentence so far: -3.271596452544022

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -3.1245690182442134
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -4.038562267066478

1GRAM: m
ENGLISH: P(m) = -1.6111104829831362 ==> log prob of sentence so far: -4.73567950122735
FRENCH: P(m) = -1.5129486053385504 ==> log prob of sentence so far: -5.551510872405029

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -5.8224681822698034
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -6.627863967510537

1GRAM: g
ENGLISH: P(g) = -1.660068197743731 ==> log prob of sentence so far: -7.482536380013535
FRENCH: P(g) = -2.0284572462247623 ==> log prob of sentence so far: -8.6563212137353

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -8.743809281325632
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -9.840349852805756

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -9.653880499385538
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -10.607315667328212

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -10.68810413716981
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -11.773282415424966

1GRAM: d
ENGLISH: P(d) = -1.396179917541683 ==> log prob of sentence so far: -12.084284054711494
FRENCH: P(d) = -1.4202616779892512 ==> log prob of sentence so far: -13.193544093414216

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -12.9943552727714
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -13.960509907936672

1GRAM: c
ENGLISH: P(c) = -1.626587631644617 ==> log prob of sentence so far: -14.620942904416017
FRENCH: P(c) = -1.492223661080204 ==> log prob of sentence so far: -15.452733569016875

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -15.707731585458472
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -16.529086664122385

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -16.8693285228611
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -17.65172414073689

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -17.956117203903553
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -18.7280772358424

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -19.21739010521565
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -19.912105874912857

1GRAM: d
ENGLISH: P(d) = -1.396179917541683 ==> log prob of sentence so far: -20.613570022757333
FRENCH: P(d) = -1.4202616779892512 ==> log prob of sentence so far: -21.33236755290211

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -21.776095503031243
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -22.46757399557095

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -22.947236515759844
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -23.532021710460608

1GRAM: y
ENGLISH: P(y) = -1.7505965755952897 ==> log prob of sentence so far: -24.697833091355132
FRENCH: P(y) = -2.672224427638286 ==> log prob of sentence so far: -26.204246138098895

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -26.250321260313047
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -27.410381199274518

1GRAM: c
ENGLISH: P(c) = -1.626587631644617 ==> log prob of sentence so far: -27.876908891957665
FRENCH: P(c) = -1.492223661080204 ==> log prob of sentence so far: -28.90260486035472

1GRAM: k
ENGLISH: P(k) = -2.072697153608919 ==> log prob of sentence so far: -29.949606045566583
FRENCH: P(k) = -3.5365941540489745 ==> log prob of sentence so far: -32.4391990144037

1GRAM: y
ENGLISH: P(y) = -1.7505965755952897 ==> log prob of sentence so far: -31.70020262116187
FRENCH: P(y) = -2.672224427638286 ==> log prob of sentence so far: -35.11142344204198

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: th
ENGLISH: P(h|t) = -0.4192831299636363 ==> log prob of sentence so far: -0.4192831299636363
FRENCH: P(h|t) = -2.127206501261668 ==> log prob of sentence so far: -2.127206501261668

2GRAM: he
ENGLISH: P(e|h) = -0.37002759861537876 ==> log prob of sentence so far: -0.7893107285790151
FRENCH: P(e|h) = -0.45804503705298444 ==> log prob of sentence so far: -2.5852515383146524

2GRAM: em
ENGLISH: P(m|e) = -1.436032682775707 ==> log prob of sentence so far: -2.2253434113547224
FRENCH: P(m|e) = -1.2279205528259898 ==> log prob of sentence so far: -3.8131720911406424

2GRAM: ma
ENGLISH: P(a|m) = -0.7038280092488644 ==> log prob of sentence so far: -2.929171420603587
FRENCH: P(a|m) = -0.7372357130058481 ==> log prob of sentence so far: -4.550407804146491

2GRAM: ag
ENGLISH: P(g|a) = -1.6541037069433895 ==> log prob of sentence so far: -4.583275127546976
FRENCH: P(g|a) = -1.604158036383654 ==> log prob of sentence so far: -6.154565840530145

2GRAM: gr
ENGLISH: P(r|g) = -1.1796403264736368 ==> log prob of sentence so far: -5.762915454020613
FRENCH: P(r|g) = -0.9217215491353413 ==> log prob of sentence so far: -7.076287389665486

2GRAM: re
ENGLISH: P(e|r) = -0.6282077999008819 ==> log prob of sentence so far: -6.391123253921495
FRENCH: P(e|r) = -0.49097082634029093 ==> log prob of sentence so far: -7.5672582160057775

2GRAM: et
ENGLISH: P(t|e) = -1.1805749996736075 ==> log prob of sentence so far: -7.571698253595102
FRENCH: P(t|e) = -1.081730001747917 ==> log prob of sentence so far: -8.648988217753695

2GRAM: td
ENGLISH: P(d|t) = -2.226418270912645 ==> log prob of sentence so far: -9.798116524507748
FRENCH: P(d|t) = -1.237245793322422 ==> log prob of sentence so far: -9.886234011076118

2GRAM: de
ENGLISH: P(e|d) = -0.8320988657918726 ==> log prob of sentence so far: -10.63021539029962
FRENCH: P(e|d) = -0.290519189469436 ==> log prob of sentence so far: -10.176753200545553

2GRAM: ec
ENGLISH: P(c|e) = -1.473324282308782 ==> log prob of sentence so far: -12.103539672608402
FRENCH: P(c|e) = -1.2462449696200582 ==> log prob of sentence so far: -11.422998170165611

2GRAM: ca
ENGLISH: P(a|c) = -0.84612780149222 ==> log prob of sentence so far: -12.949667474100622
FRENCH: P(a|c) = -0.9172798051764028 ==> log prob of sentence so far: -12.340277975342014

2GRAM: an
ENGLISH: P(n|a) = -0.704528505817304 ==> log prob of sentence so far: -13.654195979917926
FRENCH: P(n|a) = -0.8168159428818009 ==> log prob of sentence so far: -13.157093918223815

2GRAM: na
ENGLISH: P(a|n) = -1.24610849854619 ==> log prob of sentence so far: -14.900304478464117
FRENCH: P(a|n) = -1.1335472623560008 ==> log prob of sentence so far: -14.290641180579815

2GRAM: ar
ENGLISH: P(r|a) = -0.9854656313970852 ==> log prob of sentence so far: -15.885770109861202
FRENCH: P(r|a) = -1.0331920938948058 ==> log prob of sentence so far: -15.323833274474621

2GRAM: rd
ENGLISH: P(d|r) = -1.385429591354749 ==> log prob of sentence so far: -17.271199701215952
FRENCH: P(d|r) = -1.3136021053854252 ==> log prob of sentence so far: -16.637435379860047

2GRAM: di
ENGLISH: P(i|d) = -0.9226651782817907 ==> log prob of sentence so far: -18.193864879497742
FRENCH: P(i|d) = -0.9305015773390085 ==> log prob of sentence so far: -17.567936957199056

2GRAM: is
ENGLISH: P(s|i) = -0.8699344462536623 ==> log prob of sentence so far: -19.063799325751404
FRENCH: P(s|i) = -0.8503045030811127 ==> log prob of sentence so far: -18.418241460280168

2GRAM: sy
ENGLISH: P(y|s) = -2.1758746690055997 ==> log prob of sentence so far: -21.239673994757005
FRENCH: P(y|s) = -2.4661277125575425 ==> log prob of sentence so far: -20.88436917283771

2GRAM: yu
ENGLISH: P(u|y) = -2.094080455833719 ==> log prob of sentence so far: -23.333754450590725
FRENCH: P(u|y) = -3.4730488050885375 ==> log prob of sentence so far: -24.35741797792625

2GRAM: uc
ENGLISH: P(c|u) = -1.2525392024421091 ==> log prob of sentence so far: -24.586293653032833
FRENCH: P(c|u) = -1.6406795484953514 ==> log prob of sentence so far: -25.998097526421603

2GRAM: ck
ENGLISH: P(k|c) = -1.0299426255694808 ==> log prob of sentence so far: -25.616236278602315
FRENCH: P(k|c) = -3.01596376766203 ==> log prob of sentence so far: -29.014061294083632

2GRAM: ky
ENGLISH: P(y|k) = -1.613700353808245 ==> log prob of sentence so far: -27.22993663241056
FRENCH: P(y|k) = -2.6232492903979003 ==> log prob of sentence so far: -31.637310584481533

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: the
ENGLISH: P(e|th) = -0.2045358758824928 ==> log prob of sentence so far: -0.2045358758824928
FRENCH: P(e|th) = -0.3852654323756641 ==> log prob of sentence so far: -0.3852654323756641

3GRAM: hem
ENGLISH: P(m|he) = -1.2239521265529143 ==> log prob of sentence so far: -1.4284880024354072
FRENCH: P(m|he) = -1.328828629467106 ==> log prob of sentence so far: -1.71409406184277

3GRAM: ema
ENGLISH: P(a|em) = -0.6313444526783568 ==> log prob of sentence so far: -2.059832455113764
FRENCH: P(a|em) = -0.7808434903498844 ==> log prob of sentence so far: -2.4949375521926545

3GRAM: mag
ENGLISH: P(g|ma) = -1.4789330029078531 ==> log prob of sentence so far: -3.5387654580216172
FRENCH: P(g|ma) = -1.5981848077157075 ==> log prob of sentence so far: -4.093122359908362

3GRAM: agr
ENGLISH: P(r|ag) = -1.1680082365210405 ==> log prob of sentence so far: -4.706773694542658
FRENCH: P(r|ag) = -1.3409358786788512 ==> log prob of sentence so far: -5.434058238587213

3GRAM: gre
ENGLISH: P(e|gr) = -0.3323982195188285 ==> log prob of sentence so far: -5.039171914061487
FRENCH: P(e|gr) = -0.5528027529385882 ==> log prob of sentence so far: -5.986860991525802

3GRAM: ret
ENGLISH: P(t|re) = -1.090974823514154 ==> log prob of sentence so far: -6.13014673757564
FRENCH: P(t|re) = -1.224945246095241 ==> log prob of sentence so far: -7.211806237621043

3GRAM: etd
ENGLISH: P(d|et) = -2.3716248568572595 ==> log prob of sentence so far: -8.501771594432899
FRENCH: P(d|et) = -1.2063593115417326 ==> log prob of sentence so far: -8.418165549162776

3GRAM: tde
ENGLISH: P(e|td) = -0.6895772799021149 ==> log prob of sentence so far: -9.191348874335015
FRENCH: P(e|td) = -0.2063184627735822 ==> log prob of sentence so far: -8.624484011936358

3GRAM: dec
ENGLISH: P(c|de) = -1.195181894868142 ==> log prob of sentence so far: -10.386530769203157
FRENCH: P(c|de) = -1.0410199071284416 ==> log prob of sentence so far: -9.6655039190648

3GRAM: eca
ENGLISH: P(a|ec) = -0.6949703771030915 ==> log prob of sentence so far: -11.081501146306248
FRENCH: P(a|ec) = -0.7634866155506699 ==> log prob of sentence so far: -10.42899053461547

3GRAM: can
ENGLISH: P(n|ca) = -0.7238252554495043 ==> log prob of sentence so far: -11.805326401755753
FRENCH: P(n|ca) = -0.7713093200070669 ==> log prob of sentence so far: -11.200299854622537

3GRAM: ana
ENGLISH: P(a|an) = -1.5179238364296928 ==> log prob of sentence so far: -13.323250238185446
FRENCH: P(a|an) = -1.3024080844782078 ==> log prob of sentence so far: -12.502707939100745

3GRAM: nar
ENGLISH: P(r|na) = -1.2437318822714265 ==> log prob of sentence so far: -14.566982120456872
FRENCH: P(r|na) = -1.4548165349159254 ==> log prob of sentence so far: -13.95752447401667

3GRAM: ard
ENGLISH: P(d|ar) = -0.8576270332224175 ==> log prob of sentence so far: -15.42460915367929
FRENCH: P(d|ar) = -0.9395474544746565 ==> log prob of sentence so far: -14.897071928491327

3GRAM: rdi
ENGLISH: P(i|rd) = -0.9511423018189108 ==> log prob of sentence so far: -16.3757514554982
FRENCH: P(i|rd) = -1.101117883665835 ==> log prob of sentence so far: -15.998189812157161

3GRAM: dis
ENGLISH: P(s|di) = -0.8766904178817929 ==> log prob of sentence so far: -17.252441873379993
FRENCH: P(s|di) = -0.6560707693114031 ==> log prob of sentence so far: -16.654260581468563

3GRAM: isy
ENGLISH: P(y|is) = -2.4653828514484184 ==> log prob of sentence so far: -19.71782472482841
FRENCH: P(y|is) = -3.1987792341868135 ==> log prob of sentence so far: -19.853039815655375

3GRAM: syu
ENGLISH: P(u|sy) = -2.4461227639106142 ==> log prob of sentence so far: -22.163947488739026
FRENCH: P(u|sy) = -2.6314437690131722 ==> log prob of sentence so far: -22.484483584668546

3GRAM: yuc
ENGLISH: P(c|yu) = -2.4471580313422194 ==> log prob of sentence so far: -24.611105520081246
FRENCH: P(c|yu) = -1.4313637641589874 ==> log prob of sentence so far: -23.915847348827533

3GRAM: uck
ENGLISH: P(k|uc) = -0.4758280692026605 ==> log prob of sentence so far: -25.086933589283905
FRENCH: P(k|uc) = -3.3001605369513523 ==> log prob of sentence so far: -27.216007885778886

3GRAM: cky
ENGLISH: P(y|ck) = -1.8718812113153833 ==> log prob of sentence so far: -26.95881480059929
FRENCH: P(y|ck) = -1.8325089127062364 ==> log prob of sentence so far: -29.04851679848512

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: them
ENGLISH: P(m|the) = -1.1387346455790048 ==> log prob of sentence so far: -1.1387346455790048
FRENCH: P(m|the) = -1.4664631865376059 ==> log prob of sentence so far: -1.4664631865376059

4GRAM: hema
ENGLISH: P(a|hem) = -0.542954753605018 ==> log prob of sentence so far: -1.6816893991840227
FRENCH: P(a|hem) = -0.7868374295687363 ==> log prob of sentence so far: -2.2533006161063422

4GRAM: emag
ENGLISH: P(g|ema) = -1.5487801464380893 ==> log prob of sentence so far: -3.230469545622112
FRENCH: P(g|ema) = -1.6771598409975053 ==> log prob of sentence so far: -3.9304604571038473

4GRAM: magr
ENGLISH: P(r|mag) = -1.8169038393756602 ==> log prob of sentence so far: -5.047373384997773
FRENCH: P(r|mag) = -1.1159040530723647 ==> log prob of sentence so far: -5.046364510176212

4GRAM: agre
ENGLISH: P(e|agr) = -0.1974894037569117 ==> log prob of sentence so far: -5.244862788754684
FRENCH: P(e|agr) = -0.5454445731790789 ==> log prob of sentence so far: -5.591809083355291

4GRAM: gret
ENGLISH: P(t|gre) = -2.6300887149282057 ==> log prob of sentence so far: -7.87495150368289
FRENCH: P(t|gre) = -0.8133836268370502 ==> log prob of sentence so far: -6.405192710192342

4GRAM: retd
ENGLISH: P(d|ret) = -2.803912112528065 ==> log prob of sentence so far: -10.678863616210956
FRENCH: P(d|ret) = -1.2487567126413042 ==> log prob of sentence so far: -7.653949422833646

4GRAM: etde
ENGLISH: P(e|etd) = -1.24551266781415 ==> log prob of sentence so far: -11.924376284025106
FRENCH: P(e|etd) = -0.1855192793293544 ==> log prob of sentence so far: -7.839468702163001

4GRAM: tdec
ENGLISH: P(c|tde) = -1.101457640758777 ==> log prob of sentence so far: -13.025833924783884
FRENCH: P(c|tde) = -1.1107802321193272 ==> log prob of sentence so far: -8.950248934282328

4GRAM: deca
ENGLISH: P(a|dec) = -1.3265841001363694 ==> log prob of sentence so far: -14.352418024920253
FRENCH: P(a|dec) = -1.2740167529395332 ==> log prob of sentence so far: -10.22426568722186

4GRAM: ecan
ENGLISH: P(n|eca) = -0.929832999620032 ==> log prob of sentence so far: -15.282251024540285
FRENCH: P(n|eca) = -0.6420323128557139 ==> log prob of sentence so far: -10.866298000077574

4GRAM: cana
ENGLISH: P(a|can) = -1.0748598722649823 ==> log prob of sentence so far: -16.357110896805267
FRENCH: P(a|can) = -0.2447740715784702 ==> log prob of sentence so far: -11.111072071656045

4GRAM: anar
ENGLISH: P(r|ana) = -1.166331421766525 ==> log prob of sentence so far: -17.523442318571792
FRENCH: P(r|ana) = -1.5591881890047754 ==> log prob of sentence so far: -12.67026026066082

4GRAM: nard
ENGLISH: P(d|nar) = -1.9405164849325671 ==> log prob of sentence so far: -19.463958803504358
FRENCH: P(d|nar) = -0.7607771543142209 ==> log prob of sentence so far: -13.431037414975041

4GRAM: ardi
ENGLISH: P(i|ard) = -1.2594484457574422 ==> log prob of sentence so far: -20.7234072492618
FRENCH: P(i|ard) = -1.1433306380391863 ==> log prob of sentence so far: -14.574368053014227

4GRAM: rdis
ENGLISH: P(s|rdi) = -0.9217320970480017 ==> log prob of sentence so far: -21.6451393463098
FRENCH: P(s|rdi) = -0.7374062826935842 ==> log prob of sentence so far: -15.311774335707812

4GRAM: disy
ENGLISH: P(y|dis) = -3.0637085593914173 ==> log prob of sentence so far: -24.70884790570122
FRENCH: P(y|dis) = -3.1386184338994925 ==> log prob of sentence so far: -18.450392769607305

4GRAM: isyu
ENGLISH: P(u|isy) = -1.9138138523837167 ==> log prob of sentence so far: -26.622661758084938
FRENCH: P(u|isy) = -1.5314789170422551 ==> log prob of sentence so far: -19.98187168664956

4GRAM: syuc
ENGLISH: P(c|syu) = -1.4471580313422192 ==> log prob of sentence so far: -28.069819789427157
FRENCH: P(c|syu) = -1.4313637641589874 ==> log prob of sentence so far: -21.413235450808546

4GRAM: yuck
ENGLISH: P(k|yuc) = -1.4313637641589874 ==> log prob of sentence so far: -29.501183553586145
FRENCH: P(k|yuc) = -1.4313637641589874 ==> log prob of sentence so far: -22.844599214967534

4GRAM: ucky
ENGLISH: P(y|uck) = -1.6277219816490989 ==> log prob of sentence so far: -31.128905535235244
FRENCH: P(y|uck) = -1.4313637641589874 ==> log prob of sentence so far: -24.275962979126522

According to the 4gram model, the sentence is in French
----------------
