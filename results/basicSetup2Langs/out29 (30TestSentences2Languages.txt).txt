It was magnifique.

1GRAM MODEL:

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -1.162525480273911
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -1.1352064426688435

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -2.1967491180581833
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -2.301173190765597

1GRAM: w
ENGLISH: P(w) = -1.6313539967808455 ==> log prob of sentence so far: -3.8281031148390285
FRENCH: P(w) = -3.9191827290425008 ==> log prob of sentence so far: -6.220355919808098

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -4.914891795881482
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -7.296709014913606

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -6.086032808610083
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -8.361156729803263

1GRAM: m
ENGLISH: P(m) = -1.6111104829831362 ==> log prob of sentence so far: -7.697143291593219
FRENCH: P(m) = -1.5129486053385504 ==> log prob of sentence so far: -9.874105335141813

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -8.783931972635674
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -10.950458430247322

1GRAM: g
ENGLISH: P(g) = -1.660068197743731 ==> log prob of sentence so far: -10.444000170379406
FRENCH: P(g) = -2.0284572462247623 ==> log prob of sentence so far: -12.978915676472084

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -11.605597107782035
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -14.10155315308659

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -12.768122588055945
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -15.236759595755434

1GRAM: f
ENGLISH: P(f) = -1.660275542753472 ==> log prob of sentence so far: -14.428398130809416
FRENCH: P(f) = -1.9917263037969526 ==> log prob of sentence so far: -17.228485899552386

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -15.590923611083326
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -18.363692342221228

1GRAM: q
ENGLISH: P(q) = -2.7879988169444423 ==> log prob of sentence so far: -18.378922428027767
FRENCH: P(q) = -1.9394381295606844 ==> log prob of sentence so far: -20.303130471781913

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -19.931410596985682
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -21.509265532957535

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -20.84148181504559
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -22.276231347479992

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: it
ENGLISH: P(t|i) = -0.9082997757934181 ==> log prob of sentence so far: -0.9082997757934181
FRENCH: P(t|i) = -0.7742072823534025 ==> log prob of sentence so far: -0.7742072823534025

2GRAM: tw
ENGLISH: P(w|t) = -1.5828313249921209 ==> log prob of sentence so far: -2.4911311007855392
FRENCH: P(w|t) = -4.495803264508664 ==> log prob of sentence so far: -5.270010546862067

2GRAM: wa
ENGLISH: P(a|w) = -0.7200687287449945 ==> log prob of sentence so far: -3.2111998295305337
FRENCH: P(a|w) = -0.4444521379662943 ==> log prob of sentence so far: -5.714462684828361

2GRAM: as
ENGLISH: P(s|a) = -0.9959554224973615 ==> log prob of sentence so far: -4.207155252027896
FRENCH: P(s|a) = -1.2507587495318242 ==> log prob of sentence so far: -6.965221434360186

2GRAM: sm
ENGLISH: P(m|s) = -1.6762937011306411 ==> log prob of sentence so far: -5.8834489531585366
FRENCH: P(m|s) = -1.4881206067155741 ==> log prob of sentence so far: -8.45334204107576

2GRAM: ma
ENGLISH: P(a|m) = -0.7038280092488644 ==> log prob of sentence so far: -6.587276962407401
FRENCH: P(a|m) = -0.7372357130058481 ==> log prob of sentence so far: -9.190577754081609

2GRAM: ag
ENGLISH: P(g|a) = -1.6541037069433895 ==> log prob of sentence so far: -8.241380669350791
FRENCH: P(g|a) = -1.604158036383654 ==> log prob of sentence so far: -10.794735790465262

2GRAM: gn
ENGLISH: P(n|g) = -1.601888129639173 ==> log prob of sentence so far: -9.843268798989964
FRENCH: P(n|g) = -0.9950844088776849 ==> log prob of sentence so far: -11.789820199342948

2GRAM: ni
ENGLISH: P(i|n) = -1.3332322954338982 ==> log prob of sentence so far: -11.176501094423863
FRENCH: P(i|n) = -1.4845526185364621 ==> log prob of sentence so far: -13.27437281787941

2GRAM: if
ENGLISH: P(f|i) = -1.676903383005364 ==> log prob of sentence so far: -12.853404477429226
FRENCH: P(f|i) = -1.8973227638823702 ==> log prob of sentence so far: -15.17169558176178

2GRAM: fi
ENGLISH: P(i|f) = -0.963475220436938 ==> log prob of sentence so far: -13.816879697866165
FRENCH: P(i|f) = -0.8768622638773614 ==> log prob of sentence so far: -16.04855784563914

2GRAM: iq
ENGLISH: P(q|i) = -3.179393289527697 ==> log prob of sentence so far: -16.996272987393862
FRENCH: P(q|i) = -1.7047366261391819 ==> log prob of sentence so far: -17.753294471778325

2GRAM: qu
ENGLISH: P(u|q) = -0.0037433435301242783 ==> log prob of sentence so far: -17.000016330923987
FRENCH: P(u|q) = -0.00832433543958775 ==> log prob of sentence so far: -17.761618807217914

2GRAM: ue
ENGLISH: P(e|u) = -1.3246679556129457 ==> log prob of sentence so far: -18.324684286536932
FRENCH: P(e|u) = -0.8812512722642089 ==> log prob of sentence so far: -18.642870079482122

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: itw
ENGLISH: P(w|it) = -1.2216832725780034 ==> log prob of sentence so far: -1.2216832725780034
FRENCH: P(w|it) = -4.228349020623638 ==> log prob of sentence so far: -4.228349020623638

3GRAM: twa
ENGLISH: P(a|tw) = -0.594309590315528 ==> log prob of sentence so far: -1.8159928628935313
FRENCH: P(a|tw) = -1.4471580313422192 ==> log prob of sentence so far: -5.675507051965857

3GRAM: was
ENGLISH: P(s|wa) = -0.38462580587693174 ==> log prob of sentence so far: -2.200618668770463
FRENCH: P(s|wa) = -1.1280298135854419 ==> log prob of sentence so far: -6.803536865551299

3GRAM: asm
ENGLISH: P(m|as) = -1.7263124578720959 ==> log prob of sentence so far: -3.926931126642559
FRENCH: P(m|as) = -1.5062289713765453 ==> log prob of sentence so far: -8.309765836927845

3GRAM: sma
ENGLISH: P(a|sm) = -0.4139538382876019 ==> log prob of sentence so far: -4.340884964930161
FRENCH: P(a|sm) = -0.4516411693961196 ==> log prob of sentence so far: -8.761407006323964

3GRAM: mag
ENGLISH: P(g|ma) = -1.4789330029078531 ==> log prob of sentence so far: -5.819817967838014
FRENCH: P(g|ma) = -1.5981848077157075 ==> log prob of sentence so far: -10.359591814039671

3GRAM: agn
ENGLISH: P(n|ag) = -1.4491709873533787 ==> log prob of sentence so far: -7.268988955191393
FRENCH: P(n|ag) = -0.6961494183983226 ==> log prob of sentence so far: -11.055741232437994

3GRAM: gni
ENGLISH: P(i|gn) = -0.5945647902681658 ==> log prob of sentence so far: -7.8635537454595585
FRENCH: P(i|gn) = -0.8599330699617938 ==> log prob of sentence so far: -11.915674302399788

3GRAM: nif
ENGLISH: P(f|ni) = -1.3574710738995535 ==> log prob of sentence so far: -9.221024819359112
FRENCH: P(f|ni) = -1.4671082582966088 ==> log prob of sentence so far: -13.382782560696397

3GRAM: ifi
ENGLISH: P(i|if) = -0.7068242245322479 ==> log prob of sentence so far: -9.92784904389136
FRENCH: P(i|if) = -0.48014770934985024 ==> log prob of sentence so far: -13.862930270046247

3GRAM: fiq
ENGLISH: P(q|fi) = -3.6469915374771227 ==> log prob of sentence so far: -13.574840581368482
FRENCH: P(q|fi) = -1.0215319377322105 ==> log prob of sentence so far: -14.884462207778459

3GRAM: iqu
ENGLISH: P(u|iq) = -0.10969877005156307 ==> log prob of sentence so far: -13.684539351420046
FRENCH: P(u|iq) = -0.005397714049644559 ==> log prob of sentence so far: -14.889859921828103

3GRAM: que
ENGLISH: P(e|qu) = -0.3018595937804336 ==> log prob of sentence so far: -13.986398945200479
FRENCH: P(e|qu) = -0.2536416357055235 ==> log prob of sentence so far: -15.143501557533627

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: itwa
ENGLISH: P(a|itw) = -0.20789726226350533 ==> log prob of sentence so far: -0.20789726226350533
FRENCH: P(a|itw) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874

4GRAM: twas
ENGLISH: P(s|twa) = -0.12640843168907553 ==> log prob of sentence so far: -0.33430569395258086
FRENCH: P(s|twa) = -1.4313637641589874 ==> log prob of sentence so far: -2.8627275283179747

4GRAM: wasm
ENGLISH: P(m|was) = -1.5941395076918652 ==> log prob of sentence so far: -1.928445201644446
FRENCH: P(m|was) = -1.505149978319906 ==> log prob of sentence so far: -4.367877506637881

4GRAM: asma
ENGLISH: P(a|asm) = -0.5045899595680278 ==> log prob of sentence so far: -2.433035161212474
FRENCH: P(a|asm) = -0.568778604136634 ==> log prob of sentence so far: -4.936656110774515

4GRAM: smag
ENGLISH: P(g|sma) = -1.9764750338052806 ==> log prob of sentence so far: -4.409510195017754
FRENCH: P(g|sma) = -1.6793652323768318 ==> log prob of sentence so far: -6.616021343151347

4GRAM: magn
ENGLISH: P(n|mag) = -0.4551760033580674 ==> log prob of sentence so far: -4.864686198375821
FRENCH: P(n|mag) = -0.4598622492781567 ==> log prob of sentence so far: -7.075883592429504

4GRAM: agni
ENGLISH: P(i|agn) = -0.31900336667588214 ==> log prob of sentence so far: -5.183689565051703
FRENCH: P(i|agn) = -0.7999796878793164 ==> log prob of sentence so far: -7.87586328030882

4GRAM: gnif
ENGLISH: P(f|gni) = -0.5198732351406123 ==> log prob of sentence so far: -5.703562800192316
FRENCH: P(f|gni) = -0.42459220344347787 ==> log prob of sentence so far: -8.300455483752298

4GRAM: nifi
ENGLISH: P(i|nif) = -0.4973246408079494 ==> log prob of sentence so far: -6.2008874410002655
FRENCH: P(i|nif) = -0.254661253092615 ==> log prob of sentence so far: -8.555116736844914

4GRAM: ifiq
ENGLISH: P(q|ifi) = -2.749736315569061 ==> log prob of sentence so far: -8.950623756569327
FRENCH: P(q|ifi) = -0.39937727798791944 ==> log prob of sentence so far: -8.954494014832834

4GRAM: fiqu
ENGLISH: P(u|fiq) = -1.4313637641589874 ==> log prob of sentence so far: -10.381987520728314
FRENCH: P(u|fiq) = -0.05618864549996886 ==> log prob of sentence so far: -9.010682660332803

4GRAM: ique
ENGLISH: P(e|iqu) = -0.39600550889483793 ==> log prob of sentence so far: -10.777993029623152
FRENCH: P(e|iqu) = -0.09911496774854345 ==> log prob of sentence so far: -9.109797628081346

According to the 4gram model, the sentence is in French
----------------
