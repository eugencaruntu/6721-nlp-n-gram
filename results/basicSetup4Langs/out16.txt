They are bad hombres.

1GRAM MODEL:

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -1.0342236377842724
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -1.1659667480967535
DUTCH: P(t) = -1.2498840127620319 ==> log prob of sentence so far: -1.2498840127620319
PORTUGUESE: P(t) = -1.3798331462051736 ==> log prob of sentence so far: -1.3798331462051736
SPANISH: P(t) = -1.4035916054876783 ==> log prob of sentence so far: -1.4035916054876783

1GRAM: h
ENGLISH: P(h) = -1.180274162400034 ==> log prob of sentence so far: -2.214497800184306
FRENCH: P(h) = -2.1056297044472685 ==> log prob of sentence so far: -3.271596452544022
DUTCH: P(h) = -1.4907856078212052 ==> log prob of sentence so far: -2.740669620583237
PORTUGUESE: P(h) = -1.8092147133299747 ==> log prob of sentence so far: -3.1890478595351484
SPANISH: P(h) = -1.8922107698506663 ==> log prob of sentence so far: -3.2958023753383445

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -3.1245690182442134
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -4.038562267066478
DUTCH: P(e) = -0.7296525128280946 ==> log prob of sentence so far: -3.4703221334113312
PORTUGUESE: P(e) = -0.8804550652146593 ==> log prob of sentence so far: -4.069502924749807
SPANISH: P(e) = -0.8816549652595953 ==> log prob of sentence so far: -4.17745734059794

1GRAM: y
ENGLISH: P(y) = -1.7505965755952897 ==> log prob of sentence so far: -4.875165593839503
FRENCH: P(y) = -2.672224427638286 ==> log prob of sentence so far: -6.710786694704764
DUTCH: P(y) = -3.568910669966972 ==> log prob of sentence so far: -7.039232803378303
PORTUGUESE: P(y) = -3.3437360946864336 ==> log prob of sentence so far: -7.413239019436241
SPANISH: P(y) = -1.910627842042729 ==> log prob of sentence so far: -6.088085182640668

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -5.961954274881957
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -7.787139789810272
DUTCH: P(a) = -1.115393510092554 ==> log prob of sentence so far: -8.154626313470857
PORTUGUESE: P(a) = -0.8319048847689273 ==> log prob of sentence so far: -8.245143904205168
SPANISH: P(a) = -0.902025778564821 ==> log prob of sentence so far: -6.990110961205489

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -7.223227176194053
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -8.971168428880729
DUTCH: P(r) = -1.2542028641369134 ==> log prob of sentence so far: -9.408829177607771
PORTUGUESE: P(r) = -1.1945470328532288 ==> log prob of sentence so far: -9.439690937058398
SPANISH: P(r) = -1.191045490967515 ==> log prob of sentence so far: -8.181156452173004

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -8.13329839425396
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -9.738134243403184
DUTCH: P(e) = -0.7296525128280946 ==> log prob of sentence so far: -10.138481690435865
PORTUGUESE: P(e) = -0.8804550652146593 ==> log prob of sentence so far: -10.320146002273058
SPANISH: P(e) = -0.8816549652595953 ==> log prob of sentence so far: -9.0628114174326

1GRAM: b
ENGLISH: P(b) = -1.7519519887518187 ==> log prob of sentence so far: -9.885250383005777
FRENCH: P(b) = -2.0518881761829366 ==> log prob of sentence so far: -11.790022419586121
DUTCH: P(b) = -1.8074682953019041 ==> log prob of sentence so far: -11.94594998573777
PORTUGUESE: P(b) = -2.0434482110969183 ==> log prob of sentence so far: -12.363594213369975
SPANISH: P(b) = -1.806323409368014 ==> log prob of sentence so far: -10.869134826800614

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -10.972039064048232
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -12.86637551469163
DUTCH: P(a) = -1.115393510092554 ==> log prob of sentence so far: -13.061343495830323
PORTUGUESE: P(a) = -0.8319048847689273 ==> log prob of sentence so far: -13.195499098138903
SPANISH: P(a) = -0.902025778564821 ==> log prob of sentence so far: -11.771160605365436

1GRAM: d
ENGLISH: P(d) = -1.396179917541683 ==> log prob of sentence so far: -12.368218981589916
FRENCH: P(d) = -1.4202616779892512 ==> log prob of sentence so far: -14.28663719268088
DUTCH: P(d) = -1.2011871346521803 ==> log prob of sentence so far: -14.262530630482503
PORTUGUESE: P(d) = -1.3354060130264946 ==> log prob of sentence so far: -14.530905111165398
SPANISH: P(d) = -1.290763837647315 ==> log prob of sentence so far: -13.061924443012751

1GRAM: h
ENGLISH: P(h) = -1.180274162400034 ==> log prob of sentence so far: -13.54849314398995
FRENCH: P(h) = -2.1056297044472685 ==> log prob of sentence so far: -16.39226689712815
DUTCH: P(h) = -1.4907856078212052 ==> log prob of sentence so far: -15.753316238303709
PORTUGUESE: P(h) = -1.8092147133299747 ==> log prob of sentence so far: -16.34011982449537
SPANISH: P(h) = -1.8922107698506663 ==> log prob of sentence so far: -14.954135212863417

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -14.686276904575443
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -17.666610227445805
DUTCH: P(o) = -1.1998196708165811 ==> log prob of sentence so far: -16.95313590912029
PORTUGUESE: P(o) = -0.982146444324746 ==> log prob of sentence so far: -17.322266268820115
SPANISH: P(o) = -0.9966351897105246 ==> log prob of sentence so far: -15.95077040257394

1GRAM: m
ENGLISH: P(m) = -1.6111104829831362 ==> log prob of sentence so far: -16.29738738755858
FRENCH: P(m) = -1.5129486053385504 ==> log prob of sentence so far: -19.179558832784355
DUTCH: P(m) = -1.618864695295976 ==> log prob of sentence so far: -18.572000604416267
PORTUGUESE: P(m) = -1.3143919171181386 ==> log prob of sentence so far: -18.636658185938256
SPANISH: P(m) = -1.497081738512457 ==> log prob of sentence so far: -17.447852141086397

1GRAM: b
ENGLISH: P(b) = -1.7519519887518187 ==> log prob of sentence so far: -18.0493393763104
FRENCH: P(b) = -2.0518881761829366 ==> log prob of sentence so far: -21.231447008967294
DUTCH: P(b) = -1.8074682953019041 ==> log prob of sentence so far: -20.379468899718173
PORTUGUESE: P(b) = -2.0434482110969183 ==> log prob of sentence so far: -20.680106397035175
SPANISH: P(b) = -1.806323409368014 ==> log prob of sentence so far: -19.254175550454413

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -19.310612277622496
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -22.415475648037752
DUTCH: P(r) = -1.2542028641369134 ==> log prob of sentence so far: -21.633671763855087
PORTUGUESE: P(r) = -1.1945470328532288 ==> log prob of sentence so far: -21.874653429888404
SPANISH: P(r) = -1.191045490967515 ==> log prob of sentence so far: -20.445221041421927

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -20.220683495682405
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -23.18244146256021
DUTCH: P(e) = -0.7296525128280946 ==> log prob of sentence so far: -22.363324276683183
PORTUGUESE: P(e) = -0.8804550652146593 ==> log prob of sentence so far: -22.755108495103062
SPANISH: P(e) = -0.8816549652595953 ==> log prob of sentence so far: -21.326876006681523

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -21.391824508411005
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -24.246889177449866
DUTCH: P(s) = -1.460439695007922 ==> log prob of sentence so far: -23.823763971691104
PORTUGUESE: P(s) = -1.1129010285568741 ==> log prob of sentence so far: -23.868009523659936
SPANISH: P(s) = -1.1432621669802536 ==> log prob of sentence so far: -22.470138173661777

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: th
ENGLISH: P(h|t) = -0.4192831299636363 ==> log prob of sentence so far: -0.4192831299636363
FRENCH: P(h|t) = -2.127206501261668 ==> log prob of sentence so far: -2.127206501261668
DUTCH: P(h|t) = -1.3564502636224927 ==> log prob of sentence so far: -1.3564502636224927
PORTUGUESE: P(h|t) = -2.1862348739107444 ==> log prob of sentence so far: -2.1862348739107444
SPANISH: P(h|t) = -3.6165505280491983 ==> log prob of sentence so far: -3.6165505280491983

2GRAM: he
ENGLISH: P(e|h) = -0.37002759861537876 ==> log prob of sentence so far: -0.7893107285790151
FRENCH: P(e|h) = -0.45804503705298444 ==> log prob of sentence so far: -2.5852515383146524
DUTCH: P(e|h) = -0.44013802186839185 ==> log prob of sentence so far: -1.7965882854908846
PORTUGUESE: P(e|h) = -0.5754988716421023 ==> log prob of sentence so far: -2.7617337455528466
SPANISH: P(e|h) = -0.8490540263197558 ==> log prob of sentence so far: -4.465604554368954

2GRAM: ey
ENGLISH: P(y|e) = -1.8313499597677305 ==> log prob of sentence so far: -2.620660688346746
FRENCH: P(y|e) = -3.462231569660896 ==> log prob of sentence so far: -6.047483107975548
DUTCH: P(y|e) = -4.334947509059353 ==> log prob of sentence so far: -6.1315357945502384
PORTUGUESE: P(y|e) = -3.2701451708811593 ==> log prob of sentence so far: -6.031878916434006
SPANISH: P(y|e) = -2.0101828793514245 ==> log prob of sentence so far: -6.475787433720379

2GRAM: ya
ENGLISH: P(a|y) = -1.042285010275727 ==> log prob of sentence so far: -3.662945698622473
FRENCH: P(a|y) = -0.6224025699054712 ==> log prob of sentence so far: -6.66988567788102
DUTCH: P(a|y) = -1.1640027594479423 ==> log prob of sentence so far: -7.295538553998181
PORTUGUESE: P(a|y) = -1.512836806986197 ==> log prob of sentence so far: -7.544715723420203
SPANISH: P(a|y) = -0.7719794602281751 ==> log prob of sentence so far: -7.247766893948555

2GRAM: ar
ENGLISH: P(r|a) = -0.9854656313970852 ==> log prob of sentence so far: -4.648411330019558
FRENCH: P(r|a) = -1.0331920938948058 ==> log prob of sentence so far: -7.703077771775826
DUTCH: P(r|a) = -0.9128710566861395 ==> log prob of sentence so far: -8.20840961068432
PORTUGUESE: P(r|a) = -1.0106812682073374 ==> log prob of sentence so far: -8.555396991627541
SPANISH: P(r|a) = -0.9444297449083868 ==> log prob of sentence so far: -8.192196638856942

2GRAM: re
ENGLISH: P(e|r) = -0.6282077999008819 ==> log prob of sentence so far: -5.27661912992044
FRENCH: P(e|r) = -0.49097082634029093 ==> log prob of sentence so far: -8.194048598116117
DUTCH: P(e|r) = -0.7945617194164155 ==> log prob of sentence so far: -9.002971330100737
PORTUGUESE: P(e|r) = -0.7387244644522476 ==> log prob of sentence so far: -9.294121456079788
SPANISH: P(e|r) = -0.6819283677734008 ==> log prob of sentence so far: -8.874125006630342

2GRAM: eb
ENGLISH: P(b|e) = -1.6930590296644747 ==> log prob of sentence so far: -6.969678159584914
FRENCH: P(b|e) = -2.134431311091517 ==> log prob of sentence so far: -10.328479909207633
DUTCH: P(b|e) = -1.7345970348075601 ==> log prob of sentence so far: -10.737568364908297
PORTUGUESE: P(b|e) = -2.0921899841493197 ==> log prob of sentence so far: -11.386311440229107
SPANISH: P(b|e) = -2.1008354489063956 ==> log prob of sentence so far: -10.974960455536738

2GRAM: ba
ENGLISH: P(a|b) = -1.2018874970210744 ==> log prob of sentence so far: -8.171565656605988
FRENCH: P(a|b) = -0.8263108964722854 ==> log prob of sentence so far: -11.15479080567992
DUTCH: P(a|b) = -1.1578904694770384 ==> log prob of sentence so far: -11.895458834385336
PORTUGUESE: P(a|b) = -0.6448046985355126 ==> log prob of sentence so far: -12.03111613876462
SPANISH: P(a|b) = -0.4712917110589386 ==> log prob of sentence so far: -11.446252166595677

2GRAM: ad
ENGLISH: P(d|a) = -1.3608830323254337 ==> log prob of sentence so far: -9.532448688931423
FRENCH: P(d|a) = -1.6547663418439653 ==> log prob of sentence so far: -12.809557147523885
DUTCH: P(d|a) = -1.338334240128504 ==> log prob of sentence so far: -13.233793074513839
PORTUGUESE: P(d|a) = -1.0655608351407835 ==> log prob of sentence so far: -13.096676973905403
SPANISH: P(d|a) = -1.0184412167912962 ==> log prob of sentence so far: -12.464693383386972

2GRAM: dh
ENGLISH: P(h|d) = -1.4165743253154863 ==> log prob of sentence so far: -10.949023014246908
FRENCH: P(h|d) = -2.319773582881718 ==> log prob of sentence so far: -15.129330730405602
DUTCH: P(h|d) = -1.7222288494481852 ==> log prob of sentence so far: -14.956021923962025
PORTUGUESE: P(h|d) = -3.842817185260646 ==> log prob of sentence so far: -16.93949415916605
SPANISH: P(h|d) = -3.174861915027573 ==> log prob of sentence so far: -15.639555298414546

2GRAM: ho
ENGLISH: P(o|h) = -1.1036697663560255 ==> log prob of sentence so far: -12.052692780602934
FRENCH: P(o|h) = -0.8331794160095403 ==> log prob of sentence so far: -15.962510146415143
DUTCH: P(o|h) = -1.0175695134664042 ==> log prob of sentence so far: -15.973591437428428
PORTUGUESE: P(o|h) = -0.5411476315914249 ==> log prob of sentence so far: -17.480641790757474
SPANISH: P(o|h) = -0.5336991415796619 ==> log prob of sentence so far: -16.173254439994206

2GRAM: om
ENGLISH: P(m|o) = -1.183264342260203 ==> log prob of sentence so far: -13.235957122863137
FRENCH: P(m|o) = -1.1432478153884733 ==> log prob of sentence so far: -17.105757961803615
DUTCH: P(m|o) = -1.269157684902231 ==> log prob of sentence so far: -17.24274912233066
PORTUGUESE: P(m|o) = -1.0087652086181325 ==> log prob of sentence so far: -18.489406999375607
SPANISH: P(m|o) = -1.1213640740409072 ==> log prob of sentence so far: -17.294618514035115

2GRAM: mb
ENGLISH: P(b|m) = -1.558360280205732 ==> log prob of sentence so far: -14.79431740306887
FRENCH: P(b|m) = -1.4619185934547743 ==> log prob of sentence so far: -18.56767655525839
DUTCH: P(b|m) = -1.0954222299110425 ==> log prob of sentence so far: -18.338171352241705
PORTUGUESE: P(b|m) = -1.5247312114363194 ==> log prob of sentence so far: -20.014138210811925
SPANISH: P(b|m) = -1.2484406411518925 ==> log prob of sentence so far: -18.543059155187006

2GRAM: br
ENGLISH: P(r|b) = -1.240850356458931 ==> log prob of sentence so far: -16.035167759527802
FRENCH: P(r|b) = -0.7697085984080487 ==> log prob of sentence so far: -19.33738515366644
DUTCH: P(r|b) = -1.1242870307099486 ==> log prob of sentence so far: -19.462458382951652
PORTUGUESE: P(r|b) = -0.6513708308959013 ==> log prob of sentence so far: -20.665509041707825
SPANISH: P(r|b) = -0.8227743184919747 ==> log prob of sentence so far: -19.36583347367898

2GRAM: re
ENGLISH: P(e|r) = -0.6282077999008819 ==> log prob of sentence so far: -16.663375559428683
FRENCH: P(e|r) = -0.49097082634029093 ==> log prob of sentence so far: -19.82835598000673
DUTCH: P(e|r) = -0.7945617194164155 ==> log prob of sentence so far: -20.257020102368067
PORTUGUESE: P(e|r) = -0.7387244644522476 ==> log prob of sentence so far: -21.40423350616007
SPANISH: P(e|r) = -0.6819283677734008 ==> log prob of sentence so far: -20.04776184145238

2GRAM: es
ENGLISH: P(s|e) = -0.944867640322014 ==> log prob of sentence so far: -17.608243199750696
FRENCH: P(s|e) = -0.7261499946209886 ==> log prob of sentence so far: -20.55450597462772
DUTCH: P(s|e) = -1.4885220269054555 ==> log prob of sentence so far: -21.745542129273524
PORTUGUESE: P(s|e) = -0.8221373946407721 ==> log prob of sentence so far: -22.226370900800845
SPANISH: P(s|e) = -0.7773898838843721 ==> log prob of sentence so far: -20.825151725336752

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: the
ENGLISH: P(e|th) = -0.2045358758824928 ==> log prob of sentence so far: -0.2045358758824928
FRENCH: P(e|th) = -0.3852654323756641 ==> log prob of sentence so far: -0.3852654323756641
DUTCH: P(e|th) = -0.44420599830783675 ==> log prob of sentence so far: -0.44420599830783675
PORTUGUESE: P(e|th) = -0.2952004520032573 ==> log prob of sentence so far: -0.2952004520032573
SPANISH: P(e|th) = -1.505149978319906 ==> log prob of sentence so far: -1.505149978319906

3GRAM: hey
ENGLISH: P(y|he) = -1.5297874412840595 ==> log prob of sentence so far: -1.7343233171665524
FRENCH: P(y|he) = -3.5768018958289125 ==> log prob of sentence so far: -3.9620673282045766
DUTCH: P(y|he) = -3.982904117792628 ==> log prob of sentence so far: -4.427110116100465
PORTUGUESE: P(y|he) = -3.2576785748691846 ==> log prob of sentence so far: -3.552879026872442
SPANISH: P(y|he) = -1.6414057230570007 ==> log prob of sentence so far: -3.146555701376907

3GRAM: eya
ENGLISH: P(a|ey) = -1.0571470905257425 ==> log prob of sentence so far: -2.791470407692295
FRENCH: P(a|ey) = -0.9113625129579335 ==> log prob of sentence so far: -4.87342984116251
DUTCH: P(a|ey) = -1.505149978319906 ==> log prob of sentence so far: -5.932260094420371
PORTUGUESE: P(a|ey) = -1.7323937598229686 ==> log prob of sentence so far: -5.28527278669541
SPANISH: P(a|ey) = -0.9170684873278635 ==> log prob of sentence so far: -4.063624188704771

3GRAM: yar
ENGLISH: P(r|ya) = -0.8525768505928829 ==> log prob of sentence so far: -3.6440472582851777
FRENCH: P(r|ya) = -1.6896048008603892 ==> log prob of sentence so far: -6.563034642022899
DUTCH: P(r|ya) = -1.6232492903979006 ==> log prob of sentence so far: -7.555509384818272
PORTUGUESE: P(r|ya) = -1.505149978319906 ==> log prob of sentence so far: -6.790422765015316
SPANISH: P(r|ya) = -1.5132795580510534 ==> log prob of sentence so far: -5.576903746755824

3GRAM: are
ENGLISH: P(e|ar) = -0.8595957332477835 ==> log prob of sentence so far: -4.503642991532962
FRENCH: P(e|ar) = -1.0012102965990957 ==> log prob of sentence so far: -7.564244938621995
DUTCH: P(e|ar) = -0.7236914131442396 ==> log prob of sentence so far: -8.279200797962512
PORTUGUESE: P(e|ar) = -0.7951256663417321 ==> log prob of sentence so far: -7.585548431357049
SPANISH: P(e|ar) = -0.8959373554428377 ==> log prob of sentence so far: -6.472841102198662

3GRAM: reb
ENGLISH: P(b|re) = -1.6520394235875298 ==> log prob of sentence so far: -6.155682415120491
FRENCH: P(b|re) = -2.3667645179843597 ==> log prob of sentence so far: -9.931009456606354
DUTCH: P(b|re) = -2.287559648272953 ==> log prob of sentence so far: -10.566760446235465
PORTUGUESE: P(b|re) = -2.3084363471676523 ==> log prob of sentence so far: -9.8939847785247
SPANISH: P(b|re) = -2.2066886943043906 ==> log prob of sentence so far: -8.679529796503052

3GRAM: eba
ENGLISH: P(a|eb) = -1.1260012695110948 ==> log prob of sentence so far: -7.281683684631586
FRENCH: P(a|eb) = -0.5061449260217418 ==> log prob of sentence so far: -10.437154382628096
DUTCH: P(a|eb) = -1.2565160119855174 ==> log prob of sentence so far: -11.823276458220983
PORTUGUESE: P(a|eb) = -0.6466421239807029 ==> log prob of sentence so far: -10.540626902505403
SPANISH: P(a|eb) = -0.576732731719391 ==> log prob of sentence so far: -9.256262528222443

3GRAM: bad
ENGLISH: P(d|ba) = -1.4022107919156395 ==> log prob of sentence so far: -8.683894476547225
FRENCH: P(d|ba) = -2.571475903681944 ==> log prob of sentence so far: -13.00863028631004
DUTCH: P(d|ba) = -1.20507134006134 ==> log prob of sentence so far: -13.028347798282322
PORTUGUESE: P(d|ba) = -1.5500017449195571 ==> log prob of sentence so far: -12.09062864742496
SPANISH: P(d|ba) = -1.3424226808222062 ==> log prob of sentence so far: -10.59868520904465

3GRAM: adh
ENGLISH: P(h|ad) = -1.6373150793542557 ==> log prob of sentence so far: -10.321209555901481
FRENCH: P(h|ad) = -1.9232774529027363 ==> log prob of sentence so far: -14.931907739212777
DUTCH: P(h|ad) = -1.4443849461224374 ==> log prob of sentence so far: -14.472732744404759
PORTUGUESE: P(h|ad) = -3.2635571705077924 ==> log prob of sentence so far: -15.354185817932752
SPANISH: P(h|ad) = -2.810749350713349 ==> log prob of sentence so far: -13.409434559758

3GRAM: dho
ENGLISH: P(o|dh) = -0.9350848978097885 ==> log prob of sentence so far: -11.25629445371127
FRENCH: P(o|dh) = -0.641568532611636 ==> log prob of sentence so far: -15.573476271824413
DUTCH: P(o|dh) = -1.088026653275529 ==> log prob of sentence so far: -15.560759397680288
PORTUGUESE: P(o|dh) = -1.4471580313422192 ==> log prob of sentence so far: -16.80134384927497
SPANISH: P(o|dh) = -1.2218487496163564 ==> log prob of sentence so far: -14.631283309374355

3GRAM: hom
ENGLISH: P(m|ho) = -1.4923769682798602 ==> log prob of sentence so far: -12.74867142199113
FRENCH: P(m|ho) = -0.5212771657815363 ==> log prob of sentence so far: -16.09475343760595
DUTCH: P(m|ho) = -2.048880706550775 ==> log prob of sentence so far: -17.60964010423106
PORTUGUESE: P(m|ho) = -1.0381083653824248 ==> log prob of sentence so far: -17.839452214657396
SPANISH: P(m|ho) = -0.8579081021906827 ==> log prob of sentence so far: -15.489191411565038

3GRAM: omb
ENGLISH: P(b|om) = -1.7938587367729204 ==> log prob of sentence so far: -14.54253015876405
FRENCH: P(b|om) = -0.9395437620281402 ==> log prob of sentence so far: -17.03429719963409
DUTCH: P(b|om) = -1.531163410684824 ==> log prob of sentence so far: -19.140803514915884
PORTUGUESE: P(b|om) = -1.618297190198648 ==> log prob of sentence so far: -19.457749404856045
SPANISH: P(b|om) = -1.0131864241727706 ==> log prob of sentence so far: -16.502377835737807

3GRAM: mbr
ENGLISH: P(r|mb) = -1.2970025063809378 ==> log prob of sentence so far: -15.839532665144988
FRENCH: P(r|mb) = -0.3840654334461383 ==> log prob of sentence so far: -17.41836263308023
DUTCH: P(r|mb) = -1.8824432170142995 ==> log prob of sentence so far: -21.023246731930183
PORTUGUESE: P(r|mb) = -0.6007577580227178 ==> log prob of sentence so far: -20.05850716287876
SPANISH: P(r|mb) = -0.37570406801353473 ==> log prob of sentence so far: -16.87808190375134

3GRAM: bre
ENGLISH: P(e|br) = -0.5830241183391146 ==> log prob of sentence so far: -16.422556783484104
FRENCH: P(e|br) = -0.375484402040205 ==> log prob of sentence so far: -17.793847035120432
DUTCH: P(e|br) = -0.5832119080729268 ==> log prob of sentence so far: -21.60645864000311
PORTUGUESE: P(e|br) = -0.5324922252336204 ==> log prob of sentence so far: -20.590999388112383
SPANISH: P(e|br) = -0.2921865897977481 ==> log prob of sentence so far: -17.170268493549088

3GRAM: res
ENGLISH: P(s|re) = -0.8768268614328087 ==> log prob of sentence so far: -17.29938364491691
FRENCH: P(s|re) = -0.617883484477163 ==> log prob of sentence so far: -18.411730519597594
DUTCH: P(s|re) = -1.9263424466256551 ==> log prob of sentence so far: -23.532801086628762
PORTUGUESE: P(s|re) = -0.6056612692666085 ==> log prob of sentence so far: -21.19666065737899
SPANISH: P(s|re) = -0.650590711780536 ==> log prob of sentence so far: -17.820859205329622

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: they
ENGLISH: P(y|the) = -1.4142420199328793 ==> log prob of sentence so far: -1.4142420199328793
FRENCH: P(y|the) = -2.507855871695831 ==> log prob of sentence so far: -2.507855871695831
DUTCH: P(y|the) = -2.870403905279027 ==> log prob of sentence so far: -2.870403905279027
PORTUGUESE: P(y|the) = -2.0 ==> log prob of sentence so far: -2.0
SPANISH: P(y|the) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874

4GRAM: heya
ENGLISH: P(a|hey) = -0.8355187623408821 ==> log prob of sentence so far: -2.2497607822737615
FRENCH: P(a|hey) = -1.4313637641589874 ==> log prob of sentence so far: -3.939219635854818
DUTCH: P(a|hey) = -1.4313637641589874 ==> log prob of sentence so far: -4.301767669438014
PORTUGUESE: P(a|hey) = -1.4313637641589874 ==> log prob of sentence so far: -3.4313637641589874
SPANISH: P(a|hey) = -1.7160033436347992 ==> log prob of sentence so far: -3.1473671077937864

4GRAM: eyar
ENGLISH: P(r|eya) = -0.19851610513792628 ==> log prob of sentence so far: -2.448276887411688
FRENCH: P(r|eya) = -1.5797835966168101 ==> log prob of sentence so far: -5.519003232471628
DUTCH: P(r|eya) = -1.4313637641589874 ==> log prob of sentence so far: -5.733131433597002
PORTUGUESE: P(r|eya) = -1.4313637641589874 ==> log prob of sentence so far: -4.862727528317975
SPANISH: P(r|eya) = -1.6232492903979006 ==> log prob of sentence so far: -4.770616398191687

4GRAM: yare
ENGLISH: P(e|yar) = -0.4246178929813223 ==> log prob of sentence so far: -2.8728947803930103
FRENCH: P(e|yar) = -1.6020599913279623 ==> log prob of sentence so far: -7.121063223799591
DUTCH: P(e|yar) = -1.4313637641589874 ==> log prob of sentence so far: -7.16449519775599
PORTUGUESE: P(e|yar) = -1.4313637641589874 ==> log prob of sentence so far: -6.294091292476962
SPANISH: P(e|yar) = -0.7056005832350319 ==> log prob of sentence so far: -5.476216981426719

4GRAM: areb
ENGLISH: P(b|are) = -1.4684721456111491 ==> log prob of sentence so far: -4.341366926004159
FRENCH: P(b|are) = -2.562689299428688 ==> log prob of sentence so far: -9.683752523228279
DUTCH: P(b|are) = -2.041392685158225 ==> log prob of sentence so far: -9.205887882914215
PORTUGUESE: P(b|are) = -2.9822712330395684 ==> log prob of sentence so far: -9.27636252551653
SPANISH: P(b|are) = -2.6300887149282057 ==> log prob of sentence so far: -8.106305696354925

4GRAM: reba
ENGLISH: P(a|reb) = -1.6942062145399548 ==> log prob of sentence so far: -6.035573140544114
FRENCH: P(a|reb) = -0.8085338793773645 ==> log prob of sentence so far: -10.492286402605643
DUTCH: P(a|reb) = -1.792391689498254 ==> log prob of sentence so far: -10.99827957241247
PORTUGUESE: P(a|reb) = -1.0 ==> log prob of sentence so far: -10.27636252551653
SPANISH: P(a|reb) = -0.5625514500442887 ==> log prob of sentence so far: -8.668857146399214

4GRAM: ebad
ENGLISH: P(d|eba) = -1.7254448998676406 ==> log prob of sentence so far: -7.761018040411755
FRENCH: P(d|eba) = -2.7558748556724915 ==> log prob of sentence so far: -13.248161258278135
DUTCH: P(d|eba) = -2.255272505103306 ==> log prob of sentence so far: -13.253552077515776
PORTUGUESE: P(d|eba) = -2.1335389083702174 ==> log prob of sentence so far: -12.409901433886748
SPANISH: P(d|eba) = -1.3165421618422288 ==> log prob of sentence so far: -9.985399308241442

4GRAM: badh
ENGLISH: P(h|bad) = -2.0170333392987803 ==> log prob of sentence so far: -9.778051379710535
FRENCH: P(h|bad) = -1.4771212547196624 ==> log prob of sentence so far: -14.725282512997797
DUTCH: P(h|bad) = -1.9138138523837167 ==> log prob of sentence so far: -15.167365929899493
PORTUGUESE: P(h|bad) = -1.7160033436347992 ==> log prob of sentence so far: -14.125904777521548
SPANISH: P(h|bad) = -2.296665190261531 ==> log prob of sentence so far: -12.282064498502972

4GRAM: adho
ENGLISH: P(o|adh) = -0.9617956473297707 ==> log prob of sentence so far: -10.739847027040305
FRENCH: P(o|adh) = -1.271066772286538 ==> log prob of sentence so far: -15.996349285284335
DUTCH: P(o|adh) = -1.4014005407815442 ==> log prob of sentence so far: -16.568766470681037
PORTUGUESE: P(o|adh) = -1.4471580313422192 ==> log prob of sentence so far: -15.573062808863767
SPANISH: P(o|adh) = -1.1026623418971477 ==> log prob of sentence so far: -13.38472684040012

4GRAM: dhom
ENGLISH: P(m|dho) = -1.3136191229720018 ==> log prob of sentence so far: -12.053466150012307
FRENCH: P(m|dho) = -0.45312097831589593 ==> log prob of sentence so far: -16.44947026360023
DUTCH: P(m|dho) = -2.0253058652647704 ==> log prob of sentence so far: -18.594072335945807
PORTUGUESE: P(m|dho) = -1.4313637641589874 ==> log prob of sentence so far: -17.004426573022755
SPANISH: P(m|dho) = -1.4471580313422192 ==> log prob of sentence so far: -14.83188487174234

4GRAM: homb
ENGLISH: P(b|hom) = -2.526339277389844 ==> log prob of sentence so far: -14.579805427402151
FRENCH: P(b|hom) = -2.011993114659257 ==> log prob of sentence so far: -18.461463378259488
DUTCH: P(b|hom) = -1.662757831681574 ==> log prob of sentence so far: -20.25683016762738
PORTUGUESE: P(b|hom) = -1.0351136194163244 ==> log prob of sentence so far: -18.03954019243908
SPANISH: P(b|hom) = -0.0950235307533672 ==> log prob of sentence so far: -14.926908402495707

4GRAM: ombr
ENGLISH: P(r|omb) = -2.214843848047698 ==> log prob of sentence so far: -16.79464927544985
FRENCH: P(r|omb) = -0.2758272147770752 ==> log prob of sentence so far: -18.737290593036562
DUTCH: P(r|omb) = -1.1719352992845236 ==> log prob of sentence so far: -21.428765466911905
PORTUGUESE: P(r|omb) = -0.32861351729606975 ==> log prob of sentence so far: -18.36815370973515
SPANISH: P(r|omb) = -0.08919517347511034 ==> log prob of sentence so far: -15.016103575970817

4GRAM: mbre
ENGLISH: P(e|mbr) = -0.9128498242810998 ==> log prob of sentence so far: -17.70749909973095
FRENCH: P(e|mbr) = -0.09404448497491075 ==> log prob of sentence so far: -18.831335078011474
DUTCH: P(e|mbr) = -0.9637878273455552 ==> log prob of sentence so far: -22.39255329425746
PORTUGUESE: P(e|mbr) = -0.9215733926859567 ==> log prob of sentence so far: -19.289727102421107
SPANISH: P(e|mbr) = -0.13236275468750683 ==> log prob of sentence so far: -15.148466330658325

4GRAM: bres
ENGLISH: P(s|bre) = -1.3690544396487963 ==> log prob of sentence so far: -19.076553539379745
FRENCH: P(s|bre) = -0.5090802719483384 ==> log prob of sentence so far: -19.340415349959812
DUTCH: P(s|bre) = -2.4533183400470375 ==> log prob of sentence so far: -24.845871634304498
PORTUGUESE: P(s|bre) = -0.9153244434089552 ==> log prob of sentence so far: -20.205051545830063
SPANISH: P(s|bre) = -0.6846648610374535 ==> log prob of sentence so far: -15.833131191695777

According to the 4gram model, the sentence is in Spanish
----------------
