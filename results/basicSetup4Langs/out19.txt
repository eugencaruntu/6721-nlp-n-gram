John aime Brook.

1GRAM MODEL:

1GRAM: j
ENGLISH: P(j) = -2.944833324319119 ==> log prob of sentence so far: -2.944833324319119
FRENCH: P(j) = -2.2095776682007906 ==> log prob of sentence so far: -2.2095776682007906
DUTCH: P(j) = -1.7475117160205613 ==> log prob of sentence so far: -1.7475117160205613
PORTUGUESE: P(j) = -2.5146677756328772 ==> log prob of sentence so far: -2.5146677756328772
SPANISH: P(j) = -2.3398406643519727 ==> log prob of sentence so far: -2.3398406643519727

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -4.082617084904614
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -3.483920998518445
DUTCH: P(o) = -1.1998196708165811 ==> log prob of sentence so far: -2.9473313868371425
PORTUGUESE: P(o) = -0.982146444324746 ==> log prob of sentence so far: -3.4968142199576233
SPANISH: P(o) = -0.9966351897105246 ==> log prob of sentence so far: -3.3364758540624972

1GRAM: h
ENGLISH: P(h) = -1.180274162400034 ==> log prob of sentence so far: -5.262891247304648
FRENCH: P(h) = -2.1056297044472685 ==> log prob of sentence so far: -5.589550702965713
DUTCH: P(h) = -1.4907856078212052 ==> log prob of sentence so far: -4.438116994658348
PORTUGUESE: P(h) = -1.8092147133299747 ==> log prob of sentence so far: -5.306028933287598
SPANISH: P(h) = -1.8922107698506663 ==> log prob of sentence so far: -5.2286866239131635

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -6.424488184707277
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -6.712188179580218
DUTCH: P(n) = -0.9654264661937975 ==> log prob of sentence so far: -5.403543460852145
PORTUGUESE: P(n) = -1.3242854314663415 ==> log prob of sentence so far: -6.630314364753939
SPANISH: P(n) = -1.1498393665103084 ==> log prob of sentence so far: -6.378525990423472

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -7.511276865749731
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -7.788541274685727
DUTCH: P(a) = -1.115393510092554 ==> log prob of sentence so far: -6.518936970944699
PORTUGUESE: P(a) = -0.8319048847689273 ==> log prob of sentence so far: -7.4622192495228665
SPANISH: P(a) = -0.902025778564821 ==> log prob of sentence so far: -7.280551768988293

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -8.673802346023642
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -8.923747717354571
DUTCH: P(i) = -1.2262490424361108 ==> log prob of sentence so far: -7.74518601338081
PORTUGUESE: P(i) = -1.2396988999998464 ==> log prob of sentence so far: -8.701918149522713
SPANISH: P(i) = -1.2231544531761491 ==> log prob of sentence so far: -8.503706222164443

1GRAM: m
ENGLISH: P(m) = -1.6111104829831362 ==> log prob of sentence so far: -10.284912829006778
FRENCH: P(m) = -1.5129486053385504 ==> log prob of sentence so far: -10.436696322693122
DUTCH: P(m) = -1.618864695295976 ==> log prob of sentence so far: -9.364050708676785
PORTUGUESE: P(m) = -1.3143919171181386 ==> log prob of sentence so far: -10.016310066640852
SPANISH: P(m) = -1.497081738512457 ==> log prob of sentence so far: -10.000787960676899

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -11.194984047066685
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -11.203662137215577
DUTCH: P(e) = -0.7296525128280946 ==> log prob of sentence so far: -10.09370322150488
PORTUGUESE: P(e) = -0.8804550652146593 ==> log prob of sentence so far: -10.896765131855512
SPANISH: P(e) = -0.8816549652595953 ==> log prob of sentence so far: -10.882442925936495

1GRAM: b
ENGLISH: P(b) = -1.7519519887518187 ==> log prob of sentence so far: -12.946936035818503
FRENCH: P(b) = -2.0518881761829366 ==> log prob of sentence so far: -13.255550313398514
DUTCH: P(b) = -1.8074682953019041 ==> log prob of sentence so far: -11.901171516806784
PORTUGUESE: P(b) = -2.0434482110969183 ==> log prob of sentence so far: -12.940213342952429
SPANISH: P(b) = -1.806323409368014 ==> log prob of sentence so far: -12.688766335304509

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -14.2082089371306
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -14.43957895246897
DUTCH: P(r) = -1.2542028641369134 ==> log prob of sentence so far: -13.155374380943698
PORTUGUESE: P(r) = -1.1945470328532288 ==> log prob of sentence so far: -14.134760375805659
SPANISH: P(r) = -1.191045490967515 ==> log prob of sentence so far: -13.879811826272023

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -15.345992697716094
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -15.713922282786625
DUTCH: P(o) = -1.1998196708165811 ==> log prob of sentence so far: -14.355194051760279
PORTUGUESE: P(o) = -0.982146444324746 ==> log prob of sentence so far: -15.116906820130405
SPANISH: P(o) = -0.9966351897105246 ==> log prob of sentence so far: -14.876447015982547

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -16.483776458301588
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -16.98826561310428
DUTCH: P(o) = -1.1998196708165811 ==> log prob of sentence so far: -15.55501372257686
PORTUGUESE: P(o) = -0.982146444324746 ==> log prob of sentence so far: -16.09905326445515
SPANISH: P(o) = -0.9966351897105246 ==> log prob of sentence so far: -15.873082205693072

1GRAM: k
ENGLISH: P(k) = -2.072697153608919 ==> log prob of sentence so far: -18.556473611910505
FRENCH: P(k) = -3.5365941540489745 ==> log prob of sentence so far: -20.524859767153252
DUTCH: P(k) = -1.6325367017734616 ==> log prob of sentence so far: -17.18755042435032
PORTUGUESE: P(k) = -4.701247446302863 ==> log prob of sentence so far: -20.800300710758012
SPANISH: P(k) = -3.8196921134005866 ==> log prob of sentence so far: -19.692774319093658

According to the 1gram model, the sentence is in Dutch
----------------
2GRAM MODEL:

2GRAM: jo
ENGLISH: P(o|j) = -0.5261813808304232 ==> log prob of sentence so far: -0.5261813808304232
FRENCH: P(o|j) = -0.8910129977564625 ==> log prob of sentence so far: -0.8910129977564625
DUTCH: P(o|j) = -1.3517770774577649 ==> log prob of sentence so far: -1.3517770774577649
PORTUGUESE: P(o|j) = -0.47190904518122684 ==> log prob of sentence so far: -0.47190904518122684
SPANISH: P(o|j) = -0.36913610686012327 ==> log prob of sentence so far: -0.36913610686012327

2GRAM: oh
ENGLISH: P(h|o) = -1.9262959225061613 ==> log prob of sentence so far: -2.4524773033365843
FRENCH: P(h|o) = -3.298912055764982 ==> log prob of sentence so far: -4.189925053521445
DUTCH: P(h|o) = -2.712149419943157 ==> log prob of sentence so far: -4.063926497400922
PORTUGUESE: P(h|o) = -2.202028486415112 ==> log prob of sentence so far: -2.673937531596339
SPANISH: P(h|o) = -1.8293993777251516 ==> log prob of sentence so far: -2.1985354845852747

2GRAM: hn
ENGLISH: P(n|h) = -2.586587304671755 ==> log prob of sentence so far: -5.039064608008339
FRENCH: P(n|h) = -2.923084527448938 ==> log prob of sentence so far: -7.113009580970383
DUTCH: P(n|h) = -2.3418349579542044 ==> log prob of sentence so far: -6.405761455355126
PORTUGUESE: P(n|h) = -2.7336293068352755 ==> log prob of sentence so far: -5.407566838431615
SPANISH: P(n|h) = -2.8579815968773548 ==> log prob of sentence so far: -5.05651708146263

2GRAM: na
ENGLISH: P(a|n) = -1.24610849854619 ==> log prob of sentence so far: -6.285173106554529
FRENCH: P(a|n) = -1.1335472623560008 ==> log prob of sentence so far: -8.246556843326383
DUTCH: P(a|n) = -1.2185950002690131 ==> log prob of sentence so far: -7.624356455624139
PORTUGUESE: P(a|n) = -0.7416089597288706 ==> log prob of sentence so far: -6.149175798160486
SPANISH: P(a|n) = -0.9403755142775989 ==> log prob of sentence so far: -5.996892595740229

2GRAM: ai
ENGLISH: P(i|a) = -1.3482554679089638 ==> log prob of sentence so far: -7.633428574463493
FRENCH: P(i|a) = -0.6966031694951295 ==> log prob of sentence so far: -8.943160012821512
DUTCH: P(i|a) = -2.2555748524653443 ==> log prob of sentence so far: -9.879931308089484
PORTUGUESE: P(i|a) = -1.4740513510039652 ==> log prob of sentence so far: -7.623227149164451
SPANISH: P(i|a) = -1.9693218239386479 ==> log prob of sentence so far: -7.9662144196788764

2GRAM: im
ENGLISH: P(m|i) = -1.3479869275073166 ==> log prob of sentence so far: -8.98141550197081
FRENCH: P(m|i) = -1.5427023844256251 ==> log prob of sentence so far: -10.485862397247137
DUTCH: P(m|i) = -2.1528907438099565 ==> log prob of sentence so far: -12.03282205189944
PORTUGUESE: P(m|i) = -1.1491537874567963 ==> log prob of sentence so far: -8.772380936621248
SPANISH: P(m|i) = -1.4590738564302623 ==> log prob of sentence so far: -9.42528827610914

2GRAM: me
ENGLISH: P(e|m) = -0.5945354301857667 ==> log prob of sentence so far: -9.575950932156577
FRENCH: P(e|m) = -0.43058576450809083 ==> log prob of sentence so far: -10.916448161755229
DUTCH: P(e|m) = -0.4597017042065431 ==> log prob of sentence so far: -12.492523756105983
PORTUGUESE: P(e|m) = -0.7094788805939174 ==> log prob of sentence so far: -9.481859817215165
SPANISH: P(e|m) = -0.5847661832407273 ==> log prob of sentence so far: -10.010054459349867

2GRAM: eb
ENGLISH: P(b|e) = -1.6930590296644747 ==> log prob of sentence so far: -11.269009961821052
FRENCH: P(b|e) = -2.134431311091517 ==> log prob of sentence so far: -13.050879472846745
DUTCH: P(b|e) = -1.7345970348075601 ==> log prob of sentence so far: -14.227120790913544
PORTUGUESE: P(b|e) = -2.0921899841493197 ==> log prob of sentence so far: -11.574049801364485
SPANISH: P(b|e) = -2.1008354489063956 ==> log prob of sentence so far: -12.110889908256262

2GRAM: br
ENGLISH: P(r|b) = -1.240850356458931 ==> log prob of sentence so far: -12.509860318279983
FRENCH: P(r|b) = -0.7697085984080487 ==> log prob of sentence so far: -13.820588071254793
DUTCH: P(r|b) = -1.1242870307099486 ==> log prob of sentence so far: -15.351407821623493
PORTUGUESE: P(r|b) = -0.6513708308959013 ==> log prob of sentence so far: -12.225420632260386
SPANISH: P(r|b) = -0.8227743184919747 ==> log prob of sentence so far: -12.933664226748236

2GRAM: ro
ENGLISH: P(o|r) = -0.9679454985579867 ==> log prob of sentence so far: -13.47780581683797
FRENCH: P(o|r) = -1.1139096014103482 ==> log prob of sentence so far: -14.934497672665142
DUTCH: P(o|r) = -1.030099792018266 ==> log prob of sentence so far: -16.38150761364176
PORTUGUESE: P(o|r) = -0.9761621541266129 ==> log prob of sentence so far: -13.201582786386998
SPANISH: P(o|r) = -0.8581323749493194 ==> log prob of sentence so far: -13.791796601697556

2GRAM: oo
ENGLISH: P(o|o) = -1.3567983196477529 ==> log prob of sentence so far: -14.834604136485723
FRENCH: P(o|o) = -2.756524069532728 ==> log prob of sentence so far: -17.69102174219787
DUTCH: P(o|o) = -0.6980615863437308 ==> log prob of sentence so far: -17.07956919998549
PORTUGUESE: P(o|o) = -1.7208052462356038 ==> log prob of sentence so far: -14.922388032622603
SPANISH: P(o|o) = -2.4179314061492745 ==> log prob of sentence so far: -16.20972800784683

2GRAM: ok
ENGLISH: P(k|o) = -1.8483101659233663 ==> log prob of sentence so far: -16.68291430240909
FRENCH: P(k|o) = -3.404715781933021 ==> log prob of sentence so far: -21.09573752413089
DUTCH: P(k|o) = -1.7588635066657041 ==> log prob of sentence so far: -18.838432706651194
PORTUGUESE: P(k|o) = -4.650734806320192 ==> log prob of sentence so far: -19.573122838942794
SPANISH: P(k|o) = -4.002585646137426 ==> log prob of sentence so far: -20.212313653984257

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: joh
ENGLISH: P(h|jo) = -1.1990404571266498 ==> log prob of sentence so far: -1.1990404571266498
FRENCH: P(h|jo) = -2.3533390953113047 ==> log prob of sentence so far: -2.3533390953113047
DUTCH: P(h|jo) = -1.979678422461289 ==> log prob of sentence so far: -1.979678422461289
PORTUGUESE: P(h|jo) = -1.7066229685645442 ==> log prob of sentence so far: -1.7066229685645442
SPANISH: P(h|jo) = -2.433289685195026 ==> log prob of sentence so far: -2.433289685195026

3GRAM: ohn
ENGLISH: P(n|oh) = -1.5072736719497615 ==> log prob of sentence so far: -2.7063141290764112
FRENCH: P(n|oh) = -1.0934216851622351 ==> log prob of sentence so far: -3.44676078047354
DUTCH: P(n|oh) = -1.6232492903979006 ==> log prob of sentence so far: -3.6029277128591897
PORTUGUESE: P(n|oh) = -2.0057523288890913 ==> log prob of sentence so far: -3.7123752974536357
SPANISH: P(n|oh) = -3.0277572046905536 ==> log prob of sentence so far: -5.46104688988558

3GRAM: hna
ENGLISH: P(a|hn) = -0.9975111995963053 ==> log prob of sentence so far: -3.7038253286727167
FRENCH: P(a|hn) = -1.1026623418971477 ==> log prob of sentence so far: -4.549423122370688
DUTCH: P(a|hn) = -0.8026250197668442 ==> log prob of sentence so far: -4.405552732626034
PORTUGUESE: P(a|hn) = -0.6255410871774854 ==> log prob of sentence so far: -4.3379163846311215
SPANISH: P(a|hn) = -1.5797835966168101 ==> log prob of sentence so far: -7.04083048650239

3GRAM: nai
ENGLISH: P(i|na) = -1.8708019698812508 ==> log prob of sentence so far: -5.574627298553968
FRENCH: P(i|na) = -0.743119041517866 ==> log prob of sentence so far: -5.2925421638885535
DUTCH: P(i|na) = -2.382959950139757 ==> log prob of sentence so far: -6.788512682765791
PORTUGUESE: P(i|na) = -2.626454430273112 ==> log prob of sentence so far: -6.964370814904234
SPANISH: P(i|na) = -2.00189098041235 ==> log prob of sentence so far: -9.04272146691474

3GRAM: aim
ENGLISH: P(m|ai) = -1.947719046120967 ==> log prob of sentence so far: -7.522346344674935
FRENCH: P(m|ai) = -2.0012284513314658 ==> log prob of sentence so far: -7.293770615220019
DUTCH: P(m|ai) = -2.0957503474808177 ==> log prob of sentence so far: -8.88426303024661
PORTUGUESE: P(m|ai) = -1.3937264463433443 ==> log prob of sentence so far: -8.358097261247577
SPANISH: P(m|ai) = -1.1765364610238362 ==> log prob of sentence so far: -10.219257927938576

3GRAM: ime
ENGLISH: P(e|im) = -0.5939876538828333 ==> log prob of sentence so far: -8.116333998557767
FRENCH: P(e|im) = -0.4859739967020765 ==> log prob of sentence so far: -7.779744611922096
DUTCH: P(e|im) = -0.6239618327757922 ==> log prob of sentence so far: -9.508224863022402
PORTUGUESE: P(e|im) = -0.49795532269968573 ==> log prob of sentence so far: -8.856052583947262
SPANISH: P(e|im) = -0.7118495325120334 ==> log prob of sentence so far: -10.93110746045061

3GRAM: meb
ENGLISH: P(b|me) = -1.7753850341404915 ==> log prob of sentence so far: -9.891719032698258
FRENCH: P(b|me) = -2.734441147934156 ==> log prob of sentence so far: -10.514185759856252
DUTCH: P(b|me) = -2.4015281806008035 ==> log prob of sentence so far: -11.909753043623205
PORTUGUESE: P(b|me) = -2.662547976890391 ==> log prob of sentence so far: -11.518600560837653
SPANISH: P(b|me) = -2.821731821690044 ==> log prob of sentence so far: -13.752839282140654

3GRAM: ebr
ENGLISH: P(r|eb) = -1.118556038914146 ==> log prob of sentence so far: -11.010275071612405
FRENCH: P(r|eb) = -0.6308181202910425 ==> log prob of sentence so far: -11.145003880147295
DUTCH: P(r|eb) = -0.9015405936899847 ==> log prob of sentence so far: -12.81129363731319
PORTUGUESE: P(r|eb) = -0.853116012030105 ==> log prob of sentence so far: -12.371716572867758
SPANISH: P(r|eb) = -1.0329704209135286 ==> log prob of sentence so far: -14.785809703054182

3GRAM: bro
ENGLISH: P(o|br) = -0.547286288277202 ==> log prob of sentence so far: -11.557561359889608
FRENCH: P(o|br) = -1.5134225042858958 ==> log prob of sentence so far: -12.65842638443319
DUTCH: P(o|br) = -0.7584655690253832 ==> log prob of sentence so far: -13.569759206338572
PORTUGUESE: P(o|br) = -1.0884812691255872 ==> log prob of sentence so far: -13.460197841993345
SPANISH: P(o|br) = -1.1127042801912883 ==> log prob of sentence so far: -15.89851398324547

3GRAM: roo
ENGLISH: P(o|ro) = -1.6010396911813078 ==> log prob of sentence so far: -13.158601051070915
FRENCH: P(o|ro) = -2.8002170270102105 ==> log prob of sentence so far: -15.4586434114434
DUTCH: P(o|ro) = -0.572618509036706 ==> log prob of sentence so far: -14.142377715375279
PORTUGUESE: P(o|ro) = -2.021036081752454 ==> log prob of sentence so far: -15.4812339237458
SPANISH: P(o|ro) = -2.425590920606955 ==> log prob of sentence so far: -18.324104903852426

3GRAM: ook
ENGLISH: P(k|oo) = -0.6708291323860486 ==> log prob of sentence so far: -13.829430183456964
FRENCH: P(k|oo) = -0.7561569566774757 ==> log prob of sentence so far: -16.214800368120876
DUTCH: P(k|oo) = -1.2206519236131899 ==> log prob of sentence so far: -15.363029638988468
PORTUGUESE: P(k|oo) = -2.9304395947667 ==> log prob of sentence so far: -18.4116735185125
SPANISH: P(k|oo) = -2.459392487759231 ==> log prob of sentence so far: -20.783497391611657

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: john
ENGLISH: P(n|joh) = -0.24144430567973715 ==> log prob of sentence so far: -0.24144430567973715
FRENCH: P(n|joh) = -0.7781512503836436 ==> log prob of sentence so far: -0.7781512503836436
DUTCH: P(n|joh) = -1.0280287236002434 ==> log prob of sentence so far: -1.0280287236002434
PORTUGUESE: P(n|joh) = -1.5314789170422551 ==> log prob of sentence so far: -1.5314789170422551
SPANISH: P(n|joh) = -1.4771212547196624 ==> log prob of sentence so far: -1.4771212547196624

4GRAM: ohna
ENGLISH: P(a|ohn) = -1.3921104650113136 ==> log prob of sentence so far: -1.6335547706910507
FRENCH: P(a|ohn) = -1.4771212547196624 ==> log prob of sentence so far: -2.255272505103306
DUTCH: P(a|ohn) = -1.4471580313422192 ==> log prob of sentence so far: -2.4751867549424627
PORTUGUESE: P(a|ohn) = -0.9700367766225568 ==> log prob of sentence so far: -2.501515693664812
SPANISH: P(a|ohn) = -1.4313637641589874 ==> log prob of sentence so far: -2.9084850188786495

4GRAM: hnai
ENGLISH: P(i|hna) = -1.7781512503836436 ==> log prob of sentence so far: -3.4117060210746946
FRENCH: P(i|hna) = -1.4471580313422192 ==> log prob of sentence so far: -3.702430536445525
DUTCH: P(i|hna) = -1.6812412373755872 ==> log prob of sentence so far: -4.15642799231805
PORTUGUESE: P(i|hna) = -1.5314789170422551 ==> log prob of sentence so far: -4.0329946107070676
SPANISH: P(i|hna) = -1.4313637641589874 ==> log prob of sentence so far: -4.339848783037636

4GRAM: naim
ENGLISH: P(m|nai) = -1.6092385759550858 ==> log prob of sentence so far: -5.02094459702978
FRENCH: P(m|nai) = -1.6862046569071374 ==> log prob of sentence so far: -5.388635193352663
DUTCH: P(m|nai) = -1.662757831681574 ==> log prob of sentence so far: -5.819185823999624
PORTUGUESE: P(m|nai) = -1.5314789170422551 ==> log prob of sentence so far: -5.564473527749323
SPANISH: P(m|nai) = -1.2041199826559248 ==> log prob of sentence so far: -5.5439687656935615

4GRAM: aime
ENGLISH: P(e|aim) = -0.3449354813630629 ==> log prob of sentence so far: -5.365880078392843
FRENCH: P(e|aim) = -0.4857556242438642 ==> log prob of sentence so far: -5.8743908175965265
DUTCH: P(e|aim) = -1.4471580313422192 ==> log prob of sentence so far: -7.266343855341844
PORTUGUESE: P(e|aim) = -1.5720967679505191 ==> log prob of sentence so far: -7.136570295699842
SPANISH: P(e|aim) = -1.9542425094393248 ==> log prob of sentence so far: -7.498211275132887

4GRAM: imeb
ENGLISH: P(b|ime) = -1.5095463446745085 ==> log prob of sentence so far: -6.875426423067352
FRENCH: P(b|ime) = -2.2895889525425965 ==> log prob of sentence so far: -8.163979770139123
DUTCH: P(b|ime) = -2.041392685158225 ==> log prob of sentence so far: -9.307736540500068
PORTUGUESE: P(b|ime) = -2.767897616018091 ==> log prob of sentence so far: -9.904467911717934
SPANISH: P(b|ime) = -2.0198083933535527 ==> log prob of sentence so far: -9.518019668486438

4GRAM: mebr
ENGLISH: P(r|meb) = -1.8532925186295284 ==> log prob of sentence so far: -8.72871894169688
FRENCH: P(r|meb) = -1.0334237554869496 ==> log prob of sentence so far: -9.197403525626074
DUTCH: P(r|meb) = -1.7160033436347992 ==> log prob of sentence so far: -11.023739884134867
PORTUGUESE: P(r|meb) = -1.5314789170422551 ==> log prob of sentence so far: -11.435946828760189
SPANISH: P(r|meb) = -1.5314789170422551 ==> log prob of sentence so far: -11.049498585528694

4GRAM: ebro
ENGLISH: P(o|ebr) = -0.6184504075161318 ==> log prob of sentence so far: -9.347169349213011
FRENCH: P(o|ebr) = -1.1189757896346233 ==> log prob of sentence so far: -10.316379315260697
DUTCH: P(o|ebr) = -0.9841232379011619 ==> log prob of sentence so far: -12.00786312203603
PORTUGUESE: P(o|ebr) = -0.9317351684414736 ==> log prob of sentence so far: -12.367681997201663
SPANISH: P(o|ebr) = -0.8061799739838872 ==> log prob of sentence so far: -11.855678559512581

4GRAM: broo
ENGLISH: P(o|bro) = -1.3335592204909013 ==> log prob of sentence so far: -10.680728569703913
FRENCH: P(o|bro) = -1.0 ==> log prob of sentence so far: -11.316379315260697
DUTCH: P(o|bro) = -0.9744458955276118 ==> log prob of sentence so far: -12.982309017563642
PORTUGUESE: P(o|bro) = -1.4866665726258927 ==> log prob of sentence so far: -13.854348569827556
SPANISH: P(o|bro) = -2.2041199826559246 ==> log prob of sentence so far: -14.059798542168506

4GRAM: rook
ENGLISH: P(k|roo) = -1.0176089450581944 ==> log prob of sentence so far: -11.698337514762107
FRENCH: P(k|roo) = -0.6020599913279624 ==> log prob of sentence so far: -11.91843930658866
DUTCH: P(k|roo) = -1.1431277164436386 ==> log prob of sentence so far: -14.12543673400728
PORTUGUESE: P(k|roo) = -1.7160033436347992 ==> log prob of sentence so far: -15.570351913462355
SPANISH: P(k|roo) = -1.6812412373755872 ==> log prob of sentence so far: -15.741039779544094

According to the 4gram model, the sentence is in English
----------------
