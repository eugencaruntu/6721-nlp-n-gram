I'm OK.

1GRAM MODEL:

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -1.162525480273911
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -1.1352064426688435
DUTCH: P(i) = -1.2262490424361108 ==> log prob of sentence so far: -1.2262490424361108
PORTUGUESE: P(i) = -1.2396988999998464 ==> log prob of sentence so far: -1.2396988999998464
SPANISH: P(i) = -1.2231544531761491 ==> log prob of sentence so far: -1.2231544531761491

1GRAM: m
ENGLISH: P(m) = -1.6111104829831362 ==> log prob of sentence so far: -2.773635963257047
FRENCH: P(m) = -1.5129486053385504 ==> log prob of sentence so far: -2.6481550480073937
DUTCH: P(m) = -1.618864695295976 ==> log prob of sentence so far: -2.8451137377320865
PORTUGUESE: P(m) = -1.3143919171181386 ==> log prob of sentence so far: -2.5540908171179852
SPANISH: P(m) = -1.497081738512457 ==> log prob of sentence so far: -2.720236191688606

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -3.911419723842542
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -3.9224983783250487
DUTCH: P(o) = -1.1998196708165811 ==> log prob of sentence so far: -4.044933408548667
PORTUGUESE: P(o) = -0.982146444324746 ==> log prob of sentence so far: -3.5362372614427313
SPANISH: P(o) = -0.9966351897105246 ==> log prob of sentence so far: -3.7168713813991308

1GRAM: k
ENGLISH: P(k) = -2.072697153608919 ==> log prob of sentence so far: -5.984116877451461
FRENCH: P(k) = -3.5365941540489745 ==> log prob of sentence so far: -7.459092532374023
DUTCH: P(k) = -1.6325367017734616 ==> log prob of sentence so far: -5.677470110322129
PORTUGUESE: P(k) = -4.701247446302863 ==> log prob of sentence so far: -8.237484707745594
SPANISH: P(k) = -3.8196921134005866 ==> log prob of sentence so far: -7.536563494799717

According to the 1gram model, the sentence is in Dutch
----------------
2GRAM MODEL:

2GRAM: im
ENGLISH: P(m|i) = -1.3479869275073166 ==> log prob of sentence so far: -1.3479869275073166
FRENCH: P(m|i) = -1.5427023844256251 ==> log prob of sentence so far: -1.5427023844256251
DUTCH: P(m|i) = -2.1528907438099565 ==> log prob of sentence so far: -2.1528907438099565
PORTUGUESE: P(m|i) = -1.1491537874567963 ==> log prob of sentence so far: -1.1491537874567963
SPANISH: P(m|i) = -1.4590738564302623 ==> log prob of sentence so far: -1.4590738564302623

2GRAM: mo
ENGLISH: P(o|m) = -0.8592540479884508 ==> log prob of sentence so far: -2.2072409754957674
FRENCH: P(o|m) = -0.8143598732347122 ==> log prob of sentence so far: -2.3570622576603375
DUTCH: P(o|m) = -1.0904994437808537 ==> log prob of sentence so far: -3.24339018759081
PORTUGUESE: P(o|m) = -0.8616200314551774 ==> log prob of sentence so far: -2.0107738189119737
SPANISH: P(o|m) = -0.7532346241572587 ==> log prob of sentence so far: -2.212308480587521

2GRAM: ok
ENGLISH: P(k|o) = -1.8483101659233663 ==> log prob of sentence so far: -4.055551141419134
FRENCH: P(k|o) = -3.404715781933021 ==> log prob of sentence so far: -5.761778039593358
DUTCH: P(k|o) = -1.7588635066657041 ==> log prob of sentence so far: -5.002253694256514
PORTUGUESE: P(k|o) = -4.650734806320192 ==> log prob of sentence so far: -6.661508625232166
SPANISH: P(k|o) = -4.002585646137426 ==> log prob of sentence so far: -6.214894126724947

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: imo
ENGLISH: P(o|im) = -1.5293319766718552 ==> log prob of sentence so far: -1.5293319766718552
FRENCH: P(o|im) = -1.036225088283415 ==> log prob of sentence so far: -1.036225088283415
DUTCH: P(o|im) = -1.7183830453801539 ==> log prob of sentence so far: -1.7183830453801539
PORTUGUESE: P(o|im) = -0.9720019133777879 ==> log prob of sentence so far: -0.9720019133777879
SPANISH: P(o|im) = -0.8431284471513524 ==> log prob of sentence so far: -0.8431284471513524

3GRAM: mok
ENGLISH: P(k|mo) = -1.723394603667437 ==> log prob of sentence so far: -3.252726580339292
FRENCH: P(k|mo) = -3.3341855853616735 ==> log prob of sentence so far: -4.370410673645089
DUTCH: P(k|mo) = -2.5110808455391185 ==> log prob of sentence so far: -4.2294638909192726
PORTUGUESE: P(k|mo) = -3.462996612028056 ==> log prob of sentence so far: -4.434998525405844
SPANISH: P(k|mo) = -3.122980000971728 ==> log prob of sentence so far: -3.9661084481230806

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: imok
ENGLISH: P(k|imo) = -2.278753600952829 ==> log prob of sentence so far: -2.278753600952829
FRENCH: P(k|imo) = -2.4683473304121573 ==> log prob of sentence so far: -2.4683473304121573
DUTCH: P(k|imo) = -1.505149978319906 ==> log prob of sentence so far: -1.505149978319906
PORTUGUESE: P(k|imo) = -2.3138672203691533 ==> log prob of sentence so far: -2.3138672203691533
SPANISH: P(k|imo) = -2.3654879848909 ==> log prob of sentence so far: -2.3654879848909

According to the 4gram model, the sentence is in Dutch
----------------
