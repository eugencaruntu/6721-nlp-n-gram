I hate AI.

1GRAM MODEL:

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -1.162525480273911
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -1.1352064426688435
DUTCH: P(i) = -1.2262490424361108 ==> log prob of sentence so far: -1.2262490424361108
PORTUGUESE: P(i) = -1.2396988999998464 ==> log prob of sentence so far: -1.2396988999998464
SPANISH: P(i) = -1.2231544531761491 ==> log prob of sentence so far: -1.2231544531761491

1GRAM: h
ENGLISH: P(h) = -1.180274162400034 ==> log prob of sentence so far: -2.3427996426739446
FRENCH: P(h) = -2.1056297044472685 ==> log prob of sentence so far: -3.240836147116112
DUTCH: P(h) = -1.4907856078212052 ==> log prob of sentence so far: -2.717034650257316
PORTUGUESE: P(h) = -1.8092147133299747 ==> log prob of sentence so far: -3.0489136133298214
SPANISH: P(h) = -1.8922107698506663 ==> log prob of sentence so far: -3.1153652230268154

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -3.4295883237163984
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -4.317189242221621
DUTCH: P(a) = -1.115393510092554 ==> log prob of sentence so far: -3.83242816034987
PORTUGUESE: P(a) = -0.8319048847689273 ==> log prob of sentence so far: -3.8808184980987486
SPANISH: P(a) = -0.902025778564821 ==> log prob of sentence so far: -4.017391001591636

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -4.463811961500671
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -5.483155990318375
DUTCH: P(t) = -1.2498840127620319 ==> log prob of sentence so far: -5.082312173111902
PORTUGUESE: P(t) = -1.3798331462051736 ==> log prob of sentence so far: -5.260651644303922
SPANISH: P(t) = -1.4035916054876783 ==> log prob of sentence so far: -5.420982607079314

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -5.373883179560578
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -6.250121804840831
DUTCH: P(e) = -0.7296525128280946 ==> log prob of sentence so far: -5.811964685939996
PORTUGUESE: P(e) = -0.8804550652146593 ==> log prob of sentence so far: -6.141106709518581
SPANISH: P(e) = -0.8816549652595953 ==> log prob of sentence so far: -6.302637572338909

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -6.460671860603032
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -7.326474899946341
DUTCH: P(a) = -1.115393510092554 ==> log prob of sentence so far: -6.92735819603255
PORTUGUESE: P(a) = -0.8319048847689273 ==> log prob of sentence so far: -6.973011594287509
SPANISH: P(a) = -0.902025778564821 ==> log prob of sentence so far: -7.20466335090373

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -7.623197340876943
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -8.461681342615185
DUTCH: P(i) = -1.2262490424361108 ==> log prob of sentence so far: -8.15360723846866
PORTUGUESE: P(i) = -1.2396988999998464 ==> log prob of sentence so far: -8.212710494287355
SPANISH: P(i) = -1.2231544531761491 ==> log prob of sentence so far: -8.42781780407988

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: ih
ENGLISH: P(h|i) = -2.462814340133484 ==> log prob of sentence so far: -2.462814340133484
FRENCH: P(h|i) = -3.2496745645848075 ==> log prob of sentence so far: -3.2496745645848075
DUTCH: P(h|i) = -3.172195899005343 ==> log prob of sentence so far: -3.172195899005343
PORTUGUESE: P(h|i) = -3.1832865953166 ==> log prob of sentence so far: -3.1832865953166
SPANISH: P(h|i) = -2.4605095419754166 ==> log prob of sentence so far: -2.4605095419754166

2GRAM: ha
ENGLISH: P(a|h) = -0.693585657435816 ==> log prob of sentence so far: -3.1563999975693
FRENCH: P(a|h) = -0.5247438164739214 ==> log prob of sentence so far: -3.774418381058729
DUTCH: P(a|h) = -0.8579471906645381 ==> log prob of sentence so far: -4.030143089669881
PORTUGUESE: P(a|h) = -0.4536466525562752 ==> log prob of sentence so far: -3.636933247872875
SPANISH: P(a|h) = -0.347746023436169 ==> log prob of sentence so far: -2.8082555654115855

2GRAM: at
ENGLISH: P(t|a) = -0.8627525070949107 ==> log prob of sentence so far: -4.019152504664211
FRENCH: P(t|a) = -1.2479713976288138 ==> log prob of sentence so far: -5.022389778687542
DUTCH: P(t|a) = -1.1097120347995644 ==> log prob of sentence so far: -5.139855124469445
PORTUGUESE: P(t|a) = -1.5396804063374159 ==> log prob of sentence so far: -5.176613654210291
SPANISH: P(t|a) = -1.5459294951996159 ==> log prob of sentence so far: -4.354185060611202

2GRAM: te
ENGLISH: P(e|t) = -1.0589086136007413 ==> log prob of sentence so far: -5.078061118264952
FRENCH: P(e|t) = -0.674136919284091 ==> log prob of sentence so far: -5.696526697971634
DUTCH: P(e|t) = -0.5037931098324119 ==> log prob of sentence so far: -5.643648234301857
PORTUGUESE: P(e|t) = -0.5755227896680853 ==> log prob of sentence so far: -5.752136443878376
SPANISH: P(e|t) = -0.6109410826889178 ==> log prob of sentence so far: -4.965126143300119

2GRAM: ea
ENGLISH: P(a|e) = -1.0585605717592588 ==> log prob of sentence so far: -6.136621690024211
FRENCH: P(a|e) = -1.5190302730971181 ==> log prob of sentence so far: -7.215556971068752
DUTCH: P(a|e) = -2.13121546254526 ==> log prob of sentence so far: -7.774863696847117
PORTUGUESE: P(a|e) = -1.2816437516923342 ==> log prob of sentence so far: -7.03378019557071
SPANISH: P(a|e) = -1.4656394168016165 ==> log prob of sentence so far: -6.430765560101736

2GRAM: ai
ENGLISH: P(i|a) = -1.3482554679089638 ==> log prob of sentence so far: -7.484877157933175
FRENCH: P(i|a) = -0.6966031694951295 ==> log prob of sentence so far: -7.9121601405638815
DUTCH: P(i|a) = -2.2555748524653443 ==> log prob of sentence so far: -10.03043854931246
PORTUGUESE: P(i|a) = -1.4740513510039652 ==> log prob of sentence so far: -8.507831546574675
SPANISH: P(i|a) = -1.9693218239386479 ==> log prob of sentence so far: -8.400087384040383

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: iha
ENGLISH: P(a|ih) = -0.12919954355702482 ==> log prob of sentence so far: -0.12919954355702482
FRENCH: P(a|ih) = -1.0687158123694598 ==> log prob of sentence so far: -1.0687158123694598
DUTCH: P(a|ih) = -0.7220353084047123 ==> log prob of sentence so far: -0.7220353084047123
PORTUGUESE: P(a|ih) = -0.6690067809585756 ==> log prob of sentence so far: -0.6690067809585756
SPANISH: P(a|ih) = -0.47466065617200565 ==> log prob of sentence so far: -0.47466065617200565

3GRAM: hat
ENGLISH: P(t|ha) = -0.49979351514318854 ==> log prob of sentence so far: -0.6289930587002134
FRENCH: P(t|ha) = -1.5468609445344457 ==> log prob of sentence so far: -2.6155767569039057
DUTCH: P(t|ha) = -1.7835755799721118 ==> log prob of sentence so far: -2.505610888376824
PORTUGUESE: P(t|ha) = -1.799154196959024 ==> log prob of sentence so far: -2.4681609779175995
SPANISH: P(t|ha) = -1.986042265258912 ==> log prob of sentence so far: -2.4607029214309177

3GRAM: ate
ENGLISH: P(e|at) = -0.801214234075548 ==> log prob of sentence so far: -1.4302072927757614
FRENCH: P(e|at) = -0.6889752704245974 ==> log prob of sentence so far: -3.304552027328503
DUTCH: P(e|at) = -0.7270602035513276 ==> log prob of sentence so far: -3.2326710919281516
PORTUGUESE: P(e|at) = -0.5974250008890664 ==> log prob of sentence so far: -3.065585978806666
SPANISH: P(e|at) = -0.6742696341308979 ==> log prob of sentence so far: -3.1349725555618155

3GRAM: tea
ENGLISH: P(a|te) = -1.4385350912518 ==> log prob of sentence so far: -2.8687423840275614
FRENCH: P(a|te) = -1.447334502080894 ==> log prob of sentence so far: -4.751886529409397
DUTCH: P(a|te) = -2.128898082949852 ==> log prob of sentence so far: -5.361569174878003
PORTUGUESE: P(a|te) = -1.1518316249362919 ==> log prob of sentence so far: -4.217417603742958
SPANISH: P(a|te) = -1.2069746324922537 ==> log prob of sentence so far: -4.341947188054069

3GRAM: eai
ENGLISH: P(i|ea) = -1.998542560496972 ==> log prob of sentence so far: -4.867284944524533
FRENCH: P(i|ea) = -1.3771746296905671 ==> log prob of sentence so far: -6.129061159099964
DUTCH: P(i|ea) = -3.058426024457005 ==> log prob of sentence so far: -8.419995199335009
PORTUGUESE: P(i|ea) = -1.3424226808222062 ==> log prob of sentence so far: -5.559840284565164
SPANISH: P(i|ea) = -2.173879697398652 ==> log prob of sentence so far: -6.5158268854527215

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: ihat
ENGLISH: P(t|iha) = -2.1026623418971475 ==> log prob of sentence so far: -2.1026623418971475
FRENCH: P(t|iha) = -1.505149978319906 ==> log prob of sentence so far: -1.505149978319906
DUTCH: P(t|iha) = -1.5563025007672873 ==> log prob of sentence so far: -1.5563025007672873
PORTUGUESE: P(t|iha) = -1.5314789170422551 ==> log prob of sentence so far: -1.5314789170422551
SPANISH: P(t|iha) = -1.9242792860618816 ==> log prob of sentence so far: -1.9242792860618816

4GRAM: hate
ENGLISH: P(e|hat) = -1.45574888107954 ==> log prob of sentence so far: -3.5584112229766873
FRENCH: P(e|hat) = -0.6405182431471381 ==> log prob of sentence so far: -2.1456682214670444
DUTCH: P(e|hat) = -1.235528446907549 ==> log prob of sentence so far: -2.7918309476748364
PORTUGUESE: P(e|hat) = -0.838149180058929 ==> log prob of sentence so far: -2.369628097101184
SPANISH: P(e|hat) = -0.656417653650555 ==> log prob of sentence so far: -2.5806969397124364

4GRAM: atea
ENGLISH: P(a|ate) = -1.4073010692645567 ==> log prob of sentence so far: -4.965712292241244
FRENCH: P(a|ate) = -0.9376392559609837 ==> log prob of sentence so far: -3.0833074774280282
DUTCH: P(a|ate) = -2.4771212547196626 ==> log prob of sentence so far: -5.268952202394499
PORTUGUESE: P(a|ate) = -0.9289705059719211 ==> log prob of sentence so far: -3.2985986030731054
SPANISH: P(a|ate) = -1.3263358609287514 ==> log prob of sentence so far: -3.907032800641188

4GRAM: teai
ENGLISH: P(i|tea) = -2.278753600952829 ==> log prob of sentence so far: -7.244465893194073
FRENCH: P(i|tea) = -2.1631613749770184 ==> log prob of sentence so far: -5.246468852405046
DUTCH: P(i|tea) = -2.100370545117563 ==> log prob of sentence so far: -7.3693227475120615
PORTUGUESE: P(i|tea) = -1.4399396743370376 ==> log prob of sentence so far: -4.738538277410143
SPANISH: P(i|tea) = -1.9242792860618816 ==> log prob of sentence so far: -5.83131208670307

According to the 4gram model, the sentence is in Portuguese
----------------
