J'aime l'IA.

1GRAM MODEL:

1GRAM: j
ENGLISH: P(j) = -2.914447804032505 ==> log prob of sentence so far: -2.914447804032505
FRENCH: P(j) = -2.262454919901824 ==> log prob of sentence so far: -2.262454919901824

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -4.012772444804438
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -3.3331103994798603

1GRAM: i
ENGLISH: P(i) = -1.15170480747376 ==> log prob of sentence so far: -5.164477252278198
FRENCH: P(i) = -1.1290925204027853 ==> log prob of sentence so far: -4.462202919882646

1GRAM: m
ENGLISH: P(m) = -1.6019350435333317 ==> log prob of sentence so far: -6.76641229581153
FRENCH: P(m) = -1.4846148337842204 ==> log prob of sentence so far: -5.946817753666867

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -7.6683477332124
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -6.7523870306957665

1GRAM: l
ENGLISH: P(l) = -1.3835976742909466 ==> log prob of sentence so far: -9.051945407503347
FRENCH: P(l) = -1.2161248223608845 ==> log prob of sentence so far: -7.9685118530566506

1GRAM: i
ENGLISH: P(i) = -1.15170480747376 ==> log prob of sentence so far: -10.203650214977106
FRENCH: P(i) = -1.1290925204027853 ==> log prob of sentence so far: -9.097604373459436

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -11.30197485574904
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -10.168259853037473

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: ja
ENGLISH: P(a|j) = -0.9035900382641181 ==> log prob of sentence so far: -0.9035900382641181
FRENCH: P(a|j) = -0.7694446427941456 ==> log prob of sentence so far: -0.7694446427941456

2GRAM: ai
ENGLISH: P(i|a) = -1.3757177600860506 ==> log prob of sentence so far: -2.279307798350169
FRENCH: P(i|a) = -0.6635811794145366 ==> log prob of sentence so far: -1.4330258222086822

2GRAM: im
ENGLISH: P(m|i) = -1.3659132327876533 ==> log prob of sentence so far: -3.6452210311378224
FRENCH: P(m|i) = -1.535803189494064 ==> log prob of sentence so far: -2.9688290117027463

2GRAM: me
ENGLISH: P(e|m) = -0.601584537026081 ==> log prob of sentence so far: -4.246805568163904
FRENCH: P(e|m) = -0.5092479296567639 ==> log prob of sentence so far: -3.47807694135951

2GRAM: el
ENGLISH: P(l|e) = -1.331028550036677 ==> log prob of sentence so far: -5.577834118200581
FRENCH: P(l|e) = -1.0706208997726607 ==> log prob of sentence so far: -4.548697841132171

2GRAM: li
ENGLISH: P(i|l) = -0.8764964187294044 ==> log prob of sentence so far: -6.4543305369299855
FRENCH: P(i|l) = -1.1844972785622172 ==> log prob of sentence so far: -5.733195119694388

2GRAM: ia
ENGLISH: P(a|i) = -1.6479474280142195 ==> log prob of sentence so far: -8.102277964944205
FRENCH: P(a|i) = -1.745862786967327 ==> log prob of sentence so far: -7.4790579066617155

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: jai
ENGLISH: P(i|ja) = -1.7258233700283276 ==> log prob of sentence so far: -1.7258233700283276
FRENCH: P(i|ja) = -0.5105588951573813 ==> log prob of sentence so far: -0.5105588951573813

3GRAM: aim
ENGLISH: P(m|ai) = -1.663570090733335 ==> log prob of sentence so far: -3.3893934607616627
FRENCH: P(m|ai) = -1.6417913889186804 ==> log prob of sentence so far: -2.1523502840760615

3GRAM: ime
ENGLISH: P(e|im) = -0.6181367538419187 ==> log prob of sentence so far: -4.007530214603581
FRENCH: P(e|im) = -0.5030387157053924 ==> log prob of sentence so far: -2.655388999781454

3GRAM: mel
ENGLISH: P(l|me) = -1.5759050139465747 ==> log prob of sentence so far: -5.583435228550156
FRENCH: P(l|me) = -1.3761102246291044 ==> log prob of sentence so far: -4.031499224410558

3GRAM: eli
ENGLISH: P(i|el) = -0.7757844512976412 ==> log prob of sentence so far: -6.359219679847797
FRENCH: P(i|el) = -1.4072058855145395 ==> log prob of sentence so far: -5.438705109925097

3GRAM: lia
ENGLISH: P(a|li) = -1.3256973673624977 ==> log prob of sentence so far: -7.684917047210295
FRENCH: P(a|li) = -1.649153930578276 ==> log prob of sentence so far: -7.087859040503373

According to the 3gram model, the sentence is in French
----------------
4GRAM MODEL:

4GRAM: jaim
ENGLISH: P(m|jai) = -1.792391689498254 ==> log prob of sentence so far: -1.792391689498254
FRENCH: P(m|jai) = -0.968700484854734 ==> log prob of sentence so far: -0.968700484854734

4GRAM: aime
ENGLISH: P(e|aim) = -0.3428980990217387 ==> log prob of sentence so far: -2.1352897885199926
FRENCH: P(e|aim) = -0.3502033171368712 ==> log prob of sentence so far: -1.3189038019916053

4GRAM: imel
ENGLISH: P(l|ime) = -1.8961345804922205 ==> log prob of sentence so far: -4.031424369012213
FRENCH: P(l|ime) = -1.553294016323588 ==> log prob of sentence so far: -2.8721978183151933

4GRAM: meli
ENGLISH: P(i|mel) = -0.8890311646031475 ==> log prob of sentence so far: -4.92045553361536
FRENCH: P(i|mel) = -1.39737926684862 ==> log prob of sentence so far: -4.269577085163814

4GRAM: elia
ENGLISH: P(a|eli) = -1.7320329601994064 ==> log prob of sentence so far: -6.652488493814767
FRENCH: P(a|eli) = -1.8742332469694698 ==> log prob of sentence so far: -6.143810332133284

According to the 4gram model, the sentence is in French
----------------
