The Jules Verne, a gourmet Parisian restaurant, is closed.

1GRAM MODEL:

1GRAM: t
ENGLISH: P(t) = -1.0367203932049034 ==> log prob of sentence so far: -1.0367203932049034
FRENCH: P(t) = -1.1325263815326383 ==> log prob of sentence so far: -1.1325263815326383

1GRAM: h
ENGLISH: P(h) = -1.193712897320169 ==> log prob of sentence so far: -2.230433290525072
FRENCH: P(h) = -2.04605506268921 ==> log prob of sentence so far: -3.178581444221848

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -3.1323687279259427
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -3.984150721250748

1GRAM: j
ENGLISH: P(j) = -2.914447804032505 ==> log prob of sentence so far: -6.046816531958448
FRENCH: P(j) = -2.262454919901824 ==> log prob of sentence so far: -6.246605641152572

1GRAM: u
ENGLISH: P(u) = -1.564711299886533 ==> log prob of sentence so far: -7.611527831844981
FRENCH: P(u) = -1.1916704140550483 ==> log prob of sentence so far: -7.43827605520762

1GRAM: l
ENGLISH: P(l) = -1.3835976742909466 ==> log prob of sentence so far: -8.995125506135928
FRENCH: P(l) = -1.2161248223608845 ==> log prob of sentence so far: -8.654400877568504

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -9.897060943536799
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -9.459970154597404

1GRAM: s
ENGLISH: P(s) = -1.18237672796024 ==> log prob of sentence so far: -11.079437671497038
FRENCH: P(s) = -1.1050716113322194 ==> log prob of sentence so far: -10.565041765929623

1GRAM: v
ENGLISH: P(v) = -2.0119242164211473 ==> log prob of sentence so far: -13.091361887918186
FRENCH: P(v) = -1.763415903363995 ==> log prob of sentence so far: -12.328457669293618

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -13.993297325319057
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -13.134026946322518

1GRAM: r
ENGLISH: P(r) = -1.2212020525433098 ==> log prob of sentence so far: -15.214499377862367
FRENCH: P(r) = -1.166447418641506 ==> log prob of sentence so far: -14.300474364964023

1GRAM: n
ENGLISH: P(n) = -1.1599632276932161 ==> log prob of sentence so far: -16.374462605555582
FRENCH: P(n) = -1.1521751619158405 ==> log prob of sentence so far: -15.452649526879863

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -17.27639804295645
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -16.258218803908765

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -18.374722683728383
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -17.3288742834868

1GRAM: g
ENGLISH: P(g) = -1.7025038170885405 ==> log prob of sentence so far: -20.077226500816924
FRENCH: P(g) = -2.0154215775799997 ==> log prob of sentence so far: -19.3442958610668

1GRAM: o
ENGLISH: P(o) = -1.1241820463462655 ==> log prob of sentence so far: -21.20140854716319
FRENCH: P(o) = -1.265193765366532 ==> log prob of sentence so far: -20.609489626433334

1GRAM: u
ENGLISH: P(u) = -1.564711299886533 ==> log prob of sentence so far: -22.766119847049723
FRENCH: P(u) = -1.1916704140550483 ==> log prob of sentence so far: -21.801160040488384

1GRAM: r
ENGLISH: P(r) = -1.2212020525433098 ==> log prob of sentence so far: -23.98732189959303
FRENCH: P(r) = -1.166447418641506 ==> log prob of sentence so far: -22.96760745912989

1GRAM: m
ENGLISH: P(m) = -1.6019350435333317 ==> log prob of sentence so far: -25.58925694312636
FRENCH: P(m) = -1.4846148337842204 ==> log prob of sentence so far: -24.45222229291411

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -26.49119238052723
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -25.257791569943013

1GRAM: t
ENGLISH: P(t) = -1.0367203932049034 ==> log prob of sentence so far: -27.527912773732133
FRENCH: P(t) = -1.1325263815326383 ==> log prob of sentence so far: -26.39031795147565

1GRAM: p
ENGLISH: P(p) = -1.7499138411301887 ==> log prob of sentence so far: -29.27782661486232
FRENCH: P(p) = -1.560609685674401 ==> log prob of sentence so far: -27.950927637150052

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -30.37615125563425
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -29.021583116728088

1GRAM: r
ENGLISH: P(r) = -1.2212020525433098 ==> log prob of sentence so far: -31.59735330817756
FRENCH: P(r) = -1.166447418641506 ==> log prob of sentence so far: -30.188030535369595

1GRAM: i
ENGLISH: P(i) = -1.15170480747376 ==> log prob of sentence so far: -32.74905811565132
FRENCH: P(i) = -1.1290925204027853 ==> log prob of sentence so far: -31.31712305577238

1GRAM: s
ENGLISH: P(s) = -1.18237672796024 ==> log prob of sentence so far: -33.93143484361156
FRENCH: P(s) = -1.1050716113322194 ==> log prob of sentence so far: -32.4221946671046

1GRAM: i
ENGLISH: P(i) = -1.15170480747376 ==> log prob of sentence so far: -35.083139651085325
FRENCH: P(i) = -1.1290925204027853 ==> log prob of sentence so far: -33.551287187507384

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -36.18146429185726
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -34.62194266708542

1GRAM: n
ENGLISH: P(n) = -1.1599632276932161 ==> log prob of sentence so far: -37.34142751955048
FRENCH: P(n) = -1.1521751619158405 ==> log prob of sentence so far: -35.77411782900126

1GRAM: r
ENGLISH: P(r) = -1.2212020525433098 ==> log prob of sentence so far: -38.56262957209379
FRENCH: P(r) = -1.166447418641506 ==> log prob of sentence so far: -36.940565247642766

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -39.46456500949466
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -37.74613452467167

1GRAM: s
ENGLISH: P(s) = -1.18237672796024 ==> log prob of sentence so far: -40.6469417374549
FRENCH: P(s) = -1.1050716113322194 ==> log prob of sentence so far: -38.85120613600389

1GRAM: t
ENGLISH: P(t) = -1.0367203932049034 ==> log prob of sentence so far: -41.683662130659805
FRENCH: P(t) = -1.1325263815326383 ==> log prob of sentence so far: -39.983732517536524

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -42.78198677143174
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -41.05438799711456

1GRAM: u
ENGLISH: P(u) = -1.564711299886533 ==> log prob of sentence so far: -44.34669807131827
FRENCH: P(u) = -1.1916704140550483 ==> log prob of sentence so far: -42.24605841116961

1GRAM: r
ENGLISH: P(r) = -1.2212020525433098 ==> log prob of sentence so far: -45.567900123861584
FRENCH: P(r) = -1.166447418641506 ==> log prob of sentence so far: -43.41250582981111

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -46.66622476463352
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -44.483161309389146

1GRAM: n
ENGLISH: P(n) = -1.1599632276932161 ==> log prob of sentence so far: -47.82618799232674
FRENCH: P(n) = -1.1521751619158405 ==> log prob of sentence so far: -45.63533647130499

1GRAM: t
ENGLISH: P(t) = -1.0367203932049034 ==> log prob of sentence so far: -48.86290838553164
FRENCH: P(t) = -1.1325263815326383 ==> log prob of sentence so far: -46.767862852837624

1GRAM: i
ENGLISH: P(i) = -1.15170480747376 ==> log prob of sentence so far: -50.0146131930054
FRENCH: P(i) = -1.1290925204027853 ==> log prob of sentence so far: -47.89695537324041

1GRAM: s
ENGLISH: P(s) = -1.18237672796024 ==> log prob of sentence so far: -51.19698992096564
FRENCH: P(s) = -1.1050716113322194 ==> log prob of sentence so far: -49.00202698457263

1GRAM: c
ENGLISH: P(c) = -1.5877747753207887 ==> log prob of sentence so far: -52.78476469628643
FRENCH: P(c) = -1.4990816758627588 ==> log prob of sentence so far: -50.50110866043539

1GRAM: l
ENGLISH: P(l) = -1.3835976742909466 ==> log prob of sentence so far: -54.16836237057738
FRENCH: P(l) = -1.2161248223608845 ==> log prob of sentence so far: -51.717233482796274

1GRAM: o
ENGLISH: P(o) = -1.1241820463462655 ==> log prob of sentence so far: -55.29254441692364
FRENCH: P(o) = -1.265193765366532 ==> log prob of sentence so far: -52.98242724816281

1GRAM: s
ENGLISH: P(s) = -1.18237672796024 ==> log prob of sentence so far: -56.47492114488388
FRENCH: P(s) = -1.1050716113322194 ==> log prob of sentence so far: -54.08749885949503

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -57.37685658228475
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -54.89306813652393

1GRAM: d
ENGLISH: P(d) = -1.3852685001126093 ==> log prob of sentence so far: -58.76212508239736
FRENCH: P(d) = -1.4087138708407954 ==> log prob of sentence so far: -56.30178200736472

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: th
ENGLISH: P(h|t) = -0.4293566299049926 ==> log prob of sentence so far: -0.4293566299049926
FRENCH: P(h|t) = -2.129577816203349 ==> log prob of sentence so far: -2.129577816203349

2GRAM: he
ENGLISH: P(e|h) = -0.3399756421121579 ==> log prob of sentence so far: -0.7693322720171505
FRENCH: P(e|h) = -0.4941780700303355 ==> log prob of sentence so far: -2.6237558862336847

2GRAM: ej
ENGLISH: P(j|e) = -2.826969055858279 ==> log prob of sentence so far: -3.5963013278754294
FRENCH: P(j|e) = -2.0271783979809737 ==> log prob of sentence so far: -4.650934284214658

2GRAM: ju
ENGLISH: P(u|j) = -0.5162293765776274 ==> log prob of sentence so far: -4.112530704453057
FRENCH: P(u|j) = -1.0708228899657988 ==> log prob of sentence so far: -5.721757174180457

2GRAM: ul
ENGLISH: P(l|u) = -1.0022752382409368 ==> log prob of sentence so far: -5.1148059426939945
FRENCH: P(l|u) = -1.4381664024092558 ==> log prob of sentence so far: -7.159923576589713

2GRAM: le
ENGLISH: P(e|l) = -0.7501906362795048 ==> log prob of sentence so far: -5.864996578973499
FRENCH: P(e|l) = -0.4367629830025041 ==> log prob of sentence so far: -7.596686559592217

2GRAM: es
ENGLISH: P(s|e) = -0.9708855148194279 ==> log prob of sentence so far: -6.835882093792927
FRENCH: P(s|e) = -0.7615789483781602 ==> log prob of sentence so far: -8.358265507970378

2GRAM: sv
ENGLISH: P(v|s) = -2.5230332212474056 ==> log prob of sentence so far: -9.358915315040333
FRENCH: P(v|s) = -1.8864913408458885 ==> log prob of sentence so far: -10.244756848816266

2GRAM: ve
ENGLISH: P(e|v) = -0.16135495627116633 ==> log prob of sentence so far: -9.5202702713115
FRENCH: P(e|v) = -0.5527765138333302 ==> log prob of sentence so far: -10.797533362649595

2GRAM: er
ENGLISH: P(r|e) = -0.8479237637379716 ==> log prob of sentence so far: -10.368194035049472
FRENCH: P(r|e) = -1.0862077157823018 ==> log prob of sentence so far: -11.883741078431896

2GRAM: rn
ENGLISH: P(n|r) = -1.5814899652307488 ==> log prob of sentence so far: -11.94968400028022
FRENCH: P(n|r) = -1.7644500240467593 ==> log prob of sentence so far: -13.648191102478656

2GRAM: ne
ENGLISH: P(e|n) = -1.046654141607186 ==> log prob of sentence so far: -12.996338141887406
FRENCH: P(e|n) = -0.808835575276345 ==> log prob of sentence so far: -14.457026677755

2GRAM: ea
ENGLISH: P(a|e) = -1.0603428673246011 ==> log prob of sentence so far: -14.056681009212008
FRENCH: P(a|e) = -1.583214626167185 ==> log prob of sentence so far: -16.040241303922183

2GRAM: ag
ENGLISH: P(g|a) = -1.6436740026518448 ==> log prob of sentence so far: -15.700355011863852
FRENCH: P(g|a) = -1.6174241817975408 ==> log prob of sentence so far: -17.657665485719725

2GRAM: go
ENGLISH: P(o|g) = -1.0127820731005206 ==> log prob of sentence so far: -16.713137084964373
FRENCH: P(o|g) = -1.3199356078638016 ==> log prob of sentence so far: -18.977601093583527

2GRAM: ou
ENGLISH: P(u|o) = -0.9271079128131381 ==> log prob of sentence so far: -17.64024499777751
FRENCH: P(u|o) = -0.5680742132998424 ==> log prob of sentence so far: -19.545675306883368

2GRAM: ur
ENGLISH: P(r|u) = -0.8252199232908146 ==> log prob of sentence so far: -18.465464921068325
FRENCH: P(r|u) = -0.7566351172487572 ==> log prob of sentence so far: -20.302310424132124

2GRAM: rm
ENGLISH: P(m|r) = -1.5810947927904688 ==> log prob of sentence so far: -20.046559713858795
FRENCH: P(m|r) = -1.5580583996055475 ==> log prob of sentence so far: -21.86036882373767

2GRAM: me
ENGLISH: P(e|m) = -0.601584537026081 ==> log prob of sentence so far: -20.648144250884876
FRENCH: P(e|m) = -0.5092479296567639 ==> log prob of sentence so far: -22.369616753394432

2GRAM: et
ENGLISH: P(t|e) = -1.1837603181589333 ==> log prob of sentence so far: -21.831904569043807
FRENCH: P(t|e) = -1.0909072673569926 ==> log prob of sentence so far: -23.460524020751425

2GRAM: tp
ENGLISH: P(p|t) = -2.2600367871624196 ==> log prob of sentence so far: -24.091941356206227
FRENCH: P(p|t) = -1.4101258704495545 ==> log prob of sentence so far: -24.87064989120098

2GRAM: pa
ENGLISH: P(a|p) = -0.8963642632397977 ==> log prob of sentence so far: -24.988305619446024
FRENCH: P(a|p) = -0.6323653033480962 ==> log prob of sentence so far: -25.503015194549075

2GRAM: ar
ENGLISH: P(r|a) = -0.9674329311729756 ==> log prob of sentence so far: -25.955738550619
FRENCH: P(r|a) = -1.0010287410510506 ==> log prob of sentence so far: -26.504043935600127

2GRAM: ri
ENGLISH: P(i|r) = -0.9900296230966597 ==> log prob of sentence so far: -26.94576817371566
FRENCH: P(i|r) = -1.0397474629671202 ==> log prob of sentence so far: -27.543791398567247

2GRAM: is
ENGLISH: P(s|i) = -0.8483697724216743 ==> log prob of sentence so far: -27.794137946137333
FRENCH: P(s|i) = -0.8646548331837887 ==> log prob of sentence so far: -28.408446231751036

2GRAM: si
ENGLISH: P(i|s) = -1.0968382595122879 ==> log prob of sentence so far: -28.890976205649622
FRENCH: P(i|s) = -1.183237983775244 ==> log prob of sentence so far: -29.59168421552628

2GRAM: ia
ENGLISH: P(a|i) = -1.6479474280142195 ==> log prob of sentence so far: -30.538923633663842
FRENCH: P(a|i) = -1.745862786967327 ==> log prob of sentence so far: -31.337547002493604

2GRAM: an
ENGLISH: P(n|a) = -0.6677226397364787 ==> log prob of sentence so far: -31.20664627340032
FRENCH: P(n|a) = -0.8456621170358352 ==> log prob of sentence so far: -32.18320911952944

2GRAM: nr
ENGLISH: P(r|n) = -2.295412869611593 ==> log prob of sentence so far: -33.50205914301191
FRENCH: P(r|n) = -1.9344757483073973 ==> log prob of sentence so far: -34.117684867836836

2GRAM: re
ENGLISH: P(e|r) = -0.6224130940258753 ==> log prob of sentence so far: -34.124472237037786
FRENCH: P(e|r) = -0.5676012830549572 ==> log prob of sentence so far: -34.68528615089179

2GRAM: es
ENGLISH: P(s|e) = -0.9708855148194279 ==> log prob of sentence so far: -35.095357751857215
FRENCH: P(s|e) = -0.7615789483781602 ==> log prob of sentence so far: -35.44686509926995

2GRAM: st
ENGLISH: P(t|s) = -0.7279411329912379 ==> log prob of sentence so far: -35.823298884848455
FRENCH: P(t|s) = -1.135607810562159 ==> log prob of sentence so far: -36.582472909832106

2GRAM: ta
ENGLISH: P(a|t) = -1.2198154971362172 ==> log prob of sentence so far: -37.04311438198467
FRENCH: P(a|t) = -0.9213767405614084 ==> log prob of sentence so far: -37.50384965039351

2GRAM: au
ENGLISH: P(u|a) = -1.8825247515358006 ==> log prob of sentence so far: -38.92563913352047
FRENCH: P(u|a) = -1.0925732705547586 ==> log prob of sentence so far: -38.59642292094827

2GRAM: ur
ENGLISH: P(r|u) = -0.8252199232908146 ==> log prob of sentence so far: -39.75085905681129
FRENCH: P(r|u) = -0.7566351172487572 ==> log prob of sentence so far: -39.35305803819703

2GRAM: ra
ENGLISH: P(a|r) = -1.0036435158475256 ==> log prob of sentence so far: -40.754502572658815
FRENCH: P(a|r) = -0.9156039180500228 ==> log prob of sentence so far: -40.26866195624705

2GRAM: an
ENGLISH: P(n|a) = -0.6677226397364787 ==> log prob of sentence so far: -41.4222252123953
FRENCH: P(n|a) = -0.8456621170358352 ==> log prob of sentence so far: -41.11432407328289

2GRAM: nt
ENGLISH: P(t|n) = -0.8170158624991728 ==> log prob of sentence so far: -42.23924107489447
FRENCH: P(t|n) = -0.6237272622949059 ==> log prob of sentence so far: -41.738051335577794

2GRAM: ti
ENGLISH: P(i|t) = -1.0226063494679998 ==> log prob of sentence so far: -43.26184742436247
FRENCH: P(i|t) = -1.0614782867270083 ==> log prob of sentence so far: -42.7995296223048

2GRAM: is
ENGLISH: P(s|i) = -0.8483697724216743 ==> log prob of sentence so far: -44.11021719678415
FRENCH: P(s|i) = -0.8646548331837887 ==> log prob of sentence so far: -43.664184455488595

2GRAM: sc
ENGLISH: P(c|s) = -1.5120777193063237 ==> log prob of sentence so far: -45.622294916090475
FRENCH: P(c|s) = -1.336056716674345 ==> log prob of sentence so far: -45.00024117216294

2GRAM: cl
ENGLISH: P(l|c) = -1.4045697792515979 ==> log prob of sentence so far: -47.02686469534207
FRENCH: P(l|c) = -1.4740697573972146 ==> log prob of sentence so far: -46.474310929560154

2GRAM: lo
ENGLISH: P(o|l) = -1.0221490156636956 ==> log prob of sentence so far: -48.04901371100576
FRENCH: P(o|l) = -1.2420322029320794 ==> log prob of sentence so far: -47.716343132492234

2GRAM: os
ENGLISH: P(s|o) = -1.404921656688375 ==> log prob of sentence so far: -49.453935367694136
FRENCH: P(s|o) = -1.488521791495811 ==> log prob of sentence so far: -49.20486492398805

2GRAM: se
ENGLISH: P(e|s) = -0.9168568750906216 ==> log prob of sentence so far: -50.37079224278476
FRENCH: P(e|s) = -0.8072903423783238 ==> log prob of sentence so far: -50.01215526636637

2GRAM: ed
ENGLISH: P(d|e) = -1.0490469043044388 ==> log prob of sentence so far: -51.4198391470892
FRENCH: P(d|e) = -1.2555332046664434 ==> log prob of sentence so far: -51.26768847103281

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: the
ENGLISH: P(e|th) = -0.18858119624597447 ==> log prob of sentence so far: -0.18858119624597447
FRENCH: P(e|th) = -0.644504237488809 ==> log prob of sentence so far: -0.644504237488809

3GRAM: hej
ENGLISH: P(j|he) = -2.5807458675917303 ==> log prob of sentence so far: -2.7693270638377046
FRENCH: P(j|he) = -2.610954833110392 ==> log prob of sentence so far: -3.255459070599201

3GRAM: eju
ENGLISH: P(u|ej) = -0.4523287108747066 ==> log prob of sentence so far: -3.2216557747124113
FRENCH: P(u|ej) = -1.1349355856950862 ==> log prob of sentence so far: -4.390394656294287

3GRAM: jul
ENGLISH: P(l|ju) = -1.079362617727815 ==> log prob of sentence so far: -4.301018392440226
FRENCH: P(l|ju) = -1.60484012096205 ==> log prob of sentence so far: -5.9952347772563375

3GRAM: ule
ENGLISH: P(e|ul) = -1.386588963591785 ==> log prob of sentence so far: -5.687607356032011
FRENCH: P(e|ul) = -0.4068033658691363 ==> log prob of sentence so far: -6.402038143125473

3GRAM: les
ENGLISH: P(s|le) = -0.7995964748637631 ==> log prob of sentence so far: -6.487203830895774
FRENCH: P(s|le) = -0.5134755028221442 ==> log prob of sentence so far: -6.915513645947618

3GRAM: esv
ENGLISH: P(v|es) = -2.953937060041054 ==> log prob of sentence so far: -9.441140890936827
FRENCH: P(v|es) = -1.7768756889781565 ==> log prob of sentence so far: -8.692389334925775

3GRAM: sve
ENGLISH: P(e|sv) = -0.3777325873084936 ==> log prob of sentence so far: -9.81887347824532
FRENCH: P(e|sv) = -0.7104215344865253 ==> log prob of sentence so far: -9.4028108694123

3GRAM: ver
ENGLISH: P(r|ve) = -0.4318763069826047 ==> log prob of sentence so far: -10.250749785227926
FRENCH: P(r|ve) = -0.596222715260656 ==> log prob of sentence so far: -9.999033584672954

3GRAM: ern
ENGLISH: P(n|er) = -1.5444319378087452 ==> log prob of sentence so far: -11.795181723036672
FRENCH: P(n|er) = -1.4280357026054853 ==> log prob of sentence so far: -11.427069287278439

3GRAM: rne
ENGLISH: P(e|rn) = -0.5812284558883876 ==> log prob of sentence so far: -12.37641017892506
FRENCH: P(e|rn) = -0.5235728477473307 ==> log prob of sentence so far: -11.95064213502577

3GRAM: nea
ENGLISH: P(a|ne) = -1.1185652593989304 ==> log prob of sentence so far: -13.49497543832399
FRENCH: P(a|ne) = -1.4332415453747895 ==> log prob of sentence so far: -13.38388368040056

3GRAM: eag
ENGLISH: P(g|ea) = -1.7209019692765786 ==> log prob of sentence so far: -15.21587740760057
FRENCH: P(g|ea) = -2.1212573586456447 ==> log prob of sentence so far: -15.505141039046205

3GRAM: ago
ENGLISH: P(o|ag) = -1.0717502060706587 ==> log prob of sentence so far: -16.287627613671226
FRENCH: P(o|ag) = -1.2174725694419173 ==> log prob of sentence so far: -16.72261360848812

3GRAM: gou
ENGLISH: P(u|go) = -1.4667479203912017 ==> log prob of sentence so far: -17.75437553406243
FRENCH: P(u|go) = -0.5841625990569661 ==> log prob of sentence so far: -17.306776207545088

3GRAM: our
ENGLISH: P(r|ou) = -0.8059778861676324 ==> log prob of sentence so far: -18.56035342023006
FRENCH: P(r|ou) = -0.5426867880816143 ==> log prob of sentence so far: -17.849462995626702

3GRAM: urm
ENGLISH: P(m|ur) = -1.8338917726530104 ==> log prob of sentence so far: -20.394245192883073
FRENCH: P(m|ur) = -1.510433115241722 ==> log prob of sentence so far: -19.359896110868423

3GRAM: rme
ENGLISH: P(e|rm) = -0.7311685168700952 ==> log prob of sentence so far: -21.12541370975317
FRENCH: P(e|rm) = -0.4344730482757357 ==> log prob of sentence so far: -19.794369159144157

3GRAM: met
ENGLISH: P(t|me) = -0.9135460881286742 ==> log prob of sentence so far: -22.03895979788184
FRENCH: P(t|me) = -1.2204994694298827 ==> log prob of sentence so far: -21.01486862857404

3GRAM: etp
ENGLISH: P(p|et) = -2.6216976333294957 ==> log prob of sentence so far: -24.660657431211337
FRENCH: P(p|et) = -1.449833146401849 ==> log prob of sentence so far: -22.46470177497589

3GRAM: tpa
ENGLISH: P(a|tp) = -0.7209952891922982 ==> log prob of sentence so far: -25.381652720403636
FRENCH: P(a|tp) = -0.38844436681324274 ==> log prob of sentence so far: -22.853146141789132

3GRAM: par
ENGLISH: P(r|pa) = -0.4947802498939526 ==> log prob of sentence so far: -25.87643297029759
FRENCH: P(r|pa) = -0.2962539345023763 ==> log prob of sentence so far: -23.14940007629151

3GRAM: ari
ENGLISH: P(i|ar) = -1.1929700640607526 ==> log prob of sentence so far: -27.06940303435834
FRENCH: P(i|ar) = -1.1149931850386083 ==> log prob of sentence so far: -24.264393261330117

3GRAM: ris
ENGLISH: P(s|ri) = -1.0346804457574805 ==> log prob of sentence so far: -28.104083480115822
FRENCH: P(s|ri) = -0.8427194967736888 ==> log prob of sentence so far: -25.107112758103806

3GRAM: isi
ENGLISH: P(i|is) = -1.253826239706846 ==> log prob of sentence so far: -29.35790971982267
FRENCH: P(i|is) = -1.1832614746564596 ==> log prob of sentence so far: -26.290374232760264

3GRAM: sia
ENGLISH: P(a|si) = -1.6835360259778918 ==> log prob of sentence so far: -31.04144574580056
FRENCH: P(a|si) = -1.9256774877670102 ==> log prob of sentence so far: -28.216051720527275

3GRAM: ian
ENGLISH: P(n|ia) = -0.5465271084872243 ==> log prob of sentence so far: -31.587972854287784
FRENCH: P(n|ia) = -0.8143419872094309 ==> log prob of sentence so far: -29.030393707736707

3GRAM: anr
ENGLISH: P(r|an) = -2.624138741332691 ==> log prob of sentence so far: -34.21211159562048
FRENCH: P(r|an) = -3.311391637479936 ==> log prob of sentence so far: -32.34178534521664

3GRAM: nre
ENGLISH: P(e|nr) = -0.3361373868949074 ==> log prob of sentence so far: -34.54824898251538
FRENCH: P(e|nr) = -0.38750660017343314 ==> log prob of sentence so far: -32.729291945390074

3GRAM: res
ENGLISH: P(s|re) = -0.8636598570858126 ==> log prob of sentence so far: -35.411908839601196
FRENCH: P(s|re) = -0.7379153756172324 ==> log prob of sentence so far: -33.46720732100731

3GRAM: est
ENGLISH: P(t|es) = -0.6859702843095423 ==> log prob of sentence so far: -36.09787912391074
FRENCH: P(t|es) = -0.9320675866897584 ==> log prob of sentence so far: -34.39927490769707

3GRAM: sta
ENGLISH: P(a|st) = -0.9006206065673983 ==> log prob of sentence so far: -36.99849973047814
FRENCH: P(a|st) = -0.6873224850951417 ==> log prob of sentence so far: -35.086597392792214

3GRAM: tau
ENGLISH: P(u|ta) = -2.172150776580883 ==> log prob of sentence so far: -39.17065050705902
FRENCH: P(u|ta) = -1.0164832567475068 ==> log prob of sentence so far: -36.10308064953972

3GRAM: aur
ENGLISH: P(r|au) = -1.5381949449296213 ==> log prob of sentence so far: -40.708845451988644
FRENCH: P(r|au) = -1.1837103479187698 ==> log prob of sentence so far: -37.28679099745849

3GRAM: ura
ENGLISH: P(a|ur) = -1.2276370642934207 ==> log prob of sentence so far: -41.93648251628206
FRENCH: P(a|ur) = -1.1007468958273718 ==> log prob of sentence so far: -38.38753789328586

3GRAM: ran
ENGLISH: P(n|ra) = -0.6323989344986319 ==> log prob of sentence so far: -42.568881450780694
FRENCH: P(n|ra) = -0.6805690341265668 ==> log prob of sentence so far: -39.06810692741243

3GRAM: ant
ENGLISH: P(t|an) = -1.1199193279325703 ==> log prob of sentence so far: -43.688800778713265
FRENCH: P(t|an) = -0.4639872551814802 ==> log prob of sentence so far: -39.53209418259391

3GRAM: nti
ENGLISH: P(i|nt) = -1.0476648681670464 ==> log prob of sentence so far: -44.73646564688031
FRENCH: P(i|nt) = -1.1847753711704685 ==> log prob of sentence so far: -40.716869553764376

3GRAM: tis
ENGLISH: P(s|ti) = -0.9950314346243462 ==> log prob of sentence so far: -45.731497081504656
FRENCH: P(s|ti) = -1.5952083155897716 ==> log prob of sentence so far: -42.31207786935415

3GRAM: isc
ENGLISH: P(c|is) = -1.2735673058016164 ==> log prob of sentence so far: -47.00506438730627
FRENCH: P(c|is) = -1.352515018467637 ==> log prob of sentence so far: -43.66459288782178

3GRAM: scl
ENGLISH: P(l|sc) = -1.3866500431830258 ==> log prob of sentence so far: -48.391714430489294
FRENCH: P(l|sc) = -1.6101376169031723 ==> log prob of sentence so far: -45.27473050472496

3GRAM: clo
ENGLISH: P(o|cl) = -0.6367666186737095 ==> log prob of sentence so far: -49.028481049163005
FRENCH: P(o|cl) = -1.2550115892156726 ==> log prob of sentence so far: -46.52974209394063

3GRAM: los
ENGLISH: P(s|lo) = -1.1085191172811018 ==> log prob of sentence so far: -50.13700016644411
FRENCH: P(s|lo) = -1.7762274621575034 ==> log prob of sentence so far: -48.30596955609813

3GRAM: ose
ENGLISH: P(e|os) = -0.4754199750437142 ==> log prob of sentence so far: -50.612420141487824
FRENCH: P(e|os) = -0.39209096969594687 ==> log prob of sentence so far: -48.69806052579408

3GRAM: sed
ENGLISH: P(d|se) = -1.0160875136886882 ==> log prob of sentence so far: -51.62850765517651
FRENCH: P(d|se) = -1.2503307650341635 ==> log prob of sentence so far: -49.94839129082824

According to the 3gram model, the sentence is in French
----------------
4GRAM MODEL:

4GRAM: thej
ENGLISH: P(j|the) = -2.4961895700789314 ==> log prob of sentence so far: -2.4961895700789314
FRENCH: P(j|the) = -2.010145705357102 ==> log prob of sentence so far: -2.010145705357102

4GRAM: heju
ENGLISH: P(u|hej) = -0.5069863213599033 ==> log prob of sentence so far: -3.0031758914388345
FRENCH: P(u|hej) = -0.9128498242810998 ==> log prob of sentence so far: -2.922995529638202

4GRAM: ejul
ENGLISH: P(l|eju) = -1.6496422513038849 ==> log prob of sentence so far: -4.652818142742719
FRENCH: P(l|eju) = -1.3287698635955094 ==> log prob of sentence so far: -4.251765393233711

4GRAM: jule
ENGLISH: P(e|jul) = -0.7297387653022912 ==> log prob of sentence so far: -5.38255690804501
FRENCH: P(e|jul) = -0.4550069404290901 ==> log prob of sentence so far: -4.706772333662801

4GRAM: ules
ENGLISH: P(s|ule) = -0.655610611982923 ==> log prob of sentence so far: -6.038167520027933
FRENCH: P(s|ule) = -0.7537651901002453 ==> log prob of sentence so far: -5.460537523763047

4GRAM: lesv
ENGLISH: P(v|les) = -2.5634810853944106 ==> log prob of sentence so far: -8.601648605422344
FRENCH: P(v|les) = -1.5825910794243778 ==> log prob of sentence so far: -7.043128603187425

4GRAM: esve
ENGLISH: P(e|esv) = -0.530177984021837 ==> log prob of sentence so far: -9.13182658944418
FRENCH: P(e|esv) = -0.6617510957929942 ==> log prob of sentence so far: -7.7048796989804185

4GRAM: sver
ENGLISH: P(r|sve) = -0.0894122514801848 ==> log prob of sentence so far: -9.221238840924366
FRENCH: P(r|sve) = -0.25560781217045847 ==> log prob of sentence so far: -7.960487511150877

4GRAM: vern
ENGLISH: P(n|ver) = -1.4331083037407453 ==> log prob of sentence so far: -10.654347144665111
FRENCH: P(n|ver) = -1.2802593574324803 ==> log prob of sentence so far: -9.240746868583358

4GRAM: erne
ENGLISH: P(e|ern) = -0.7678454740132886 ==> log prob of sentence so far: -11.4221926186784
FRENCH: P(e|ern) = -0.5967261233709261 ==> log prob of sentence so far: -9.837472991954284

4GRAM: rnea
ENGLISH: P(a|rne) = -1.4190198723169534 ==> log prob of sentence so far: -12.841212490995353
FRENCH: P(a|rne) = -1.6190933306267428 ==> log prob of sentence so far: -11.456566322581027

4GRAM: neag
ENGLISH: P(g|nea) = -1.8149819767682984 ==> log prob of sentence so far: -14.656194467763651
FRENCH: P(g|nea) = -1.748434155934241 ==> log prob of sentence so far: -13.205000478515268

4GRAM: eago
ENGLISH: P(o|eag) = -1.0275779768219473 ==> log prob of sentence so far: -15.683772444585598
FRENCH: P(o|eag) = -1.2524246622304798 ==> log prob of sentence so far: -14.457425140745748

4GRAM: agou
ENGLISH: P(u|ago) = -2.328152995303201 ==> log prob of sentence so far: -18.0119254398888
FRENCH: P(u|ago) = -1.1424630815918326 ==> log prob of sentence so far: -15.599888222337581

4GRAM: gour
ENGLISH: P(r|gou) = -0.7637420549110121 ==> log prob of sentence so far: -18.775667494799812
FRENCH: P(r|gou) = -0.7282593804380063 ==> log prob of sentence so far: -16.328147602775587

4GRAM: ourm
ENGLISH: P(m|our) = -1.5656999449763758 ==> log prob of sentence so far: -20.34136743977619
FRENCH: P(m|our) = -1.4094266694047775 ==> log prob of sentence so far: -17.737574272180364

4GRAM: urme
ENGLISH: P(e|urm) = -0.9382272936908234 ==> log prob of sentence so far: -21.27959473346701
FRENCH: P(e|urm) = -0.7310906119974797 ==> log prob of sentence so far: -18.468664884177844

4GRAM: rmet
ENGLISH: P(t|rme) = -1.2401268432264232 ==> log prob of sentence so far: -22.519721576693435
FRENCH: P(t|rme) = -0.9139123111269791 ==> log prob of sentence so far: -19.382577195304822

4GRAM: metp
ENGLISH: P(p|met) = -3.1148015478516866 ==> log prob of sentence so far: -25.634523124545122
FRENCH: P(p|met) = -2.195662484214442 ==> log prob of sentence so far: -21.578239679519264

4GRAM: etpa
ENGLISH: P(a|etp) = -0.7881354712902445 ==> log prob of sentence so far: -26.422658595835365
FRENCH: P(a|etp) = -0.6529373129031331 ==> log prob of sentence so far: -22.231176992422398

4GRAM: tpar
ENGLISH: P(r|tpa) = -0.4044895095381627 ==> log prob of sentence so far: -26.827148105373528
FRENCH: P(r|tpa) = -0.4611213380658132 ==> log prob of sentence so far: -22.69229833048821

4GRAM: pari
ENGLISH: P(i|par) = -1.0784295461941817 ==> log prob of sentence so far: -27.90557765156771
FRENCH: P(i|par) = -1.1893386767163854 ==> log prob of sentence so far: -23.881637007204596

4GRAM: aris
ENGLISH: P(s|ari) = -0.7755293697397054 ==> log prob of sentence so far: -28.681107021307415
FRENCH: P(s|ari) = -0.4785145180769012 ==> log prob of sentence so far: -24.3601515252815

4GRAM: risi
ENGLISH: P(i|ris) = -0.998506294376536 ==> log prob of sentence so far: -29.67961331568395
FRENCH: P(i|ris) = -1.5641602012198976 ==> log prob of sentence so far: -25.924311726501397

4GRAM: isia
ENGLISH: P(a|isi) = -1.9000135079033058 ==> log prob of sentence so far: -31.579626823587258
FRENCH: P(a|isi) = -2.660865478003869 ==> log prob of sentence so far: -28.585177204505268

4GRAM: sian
ENGLISH: P(n|sia) = -0.490146056180826 ==> log prob of sentence so far: -32.06977287976808
FRENCH: P(n|sia) = -1.2568263331719278 ==> log prob of sentence so far: -29.842003537677197

4GRAM: ianr
ENGLISH: P(r|ian) = -1.894348174369314 ==> log prob of sentence so far: -33.964121054137394
FRENCH: P(r|ian) = -2.803457115648414 ==> log prob of sentence so far: -32.645460653325614

4GRAM: anre
ENGLISH: P(e|anr) = -0.3544164436826661 ==> log prob of sentence so far: -34.31853749782006
FRENCH: P(e|anr) = -0.33488826292494855 ==> log prob of sentence so far: -32.98034891625056

4GRAM: nres
ENGLISH: P(s|nre) = -0.9595713429443918 ==> log prob of sentence so far: -35.27810884076445
FRENCH: P(s|nre) = -1.0534276951965749 ==> log prob of sentence so far: -34.03377661144714

4GRAM: rest
ENGLISH: P(t|res) = -0.6816655821243799 ==> log prob of sentence so far: -35.959774422888835
FRENCH: P(t|res) = -0.978195517397559 ==> log prob of sentence so far: -35.0119721288447

4GRAM: esta
ENGLISH: P(a|est) = -0.913459616549317 ==> log prob of sentence so far: -36.87323403943815
FRENCH: P(a|est) = -0.7718284704610405 ==> log prob of sentence so far: -35.783800599305735

4GRAM: stau
ENGLISH: P(u|sta) = -2.3071903043688 ==> log prob of sentence so far: -39.180424343806955
FRENCH: P(u|sta) = -1.4206867259335576 ==> log prob of sentence so far: -37.20448732523929

4GRAM: taur
ENGLISH: P(r|tau) = -1.171682140150626 ==> log prob of sentence so far: -40.352106483957584
FRENCH: P(r|tau) = -1.2199099317449698 ==> log prob of sentence so far: -38.424397256984264

4GRAM: aura
ENGLISH: P(a|aur) = -1.0217192496932364 ==> log prob of sentence so far: -41.37382573365082
FRENCH: P(a|aur) = -0.32313957858821485 ==> log prob of sentence so far: -38.747536835572475

4GRAM: uran
ENGLISH: P(n|ura) = -0.9111647518000661 ==> log prob of sentence so far: -42.284990485450884
FRENCH: P(n|ura) = -0.9388709664032882 ==> log prob of sentence so far: -39.68640780197576

4GRAM: rant
ENGLISH: P(t|ran) = -1.1597960417459883 ==> log prob of sentence so far: -43.444786527196875
FRENCH: P(t|ran) = -0.8275659792466211 ==> log prob of sentence so far: -40.51397378122238

4GRAM: anti
ENGLISH: P(i|ant) = -0.8253068436692417 ==> log prob of sentence so far: -44.270093370866114
FRENCH: P(i|ant) = -1.3797086275572477 ==> log prob of sentence so far: -41.89368240877963

4GRAM: ntis
ENGLISH: P(s|nti) = -1.3771409515281006 ==> log prob of sentence so far: -45.64723432239421
FRENCH: P(s|nti) = -1.5814197016648917 ==> log prob of sentence so far: -43.475102110444524

4GRAM: tisc
ENGLISH: P(c|tis) = -1.5743424784052449 ==> log prob of sentence so far: -47.221576800799454
FRENCH: P(c|tis) = -1.8397473216301556 ==> log prob of sentence so far: -45.31484943207468

4GRAM: iscl
ENGLISH: P(l|isc) = -1.2281080661127135 ==> log prob of sentence so far: -48.449684866912165
FRENCH: P(l|isc) = -1.9955281859775917 ==> log prob of sentence so far: -47.31037761805227

4GRAM: sclo
ENGLISH: P(o|scl) = -0.4817125894579813 ==> log prob of sentence so far: -48.93139745637015
FRENCH: P(o|scl) = -0.8865434340119062 ==> log prob of sentence so far: -48.19692105206418

4GRAM: clos
ENGLISH: P(s|clo) = -0.34210808864481146 ==> log prob of sentence so far: -49.27350554501496
FRENCH: P(s|clo) = -0.7505907688577651 ==> log prob of sentence so far: -48.94751182092194

4GRAM: lose
ENGLISH: P(e|los) = -0.3924316197027133 ==> log prob of sentence so far: -49.665937164717676
FRENCH: P(e|los) = -0.7987980203612877 ==> log prob of sentence so far: -49.74630984128323

4GRAM: osed
ENGLISH: P(d|ose) = -0.9379261606994297 ==> log prob of sentence so far: -50.6038633254171
FRENCH: P(d|ose) = -1.1368596456274576 ==> log prob of sentence so far: -50.88316948691069

According to the 4gram model, the sentence is in English
----------------
