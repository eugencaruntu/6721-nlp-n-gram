La sonde InSight de la NASA a atterri.

1GRAM MODEL:

1GRAM: l
ENGLISH: P(l) = -1.3835976742909466 ==> log prob of sentence so far: -1.3835976742909466
FRENCH: P(l) = -1.2161248223608845 ==> log prob of sentence so far: -1.2161248223608845

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -2.481922315062879
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -2.2867803019389212

1GRAM: s
ENGLISH: P(s) = -1.18237672796024 ==> log prob of sentence so far: -3.664299043023119
FRENCH: P(s) = -1.1050716113322194 ==> log prob of sentence so far: -3.3918519132711404

1GRAM: o
ENGLISH: P(o) = -1.1241820463462655 ==> log prob of sentence so far: -4.788481089369385
FRENCH: P(o) = -1.265193765366532 ==> log prob of sentence so far: -4.657045678637672

1GRAM: n
ENGLISH: P(n) = -1.1599632276932161 ==> log prob of sentence so far: -5.9484443170626005
FRENCH: P(n) = -1.1521751619158405 ==> log prob of sentence so far: -5.809220840553513

1GRAM: d
ENGLISH: P(d) = -1.3852685001126093 ==> log prob of sentence so far: -7.3337128171752095
FRENCH: P(d) = -1.4087138708407954 ==> log prob of sentence so far: -7.217934711394308

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -8.23564825457608
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -8.023503988423208

1GRAM: i
ENGLISH: P(i) = -1.15170480747376 ==> log prob of sentence so far: -9.38735306204984
FRENCH: P(i) = -1.1290925204027853 ==> log prob of sentence so far: -9.152596508825994

1GRAM: n
ENGLISH: P(n) = -1.1599632276932161 ==> log prob of sentence so far: -10.547316289743057
FRENCH: P(n) = -1.1521751619158405 ==> log prob of sentence so far: -10.304771670741834

1GRAM: s
ENGLISH: P(s) = -1.18237672796024 ==> log prob of sentence so far: -11.729693017703296
FRENCH: P(s) = -1.1050716113322194 ==> log prob of sentence so far: -11.409843282074053

1GRAM: i
ENGLISH: P(i) = -1.15170480747376 ==> log prob of sentence so far: -12.881397825177055
FRENCH: P(i) = -1.1290925204027853 ==> log prob of sentence so far: -12.53893580247684

1GRAM: g
ENGLISH: P(g) = -1.7025038170885405 ==> log prob of sentence so far: -14.583901642265596
FRENCH: P(g) = -2.0154215775799997 ==> log prob of sentence so far: -14.554357380056839

1GRAM: h
ENGLISH: P(h) = -1.193712897320169 ==> log prob of sentence so far: -15.777614539585766
FRENCH: P(h) = -2.04605506268921 ==> log prob of sentence so far: -16.600412442746048

1GRAM: t
ENGLISH: P(t) = -1.0367203932049034 ==> log prob of sentence so far: -16.81433493279067
FRENCH: P(t) = -1.1325263815326383 ==> log prob of sentence so far: -17.732938824278687

1GRAM: d
ENGLISH: P(d) = -1.3852685001126093 ==> log prob of sentence so far: -18.199603432903277
FRENCH: P(d) = -1.4087138708407954 ==> log prob of sentence so far: -19.141652695119483

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -19.101538870304147
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -19.947221972148384

1GRAM: l
ENGLISH: P(l) = -1.3835976742909466 ==> log prob of sentence so far: -20.485136544595093
FRENCH: P(l) = -1.2161248223608845 ==> log prob of sentence so far: -21.16334679450927

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -21.583461185367025
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -22.234002274087306

1GRAM: n
ENGLISH: P(n) = -1.1599632276932161 ==> log prob of sentence so far: -22.74342441306024
FRENCH: P(n) = -1.1521751619158405 ==> log prob of sentence so far: -23.386177436003145

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -23.84174905383217
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -24.45683291558118

1GRAM: s
ENGLISH: P(s) = -1.18237672796024 ==> log prob of sentence so far: -25.02412578179241
FRENCH: P(s) = -1.1050716113322194 ==> log prob of sentence so far: -25.561904526913402

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -26.12245042256434
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -26.63256000649144

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -27.220775063336273
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -27.703215486069475

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -28.319099704108204
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -28.77387096564751

1GRAM: t
ENGLISH: P(t) = -1.0367203932049034 ==> log prob of sentence so far: -29.355820097313106
FRENCH: P(t) = -1.1325263815326383 ==> log prob of sentence so far: -29.90639734718015

1GRAM: t
ENGLISH: P(t) = -1.0367203932049034 ==> log prob of sentence so far: -30.39254049051801
FRENCH: P(t) = -1.1325263815326383 ==> log prob of sentence so far: -31.03892372871279

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -31.294475927918878
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -31.84449300574169

1GRAM: r
ENGLISH: P(r) = -1.2212020525433098 ==> log prob of sentence so far: -32.515677980462186
FRENCH: P(r) = -1.166447418641506 ==> log prob of sentence so far: -33.01094042438319

1GRAM: r
ENGLISH: P(r) = -1.2212020525433098 ==> log prob of sentence so far: -33.7368800330055
FRENCH: P(r) = -1.166447418641506 ==> log prob of sentence so far: -34.1773878430247

1GRAM: i
ENGLISH: P(i) = -1.15170480747376 ==> log prob of sentence so far: -34.88858484047926
FRENCH: P(i) = -1.1290925204027853 ==> log prob of sentence so far: -35.30648036342748

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: la
ENGLISH: P(a|l) = -0.9671379229058816 ==> log prob of sentence so far: -0.9671379229058816
FRENCH: P(a|l) = -0.6845081303579077 ==> log prob of sentence so far: -0.6845081303579077

2GRAM: as
ENGLISH: P(s|a) = -0.988179589418747 ==> log prob of sentence so far: -1.9553175123246285
FRENCH: P(s|a) = -1.275597448308868 ==> log prob of sentence so far: -1.9601055786667756

2GRAM: so
ENGLISH: P(o|s) = -1.0211159332722501 ==> log prob of sentence so far: -2.9764334455968786
FRENCH: P(o|s) = -1.130223269255406 ==> log prob of sentence so far: -3.0903288479221818

2GRAM: on
ENGLISH: P(n|o) = -0.8301310176775201 ==> log prob of sentence so far: -3.8065644632743987
FRENCH: P(n|o) = -0.5569948935034322 ==> log prob of sentence so far: -3.647323741425614

2GRAM: nd
ENGLISH: P(d|n) = -0.7146009868879144 ==> log prob of sentence so far: -4.521165450162313
FRENCH: P(d|n) = -1.0602997095046485 ==> log prob of sentence so far: -4.707623450930262

2GRAM: de
ENGLISH: P(e|d) = -0.8186179792453097 ==> log prob of sentence so far: -5.339783429407622
FRENCH: P(e|d) = -0.3138194685480035 ==> log prob of sentence so far: -5.0214429194782655

2GRAM: ei
ENGLISH: P(i|e) = -1.4828127985912738 ==> log prob of sentence so far: -6.822596227998896
FRENCH: P(i|e) = -1.8038437431369243 ==> log prob of sentence so far: -6.82528666261519

2GRAM: in
ENGLISH: P(n|i) = -0.5882852299527852 ==> log prob of sentence so far: -7.410881457951682
FRENCH: P(n|i) = -0.9667747855545095 ==> log prob of sentence so far: -7.792061448169699

2GRAM: ns
ENGLISH: P(s|n) = -1.2320063275471609 ==> log prob of sentence so far: -8.642887785498843
FRENCH: P(s|n) = -0.9588212906427211 ==> log prob of sentence so far: -8.75088273881242

2GRAM: si
ENGLISH: P(i|s) = -1.0968382595122879 ==> log prob of sentence so far: -9.73972604501113
FRENCH: P(i|s) = -1.183237983775244 ==> log prob of sentence so far: -9.934120722587664

2GRAM: ig
ENGLISH: P(g|i) = -1.485911389637668 ==> log prob of sentence so far: -11.225637434648798
FRENCH: P(g|i) = -1.7250514197772122 ==> log prob of sentence so far: -11.659172142364877

2GRAM: gh
ENGLISH: P(h|g) = -0.7879600299747146 ==> log prob of sentence so far: -12.013597464623512
FRENCH: P(h|g) = -2.7892064568454678 ==> log prob of sentence so far: -14.448378599210345

2GRAM: ht
ENGLISH: P(t|h) = -1.3270552987534296 ==> log prob of sentence so far: -13.34065276337694
FRENCH: P(t|h) = -1.5722203401470858 ==> log prob of sentence so far: -16.02059893935743

2GRAM: td
ENGLISH: P(d|t) = -2.2756460340939557 ==> log prob of sentence so far: -15.616298797470897
FRENCH: P(d|t) = -1.1939167310948278 ==> log prob of sentence so far: -17.214515670452258

2GRAM: de
ENGLISH: P(e|d) = -0.8186179792453097 ==> log prob of sentence so far: -16.434916776716207
FRENCH: P(e|d) = -0.3138194685480035 ==> log prob of sentence so far: -17.528335139000262

2GRAM: el
ENGLISH: P(l|e) = -1.331028550036677 ==> log prob of sentence so far: -17.765945326752885
FRENCH: P(l|e) = -1.0706208997726607 ==> log prob of sentence so far: -18.598956038772922

2GRAM: la
ENGLISH: P(a|l) = -0.9671379229058816 ==> log prob of sentence so far: -18.733083249658765
FRENCH: P(a|l) = -0.6845081303579077 ==> log prob of sentence so far: -19.28346416913083

2GRAM: an
ENGLISH: P(n|a) = -0.6677226397364787 ==> log prob of sentence so far: -19.400805889395244
FRENCH: P(n|a) = -0.8456621170358352 ==> log prob of sentence so far: -20.129126286166667

2GRAM: na
ENGLISH: P(a|n) = -1.2226923423957918 ==> log prob of sentence so far: -20.623498231791036
FRENCH: P(a|n) = -1.1439366732128127 ==> log prob of sentence so far: -21.27306295937948

2GRAM: as
ENGLISH: P(s|a) = -0.988179589418747 ==> log prob of sentence so far: -21.61167782120978
FRENCH: P(s|a) = -1.275597448308868 ==> log prob of sentence so far: -22.54866040768835

2GRAM: sa
ENGLISH: P(a|s) = -1.0176424581781174 ==> log prob of sentence so far: -22.629320279387898
FRENCH: P(a|s) = -0.9584661807556515 ==> log prob of sentence so far: -23.507126588444

2GRAM: aa
ENGLISH: P(a|a) = -2.7029169187572823 ==> log prob of sentence so far: -25.33223719814518
FRENCH: P(a|a) = -2.6403799040406977 ==> log prob of sentence so far: -26.147506492484695

2GRAM: aa
ENGLISH: P(a|a) = -2.7029169187572823 ==> log prob of sentence so far: -28.03515411690246
FRENCH: P(a|a) = -2.6403799040406977 ==> log prob of sentence so far: -28.787886396525394

2GRAM: at
ENGLISH: P(t|a) = -0.8663792099318748 ==> log prob of sentence so far: -28.901533326834336
FRENCH: P(t|a) = -1.316564816653678 ==> log prob of sentence so far: -30.104451213179072

2GRAM: tt
ENGLISH: P(t|t) = -1.2788863883918729 ==> log prob of sentence so far: -30.180419715226208
FRENCH: P(t|t) = -1.2145874190893862 ==> log prob of sentence so far: -31.31903863226846

2GRAM: te
ENGLISH: P(e|t) = -1.0419538288470722 ==> log prob of sentence so far: -31.222373544073278
FRENCH: P(e|t) = -0.7278672106405729 ==> log prob of sentence so far: -32.046905842909034

2GRAM: er
ENGLISH: P(r|e) = -0.8479237637379716 ==> log prob of sentence so far: -32.07029730781125
FRENCH: P(r|e) = -1.0862077157823018 ==> log prob of sentence so far: -33.133113558691335

2GRAM: rr
ENGLISH: P(r|r) = -1.6989020730524151 ==> log prob of sentence so far: -33.76919938086367
FRENCH: P(r|r) = -1.5178511138364532 ==> log prob of sentence so far: -34.650964672527785

2GRAM: ri
ENGLISH: P(i|r) = -0.9900296230966597 ==> log prob of sentence so far: -34.75922900396033
FRENCH: P(i|r) = -1.0397474629671202 ==> log prob of sentence so far: -35.690712135494906

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: las
ENGLISH: P(s|la) = -0.9500768232359701 ==> log prob of sentence so far: -0.9500768232359701
FRENCH: P(s|la) = -1.3050667837156098 ==> log prob of sentence so far: -1.3050667837156098

3GRAM: aso
ENGLISH: P(o|as) = -1.3452577319755616 ==> log prob of sentence so far: -2.295334555211532
FRENCH: P(o|as) = -1.240305935792107 ==> log prob of sentence so far: -2.545372719507717

3GRAM: son
ENGLISH: P(n|so) = -0.7886638365324057 ==> log prob of sentence so far: -3.0839983917439375
FRENCH: P(n|so) = -0.3227675807689859 ==> log prob of sentence so far: -2.868140300276703

3GRAM: ond
ENGLISH: P(d|on) = -1.3360449438957083 ==> log prob of sentence so far: -4.420043335639646
FRENCH: P(d|on) = -0.8899043500383695 ==> log prob of sentence so far: -3.7580446503150724

3GRAM: nde
ENGLISH: P(e|nd) = -0.9535583149103695 ==> log prob of sentence so far: -5.373601650550015
FRENCH: P(e|nd) = -0.494792678874875 ==> log prob of sentence so far: -4.252837329189948

3GRAM: dei
ENGLISH: P(i|de) = -1.8496926667890916 ==> log prob of sentence so far: -7.223294317339106
FRENCH: P(i|de) = -2.6747343888979858 ==> log prob of sentence so far: -6.9275717180879335

3GRAM: ein
ENGLISH: P(n|ei) = -0.48361361319797636 ==> log prob of sentence so far: -7.706907930537082
FRENCH: P(n|ei) = -0.5638622058050503 ==> log prob of sentence so far: -7.491433923892984

3GRAM: ins
ENGLISH: P(s|in) = -1.2678531822639012 ==> log prob of sentence so far: -8.974761112800984
FRENCH: P(s|in) = -0.8364499665525961 ==> log prob of sentence so far: -8.32788389044558

3GRAM: nsi
ENGLISH: P(i|ns) = -0.9329310072854942 ==> log prob of sentence so far: -9.907692120086478
FRENCH: P(i|ns) = -0.9893847101681175 ==> log prob of sentence so far: -9.317268600613698

3GRAM: sig
ENGLISH: P(g|si) = -1.3667175636620144 ==> log prob of sentence so far: -11.274409683748493
FRENCH: P(g|si) = -1.505283857496275 ==> log prob of sentence so far: -10.822552458109973

3GRAM: igh
ENGLISH: P(h|ig) = -0.18036845029157952 ==> log prob of sentence so far: -11.454778134040073
FRENCH: P(h|ig) = -3.4161077123235617 ==> log prob of sentence so far: -14.238660170433535

3GRAM: ght
ENGLISH: P(t|gh) = -0.19118769906938574 ==> log prob of sentence so far: -11.645965833109459
FRENCH: P(t|gh) = -1.390430406893292 ==> log prob of sentence so far: -15.629090577326828

3GRAM: htd
ENGLISH: P(d|ht) = -2.198032479228134 ==> log prob of sentence so far: -13.843998312337593
FRENCH: P(d|ht) = -2.6589648426644352 ==> log prob of sentence so far: -18.288055419991263

3GRAM: tde
ENGLISH: P(e|td) = -0.6086295168489577 ==> log prob of sentence so far: -14.45262782918655
FRENCH: P(e|td) = -0.23439166416552146 ==> log prob of sentence so far: -18.522447084156784

3GRAM: del
ENGLISH: P(l|de) = -1.3812463969797864 ==> log prob of sentence so far: -15.833874226166337
FRENCH: P(l|de) = -0.8011763849301723 ==> log prob of sentence so far: -19.323623469086957

3GRAM: ela
ENGLISH: P(a|el) = -0.997127703455311 ==> log prob of sentence so far: -16.83100192962165
FRENCH: P(a|el) = -0.5598915813740939 ==> log prob of sentence so far: -19.88351505046105

3GRAM: lan
ENGLISH: P(n|la) = -0.6655287692637317 ==> log prob of sentence so far: -17.49653069888538
FRENCH: P(n|la) = -1.0025127405259997 ==> log prob of sentence so far: -20.88602779098705

3GRAM: ana
ENGLISH: P(a|an) = -1.4955396929264955 ==> log prob of sentence so far: -18.992070391811875
FRENCH: P(a|an) = -1.5252858145047024 ==> log prob of sentence so far: -22.411313605491753

3GRAM: nas
ENGLISH: P(s|na) = -1.1413448409257165 ==> log prob of sentence so far: -20.13341523273759
FRENCH: P(s|na) = -1.783806206347088 ==> log prob of sentence so far: -24.19511981183884

3GRAM: asa
ENGLISH: P(a|as) = -1.0330350554861494 ==> log prob of sentence so far: -21.16645028822374
FRENCH: P(a|as) = -1.1651699183243205 ==> log prob of sentence so far: -25.36028973016316

3GRAM: saa
ENGLISH: P(a|sa) = -3.033095135101668 ==> log prob of sentence so far: -24.199545423325407
FRENCH: P(a|sa) = -2.7154689532266496 ==> log prob of sentence so far: -28.07575868338981

3GRAM: aaa
ENGLISH: P(a|aa) = -2.4619699106470416 ==> log prob of sentence so far: -26.66151533397245
FRENCH: P(a|aa) = -2.5630061870617937 ==> log prob of sentence so far: -30.638764870451606

3GRAM: aat
ENGLISH: P(t|aa) = -1.261744971874641 ==> log prob of sentence so far: -27.92326030584709
FRENCH: P(t|aa) = -1.608763677622469 ==> log prob of sentence so far: -32.24752854807407

3GRAM: att
ENGLISH: P(t|at) = -0.8644098417478667 ==> log prob of sentence so far: -28.78767014759496
FRENCH: P(t|at) = -0.7569586646773712 ==> log prob of sentence so far: -33.00448721275144

3GRAM: tte
ENGLISH: P(e|tt) = -0.7880752720451538 ==> log prob of sentence so far: -29.575745419640114
FRENCH: P(e|tt) = -0.32434275129318035 ==> log prob of sentence so far: -33.32882996404462

3GRAM: ter
ENGLISH: P(r|te) = -0.47762413296897416 ==> log prob of sentence so far: -30.053369552609087
FRENCH: P(r|te) = -0.9951495044873662 ==> log prob of sentence so far: -34.32397946853199

3GRAM: err
ENGLISH: P(r|er) = -1.7289182555797105 ==> log prob of sentence so far: -31.782287808188798
FRENCH: P(r|er) = -1.2023043191784606 ==> log prob of sentence so far: -35.52628378771045

3GRAM: rri
ENGLISH: P(i|rr) = -0.5805722654578558 ==> log prob of sentence so far: -32.36286007364665
FRENCH: P(i|rr) = -0.5864698578650122 ==> log prob of sentence so far: -36.11275364557546

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: laso
ENGLISH: P(o|las) = -1.9066008750040984 ==> log prob of sentence so far: -1.9066008750040984
FRENCH: P(o|las) = -0.7789347513753352 ==> log prob of sentence so far: -0.7789347513753352

4GRAM: ason
ENGLISH: P(n|aso) = -0.26313038433203656 ==> log prob of sentence so far: -2.169731259336135
FRENCH: P(n|aso) = -0.46812856934023195 ==> log prob of sentence so far: -1.2470633207155672

4GRAM: sond
ENGLISH: P(d|son) = -2.1001481151603816 ==> log prob of sentence so far: -4.269879374496517
FRENCH: P(d|son) = -1.2319711476942348 ==> log prob of sentence so far: -2.479034468409802

4GRAM: onde
ENGLISH: P(e|ond) = -0.5185494113102511 ==> log prob of sentence so far: -4.788428785806768
FRENCH: P(e|ond) = -0.3698311660516151 ==> log prob of sentence so far: -2.848865634461417

4GRAM: ndei
ENGLISH: P(i|nde) = -2.481115608980656 ==> log prob of sentence so far: -7.269544394787424
FRENCH: P(i|nde) = -1.9522464556287284 ==> log prob of sentence so far: -4.801112090090146

4GRAM: dein
ENGLISH: P(n|dei) = -0.43699202663332165 ==> log prob of sentence so far: -7.706536421420745
FRENCH: P(n|dei) = -0.7285252478977349 ==> log prob of sentence so far: -5.5296373379878805

4GRAM: eins
ENGLISH: P(s|ein) = -1.0604118559868088 ==> log prob of sentence so far: -8.766948277407554
FRENCH: P(s|ein) = -0.9741789091023144 ==> log prob of sentence so far: -6.503816247090195

4GRAM: insi
ENGLISH: P(i|ins) = -1.0941469600166736 ==> log prob of sentence so far: -9.861095237424227
FRENCH: P(i|ins) = -0.6556324651268228 ==> log prob of sentence so far: -7.159448712217018

4GRAM: nsig
ENGLISH: P(g|nsi) = -1.2079477395156355 ==> log prob of sentence so far: -11.069042976939862
FRENCH: P(g|nsi) = -1.4120641269322547 ==> log prob of sentence so far: -8.571512839149273

4GRAM: sigh
ENGLISH: P(h|sig) = -0.38577238652601703 ==> log prob of sentence so far: -11.45481536346588
FRENCH: P(h|sig) = -3.1595671932336202 ==> log prob of sentence so far: -11.731080032382893

4GRAM: ight
ENGLISH: P(t|igh) = -0.06954852765632158 ==> log prob of sentence so far: -11.524363891122201
FRENCH: P(t|igh) = -0.7781512503836436 ==> log prob of sentence so far: -12.509231282766537

4GRAM: ghtd
ENGLISH: P(d|ght) = -2.005053062177165 ==> log prob of sentence so far: -13.529416953299366
FRENCH: P(d|ght) = -1.505149978319906 ==> log prob of sentence so far: -14.014381261086443

4GRAM: htde
ENGLISH: P(e|htd) = -0.6972635405220017 ==> log prob of sentence so far: -14.226680493821368
FRENCH: P(e|htd) = -1.0 ==> log prob of sentence so far: -15.014381261086443

4GRAM: tdel
ENGLISH: P(l|tde) = -1.0786109314785972 ==> log prob of sentence so far: -15.305291425299965
FRENCH: P(l|tde) = -0.730949870729012 ==> log prob of sentence so far: -15.745331131815455

4GRAM: dela
ENGLISH: P(a|del) = -0.8977265160573898 ==> log prob of sentence so far: -16.203017941357356
FRENCH: P(a|del) = -0.24078926172065088 ==> log prob of sentence so far: -15.986120393536106

4GRAM: elan
ENGLISH: P(n|ela) = -0.588535388199108 ==> log prob of sentence so far: -16.791553329556464
FRENCH: P(n|ela) = -1.1852424292136958 ==> log prob of sentence so far: -17.171362822749803

4GRAM: lana
ENGLISH: P(a|lan) = -1.8449545664477378 ==> log prob of sentence so far: -18.6365078960042
FRENCH: P(a|lan) = -1.0915632870534286 ==> log prob of sentence so far: -18.26292610980323

4GRAM: anas
ENGLISH: P(s|ana) = -1.1533214834783092 ==> log prob of sentence so far: -19.78982937948251
FRENCH: P(s|ana) = -1.662757831681574 ==> log prob of sentence so far: -19.925683941484806

4GRAM: nasa
ENGLISH: P(a|nas) = -0.9845936689585997 ==> log prob of sentence so far: -20.774423048441108
FRENCH: P(a|nas) = -0.976344453663334 ==> log prob of sentence so far: -20.90202839514814

4GRAM: asaa
ENGLISH: P(a|asa) = -3.2472857028633784 ==> log prob of sentence so far: -24.021708751304487
FRENCH: P(a|asa) = -2.9622114391106003 ==> log prob of sentence so far: -23.86423983425874

4GRAM: saaa
ENGLISH: P(a|saa) = -1.9822712330395684 ==> log prob of sentence so far: -26.003979984344056
FRENCH: P(a|saa) = -2.2355284469075487 ==> log prob of sentence so far: -26.099768281166288

4GRAM: aaat
ENGLISH: P(t|aaa) = -1.505149978319906 ==> log prob of sentence so far: -27.509129962663962
FRENCH: P(t|aaa) = -1.4771212547196624 ==> log prob of sentence so far: -27.57688953588595

4GRAM: aatt
ENGLISH: P(t|aat) = -0.6085908584869202 ==> log prob of sentence so far: -28.117720821150883
FRENCH: P(t|aat) = -0.32658410013636935 ==> log prob of sentence so far: -27.90347363602232

4GRAM: atte
ENGLISH: P(e|att) = -0.725229190346533 ==> log prob of sentence so far: -28.842950011497415
FRENCH: P(e|att) = -0.22568675133154995 ==> log prob of sentence so far: -28.12916038735387

4GRAM: tter
ENGLISH: P(r|tte) = -0.23235069604626252 ==> log prob of sentence so far: -29.075300707543676
FRENCH: P(r|tte) = -1.00408201913991 ==> log prob of sentence so far: -29.133242406493782

4GRAM: terr
ENGLISH: P(r|ter) = -1.3674241569900376 ==> log prob of sentence so far: -30.442724864533712
FRENCH: P(r|ter) = -0.5778059417429826 ==> log prob of sentence so far: -29.711048348236766

4GRAM: erri
ENGLISH: P(i|err) = -0.6437160981371487 ==> log prob of sentence so far: -31.08644096267086
FRENCH: P(i|err) = -0.6467119374259468 ==> log prob of sentence so far: -30.357760285662714

According to the 4gram model, the sentence is in French
----------------
