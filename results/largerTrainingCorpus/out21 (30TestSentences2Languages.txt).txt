NTSB exhorte l'installation d'altimetres radar.

1GRAM MODEL:

1GRAM: n
ENGLISH: P(n) = -1.1599632276932161 ==> log prob of sentence so far: -1.1599632276932161
FRENCH: P(n) = -1.1521751619158405 ==> log prob of sentence so far: -1.1521751619158405

1GRAM: t
ENGLISH: P(t) = -1.0367203932049034 ==> log prob of sentence so far: -2.1966836208981197
FRENCH: P(t) = -1.1325263815326383 ==> log prob of sentence so far: -2.2847015434484788

1GRAM: s
ENGLISH: P(s) = -1.18237672796024 ==> log prob of sentence so far: -3.37906034885836
FRENCH: P(s) = -1.1050716113322194 ==> log prob of sentence so far: -3.3897731547806984

1GRAM: b
ENGLISH: P(b) = -1.8224223207518357 ==> log prob of sentence so far: -5.201482669610195
FRENCH: P(b) = -2.0038808526300764 ==> log prob of sentence so far: -5.393654007410775

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -6.103418107011065
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -6.199223284439675

1GRAM: x
ENGLISH: P(x) = -2.8376483203857426 ==> log prob of sentence so far: -8.941066427396809
FRENCH: P(x) = -2.382859986482064 ==> log prob of sentence so far: -8.58208327092174

1GRAM: h
ENGLISH: P(h) = -1.193712897320169 ==> log prob of sentence so far: -10.134779324716977
FRENCH: P(h) = -2.04605506268921 ==> log prob of sentence so far: -10.628138333610949

1GRAM: o
ENGLISH: P(o) = -1.1241820463462655 ==> log prob of sentence so far: -11.258961371063242
FRENCH: P(o) = -1.265193765366532 ==> log prob of sentence so far: -11.893332098977481

1GRAM: r
ENGLISH: P(r) = -1.2212020525433098 ==> log prob of sentence so far: -12.480163423606552
FRENCH: P(r) = -1.166447418641506 ==> log prob of sentence so far: -13.059779517618987

1GRAM: t
ENGLISH: P(t) = -1.0367203932049034 ==> log prob of sentence so far: -13.516883816811456
FRENCH: P(t) = -1.1325263815326383 ==> log prob of sentence so far: -14.192305899151625

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -14.418819254212327
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -14.997875176180525

1GRAM: l
ENGLISH: P(l) = -1.3835976742909466 ==> log prob of sentence so far: -15.802416928503273
FRENCH: P(l) = -1.2161248223608845 ==> log prob of sentence so far: -16.21399999854141

1GRAM: i
ENGLISH: P(i) = -1.15170480747376 ==> log prob of sentence so far: -16.954121735977033
FRENCH: P(i) = -1.1290925204027853 ==> log prob of sentence so far: -17.343092518944196

1GRAM: n
ENGLISH: P(n) = -1.1599632276932161 ==> log prob of sentence so far: -18.114084963670248
FRENCH: P(n) = -1.1521751619158405 ==> log prob of sentence so far: -18.49526768086004

1GRAM: s
ENGLISH: P(s) = -1.18237672796024 ==> log prob of sentence so far: -19.296461691630487
FRENCH: P(s) = -1.1050716113322194 ==> log prob of sentence so far: -19.60033929219226

1GRAM: t
ENGLISH: P(t) = -1.0367203932049034 ==> log prob of sentence so far: -20.33318208483539
FRENCH: P(t) = -1.1325263815326383 ==> log prob of sentence so far: -20.7328656737249

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -21.43150672560732
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -21.803521153302935

1GRAM: l
ENGLISH: P(l) = -1.3835976742909466 ==> log prob of sentence so far: -22.815104399898267
FRENCH: P(l) = -1.2161248223608845 ==> log prob of sentence so far: -23.01964597566382

1GRAM: l
ENGLISH: P(l) = -1.3835976742909466 ==> log prob of sentence so far: -24.198702074189214
FRENCH: P(l) = -1.2161248223608845 ==> log prob of sentence so far: -24.235770798024706

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -25.297026714961145
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -25.306426277602743

1GRAM: t
ENGLISH: P(t) = -1.0367203932049034 ==> log prob of sentence so far: -26.333747108166047
FRENCH: P(t) = -1.1325263815326383 ==> log prob of sentence so far: -26.43895265913538

1GRAM: i
ENGLISH: P(i) = -1.15170480747376 ==> log prob of sentence so far: -27.485451915639807
FRENCH: P(i) = -1.1290925204027853 ==> log prob of sentence so far: -27.568045179538167

1GRAM: o
ENGLISH: P(o) = -1.1241820463462655 ==> log prob of sentence so far: -28.609633961986074
FRENCH: P(o) = -1.265193765366532 ==> log prob of sentence so far: -28.8332389449047

1GRAM: n
ENGLISH: P(n) = -1.1599632276932161 ==> log prob of sentence so far: -29.76959718967929
FRENCH: P(n) = -1.1521751619158405 ==> log prob of sentence so far: -29.985414106820542

1GRAM: d
ENGLISH: P(d) = -1.3852685001126093 ==> log prob of sentence so far: -31.154865689791897
FRENCH: P(d) = -1.4087138708407954 ==> log prob of sentence so far: -31.394127977661338

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -32.25319033056383
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -32.46478345723938

1GRAM: l
ENGLISH: P(l) = -1.3835976742909466 ==> log prob of sentence so far: -33.63678800485478
FRENCH: P(l) = -1.2161248223608845 ==> log prob of sentence so far: -33.68090827960026

1GRAM: t
ENGLISH: P(t) = -1.0367203932049034 ==> log prob of sentence so far: -34.67350839805968
FRENCH: P(t) = -1.1325263815326383 ==> log prob of sentence so far: -34.813434661132895

1GRAM: i
ENGLISH: P(i) = -1.15170480747376 ==> log prob of sentence so far: -35.825213205533444
FRENCH: P(i) = -1.1290925204027853 ==> log prob of sentence so far: -35.94252718153568

1GRAM: m
ENGLISH: P(m) = -1.6019350435333317 ==> log prob of sentence so far: -37.427148249066775
FRENCH: P(m) = -1.4846148337842204 ==> log prob of sentence so far: -37.4271420153199

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -38.32908368646765
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -38.2327112923488

1GRAM: t
ENGLISH: P(t) = -1.0367203932049034 ==> log prob of sentence so far: -39.36580407967255
FRENCH: P(t) = -1.1325263815326383 ==> log prob of sentence so far: -39.36523767388144

1GRAM: r
ENGLISH: P(r) = -1.2212020525433098 ==> log prob of sentence so far: -40.58700613221586
FRENCH: P(r) = -1.166447418641506 ==> log prob of sentence so far: -40.53168509252294

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -41.488941569616735
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -41.33725436955184

1GRAM: s
ENGLISH: P(s) = -1.18237672796024 ==> log prob of sentence so far: -42.671318297576974
FRENCH: P(s) = -1.1050716113322194 ==> log prob of sentence so far: -42.442325980884064

1GRAM: r
ENGLISH: P(r) = -1.2212020525433098 ==> log prob of sentence so far: -43.892520350120286
FRENCH: P(r) = -1.166447418641506 ==> log prob of sentence so far: -43.60877339952557

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -44.99084499089222
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -44.679428879103604

1GRAM: d
ENGLISH: P(d) = -1.3852685001126093 ==> log prob of sentence so far: -46.37611349100483
FRENCH: P(d) = -1.4087138708407954 ==> log prob of sentence so far: -46.088142749944396

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -47.474438131776765
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -47.15879822952243

1GRAM: r
ENGLISH: P(r) = -1.2212020525433098 ==> log prob of sentence so far: -48.69564018432008
FRENCH: P(r) = -1.166447418641506 ==> log prob of sentence so far: -48.325245648163936

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: nt
ENGLISH: P(t|n) = -0.8170158624991728 ==> log prob of sentence so far: -0.8170158624991728
FRENCH: P(t|n) = -0.6237272622949059 ==> log prob of sentence so far: -0.6237272622949059

2GRAM: ts
ENGLISH: P(s|t) = -1.4257498908860722 ==> log prob of sentence so far: -2.242765753385245
FRENCH: P(s|t) = -1.2644001611043965 ==> log prob of sentence so far: -1.8881274233993024

2GRAM: sb
ENGLISH: P(b|s) = -1.7301099241263256 ==> log prob of sentence so far: -3.9728756775115706
FRENCH: P(b|s) = -1.911167151775863 ==> log prob of sentence so far: -3.7992945751751654

2GRAM: be
ENGLISH: P(e|b) = -0.5257361592759486 ==> log prob of sentence so far: -4.49861183678752
FRENCH: P(e|b) = -0.783230008727944 ==> log prob of sentence so far: -4.582524583903109

2GRAM: ex
ENGLISH: P(x|e) = -2.0507989502417865 ==> log prob of sentence so far: -6.549410787029306
FRENCH: P(x|e) = -2.3006083005163496 ==> log prob of sentence so far: -6.883132884419458

2GRAM: xh
ENGLISH: P(h|x) = -1.58949103202604 ==> log prob of sentence so far: -8.138901819055345
FRENCH: P(h|x) = -1.6958284032306161 ==> log prob of sentence so far: -8.578961287650074

2GRAM: ho
ENGLISH: P(o|h) = -1.0694298750156104 ==> log prob of sentence so far: -9.208331694070955
FRENCH: P(o|h) = -0.7965433426995154 ==> log prob of sentence so far: -9.375504630349589

2GRAM: or
ENGLISH: P(r|o) = -0.8944827546913645 ==> log prob of sentence so far: -10.10281444876232
FRENCH: P(r|o) = -1.0382587462837478 ==> log prob of sentence so far: -10.413763376633337

2GRAM: rt
ENGLISH: P(t|r) = -1.1044712213482673 ==> log prob of sentence so far: -11.207285670110588
FRENCH: P(t|r) = -1.2375414237192641 ==> log prob of sentence so far: -11.651304800352602

2GRAM: te
ENGLISH: P(e|t) = -1.0419538288470722 ==> log prob of sentence so far: -12.24923949895766
FRENCH: P(e|t) = -0.7278672106405729 ==> log prob of sentence so far: -12.379172010993175

2GRAM: el
ENGLISH: P(l|e) = -1.331028550036677 ==> log prob of sentence so far: -13.580268048994338
FRENCH: P(l|e) = -1.0706208997726607 ==> log prob of sentence so far: -13.449792910765836

2GRAM: li
ENGLISH: P(i|l) = -0.8764964187294044 ==> log prob of sentence so far: -14.456764467723742
FRENCH: P(i|l) = -1.1844972785622172 ==> log prob of sentence so far: -14.634290189328054

2GRAM: in
ENGLISH: P(n|i) = -0.5882852299527852 ==> log prob of sentence so far: -15.045049697676527
FRENCH: P(n|i) = -0.9667747855545095 ==> log prob of sentence so far: -15.601064974882563

2GRAM: ns
ENGLISH: P(s|n) = -1.2320063275471609 ==> log prob of sentence so far: -16.277056025223686
FRENCH: P(s|n) = -0.9588212906427211 ==> log prob of sentence so far: -16.559886265525286

2GRAM: st
ENGLISH: P(t|s) = -0.7279411329912379 ==> log prob of sentence so far: -17.004997158214923
FRENCH: P(t|s) = -1.135607810562159 ==> log prob of sentence so far: -17.695494076087446

2GRAM: ta
ENGLISH: P(a|t) = -1.2198154971362172 ==> log prob of sentence so far: -18.22481265535114
FRENCH: P(a|t) = -0.9213767405614084 ==> log prob of sentence so far: -18.616870816648856

2GRAM: al
ENGLISH: P(l|a) = -1.0528481362365971 ==> log prob of sentence so far: -19.27766079158774
FRENCH: P(l|a) = -1.2310228913879266 ==> log prob of sentence so far: -19.847893708036782

2GRAM: ll
ENGLISH: P(l|l) = -0.8546831385767429 ==> log prob of sentence so far: -20.13234393016448
FRENCH: P(l|l) = -0.9404871687407055 ==> log prob of sentence so far: -20.788380876777488

2GRAM: la
ENGLISH: P(a|l) = -0.9671379229058816 ==> log prob of sentence so far: -21.099481853070362
FRENCH: P(a|l) = -0.6845081303579077 ==> log prob of sentence so far: -21.472889007135397

2GRAM: at
ENGLISH: P(t|a) = -0.8663792099318748 ==> log prob of sentence so far: -21.965861063002237
FRENCH: P(t|a) = -1.316564816653678 ==> log prob of sentence so far: -22.789453823789074

2GRAM: ti
ENGLISH: P(i|t) = -1.0226063494679998 ==> log prob of sentence so far: -22.988467412470236
FRENCH: P(i|t) = -1.0614782867270083 ==> log prob of sentence so far: -23.850932110516084

2GRAM: io
ENGLISH: P(o|i) = -1.2876354961916932 ==> log prob of sentence so far: -24.27610290866193
FRENCH: P(o|i) = -1.445269856817262 ==> log prob of sentence so far: -25.296201967333346

2GRAM: on
ENGLISH: P(n|o) = -0.8301310176775201 ==> log prob of sentence so far: -25.10623392633945
FRENCH: P(n|o) = -0.5569948935034322 ==> log prob of sentence so far: -25.85319686083678

2GRAM: nd
ENGLISH: P(d|n) = -0.7146009868879144 ==> log prob of sentence so far: -25.820834913227365
FRENCH: P(d|n) = -1.0602997095046485 ==> log prob of sentence so far: -26.913496570341426

2GRAM: da
ENGLISH: P(a|d) = -1.0031400437490998 ==> log prob of sentence so far: -26.823974956976464
FRENCH: P(a|d) = -0.8700127992364188 ==> log prob of sentence so far: -27.783509369577846

2GRAM: al
ENGLISH: P(l|a) = -1.0528481362365971 ==> log prob of sentence so far: -27.876823093213062
FRENCH: P(l|a) = -1.2310228913879266 ==> log prob of sentence so far: -29.014532260965773

2GRAM: lt
ENGLISH: P(t|l) = -1.4082383017020734 ==> log prob of sentence so far: -29.285061394915136
FRENCH: P(t|l) = -1.9008830155999266 ==> log prob of sentence so far: -30.9154152765657

2GRAM: ti
ENGLISH: P(i|t) = -1.0226063494679998 ==> log prob of sentence so far: -30.307667744383135
FRENCH: P(i|t) = -1.0614782867270083 ==> log prob of sentence so far: -31.97689356329271

2GRAM: im
ENGLISH: P(m|i) = -1.3659132327876533 ==> log prob of sentence so far: -31.67358097717079
FRENCH: P(m|i) = -1.535803189494064 ==> log prob of sentence so far: -33.51269675278677

2GRAM: me
ENGLISH: P(e|m) = -0.601584537026081 ==> log prob of sentence so far: -32.27516551419687
FRENCH: P(e|m) = -0.5092479296567639 ==> log prob of sentence so far: -34.02194468244354

2GRAM: et
ENGLISH: P(t|e) = -1.1837603181589333 ==> log prob of sentence so far: -33.4589258323558
FRENCH: P(t|e) = -1.0909072673569926 ==> log prob of sentence so far: -35.11285194980053

2GRAM: tr
ENGLISH: P(r|t) = -1.4573466072495562 ==> log prob of sentence so far: -34.91627243960536
FRENCH: P(r|t) = -0.9972999921011502 ==> log prob of sentence so far: -36.11015194190168

2GRAM: re
ENGLISH: P(e|r) = -0.6224130940258753 ==> log prob of sentence so far: -35.53868553363123
FRENCH: P(e|r) = -0.5676012830549572 ==> log prob of sentence so far: -36.67775322495663

2GRAM: es
ENGLISH: P(s|e) = -0.9708855148194279 ==> log prob of sentence so far: -36.50957104845066
FRENCH: P(s|e) = -0.7615789483781602 ==> log prob of sentence so far: -37.43933217333479

2GRAM: sr
ENGLISH: P(r|s) = -2.0973558489790722 ==> log prob of sentence so far: -38.60692689742973
FRENCH: P(r|s) = -1.7984800759652295 ==> log prob of sentence so far: -39.23781224930002

2GRAM: ra
ENGLISH: P(a|r) = -1.0036435158475256 ==> log prob of sentence so far: -39.61057041327726
FRENCH: P(a|r) = -0.9156039180500228 ==> log prob of sentence so far: -40.153416167350045

2GRAM: ad
ENGLISH: P(d|a) = -1.3449915249632305 ==> log prob of sentence so far: -40.95556193824049
FRENCH: P(d|a) = -1.4692731976432378 ==> log prob of sentence so far: -41.622689364993285

2GRAM: da
ENGLISH: P(a|d) = -1.0031400437490998 ==> log prob of sentence so far: -41.95870198198959
FRENCH: P(a|d) = -0.8700127992364188 ==> log prob of sentence so far: -42.492702164229705

2GRAM: ar
ENGLISH: P(r|a) = -0.9674329311729756 ==> log prob of sentence so far: -42.926134913162564
FRENCH: P(r|a) = -1.0010287410510506 ==> log prob of sentence so far: -43.493730905280756

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: nts
ENGLISH: P(s|nt) = -1.2353156525196063 ==> log prob of sentence so far: -1.2353156525196063
FRENCH: P(s|nt) = -1.0689056652900788 ==> log prob of sentence so far: -1.0689056652900788

3GRAM: tsb
ENGLISH: P(b|ts) = -1.6075242347634766 ==> log prob of sentence so far: -2.842839887283083
FRENCH: P(b|ts) = -2.1956617685789643 ==> log prob of sentence so far: -3.2645674338690434

3GRAM: sbe
ENGLISH: P(e|sb) = -0.4845348358770349 ==> log prob of sentence so far: -3.327374723160118
FRENCH: P(e|sb) = -0.7983260465978606 ==> log prob of sentence so far: -4.062893480466904

3GRAM: bex
ENGLISH: P(x|be) = -3.7928895195936354 ==> log prob of sentence so far: -7.120264242753754
FRENCH: P(x|be) = -3.7060346607143506 ==> log prob of sentence so far: -7.768928141181255

3GRAM: exh
ENGLISH: P(h|ex) = -1.5887686609775418 ==> log prob of sentence so far: -8.709032903731295
FRENCH: P(h|ex) = -2.2835456950662687 ==> log prob of sentence so far: -10.052473836247524

3GRAM: xho
ENGLISH: P(o|xh) = -0.9746941347352298 ==> log prob of sentence so far: -9.683727038466525
FRENCH: P(o|xh) = -0.5997560364183981 ==> log prob of sentence so far: -10.652229872665922

3GRAM: hor
ENGLISH: P(r|ho) = -1.0470596144746913 ==> log prob of sentence so far: -10.730786652941216
FRENCH: P(r|ho) = -0.9800629077325101 ==> log prob of sentence so far: -11.632292780398432

3GRAM: ort
ENGLISH: P(t|or) = -0.7793430339358287 ==> log prob of sentence so far: -11.510129686877045
FRENCH: P(t|or) = -0.5404552766741589 ==> log prob of sentence so far: -12.17274805707259

3GRAM: rte
ENGLISH: P(e|rt) = -1.2248049456586745 ==> log prob of sentence so far: -12.734934632535719
FRENCH: P(e|rt) = -0.5708630642973106 ==> log prob of sentence so far: -12.7436111213699

3GRAM: tel
ENGLISH: P(l|te) = -1.3466326550217027 ==> log prob of sentence so far: -14.081567287557421
FRENCH: P(l|te) = -1.0778680654071915 ==> log prob of sentence so far: -13.821479186777092

3GRAM: eli
ENGLISH: P(i|el) = -0.7757844512976412 ==> log prob of sentence so far: -14.857351738855062
FRENCH: P(i|el) = -1.4072058855145395 ==> log prob of sentence so far: -15.228685072291631

3GRAM: lin
ENGLISH: P(n|li) = -0.7590934710580197 ==> log prob of sentence so far: -15.616445209913081
FRENCH: P(n|li) = -0.878488423864714 ==> log prob of sentence so far: -16.107173496156346

3GRAM: ins
ENGLISH: P(s|in) = -1.2678531822639012 ==> log prob of sentence so far: -16.884298392176984
FRENCH: P(s|in) = -0.8364499665525961 ==> log prob of sentence so far: -16.943623462708942

3GRAM: nst
ENGLISH: P(t|ns) = -0.6895985031667035 ==> log prob of sentence so far: -17.573896895343687
FRENCH: P(t|ns) = -1.1700608291303916 ==> log prob of sentence so far: -18.113684291839334

3GRAM: sta
ENGLISH: P(a|st) = -0.9006206065673983 ==> log prob of sentence so far: -18.474517501911084
FRENCH: P(a|st) = -0.6873224850951417 ==> log prob of sentence so far: -18.801006776934475

3GRAM: tal
ENGLISH: P(l|ta) = -0.9355918458734487 ==> log prob of sentence so far: -19.410109347784534
FRENCH: P(l|ta) = -1.02449735359535 ==> log prob of sentence so far: -19.825504130529826

3GRAM: all
ENGLISH: P(l|al) = -0.44315815965265565 ==> log prob of sentence so far: -19.853267507437188
FRENCH: P(l|al) = -0.7398897448849626 ==> log prob of sentence so far: -20.565393875414788

3GRAM: lla
ENGLISH: P(a|ll) = -1.0575997034874136 ==> log prob of sentence so far: -20.9108672109246
FRENCH: P(a|ll) = -0.9732950970655565 ==> log prob of sentence so far: -21.538688972480344

3GRAM: lat
ENGLISH: P(t|la) = -0.9055417935588892 ==> log prob of sentence so far: -21.81640900448349
FRENCH: P(t|la) = -1.2129908001290703 ==> log prob of sentence so far: -22.751679772609414

3GRAM: ati
ENGLISH: P(i|at) = -0.7115267344846236 ==> log prob of sentence so far: -22.527935738968115
FRENCH: P(i|at) = -0.511644733681952 ==> log prob of sentence so far: -23.263324506291365

3GRAM: tio
ENGLISH: P(o|ti) = -0.5891609118479846 ==> log prob of sentence so far: -23.117096650816098
FRENCH: P(o|ti) = -0.5804702164224905 ==> log prob of sentence so far: -23.843794722713856

3GRAM: ion
ENGLISH: P(n|io) = -0.10167583622712065 ==> log prob of sentence so far: -23.218772487043218
FRENCH: P(n|io) = -0.052148608634734674 ==> log prob of sentence so far: -23.89594333134859

3GRAM: ond
ENGLISH: P(d|on) = -1.3360449438957083 ==> log prob of sentence so far: -24.554817430938925
FRENCH: P(d|on) = -0.8899043500383695 ==> log prob of sentence so far: -24.78584768138696

3GRAM: nda
ENGLISH: P(a|nd) = -1.084727924024753 ==> log prob of sentence so far: -25.63954535496368
FRENCH: P(a|nd) = -0.8147171973587001 ==> log prob of sentence so far: -25.60056487874566

3GRAM: dal
ENGLISH: P(l|da) = -1.068385859208551 ==> log prob of sentence so far: -26.70793121417223
FRENCH: P(l|da) = -1.5067406081601624 ==> log prob of sentence so far: -27.107305486905823

3GRAM: alt
ENGLISH: P(t|al) = -1.2519088847524023 ==> log prob of sentence so far: -27.959840098924634
FRENCH: P(t|al) = -1.6728154234708335 ==> log prob of sentence so far: -28.780120910376656

3GRAM: lti
ENGLISH: P(i|lt) = -1.0691166265641854 ==> log prob of sentence so far: -29.02895672548882
FRENCH: P(i|lt) = -1.0001235370458916 ==> log prob of sentence so far: -29.780244447422547

3GRAM: tim
ENGLISH: P(m|ti) = -1.0899921125172827 ==> log prob of sentence so far: -30.1189488380061
FRENCH: P(m|ti) = -1.3063703709839922 ==> log prob of sentence so far: -31.086614818406538

3GRAM: ime
ENGLISH: P(e|im) = -0.6181367538419187 ==> log prob of sentence so far: -30.73708559184802
FRENCH: P(e|im) = -0.5030387157053924 ==> log prob of sentence so far: -31.58965353411193

3GRAM: met
ENGLISH: P(t|me) = -0.9135460881286742 ==> log prob of sentence so far: -31.650631679976694
FRENCH: P(t|me) = -1.2204994694298827 ==> log prob of sentence so far: -32.810153003541814

3GRAM: etr
ENGLISH: P(r|et) = -1.3027789925452502 ==> log prob of sentence so far: -32.95341067252195
FRENCH: P(r|et) = -1.1124159214373532 ==> log prob of sentence so far: -33.92256892497917

3GRAM: tre
ENGLISH: P(e|tr) = -0.6483950378801738 ==> log prob of sentence so far: -33.60180571040212
FRENCH: P(e|tr) = -0.2789071371009522 ==> log prob of sentence so far: -34.20147606208012

3GRAM: res
ENGLISH: P(s|re) = -0.8636598570858126 ==> log prob of sentence so far: -34.46546556748793
FRENCH: P(s|re) = -0.7379153756172324 ==> log prob of sentence so far: -34.93939143769735

3GRAM: esr
ENGLISH: P(r|es) = -2.3586775483048514 ==> log prob of sentence so far: -36.824143115792786
FRENCH: P(r|es) = -1.675413351747651 ==> log prob of sentence so far: -36.614804789445

3GRAM: sra
ENGLISH: P(a|sr) = -0.8640952379063904 ==> log prob of sentence so far: -37.68823835369918
FRENCH: P(a|sr) = -0.8634197424645129 ==> log prob of sentence so far: -37.47822453190952

3GRAM: rad
ENGLISH: P(d|ra) = -1.5673787818710525 ==> log prob of sentence so far: -39.255617135570226
FRENCH: P(d|ra) = -1.6144231476244544 ==> log prob of sentence so far: -39.092647679533975

3GRAM: ada
ENGLISH: P(a|ad) = -1.0372569390000907 ==> log prob of sentence so far: -40.292874074570314
FRENCH: P(a|ad) = -0.4113072530671749 ==> log prob of sentence so far: -39.50395493260115

3GRAM: dar
ENGLISH: P(r|da) = -1.1123555338271762 ==> log prob of sentence so far: -41.40522960839749
FRENCH: P(r|da) = -1.6748320335040012 ==> log prob of sentence so far: -41.17878696610515

According to the 3gram model, the sentence is in French
----------------
4GRAM MODEL:

4GRAM: ntsb
ENGLISH: P(b|nts) = -1.55094922595786 ==> log prob of sentence so far: -1.55094922595786
FRENCH: P(b|nts) = -2.153980068612762 ==> log prob of sentence so far: -2.153980068612762

4GRAM: tsbe
ENGLISH: P(e|tsb) = -0.5775170502504364 ==> log prob of sentence so far: -2.128466276208296
FRENCH: P(e|tsb) = -0.8995734132691064 ==> log prob of sentence so far: -3.0535534818818686

4GRAM: sbex
ENGLISH: P(x|sbe) = -3.69196510276736 ==> log prob of sentence so far: -5.820431378975656
FRENCH: P(x|sbe) = -3.147985320683805 ==> log prob of sentence so far: -6.201538802565674

4GRAM: bexh
ENGLISH: P(h|bex) = -1.5314789170422551 ==> log prob of sentence so far: -7.351910296017912
FRENCH: P(h|bex) = -1.4471580313422192 ==> log prob of sentence so far: -7.648696833907893

4GRAM: exho
ENGLISH: P(o|exh) = -1.0115818725498151 ==> log prob of sentence so far: -8.363492168567728
FRENCH: P(o|exh) = -0.47017239476433464 ==> log prob of sentence so far: -8.118869228672228

4GRAM: xhor
ENGLISH: P(r|xho) = -0.3010299956639812 ==> log prob of sentence so far: -8.664522164231709
FRENCH: P(r|xho) = -0.8680157754502348 ==> log prob of sentence so far: -8.986885004122463

4GRAM: hort
ENGLISH: P(t|hor) = -0.7272836046154852 ==> log prob of sentence so far: -9.391805768847194
FRENCH: P(t|hor) = -0.8953077963164343 ==> log prob of sentence so far: -9.882192800438897

4GRAM: orte
ENGLISH: P(e|ort) = -1.4019545334885062 ==> log prob of sentence so far: -10.7937603023357
FRENCH: P(e|ort) = -0.47063687053622866 ==> log prob of sentence so far: -10.352829670975126

4GRAM: rtel
ENGLISH: P(l|rte) = -1.7692270771620284 ==> log prob of sentence so far: -12.562987379497729
FRENCH: P(l|rte) = -1.2539273441281997 ==> log prob of sentence so far: -11.606757015103327

4GRAM: teli
ENGLISH: P(i|tel) = -1.3726129793026238 ==> log prob of sentence so far: -13.935600358800352
FRENCH: P(i|tel) = -1.545862019371916 ==> log prob of sentence so far: -13.152619034475242

4GRAM: elin
ENGLISH: P(n|eli) = -0.820027877452647 ==> log prob of sentence so far: -14.755628236253
FRENCH: P(n|eli) = -0.6953932794856376 ==> log prob of sentence so far: -13.84801231396088

4GRAM: lins
ENGLISH: P(s|lin) = -1.43615900669992 ==> log prob of sentence so far: -16.19178724295292
FRENCH: P(s|lin) = -0.8585210129836891 ==> log prob of sentence so far: -14.706533326944568

4GRAM: inst
ENGLISH: P(t|ins) = -0.38083490514210305 ==> log prob of sentence so far: -16.57262214809502
FRENCH: P(t|ins) = -0.7090376704361366 ==> log prob of sentence so far: -15.415570997380705

4GRAM: nsta
ENGLISH: P(a|nst) = -0.6891710167284458 ==> log prob of sentence so far: -17.261793164823466
FRENCH: P(a|nst) = -0.4150669156716869 ==> log prob of sentence so far: -15.830637913052392

4GRAM: stal
ENGLISH: P(l|sta) = -1.2862283251061468 ==> log prob of sentence so far: -18.548021489929614
FRENCH: P(l|sta) = -0.8178029066976619 ==> log prob of sentence so far: -16.648440819750054

4GRAM: tall
ENGLISH: P(l|tal) = -0.5281012313035025 ==> log prob of sentence so far: -19.076122721233116
FRENCH: P(l|tal) = -0.5489158370222761 ==> log prob of sentence so far: -17.197356656772328

4GRAM: alla
ENGLISH: P(a|all) = -1.159062967217916 ==> log prob of sentence so far: -20.235185688451033
FRENCH: P(a|all) = -0.5014332183984705 ==> log prob of sentence so far: -17.6987898751708

4GRAM: llat
ENGLISH: P(t|lla) = -1.093999012574477 ==> log prob of sentence so far: -21.32918470102551
FRENCH: P(t|lla) = -1.6380120626621446 ==> log prob of sentence so far: -19.336801937832945

4GRAM: lati
ENGLISH: P(i|lat) = -0.44790638502865854 ==> log prob of sentence so far: -21.77709108605417
FRENCH: P(i|lat) = -0.7503854386155915 ==> log prob of sentence so far: -20.087187376448536

4GRAM: atio
ENGLISH: P(o|ati) = -0.2906500740278565 ==> log prob of sentence so far: -22.067741160082026
FRENCH: P(o|ati) = -0.20462752790960456 ==> log prob of sentence so far: -20.29181490435814

4GRAM: tion
ENGLISH: P(n|tio) = -0.008705335315906892 ==> log prob of sentence so far: -22.07644649539793
FRENCH: P(n|tio) = -0.0022114702486365633 ==> log prob of sentence so far: -20.294026374606776

4GRAM: iond
ENGLISH: P(d|ion) = -2.1830862778996853 ==> log prob of sentence so far: -24.259532773297618
FRENCH: P(d|ion) = -0.7300604773700319 ==> log prob of sentence so far: -21.024086851976808

4GRAM: onda
ENGLISH: P(a|ond) = -1.2566936270582336 ==> log prob of sentence so far: -25.51622640035585
FRENCH: P(a|ond) = -1.1254202805592657 ==> log prob of sentence so far: -22.149507132536073

4GRAM: ndal
ENGLISH: P(l|nda) = -0.8576638292582518 ==> log prob of sentence so far: -26.3738902296141
FRENCH: P(l|nda) = -1.4734742537339356 ==> log prob of sentence so far: -23.62298138627001

4GRAM: dalt
ENGLISH: P(t|dal) = -1.4147657998273682 ==> log prob of sentence so far: -27.78865602944147
FRENCH: P(t|dal) = -1.4961951356669796 ==> log prob of sentence so far: -25.11917652193699

4GRAM: alti
ENGLISH: P(i|alt) = -1.027496173586359 ==> log prob of sentence so far: -28.81615220302783
FRENCH: P(i|alt) = -0.4983325773947628 ==> log prob of sentence so far: -25.617509099331752

4GRAM: ltim
ENGLISH: P(m|lti) = -0.7539097751944579 ==> log prob of sentence so far: -29.570061978222288
FRENCH: P(m|lti) = -1.091279367670893 ==> log prob of sentence so far: -26.708788467002645

4GRAM: time
ENGLISH: P(e|tim) = -0.10160659250112851 ==> log prob of sentence so far: -29.671668570723416
FRENCH: P(e|tim) = -0.2800426470659004 ==> log prob of sentence so far: -26.988831114068546

4GRAM: imet
ENGLISH: P(t|ime) = -0.9365137424788933 ==> log prob of sentence so far: -30.60818231320231
FRENCH: P(t|ime) = -1.2236735190316037 ==> log prob of sentence so far: -28.21250463310015

4GRAM: metr
ENGLISH: P(r|met) = -1.4348547808290983 ==> log prob of sentence so far: -32.043037094031405
FRENCH: P(r|met) = -0.717302157379231 ==> log prob of sentence so far: -28.92980679047938

4GRAM: etre
ENGLISH: P(e|etr) = -0.6754158579647782 ==> log prob of sentence so far: -32.718452951996184
FRENCH: P(e|etr) = -0.49363870588750236 ==> log prob of sentence so far: -29.42344549636688

4GRAM: tres
ENGLISH: P(s|tre) = -0.9071606189443029 ==> log prob of sentence so far: -33.62561357094049
FRENCH: P(s|tre) = -0.6725873509603395 ==> log prob of sentence so far: -30.09603284732722

4GRAM: resr
ENGLISH: P(r|res) = -2.6780164011588723 ==> log prob of sentence so far: -36.30362997209936
FRENCH: P(r|res) = -1.947399702144886 ==> log prob of sentence so far: -32.04343254947211

4GRAM: esra
ENGLISH: P(a|esr) = -0.8396427689316842 ==> log prob of sentence so far: -37.14327274103104
FRENCH: P(a|esr) = -0.8556285433600597 ==> log prob of sentence so far: -32.89906109283217

4GRAM: srad
ENGLISH: P(d|sra) = -1.6368220975871743 ==> log prob of sentence so far: -38.78009483861821
FRENCH: P(d|sra) = -1.8313967623368688 ==> log prob of sentence so far: -34.730457855169035

4GRAM: rada
ENGLISH: P(a|rad) = -1.2405919119185596 ==> log prob of sentence so far: -40.02068675053677
FRENCH: P(a|rad) = -0.7076397801499971 ==> log prob of sentence so far: -35.43809763531903

4GRAM: adar
ENGLISH: P(r|ada) = -1.2749454074761037 ==> log prob of sentence so far: -41.29563215801287
FRENCH: P(r|ada) = -3.064624353711582 ==> log prob of sentence so far: -38.50272198903061

According to the 4gram model, the sentence is in French
----------------
