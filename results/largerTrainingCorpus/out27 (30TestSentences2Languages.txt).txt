J’call malade.

1GRAM MODEL:

1GRAM: j
ENGLISH: P(j) = -2.914447804032505 ==> log prob of sentence so far: -2.914447804032505
FRENCH: P(j) = -2.262454919901824 ==> log prob of sentence so far: -2.262454919901824

1GRAM: c
ENGLISH: P(c) = -1.5877747753207887 ==> log prob of sentence so far: -4.502222579353294
FRENCH: P(c) = -1.4990816758627588 ==> log prob of sentence so far: -3.761536595764583

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -5.600547220125226
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -4.832192075342619

1GRAM: l
ENGLISH: P(l) = -1.3835976742909466 ==> log prob of sentence so far: -6.984144894416173
FRENCH: P(l) = -1.2161248223608845 ==> log prob of sentence so far: -6.048316897703504

1GRAM: l
ENGLISH: P(l) = -1.3835976742909466 ==> log prob of sentence so far: -8.367742568707119
FRENCH: P(l) = -1.2161248223608845 ==> log prob of sentence so far: -7.264441720064388

1GRAM: m
ENGLISH: P(m) = -1.6019350435333317 ==> log prob of sentence so far: -9.96967761224045
FRENCH: P(m) = -1.4846148337842204 ==> log prob of sentence so far: -8.74905655384861

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -11.068002253012382
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -9.819712033426645

1GRAM: l
ENGLISH: P(l) = -1.3835976742909466 ==> log prob of sentence so far: -12.451599927303329
FRENCH: P(l) = -1.2161248223608845 ==> log prob of sentence so far: -11.03583685578753

1GRAM: a
ENGLISH: P(a) = -1.0983246407719325 ==> log prob of sentence so far: -13.549924568075262
FRENCH: P(a) = -1.0706554795780365 ==> log prob of sentence so far: -12.106492335365566

1GRAM: d
ENGLISH: P(d) = -1.3852685001126093 ==> log prob of sentence so far: -14.935193068187871
FRENCH: P(d) = -1.4087138708407954 ==> log prob of sentence so far: -13.515206206206361

1GRAM: e
ENGLISH: P(e) = -0.9019354374008705 ==> log prob of sentence so far: -15.837128505588742
FRENCH: P(e) = -0.8055692770289 ==> log prob of sentence so far: -14.320775483235261

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: jc
ENGLISH: P(c|j) = -3.2400497721126476 ==> log prob of sentence so far: -3.2400497721126476
FRENCH: P(c|j) = -2.649910262599007 ==> log prob of sentence so far: -2.649910262599007

2GRAM: ca
ENGLISH: P(a|c) = -0.9139239393391472 ==> log prob of sentence so far: -4.153973711451795
FRENCH: P(a|c) = -1.1026340699603883 ==> log prob of sentence so far: -3.7525443325593955

2GRAM: al
ENGLISH: P(l|a) = -1.0528481362365971 ==> log prob of sentence so far: -5.206821847688392
FRENCH: P(l|a) = -1.2310228913879266 ==> log prob of sentence so far: -4.983567223947322

2GRAM: ll
ENGLISH: P(l|l) = -0.8546831385767429 ==> log prob of sentence so far: -6.061504986265135
FRENCH: P(l|l) = -0.9404871687407055 ==> log prob of sentence so far: -5.924054392688028

2GRAM: lm
ENGLISH: P(m|l) = -1.9089435862630626 ==> log prob of sentence so far: -7.9704485725281975
FRENCH: P(m|l) = -2.120014627742949 ==> log prob of sentence so far: -8.044069020430976

2GRAM: ma
ENGLISH: P(a|m) = -0.7052977882442032 ==> log prob of sentence so far: -8.6757463607724
FRENCH: P(a|m) = -0.6714285273671705 ==> log prob of sentence so far: -8.715497547798147

2GRAM: al
ENGLISH: P(l|a) = -1.0528481362365971 ==> log prob of sentence so far: -9.728594497008999
FRENCH: P(l|a) = -1.2310228913879266 ==> log prob of sentence so far: -9.946520439186074

2GRAM: la
ENGLISH: P(a|l) = -0.9671379229058816 ==> log prob of sentence so far: -10.69573241991488
FRENCH: P(a|l) = -0.6845081303579077 ==> log prob of sentence so far: -10.631028569543982

2GRAM: ad
ENGLISH: P(d|a) = -1.3449915249632305 ==> log prob of sentence so far: -12.04072394487811
FRENCH: P(d|a) = -1.4692731976432378 ==> log prob of sentence so far: -12.10030176718722

2GRAM: de
ENGLISH: P(e|d) = -0.8186179792453097 ==> log prob of sentence so far: -12.85934192412342
FRENCH: P(e|d) = -0.3138194685480035 ==> log prob of sentence so far: -12.414121235735223

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: jca
ENGLISH: P(a|jc) = -1.5314789170422551 ==> log prob of sentence so far: -1.5314789170422551
FRENCH: P(a|jc) = -1.6690067809585756 ==> log prob of sentence so far: -1.6690067809585756

3GRAM: cal
ENGLISH: P(l|ca) = -0.6945366917176442 ==> log prob of sentence so far: -2.2260156087598992
FRENCH: P(l|ca) = -1.1950691816056753 ==> log prob of sentence so far: -2.864075962564251

3GRAM: all
ENGLISH: P(l|al) = -0.44315815965265565 ==> log prob of sentence so far: -2.6691737684125547
FRENCH: P(l|al) = -0.7398897448849626 ==> log prob of sentence so far: -3.6039657074492135

3GRAM: llm
ENGLISH: P(m|ll) = -1.6144060679680226 ==> log prob of sentence so far: -4.283579836380577
FRENCH: P(m|ll) = -3.381095015821219 ==> log prob of sentence so far: -6.985060723270433

3GRAM: lma
ENGLISH: P(a|lm) = -0.7441667530732763 ==> log prob of sentence so far: -5.027746589453853
FRENCH: P(a|lm) = -0.4686396454706033 ==> log prob of sentence so far: -7.453700368741036

3GRAM: mal
ENGLISH: P(l|ma) = -1.2482397478267662 ==> log prob of sentence so far: -6.275986337280619
FRENCH: P(l|ma) = -1.154955162788934 ==> log prob of sentence so far: -8.60865553152997

3GRAM: ala
ENGLISH: P(a|al) = -1.2779670818089546 ==> log prob of sentence so far: -7.553953419089574
FRENCH: P(a|al) = -0.9405883793819289 ==> log prob of sentence so far: -9.5492439109119

3GRAM: lad
ENGLISH: P(d|la) = -1.357393592924784 ==> log prob of sentence so far: -8.911347012014357
FRENCH: P(d|la) = -1.3152057985820542 ==> log prob of sentence so far: -10.864449709493954

3GRAM: ade
ENGLISH: P(e|ad) = -0.7248683580769698 ==> log prob of sentence so far: -9.636215370091326
FRENCH: P(e|ad) = -0.6500643591577443 ==> log prob of sentence so far: -11.514514068651698

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: jcal
ENGLISH: P(l|jca) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
FRENCH: P(l|jca) = -1.4471580313422192 ==> log prob of sentence so far: -1.4471580313422192

4GRAM: call
ENGLISH: P(l|cal) = -0.39136155596842054 ==> log prob of sentence so far: -1.8227253201274078
FRENCH: P(l|cal) = -1.5867603648381825 ==> log prob of sentence so far: -3.0339183961804017

4GRAM: allm
ENGLISH: P(m|all) = -1.6172541328274532 ==> log prob of sentence so far: -3.4399794529548613
FRENCH: P(m|all) = -2.96399758078209 ==> log prob of sentence so far: -5.997915976962492

4GRAM: llma
ENGLISH: P(a|llm) = -0.5979916291935496 ==> log prob of sentence so far: -4.037971082148411
FRENCH: P(a|llm) = -0.6746106584765741 ==> log prob of sentence so far: -6.672526635439066

4GRAM: lmal
ENGLISH: P(l|lma) = -1.7894322607933326 ==> log prob of sentence so far: -5.827403342941744
FRENCH: P(l|lma) = -1.3296483927857774 ==> log prob of sentence so far: -8.002175028224844

4GRAM: mala
ENGLISH: P(a|mal) = -1.3563883369590302 ==> log prob of sentence so far: -7.183791679900774
FRENCH: P(a|mal) = -0.8905955907306294 ==> log prob of sentence so far: -8.892770618955474

4GRAM: alad
ENGLISH: P(d|ala) = -1.3563932607094484 ==> log prob of sentence so far: -8.540184940610223
FRENCH: P(d|ala) = -0.9456024694578945 ==> log prob of sentence so far: -9.838373088413368

4GRAM: lade
ENGLISH: P(e|lad) = -0.8329380574861189 ==> log prob of sentence so far: -9.373122998096342
FRENCH: P(e|lad) = -0.6709263177051651 ==> log prob of sentence so far: -10.509299406118533

According to the 4gram model, the sentence is in English
----------------
