I'm OK.

1GRAM MODEL:

1GRAM: i
ENGLISH: P(i) = -1.15170480747376 ==> log prob of sentence so far: -1.15170480747376
FRENCH: P(i) = -1.1290925204027853 ==> log prob of sentence so far: -1.1290925204027853

1GRAM: m
ENGLISH: P(m) = -1.6019350435333317 ==> log prob of sentence so far: -2.7536398510070916
FRENCH: P(m) = -1.4846148337842204 ==> log prob of sentence so far: -2.6137073541870057

1GRAM: o
ENGLISH: P(o) = -1.1241820463462655 ==> log prob of sentence so far: -3.877821897353357
FRENCH: P(o) = -1.265193765366532 ==> log prob of sentence so far: -3.8789011195535377

1GRAM: k
ENGLISH: P(k) = -2.1676102268694497 ==> log prob of sentence so far: -6.045432124222806
FRENCH: P(k) = -3.5947034117295056 ==> log prob of sentence so far: -7.473604531283043

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: im
ENGLISH: P(m|i) = -1.3659132327876533 ==> log prob of sentence so far: -1.3659132327876533
FRENCH: P(m|i) = -1.535803189494064 ==> log prob of sentence so far: -1.535803189494064

2GRAM: mo
ENGLISH: P(o|m) = -0.9049335075240692 ==> log prob of sentence so far: -2.2708467403117227
FRENCH: P(o|m) = -0.9364974732642889 ==> log prob of sentence so far: -2.472300662758353

2GRAM: ok
ENGLISH: P(k|o) = -1.9296021921934259 ==> log prob of sentence so far: -4.200448932505148
FRENCH: P(k|o) = -3.9371233169304776 ==> log prob of sentence so far: -6.40942397968883

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: imo
ENGLISH: P(o|im) = -1.55967608534555 ==> log prob of sentence so far: -1.55967608534555
FRENCH: P(o|im) = -1.2446065839208351 ==> log prob of sentence so far: -1.2446065839208351

3GRAM: mok
ENGLISH: P(k|mo) = -2.121879149539587 ==> log prob of sentence so far: -3.6815552348851366
FRENCH: P(k|mo) = -4.073645045513152 ==> log prob of sentence so far: -5.318251629433988

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: imok
ENGLISH: P(k|imo) = -3.0236639181977933 ==> log prob of sentence so far: -3.0236639181977933
FRENCH: P(k|imo) = -3.0681858617461617 ==> log prob of sentence so far: -3.0681858617461617

According to the 4gram model, the sentence is in English
----------------
