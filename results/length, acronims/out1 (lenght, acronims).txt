Etienne loves Veronique.

1GRAM MODEL:

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -0.9100712180599071
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -0.7669658145224565

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -1.9442948558441795
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -1.93293256261921

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -3.1068203361180906
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -3.0681390052880535

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -4.016891554177998
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -3.83510481981051

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -5.178488491580627
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -4.957742296425015

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -6.340085428983256
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -6.080379773039519

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -7.2501566470431635
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -6.847345587561976

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -8.597925532464163
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -8.115558067847498

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -9.735709293049657
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -9.389901398165152

1GRAM: v
ENGLISH: P(v) = -2.043619342325931 ==> log prob of sentence so far: -11.779328635375588
FRENCH: P(v) = -1.82784228094936 ==> log prob of sentence so far: -11.217743679114513

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -12.689399853435495
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -11.98470949363697

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -13.860540866164095
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -13.049157208526626

1GRAM: v
ENGLISH: P(v) = -2.043619342325931 ==> log prob of sentence so far: -15.904160208490026
FRENCH: P(v) = -1.82784228094936 ==> log prob of sentence so far: -14.876999489475986

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -16.814231426549934
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -15.643965303998442

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -18.07550432786203
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -16.8279939430689

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -19.213288088447527
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -18.102337273386556

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -20.374885025850155
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -19.22497475000106

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -21.537410506124065
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -20.360181192669902

1GRAM: q
ENGLISH: P(q) = -2.7879988169444423 ==> log prob of sentence so far: -24.325409323068506
FRENCH: P(q) = -1.9394381295606844 ==> log prob of sentence so far: -22.299619322230587

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -25.87789749202642
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -23.50575438340621

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -26.78796871008633
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -24.272720197928667

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: et
ENGLISH: P(t|e) = -1.1805749996736075 ==> log prob of sentence so far: -1.1805749996736075
FRENCH: P(t|e) = -1.081730001747917 ==> log prob of sentence so far: -1.081730001747917

2GRAM: ti
ENGLISH: P(i|t) = -1.0855982270227826 ==> log prob of sentence so far: -2.26617322669639
FRENCH: P(i|t) = -0.9860184878476054 ==> log prob of sentence so far: -2.0677484895955223

2GRAM: ie
ENGLISH: P(e|i) = -1.5107075344419896 ==> log prob of sentence so far: -3.7768807611383792
FRENCH: P(e|i) = -0.9021220228745312 ==> log prob of sentence so far: -2.9698705124700533

2GRAM: en
ENGLISH: P(n|e) = -1.0655182392646758 ==> log prob of sentence so far: -4.842399000403055
FRENCH: P(n|e) = -0.8688224207402516 ==> log prob of sentence so far: -3.838692933210305

2GRAM: nn
ENGLISH: P(n|n) = -1.9288837797167968 ==> log prob of sentence so far: -6.771282780119852
FRENCH: P(n|n) = -1.4789797928128527 ==> log prob of sentence so far: -5.317672726023158

2GRAM: ne
ENGLISH: P(e|n) = -1.0532701120323171 ==> log prob of sentence so far: -7.824552892152169
FRENCH: P(e|n) = -0.7543075705657655 ==> log prob of sentence so far: -6.071980296588923

2GRAM: el
ENGLISH: P(l|e) = -1.3277804884685662 ==> log prob of sentence so far: -9.152333380620735
FRENCH: P(l|e) = -1.2044345212229748 ==> log prob of sentence so far: -7.276414817811898

2GRAM: lo
ENGLISH: P(o|l) = -1.0313867050170902 ==> log prob of sentence so far: -10.183720085637825
FRENCH: P(o|l) = -1.1639759810063668 ==> log prob of sentence so far: -8.440390798818266

2GRAM: ov
ENGLISH: P(v|o) = -1.7957558593393887 ==> log prob of sentence so far: -11.979475944977214
FRENCH: P(v|o) = -2.526669664991859 ==> log prob of sentence so far: -10.967060463810125

2GRAM: ve
ENGLISH: P(e|v) = -0.17428026499063828 ==> log prob of sentence so far: -12.153756209967852
FRENCH: P(e|v) = -0.4815039345822809 ==> log prob of sentence so far: -11.448564398392406

2GRAM: es
ENGLISH: P(s|e) = -0.944867640322014 ==> log prob of sentence so far: -13.098623850289867
FRENCH: P(s|e) = -0.7261499946209886 ==> log prob of sentence so far: -12.174714393013396

2GRAM: sv
ENGLISH: P(v|s) = -2.539334750977554 ==> log prob of sentence so far: -15.637958601267421
FRENCH: P(v|s) = -1.956488342986067 ==> log prob of sentence so far: -14.131202735999462

2GRAM: ve
ENGLISH: P(e|v) = -0.17428026499063828 ==> log prob of sentence so far: -15.81223886625806
FRENCH: P(e|v) = -0.4815039345822809 ==> log prob of sentence so far: -14.612706670581744

2GRAM: er
ENGLISH: P(r|e) = -0.8561613604222861 ==> log prob of sentence so far: -16.668400226680347
FRENCH: P(r|e) = -1.0504664166531121 ==> log prob of sentence so far: -15.663173087234856

2GRAM: ro
ENGLISH: P(o|r) = -0.9679454985579867 ==> log prob of sentence so far: -17.636345725238336
FRENCH: P(o|r) = -1.1139096014103482 ==> log prob of sentence so far: -16.777082688645205

2GRAM: on
ENGLISH: P(n|o) = -0.8622085952026249 ==> log prob of sentence so far: -18.49855432044096
FRENCH: P(n|o) = -0.5205456677173079 ==> log prob of sentence so far: -17.297628356362512

2GRAM: ni
ENGLISH: P(i|n) = -1.3332322954338982 ==> log prob of sentence so far: -19.831786615874858
FRENCH: P(i|n) = -1.4845526185364621 ==> log prob of sentence so far: -18.782180974898974

2GRAM: iq
ENGLISH: P(q|i) = -3.179393289527697 ==> log prob of sentence so far: -23.011179905402557
FRENCH: P(q|i) = -1.7047366261391819 ==> log prob of sentence so far: -20.486917601038158

2GRAM: qu
ENGLISH: P(u|q) = -0.0037433435301242783 ==> log prob of sentence so far: -23.014923248932682
FRENCH: P(u|q) = -0.00832433543958775 ==> log prob of sentence so far: -20.495241936477747

2GRAM: ue
ENGLISH: P(e|u) = -1.3246679556129457 ==> log prob of sentence so far: -24.339591204545627
FRENCH: P(e|u) = -0.8812512722642089 ==> log prob of sentence so far: -21.376493208741955

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: eti
ENGLISH: P(i|et) = -1.1129191759155594 ==> log prob of sentence so far: -1.1129191759155594
FRENCH: P(i|et) = -1.2501654504528017 ==> log prob of sentence so far: -1.2501654504528017

3GRAM: tie
ENGLISH: P(e|ti) = -1.5553852998310138 ==> log prob of sentence so far: -2.6683044757465733
FRENCH: P(e|ti) = -1.1287066528767673 ==> log prob of sentence so far: -2.378872103329569

3GRAM: ien
ENGLISH: P(n|ie) = -0.8538414328807916 ==> log prob of sentence so far: -3.522145908627365
FRENCH: P(n|ie) = -0.407374576625151 ==> log prob of sentence so far: -2.7862466799547203

3GRAM: enn
ENGLISH: P(n|en) = -2.0813427203721244 ==> log prob of sentence so far: -5.603488628999489
FRENCH: P(n|en) = -1.8006731846590913 ==> log prob of sentence so far: -4.586919864613812

3GRAM: nne
ENGLISH: P(e|nn) = -0.5507814951358535 ==> log prob of sentence so far: -6.154270124135343
FRENCH: P(e|nn) = -0.25126792865900743 ==> log prob of sentence so far: -4.8381877932728194

3GRAM: nel
ENGLISH: P(l|ne) = -1.6422024576108396 ==> log prob of sentence so far: -7.796472581746183
FRENCH: P(l|ne) = -1.4206738468176074 ==> log prob of sentence so far: -6.2588616400904264

3GRAM: elo
ENGLISH: P(o|el) = -1.0284913035250804 ==> log prob of sentence so far: -8.824963885271263
FRENCH: P(o|el) = -1.1623020489331568 ==> log prob of sentence so far: -7.421163689023583

3GRAM: lov
ENGLISH: P(v|lo) = -1.5902409874322145 ==> log prob of sentence so far: -10.415204872703477
FRENCH: P(v|lo) = -3.0108085975122063 ==> log prob of sentence so far: -10.431972286535789

3GRAM: ove
ENGLISH: P(e|ov) = -0.054785174934942456 ==> log prob of sentence so far: -10.469990047638419
FRENCH: P(e|ov) = -0.5880492768851477 ==> log prob of sentence so far: -11.020021563420936

3GRAM: ves
ENGLISH: P(s|ve) = -0.985340464767245 ==> log prob of sentence so far: -11.455330512405663
FRENCH: P(s|ve) = -1.2677150959949515 ==> log prob of sentence so far: -12.287736659415888

3GRAM: esv
ENGLISH: P(v|es) = -3.075963850776022 ==> log prob of sentence so far: -14.531294363181685
FRENCH: P(v|es) = -1.8501693273971493 ==> log prob of sentence so far: -14.137905986813037

3GRAM: sve
ENGLISH: P(e|sv) = -0.44761065721112403 ==> log prob of sentence so far: -14.978905020392808
FRENCH: P(e|sv) = -0.5468567248262058 ==> log prob of sentence so far: -14.684762711639243

3GRAM: ver
ENGLISH: P(r|ve) = -0.3855967161664635 ==> log prob of sentence so far: -15.364501736559273
FRENCH: P(r|ve) = -0.5174611042546211 ==> log prob of sentence so far: -15.202223815893865

3GRAM: ero
ENGLISH: P(o|er) = -1.332688208238226 ==> log prob of sentence so far: -16.6971899447975
FRENCH: P(o|er) = -1.5189151162273014 ==> log prob of sentence so far: -16.721138932121164

3GRAM: ron
ENGLISH: P(n|ro) = -1.1323618515057796 ==> log prob of sentence so far: -17.82955179630328
FRENCH: P(n|ro) = -0.8429145538567797 ==> log prob of sentence so far: -17.564053485977944

3GRAM: oni
ENGLISH: P(i|on) = -1.4217738275625775 ==> log prob of sentence so far: -19.25132562386586
FRENCH: P(i|on) = -1.7769662715259649 ==> log prob of sentence so far: -19.341019757503908

3GRAM: niq
ENGLISH: P(q|ni) = -3.0784568180532927 ==> log prob of sentence so far: -22.329782441919154
FRENCH: P(q|ni) = -1.3178101758288643 ==> log prob of sentence so far: -20.658829933332772

3GRAM: iqu
ENGLISH: P(u|iq) = -0.10969877005156307 ==> log prob of sentence so far: -22.439481211970715
FRENCH: P(u|iq) = -0.005397714049644559 ==> log prob of sentence so far: -20.664227647382415

3GRAM: que
ENGLISH: P(e|qu) = -0.3018595937804336 ==> log prob of sentence so far: -22.74134080575115
FRENCH: P(e|qu) = -0.2536416357055235 ==> log prob of sentence so far: -20.91786928308794

According to the 3gram model, the sentence is in French
----------------
4GRAM MODEL:

4GRAM: etie
ENGLISH: P(e|eti) = -1.5730962953926457 ==> log prob of sentence so far: -1.5730962953926457
FRENCH: P(e|eti) = -1.2223670008495782 ==> log prob of sentence so far: -1.2223670008495782

4GRAM: tien
ENGLISH: P(n|tie) = -0.951151432462183 ==> log prob of sentence so far: -2.5242477278548288
FRENCH: P(n|tie) = -0.599706085934485 ==> log prob of sentence so far: -1.822073086784063

4GRAM: ienn
ENGLISH: P(n|ien) = -2.7723217067229196 ==> log prob of sentence so far: -5.296569434577748
FRENCH: P(n|ien) = -1.1792367061973916 ==> log prob of sentence so far: -3.0013097929814547

4GRAM: enne
ENGLISH: P(e|enn) = -0.6690067809585756 ==> log prob of sentence so far: -5.9655762155363234
FRENCH: P(e|enn) = -0.12001284846699084 ==> log prob of sentence so far: -3.1213226414484456

4GRAM: nnel
ENGLISH: P(l|nne) = -1.4285159212861611 ==> log prob of sentence so far: -7.394092136822485
FRENCH: P(l|nne) = -1.133360223125194 ==> log prob of sentence so far: -4.25468286457364

4GRAM: nelo
ENGLISH: P(o|nel) = -0.8968410377149421 ==> log prob of sentence so far: -8.290933174537427
FRENCH: P(o|nel) = -1.1561410321012917 ==> log prob of sentence so far: -5.410823896674932

4GRAM: elov
ENGLISH: P(v|elo) = -1.4653828514484184 ==> log prob of sentence so far: -9.756316025985846
FRENCH: P(v|elo) = -3.0145205387579237 ==> log prob of sentence so far: -8.425344435432855

4GRAM: love
ENGLISH: P(e|lov) = -0.09076311859744 ==> log prob of sentence so far: -9.847079144583287
FRENCH: P(e|lov) = -1.4771212547196624 ==> log prob of sentence so far: -9.902465690152518

4GRAM: oves
ENGLISH: P(s|ove) = -1.420285884941918 ==> log prob of sentence so far: -11.267365029525205
FRENCH: P(s|ove) = -1.9444826721501687 ==> log prob of sentence so far: -11.846948362302687

4GRAM: vesv
ENGLISH: P(v|ves) = -2.582063362911709 ==> log prob of sentence so far: -13.849428392436915
FRENCH: P(v|ves) = -2.575187844927661 ==> log prob of sentence so far: -14.422136207230349

4GRAM: esve
ENGLISH: P(e|esv) = -0.7085153222422492 ==> log prob of sentence so far: -14.557943714679164
FRENCH: P(e|esv) = -0.493066817665408 ==> log prob of sentence so far: -14.915203024895757

4GRAM: sver
ENGLISH: P(r|sve) = -0.18293068358598671 ==> log prob of sentence so far: -14.74087439826515
FRENCH: P(r|sve) = -0.24061407319232575 ==> log prob of sentence so far: -15.155817098088082

4GRAM: vero
ENGLISH: P(o|ver) = -1.768839832836859 ==> log prob of sentence so far: -16.50971423110201
FRENCH: P(o|ver) = -2.0388919422683296 ==> log prob of sentence so far: -17.19470904035641

4GRAM: eron
ENGLISH: P(n|ero) = -0.9253663817030958 ==> log prob of sentence so far: -17.435080612805105
FRENCH: P(n|ero) = -0.4818529145563464 ==> log prob of sentence so far: -17.676561954912756

4GRAM: roni
ENGLISH: P(i|ron) = -1.1408381471117168 ==> log prob of sentence so far: -18.57591875991682
FRENCH: P(i|ron) = -1.4636797336504643 ==> log prob of sentence so far: -19.140241688563222

4GRAM: oniq
ENGLISH: P(q|oni) = -2.8561244442423 ==> log prob of sentence so far: -21.432043204159122
FRENCH: P(q|oni) = -0.8857158909225209 ==> log prob of sentence so far: -20.02595757948574

4GRAM: niqu
ENGLISH: P(u|niq) = -0.7781512503836436 ==> log prob of sentence so far: -22.210194454542766
FRENCH: P(u|niq) = -0.06126965673892266 ==> log prob of sentence so far: -20.087227236224663

4GRAM: ique
ENGLISH: P(e|iqu) = -0.39600550889483793 ==> log prob of sentence so far: -22.606199963437604
FRENCH: P(e|iqu) = -0.09911496774854345 ==> log prob of sentence so far: -20.186342203973208

According to the 4gram model, the sentence is in French
----------------
