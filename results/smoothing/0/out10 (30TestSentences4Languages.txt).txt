J'aime l'IA.

1GRAM MODEL:

1GRAM: j
ENGLISH: P(j) = -2.945027151073803 ==> log prob of sentence so far: -2.945027151073803
FRENCH: P(j) = -2.209620279826156 ==> log prob of sentence so far: -2.209620279826156
DUTCH: P(j) = -1.747527435865423 ==> log prob of sentence so far: -1.747527435865423
PORTUGUESE: P(j) = -2.5149569506577025 ==> log prob of sentence so far: -2.5149569506577025
SPANISH: P(j) = -2.339954877008437 ==> log prob of sentence so far: -2.339954877008437

1GRAM: a
ENGLISH: P(a) = -1.08678555499431 ==> log prob of sentence so far: -4.031812706068113
FRENCH: P(a) = -1.076348685206508 ==> log prob of sentence so far: -3.285968965032664
DUTCH: P(a) = -1.1153867013427927 ==> log prob of sentence so far: -2.8629141372082154
PORTUGUESE: P(a) = -0.8318864420456407 ==> log prob of sentence so far: -3.3468433927033434
SPANISH: P(a) = -0.9020150997282854 ==> log prob of sentence so far: -3.2419699767367227

1GRAM: i
ENGLISH: P(i) = -1.162522881728433 ==> log prob of sentence so far: -5.194335587796546
FRENCH: P(i) = -1.1352025747557652 ==> log prob of sentence so far: -4.421171539788429
DUTCH: P(i) = -1.226244226879294 ==> log prob of sentence so far: -4.08915836408751
PORTUGUESE: P(i) = -1.2396906104867749 ==> log prob of sentence so far: -4.586534003190119
SPANISH: P(i) = -1.223148951725382 ==> log prob of sentence so far: -4.465118928462105

1GRAM: m
ENGLISH: P(m) = -1.6111138481089953 ==> log prob of sentence so far: -6.805449435905541
FRENCH: P(m) = -1.5129506664806158 ==> log prob of sentence so far: -5.934122206269045
DUTCH: P(m) = -1.6188728818507347 ==> log prob of sentence so far: -5.708031245938244
PORTUGUESE: P(m) = -1.3143867564844387 ==> log prob of sentence so far: -5.900920759674557
SPANISH: P(m) = -1.4970849452515298 ==> log prob of sentence so far: -5.9622038737136345

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -7.715516602343268
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -6.7010817080165035
DUTCH: P(e) = -0.7296416695657266 ==> log prob of sentence so far: -6.4376729155039705
PORTUGUESE: P(e) = -0.8804373935970373 ==> log prob of sentence so far: -6.7813581532715945
SPANISH: P(e) = -0.8816440697134942 ==> log prob of sentence so far: -6.843847943427129

1GRAM: l
ENGLISH: P(l) = -1.3477680403069134 ==> log prob of sentence so far: -9.063284642650181
FRENCH: P(l) = -1.2682101448093062 ==> log prob of sentence so far: -7.96929185282581
DUTCH: P(l) = -1.424888573441733 ==> log prob of sentence so far: -7.862561488945703
PORTUGUESE: P(l) = -1.4370345300766503 ==> log prob of sentence so far: -8.218392683348245
SPANISH: P(l) = -1.2802163050487747 ==> log prob of sentence so far: -8.124064248475904

1GRAM: i
ENGLISH: P(i) = -1.162522881728433 ==> log prob of sentence so far: -10.225807524378615
FRENCH: P(i) = -1.1352025747557652 ==> log prob of sentence so far: -9.104494427581574
DUTCH: P(i) = -1.226244226879294 ==> log prob of sentence so far: -9.088805715824996
PORTUGUESE: P(i) = -1.2396906104867749 ==> log prob of sentence so far: -9.45808329383502
SPANISH: P(i) = -1.223148951725382 ==> log prob of sentence so far: -9.347213200201285

1GRAM: a
ENGLISH: P(a) = -1.08678555499431 ==> log prob of sentence so far: -11.312593079372924
FRENCH: P(a) = -1.076348685206508 ==> log prob of sentence so far: -10.180843112788082
DUTCH: P(a) = -1.1153867013427927 ==> log prob of sentence so far: -10.20419241716779
PORTUGUESE: P(a) = -0.8318864420456407 ==> log prob of sentence so far: -10.28996973588066
SPANISH: P(a) = -0.9020150997282854 ==> log prob of sentence so far: -10.24922829992957

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: ja
ENGLISH: P(a|j) = -0.6879246810381339 ==> log prob of sentence so far: -0.6879246810381339
FRENCH: P(a|j) = -0.811039320749869 ==> log prob of sentence so far: -0.811039320749869
DUTCH: P(a|j) = -1.215711617740223 ==> log prob of sentence so far: -1.215711617740223
PORTUGUESE: P(a|j) = -0.3740951959257801 ==> log prob of sentence so far: -0.3740951959257801
SPANISH: P(a|j) = -0.57273679737472 ==> log prob of sentence so far: -0.57273679737472

2GRAM: ai
ENGLISH: P(i|a) = -1.3482451331022975 ==> log prob of sentence so far: -2.0361698141404316
FRENCH: P(i|a) = -0.6965245704990287 ==> log prob of sentence so far: -1.5075638912488976
DUTCH: P(i|a) = -2.2566352902183815 ==> log prob of sentence so far: -3.4723469079586042
PORTUGUESE: P(i|a) = -1.474077185559241 ==> log prob of sentence so far: -1.848172381485021
SPANISH: P(i|a) = -1.9696512916914282 ==> log prob of sentence so far: -2.542388089066148

2GRAM: im
ENGLISH: P(m|i) = -1.3479746512911457 ==> log prob of sentence so far: -3.3841444654315773
FRENCH: P(m|i) = -1.5427405149491051 ==> log prob of sentence so far: -3.050304406198003
DUTCH: P(m|i) = -2.1539211657725237 ==> log prob of sentence so far: -5.6262680737311275
PORTUGUESE: P(m|i) = -1.1489542570311593 ==> log prob of sentence so far: -2.99712663851618
SPANISH: P(m|i) = -1.459101553906209 ==> log prob of sentence so far: -4.001489642972357

2GRAM: me
ENGLISH: P(e|m) = -0.5943275410972717 ==> log prob of sentence so far: -3.978472006528849
FRENCH: P(e|m) = -0.4303478415391602 ==> log prob of sentence so far: -3.4806522477371633
DUTCH: P(e|m) = -0.45919090730009965 ==> log prob of sentence so far: -6.085458981031227
PORTUGUESE: P(e|m) = -0.7090560275933477 ==> log prob of sentence so far: -3.7061826661095276
SPANISH: P(e|m) = -0.5843535824453191 ==> log prob of sentence so far: -4.585843225417676

2GRAM: el
ENGLISH: P(l|e) = -1.327771460508567 ==> log prob of sentence so far: -5.306243467037416
FRENCH: P(l|e) = -1.2044160454405197 ==> log prob of sentence so far: -4.685068293177683
DUTCH: P(l|e) = -1.163689922712868 ==> log prob of sentence so far: -7.249148903744095
PORTUGUESE: P(l|e) = -1.1286515605479783 ==> log prob of sentence so far: -4.834834226657506
SPANISH: P(l|e) = -0.9033131407941831 ==> log prob of sentence so far: -5.489156366211859

2GRAM: li
ENGLISH: P(i|l) = -0.9280383126561762 ==> log prob of sentence so far: -6.2342817796935925
FRENCH: P(i|l) = -1.2029072596544539 ==> log prob of sentence so far: -5.887975552832137
DUTCH: P(i|l) = -0.7909618702720241 ==> log prob of sentence so far: -8.040110774016119
PORTUGUESE: P(i|l) = -1.0725941137341026 ==> log prob of sentence so far: -5.907428340391609
SPANISH: P(i|l) = -1.2536148905646234 ==> log prob of sentence so far: -6.742771256776483

2GRAM: ia
ENGLISH: P(a|i) = -1.7533386780423903 ==> log prob of sentence so far: -7.987620457735983
FRENCH: P(a|i) = -1.774478367302253 ==> log prob of sentence so far: -7.66245392013439
DUTCH: P(a|i) = -2.0509931190754678 ==> log prob of sentence so far: -10.091103893091587
PORTUGUESE: P(a|i) = -0.8371888699604121 ==> log prob of sentence so far: -6.744617210352021
SPANISH: P(a|i) = -0.8671992637454082 ==> log prob of sentence so far: -7.609970520521891

According to the 2gram model, the sentence is in Portuguese
----------------
3GRAM MODEL:

3GRAM: jai
ENGLISH: P(i|ja) = -1.8711836083284983 ==> log prob of sentence so far: -1.8711836083284983
FRENCH: P(i|ja) = -0.7302963480462423 ==> log prob of sentence so far: -0.7302963480462423
DUTCH: P(i|ja) = -Infinity ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(i|ja) = -1.9731278535996988 ==> log prob of sentence so far: -1.9731278535996988
SPANISH: P(i|ja) = -2.629409599102719 ==> log prob of sentence so far: -2.629409599102719

3GRAM: aim
ENGLISH: P(m|ai) = -1.9516363624216115 ==> log prob of sentence so far: -3.8228199707501096
FRENCH: P(m|ai) = -2.0026128668184517 ==> log prob of sentence so far: -2.7329092148646943
DUTCH: P(m|ai) = -2.2405492482826 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(m|ai) = -1.393473172379443 ==> log prob of sentence so far: -3.3666010259791417
SPANISH: P(m|ai) = -1.1715436313049605 ==> log prob of sentence so far: -3.8009532304076794

3GRAM: ime
ENGLISH: P(e|im) = -0.5922864259649037 ==> log prob of sentence so far: -4.4151063967150135
FRENCH: P(e|im) = -0.48255000643106066 ==> log prob of sentence so far: -3.215459221295755
DUTCH: P(e|im) = -0.5969804657986875 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(e|im) = -0.4923079238004898 ==> log prob of sentence so far: -3.8589089497796314
SPANISH: P(e|im) = -0.7059096110877987 ==> log prob of sentence so far: -4.506862841495478

3GRAM: mel
ENGLISH: P(l|me) = -1.6005944726375578 ==> log prob of sentence so far: -6.015700869352571
FRENCH: P(l|me) = -1.505924812569867 ==> log prob of sentence so far: -4.721384033865622
DUTCH: P(l|me) = -1.5131663589043018 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(l|me) = -1.22666327964849 ==> log prob of sentence so far: -5.085572229428122
SPANISH: P(l|me) = -0.7073802503295236 ==> log prob of sentence so far: -5.214243091825002

3GRAM: eli
ENGLISH: P(i|el) = -0.8552763131021409 ==> log prob of sentence so far: -6.870977182454712
FRENCH: P(i|el) = -1.2612480325304063 ==> log prob of sentence so far: -5.982632066396028
DUTCH: P(i|el) = -0.692111912115371 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(i|el) = -1.1054591902907394 ==> log prob of sentence so far: -6.191031419718861
SPANISH: P(i|el) = -1.451212515558259 ==> log prob of sentence so far: -6.665455607383261

3GRAM: lia
ENGLISH: P(a|li) = -1.4735738550267274 ==> log prob of sentence so far: -8.34455103748144
FRENCH: P(a|li) = -1.6112946328621893 ==> log prob of sentence so far: -7.593926699258217
DUTCH: P(a|li) = -2.6960067152185454 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(a|li) = -0.8559479888680575 ==> log prob of sentence so far: -7.046979408586918
SPANISH: P(a|li) = -1.0156253078961996 ==> log prob of sentence so far: -7.681080915279461

According to the 3gram model, the sentence is in Portuguese
----------------
4GRAM MODEL:

4GRAM: jaim
ENGLISH: P(m|jai) = -Infinity ==> log prob of sentence so far: -Infinity
FRENCH: P(m|jai) = -1.135662602000073 ==> log prob of sentence so far: -1.135662602000073
DUTCH: P(m|jai) = NaN ==> log prob of sentence so far: NaN
PORTUGUESE: P(m|jai) = -Infinity ==> log prob of sentence so far: -Infinity
SPANISH: P(m|jai) = 0.0 ==> log prob of sentence so far: 0.0

4GRAM: aime
ENGLISH: P(e|aim) = -0.22933677100890631 ==> log prob of sentence so far: -Infinity
FRENCH: P(e|aim) = -0.4441209946167371 ==> log prob of sentence so far: -1.5797835966168101
DUTCH: P(e|aim) = -Infinity ==> log prob of sentence so far: NaN
PORTUGUESE: P(e|aim) = -1.6334684555795866 ==> log prob of sentence so far: -Infinity
SPANISH: P(e|aim) = -Infinity ==> log prob of sentence so far: -Infinity

4GRAM: imel
ENGLISH: P(l|ime) = -1.55503077370365 ==> log prob of sentence so far: -Infinity
FRENCH: P(l|ime) = -1.8976270912904414 ==> log prob of sentence so far: -3.4774106879072515
DUTCH: P(l|ime) = -0.7781512503836436 ==> log prob of sentence so far: NaN
PORTUGUESE: P(l|ime) = -2.146128035678238 ==> log prob of sentence so far: -Infinity
SPANISH: P(l|ime) = -1.5563025007672873 ==> log prob of sentence so far: -Infinity

4GRAM: meli
ENGLISH: P(i|mel) = -0.890855530574932 ==> log prob of sentence so far: -Infinity
FRENCH: P(i|mel) = -1.611014833980889 ==> log prob of sentence so far: -5.088425521888141
DUTCH: P(i|mel) = -0.5698753079565612 ==> log prob of sentence so far: NaN
PORTUGUESE: P(i|mel) = -1.3082085802911045 ==> log prob of sentence so far: -Infinity
SPANISH: P(i|mel) = -2.1636085634310516 ==> log prob of sentence so far: -Infinity

4GRAM: elia
ENGLISH: P(a|eli) = -1.7552883674241393 ==> log prob of sentence so far: -Infinity
FRENCH: P(a|eli) = -1.7000543856282386 ==> log prob of sentence so far: -6.788479907516379
DUTCH: P(a|eli) = -Infinity ==> log prob of sentence so far: NaN
PORTUGUESE: P(a|eli) = -1.745595216427921 ==> log prob of sentence so far: -Infinity
SPANISH: P(a|eli) = -1.4515671502472742 ==> log prob of sentence so far: -Infinity

According to the 4gram model, the sentence is in French
----------------
