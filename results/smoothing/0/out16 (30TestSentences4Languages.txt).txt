They are bad hombres.

1GRAM MODEL:

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -1.034220196106131
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -1.165963194070616
DUTCH: P(t) = -1.2498796920497102 ==> log prob of sentence so far: -1.2498796920497102
PORTUGUESE: P(t) = -1.379831205944868 ==> log prob of sentence so far: -1.379831205944868
SPANISH: P(t) = -1.4035912069184395 ==> log prob of sentence so far: -1.4035912069184395

1GRAM: h
ENGLISH: P(h) = -1.1802717013558974 ==> log prob of sentence so far: -2.2144918974620285
FRENCH: P(h) = -2.105661511639352 ==> log prob of sentence so far: -3.271624705709968
DUTCH: P(h) = -1.4907882137925417 ==> log prob of sentence so far: -2.740667905842252
PORTUGUESE: P(h) = -1.8092516298164947 ==> log prob of sentence so far: -3.1890828357613628
SPANISH: P(h) = -1.8922416001417433 ==> log prob of sentence so far: -3.295832807060183

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -3.124559063899755
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -4.0385842074574265
DUTCH: P(e) = -0.7296416695657266 ==> log prob of sentence so far: -3.4703095754079785
PORTUGUESE: P(e) = -0.8804373935970373 ==> log prob of sentence so far: -4.0695202293584
SPANISH: P(e) = -0.8816440697134942 ==> log prob of sentence so far: -4.177476876773677

1GRAM: y
ENGLISH: P(y) = -1.7506034479808759 ==> log prob of sentence so far: -4.875162511880631
FRENCH: P(y) = -2.6723635765386327 ==> log prob of sentence so far: -6.710947783996059
DUTCH: P(y) = -3.570848895473591 ==> log prob of sentence so far: -7.0411584708815695
PORTUGUESE: P(y) = -3.345834826269983 ==> log prob of sentence so far: -7.415355055628383
SPANISH: P(y) = -1.9106606754428224 ==> log prob of sentence so far: -6.0881375522165

1GRAM: a
ENGLISH: P(a) = -1.08678555499431 ==> log prob of sentence so far: -5.961948066874941
FRENCH: P(a) = -1.076348685206508 ==> log prob of sentence so far: -7.787296469202567
DUTCH: P(a) = -1.1153867013427927 ==> log prob of sentence so far: -8.156545172224362
PORTUGUESE: P(a) = -0.8318864420456407 ==> log prob of sentence so far: -8.247241497674024
SPANISH: P(a) = -0.9020150997282854 ==> log prob of sentence so far: -6.990152651944785

1GRAM: r
ENGLISH: P(r) = -1.261271144299728 ==> log prob of sentence so far: -7.223219211174669
FRENCH: P(r) = -1.1840252799828537 ==> log prob of sentence so far: -8.971321749185421
DUTCH: P(r) = -1.2541986367945883 ==> log prob of sentence so far: -9.41074380901895
PORTUGUESE: P(r) = -1.1945370969978044 ==> log prob of sentence so far: -9.441778594671828
SPANISH: P(r) = -1.1910392834925079 ==> log prob of sentence so far: -8.181191935437294

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -8.133286377612396
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -9.738281250932879
DUTCH: P(e) = -0.7296416695657266 ==> log prob of sentence so far: -10.140385478584676
PORTUGUESE: P(e) = -0.8804373935970373 ==> log prob of sentence so far: -10.322215988268866
SPANISH: P(e) = -0.8816440697134942 ==> log prob of sentence so far: -9.062836005150789

1GRAM: b
ENGLISH: P(b) = -1.7519589010459922 ==> log prob of sentence so far: -9.885245278658388
FRENCH: P(b) = -2.0519153330379174 ==> log prob of sentence so far: -11.790196583970797
DUTCH: P(b) = -1.8074883651312794 ==> log prob of sentence so far: -11.947873843715955
PORTUGUESE: P(b) = -2.0435293686557596 ==> log prob of sentence so far: -12.365745356924625
SPANISH: P(b) = -1.8063459424729122 ==> log prob of sentence so far: -10.869181947623701

1GRAM: a
ENGLISH: P(a) = -1.08678555499431 ==> log prob of sentence so far: -10.972030833652699
FRENCH: P(a) = -1.076348685206508 ==> log prob of sentence so far: -12.866545269177305
DUTCH: P(a) = -1.1153867013427927 ==> log prob of sentence so far: -13.063260545058748
PORTUGUESE: P(a) = -0.8318864420456407 ==> log prob of sentence so far: -13.197631798970265
SPANISH: P(a) = -0.9020150997282854 ==> log prob of sentence so far: -11.771197047351986

1GRAM: d
ENGLISH: P(d) = -1.3961796678927219 ==> log prob of sentence so far: -12.368210501545422
FRENCH: P(d) = -1.4202617777702253 ==> log prob of sentence so far: -14.28680704694753
DUTCH: P(d) = -1.2011818229767268 ==> log prob of sentence so far: -14.264442368035475
PORTUGUESE: P(d) = -1.3354018341009022 ==> log prob of sentence so far: -14.533033633071167
SPANISH: P(d) = -1.2907600049809702 ==> log prob of sentence so far: -13.061957052332957

1GRAM: h
ENGLISH: P(h) = -1.1802717013558974 ==> log prob of sentence so far: -13.548482202901319
FRENCH: P(h) = -2.105661511639352 ==> log prob of sentence so far: -16.39246855858688
DUTCH: P(h) = -1.4907882137925417 ==> log prob of sentence so far: -15.755230581828016
PORTUGUESE: P(h) = -1.8092516298164947 ==> log prob of sentence so far: -16.34228526288766
SPANISH: P(h) = -1.8922416001417433 ==> log prob of sentence so far: -14.9541986524747

1GRAM: o
ENGLISH: P(o) = -1.1377809795017801 ==> log prob of sentence so far: -14.6862631824031
FRENCH: P(o) = -1.2743410774278996 ==> log prob of sentence so far: -17.66680963601478
DUTCH: P(o) = -1.1998143328861146 ==> log prob of sentence so far: -16.95504491471413
PORTUGUESE: P(o) = -0.9821306962190126 ==> log prob of sentence so far: -17.324415959106673
SPANISH: P(o) = -0.9966256619750637 ==> log prob of sentence so far: -15.950824314449763

1GRAM: m
ENGLISH: P(m) = -1.6111138481089953 ==> log prob of sentence so far: -16.297377030512095
FRENCH: P(m) = -1.5129506664806158 ==> log prob of sentence so far: -19.179760302495396
DUTCH: P(m) = -1.6188728818507347 ==> log prob of sentence so far: -18.573917796564864
PORTUGUESE: P(m) = -1.3143867564844387 ==> log prob of sentence so far: -18.638802715591112
SPANISH: P(m) = -1.4970849452515298 ==> log prob of sentence so far: -17.447909259701294

1GRAM: b
ENGLISH: P(b) = -1.7519589010459922 ==> log prob of sentence so far: -18.049335931558087
FRENCH: P(b) = -2.0519153330379174 ==> log prob of sentence so far: -21.231675635533314
DUTCH: P(b) = -1.8074883651312794 ==> log prob of sentence so far: -20.381406161696145
PORTUGUESE: P(b) = -2.0435293686557596 ==> log prob of sentence so far: -20.682332084246873
SPANISH: P(b) = -1.8063459424729122 ==> log prob of sentence so far: -19.254255202174207

1GRAM: r
ENGLISH: P(r) = -1.261271144299728 ==> log prob of sentence so far: -19.310607075857813
FRENCH: P(r) = -1.1840252799828537 ==> log prob of sentence so far: -22.41570091551617
DUTCH: P(r) = -1.2541986367945883 ==> log prob of sentence so far: -21.635604798490732
PORTUGUESE: P(r) = -1.1945370969978044 ==> log prob of sentence so far: -21.87686918124468
SPANISH: P(r) = -1.1910392834925079 ==> log prob of sentence so far: -20.445294485666714

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -20.22067424229554
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -23.182660417263627
DUTCH: P(e) = -0.7296416695657266 ==> log prob of sentence so far: -22.36524646805646
PORTUGUESE: P(e) = -0.8804373935970373 ==> log prob of sentence so far: -22.757306574841717
SPANISH: P(e) = -0.8816440697134942 ==> log prob of sentence so far: -21.32693855538021

1GRAM: s
ENGLISH: P(s) = -1.1711384802271032 ==> log prob of sentence so far: -21.391812722522644
FRENCH: P(s) = -1.0644432040045189 ==> log prob of sentence so far: -24.247103621268145
DUTCH: P(s) = -1.46044120298166 ==> log prob of sentence so far: -23.825687671038118
PORTUGUESE: P(s) = -1.1128885174353524 ==> log prob of sentence so far: -23.87019509227707
SPANISH: P(s) = -1.1432550008932698 ==> log prob of sentence so far: -22.47019355627348

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: th
ENGLISH: P(h|t) = -0.41922455380664086 ==> log prob of sentence so far: -0.41922455380664086
FRENCH: P(h|t) = -2.127706282322321 ==> log prob of sentence so far: -2.127706282322321
DUTCH: P(h|t) = -1.3564184283833902 ==> log prob of sentence so far: -1.3564184283833902
PORTUGUESE: P(h|t) = -2.1891818577265614 ==> log prob of sentence so far: -2.1891818577265614
SPANISH: P(h|t) = -3.6831071024781963 ==> log prob of sentence so far: -3.6831071024781963

2GRAM: he
ENGLISH: P(e|h) = -0.3699456961286581 ==> log prob of sentence so far: -0.7891702499352989
FRENCH: P(e|h) = -0.45712142827039787 ==> log prob of sentence so far: -2.5848277105927187
DUTCH: P(e|h) = -0.4397580717708245 ==> log prob of sentence so far: -1.7961765001542147
PORTUGUESE: P(e|h) = -0.5741241550089848 ==> log prob of sentence so far: -2.763306012735546
SPANISH: P(e|h) = -0.848175179651435 ==> log prob of sentence so far: -4.531282282129631

2GRAM: ey
ENGLISH: P(y|e) = -1.831429796492428 ==> log prob of sentence so far: -2.620600046427727
FRENCH: P(y|e) = -3.4675785104027366 ==> log prob of sentence so far: -6.052406220995455
DUTCH: P(y|e) = -4.401819696872055 ==> log prob of sentence so far: -6.19799619702627
PORTUGUESE: P(y|e) = -3.2841900191222293 ==> log prob of sentence so far: -6.047496031857776
SPANISH: P(y|e) = -2.010537991439908 ==> log prob of sentence so far: -6.541820273569539

2GRAM: ya
ENGLISH: P(a|y) = -1.04207949063062 ==> log prob of sentence so far: -3.662679537058347
FRENCH: P(a|y) = -0.6191994848168431 ==> log prob of sentence so far: -6.671605705812298
DUTCH: P(a|y) = -1.1422329917947138 ==> log prob of sentence so far: -7.340229188820984
PORTUGUESE: P(a|y) = -1.5272001190629803 ==> log prob of sentence so far: -7.574696150920756
SPANISH: P(a|y) = -0.7709542346582459 ==> log prob of sentence so far: -7.312774508227784

2GRAM: ar
ENGLISH: P(r|a) = -0.98542005411536 ==> log prob of sentence so far: -4.648099591173707
FRENCH: P(r|a) = -1.0331352537647966 ==> log prob of sentence so far: -7.704740959577094
DUTCH: P(r|a) = -0.9127486307598558 ==> log prob of sentence so far: -8.25297781958084
PORTUGUESE: P(r|a) = -1.0105738806391424 ==> log prob of sentence so far: -8.585270031559899
SPANISH: P(r|a) = -0.944345426426658 ==> log prob of sentence so far: -8.257119934654442

2GRAM: re
ENGLISH: P(e|r) = -0.628115605628671 ==> log prob of sentence so far: -5.276215196802378
FRENCH: P(e|r) = -0.49086037711507113 ==> log prob of sentence so far: -8.195601336692166
DUTCH: P(e|r) = -0.794370784819527 ==> log prob of sentence so far: -9.047348604400367
PORTUGUESE: P(e|r) = -0.7384080339681381 ==> log prob of sentence so far: -9.323678065528037
SPANISH: P(e|r) = -0.6817295252769482 ==> log prob of sentence so far: -8.93884945993139

2GRAM: eb
ENGLISH: P(b|e) = -1.6931035561398335 ==> log prob of sentence so far: -6.969318752942211
FRENCH: P(b|e) = -2.134635350277044 ==> log prob of sentence so far: -10.33023668696921
DUTCH: P(b|e) = -1.7346781778297267 ==> log prob of sentence so far: -10.782026782230094
PORTUGUESE: P(b|e) = -2.0929253571518918 ==> log prob of sentence so far: -11.416603422679929
SPANISH: P(b|e) = -2.101301110037348 ==> log prob of sentence so far: -11.040150569968738

2GRAM: ba
ENGLISH: P(a|b) = -1.2017577270780557 ==> log prob of sentence so far: -8.171076480020266
FRENCH: P(a|b) = -0.8256302156376365 ==> log prob of sentence so far: -11.155866902606846
DUTCH: P(a|b) = -1.157498009138688 ==> log prob of sentence so far: -11.939524791368783
PORTUGUESE: P(a|b) = -0.6425197126861412 ==> log prob of sentence so far: -12.05912313536607
SPANISH: P(a|b) = -0.470418232750133 ==> log prob of sentence so far: -11.51056880271887

2GRAM: ad
ENGLISH: P(d|a) = -1.3608745337130301 ==> log prob of sentence so far: -9.531951013733297
FRENCH: P(d|a) = -1.654837978604329 ==> log prob of sentence so far: -12.810704881211175
DUTCH: P(d|a) = -1.338305336669701 ==> log prob of sentence so far: -13.277830128038485
PORTUGUESE: P(d|a) = -1.0654628585140578 ==> log prob of sentence so far: -13.124585993880128
SPANISH: P(d|a) = -1.0183649114863147 ==> log prob of sentence so far: -12.528933714205184

2GRAM: dh
ENGLISH: P(h|d) = -1.4165748980857382 ==> log prob of sentence so far: -10.948525911819035
FRENCH: P(h|d) = -2.3212918101701914 ==> log prob of sentence so far: -15.131996691381365
DUTCH: P(h|d) = -1.7224567790563161 ==> log prob of sentence so far: -15.0002869070948
PORTUGUESE: P(h|d) = -4.018367578387845 ==> log prob of sentence so far: -17.142953572267974
SPANISH: P(h|d) = -3.1922886125681202 ==> log prob of sentence so far: -15.721222326773304

2GRAM: ho
ENGLISH: P(o|h) = -1.1036237025152846 ==> log prob of sentence so far: -12.052149614334319
FRENCH: P(o|h) = -0.8324129819584538 ==> log prob of sentence so far: -15.96440967333982
DUTCH: P(o|h) = -1.017314696517776 ==> log prob of sentence so far: -16.017601603612576
PORTUGUESE: P(o|h) = -0.5397552533777222 ==> log prob of sentence so far: -17.682708825645697
SPANISH: P(o|h) = -0.5326512532666954 ==> log prob of sentence so far: -16.25387358004

2GRAM: om
ENGLISH: P(m|o) = -1.1832305419394793 ==> log prob of sentence so far: -13.235380156273798
FRENCH: P(m|o) = -1.1431764798796071 ==> log prob of sentence so far: -17.107586153219426
DUTCH: P(m|o) = -1.2690957945538175 ==> log prob of sentence so far: -17.286697398166393
PORTUGUESE: P(m|o) = -1.0086118261375816 ==> log prob of sentence so far: -18.69132065178328
SPANISH: P(m|o) = -1.1212852578895207 ==> log prob of sentence so far: -17.375158837929522

2GRAM: mb
ENGLISH: P(b|m) = -1.5584561252134694 ==> log prob of sentence so far: -14.793836281487268
FRENCH: P(b|m) = -1.4619489036825863 ==> log prob of sentence so far: -18.569535056902012
DUTCH: P(b|m) = -1.0951229265861648 ==> log prob of sentence so far: -18.381820324752557
PORTUGUESE: P(b|m) = -1.5248827266162652 ==> log prob of sentence so far: -20.216203378399545
SPANISH: P(b|m) = -1.24828638424264 ==> log prob of sentence so far: -18.623445222172162

2GRAM: br
ENGLISH: P(r|b) = -1.2407398152920606 ==> log prob of sentence so far: -16.03457609677933
FRENCH: P(r|b) = -0.7689990453449054 ==> log prob of sentence so far: -19.338534102246918
DUTCH: P(r|b) = -1.1238584015191124 ==> log prob of sentence so far: -19.505678726271668
PORTUGUESE: P(r|b) = -0.6490929438388802 ==> log prob of sentence so far: -20.865296322238425
SPANISH: P(r|b) = -0.822040587644531 ==> log prob of sentence so far: -19.44548580981669

2GRAM: re
ENGLISH: P(e|r) = -0.628115605628671 ==> log prob of sentence so far: -16.662691702408
FRENCH: P(e|r) = -0.49086037711507113 ==> log prob of sentence so far: -19.829394479361987
DUTCH: P(e|r) = -0.794370784819527 ==> log prob of sentence so far: -20.300049511091196
PORTUGUESE: P(e|r) = -0.7384080339681381 ==> log prob of sentence so far: -21.603704356206563
SPANISH: P(e|r) = -0.6817295252769482 ==> log prob of sentence so far: -20.12721533509364

2GRAM: es
ENGLISH: P(s|e) = -0.9448348227533898 ==> log prob of sentence so far: -17.60752652516139
FRENCH: P(s|e) = -0.7261117486329816 ==> log prob of sentence so far: -20.555506227994968
DUTCH: P(s|e) = -1.4885357951116365 ==> log prob of sentence so far: -21.788585306202833
PORTUGUESE: P(s|e) = -0.8219917427636538 ==> log prob of sentence so far: -22.425696098970217
SPANISH: P(s|e) = -0.7772968881285706 ==> log prob of sentence so far: -20.904512223222213

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: the
ENGLISH: P(e|th) = -0.20437434030792398 ==> log prob of sentence so far: -0.20437434030792398
FRENCH: P(e|th) = -0.3708817759380016 ==> log prob of sentence so far: -0.3708817759380016
DUTCH: P(e|th) = -0.43911207178746714 ==> log prob of sentence so far: -0.43911207178746714
PORTUGUESE: P(e|th) = -0.21712811094377202 ==> log prob of sentence so far: -0.21712811094377202
SPANISH: P(e|th) = -Infinity ==> log prob of sentence so far: -Infinity

3GRAM: hey
ENGLISH: P(y|he) = -1.52985551350171 ==> log prob of sentence so far: -1.734229853809634
FRENCH: P(y|he) = -Infinity ==> log prob of sentence so far: -Infinity
DUTCH: P(y|he) = -Infinity ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(y|he) = -Infinity ==> log prob of sentence so far: -Infinity
SPANISH: P(y|he) = -1.6476623490125806 ==> log prob of sentence so far: -Infinity

3GRAM: eya
ENGLISH: P(a|ey) = -1.05517221926392 ==> log prob of sentence so far: -2.789402073073554
FRENCH: P(a|ey) = -0.8239087409443188 ==> log prob of sentence so far: -Infinity
DUTCH: P(a|ey) = -Infinity ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(a|ey) = -Infinity ==> log prob of sentence so far: -Infinity
SPANISH: P(a|ey) = -0.9081814225839352 ==> log prob of sentence so far: -Infinity

3GRAM: yar
ENGLISH: P(r|ya) = -0.8497035389732587 ==> log prob of sentence so far: -3.6391056120468126
FRENCH: P(r|ya) = -1.703905222011531 ==> log prob of sentence so far: -Infinity
DUTCH: P(r|ya) = -Infinity ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(r|ya) = -Infinity ==> log prob of sentence so far: -Infinity
SPANISH: P(r|ya) = -1.5153691435015921 ==> log prob of sentence so far: -Infinity

3GRAM: are
ENGLISH: P(e|ar) = -0.8590836862310319 ==> log prob of sentence so far: -4.498189298277844
FRENCH: P(e|ar) = -1.0005657507775636 ==> log prob of sentence so far: -Infinity
DUTCH: P(e|ar) = -0.7224898401598685 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(e|ar) = -0.7936661069495455 ==> log prob of sentence so far: -Infinity
SPANISH: P(e|ar) = -0.8951386940029141 ==> log prob of sentence so far: -Infinity

3GRAM: reb
ENGLISH: P(b|re) = -1.652391964012019 ==> log prob of sentence so far: -6.1505812622898635
FRENCH: P(b|re) = -2.369915082165419 ==> log prob of sentence so far: -Infinity
DUTCH: P(b|re) = -2.2978820430663194 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(b|re) = -2.323939275128193 ==> log prob of sentence so far: -Infinity
SPANISH: P(b|re) = -2.212921985440336 ==> log prob of sentence so far: -Infinity

3GRAM: eba
ENGLISH: P(a|eb) = -1.1248131998298847 ==> log prob of sentence so far: -7.275394462119748
FRENCH: P(a|eb) = -0.5004342474194561 ==> log prob of sentence so far: -Infinity
DUTCH: P(a|eb) = -1.255272505103306 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(a|eb) = -0.6269932315317751 ==> log prob of sentence so far: -Infinity
SPANISH: P(a|eb) = -0.5638803502843525 ==> log prob of sentence so far: -Infinity

3GRAM: bad
ENGLISH: P(d|ba) = -1.4020565748668699 ==> log prob of sentence so far: -8.677451036986618
FRENCH: P(d|ba) = -2.66228551572213 ==> log prob of sentence so far: -Infinity
DUTCH: P(d|ba) = -1.2002249387724007 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(d|ba) = -1.5544425643831634 ==> log prob of sentence so far: -Infinity
SPANISH: P(d|ba) = -1.3419633529296764 ==> log prob of sentence so far: -Infinity

3GRAM: adh
ENGLISH: P(h|ad) = -1.6384892569546374 ==> log prob of sentence so far: -10.315940293941255
FRENCH: P(h|ad) = -1.933149709532522 ==> log prob of sentence so far: -Infinity
DUTCH: P(h|ad) = -1.4446692309385245 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(h|ad) = -3.4375920322539613 ==> log prob of sentence so far: -Infinity
SPANISH: P(h|ad) = -2.844166410450201 ==> log prob of sentence so far: -Infinity

3GRAM: dho
ENGLISH: P(o|dh) = -0.9323866032056586 ==> log prob of sentence so far: -11.248326897146914
FRENCH: P(o|dh) = -0.6055483191737837 ==> log prob of sentence so far: -Infinity
DUTCH: P(o|dh) = -1.0818871394235499 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(o|dh) = -Infinity ==> log prob of sentence so far: -Infinity
SPANISH: P(o|dh) = -1.0791812460476249 ==> log prob of sentence so far: -Infinity

3GRAM: hom
ENGLISH: P(m|ho) = -1.4926012711786887 ==> log prob of sentence so far: -12.740928168325603
FRENCH: P(m|ho) = -0.515156952975262 ==> log prob of sentence so far: -Infinity
DUTCH: P(m|ho) = -2.0637767428411067 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(m|ho) = -1.0347621062592118 ==> log prob of sentence so far: -Infinity
SPANISH: P(m|ho) = -0.8548872106242782 ==> log prob of sentence so far: -Infinity

3GRAM: omb
ENGLISH: P(b|om) = -1.79563527544867 ==> log prob of sentence so far: -14.536563443774273
FRENCH: P(b|om) = -0.9381258370244187 ==> log prob of sentence so far: -Infinity
DUTCH: P(b|om) = -1.532435864506711 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(b|om) = -1.6198555676850188 ==> log prob of sentence so far: -Infinity
SPANISH: P(b|om) = -1.0119082870574045 ==> log prob of sentence so far: -Infinity

3GRAM: mbr
ENGLISH: P(r|mb) = -1.2948793809242283 ==> log prob of sentence so far: -15.831442824698502
FRENCH: P(r|mb) = -0.3771453434156263 ==> log prob of sentence so far: -Infinity
DUTCH: P(r|mb) = -1.8965262174895554 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(r|mb) = -0.5860718859438321 ==> log prob of sentence so far: -Infinity
SPANISH: P(r|mb) = -0.36797678529459443 ==> log prob of sentence so far: -Infinity

3GRAM: bre
ENGLISH: P(e|br) = -0.5780838117389158 ==> log prob of sentence so far: -16.40952663643742
FRENCH: P(e|br) = -0.3706118229667005 ==> log prob of sentence so far: -Infinity
DUTCH: P(e|br) = -0.5733574204522631 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(e|br) = -0.5219335991360248 ==> log prob of sentence so far: -Infinity
SPANISH: P(e|br) = -0.2861724071125709 ==> log prob of sentence so far: -Infinity

3GRAM: res
ENGLISH: P(s|re) = -0.8764822385109647 ==> log prob of sentence so far: -17.286008874948383
FRENCH: P(s|re) = -0.6175517228087953 ==> log prob of sentence so far: -Infinity
DUTCH: P(s|re) = -1.929905257771725 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(s|re) = -0.6037799717222362 ==> log prob of sentence so far: -Infinity
SPANISH: P(s|re) = -0.6496045994379516 ==> log prob of sentence so far: -Infinity

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: they
ENGLISH: P(y|the) = -1.414241519286803 ==> log prob of sentence so far: -1.414241519286803
FRENCH: P(y|the) = -Infinity ==> log prob of sentence so far: -Infinity
DUTCH: P(y|the) = -Infinity ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(y|the) = -Infinity ==> log prob of sentence so far: -Infinity
SPANISH: P(y|the) = NaN ==> log prob of sentence so far: NaN

4GRAM: heya
ENGLISH: P(a|hey) = -0.8294630008738363 ==> log prob of sentence so far: -2.243704520160639
FRENCH: P(a|hey) = NaN ==> log prob of sentence so far: NaN
DUTCH: P(a|hey) = NaN ==> log prob of sentence so far: NaN
PORTUGUESE: P(a|hey) = NaN ==> log prob of sentence so far: NaN
SPANISH: P(a|hey) = -Infinity ==> log prob of sentence so far: NaN

4GRAM: eyar
ENGLISH: P(r|eya) = -0.16244737838913506 ==> log prob of sentence so far: -2.406151898549774
FRENCH: P(r|eya) = -Infinity ==> log prob of sentence so far: NaN
DUTCH: P(r|eya) = NaN ==> log prob of sentence so far: NaN
PORTUGUESE: P(r|eya) = NaN ==> log prob of sentence so far: NaN
SPANISH: P(r|eya) = -1.6989700043360187 ==> log prob of sentence so far: NaN

4GRAM: yare
ENGLISH: P(e|yar) = -0.4001060704285453 ==> log prob of sentence so far: -2.8062579689783194
FRENCH: P(e|yar) = -Infinity ==> log prob of sentence so far: NaN
DUTCH: P(e|yar) = NaN ==> log prob of sentence so far: NaN
PORTUGUESE: P(e|yar) = NaN ==> log prob of sentence so far: NaN
SPANISH: P(e|yar) = -0.5228787452803376 ==> log prob of sentence so far: NaN

4GRAM: areb
ENGLISH: P(b|are) = -1.4691906209332408 ==> log prob of sentence so far: -4.27544858991156
FRENCH: P(b|are) = -2.7283537820212285 ==> log prob of sentence so far: NaN
DUTCH: P(b|are) = -2.0681858617461617 ==> log prob of sentence so far: NaN
PORTUGUESE: P(b|are) = -Infinity ==> log prob of sentence so far: NaN
SPANISH: P(b|are) = -2.7972675408307164 ==> log prob of sentence so far: NaN

4GRAM: reba
ENGLISH: P(a|reb) = -1.714329759745233 ==> log prob of sentence so far: -5.989778349656794
FRENCH: P(a|reb) = -0.743937149852542 ==> log prob of sentence so far: NaN
DUTCH: P(a|reb) = -Infinity ==> log prob of sentence so far: NaN
PORTUGUESE: P(a|reb) = -0.7781512503836436 ==> log prob of sentence so far: NaN
SPANISH: P(a|reb) = -0.42100531274073105 ==> log prob of sentence so far: NaN

4GRAM: ebad
ENGLISH: P(d|eba) = -1.760924848409133 ==> log prob of sentence so far: -7.750703198065927
FRENCH: P(d|eba) = -Infinity ==> log prob of sentence so far: NaN
DUTCH: P(d|eba) = -Infinity ==> log prob of sentence so far: NaN
PORTUGUESE: P(d|eba) = -Infinity ==> log prob of sentence so far: NaN
SPANISH: P(d|eba) = -1.3053513694466237 ==> log prob of sentence so far: NaN

4GRAM: badh
ENGLISH: P(h|bad) = -Infinity ==> log prob of sentence so far: -Infinity
FRENCH: P(h|bad) = -Infinity ==> log prob of sentence so far: NaN
DUTCH: P(h|bad) = -Infinity ==> log prob of sentence so far: NaN
PORTUGUESE: P(h|bad) = -Infinity ==> log prob of sentence so far: NaN
SPANISH: P(h|bad) = -Infinity ==> log prob of sentence so far: NaN

4GRAM: adho
ENGLISH: P(o|adh) = -0.9149892102916513 ==> log prob of sentence so far: -Infinity
FRENCH: P(o|adh) = -1.1760912590556813 ==> log prob of sentence so far: NaN
DUTCH: P(o|adh) = -1.3979400086720375 ==> log prob of sentence so far: NaN
PORTUGUESE: P(o|adh) = -Infinity ==> log prob of sentence so far: NaN
SPANISH: P(o|adh) = -0.7781512503836436 ==> log prob of sentence so far: NaN

4GRAM: dhom
ENGLISH: P(m|dho) = -1.3064250275506875 ==> log prob of sentence so far: -Infinity
FRENCH: P(m|dho) = -0.3152704347785914 ==> log prob of sentence so far: NaN
DUTCH: P(m|dho) = -Infinity ==> log prob of sentence so far: NaN
PORTUGUESE: P(m|dho) = NaN ==> log prob of sentence so far: NaN
SPANISH: P(m|dho) = -Infinity ==> log prob of sentence so far: NaN

4GRAM: homb
ENGLISH: P(b|hom) = -Infinity ==> log prob of sentence so far: -Infinity
FRENCH: P(b|hom) = -2.0863598306747484 ==> log prob of sentence so far: NaN
DUTCH: P(b|hom) = -Infinity ==> log prob of sentence so far: NaN
PORTUGUESE: P(b|hom) = -1.0 ==> log prob of sentence so far: NaN
SPANISH: P(b|hom) = -0.06733197323183013 ==> log prob of sentence so far: NaN

4GRAM: ombr
ENGLISH: P(r|omb) = -Infinity ==> log prob of sentence so far: -Infinity
FRENCH: P(r|omb) = -0.25899055762092293 ==> log prob of sentence so far: NaN
DUTCH: P(r|omb) = -1.1139433523068367 ==> log prob of sentence so far: NaN
PORTUGUESE: P(r|omb) = -0.23888208891513676 ==> log prob of sentence so far: NaN
SPANISH: P(r|omb) = -0.06890749039614115 ==> log prob of sentence so far: NaN

4GRAM: mbre
ENGLISH: P(e|mbr) = -0.8061799739838872 ==> log prob of sentence so far: -Infinity
FRENCH: P(e|mbr) = -0.0769310105372141 ==> log prob of sentence so far: NaN
DUTCH: P(e|mbr) = -0.6989700043360187 ==> log prob of sentence so far: NaN
PORTUGUESE: P(e|mbr) = -0.8776854072178488 ==> log prob of sentence so far: NaN
SPANISH: P(e|mbr) = -0.11378937447083162 ==> log prob of sentence so far: NaN

4GRAM: bres
ENGLISH: P(s|bre) = -1.3668472801536244 ==> log prob of sentence so far: -Infinity
FRENCH: P(s|bre) = -0.49775670300432484 ==> log prob of sentence so far: NaN
DUTCH: P(s|bre) = -Infinity ==> log prob of sentence so far: NaN
PORTUGUESE: P(s|bre) = -0.8868223742774903 ==> log prob of sentence so far: NaN
SPANISH: P(s|bre) = -0.6738592837529876 ==> log prob of sentence so far: NaN

According to the 4gram model, the sentence is in English
----------------
