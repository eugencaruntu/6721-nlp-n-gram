Tinctures constitute limited palette of colours.

1GRAM MODEL:

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -1.034220196106131
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -1.165963194070616

1GRAM: i
ENGLISH: P(i) = -1.162522881728433 ==> log prob of sentence so far: -2.1967430778345642
FRENCH: P(i) = -1.1352025747557652 ==> log prob of sentence so far: -2.3011657688263814

1GRAM: n
ENGLISH: P(n) = -1.1615943318171305 ==> log prob of sentence so far: -3.3583374096516945
FRENCH: P(n) = -1.122633486706524 ==> log prob of sentence so far: -3.4237992555329053

1GRAM: c
ENGLISH: P(c) = -1.626591332724491 ==> log prob of sentence so far: -4.984928742376185
FRENCH: P(c) = -1.4922252466336208 ==> log prob of sentence so far: -4.916024502166526

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -6.019148938482316
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -6.081987696237142

1GRAM: u
ENGLISH: P(u) = -1.5524903648320154 ==> log prob of sentence so far: -7.571639303314331
FRENCH: P(u) = -1.2061319519803348 ==> log prob of sentence so far: -7.288119648217477

1GRAM: r
ENGLISH: P(r) = -1.261271144299728 ==> log prob of sentence so far: -8.83291044761406
FRENCH: P(r) = -1.1840252799828537 ==> log prob of sentence so far: -8.472144928200331

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -9.742977614051787
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -9.239104429947789

1GRAM: s
ENGLISH: P(s) = -1.1711384802271032 ==> log prob of sentence so far: -10.91411609427889
FRENCH: P(s) = -1.0644432040045189 ==> log prob of sentence so far: -10.303547633952308

1GRAM: c
ENGLISH: P(c) = -1.626591332724491 ==> log prob of sentence so far: -12.540707427003381
FRENCH: P(c) = -1.4922252466336208 ==> log prob of sentence so far: -11.795772880585929

1GRAM: o
ENGLISH: P(o) = -1.1377809795017801 ==> log prob of sentence so far: -13.678488406505162
FRENCH: P(o) = -1.2743410774278996 ==> log prob of sentence so far: -13.07011395801383

1GRAM: n
ENGLISH: P(n) = -1.1615943318171305 ==> log prob of sentence so far: -14.840082738322293
FRENCH: P(n) = -1.122633486706524 ==> log prob of sentence so far: -14.192747444720354

1GRAM: s
ENGLISH: P(s) = -1.1711384802271032 ==> log prob of sentence so far: -16.011221218549395
FRENCH: P(s) = -1.0644432040045189 ==> log prob of sentence so far: -15.257190648724873

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -17.045441414655524
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -16.42315384279549

1GRAM: i
ENGLISH: P(i) = -1.162522881728433 ==> log prob of sentence so far: -18.207964296383956
FRENCH: P(i) = -1.1352025747557652 ==> log prob of sentence so far: -17.558356417551256

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -19.242184492490086
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -18.724319611621873

1GRAM: u
ENGLISH: P(u) = -1.5524903648320154 ==> log prob of sentence so far: -20.7946748573221
FRENCH: P(u) = -1.2061319519803348 ==> log prob of sentence so far: -19.930451563602208

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -21.82889505342823
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -21.096414757672825

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -22.738962219865957
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -21.863374259420283

1GRAM: l
ENGLISH: P(l) = -1.3477680403069134 ==> log prob of sentence so far: -24.08673026017287
FRENCH: P(l) = -1.2682101448093062 ==> log prob of sentence so far: -23.13158440422959

1GRAM: i
ENGLISH: P(i) = -1.162522881728433 ==> log prob of sentence so far: -25.249253141901303
FRENCH: P(i) = -1.1352025747557652 ==> log prob of sentence so far: -24.266786978985355

1GRAM: m
ENGLISH: P(m) = -1.6111138481089953 ==> log prob of sentence so far: -26.8603669900103
FRENCH: P(m) = -1.5129506664806158 ==> log prob of sentence so far: -25.77973764546597

1GRAM: i
ENGLISH: P(i) = -1.162522881728433 ==> log prob of sentence so far: -28.02288987173873
FRENCH: P(i) = -1.1352025747557652 ==> log prob of sentence so far: -26.914940220221737

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -29.05711006784486
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -28.080903414292354

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -29.967177234282588
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -28.847862916039812

1GRAM: d
ENGLISH: P(d) = -1.3961796678927219 ==> log prob of sentence so far: -31.36335690217531
FRENCH: P(d) = -1.4202617777702253 ==> log prob of sentence so far: -30.268124693810037

1GRAM: p
ENGLISH: P(p) = -1.7418320771526665 ==> log prob of sentence so far: -33.10518897932798
FRENCH: P(p) = -1.5386383937439996 ==> log prob of sentence so far: -31.806763087554035

1GRAM: a
ENGLISH: P(a) = -1.08678555499431 ==> log prob of sentence so far: -34.191974534322284
FRENCH: P(a) = -1.076348685206508 ==> log prob of sentence so far: -32.88311177276054

1GRAM: l
ENGLISH: P(l) = -1.3477680403069134 ==> log prob of sentence so far: -35.539742574629194
FRENCH: P(l) = -1.2682101448093062 ==> log prob of sentence so far: -34.15132191756985

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -36.44980974106692
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -34.91828141931731

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -37.484029937173055
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -36.08424461338792

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -38.51825013327919
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -37.25020780745854

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -39.428317299716916
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -38.017167309205995

1GRAM: o
ENGLISH: P(o) = -1.1377809795017801 ==> log prob of sentence so far: -40.56609827921869
FRENCH: P(o) = -1.2743410774278996 ==> log prob of sentence so far: -39.29150838663389

1GRAM: f
ENGLISH: P(f) = -1.6602800178172739 ==> log prob of sentence so far: -42.226378297035964
FRENCH: P(f) = -1.9917488938770878 ==> log prob of sentence so far: -41.28325728051098

1GRAM: c
ENGLISH: P(c) = -1.626591332724491 ==> log prob of sentence so far: -43.85296962976045
FRENCH: P(c) = -1.4922252466336208 ==> log prob of sentence so far: -42.775482527144604

1GRAM: o
ENGLISH: P(o) = -1.1377809795017801 ==> log prob of sentence so far: -44.99075060926223
FRENCH: P(o) = -1.2743410774278996 ==> log prob of sentence so far: -44.0498236045725

1GRAM: l
ENGLISH: P(l) = -1.3477680403069134 ==> log prob of sentence so far: -46.33851864956914
FRENCH: P(l) = -1.2682101448093062 ==> log prob of sentence so far: -45.31803374938181

1GRAM: o
ENGLISH: P(o) = -1.1377809795017801 ==> log prob of sentence so far: -47.476299629070915
FRENCH: P(o) = -1.2743410774278996 ==> log prob of sentence so far: -46.59237482680971

1GRAM: u
ENGLISH: P(u) = -1.5524903648320154 ==> log prob of sentence so far: -49.02878999390293
FRENCH: P(u) = -1.2061319519803348 ==> log prob of sentence so far: -47.79850677879004

1GRAM: r
ENGLISH: P(r) = -1.261271144299728 ==> log prob of sentence so far: -50.29006113820266
FRENCH: P(r) = -1.1840252799828537 ==> log prob of sentence so far: -48.9825320587729

1GRAM: s
ENGLISH: P(s) = -1.1711384802271032 ==> log prob of sentence so far: -51.46119961842976
FRENCH: P(s) = -1.0644432040045189 ==> log prob of sentence so far: -50.046975262777416

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: ti
ENGLISH: P(i|t) = -1.0855635892642252 ==> log prob of sentence so far: -1.0855635892642252
FRENCH: P(i|t) = -0.9859430520465329 ==> log prob of sentence so far: -0.9859430520465329

2GRAM: in
ENGLISH: P(n|i) = -0.5176578891035971 ==> log prob of sentence so far: -1.6032214783678223
FRENCH: P(n|i) = -0.8961969544626189 ==> log prob of sentence so far: -1.8821400065091518

2GRAM: nc
ENGLISH: P(c|n) = -1.3671579012153008 ==> log prob of sentence so far: -2.970379379583123
FRENCH: P(c|n) = -1.236931174560272 ==> log prob of sentence so far: -3.119071181069424

2GRAM: ct
ENGLISH: P(t|c) = -1.2536405028750397 ==> log prob of sentence so far: -4.224019882458163
FRENCH: P(t|c) = -1.5426481762257624 ==> log prob of sentence so far: -4.661719357295186

2GRAM: tu
ENGLISH: P(u|t) = -1.618632434374752 ==> log prob of sentence so far: -5.842652316832915
FRENCH: P(u|t) = -1.4956830676169153 ==> log prob of sentence so far: -6.157402424912101

2GRAM: ur
ENGLISH: P(r|u) = -0.9176649397398913 ==> log prob of sentence so far: -6.760317256572806
FRENCH: P(r|u) = -0.7886730035857709 ==> log prob of sentence so far: -6.946075428497872

2GRAM: re
ENGLISH: P(e|r) = -0.628115605628671 ==> log prob of sentence so far: -7.388432862201477
FRENCH: P(e|r) = -0.49086037711507113 ==> log prob of sentence so far: -7.4369358056129435

2GRAM: es
ENGLISH: P(s|e) = -0.9448348227533898 ==> log prob of sentence so far: -8.333267684954867
FRENCH: P(s|e) = -0.7261117486329816 ==> log prob of sentence so far: -8.163047554245924

2GRAM: sc
ENGLISH: P(c|s) = -1.5669662564862925 ==> log prob of sentence so far: -9.90023394144116
FRENCH: P(c|s) = -1.3370172762312347 ==> log prob of sentence so far: -9.500064830477159

2GRAM: co
ENGLISH: P(o|c) = -0.7818409839420732 ==> log prob of sentence so far: -10.682074925383233
FRENCH: P(o|c) = -0.6554783352277935 ==> log prob of sentence so far: -10.155543165704952

2GRAM: on
ENGLISH: P(n|o) = -0.8621497423065837 ==> log prob of sentence so far: -11.544224667689816
FRENCH: P(n|o) = -0.5204118582964626 ==> log prob of sentence so far: -10.675955024001414

2GRAM: ns
ENGLISH: P(s|n) = -1.2471501236353728 ==> log prob of sentence so far: -12.79137479132519
FRENCH: P(s|n) = -0.9210890151969363 ==> log prob of sentence so far: -11.59704403919835

2GRAM: st
ENGLISH: P(t|s) = -0.7036151348650846 ==> log prob of sentence so far: -13.494989926190275
FRENCH: P(t|s) = -1.237297938930775 ==> log prob of sentence so far: -12.834341978129125

2GRAM: ti
ENGLISH: P(i|t) = -1.0855635892642252 ==> log prob of sentence so far: -14.5805535154545
FRENCH: P(i|t) = -0.9859430520465329 ==> log prob of sentence so far: -13.820285030175658

2GRAM: it
ENGLISH: P(t|i) = -0.9082406380281335 ==> log prob of sentence so far: -15.488794153482633
FRENCH: P(t|i) = -0.7741212801413213 ==> log prob of sentence so far: -14.594406310316979

2GRAM: tu
ENGLISH: P(u|t) = -1.618632434374752 ==> log prob of sentence so far: -17.107426587857386
FRENCH: P(u|t) = -1.4956830676169153 ==> log prob of sentence so far: -16.090089377933893

2GRAM: ut
ENGLISH: P(t|u) = -0.800039098717541 ==> log prob of sentence so far: -17.907465686574927
FRENCH: P(t|u) = -1.1015781198504553 ==> log prob of sentence so far: -17.191667497784348

2GRAM: te
ENGLISH: P(e|t) = -1.058872156727803 ==> log prob of sentence so far: -18.96633784330273
FRENCH: P(e|t) = -0.6740385500746982 ==> log prob of sentence so far: -17.865706047859046

2GRAM: el
ENGLISH: P(l|e) = -1.327771460508567 ==> log prob of sentence so far: -20.294109303811297
FRENCH: P(l|e) = -1.2044160454405197 ==> log prob of sentence so far: -19.070122093299567

2GRAM: li
ENGLISH: P(i|l) = -0.9280383126561762 ==> log prob of sentence so far: -21.222147616467474
FRENCH: P(i|l) = -1.2029072596544539 ==> log prob of sentence so far: -20.27302935295402

2GRAM: im
ENGLISH: P(m|i) = -1.3479746512911457 ==> log prob of sentence so far: -22.57012226775862
FRENCH: P(m|i) = -1.5427405149491051 ==> log prob of sentence so far: -21.815769867903125

2GRAM: mi
ENGLISH: P(i|m) = -1.0703394861923436 ==> log prob of sentence so far: -23.640461753950962
FRENCH: P(i|m) = -1.0434398958968627 ==> log prob of sentence so far: -22.85920976379999

2GRAM: it
ENGLISH: P(t|i) = -0.9082406380281335 ==> log prob of sentence so far: -24.548702391979095
FRENCH: P(t|i) = -0.7741212801413213 ==> log prob of sentence so far: -23.63333104394131

2GRAM: te
ENGLISH: P(e|t) = -1.058872156727803 ==> log prob of sentence so far: -25.607574548706896
FRENCH: P(e|t) = -0.6740385500746982 ==> log prob of sentence so far: -24.30736959401601

2GRAM: ed
ENGLISH: P(d|e) = -1.0374287886703306 ==> log prob of sentence so far: -26.645003337377226
FRENCH: P(d|e) = -1.2790718765846223 ==> log prob of sentence so far: -25.586441470600633

2GRAM: dp
ENGLISH: P(p|d) = -1.9002836276914854 ==> log prob of sentence so far: -28.545286965068712
FRENCH: P(p|d) = -2.548970103447272 ==> log prob of sentence so far: -28.135411574047904

2GRAM: pa
ENGLISH: P(a|p) = -0.9076895034466613 ==> log prob of sentence so far: -29.452976468515374
FRENCH: P(a|p) = -0.6468271379316752 ==> log prob of sentence so far: -28.78223871197958

2GRAM: al
ENGLISH: P(l|a) = -0.9618849784656429 ==> log prob of sentence so far: -30.414861446981018
FRENCH: P(l|a) = -1.1917883410411148 ==> log prob of sentence so far: -29.974027053020695

2GRAM: le
ENGLISH: P(e|l) = -0.6937165011526335 ==> log prob of sentence so far: -31.10857794813365
FRENCH: P(e|l) = -0.4011572582950946 ==> log prob of sentence so far: -30.375184311315788

2GRAM: et
ENGLISH: P(t|e) = -1.1805542990214501 ==> log prob of sentence so far: -32.2891322471551
FRENCH: P(t|e) = -1.08170423649854 ==> log prob of sentence so far: -31.45688854781433

2GRAM: tt
ENGLISH: P(t|t) = -1.2608620617113913 ==> log prob of sentence so far: -33.549994308866495
FRENCH: P(t|t) = -1.3532936014980792 ==> log prob of sentence so far: -32.81018214931241

2GRAM: te
ENGLISH: P(e|t) = -1.058872156727803 ==> log prob of sentence so far: -34.608866465594296
FRENCH: P(e|t) = -0.6740385500746982 ==> log prob of sentence so far: -33.48422069938711

2GRAM: eo
ENGLISH: P(o|e) = -1.6145819741820024 ==> log prob of sentence so far: -36.2234484397763
FRENCH: P(o|e) = -2.31529016601968 ==> log prob of sentence so far: -35.79951086540679

2GRAM: of
ENGLISH: P(f|o) = -0.9585907362181515 ==> log prob of sentence so far: -37.18203917599445
FRENCH: P(f|o) = -1.9344866521677835 ==> log prob of sentence so far: -37.73399751757457

2GRAM: fc
ENGLISH: P(c|f) = -2.0562837421207467 ==> log prob of sentence so far: -39.238322918115195
FRENCH: P(c|f) = -2.769315772042742 ==> log prob of sentence so far: -40.50331328961732

2GRAM: co
ENGLISH: P(o|c) = -0.7818409839420732 ==> log prob of sentence so far: -40.02016390205727
FRENCH: P(o|c) = -0.6554783352277935 ==> log prob of sentence so far: -41.15879162484511

2GRAM: ol
ENGLISH: P(l|o) = -1.416314881854774 ==> log prob of sentence so far: -41.436478783912044
FRENCH: P(l|o) = -1.4871112380821068 ==> log prob of sentence so far: -42.645902862927215

2GRAM: lo
ENGLISH: P(o|l) = -1.0313086061321195 ==> log prob of sentence so far: -42.467787390044165
FRENCH: P(o|l) = -1.1639093527410755 ==> log prob of sentence so far: -43.80981221566829

2GRAM: ou
ENGLISH: P(u|o) = -0.9045829397217254 ==> log prob of sentence so far: -43.37237032976589
FRENCH: P(u|o) = -0.6276609377180441 ==> log prob of sentence so far: -44.43747315338633

2GRAM: ur
ENGLISH: P(r|u) = -0.9176649397398913 ==> log prob of sentence so far: -44.290035269505786
FRENCH: P(r|u) = -0.7886730035857709 ==> log prob of sentence so far: -45.2261461569721

2GRAM: rs
ENGLISH: P(s|r) = -1.1636971659316802 ==> log prob of sentence so far: -45.453732435437466
FRENCH: P(s|r) = -1.199647923947648 ==> log prob of sentence so far: -46.42579408091975

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: tin
ENGLISH: P(n|ti) = -0.5903232233586087 ==> log prob of sentence so far: -0.5903232233586087
FRENCH: P(n|ti) = -1.0172662674255242 ==> log prob of sentence so far: -1.0172662674255242

3GRAM: inc
ENGLISH: P(c|in) = -1.552463187822013 ==> log prob of sentence so far: -2.1427864111806216
FRENCH: P(c|in) = -1.2402907356054023 ==> log prob of sentence so far: -2.2575570030309264

3GRAM: nct
ENGLISH: P(t|nc) = -1.4598948527451518 ==> log prob of sentence so far: -3.6026812639257733
FRENCH: P(t|nc) = -1.834391799852571 ==> log prob of sentence so far: -4.091948802883497

3GRAM: ctu
ENGLISH: P(u|ct) = -0.9943597326473073 ==> log prob of sentence so far: -4.597040996573081
FRENCH: P(u|ct) = -1.247836931568063 ==> log prob of sentence so far: -5.33978573445156

3GRAM: tur
ENGLISH: P(r|tu) = -0.4203808777202828 ==> log prob of sentence so far: -5.017421874293364
FRENCH: P(r|tu) = -0.6270879970298935 ==> log prob of sentence so far: -5.966873731481454

3GRAM: ure
ENGLISH: P(e|ur) = -0.6380103185251103 ==> log prob of sentence so far: -5.655432192818473
FRENCH: P(e|ur) = -0.6863628733185169 ==> log prob of sentence so far: -6.653236604799971

3GRAM: res
ENGLISH: P(s|re) = -0.8764822385109647 ==> log prob of sentence so far: -6.531914431329438
FRENCH: P(s|re) = -0.6175517228087953 ==> log prob of sentence so far: -7.270788327608766

3GRAM: esc
ENGLISH: P(c|es) = -1.5677848013323483 ==> log prob of sentence so far: -8.099699232661786
FRENCH: P(c|es) = -1.2063276861488783 ==> log prob of sentence so far: -8.477116013757644

3GRAM: sco
ENGLISH: P(o|sc) = -0.5989762305708981 ==> log prob of sentence so far: -8.698675463232684
FRENCH: P(o|sc) = -0.47092034952500467 ==> log prob of sentence so far: -8.948036363282649

3GRAM: con
ENGLISH: P(n|co) = -0.5357625581084791 ==> log prob of sentence so far: -9.234438021341163
FRENCH: P(n|co) = -0.4325111742898343 ==> log prob of sentence so far: -9.380547537572482

3GRAM: ons
ENGLISH: P(s|on) = -0.9339789884185266 ==> log prob of sentence so far: -10.16841700975969
FRENCH: P(s|on) = -0.56572586743725 ==> log prob of sentence so far: -9.946273405009732

3GRAM: nst
ENGLISH: P(t|ns) = -0.6470539349449231 ==> log prob of sentence so far: -10.815470944704613
FRENCH: P(t|ns) = -1.2221311257523793 ==> log prob of sentence so far: -11.16840453076211

3GRAM: sti
ENGLISH: P(i|st) = -0.9933836951304011 ==> log prob of sentence so far: -11.808854639835014
FRENCH: P(i|st) = -1.0016622508927273 ==> log prob of sentence so far: -12.170066781654837

3GRAM: tit
ENGLISH: P(t|ti) = -1.107430791117394 ==> log prob of sentence so far: -12.916285430952408
FRENCH: P(t|ti) = -1.2591409227509491 ==> log prob of sentence so far: -13.429207704405787

3GRAM: itu
ENGLISH: P(u|it) = -1.5921712313528729 ==> log prob of sentence so far: -14.508456662305282
FRENCH: P(u|it) = -1.2242205406433635 ==> log prob of sentence so far: -14.65342824504915

3GRAM: tut
ENGLISH: P(t|tu) = -1.9211660506377388 ==> log prob of sentence so far: -16.429622712943022
FRENCH: P(t|tu) = -2.2730012720637376 ==> log prob of sentence so far: -16.926429517112886

3GRAM: ute
ENGLISH: P(e|ut) = -1.2508926687551685 ==> log prob of sentence so far: -17.68051538169819
FRENCH: P(e|ut) = -0.5458040672424515 ==> log prob of sentence so far: -17.472233584355337

3GRAM: tel
ENGLISH: P(l|te) = -1.3216961533553806 ==> log prob of sentence so far: -19.00221153505357
FRENCH: P(l|te) = -1.3087389084106158 ==> log prob of sentence so far: -18.78097249276595

3GRAM: eli
ENGLISH: P(i|el) = -0.8552763131021409 ==> log prob of sentence so far: -19.85748784815571
FRENCH: P(i|el) = -1.2612480325304063 ==> log prob of sentence so far: -20.042220525296358

3GRAM: lim
ENGLISH: P(m|li) = -1.591673167104722 ==> log prob of sentence so far: -21.449161015260433
FRENCH: P(m|li) = -1.287988242487056 ==> log prob of sentence so far: -21.330208767783414

3GRAM: imi
ENGLISH: P(i|im) = -1.1330449708908685 ==> log prob of sentence so far: -22.5822059861513
FRENCH: P(i|im) = -1.3686766959729206 ==> log prob of sentence so far: -22.698885463756334

3GRAM: mit
ENGLISH: P(t|mi) = -0.9225968300569761 ==> log prob of sentence so far: -23.504802816208276
FRENCH: P(t|mi) = -1.3433293502160262 ==> log prob of sentence so far: -24.04221481397236

3GRAM: ite
ENGLISH: P(e|it) = -1.0751658292877215 ==> log prob of sentence so far: -24.579968645496
FRENCH: P(e|it) = -0.7263478941073039 ==> log prob of sentence so far: -24.768562708079664

3GRAM: ted
ENGLISH: P(d|te) = -0.6935943601630679 ==> log prob of sentence so far: -25.273563005659067
FRENCH: P(d|te) = -1.2181690734335784 ==> log prob of sentence so far: -25.986731781513242

3GRAM: edp
ENGLISH: P(p|ed) = -1.8836390839798445 ==> log prob of sentence so far: -27.15720208963891
FRENCH: P(p|ed) = -2.8849368971038603 ==> log prob of sentence so far: -28.8716686786171

3GRAM: dpa
ENGLISH: P(a|dp) = -0.7577754910119257 ==> log prob of sentence so far: -27.914977580650838
FRENCH: P(a|dp) = -0.422073688388757 ==> log prob of sentence so far: -29.29374236700586

3GRAM: pal
ENGLISH: P(l|pa) = -1.1962069000215556 ==> log prob of sentence so far: -29.111184480672392
FRENCH: P(l|pa) = -1.7467494881556445 ==> log prob of sentence so far: -31.040491855161502

3GRAM: ale
ENGLISH: P(e|al) = -0.6257318143709264 ==> log prob of sentence so far: -29.73691629504332
FRENCH: P(e|al) = -0.5884907782652161 ==> log prob of sentence so far: -31.62898263342672

3GRAM: let
ENGLISH: P(t|le) = -1.0296255575717301 ==> log prob of sentence so far: -30.76654185261505
FRENCH: P(t|le) = -1.3254053163287955 ==> log prob of sentence so far: -32.954387949755514

3GRAM: ett
ENGLISH: P(t|et) = -1.2500796966949403 ==> log prob of sentence so far: -32.01662154930999
FRENCH: P(t|et) = -0.9772988673753815 ==> log prob of sentence so far: -33.931686817130895

3GRAM: tte
ENGLISH: P(e|tt) = -0.8907438201766512 ==> log prob of sentence so far: -32.90736536948664
FRENCH: P(e|tt) = -0.22331137374276216 ==> log prob of sentence so far: -34.154998190873656

3GRAM: teo
ENGLISH: P(o|te) = -2.0098232912077996 ==> log prob of sentence so far: -34.91718866069444
FRENCH: P(o|te) = -2.0895960521701844 ==> log prob of sentence so far: -36.24459424304384

3GRAM: eof
ENGLISH: P(f|eo) = -0.3420456072305536 ==> log prob of sentence so far: -35.259234267924995
FRENCH: P(f|eo) = -2.055378331375 ==> log prob of sentence so far: -38.299972574418845

3GRAM: ofc
ENGLISH: P(c|of) = -1.6963317730510774 ==> log prob of sentence so far: -36.95556604097607
FRENCH: P(c|of) = -Infinity ==> log prob of sentence so far: -Infinity

3GRAM: fco
ENGLISH: P(o|fc) = -0.3366061499251773 ==> log prob of sentence so far: -37.29217219090125
FRENCH: P(o|fc) = -Infinity ==> log prob of sentence so far: -Infinity

3GRAM: col
ENGLISH: P(l|co) = -1.351484295321252 ==> log prob of sentence so far: -38.6436564862225
FRENCH: P(l|co) = -1.4520333139923094 ==> log prob of sentence so far: -Infinity

3GRAM: olo
ENGLISH: P(o|ol) = -1.065413533026927 ==> log prob of sentence so far: -39.709070019249424
FRENCH: P(o|ol) = -0.8980273795001809 ==> log prob of sentence so far: -Infinity

3GRAM: lou
ENGLISH: P(u|lo) = -1.2260598543919565 ==> log prob of sentence so far: -40.93512987364138
FRENCH: P(u|lo) = -1.1610275126198053 ==> log prob of sentence so far: -Infinity

3GRAM: our
ENGLISH: P(r|ou) = -0.9326674845605442 ==> log prob of sentence so far: -41.867797358201926
FRENCH: P(r|ou) = -0.6943373531005168 ==> log prob of sentence so far: -Infinity

3GRAM: urs
ENGLISH: P(s|ur) = -0.9261453337179052 ==> log prob of sentence so far: -42.79394269191983
FRENCH: P(s|ur) = -0.8242449472563703 ==> log prob of sentence so far: -Infinity

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: tinc
ENGLISH: P(c|tin) = -1.3746261644136015 ==> log prob of sentence so far: -1.3746261644136015
FRENCH: P(c|tin) = -0.796349271286977 ==> log prob of sentence so far: -0.796349271286977

4GRAM: inct
ENGLISH: P(t|inc) = -0.9890046156985368 ==> log prob of sentence so far: -2.363630780112138
FRENCH: P(t|inc) = -1.1321173212354234 ==> log prob of sentence so far: -1.9284665925224003

4GRAM: nctu
ENGLISH: P(u|nct) = -0.6260602677317288 ==> log prob of sentence so far: -2.989691047843867
FRENCH: P(u|nct) = -1.3324384599156054 ==> log prob of sentence so far: -3.2609050524380057

4GRAM: ctur
ENGLISH: P(r|ctu) = -0.23608918873096657 ==> log prob of sentence so far: -3.225780236574834
FRENCH: P(r|ctu) = -0.35218251811136253 ==> log prob of sentence so far: -3.6130875705493684

4GRAM: ture
ENGLISH: P(e|tur) = -0.33707509984862055 ==> log prob of sentence so far: -3.5628553364234543
FRENCH: P(e|tur) = -0.09415840201727764 ==> log prob of sentence so far: -3.707245972566646

4GRAM: ures
ENGLISH: P(s|ure) = -0.6299630328315675 ==> log prob of sentence so far: -4.192818369255022
FRENCH: P(s|ure) = -0.5971107128993197 ==> log prob of sentence so far: -4.304356685465965

4GRAM: resc
ENGLISH: P(c|res) = -1.7191597759638302 ==> log prob of sentence so far: -5.9119781452188525
FRENCH: P(c|res) = -1.3254588628054231 ==> log prob of sentence so far: -5.629815548271388

4GRAM: esco
ENGLISH: P(o|esc) = -0.9726141995103674 ==> log prob of sentence so far: -6.88459234472922
FRENCH: P(o|esc) = -0.3818239226657038 ==> log prob of sentence so far: -6.011639470937092

4GRAM: scon
ENGLISH: P(n|sco) = -0.597982078907523 ==> log prob of sentence so far: -7.482574423636743
FRENCH: P(n|sco) = -0.4969864842216028 ==> log prob of sentence so far: -6.508625955158695

4GRAM: cons
ENGLISH: P(s|con) = -0.5583084834648858 ==> log prob of sentence so far: -8.04088290710163
FRENCH: P(s|con) = -0.3997433101497555 ==> log prob of sentence so far: -6.90836926530845

4GRAM: onst
ENGLISH: P(t|ons) = -0.6872316010647747 ==> log prob of sentence so far: -8.728114508166405
FRENCH: P(t|ons) = -1.2069444304533608 ==> log prob of sentence so far: -8.115313695761811

4GRAM: nsti
ENGLISH: P(i|nst) = -1.163580972536096 ==> log prob of sentence so far: -9.8916954807025
FRENCH: P(i|nst) = -1.2882727652062314 ==> log prob of sentence so far: -9.403586460968043

4GRAM: stit
ENGLISH: P(t|sti) = -1.1610112921681834 ==> log prob of sentence so far: -11.052706772870684
FRENCH: P(t|sti) = -1.530199698203082 ==> log prob of sentence so far: -10.933786159171126

4GRAM: titu
ENGLISH: P(u|tit) = -0.8129133566428556 ==> log prob of sentence so far: -11.86562012951354
FRENCH: P(u|tit) = -0.48536238401244847 ==> log prob of sentence so far: -11.419148543183574

4GRAM: itut
ENGLISH: P(t|itu) = -1.1271047983648077 ==> log prob of sentence so far: -12.992724927878347
FRENCH: P(t|itu) = -2.225309281725863 ==> log prob of sentence so far: -13.644457824909438

4GRAM: tute
ENGLISH: P(e|tut) = -0.494850021680094 ==> log prob of sentence so far: -13.487574949558441
FRENCH: P(e|tut) = -0.9030899869919435 ==> log prob of sentence so far: -14.547547811901381

4GRAM: utel
ENGLISH: P(l|ute) = -1.1178559416698868 ==> log prob of sentence so far: -14.605430891228329
FRENCH: P(l|ute) = -1.2879844369089002 ==> log prob of sentence so far: -15.835532248810281

4GRAM: teli
ENGLISH: P(i|tel) = -1.6483600109809315 ==> log prob of sentence so far: -16.25379090220926
FRENCH: P(i|tel) = -1.2671717284030137 ==> log prob of sentence so far: -17.102703977213295

4GRAM: elim
ENGLISH: P(m|eli) = -1.547012424997057 ==> log prob of sentence so far: -17.800803327206317
FRENCH: P(m|eli) = -1.3726954512419083 ==> log prob of sentence so far: -18.475399428455205

4GRAM: limi
ENGLISH: P(i|lim) = -0.7092699609758307 ==> log prob of sentence so far: -18.51007328818215
FRENCH: P(i|lim) = -0.8239087409443188 ==> log prob of sentence so far: -19.299308169399524

4GRAM: imit
ENGLISH: P(t|imi) = -0.8688123141146986 ==> log prob of sentence so far: -19.378885602296847
FRENCH: P(t|imi) = -0.3452336581560347 ==> log prob of sentence so far: -19.644541827555557

4GRAM: mite
ENGLISH: P(e|mit) = -1.286306738843275 ==> log prob of sentence so far: -20.665192341140123
FRENCH: P(e|mit) = -0.22351590898381932 ==> log prob of sentence so far: -19.868057736539377

4GRAM: ited
ENGLISH: P(d|ite) = -0.9598015547080136 ==> log prob of sentence so far: -21.62499389584814
FRENCH: P(d|ite) = -0.9864918151576801 ==> log prob of sentence so far: -20.85454955169706

4GRAM: tedp
ENGLISH: P(p|ted) = -1.635986111800833 ==> log prob of sentence so far: -23.26098000764897
FRENCH: P(p|ted) = -Infinity ==> log prob of sentence so far: -Infinity

4GRAM: edpa
ENGLISH: P(a|edp) = -0.7815844262220376 ==> log prob of sentence so far: -24.042564433871007
FRENCH: P(a|edp) = -0.9030899869919435 ==> log prob of sentence so far: -Infinity

4GRAM: dpal
ENGLISH: P(l|dpa) = -0.9488474775526187 ==> log prob of sentence so far: -24.991411911423626
FRENCH: P(l|dpa) = -Infinity ==> log prob of sentence so far: -Infinity

4GRAM: pale
ENGLISH: P(e|pal) = -0.7653769623472148 ==> log prob of sentence so far: -25.75678887377084
FRENCH: P(e|pal) = -0.3064250275506874 ==> log prob of sentence so far: -Infinity

4GRAM: alet
ENGLISH: P(t|ale) = -1.206220846917788 ==> log prob of sentence so far: -26.96300972068863
FRENCH: P(t|ale) = -1.1986570869544226 ==> log prob of sentence so far: -Infinity

4GRAM: lett
ENGLISH: P(t|let) = -1.122646939828715 ==> log prob of sentence so far: -28.085656660517344
FRENCH: P(t|let) = -1.2465238312090121 ==> log prob of sentence so far: -Infinity

4GRAM: ette
ENGLISH: P(e|ett) = -0.5727251179254353 ==> log prob of sentence so far: -28.65838177844278
FRENCH: P(e|ett) = -0.08818915822855805 ==> log prob of sentence so far: -Infinity

4GRAM: tteo
ENGLISH: P(o|tte) = -Infinity ==> log prob of sentence so far: -Infinity
FRENCH: P(o|tte) = -1.6954816764901974 ==> log prob of sentence so far: -Infinity

4GRAM: teof
ENGLISH: P(f|teo) = -0.19122500218473842 ==> log prob of sentence so far: -Infinity
FRENCH: P(f|teo) = -1.6020599913279623 ==> log prob of sentence so far: -Infinity

4GRAM: eofc
ENGLISH: P(c|eof) = -1.5207641753882981 ==> log prob of sentence so far: -Infinity
FRENCH: P(c|eof) = -Infinity ==> log prob of sentence so far: -Infinity

4GRAM: ofco
ENGLISH: P(o|ofc) = -0.33274440884118783 ==> log prob of sentence so far: -Infinity
FRENCH: P(o|ofc) = NaN ==> log prob of sentence so far: NaN

4GRAM: fcol
ENGLISH: P(l|fco) = -1.135662602000073 ==> log prob of sentence so far: -Infinity
FRENCH: P(l|fco) = NaN ==> log prob of sentence so far: NaN

4GRAM: colo
ENGLISH: P(o|col) = -0.45668009447711777 ==> log prob of sentence so far: -Infinity
FRENCH: P(o|col) = -0.5970965717964123 ==> log prob of sentence so far: NaN

4GRAM: olou
ENGLISH: P(u|olo) = -0.7723989214945524 ==> log prob of sentence so far: -Infinity
FRENCH: P(u|olo) = -1.8779469516291882 ==> log prob of sentence so far: NaN

4GRAM: lour
ENGLISH: P(r|lou) = -0.6532125137753437 ==> log prob of sentence so far: -Infinity
FRENCH: P(r|lou) = -0.7403626894942439 ==> log prob of sentence so far: NaN

4GRAM: ours
ENGLISH: P(s|our) = -0.6220420131819326 ==> log prob of sentence so far: -Infinity
FRENCH: P(s|our) = -0.7554999791003129 ==> log prob of sentence so far: NaN

According to the 4gram model, the sentence is in English
----------------
