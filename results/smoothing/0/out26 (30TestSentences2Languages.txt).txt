C'est plate.

1GRAM MODEL:

1GRAM: c
ENGLISH: P(c) = -1.626591332724491 ==> log prob of sentence so far: -1.626591332724491
FRENCH: P(c) = -1.4922252466336208 ==> log prob of sentence so far: -1.4922252466336208

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -2.5366584991622174
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -2.259184748381079

1GRAM: s
ENGLISH: P(s) = -1.1711384802271032 ==> log prob of sentence so far: -3.707796979389321
FRENCH: P(s) = -1.0644432040045189 ==> log prob of sentence so far: -3.323627952385598

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -4.742017175495452
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -4.489591146456214

1GRAM: p
ENGLISH: P(p) = -1.7418320771526665 ==> log prob of sentence so far: -6.483849252648119
FRENCH: P(p) = -1.5386383937439996 ==> log prob of sentence so far: -6.028229540200214

1GRAM: l
ENGLISH: P(l) = -1.3477680403069134 ==> log prob of sentence so far: -7.831617292955032
FRENCH: P(l) = -1.2682101448093062 ==> log prob of sentence so far: -7.296439685009521

1GRAM: a
ENGLISH: P(a) = -1.08678555499431 ==> log prob of sentence so far: -8.918402847949341
FRENCH: P(a) = -1.076348685206508 ==> log prob of sentence so far: -8.372788370216028

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -9.952623044055473
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -9.538751564286645

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -10.8626902104932
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -10.305711066034103

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: ce
ENGLISH: P(e|c) = -0.8163509598708927 ==> log prob of sentence so far: -0.8163509598708927
FRENCH: P(e|c) = -0.5223984530361349 ==> log prob of sentence so far: -0.5223984530361349

2GRAM: es
ENGLISH: P(s|e) = -0.9448348227533898 ==> log prob of sentence so far: -1.7611857826242825
FRENCH: P(s|e) = -0.7261117486329816 ==> log prob of sentence so far: -1.2485102016691165

2GRAM: st
ENGLISH: P(t|s) = -0.7036151348650846 ==> log prob of sentence so far: -2.464800917489367
FRENCH: P(t|s) = -1.237297938930775 ==> log prob of sentence so far: -2.4858081405998913

2GRAM: tp
ENGLISH: P(p|t) = -2.261960152010443 ==> log prob of sentence so far: -4.72676106949981
FRENCH: P(p|t) = -1.4428609206779084 ==> log prob of sentence so far: -3.9286690612778

2GRAM: pl
ENGLISH: P(l|p) = -1.1217737453686412 ==> log prob of sentence so far: -5.848534814868451
FRENCH: P(l|p) = -1.031013377991938 ==> log prob of sentence so far: -4.959682439269738

2GRAM: la
ENGLISH: P(a|l) = -1.0027641545507109 ==> log prob of sentence so far: -6.851298969419162
FRENCH: P(a|l) = -0.6762996864071086 ==> log prob of sentence so far: -5.635982125676846

2GRAM: at
ENGLISH: P(t|a) = -0.8627002859779346 ==> log prob of sentence so far: -7.713999255397097
FRENCH: P(t|a) = -1.247940370475458 ==> log prob of sentence so far: -6.883922496152304

2GRAM: te
ENGLISH: P(e|t) = -1.058872156727803 ==> log prob of sentence so far: -8.7728714121249
FRENCH: P(e|t) = -0.6740385500746982 ==> log prob of sentence so far: -7.557961046227002

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: ces
ENGLISH: P(s|ce) = -0.8650381375331564 ==> log prob of sentence so far: -0.8650381375331564
FRENCH: P(s|ce) = -0.62492995804391 ==> log prob of sentence so far: -0.62492995804391

3GRAM: est
ENGLISH: P(t|es) = -0.723605514531276 ==> log prob of sentence so far: -1.5886436520644325
FRENCH: P(t|es) = -1.0741259303443658 ==> log prob of sentence so far: -1.6990558883882758

3GRAM: stp
ENGLISH: P(p|st) = -2.061640934061686 ==> log prob of sentence so far: -3.6502845861261184
FRENCH: P(p|st) = -1.398323040725592 ==> log prob of sentence so far: -3.0973789291138676

3GRAM: tpl
ENGLISH: P(l|tp) = -1.0423098860944986 ==> log prob of sentence so far: -4.692594472220617
FRENCH: P(l|tp) = -0.8904569123900833 ==> log prob of sentence so far: -3.987835841503951

3GRAM: pla
ENGLISH: P(a|pl) = -0.3760820497596132 ==> log prob of sentence so far: -5.0686765219802306
FRENCH: P(a|pl) = -0.6188979769457551 ==> log prob of sentence so far: -4.606733818449706

3GRAM: lat
ENGLISH: P(t|la) = -1.0565330238463766 ==> log prob of sentence so far: -6.125209545826607
FRENCH: P(t|la) = -1.0649694864867398 ==> log prob of sentence so far: -5.671703304936447

3GRAM: ate
ENGLISH: P(e|at) = -0.8007954594082685 ==> log prob of sentence so far: -6.926005005234876
FRENCH: P(e|at) = -0.687576732254179 ==> log prob of sentence so far: -6.359280037190626

According to the 3gram model, the sentence is in French
----------------
4GRAM MODEL:

4GRAM: cest
ENGLISH: P(t|ces) = -0.9491866376494367 ==> log prob of sentence so far: -0.9491866376494367
FRENCH: P(t|ces) = -0.6551719507913262 ==> log prob of sentence so far: -0.6551719507913262

4GRAM: estp
ENGLISH: P(p|est) = -1.9256176368341438 ==> log prob of sentence so far: -2.8748042744835804
FRENCH: P(p|est) = -1.1265324796148573 ==> log prob of sentence so far: -1.7817044304061835

4GRAM: stpl
ENGLISH: P(l|stp) = -0.7865844179205064 ==> log prob of sentence so far: -3.6613886924040866
FRENCH: P(l|stp) = -0.9030899869919435 ==> log prob of sentence so far: -2.684794417398127

4GRAM: tpla
ENGLISH: P(a|tpl) = -0.11495451570169903 ==> log prob of sentence so far: -3.7763432081057857
FRENCH: P(a|tpl) = -1.2245131412977681 ==> log prob of sentence so far: -3.909307558695895

4GRAM: plat
ENGLISH: P(t|pla) = -1.2186787028268498 ==> log prob of sentence so far: -4.9950219109326355
FRENCH: P(t|pla) = -0.5840604792571615 ==> log prob of sentence so far: -4.493368037953056

4GRAM: late
ENGLISH: P(e|lat) = -0.42198800337675896 ==> log prob of sentence so far: -5.417009914309395
FRENCH: P(e|lat) = -0.41848255485714253 ==> log prob of sentence so far: -4.9118505928101985

According to the 4gram model, the sentence is in French
----------------
