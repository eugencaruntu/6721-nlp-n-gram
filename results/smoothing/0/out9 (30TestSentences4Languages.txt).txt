Cette phrase est en anglais.

1GRAM MODEL:

1GRAM: c
ENGLISH: P(c) = -1.626591332724491 ==> log prob of sentence so far: -1.626591332724491
FRENCH: P(c) = -1.4922252466336208 ==> log prob of sentence so far: -1.4922252466336208
DUTCH: P(c) = -1.8230802740836685 ==> log prob of sentence so far: -1.8230802740836685
PORTUGUESE: P(c) = -1.4658062726466747 ==> log prob of sentence so far: -1.4658062726466747
SPANISH: P(c) = -1.4008083134042828 ==> log prob of sentence so far: -1.4008083134042828

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -2.5366584991622174
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -2.259184748381079
DUTCH: P(e) = -0.7296416695657266 ==> log prob of sentence so far: -2.552721943649395
PORTUGUESE: P(e) = -0.8804373935970373 ==> log prob of sentence so far: -2.346243666243712
SPANISH: P(e) = -0.8816440697134942 ==> log prob of sentence so far: -2.2824523831177768

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -3.5708786952683482
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -3.4251479424516953
DUTCH: P(t) = -1.2498796920497102 ==> log prob of sentence so far: -3.802601635699105
PORTUGUESE: P(t) = -1.379831205944868 ==> log prob of sentence so far: -3.72607487218858
SPANISH: P(t) = -1.4035912069184395 ==> log prob of sentence so far: -3.686043590036216

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -4.605098891374479
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -4.591111136522311
DUTCH: P(t) = -1.2498796920497102 ==> log prob of sentence so far: -5.052481327748815
PORTUGUESE: P(t) = -1.379831205944868 ==> log prob of sentence so far: -5.105906078133448
SPANISH: P(t) = -1.4035912069184395 ==> log prob of sentence so far: -5.089634796954655

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -5.5151660578122055
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -5.35807063826977
DUTCH: P(e) = -0.7296416695657266 ==> log prob of sentence so far: -5.782122997314541
PORTUGUESE: P(e) = -0.8804373935970373 ==> log prob of sentence so far: -5.9863434717304855
SPANISH: P(e) = -0.8816440697134942 ==> log prob of sentence so far: -5.9712788666681496

1GRAM: p
ENGLISH: P(p) = -1.7418320771526665 ==> log prob of sentence so far: -7.2569981349648724
FRENCH: P(p) = -1.5386383937439996 ==> log prob of sentence so far: -6.89670903201377
DUTCH: P(p) = -1.8900970040448786 ==> log prob of sentence so far: -7.67222000135942
PORTUGUESE: P(p) = -1.6180384957552585 ==> log prob of sentence so far: -7.604381967485744
SPANISH: P(p) = -1.6225576480077517 ==> log prob of sentence so far: -7.5938365146759015

1GRAM: h
ENGLISH: P(h) = -1.1802717013558974 ==> log prob of sentence so far: -8.43726983632077
FRENCH: P(h) = -2.105661511639352 ==> log prob of sentence so far: -9.002370543653122
DUTCH: P(h) = -1.4907882137925417 ==> log prob of sentence so far: -9.163008215151962
PORTUGUESE: P(h) = -1.8092516298164947 ==> log prob of sentence so far: -9.413633597302239
SPANISH: P(h) = -1.8922416001417433 ==> log prob of sentence so far: -9.486078114817644

1GRAM: r
ENGLISH: P(r) = -1.261271144299728 ==> log prob of sentence so far: -9.698540980620498
FRENCH: P(r) = -1.1840252799828537 ==> log prob of sentence so far: -10.186395823635975
DUTCH: P(r) = -1.2541986367945883 ==> log prob of sentence so far: -10.417206851946549
PORTUGUESE: P(r) = -1.1945370969978044 ==> log prob of sentence so far: -10.608170694300043
SPANISH: P(r) = -1.1910392834925079 ==> log prob of sentence so far: -10.677117398310152

1GRAM: a
ENGLISH: P(a) = -1.08678555499431 ==> log prob of sentence so far: -10.785326535614807
FRENCH: P(a) = -1.076348685206508 ==> log prob of sentence so far: -11.262744508842482
DUTCH: P(a) = -1.1153867013427927 ==> log prob of sentence so far: -11.532593553289342
PORTUGUESE: P(a) = -0.8318864420456407 ==> log prob of sentence so far: -11.440057136345683
SPANISH: P(a) = -0.9020150997282854 ==> log prob of sentence so far: -11.579132498038437

1GRAM: s
ENGLISH: P(s) = -1.1711384802271032 ==> log prob of sentence so far: -11.95646501584191
FRENCH: P(s) = -1.0644432040045189 ==> log prob of sentence so far: -12.327187712847001
DUTCH: P(s) = -1.46044120298166 ==> log prob of sentence so far: -12.993034756271001
PORTUGUESE: P(s) = -1.1128885174353524 ==> log prob of sentence so far: -12.552945653781036
SPANISH: P(s) = -1.1432550008932698 ==> log prob of sentence so far: -12.722387498931706

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -12.866532182279638
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -13.094147214594459
DUTCH: P(e) = -0.7296416695657266 ==> log prob of sentence so far: -13.722676425836728
PORTUGUESE: P(e) = -0.8804373935970373 ==> log prob of sentence so far: -13.433383047378072
SPANISH: P(e) = -0.8816440697134942 ==> log prob of sentence so far: -13.604031568645201

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -13.776599348717365
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -13.861106716341917
DUTCH: P(e) = -0.7296416695657266 ==> log prob of sentence so far: -14.452318095402454
PORTUGUESE: P(e) = -0.8804373935970373 ==> log prob of sentence so far: -14.31382044097511
SPANISH: P(e) = -0.8816440697134942 ==> log prob of sentence so far: -14.485675638358696

1GRAM: s
ENGLISH: P(s) = -1.1711384802271032 ==> log prob of sentence so far: -14.947737828944469
FRENCH: P(s) = -1.0644432040045189 ==> log prob of sentence so far: -14.925549920346436
DUTCH: P(s) = -1.46044120298166 ==> log prob of sentence so far: -15.912759298384113
PORTUGUESE: P(s) = -1.1128885174353524 ==> log prob of sentence so far: -15.426708958410464
SPANISH: P(s) = -1.1432550008932698 ==> log prob of sentence so far: -15.628930639251966

1GRAM: t
ENGLISH: P(t) = -1.034220196106131 ==> log prob of sentence so far: -15.9819580250506
FRENCH: P(t) = -1.165963194070616 ==> log prob of sentence so far: -16.09151311441705
DUTCH: P(t) = -1.2498796920497102 ==> log prob of sentence so far: -17.162638990433823
PORTUGUESE: P(t) = -1.379831205944868 ==> log prob of sentence so far: -16.806540164355333
SPANISH: P(t) = -1.4035912069184395 ==> log prob of sentence so far: -17.032521846170404

1GRAM: e
ENGLISH: P(e) = -0.9100671664377266 ==> log prob of sentence so far: -16.892025191488326
FRENCH: P(e) = -0.7669595017474583 ==> log prob of sentence so far: -16.85847261616451
DUTCH: P(e) = -0.7296416695657266 ==> log prob of sentence so far: -17.89228065999955
PORTUGUESE: P(e) = -0.8804373935970373 ==> log prob of sentence so far: -17.68697755795237
SPANISH: P(e) = -0.8816440697134942 ==> log prob of sentence so far: -17.9141659158839

1GRAM: n
ENGLISH: P(n) = -1.1615943318171305 ==> log prob of sentence so far: -18.053619523305457
FRENCH: P(n) = -1.122633486706524 ==> log prob of sentence so far: -17.98110610287103
DUTCH: P(n) = -0.9654176559618705 ==> log prob of sentence so far: -18.85769831596142
PORTUGUESE: P(n) = -1.3242807271097985 ==> log prob of sentence so far: -19.01125828506217
SPANISH: P(n) = -1.1498323261968246 ==> log prob of sentence so far: -19.063998242080725

1GRAM: a
ENGLISH: P(a) = -1.08678555499431 ==> log prob of sentence so far: -19.140405078299768
FRENCH: P(a) = -1.076348685206508 ==> log prob of sentence so far: -19.05745478807754
DUTCH: P(a) = -1.1153867013427927 ==> log prob of sentence so far: -19.973085017304214
PORTUGUESE: P(a) = -0.8318864420456407 ==> log prob of sentence so far: -19.84314472710781
SPANISH: P(a) = -0.9020150997282854 ==> log prob of sentence so far: -19.96601334180901

1GRAM: n
ENGLISH: P(n) = -1.1615943318171305 ==> log prob of sentence so far: -20.3019994101169
FRENCH: P(n) = -1.122633486706524 ==> log prob of sentence so far: -20.18008827478406
DUTCH: P(n) = -0.9654176559618705 ==> log prob of sentence so far: -20.938502673266086
PORTUGUESE: P(n) = -1.3242807271097985 ==> log prob of sentence so far: -21.167425454217607
SPANISH: P(n) = -1.1498323261968246 ==> log prob of sentence so far: -21.115845668005836

1GRAM: g
ENGLISH: P(g) = -1.6600726678577833 ==> log prob of sentence so far: -21.962072077974682
FRENCH: P(g) = -2.028482548907471 ==> log prob of sentence so far: -22.20857082369153
DUTCH: P(g) = -1.483976244686825 ==> log prob of sentence so far: -22.422478917952912
PORTUGUESE: P(g) = -1.9282491727873894 ==> log prob of sentence so far: -23.095674627004996
SPANISH: P(g) = -1.9669330234036477 ==> log prob of sentence so far: -23.082778691409484

1GRAM: l
ENGLISH: P(l) = -1.3477680403069134 ==> log prob of sentence so far: -23.309840118281596
FRENCH: P(l) = -1.2682101448093062 ==> log prob of sentence so far: -23.476780968500837
DUTCH: P(l) = -1.424888573441733 ==> log prob of sentence so far: -23.847367491394646
PORTUGUESE: P(l) = -1.4370345300766503 ==> log prob of sentence so far: -24.532709157081648
SPANISH: P(l) = -1.2802163050487747 ==> log prob of sentence so far: -24.36299499645826

1GRAM: a
ENGLISH: P(a) = -1.08678555499431 ==> log prob of sentence so far: -24.396625673275906
FRENCH: P(a) = -1.076348685206508 ==> log prob of sentence so far: -24.553129653707344
DUTCH: P(a) = -1.1153867013427927 ==> log prob of sentence so far: -24.96275419273744
PORTUGUESE: P(a) = -0.8318864420456407 ==> log prob of sentence so far: -25.364595599127288
SPANISH: P(a) = -0.9020150997282854 ==> log prob of sentence so far: -25.265010096186543

1GRAM: i
ENGLISH: P(i) = -1.162522881728433 ==> log prob of sentence so far: -25.559148555004338
FRENCH: P(i) = -1.1352025747557652 ==> log prob of sentence so far: -25.68833222846311
DUTCH: P(i) = -1.226244226879294 ==> log prob of sentence so far: -26.188998419616734
PORTUGUESE: P(i) = -1.2396906104867749 ==> log prob of sentence so far: -26.604286209614063
SPANISH: P(i) = -1.223148951725382 ==> log prob of sentence so far: -26.488159047911925

1GRAM: s
ENGLISH: P(s) = -1.1711384802271032 ==> log prob of sentence so far: -26.73028703523144
FRENCH: P(s) = -1.0644432040045189 ==> log prob of sentence so far: -26.75277543246763
DUTCH: P(s) = -1.46044120298166 ==> log prob of sentence so far: -27.649439622598393
PORTUGUESE: P(s) = -1.1128885174353524 ==> log prob of sentence so far: -27.717174727049414
SPANISH: P(s) = -1.1432550008932698 ==> log prob of sentence so far: -27.631414048805194

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: ce
ENGLISH: P(e|c) = -0.8163509598708927 ==> log prob of sentence so far: -0.8163509598708927
FRENCH: P(e|c) = -0.5223984530361349 ==> log prob of sentence so far: -0.5223984530361349
DUTCH: P(e|c) = -1.7844914284146627 ==> log prob of sentence so far: -1.7844914284146627
PORTUGUESE: P(e|c) = -0.9993270228789158 ==> log prob of sentence so far: -0.9993270228789158
SPANISH: P(e|c) = -1.0762965458299472 ==> log prob of sentence so far: -1.0762965458299472

2GRAM: et
ENGLISH: P(t|e) = -1.1805542990214501 ==> log prob of sentence so far: -1.9969052588923428
FRENCH: P(t|e) = -1.08170423649854 ==> log prob of sentence so far: -1.6041026895346748
DUTCH: P(t|e) = -1.1837220326865665 ==> log prob of sentence so far: -2.968213461101229
PORTUGUESE: P(t|e) = -1.551796259299261 ==> log prob of sentence so far: -2.551123282178177
SPANISH: P(t|e) = -1.5943209794856514 ==> log prob of sentence so far: -2.6706175253155986

2GRAM: tt
ENGLISH: P(t|t) = -1.2608620617113913 ==> log prob of sentence so far: -3.257767320603734
FRENCH: P(t|t) = -1.3532936014980792 ==> log prob of sentence so far: -2.957396291032754
DUTCH: P(t|t) = -1.3898951498417333 ==> log prob of sentence so far: -4.3581086109429625
PORTUGUESE: P(t|t) = -2.0400132414937606 ==> log prob of sentence so far: -4.5911365236719375
SPANISH: P(t|t) = -3.6831071024781963 ==> log prob of sentence so far: -6.353724627793795

2GRAM: te
ENGLISH: P(e|t) = -1.058872156727803 ==> log prob of sentence so far: -4.316639477331537
FRENCH: P(e|t) = -0.6740385500746982 ==> log prob of sentence so far: -3.631434841107452
DUTCH: P(e|t) = -0.5035716193929743 ==> log prob of sentence so far: -4.861680230335937
PORTUGUESE: P(e|t) = -0.5750110314227178 ==> log prob of sentence so far: -5.166147555094655
SPANISH: P(e|t) = -0.6106121176787732 ==> log prob of sentence so far: -6.964336745472568

2GRAM: ep
ENGLISH: P(p|e) = -1.662563163805412 ==> log prob of sentence so far: -5.979202641136949
FRENCH: P(p|e) = -1.3644592568570226 ==> log prob of sentence so far: -4.995894097964475
DUTCH: P(p|e) = -1.9645977944722766 ==> log prob of sentence so far: -6.8262780248082136
PORTUGUESE: P(p|e) = -1.5698602593769966 ==> log prob of sentence so far: -6.736007814471652
SPANISH: P(p|e) = -1.584569259167627 ==> log prob of sentence so far: -8.548906004640195

2GRAM: ph
ENGLISH: P(h|p) = -1.5965591808783544 ==> log prob of sentence so far: -7.575761822015303
FRENCH: P(h|p) = -1.6254640863161496 ==> log prob of sentence so far: -6.621358184280624
DUTCH: P(h|p) = -1.4120403301916582 ==> log prob of sentence so far: -8.238318354999873
PORTUGUESE: P(h|p) = -1.910321699575816 ==> log prob of sentence so far: -8.646329514047467
SPANISH: P(h|p) = -3.6403820447095683 ==> log prob of sentence so far: -12.189288049349763

2GRAM: hr
ENGLISH: P(r|h) = -1.8523747430268838 ==> log prob of sentence so far: -9.428136565042188
FRENCH: P(r|h) = -2.072201929590871 ==> log prob of sentence so far: -8.693560113871495
DUTCH: P(r|h) = -1.6445988753090761 ==> log prob of sentence so far: -9.882917230308948
PORTUGUESE: P(r|h) = -2.2227164711475833 ==> log prob of sentence so far: -10.86904598519505
SPANISH: P(r|h) = -3.669688708056208 ==> log prob of sentence so far: -15.858976757405971

2GRAM: ra
ENGLISH: P(a|r) = -1.0362014833313207 ==> log prob of sentence so far: -10.464338048373508
FRENCH: P(a|r) = -0.9044619223819287 ==> log prob of sentence so far: -9.598022036253424
DUTCH: P(a|r) = -1.149814603477384 ==> log prob of sentence so far: -11.032731833786332
PORTUGUESE: P(a|r) = -0.5220670852797041 ==> log prob of sentence so far: -11.391113070474756
SPANISH: P(a|r) = -0.6564151510243955 ==> log prob of sentence so far: -16.515391908430367

2GRAM: as
ENGLISH: P(s|a) = -0.9959105050980857 ==> log prob of sentence so far: -11.460248553471594
FRENCH: P(s|a) = -1.2507281483865753 ==> log prob of sentence so far: -10.848750184639998
DUTCH: P(s|a) = -1.3356180470112244 ==> log prob of sentence so far: -12.368349880797556
PORTUGUESE: P(s|a) = -0.8895301879143667 ==> log prob of sentence so far: -12.280643258389123
SPANISH: P(s|a) = -0.8835163481272043 ==> log prob of sentence so far: -17.398908256557572

2GRAM: se
ENGLISH: P(e|s) = -0.9356743584999151 ==> log prob of sentence so far: -12.395922911971509
FRENCH: P(e|s) = -0.7710023890879116 ==> log prob of sentence so far: -11.61975257372791
DUTCH: P(e|s) = -1.2730719791132905 ==> log prob of sentence so far: -13.641421859910846
PORTUGUESE: P(e|s) = -0.7109732060204982 ==> log prob of sentence so far: -12.99161646440962
SPANISH: P(e|s) = -0.8021924468890896 ==> log prob of sentence so far: -18.201100703446663

2GRAM: ee
ENGLISH: P(e|e) = -1.318054935723875 ==> log prob of sentence so far: -13.713977847695384
FRENCH: P(e|e) = -1.5122516196711915 ==> log prob of sentence so far: -13.132004193399101
DUTCH: P(e|e) = -1.0245741735508478 ==> log prob of sentence so far: -14.665996033461694
PORTUGUESE: P(e|e) = -1.389504815334476 ==> log prob of sentence so far: -14.381121279744097
SPANISH: P(e|e) = -1.4722222759787238 ==> log prob of sentence so far: -19.673322979425386

2GRAM: es
ENGLISH: P(s|e) = -0.9448348227533898 ==> log prob of sentence so far: -14.658812670448773
FRENCH: P(s|e) = -0.7261117486329816 ==> log prob of sentence so far: -13.858115942032082
DUTCH: P(s|e) = -1.4885357951116365 ==> log prob of sentence so far: -16.15453182857333
PORTUGUESE: P(s|e) = -0.8219917427636538 ==> log prob of sentence so far: -15.20311302250775
SPANISH: P(s|e) = -0.7772968881285706 ==> log prob of sentence so far: -20.450619867553957

2GRAM: st
ENGLISH: P(t|s) = -0.7036151348650846 ==> log prob of sentence so far: -15.362427805313859
FRENCH: P(t|s) = -1.237297938930775 ==> log prob of sentence so far: -15.095413880962857
DUTCH: P(t|s) = -0.5214461363455993 ==> log prob of sentence so far: -16.67597796491893
PORTUGUESE: P(t|s) = -0.9394210490911225 ==> log prob of sentence so far: -16.142534071598874
SPANISH: P(t|s) = -0.8813297826710784 ==> log prob of sentence so far: -21.331949650225035

2GRAM: te
ENGLISH: P(e|t) = -1.058872156727803 ==> log prob of sentence so far: -16.42129996204166
FRENCH: P(e|t) = -0.6740385500746982 ==> log prob of sentence so far: -15.769452431037555
DUTCH: P(e|t) = -0.5035716193929743 ==> log prob of sentence so far: -17.179549584311903
PORTUGUESE: P(e|t) = -0.5750110314227178 ==> log prob of sentence so far: -16.717545103021592
SPANISH: P(e|t) = -0.6106121176787732 ==> log prob of sentence so far: -21.942561767903808

2GRAM: en
ENGLISH: P(n|e) = -1.0654908055728944 ==> log prob of sentence so far: -17.486790767614558
FRENCH: P(n|e) = -0.8687880036396214 ==> log prob of sentence so far: -16.638240434677176
DUTCH: P(n|e) = -0.526912876754352 ==> log prob of sentence so far: -17.706462461066256
PORTUGUESE: P(n|e) = -0.9798431310000937 ==> log prob of sentence so far: -17.697388234021687
SPANISH: P(n|e) = -0.7295345435530068 ==> log prob of sentence so far: -22.672096311456816

2GRAM: na
ENGLISH: P(a|n) = -1.246080285859623 ==> log prob of sentence so far: -18.73287105347418
FRENCH: P(a|n) = -1.133495459369941 ==> log prob of sentence so far: -17.771735894047115
DUTCH: P(a|n) = -1.218546061796263 ==> log prob of sentence so far: -18.92500852286252
PORTUGUESE: P(a|n) = -0.7411941470458665 ==> log prob of sentence so far: -18.438582381067555
SPANISH: P(a|n) = -0.9402268798277433 ==> log prob of sentence so far: -23.61232319128456

2GRAM: an
ENGLISH: P(n|a) = -0.7044700719963498 ==> log prob of sentence so far: -19.43734112547053
FRENCH: P(n|a) = -0.8167432712437924 ==> log prob of sentence so far: -18.588479165290906
DUTCH: P(n|a) = -0.5908667394990018 ==> log prob of sentence so far: -19.515875262361522
PORTUGUESE: P(n|a) = -1.1303269498824235 ==> log prob of sentence so far: -19.568909330949978
SPANISH: P(n|a) = -0.8950657973329524 ==> log prob of sentence so far: -24.507388988617514

2GRAM: ng
ENGLISH: P(g|n) = -0.7995332676457232 ==> log prob of sentence so far: -20.236874393116256
FRENCH: P(g|n) = -1.565029702228626 ==> log prob of sentence so far: -20.15350886751953
DUTCH: P(g|n) = -1.0386484169005887 ==> log prob of sentence so far: -20.55452367926211
PORTUGUESE: P(g|n) = -1.5955448486733943 ==> log prob of sentence so far: -21.164454179623373
SPANISH: P(g|n) = -1.697167415698169 ==> log prob of sentence so far: -26.20455640431568

2GRAM: gl
ENGLISH: P(l|g) = -1.3312789663947304 ==> log prob of sentence so far: -21.568153359510987
FRENCH: P(l|g) = -1.322152361778265 ==> log prob of sentence so far: -21.475661229297796
DUTCH: P(l|g) = -2.0417215715669026 ==> log prob of sentence so far: -22.596245250829014
PORTUGUESE: P(l|g) = -1.5017437296279945 ==> log prob of sentence so far: -22.666197909251366
SPANISH: P(l|g) = -1.4151930770328809 ==> log prob of sentence so far: -27.619749481348563

2GRAM: la
ENGLISH: P(a|l) = -1.0027641545507109 ==> log prob of sentence so far: -22.570917514061698
FRENCH: P(a|l) = -0.6762996864071086 ==> log prob of sentence so far: -22.151960915704905
DUTCH: P(a|l) = -0.8488788635060638 ==> log prob of sentence so far: -23.445124114335076
PORTUGUESE: P(a|l) = -0.7564540177935158 ==> log prob of sentence so far: -23.422651927044882
SPANISH: P(a|l) = -0.5718569434530056 ==> log prob of sentence so far: -28.19160642480157

2GRAM: ai
ENGLISH: P(i|a) = -1.3482451331022975 ==> log prob of sentence so far: -23.919162647163994
FRENCH: P(i|a) = -0.6965245704990287 ==> log prob of sentence so far: -22.848485486203934
DUTCH: P(i|a) = -2.2566352902183815 ==> log prob of sentence so far: -25.701759404553457
PORTUGUESE: P(i|a) = -1.474077185559241 ==> log prob of sentence so far: -24.896729112604124
SPANISH: P(i|a) = -1.9696512916914282 ==> log prob of sentence so far: -30.161257716492997

2GRAM: is
ENGLISH: P(s|i) = -0.8698730474375823 ==> log prob of sentence so far: -24.789035694601576
FRENCH: P(s|i) = -0.8502233835532343 ==> log prob of sentence so far: -23.69870886975717
DUTCH: P(s|i) = -1.280884588947238 ==> log prob of sentence so far: -26.982643993500695
PORTUGUESE: P(s|i) = -0.9630506557618908 ==> log prob of sentence so far: -25.859779768366014
SPANISH: P(s|i) = -1.1735472174719852 ==> log prob of sentence so far: -31.334804933964982

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: cet
ENGLISH: P(t|ce) = -1.0282921718836653 ==> log prob of sentence so far: -1.0282921718836653
FRENCH: P(t|ce) = -0.7436513200949071 ==> log prob of sentence so far: -0.7436513200949071
DUTCH: P(t|ce) = -1.6989700043360187 ==> log prob of sentence so far: -1.6989700043360187
PORTUGUESE: P(t|ce) = -2.575187844927661 ==> log prob of sentence so far: -2.575187844927661
SPANISH: P(t|ce) = -1.7544211489364225 ==> log prob of sentence so far: -1.7544211489364225

3GRAM: ett
ENGLISH: P(t|et) = -1.2500796966949403 ==> log prob of sentence so far: -2.2783718685786054
FRENCH: P(t|et) = -0.9772988673753815 ==> log prob of sentence so far: -1.7209501874702886
DUTCH: P(t|et) = -1.2266372574462987 ==> log prob of sentence so far: -2.9256072617823174
PORTUGUESE: P(t|et) = -1.4613269875364305 ==> log prob of sentence so far: -4.036514832464091
SPANISH: P(t|et) = -3.074816440645175 ==> log prob of sentence so far: -4.829237589581597

3GRAM: tte
ENGLISH: P(e|tt) = -0.8907438201766512 ==> log prob of sentence so far: -3.1691156887552565
FRENCH: P(e|tt) = -0.22331137374276216 ==> log prob of sentence so far: -1.9442615612130507
DUTCH: P(e|tt) = -0.14004191750031447 ==> log prob of sentence so far: -3.065649179282632
PORTUGUESE: P(e|tt) = -0.26152145438030755 ==> log prob of sentence so far: -4.298036286844399
SPANISH: P(e|tt) = -0.47712125471966244 ==> log prob of sentence so far: -5.3063588443012595

3GRAM: tep
ENGLISH: P(p|te) = -2.0158136548969874 ==> log prob of sentence so far: -5.184929343652244
FRENCH: P(p|te) = -1.495756391088913 ==> log prob of sentence so far: -3.4400179523019636
DUTCH: P(p|te) = -2.15925992750635 ==> log prob of sentence so far: -5.2249091067889815
PORTUGUESE: P(p|te) = -1.5042320019013928 ==> log prob of sentence so far: -5.8022682887457915
SPANISH: P(p|te) = -1.4990902568390716 ==> log prob of sentence so far: -6.805449101140331

3GRAM: eph
ENGLISH: P(h|ep) = -1.4487596108410206 ==> log prob of sentence so far: -6.633688954493264
FRENCH: P(h|ep) = -1.7757603191593836 ==> log prob of sentence so far: -5.215778271461347
DUTCH: P(h|ep) = -1.7940051693054355 ==> log prob of sentence so far: -7.018914276094417
PORTUGUESE: P(h|ep) = -2.1914510144648953 ==> log prob of sentence so far: -7.993719303210687
SPANISH: P(h|ep) = -Infinity ==> log prob of sentence so far: -Infinity

3GRAM: phr
ENGLISH: P(r|ph) = -1.2757618934949178 ==> log prob of sentence so far: -7.909450847988182
FRENCH: P(r|ph) = -1.4725736269689418 ==> log prob of sentence so far: -6.688351898430289
DUTCH: P(r|ph) = -Infinity ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(r|ph) = -1.348953547981164 ==> log prob of sentence so far: -9.342672851191852
SPANISH: P(r|ph) = -Infinity ==> log prob of sentence so far: -Infinity

3GRAM: hra
ENGLISH: P(a|hr) = -1.564764666700442 ==> log prob of sentence so far: -9.474215514688625
FRENCH: P(a|hr) = -0.516629796003336 ==> log prob of sentence so far: -7.204981694433625
DUTCH: P(a|hr) = -1.7004152452101997 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(a|hr) = -0.7201593034059569 ==> log prob of sentence so far: -10.062832154597809
SPANISH: P(a|hr) = -Infinity ==> log prob of sentence so far: -Infinity

3GRAM: ras
ENGLISH: P(s|ra) = -1.253048200717927 ==> log prob of sentence so far: -10.727263715406552
FRENCH: P(s|ra) = -1.4022226424803532 ==> log prob of sentence so far: -8.607204336913979
DUTCH: P(s|ra) = -1.62024566993696 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(s|ra) = -0.9789765096490383 ==> log prob of sentence so far: -11.041808664246847
SPANISH: P(s|ra) = -0.8726307104687329 ==> log prob of sentence so far: -Infinity

3GRAM: ase
ENGLISH: P(e|as) = -1.2582063553993075 ==> log prob of sentence so far: -11.98547007080586
FRENCH: P(e|as) = -0.9527572789226282 ==> log prob of sentence so far: -9.559961615836606
DUTCH: P(e|as) = -1.0151945833619724 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(e|as) = -0.8215065349911628 ==> log prob of sentence so far: -11.86331519923801
SPANISH: P(e|as) = -0.968458835148865 ==> log prob of sentence so far: -Infinity

3GRAM: see
ENGLISH: P(e|se) = -0.8025034242825914 ==> log prob of sentence so far: -12.787973495088451
FRENCH: P(e|se) = -1.4967808076433133 ==> log prob of sentence so far: -11.05674242347992
DUTCH: P(e|se) = -0.5915382106523907 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(e|se) = -1.2100480461754597 ==> log prob of sentence so far: -13.07336324541347
SPANISH: P(e|se) = -1.3142795035431072 ==> log prob of sentence so far: -Infinity

3GRAM: ees
ENGLISH: P(s|ee) = -1.5950293163708567 ==> log prob of sentence so far: -14.383002811459308
FRENCH: P(s|ee) = -0.7120939263000553 ==> log prob of sentence so far: -11.768836349779974
DUTCH: P(s|ee) = -1.2949727917311984 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(s|ee) = -0.6242970459890942 ==> log prob of sentence so far: -13.697660291402563
SPANISH: P(s|ee) = -0.4416164562282157 ==> log prob of sentence so far: -Infinity

3GRAM: est
ENGLISH: P(t|es) = -0.723605514531276 ==> log prob of sentence so far: -15.106608325990583
FRENCH: P(t|es) = -1.0741259303443658 ==> log prob of sentence so far: -12.84296228012434
DUTCH: P(t|es) = -0.3788847127708786 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(t|es) = -0.6333511233761261 ==> log prob of sentence so far: -14.33101141477869
SPANISH: P(t|es) = -0.6162773118414172 ==> log prob of sentence so far: -Infinity

3GRAM: ste
ENGLISH: P(e|st) = -1.0416463936046911 ==> log prob of sentence so far: -16.148254719595275
FRENCH: P(e|st) = -0.629315169781818 ==> log prob of sentence so far: -13.472277449906159
DUTCH: P(e|st) = -0.41267063728643827 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(e|st) = -0.5806618466267319 ==> log prob of sentence so far: -14.911673261405422
SPANISH: P(e|st) = -0.7146618314947392 ==> log prob of sentence so far: -Infinity

3GRAM: ten
ENGLISH: P(n|te) = -1.0074500914121851 ==> log prob of sentence so far: -17.15570481100746
FRENCH: P(n|te) = -0.7815654979040786 ==> log prob of sentence so far: -14.253842947810238
DUTCH: P(n|te) = -0.5802064422667128 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(n|te) = -0.9294189257142927 ==> log prob of sentence so far: -15.841092187119715
SPANISH: P(n|te) = -0.7624682836833129 ==> log prob of sentence so far: -Infinity

3GRAM: ena
ENGLISH: P(a|en) = -1.1909983053340196 ==> log prob of sentence so far: -18.34670311634148
FRENCH: P(a|en) = -1.087525589742931 ==> log prob of sentence so far: -15.34136853755317
DUTCH: P(a|en) = -1.3140542001290207 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(a|en) = -0.8719121136006229 ==> log prob of sentence so far: -16.71300430072034
SPANISH: P(a|en) = -1.2738823885026829 ==> log prob of sentence so far: -Infinity

3GRAM: nan
ENGLISH: P(n|na) = -0.6612116962752964 ==> log prob of sentence so far: -19.007914812616775
FRENCH: P(n|na) = -1.212847505501566 ==> log prob of sentence so far: -16.554216043054737
DUTCH: P(n|na) = -1.1564045827034248 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(n|na) = -1.7995702741253772 ==> log prob of sentence so far: -18.512574574845715
SPANISH: P(n|na) = -1.196136691157174 ==> log prob of sentence so far: -Infinity

3GRAM: ang
ENGLISH: P(g|an) = -1.494893268769759 ==> log prob of sentence so far: -20.502808081386533
FRENCH: P(g|an) = -1.3287143729389612 ==> log prob of sentence so far: -17.882930415993698
DUTCH: P(g|an) = -1.04195473322671 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(g|an) = -1.7929443442687854 ==> log prob of sentence so far: -20.3055189191145
SPANISH: P(g|an) = -1.4976403765411603 ==> log prob of sentence so far: -Infinity

3GRAM: ngl
ENGLISH: P(l|ng) = -1.238775780052566 ==> log prob of sentence so far: -21.7415838614391
FRENCH: P(l|ng) = -1.1081343285961227 ==> log prob of sentence so far: -18.99106474458982
DUTCH: P(l|ng) = -2.1074439015048054 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(l|ng) = -0.6637168923920546 ==> log prob of sentence so far: -20.969235811506557
SPANISH: P(l|ng) = -0.6368220975871743 ==> log prob of sentence so far: -Infinity

3GRAM: gla
ENGLISH: P(a|gl) = -0.6930175355451498 ==> log prob of sentence so far: -22.43460139698425
FRENCH: P(a|gl) = -0.37268718376907034 ==> log prob of sentence so far: -19.36375192835889
DUTCH: P(a|gl) = -0.5740312677277188 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(a|gl) = -1.021189299069938 ==> log prob of sentence so far: -21.990425110576496
SPANISH: P(a|gl) = -0.9208187539523752 ==> log prob of sentence so far: -Infinity

3GRAM: lai
ENGLISH: P(i|la) = -1.3203633325849482 ==> log prob of sentence so far: -23.754964729569195
FRENCH: P(i|la) = -0.8722090625171588 ==> log prob of sentence so far: -20.23596099087605
DUTCH: P(i|la) = -2.4283373095287994 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(i|la) = -1.9316485319118974 ==> log prob of sentence so far: -23.922073642488392
SPANISH: P(i|la) = -1.7903857068006552 ==> log prob of sentence so far: -Infinity

3GRAM: ais
ENGLISH: P(s|ai) = -1.6676397060564108 ==> log prob of sentence so far: -25.422604435625605
FRENCH: P(s|ai) = -0.653100884297309 ==> log prob of sentence so far: -20.889061875173358
DUTCH: P(s|ai) = -1.2405492482825997 ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(s|ai) = -0.47793836593324157 ==> log prob of sentence so far: -24.400012008421633
SPANISH: P(s|ai) = -0.7082106610709314 ==> log prob of sentence so far: -Infinity

According to the 3gram model, the sentence is in French
----------------
4GRAM MODEL:

4GRAM: cett
ENGLISH: P(t|cet) = -1.586868492432891 ==> log prob of sentence so far: -1.586868492432891
FRENCH: P(t|cet) = -0.2516563769515656 ==> log prob of sentence so far: -0.2516563769515656
DUTCH: P(t|cet) = 0.0 ==> log prob of sentence so far: 0.0
PORTUGUESE: P(t|cet) = -Infinity ==> log prob of sentence so far: -Infinity
SPANISH: P(t|cet) = -Infinity ==> log prob of sentence so far: -Infinity

4GRAM: ette
ENGLISH: P(e|ett) = -0.5727251179254353 ==> log prob of sentence so far: -2.1595936103583266
FRENCH: P(e|ett) = -0.08818915822855805 ==> log prob of sentence so far: -0.33984553518012367
DUTCH: P(e|ett) = -0.11252947862648 ==> log prob of sentence so far: -0.11252947862648
PORTUGUESE: P(e|ett) = -0.3174204118521506 ==> log prob of sentence so far: -Infinity
SPANISH: P(e|ett) = 0.0 ==> log prob of sentence so far: -Infinity

4GRAM: ttep
ENGLISH: P(p|tte) = -Infinity ==> log prob of sentence so far: -Infinity
FRENCH: P(p|tte) = -1.1156980798733873 ==> log prob of sentence so far: -1.455543615053511
DUTCH: P(p|tte) = -2.3201462861110542 ==> log prob of sentence so far: -2.4326757647375343
PORTUGUESE: P(p|tte) = -Infinity ==> log prob of sentence so far: -Infinity
SPANISH: P(p|tte) = -Infinity ==> log prob of sentence so far: -Infinity

4GRAM: teph
ENGLISH: P(h|tep) = -1.0731070983354316 ==> log prob of sentence so far: -Infinity
FRENCH: P(h|tep) = -1.8948696567452525 ==> log prob of sentence so far: -3.3504132717987636
DUTCH: P(h|tep) = -1.6720978579357175 ==> log prob of sentence so far: -4.104773622673251
PORTUGUESE: P(h|tep) = -1.8692317197309762 ==> log prob of sentence so far: -Infinity
SPANISH: P(h|tep) = -Infinity ==> log prob of sentence so far: -Infinity

4GRAM: ephr
ENGLISH: P(r|eph) = -1.2405492482825997 ==> log prob of sentence so far: -Infinity
FRENCH: P(r|eph) = -1.2304489213782739 ==> log prob of sentence so far: -4.580862193177038
DUTCH: P(r|eph) = -Infinity ==> log prob of sentence so far: -Infinity
PORTUGUESE: P(r|eph) = -Infinity ==> log prob of sentence so far: -Infinity
SPANISH: P(r|eph) = NaN ==> log prob of sentence so far: NaN

4GRAM: phra
ENGLISH: P(a|phr) = -0.516629796003336 ==> log prob of sentence so far: -Infinity
FRENCH: P(a|phr) = -0.09017663034908802 ==> log prob of sentence so far: -4.671038823526126
DUTCH: P(a|phr) = NaN ==> log prob of sentence so far: NaN
PORTUGUESE: P(a|phr) = 0.0 ==> log prob of sentence so far: -Infinity
SPANISH: P(a|phr) = NaN ==> log prob of sentence so far: NaN

4GRAM: hras
ENGLISH: P(s|hra) = -0.338818556553381 ==> log prob of sentence so far: -Infinity
FRENCH: P(s|hra) = -0.03218468337140124 ==> log prob of sentence so far: -4.703223506897527
DUTCH: P(s|hra) = -Infinity ==> log prob of sentence so far: NaN
PORTUGUESE: P(s|hra) = -0.12493873660829995 ==> log prob of sentence so far: -Infinity
SPANISH: P(s|hra) = NaN ==> log prob of sentence so far: NaN

4GRAM: rase
ENGLISH: P(e|ras) = -1.36514749527573 ==> log prob of sentence so far: -Infinity
FRENCH: P(e|ras) = -0.6224531499698056 ==> log prob of sentence so far: -5.325676656867333
DUTCH: P(e|ras) = -0.7781512503836436 ==> log prob of sentence so far: NaN
PORTUGUESE: P(e|ras) = -0.5929166118880926 ==> log prob of sentence so far: -Infinity
SPANISH: P(e|ras) = -0.8578932382546862 ==> log prob of sentence so far: NaN

4GRAM: asee
ENGLISH: P(e|ase) = -1.8998205024270962 ==> log prob of sentence so far: -Infinity
FRENCH: P(e|ase) = -1.7732987475892314 ==> log prob of sentence so far: -7.098975404456564
DUTCH: P(e|ase) = -0.38818017138288136 ==> log prob of sentence so far: NaN
PORTUGUESE: P(e|ase) = -1.4866665726258927 ==> log prob of sentence so far: -Infinity
SPANISH: P(e|ase) = -1.5246633450167972 ==> log prob of sentence so far: NaN

4GRAM: sees
ENGLISH: P(s|see) = -1.6507930396519308 ==> log prob of sentence so far: -Infinity
FRENCH: P(s|see) = -0.5647356683041501 ==> log prob of sentence so far: -7.663711072760714
DUTCH: P(s|see) = -Infinity ==> log prob of sentence so far: NaN
PORTUGUESE: P(s|see) = -0.8016323462331666 ==> log prob of sentence so far: -Infinity
SPANISH: P(s|see) = -0.746303883131649 ==> log prob of sentence so far: NaN

4GRAM: eest
ENGLISH: P(t|ees) = -0.8619987905524582 ==> log prob of sentence so far: -Infinity
FRENCH: P(t|ees) = -0.7246598046326334 ==> log prob of sentence so far: -8.388370877393347
DUTCH: P(t|ees) = -0.2372494009700335 ==> log prob of sentence so far: NaN
PORTUGUESE: P(t|ees) = -0.27359174731704156 ==> log prob of sentence so far: -Infinity
SPANISH: P(t|ees) = -0.29872195932705603 ==> log prob of sentence so far: NaN

4GRAM: este
ENGLISH: P(e|est) = -1.0714240919853575 ==> log prob of sentence so far: -Infinity
FRENCH: P(e|est) = -0.7687096941508021 ==> log prob of sentence so far: -9.15708057154415
DUTCH: P(e|est) = -0.5074652711788477 ==> log prob of sentence so far: NaN
PORTUGUESE: P(e|est) = -0.4644666180243991 ==> log prob of sentence so far: -Infinity
SPANISH: P(e|est) = -0.8139896756123511 ==> log prob of sentence so far: NaN

4GRAM: sten
ENGLISH: P(n|ste) = -1.0494578098200498 ==> log prob of sentence so far: -Infinity
FRENCH: P(n|ste) = -0.7202962400244344 ==> log prob of sentence so far: -9.877376811568585
DUTCH: P(n|ste) = -0.6393037870938185 ==> log prob of sentence so far: NaN
PORTUGUESE: P(n|ste) = -1.130793096387536 ==> log prob of sentence so far: -Infinity
SPANISH: P(n|ste) = -0.9244228782212004 ==> log prob of sentence so far: NaN

4GRAM: tena
ENGLISH: P(a|ten) = -1.3814159428499766 ==> log prob of sentence so far: -Infinity
FRENCH: P(a|ten) = -0.8382085382879427 ==> log prob of sentence so far: -10.715585349856527
DUTCH: P(a|ten) = -1.3012979959698652 ==> log prob of sentence so far: NaN
PORTUGUESE: P(a|ten) = -0.8877422951507891 ==> log prob of sentence so far: -Infinity
SPANISH: P(a|ten) = -2.060697840353612 ==> log prob of sentence so far: NaN

4GRAM: enan
ENGLISH: P(n|ena) = -0.6132249887860057 ==> log prob of sentence so far: -Infinity
FRENCH: P(n|ena) = -1.0471405356534755 ==> log prob of sentence so far: -11.762725885510003
DUTCH: P(n|ena) = -1.0093019202190305 ==> log prob of sentence so far: NaN
PORTUGUESE: P(n|ena) = -2.1227618173540255 ==> log prob of sentence so far: -Infinity
SPANISH: P(n|ena) = -1.2120889123272 ==> log prob of sentence so far: NaN

4GRAM: nang
ENGLISH: P(g|nan) = -1.9845273133437926 ==> log prob of sentence so far: -Infinity
FRENCH: P(g|nan) = -1.524117817395886 ==> log prob of sentence so far: -13.28684370290589
DUTCH: P(g|nan) = -1.1991565631243746 ==> log prob of sentence so far: NaN
PORTUGUESE: P(g|nan) = -Infinity ==> log prob of sentence so far: -Infinity
SPANISH: P(g|nan) = -2.2405492482826 ==> log prob of sentence so far: NaN

4GRAM: angl
ENGLISH: P(l|ang) = -1.1751855350946574 ==> log prob of sentence so far: -Infinity
FRENCH: P(l|ang) = -0.6906179299712529 ==> log prob of sentence so far: -13.977461632877143
DUTCH: P(l|ang) = -2.362356792654536 ==> log prob of sentence so far: NaN
PORTUGUESE: P(l|ang) = -Infinity ==> log prob of sentence so far: -Infinity
SPANISH: P(l|ang) = -0.24303804868629447 ==> log prob of sentence so far: NaN

4GRAM: ngla
ENGLISH: P(a|ngl) = -0.9695106778520997 ==> log prob of sentence so far: -Infinity
FRENCH: P(a|ngl) = -0.378634853476651 ==> log prob of sentence so far: -14.356096486353794
DUTCH: P(a|ngl) = -0.5081554884596312 ==> log prob of sentence so far: NaN
PORTUGUESE: P(a|ngl) = -0.9927007612585005 ==> log prob of sentence so far: -Infinity
SPANISH: P(a|ngl) = -1.1139433523068367 ==> log prob of sentence so far: NaN

4GRAM: glai
ENGLISH: P(i|gla) = -1.9800033715837464 ==> log prob of sentence so far: -Infinity
FRENCH: P(i|gla) = -0.4940220052578638 ==> log prob of sentence so far: -14.850118491611658
DUTCH: P(i|gla) = -Infinity ==> log prob of sentence so far: NaN
PORTUGUESE: P(i|gla) = -Infinity ==> log prob of sentence so far: -Infinity
SPANISH: P(i|gla) = -Infinity ==> log prob of sentence so far: NaN

4GRAM: lais
ENGLISH: P(s|lai) = -1.5250448070368452 ==> log prob of sentence so far: -Infinity
FRENCH: P(s|lai) = -0.5628935914595655 ==> log prob of sentence so far: -15.413012083071223
DUTCH: P(s|lai) = -0.6020599913279624 ==> log prob of sentence so far: NaN
PORTUGUESE: P(s|lai) = -Infinity ==> log prob of sentence so far: -Infinity
SPANISH: P(s|lai) = -1.4093694704528195 ==> log prob of sentence so far: NaN

According to the 4gram model, the sentence is in French
----------------
