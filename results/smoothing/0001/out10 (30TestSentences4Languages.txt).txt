J'aime l'IA.

1GRAM MODEL:

1GRAM: j
ENGLISH: P(j) = -2.9450271122992757 ==> log prob of sentence so far: -2.9450271122992757
FRENCH: P(j) = -2.2096202713032533 ==> log prob of sentence so far: -2.2096202713032533
DUTCH: P(j) = -1.7475274327212982 ==> log prob of sentence so far: -1.7475274327212982
PORTUGUESE: P(j) = -2.5149568928001176 ==> log prob of sentence so far: -2.5149568928001176
SPANISH: P(j) = -2.3399548541620923 ==> log prob of sentence so far: -2.3399548541620923

1GRAM: a
ENGLISH: P(a) = -1.0867855556195258 ==> log prob of sentence so far: -4.0318126679188016
FRENCH: P(a) = -1.0763486860884999 ==> log prob of sentence so far: -3.285968957391753
DUTCH: P(a) = -1.1153867027045747 ==> log prob of sentence so far: -2.862914135425873
PORTUGUESE: P(a) = -0.8318864457343189 ==> log prob of sentence so far: -3.3468433385344367
SPANISH: P(a) = -0.9020151018641022 ==> log prob of sentence so far: -3.2419699560261943

1GRAM: i
ENGLISH: P(i) = -1.1625228822481477 ==> log prob of sentence so far: -5.194335550166949
FRENCH: P(i) = -1.1352025755293589 ==> log prob of sentence so far: -4.421171532921112
DUTCH: P(i) = -1.2262442278424301 ==> log prob of sentence so far: -4.089158363268304
PORTUGUESE: P(i) = -1.2396906121447568 ==> log prob of sentence so far: -4.586533950679193
SPANISH: P(i) = -1.2231489528257042 ==> log prob of sentence so far: -4.465118908851899

1GRAM: m
ENGLISH: P(m) = -1.6111138474359583 ==> log prob of sentence so far: -6.805449397602907
FRENCH: P(m) = -1.5129506660683787 ==> log prob of sentence so far: -5.93412219898949
DUTCH: P(m) = -1.6188728802133567 ==> log prob of sentence so far: -5.708031243481661
PORTUGUESE: P(m) = -1.3143867575166186 ==> log prob of sentence so far: -5.900920708195812
SPANISH: P(m) = -1.4970849446101568 ==> log prob of sentence so far: -5.962203853462055

1GRAM: e
ENGLISH: P(e) = -0.9100671672480583 ==> log prob of sentence so far: -7.715516564850965
FRENCH: P(e) = -0.7669595030100278 ==> log prob of sentence so far: -6.701081701999518
DUTCH: P(e) = -0.7296416717344202 ==> log prob of sentence so far: -6.437672915216081
PORTUGUESE: P(e) = -0.880437397131492 ==> log prob of sentence so far: -6.781358105327304
SPANISH: P(e) = -0.8816440718926534 ==> log prob of sentence so far: -6.843847925354709

1GRAM: l
ENGLISH: P(l) = -1.3477680404759382 ==> log prob of sentence so far: -9.063284605326903
FRENCH: P(l) = -1.268210145276409 ==> log prob of sentence so far: -7.969291847275928
DUTCH: P(l) = -1.424888573378627 ==> log prob of sentence so far: -7.862561488594707
PORTUGUESE: P(l) = -1.4370345298164877 ==> log prob of sentence so far: -8.218392635143791
SPANISH: P(l) = -1.280216305870861 ==> log prob of sentence so far: -8.124064231225569

1GRAM: i
ENGLISH: P(i) = -1.1625228822481477 ==> log prob of sentence so far: -10.22580748757505
FRENCH: P(i) = -1.1352025755293589 ==> log prob of sentence so far: -9.104494422805287
DUTCH: P(i) = -1.2262442278424301 ==> log prob of sentence so far: -9.088805716437138
PORTUGUESE: P(i) = -1.2396906121447568 ==> log prob of sentence so far: -9.458083247288549
SPANISH: P(i) = -1.2231489528257042 ==> log prob of sentence so far: -9.347213184051274

1GRAM: a
ENGLISH: P(a) = -1.0867855556195258 ==> log prob of sentence so far: -11.312593043194576
FRENCH: P(a) = -1.0763486860884999 ==> log prob of sentence so far: -10.180843108893786
DUTCH: P(a) = -1.1153867027045747 ==> log prob of sentence so far: -10.204192419141712
PORTUGUESE: P(a) = -0.8318864457343189 ==> log prob of sentence so far: -10.289969693022869
SPANISH: P(a) = -0.9020151018641022 ==> log prob of sentence so far: -10.249228285915375

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: ja
ENGLISH: P(a|j) = -0.6879255250768955 ==> log prob of sentence so far: -0.6879255250768955
FRENCH: P(a|j) = -0.8110395189942394 ==> log prob of sentence so far: -0.8110395189942394
DUTCH: P(a|j) = -1.2157116753362882 ==> log prob of sentence so far: -1.2157116753362882
PORTUGUESE: P(a|j) = -0.374096681295439 ==> log prob of sentence so far: -0.374096681295439
SPANISH: P(a|j) = -0.5727373745622231 ==> log prob of sentence so far: -0.5727373745622231

2GRAM: ai
ENGLISH: P(i|a) = -1.3482451351695797 ==> log prob of sentence so far: -2.036170660246475
FRENCH: P(i|a) = -0.696524586220923 ==> log prob of sentence so far: -1.5075641052151623
DUTCH: P(i|a) = -2.256635077784397 ==> log prob of sentence so far: -3.472346753120685
PORTUGUESE: P(i|a) = -1.4740771803900676 ==> log prob of sentence so far: -1.8481738616855066
SPANISH: P(i|a) = -1.9696512257535392 ==> log prob of sentence so far: -2.5423886003157623

2GRAM: im
ENGLISH: P(m|i) = -1.3479746537468398 ==> log prob of sentence so far: -3.384145313993315
FRENCH: P(m|i) = -1.5427405073207079 ==> log prob of sentence so far: -3.05030461253587
DUTCH: P(m|i) = -2.1539209593341178 ==> log prob of sentence so far: -5.626267712454803
PORTUGUESE: P(m|i) = -1.1489542969681235 ==> log prob of sentence so far: -2.99712815865363
SPANISH: P(m|i) = -1.4591015483632326 ==> log prob of sentence so far: -4.001490148678995

2GRAM: me
ENGLISH: P(e|m) = -0.594327582688585 ==> log prob of sentence so far: -3.9784728966819003
FRENCH: P(e|m) = -0.4303478891398023 ==> log prob of sentence so far: -3.4806525016756726
DUTCH: P(e|m) = -0.45919100953454645 ==> log prob of sentence so far: -6.085458721989349
PORTUGUESE: P(e|m) = -0.7090561122253235 ==> log prob of sentence so far: -3.7061842708789534
SPANISH: P(e|m) = -0.5843536650182802 ==> log prob of sentence so far: -4.5858438136972755

2GRAM: el
ENGLISH: P(l|e) = -1.3277714623143466 ==> log prob of sentence so far: -5.306244358996247
FRENCH: P(l|e) = -1.2044160491360067 ==> log prob of sentence so far: -4.685068550811679
DUTCH: P(l|e) = -1.163689929268216 ==> log prob of sentence so far: -7.249148651257565
PORTUGUESE: P(l|e) = -1.1286515794375676 ==> log prob of sentence so far: -4.834835850316521
SPANISH: P(l|e) = -0.9033131575225964 ==> log prob of sentence so far: -5.489156971219872

2GRAM: li
ENGLISH: P(i|l) = -0.9280383306091224 ==> log prob of sentence so far: -6.23428268960537
FRENCH: P(i|l) = -1.202907271383646 ==> log prob of sentence so far: -5.887975822195325
DUTCH: P(i|l) = -0.7909619263713471 ==> log prob of sentence so far: -8.040110577628912
PORTUGUESE: P(i|l) = -1.0725941890314552 ==> log prob of sentence so far: -5.907430039347976
SPANISH: P(i|l) = -1.2536149091759239 ==> log prob of sentence so far: -6.742771880395796

2GRAM: ia
ENGLISH: P(a|i) = -1.753338657780753 ==> log prob of sentence so far: -7.987621347386122
FRENCH: P(a|i) = -1.7744783385711305 ==> log prob of sentence so far: -7.662454160766456
DUTCH: P(a|i) = -2.0509929659161736 ==> log prob of sentence so far: -10.091103543545085
PORTUGUESE: P(a|i) = -0.8371889341029388 ==> log prob of sentence so far: -6.744618973450915
SPANISH: P(a|i) = -0.8671993008908702 ==> log prob of sentence so far: -7.6099711812866655

According to the 2gram model, the sentence is in Portuguese
----------------
3GRAM MODEL:

3GRAM: jai
ENGLISH: P(i|ja) = -1.871174195580627 ==> log prob of sentence so far: -1.871174195580627
FRENCH: P(i|ja) = -0.7302977032268454 ==> log prob of sentence so far: -0.7302977032268454
DUTCH: P(i|ja) = -6.639489079090343 ==> log prob of sentence so far: -6.639489079090343
PORTUGUESE: P(i|ja) = -1.9731173814733003 ==> log prob of sentence so far: -1.9731173814733003
SPANISH: P(i|ja) = -2.629368822441366 ==> log prob of sentence so far: -2.629368822441366

3GRAM: aim
ENGLISH: P(m|ai) = -1.9516355724832233 ==> log prob of sentence so far: -3.82280976806385
FRENCH: P(m|ai) = -2.00261258918479 ==> log prob of sentence so far: -2.7329102924116353
DUTCH: P(m|ai) = -2.2405123104150286 ==> log prob of sentence so far: -8.880001389505372
PORTUGUESE: P(m|ai) = -1.3934732236380678 ==> log prob of sentence so far: -3.366590605111368
SPANISH: P(m|ai) = -1.1715446513211674 ==> log prob of sentence so far: -3.8009134737625336

3GRAM: ime
ENGLISH: P(e|im) = -0.59228676711466 ==> log prob of sentence so far: -4.4150965351785105
FRENCH: P(e|im) = -0.4825506946556722 ==> log prob of sentence so far: -3.2154609870673077
DUTCH: P(e|im) = -0.5969860979128019 ==> log prob of sentence so far: -9.476987487418175
PORTUGUESE: P(e|im) = -0.4923090626768416 ==> log prob of sentence so far: -3.8588996677882097
SPANISH: P(e|im) = -0.7059108112448308 ==> log prob of sentence so far: -4.506824285007364

3GRAM: mel
ENGLISH: P(l|me) = -1.6005943655107606 ==> log prob of sentence so far: -6.015690900689271
FRENCH: P(l|me) = -1.5059247790763255 ==> log prob of sentence so far: -4.721385766143634
DUTCH: P(l|me) = -1.5131662744007401 ==> log prob of sentence so far: -10.990153761818915
PORTUGUESE: P(l|me) = -1.2266634728742891 ==> log prob of sentence so far: -5.085563140662499
SPANISH: P(l|me) = -0.7073805557709211 ==> log prob of sentence so far: -5.214204840778286

3GRAM: eli
ENGLISH: P(i|el) = -0.8552764671417498 ==> log prob of sentence so far: -6.870967367831021
FRENCH: P(i|el) = -1.2612480785272904 ==> log prob of sentence so far: -5.982633844670924
DUTCH: P(i|el) = -0.6921120919975182 ==> log prob of sentence so far: -11.682265853816434
PORTUGUESE: P(i|el) = -1.1054594606076387 ==> log prob of sentence so far: -6.191022601270138
SPANISH: P(i|el) = -1.4512124979984729 ==> log prob of sentence so far: -6.665417338776758

3GRAM: lia
ENGLISH: P(a|li) = -1.4735738223964316 ==> log prob of sentence so far: -8.344541190227453
FRENCH: P(a|li) = -1.611294355770976 ==> log prob of sentence so far: -7.5939282004419
DUTCH: P(a|li) = -2.695998484174147 ==> log prob of sentence so far: -14.378264337990581
PORTUGUESE: P(a|li) = -0.8559491753222171 ==> log prob of sentence so far: -7.046971776592355
SPANISH: P(a|li) = -1.0156259563777605 ==> log prob of sentence so far: -7.6810432951545184

According to the 3gram model, the sentence is in Portuguese
----------------
4GRAM MODEL:

4GRAM: jaim
ENGLISH: P(m|jai) = -4.477497480263115 ==> log prob of sentence so far: -4.477497480263115
FRENCH: P(m|jai) = -1.135666956644173 ==> log prob of sentence so far: -1.135666956644173
DUTCH: P(m|jai) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
PORTUGUESE: P(m|jai) = -4.477497480263115 ==> log prob of sentence so far: -4.477497480263115
SPANISH: P(m|jai) = -0.0010842730001722966 ==> log prob of sentence so far: -0.0010842730001722966

4GRAM: aime
ENGLISH: P(e|aim) = -0.2293638347765376 ==> log prob of sentence so far: -4.706861315039653
FRENCH: P(e|aim) = -0.44412984021211327 ==> log prob of sentence so far: -1.5797967968562863
DUTCH: P(e|aim) = -4.001127700277035 ==> log prob of sentence so far: -5.432491464436023
PORTUGUESE: P(e|aim) = -1.6334512871752067 ==> log prob of sentence so far: -6.110948767438321
SPANISH: P(e|aim) = -5.505185263313127 ==> log prob of sentence so far: -5.506269536313299

4GRAM: imel
ENGLISH: P(l|ime) = -1.555030143614191 ==> log prob of sentence so far: -6.261891458653844
FRENCH: P(l|ime) = -1.8976222353088799 ==> log prob of sentence so far: -3.477419032165166
DUTCH: P(l|ime) = -0.7781719302854563 ==> log prob of sentence so far: -6.210663394721479
PORTUGUESE: P(l|ime) = -2.146110354212744 ==> log prob of sentence so far: -8.257059121651066
SPANISH: P(l|ime) = -1.55629948489831 ==> log prob of sentence so far: -7.062569021211609

4GRAM: meli
ENGLISH: P(i|mel) = -0.8908611832285693 ==> log prob of sentence so far: -7.152752641882413
FRENCH: P(i|mel) = -1.6110122046147868 ==> log prob of sentence so far: -5.088431236779953
DUTCH: P(i|mel) = -0.5698846141339425 ==> log prob of sentence so far: -6.780548008855422
PORTUGUESE: P(i|mel) = -1.308210597467607 ==> log prob of sentence so far: -9.565269719118673
SPANISH: P(i|mel) = -2.163599643019702 ==> log prob of sentence so far: -9.226168664231311

4GRAM: elia
ENGLISH: P(a|eli) = -1.755286552607055 ==> log prob of sentence so far: -8.908039194489469
FRENCH: P(a|eli) = -1.7000517728464615 ==> log prob of sentence so far: -6.788483009626415
DUTCH: P(a|eli) = -7.014521630792966 ==> log prob of sentence so far: -13.795069639648387
PORTUGUESE: P(a|eli) = -1.7455875016048021 ==> log prob of sentence so far: -11.310857220723475
SPANISH: P(a|eli) = -1.4515666489040957 ==> log prob of sentence so far: -10.677735313135408

According to the 4gram model, the sentence is in French
----------------
