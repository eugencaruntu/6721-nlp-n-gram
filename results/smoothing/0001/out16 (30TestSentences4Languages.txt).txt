They are bad hombres.

1GRAM MODEL:

1GRAM: t
ENGLISH: P(t) = -1.0342201967944733 ==> log prob of sentence so far: -1.0342201967944733
FRENCH: P(t) = -1.1659631947814317 ==> log prob of sentence so far: -1.1659631947814317
DUTCH: P(t) = -1.2498796929138756 ==> log prob of sentence so far: -1.2498796929138756
PORTUGUESE: P(t) = -1.3798312063329417 ==> log prob of sentence so far: -1.3798312063329417
SPANISH: P(t) = -1.403591206998156 ==> log prob of sentence so far: -1.403591206998156

1GRAM: h
ENGLISH: P(h) = -1.1802717018481115 ==> log prob of sentence so far: -2.214491898642585
FRENCH: P(h) = -2.105661505277561 ==> log prob of sentence so far: -3.271624700058993
DUTCH: P(h) = -1.4907882132713295 ==> log prob of sentence so far: -2.7406679061852053
PORTUGUESE: P(h) = -1.8092516224324593 ==> log prob of sentence so far: -3.189082828765401
SPANISH: P(h) = -1.8922415939752475 ==> log prob of sentence so far: -3.2958328009734035

1GRAM: e
ENGLISH: P(e) = -0.9100671672480583 ==> log prob of sentence so far: -3.1245590658906433
FRENCH: P(e) = -0.7669595030100278 ==> log prob of sentence so far: -4.038584203069021
DUTCH: P(e) = -0.7296416717344202 ==> log prob of sentence so far: -3.4703095779196254
PORTUGUESE: P(e) = -0.880437397131492 ==> log prob of sentence so far: -4.069520225896893
SPANISH: P(e) = -0.8816440718926534 ==> log prob of sentence so far: -4.177476872866057

1GRAM: y
ENGLISH: P(y) = -1.7506034466063691 ==> log prob of sentence so far: -4.875162512497012
FRENCH: P(y) = -2.672363548703873 ==> log prob of sentence so far: -6.710947751772894
DUTCH: P(y) = -3.5708485069501386 ==> log prob of sentence so far: -7.041158084869764
PORTUGUESE: P(y) = -3.3458344054838407 ==> log prob of sentence so far: -7.415354631380733
SPANISH: P(y) = -1.910660668875661 ==> log prob of sentence so far: -6.088137541741718

1GRAM: a
ENGLISH: P(a) = -1.0867855556195258 ==> log prob of sentence so far: -5.961948068116538
FRENCH: P(a) = -1.0763486860884999 ==> log prob of sentence so far: -7.787296437861394
DUTCH: P(a) = -1.1153867027045747 ==> log prob of sentence so far: -8.156544787574338
PORTUGUESE: P(a) = -0.8318864457343189 ==> log prob of sentence so far: -8.247241077115053
SPANISH: P(a) = -0.9020151018641022 ==> log prob of sentence so far: -6.99015264360582

1GRAM: r
ENGLISH: P(r) = -1.2612711446511347 ==> log prob of sentence so far: -7.223219212767672
FRENCH: P(r) = -1.1840252806546812 ==> log prob of sentence so far: -8.971321718516075
DUTCH: P(r) = -1.254198637640079 ==> log prob of sentence so far: -9.410743425214417
PORTUGUESE: P(r) = -1.194537098985067 ==> log prob of sentence so far: -9.44177817610012
SPANISH: P(r) = -1.191039284734038 ==> log prob of sentence so far: -8.181191928339858

1GRAM: e
ENGLISH: P(e) = -0.9100671672480583 ==> log prob of sentence so far: -8.133286380015731
FRENCH: P(e) = -0.7669595030100278 ==> log prob of sentence so far: -9.738281221526103
DUTCH: P(e) = -0.7296416717344202 ==> log prob of sentence so far: -10.140385096948837
PORTUGUESE: P(e) = -0.880437397131492 ==> log prob of sentence so far: -10.322215573231613
SPANISH: P(e) = -0.8816440718926534 ==> log prob of sentence so far: -9.062836000232512

1GRAM: b
ENGLISH: P(b) = -1.7519588996635036 ==> log prob of sentence so far: -9.885245279679234
FRENCH: P(b) = -2.0519153276062747 ==> log prob of sentence so far: -11.790196549132379
DUTCH: P(b) = -1.8074883611170944 ==> log prob of sentence so far: -11.947873458065931
PORTUGUESE: P(b) = -2.0435293524217983 ==> log prob of sentence so far: -12.365744925653411
SPANISH: P(b) = -1.8063459379660145 ==> log prob of sentence so far: -10.869181938198526

1GRAM: a
ENGLISH: P(a) = -1.0867855556195258 ==> log prob of sentence so far: -10.97203083529876
FRENCH: P(a) = -1.0763486860884999 ==> log prob of sentence so far: -12.866545235220878
DUTCH: P(a) = -1.1153867027045747 ==> log prob of sentence so far: -13.063260160770506
PORTUGUESE: P(a) = -0.8318864457343189 ==> log prob of sentence so far: -13.19763137138773
SPANISH: P(a) = -0.9020151018641022 ==> log prob of sentence so far: -11.771197040062628

1GRAM: d
ENGLISH: P(d) = -1.3961796679426524 ==> log prob of sentence so far: -12.368210503241412
FRENCH: P(d) = -1.4202617777502686 ==> log prob of sentence so far: -14.286807012971146
DUTCH: P(d) = -1.2011818240390888 ==> log prob of sentence so far: -14.264441984809595
PORTUGUESE: P(d) = -1.3354018349367311 ==> log prob of sentence so far: -14.533033206324461
SPANISH: P(d) = -1.2907600057475275 ==> log prob of sentence so far: -13.061957045810155

1GRAM: h
ENGLISH: P(h) = -1.1802717018481115 ==> log prob of sentence so far: -13.548482205089524
FRENCH: P(h) = -2.105661505277561 ==> log prob of sentence so far: -16.392468518248705
DUTCH: P(h) = -1.4907882132713295 ==> log prob of sentence so far: -15.755230198080925
PORTUGUESE: P(h) = -1.8092516224324593 ==> log prob of sentence so far: -16.34228482875692
SPANISH: P(h) = -1.8922415939752475 ==> log prob of sentence so far: -14.954198639785403

1GRAM: o
ENGLISH: P(o) = -1.1377809800580025 ==> log prob of sentence so far: -14.686263185147526
FRENCH: P(o) = -1.2743410778784847 ==> log prob of sentence so far: -17.66680959612719
DUTCH: P(o) = -1.199814333953728 ==> log prob of sentence so far: -16.955044532034652
PORTUGUESE: P(o) = -0.9821306993687576 ==> log prob of sentence so far: -17.324415528125677
SPANISH: P(o) = -0.9966256638806575 ==> log prob of sentence so far: -15.95082430366606

1GRAM: m
ENGLISH: P(m) = -1.6111138474359583 ==> log prob of sentence so far: -16.297377032583483
FRENCH: P(m) = -1.5129506660683787 ==> log prob of sentence so far: -19.17976026219557
DUTCH: P(m) = -1.6188728802133567 ==> log prob of sentence so far: -18.573917412248008
PORTUGUESE: P(m) = -1.3143867575166186 ==> log prob of sentence so far: -18.638802285642296
SPANISH: P(m) = -1.4970849446101568 ==> log prob of sentence so far: -17.44790924827622

1GRAM: b
ENGLISH: P(b) = -1.7519588996635036 ==> log prob of sentence so far: -18.049335932246986
FRENCH: P(b) = -2.0519153276062747 ==> log prob of sentence so far: -21.231675589801842
DUTCH: P(b) = -1.8074883611170944 ==> log prob of sentence so far: -20.381405773365103
PORTUGUESE: P(b) = -2.0435293524217983 ==> log prob of sentence so far: -20.682331638064095
SPANISH: P(b) = -1.8063459379660145 ==> log prob of sentence so far: -19.254255186242233

1GRAM: r
ENGLISH: P(r) = -1.2612711446511347 ==> log prob of sentence so far: -19.310607076898123
FRENCH: P(r) = -1.1840252806546812 ==> log prob of sentence so far: -22.415700870456522
DUTCH: P(r) = -1.254198637640079 ==> log prob of sentence so far: -21.635604411005183
PORTUGUESE: P(r) = -1.194537098985067 ==> log prob of sentence so far: -21.87686873704916
SPANISH: P(r) = -1.191039284734038 ==> log prob of sentence so far: -20.445294470976272

1GRAM: e
ENGLISH: P(e) = -0.9100671672480583 ==> log prob of sentence so far: -20.22067424414618
FRENCH: P(e) = -0.7669595030100278 ==> log prob of sentence so far: -23.18266037346655
DUTCH: P(e) = -0.7296416717344202 ==> log prob of sentence so far: -22.365246082739603
PORTUGUESE: P(e) = -0.880437397131492 ==> log prob of sentence so far: -22.757306134180652
SPANISH: P(e) = -0.8816440718926534 ==> log prob of sentence so far: -21.326938542868927

1GRAM: s
ENGLISH: P(s) = -1.1711384807336087 ==> log prob of sentence so far: -21.39181272487979
FRENCH: P(s) = -1.064443204906708 ==> log prob of sentence so far: -24.24710357837326
DUTCH: P(s) = -1.4604412026800553 ==> log prob of sentence so far: -23.825687285419658
PORTUGUESE: P(s) = -1.1128885199376846 ==> log prob of sentence so far: -23.870194654118336
SPANISH: P(s) = -1.1432550023265262 ==> log prob of sentence so far: -22.470193545195453

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: th
ENGLISH: P(h|t) = -0.4192245655228396 ==> log prob of sentence so far: -0.4192245655228396
FRENCH: P(h|t) = -2.1277061822809054 ==> log prob of sentence so far: -2.1277061822809054
DUTCH: P(h|t) = -1.356418434753906 ==> log prob of sentence so far: -1.356418434753906
PORTUGUESE: P(h|t) = -2.189181265510812 ==> log prob of sentence so far: -2.189181265510812
SPANISH: P(h|t) = -3.68309270431484 ==> log prob of sentence so far: -3.68309270431484

2GRAM: he
ENGLISH: P(e|h) = -0.36994571251100583 ==> log prob of sentence so far: -0.7891702780338454
FRENCH: P(e|h) = -0.4571216132374326 ==> log prob of sentence so far: -2.584827795518338
DUTCH: P(e|h) = -0.43975814780196576 ==> log prob of sentence so far: -1.796176582555872
PORTUGUESE: P(e|h) = -0.5741244305350686 ==> log prob of sentence so far: -2.7633056960458804
SPANISH: P(e|h) = -0.8481753557313617 ==> log prob of sentence so far: -4.531268060046202

2GRAM: ey
ENGLISH: P(y|e) = -1.8314297805217912 ==> log prob of sentence so far: -2.6206000585556364
FRENCH: P(y|e) = -3.46757743428665 ==> log prob of sentence so far: -6.052405229804988
DUTCH: P(y|e) = -4.4018052355522395 ==> log prob of sentence so far: -6.197981818108111
PORTUGUESE: P(y|e) = -3.2841871629623176 ==> log prob of sentence so far: -6.047492859008198
SPANISH: P(y|e) = -2.010537920368685 ==> log prob of sentence so far: -6.541805980414887

2GRAM: ya
ENGLISH: P(a|y) = -1.0420795317585871 ==> log prob of sentence so far: -3.6626795903142235
FRENCH: P(a|y) = -0.6192001287095872 ==> log prob of sentence so far: -6.671605358514575
DUTCH: P(a|y) = -1.1422377356920217 ==> log prob of sentence so far: -7.340219553800133
PORTUGUESE: P(a|y) = -1.5271968225355728 ==> log prob of sentence so far: -7.574689681543771
SPANISH: P(a|y) = -0.7709544400880558 ==> log prob of sentence so far: -7.312760420502943

2GRAM: ar
ENGLISH: P(r|a) = -0.985420063231861 ==> log prob of sentence so far: -4.648099653546085
FRENCH: P(r|a) = -1.0331352651346224 ==> log prob of sentence so far: -7.704740623649197
DUTCH: P(r|a) = -0.9127486552516607 ==> log prob of sentence so far: -8.252968209051794
PORTUGUESE: P(r|a) = -1.010573902122766 ==> log prob of sentence so far: -8.585263583666537
SPANISH: P(r|a) = -0.9443454432936655 ==> log prob of sentence so far: -8.257105863796609

2GRAM: re
ENGLISH: P(e|r) = -0.6281156240702467 ==> log prob of sentence so far: -5.276215277616331
FRENCH: P(e|r) = -0.4908603992084843 ==> log prob of sentence so far: -8.195601022857682
DUTCH: P(e|r) = -0.7943708230201308 ==> log prob of sentence so far: -9.047339032071925
PORTUGUESE: P(e|r) = -0.738408097289601 ==> log prob of sentence so far: -9.323671680956139
SPANISH: P(e|r) = -0.6817295650586808 ==> log prob of sentence so far: -8.93883542885529

2GRAM: eb
ENGLISH: P(b|e) = -1.6931035472330644 ==> log prob of sentence so far: -6.969318824849395
FRENCH: P(b|e) = -2.134635309455102 ==> log prob of sentence so far: -10.330236332312783
DUTCH: P(b|e) = -1.7346781615968188 ==> log prob of sentence so far: -10.782017193668743
PORTUGUESE: P(b|e) = -2.0929252098864333 ==> log prob of sentence so far: -11.416596890842571
SPANISH: P(b|e) = -2.101301016829262 ==> log prob of sentence so far: -11.040136445684553

2GRAM: ba
ENGLISH: P(a|b) = -1.2017577530481633 ==> log prob of sentence so far: -8.171076577897558
FRENCH: P(a|b) = -0.8256303519546335 ==> log prob of sentence so far: -11.155866684267416
DUTCH: P(a|b) = -1.1574980877540457 ==> log prob of sentence so far: -11.93951528142279
PORTUGUESE: P(a|b) = -0.6425201713789307 ==> log prob of sentence so far: -12.059117062221501
SPANISH: P(a|b) = -0.4704184076666872 ==> log prob of sentence so far: -11.51055485335124

2GRAM: ad
ENGLISH: P(d|a) = -1.3608745354130198 ==> log prob of sentence so far: -9.531951113310578
FRENCH: P(d|a) = -1.6548379642725894 ==> log prob of sentence so far: -12.810704648540005
DUTCH: P(d|a) = -1.3383053424525784 ==> log prob of sentence so far: -13.277820623875368
PORTUGUESE: P(d|a) = -1.0654628781151698 ==> log prob of sentence so far: -13.124579940336671
SPANISH: P(d|a) = -1.018364926750513 ==> log prob of sentence so far: -12.528919780101752

2GRAM: dh
ENGLISH: P(h|d) = -1.4165748979711432 ==> log prob of sentence so far: -10.948526011281722
FRENCH: P(h|d) = -2.3212915058425407 ==> log prob of sentence so far: -15.131996154382545
DUTCH: P(h|d) = -1.722456733435184 ==> log prob of sentence so far: -15.000277357310551
PORTUGUESE: P(h|d) = -4.018324259351542 ==> log prob of sentence so far: -17.142904199688214
SPANISH: P(h|d) = -3.192285053897412 ==> log prob of sentence so far: -15.721204833999163

2GRAM: ho
ENGLISH: P(o|h) = -1.1036237117294738 ==> log prob of sentence so far: -12.052149723011196
FRENCH: P(o|h) = -0.8324131354765649 ==> log prob of sentence so far: -15.964409289859109
DUTCH: P(o|h) = -1.0173147475160884 ==> log prob of sentence so far: -16.01759210482664
PORTUGUESE: P(o|h) = -0.5397555324379388 ==> log prob of sentence so far: -17.682659732126154
SPANISH: P(o|h) = -0.53265146317386 ==> log prob of sentence so far: -16.253856297173023

2GRAM: om
ENGLISH: P(m|o) = -1.1832305487005526 ==> log prob of sentence so far: -13.235380271711747
FRENCH: P(m|o) = -1.143176494150575 ==> log prob of sentence so far: -17.107585784009686
DUTCH: P(m|o) = -1.2690958069371892 ==> log prob of sentence so far: -17.286687911763828
PORTUGUESE: P(m|o) = -1.0086118568264915 ==> log prob of sentence so far: -18.691271588952645
SPANISH: P(m|o) = -1.1212852736571415 ==> log prob of sentence so far: -17.375141570830163

2GRAM: mb
ENGLISH: P(b|m) = -1.5584561060315407 ==> log prob of sentence so far: -14.793836377743288
FRENCH: P(b|m) = -1.4619488976166237 ==> log prob of sentence so far: -18.56953468162631
DUTCH: P(b|m) = -1.0951229865053975 ==> log prob of sentence so far: -18.381810898269226
PORTUGUESE: P(b|m) = -1.524882696271183 ==> log prob of sentence so far: -20.21615428522383
SPANISH: P(b|m) = -1.2482864151229431 ==> log prob of sentence so far: -18.623427985953107

2GRAM: br
ENGLISH: P(r|b) = -1.240739837414514 ==> log prob of sentence so far: -16.034576215157802
FRENCH: P(r|b) = -0.7689991874392972 ==> log prob of sentence so far: -19.338533869065607
DUTCH: P(r|b) = -1.123858487376029 ==> log prob of sentence so far: -19.505669385645255
PORTUGUESE: P(r|b) = -0.6490934011103765 ==> log prob of sentence so far: -20.865247686334207
SPANISH: P(r|b) = -0.8220407345998821 ==> log prob of sentence so far: -19.445468720552988

2GRAM: re
ENGLISH: P(e|r) = -0.6281156240702467 ==> log prob of sentence so far: -16.66269183922805
FRENCH: P(e|r) = -0.4908603992084843 ==> log prob of sentence so far: -19.82939426827409
DUTCH: P(e|r) = -0.7943708230201308 ==> log prob of sentence so far: -20.300040208665386
PORTUGUESE: P(e|r) = -0.738408097289601 ==> log prob of sentence so far: -21.603655783623807
SPANISH: P(e|r) = -0.6817295650586808 ==> log prob of sentence so far: -20.12719828561167

2GRAM: es
ENGLISH: P(s|e) = -0.9448348293174055 ==> log prob of sentence so far: -17.607526668545454
FRENCH: P(s|e) = -0.7261117562826892 ==> log prob of sentence so far: -20.555506024556777
DUTCH: P(s|e) = -1.4885357923574787 ==> log prob of sentence so far: -21.788576001022864
PORTUGUESE: P(s|e) = -0.8219917719022634 ==> log prob of sentence so far: -22.42564755552607
SPANISH: P(s|e) = -0.7772969067309046 ==> log prob of sentence so far: -20.904495192342573

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: the
ENGLISH: P(e|th) = -0.20437437262183536 ==> log prob of sentence so far: -0.20437437262183536
FRENCH: P(e|th) = -0.3708847106408896 ==> log prob of sentence so far: -0.3708847106408896
DUTCH: P(e|th) = -0.43911309800073034 ==> log prob of sentence so far: -0.43911309800073034
PORTUGUESE: P(e|th) = -0.21714544769434793 ==> log prob of sentence so far: -0.21714544769434793
SPANISH: P(e|th) = -4.477497480263115 ==> log prob of sentence so far: -4.477497480263115

3GRAM: hey
ENGLISH: P(y|he) = -1.5298554998791491 ==> log prob of sentence so far: -1.7342298725009846
FRENCH: P(y|he) = -7.27277018909438 ==> log prob of sentence so far: -7.643654899735269
DUTCH: P(y|he) = -7.680698265234837 ==> log prob of sentence so far: -8.119811363235568
PORTUGUESE: P(y|he) = -6.950366120255055 ==> log prob of sentence so far: -7.167511567949402
SPANISH: P(y|he) = -1.6476610622952865 ==> log prob of sentence so far: -6.125158542558402

3GRAM: eya
ENGLISH: P(a|ey) = -1.0551726165409137 ==> log prob of sentence so far: -2.7894024890418985
FRENCH: P(a|ey) = -0.8239297309871881 ==> log prob of sentence so far: -8.467584630722458
DUTCH: P(a|ey) = -4.477497480263115 ==> log prob of sentence so far: -12.597308843498684
PORTUGUESE: P(a|ey) = -5.146208682879298 ==> log prob of sentence so far: -12.3137202508287
SPANISH: P(a|ey) = -0.9081832352409506 ==> log prob of sentence so far: -7.033341777799352

3GRAM: yar
ENGLISH: P(r|ya) = -0.8497041169743529 ==> log prob of sentence so far: -3.6391066060162514
FRENCH: P(r|ya) = -1.7039022075707313 ==> log prob of sentence so far: -10.17148683829319
DUTCH: P(r|ya) = -4.903231109767353 ==> log prob of sentence so far: -17.50053995326604
PORTUGUESE: P(r|ya) = -4.477497480263115 ==> log prob of sentence so far: -16.791217731091816
SPANISH: P(r|ya) = -1.5153687166636 ==> log prob of sentence so far: -8.548710494462952

3GRAM: are
ENGLISH: P(e|ar) = -0.8590837887473867 ==> log prob of sentence so far: -4.498190394763638
FRENCH: P(e|ar) = -1.0005658799025068 ==> log prob of sentence so far: -11.172052718195696
DUTCH: P(e|ar) = -0.7224900809770622 ==> log prob of sentence so far: -18.223030034243102
PORTUGUESE: P(e|ar) = -0.793666399662085 ==> log prob of sentence so far: -17.584884130753903
SPANISH: P(e|ar) = -0.8951388540096054 ==> log prob of sentence so far: -9.443849348472558

3GRAM: reb
ENGLISH: P(b|re) = -1.6523918933965047 ==> log prob of sentence so far: -6.150582288160143
FRENCH: P(b|re) = -2.3699144491870303 ==> log prob of sentence so far: -13.541967167382726
DUTCH: P(b|re) = -2.2978799462646915 ==> log prob of sentence so far: -20.520909980507795
PORTUGUESE: P(b|re) = -2.3239361023328957 ==> log prob of sentence so far: -19.9088202330868
SPANISH: P(b|re) = -2.2129207263478285 ==> log prob of sentence so far: -11.656770074820386

3GRAM: eba
ENGLISH: P(a|eb) = -1.1248134384566117 ==> log prob of sentence so far: -7.275395726616755
FRENCH: P(a|eb) = -0.5004353992087822 ==> log prob of sentence so far: -14.042402566591509
DUTCH: P(a|eb) = -1.2552727557779337 ==> log prob of sentence so far: -21.77618273628573
PORTUGUESE: P(a|eb) = -0.6269972880836426 ==> log prob of sentence so far: -20.535817521170443
SPANISH: P(a|eb) = -0.5638829720783682 ==> log prob of sentence so far: -12.220653046898754

3GRAM: bad
ENGLISH: P(d|ba) = -1.4020566060829296 ==> log prob of sentence so far: -8.677452332699684
FRENCH: P(d|ba) = -2.6622650302286495 ==> log prob of sentence so far: -16.70466759682016
DUTCH: P(d|ba) = -1.2002259308818881 ==> log prob of sentence so far: -22.976408667167618
PORTUGUESE: P(d|ba) = -1.5544416467647282 ==> log prob of sentence so far: -22.090259167935173
SPANISH: P(d|ba) = -1.34196344537812 ==> log prob of sentence so far: -13.562616492276874

3GRAM: adh
ENGLISH: P(h|ad) = -1.6384890208519585 ==> log prob of sentence so far: -10.315941353551642
FRENCH: P(h|ad) = -1.9331476922895705 ==> log prob of sentence so far: -18.637815289109728
DUTCH: P(h|ad) = -1.4446691735318937 ==> log prob of sentence so far: -24.42107784069951
PORTUGUESE: P(h|ad) = -3.4375490172316137 ==> log prob of sentence so far: -25.527808185166787
SPANISH: P(h|ad) = -2.844159441695377 ==> log prob of sentence so far: -16.406775933972252

3GRAM: dho
ENGLISH: P(o|dh) = -0.9323871462043976 ==> log prob of sentence so far: -11.24832849975604
FRENCH: P(o|dh) = -0.6055559514573801 ==> log prob of sentence so far: -19.243371240567107
DUTCH: P(o|dh) = -1.0818883914994968 ==> log prob of sentence so far: -25.502966232199007
PORTUGUESE: P(o|dh) = -4.001127700277035 ==> log prob of sentence so far: -29.528935885443822
SPANISH: P(o|dh) = -1.0792319057161235 ==> log prob of sentence so far: -17.486007839688376

3GRAM: hom
ENGLISH: P(m|ho) = -1.4926012261877906 ==> log prob of sentence so far: -12.74092972594383
FRENCH: P(m|ho) = -0.5151581882070531 ==> log prob of sentence so far: -19.75852942877416
DUTCH: P(m|ho) = -2.0637736810417637 ==> log prob of sentence so far: -27.56673991324077
PORTUGUESE: P(m|ho) = -1.0347627818271299 ==> log prob of sentence so far: -30.563698667270952
SPANISH: P(m|ho) = -0.8548878185204718 ==> log prob of sentence so far: -18.340895658208847

3GRAM: omb
ENGLISH: P(b|om) = -1.7956349183699611 ==> log prob of sentence so far: -14.536564644313792
FRENCH: P(b|om) = -0.9381261215383278 ==> log prob of sentence so far: -20.69665555031249
DUTCH: P(b|om) = -1.5324356072124916 ==> log prob of sentence so far: -29.09917552045326
PORTUGUESE: P(b|om) = -1.6198552535770265 ==> log prob of sentence so far: -32.18355392084798
SPANISH: P(b|om) = -1.0119085435553965 ==> log prob of sentence so far: -19.352804201764243

3GRAM: mbr
ENGLISH: P(r|mb) = -1.2948798132383856 ==> log prob of sentence so far: -15.831444457552177
FRENCH: P(r|mb) = -0.3771467407810469 ==> log prob of sentence so far: -21.073802291093536
DUTCH: P(r|mb) = -1.896523307515423 ==> log prob of sentence so far: -30.995698827968685
PORTUGUESE: P(r|mb) = -0.5860748913261139 ==> log prob of sentence so far: -32.76962881217409
SPANISH: P(r|mb) = -0.3679783473433193 ==> log prob of sentence so far: -19.72078254910756

3GRAM: bre
ENGLISH: P(e|br) = -0.5780848073807351 ==> log prob of sentence so far: -16.40952926493291
FRENCH: P(e|br) = -0.3706128040671514 ==> log prob of sentence so far: -21.444415095160686
DUTCH: P(e|br) = -0.5733594216007992 ==> log prob of sentence so far: -31.569058249569483
PORTUGUESE: P(e|br) = -0.5219357444789137 ==> log prob of sentence so far: -33.29156455665301
SPANISH: P(e|br) = -0.2861736196721143 ==> log prob of sentence so far: -20.006956168779674

3GRAM: res
ENGLISH: P(s|re) = -0.8764823074851932 ==> log prob of sentence so far: -17.286011572418104
FRENCH: P(s|re) = -0.6175517891960948 ==> log prob of sentence so far: -22.06196688435678
DUTCH: P(s|re) = -1.9299045396772672 ==> log prob of sentence so far: -33.498962789246754
PORTUGUESE: P(s|re) = -0.603780349096915 ==> log prob of sentence so far: -33.89534490574992
SPANISH: P(s|re) = -0.6496047969774809 ==> log prob of sentence so far: -20.656560965757155

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: they
ENGLISH: P(y|the) = -1.4142415193870006 ==> log prob of sentence so far: -1.4142415193870006
FRENCH: P(y|the) = -6.170269344825598 ==> log prob of sentence so far: -6.170269344825598
DUTCH: P(y|the) = -6.5538861807264235 ==> log prob of sentence so far: -6.5538861807264235
PORTUGUESE: P(y|the) = -5.568232240985412 ==> log prob of sentence so far: -5.568232240985412
SPANISH: P(y|the) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874

4GRAM: heya
ENGLISH: P(a|hey) = -0.8294642265444696 ==> log prob of sentence so far: -2.24370574593147
FRENCH: P(a|hey) = -1.4313637641589874 ==> log prob of sentence so far: -7.6016331089845846
DUTCH: P(a|hey) = -1.4313637641589874 ==> log prob of sentence so far: -7.98524994488541
PORTUGUESE: P(a|hey) = -1.4313637641589874 ==> log prob of sentence so far: -6.999596005144399
SPANISH: P(a|hey) = -5.114030202518486 ==> log prob of sentence so far: -6.545393966677473

4GRAM: eyar
ENGLISH: P(r|eya) = -0.1624549388563038 ==> log prob of sentence so far: -2.406160684787774
FRENCH: P(r|eya) = -4.778339403895484 ==> log prob of sentence so far: -12.37997251288007
DUTCH: P(r|eya) = -1.4313637641589874 ==> log prob of sentence so far: -9.416613709044398
PORTUGUESE: P(r|eya) = -1.4313637641589874 ==> log prob of sentence so far: -8.430959769303387
SPANISH: P(r|eya) = -1.6989491597850694 ==> log prob of sentence so far: -8.244343126462542

4GRAM: yare
ENGLISH: P(e|yar) = -0.40011114526401576 ==> log prob of sentence so far: -2.8062718300517897
FRENCH: P(e|yar) = -4.845259319443209 ==> log prob of sentence so far: -17.225231832323278
DUTCH: P(e|yar) = -1.4313637641589874 ==> log prob of sentence so far: -10.847977473203386
PORTUGUESE: P(e|yar) = -1.4313637641589874 ==> log prob of sentence so far: -9.862323533462375
SPANISH: P(e|yar) = -0.5229279617124676 ==> log prob of sentence so far: -8.76727108817501

4GRAM: areb
ENGLISH: P(b|are) = -1.4691904753062741 ==> log prob of sentence so far: -4.275462305358063
FRENCH: P(b|are) = -2.728312465329243 ==> log prob of sentence so far: -19.95354429765252
DUTCH: P(b|are) = -2.068180232060217 ==> log prob of sentence so far: -12.916157705263604
PORTUGUESE: P(b|are) = -6.669319298472985 ==> log prob of sentence so far: -16.531642831935358
SPANISH: P(b|are) = -2.7972259144522775 ==> log prob of sentence so far: -11.564497002627288

4GRAM: reba
ENGLISH: P(a|reb) = -1.7143254336335159 ==> log prob of sentence so far: -5.989787738991579
FRENCH: P(a|reb) = -0.7439517122566941 ==> log prob of sentence so far: -20.697496009909216
DUTCH: P(a|reb) = -5.255335231998303 ==> log prob of sentence so far: -18.171492937261906
PORTUGUESE: P(a|reb) = -0.7782236231477598 ==> log prob of sentence so far: -17.309866455083117
SPANISH: P(a|reb) = -0.4210402996283407 ==> log prob of sentence so far: -11.98553730225563

4GRAM: ebad
ENGLISH: P(d|eba) = -1.7609168990877184 ==> log prob of sentence so far: -7.750704638079297
FRENCH: P(d|eba) = -6.43457305535867 ==> log prob of sentence so far: -27.132069065267885
DUTCH: P(d|eba) = -5.886505389413904 ==> log prob of sentence so far: -24.05799832667581
PORTUGUESE: P(d|eba) = -5.740383219293597 ==> log prob of sentence so far: -23.050249674376715
SPANISH: P(d|eba) = -1.3053538633578967 ==> log prob of sentence so far: -13.290891165613527

4GRAM: badh
ENGLISH: P(h|bad) = -5.591093559026904 ==> log prob of sentence so far: -13.3417981971062
FRENCH: P(h|bad) = -4.301594211829356 ==> log prob of sentence so far: -31.433663277097242
DUTCH: P(h|bad) = -5.447198356814742 ==> log prob of sentence so far: -29.505196683490553
PORTUGUESE: P(h|bad) = -5.114030202518486 ==> log prob of sentence so far: -28.1642798768952
SPANISH: P(h|bad) = -5.934511580878271 ==> log prob of sentence so far: -19.2254027464918

4GRAM: adho
ENGLISH: P(o|adh) = -0.914999643551469 ==> log prob of sentence so far: -14.25679784065767
FRENCH: P(o|adh) = -1.1761231029657007 ==> log prob of sentence so far: -32.60978638006294
DUTCH: P(o|adh) = -1.3979408772167057 ==> log prob of sentence so far: -30.903137560707258
PORTUGUESE: P(o|adh) = -4.001127700277035 ==> log prob of sentence so far: -32.16540757717223
SPANISH: P(o|adh) = -0.7782959766186218 ==> log prob of sentence so far: -20.00369872311042

4GRAM: dhom
ENGLISH: P(m|dho) = -1.3064265690060113 ==> log prob of sentence so far: -15.563224409663682
FRENCH: P(m|dho) = -0.3153039626627766 ==> log prob of sentence so far: -32.92509034272572
DUTCH: P(m|dho) = -5.602088219551879 ==> log prob of sentence so far: -36.50522578025914
PORTUGUESE: P(m|dho) = -1.4313637641589874 ==> log prob of sentence so far: -33.59677134133122
SPANISH: P(m|dho) = -4.001127700277035 ==> log prob of sentence so far: -24.004826423387456

4GRAM: homb
ENGLISH: P(b|hom) = -6.190338983048889 ==> log prob of sentence so far: -21.75356339271257
FRENCH: P(b|hom) = -2.086342744196933 ==> log prob of sentence so far: -35.01143308692265
DUTCH: P(b|hom) = -5.000112901888685 ==> log prob of sentence so far: -41.50533868214782
PORTUGUESE: P(b|hom) = -1.0000077206363769 ==> log prob of sentence so far: -34.596779061967595
SPANISH: P(b|hom) = -0.06733770964253827 ==> log prob of sentence so far: -24.072164133029993

4GRAM: ombr
ENGLISH: P(r|omb) = -5.83886545514855 ==> log prob of sentence so far: -27.59242884786112
FRENCH: P(r|omb) = -0.2589940012790687 ==> log prob of sentence so far: -35.27042708820172
DUTCH: P(r|omb) = -1.1139578280657803 ==> log prob of sentence so far: -42.6192965102136
PORTUGUESE: P(r|omb) = -0.23890235545052163 ==> log prob of sentence so far: -34.835681417418115
SPANISH: P(r|omb) = -0.0689116535739085 ==> log prob of sentence so far: -24.141075786603903

4GRAM: mbre
ENGLISH: P(e|mbr) = -0.8062065731743281 ==> log prob of sentence so far: -28.39863542103545
FRENCH: P(e|mbr) = -0.07693450831282819 ==> log prob of sentence so far: -35.34736159651455
DUTCH: P(e|mbr) = -0.699061192043459 ==> log prob of sentence so far: -43.31835770225706
PORTUGUESE: P(e|mbr) = -0.8776950632965218 ==> log prob of sentence so far: -35.713376480714636
SPANISH: P(e|mbr) = -0.11379317845023415 ==> log prob of sentence so far: -24.254868965054136

4GRAM: bres
ENGLISH: P(s|bre) = -1.36684774282085 ==> log prob of sentence so far: -29.765483163856302
FRENCH: P(s|bre) = -0.49775900586717164 ==> log prob of sentence so far: -35.845120602381726
DUTCH: P(s|bre) = -6.110598463433155 ==> log prob of sentence so far: -49.42895616569022
PORTUGUESE: P(s|bre) = -0.8868284391111814 ==> log prob of sentence so far: -36.60020491982582
SPANISH: P(s|bre) = -0.6738614842632622 ==> log prob of sentence so far: -24.9287304493174

According to the 4gram model, the sentence is in Spanish
----------------
