Ik hou van Mary.

1GRAM MODEL:

1GRAM: i
ENGLISH: P(i) = -1.1625228822481477 ==> log prob of sentence so far: -1.1625228822481477
FRENCH: P(i) = -1.1352025755293589 ==> log prob of sentence so far: -1.1352025755293589
DUTCH: P(i) = -1.2262442278424301 ==> log prob of sentence so far: -1.2262442278424301
PORTUGUESE: P(i) = -1.2396906121447568 ==> log prob of sentence so far: -1.2396906121447568
SPANISH: P(i) = -1.2231489528257042 ==> log prob of sentence so far: -1.2231489528257042

1GRAM: k
ENGLISH: P(k) = -2.072718058806105 ==> log prob of sentence so far: -3.2352409410542524
FRENCH: P(k) = -3.537664788203232 ==> log prob of sentence so far: -4.672867363732591
DUTCH: P(k) = -1.6325455853585675 ==> log prob of sentence so far: -2.858789813200998
PORTUGUESE: P(k) = -4.7523641544701585 ==> log prob of sentence so far: -5.992054766614915
SPANISH: P(k) = -3.8236062123431274 ==> log prob of sentence so far: -5.046755165168832

1GRAM: h
ENGLISH: P(h) = -1.1802717018481115 ==> log prob of sentence so far: -4.415512642902364
FRENCH: P(h) = -2.105661505277561 ==> log prob of sentence so far: -6.778528869010152
DUTCH: P(h) = -1.4907882132713295 ==> log prob of sentence so far: -4.349578026472328
PORTUGUESE: P(h) = -1.8092516224324593 ==> log prob of sentence so far: -7.801306389047374
SPANISH: P(h) = -1.8922415939752475 ==> log prob of sentence so far: -6.938996759144079

1GRAM: o
ENGLISH: P(o) = -1.1377809800580025 ==> log prob of sentence so far: -5.553293622960366
FRENCH: P(o) = -1.2743410778784847 ==> log prob of sentence so far: -8.052869946888638
DUTCH: P(o) = -1.199814333953728 ==> log prob of sentence so far: -5.549392360426055
PORTUGUESE: P(o) = -0.9821306993687576 ==> log prob of sentence so far: -8.783437088416132
SPANISH: P(o) = -0.9966256638806575 ==> log prob of sentence so far: -7.9356224230247365

1GRAM: u
ENGLISH: P(u) = -1.5524903643928334 ==> log prob of sentence so far: -7.105783987353199
FRENCH: P(u) = -1.2061319526021832 ==> log prob of sentence so far: -9.259001899490821
DUTCH: P(u) = -1.7453584472298034 ==> log prob of sentence so far: -7.294750807655859
PORTUGUESE: P(u) = -1.314188784008179 ==> log prob of sentence so far: -10.097625872424311
SPANISH: P(u) = -1.3656700797443349 ==> log prob of sentence so far: -9.301292502769071

1GRAM: v
ENGLISH: P(v) = -2.0436385119314493 ==> log prob of sentence so far: -9.149422499284649
FRENCH: P(v) = -1.827855207460022 ==> log prob of sentence so far: -11.086857106950843
DUTCH: P(v) = -1.610005447225762 ==> log prob of sentence so far: -8.90475625488162
PORTUGUESE: P(v) = -1.7546610529634015 ==> log prob of sentence so far: -11.852286925387713
SPANISH: P(v) = -1.9461309332760082 ==> log prob of sentence so far: -11.247423436045079

1GRAM: a
ENGLISH: P(a) = -1.0867855556195258 ==> log prob of sentence so far: -10.236208054904175
FRENCH: P(a) = -1.0763486860884999 ==> log prob of sentence so far: -12.163205793039342
DUTCH: P(a) = -1.1153867027045747 ==> log prob of sentence so far: -10.020142957586195
PORTUGUESE: P(a) = -0.8318864457343189 ==> log prob of sentence so far: -12.684173371122032
SPANISH: P(a) = -0.9020151018641022 ==> log prob of sentence so far: -12.14943853790918

1GRAM: n
ENGLISH: P(n) = -1.161594332338253 ==> log prob of sentence so far: -11.397802387242429
FRENCH: P(n) = -1.1226334875045167 ==> log prob of sentence so far: -13.28583928054386
DUTCH: P(n) = -0.9654176577239543 ==> log prob of sentence so far: -10.985560615310149
PORTUGUESE: P(n) = -1.3242807280507187 ==> log prob of sentence so far: -14.00845409917275
SPANISH: P(n) = -1.1498323276049258 ==> log prob of sentence so far: -13.299270865514107

1GRAM: m
ENGLISH: P(m) = -1.6111138474359583 ==> log prob of sentence so far: -13.008916234678388
FRENCH: P(m) = -1.5129506660683787 ==> log prob of sentence so far: -14.798789946612239
DUTCH: P(m) = -1.6188728802133567 ==> log prob of sentence so far: -12.604433495523505
PORTUGUESE: P(m) = -1.3143867575166186 ==> log prob of sentence so far: -15.32284085668937
SPANISH: P(m) = -1.4970849446101568 ==> log prob of sentence so far: -14.796355810124263

1GRAM: a
ENGLISH: P(a) = -1.0867855556195258 ==> log prob of sentence so far: -14.095701790297914
FRENCH: P(a) = -1.0763486860884999 ==> log prob of sentence so far: -15.875138632700738
DUTCH: P(a) = -1.1153867027045747 ==> log prob of sentence so far: -13.71982019822808
PORTUGUESE: P(a) = -0.8318864457343189 ==> log prob of sentence so far: -16.154727302423687
SPANISH: P(a) = -0.9020151018641022 ==> log prob of sentence so far: -15.698370911988365

1GRAM: r
ENGLISH: P(r) = -1.2612711446511347 ==> log prob of sentence so far: -15.356972934949049
FRENCH: P(r) = -1.1840252806546812 ==> log prob of sentence so far: -17.05916391335542
DUTCH: P(r) = -1.254198637640079 ==> log prob of sentence so far: -14.974018835868158
PORTUGUESE: P(r) = -1.194537098985067 ==> log prob of sentence so far: -17.349264401408753
SPANISH: P(r) = -1.191039284734038 ==> log prob of sentence so far: -16.8894101967224

1GRAM: y
ENGLISH: P(y) = -1.7506034466063691 ==> log prob of sentence so far: -17.10757638155542
FRENCH: P(y) = -2.672363548703873 ==> log prob of sentence so far: -19.731527462059294
DUTCH: P(y) = -3.5708485069501386 ==> log prob of sentence so far: -18.544867342818296
PORTUGUESE: P(y) = -3.3458344054838407 ==> log prob of sentence so far: -20.695098806892595
SPANISH: P(y) = -1.910660668875661 ==> log prob of sentence so far: -18.800070865598062

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: ik
ENGLISH: P(k|i) = -1.8747970415603463 ==> log prob of sentence so far: -1.8747970415603463
FRENCH: P(k|i) = -3.4033757825581823 ==> log prob of sentence so far: -3.4033757825581823
DUTCH: P(k|i) = -1.6459398156174567 ==> log prob of sentence so far: -1.6459398156174567
PORTUGUESE: P(k|i) = -8.11226985561152 ==> log prob of sentence so far: -8.11226985561152
SPANISH: P(k|i) = -4.338154057956912 ==> log prob of sentence so far: -4.338154057956912

2GRAM: kh
ENGLISH: P(h|k) = -1.734510757705665 ==> log prob of sentence so far: -3.6093077992660114
FRENCH: P(h|k) = -1.817336226967991 ==> log prob of sentence so far: -5.220712009526173
DUTCH: P(h|k) = -1.5632677673260886 ==> log prob of sentence so far: -3.209207582943545
PORTUGUESE: P(h|k) = -4.477497480263115 ==> log prob of sentence so far: -12.589767335874637
SPANISH: P(h|k) = -1.3891674137689312 ==> log prob of sentence so far: -5.727321471725842

2GRAM: ho
ENGLISH: P(o|h) = -1.1036237117294738 ==> log prob of sentence so far: -4.712931510995485
FRENCH: P(o|h) = -0.8324131354765649 ==> log prob of sentence so far: -6.053125145002738
DUTCH: P(o|h) = -1.0173147475160884 ==> log prob of sentence so far: -4.226522330459634
PORTUGUESE: P(o|h) = -0.5397555324379388 ==> log prob of sentence so far: -13.129522868312575
SPANISH: P(o|h) = -0.53265146317386 ==> log prob of sentence so far: -6.259972934899702

2GRAM: ou
ENGLISH: P(u|o) = -0.9045829510238154 ==> log prob of sentence so far: -5.617514462019301
FRENCH: P(u|o) = -0.6276609633897804 ==> log prob of sentence so far: -6.680786108392518
DUTCH: P(u|o) = -1.4239977536282045 ==> log prob of sentence so far: -5.650520084087838
PORTUGUESE: P(u|o) = -1.1236264091695107 ==> log prob of sentence so far: -14.253149277482086
SPANISH: P(u|o) = -2.0074171167989965 ==> log prob of sentence so far: -8.267390051698698

2GRAM: uv
ENGLISH: P(v|u) = -2.9802454151230124 ==> log prob of sentence so far: -8.597759877142312
FRENCH: P(v|u) = -1.5222708373996567 ==> log prob of sentence so far: -8.203056945792175
DUTCH: P(v|u) = -2.1870496987836057 ==> log prob of sentence so far: -7.837569782871444
PORTUGUESE: P(v|u) = -1.7528323771669048 ==> log prob of sentence so far: -16.00598165464899
SPANISH: P(v|u) = -2.1426548924074313 ==> log prob of sentence so far: -10.41004494410613

2GRAM: va
ENGLISH: P(a|v) = -1.0602469062838338 ==> log prob of sentence so far: -9.658006783426146
FRENCH: P(a|v) = -0.6556450405520147 ==> log prob of sentence so far: -8.85870198634419
DUTCH: P(a|v) = -0.5208282646147931 ==> log prob of sentence so far: -8.358398047486238
PORTUGUESE: P(a|v) = -0.5476382301362861 ==> log prob of sentence so far: -16.553619884785277
SPANISH: P(a|v) = -0.7119542644058031 ==> log prob of sentence so far: -11.121999208511932

2GRAM: an
ENGLISH: P(n|a) = -0.7044700836842804 ==> log prob of sentence so far: -10.362476867110425
FRENCH: P(n|a) = -0.8167432857801561 ==> log prob of sentence so far: -9.675445272124346
DUTCH: P(n|a) = -0.59086676987553 ==> log prob of sentence so far: -8.949264817361769
PORTUGUESE: P(n|a) = -1.130326966929377 ==> log prob of sentence so far: -17.683946851714655
SPANISH: P(n|a) = -0.8950658151252144 ==> log prob of sentence so far: -12.017065023637146

2GRAM: nm
ENGLISH: P(m|n) = -2.108620200858114 ==> log prob of sentence so far: -12.47109706796854
FRENCH: P(m|n) = -1.986621293096669 ==> log prob of sentence so far: -11.662066565221014
DUTCH: P(m|n) = -1.5290947394323398 ==> log prob of sentence so far: -10.478359556794109
PORTUGUESE: P(m|n) = -4.03007043078299 ==> log prob of sentence so far: -21.714017282497643
SPANISH: P(m|n) = -1.635762498281905 ==> log prob of sentence so far: -13.65282752191905

2GRAM: ma
ENGLISH: P(a|m) = -0.7036307543565469 ==> log prob of sentence so far: -13.174727822325087
FRENCH: P(a|m) = -0.7370260573471089 ==> log prob of sentence so far: -12.399092622568123
DUTCH: P(a|m) = -0.7531509265724662 ==> log prob of sentence so far: -11.231510483366575
PORTUGUESE: P(a|m) = -0.5413410978952592 ==> log prob of sentence so far: -22.255358380392902
SPANISH: P(a|m) = -0.6659743680789785 ==> log prob of sentence so far: -14.31880188999803

2GRAM: ar
ENGLISH: P(r|a) = -0.985420063231861 ==> log prob of sentence so far: -14.160147885556949
FRENCH: P(r|a) = -1.0331352651346224 ==> log prob of sentence so far: -13.432227887702744
DUTCH: P(r|a) = -0.9127486552516607 ==> log prob of sentence so far: -12.144259138618237
PORTUGUESE: P(r|a) = -1.010573902122766 ==> log prob of sentence so far: -23.265932282515667
SPANISH: P(r|a) = -0.9443454432936655 ==> log prob of sentence so far: -15.263147333291695

2GRAM: ry
ENGLISH: P(y|r) = -1.4566458898911945 ==> log prob of sentence so far: -15.616793775448144
FRENCH: P(y|r) = -3.148273473230357 ==> log prob of sentence so far: -16.580501360933102
DUTCH: P(y|r) = -4.351714289836427 ==> log prob of sentence so far: -16.495973428454665
PORTUGUESE: P(y|r) = -3.303405946647681 ==> log prob of sentence so far: -26.56933822916335
SPANISH: P(y|r) = -2.1999727122925403 ==> log prob of sentence so far: -17.463120045584233

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: ikh
ENGLISH: P(h|ik) = -6.943000880896234 ==> log prob of sentence so far: -6.943000880896234
FRENCH: P(h|ik) = -5.301086450277158 ==> log prob of sentence so far: -5.301086450277158
DUTCH: P(h|ik) = -1.3883597195986874 ==> log prob of sentence so far: -1.3883597195986874
PORTUGUESE: P(h|ik) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
SPANISH: P(h|ik) = -4.001127700277035 ==> log prob of sentence so far: -4.001127700277035

3GRAM: kho
ENGLISH: P(o|kh) = -1.158365990529501 ==> log prob of sentence so far: -8.101366871425736
FRENCH: P(o|kh) = -4.477497480263115 ==> log prob of sentence so far: -9.778583930540274
DUTCH: P(o|kh) = -1.329059510939555 ==> log prob of sentence so far: -2.7174192305382423
PORTUGUESE: P(o|kh) = -1.4313637641589874 ==> log prob of sentence so far: -2.8627275283179747
SPANISH: P(o|kh) = -4.301594211829356 ==> log prob of sentence so far: -8.302721912106392

3GRAM: hou
ENGLISH: P(u|ho) = -0.5108459265074261 ==> log prob of sentence so far: -8.61221279793316
FRENCH: P(u|ho) = -1.0896341677475894 ==> log prob of sentence so far: -10.868218098287862
DUTCH: P(u|ho) = -0.7687103803295071 ==> log prob of sentence so far: -3.4861296108677493
PORTUGUESE: P(u|ho) = -1.0396152791996445 ==> log prob of sentence so far: -3.902342807517619
SPANISH: P(u|ho) = -2.129041555868689 ==> log prob of sentence so far: -10.431763467975081

3GRAM: ouv
ENGLISH: P(v|ou) = -3.3284198492675805 ==> log prob of sentence so far: -11.940632647200742
FRENCH: P(v|ou) = -0.9553959665420321 ==> log prob of sentence so far: -11.823614064829894
DUTCH: P(v|ou) = -1.5838780387632991 ==> log prob of sentence so far: -5.070007649631048
PORTUGUESE: P(v|ou) = -1.1253874966029767 ==> log prob of sentence so far: -5.027730304120595
SPANISH: P(v|ou) = -6.539079362265061 ==> log prob of sentence so far: -16.97084283024014

3GRAM: uva
ENGLISH: P(a|uv) = -5.447198356814742 ==> log prob of sentence so far: -17.387831004015485
FRENCH: P(a|uv) = -0.5947391189938731 ==> log prob of sentence so far: -12.418353183823767
DUTCH: P(a|uv) = -1.2041290300618075 ==> log prob of sentence so far: -6.274136679692855
PORTUGUESE: P(a|uv) = -0.8660642607385497 ==> log prob of sentence so far: -5.893794564859145
SPANISH: P(a|uv) = -1.4510175874305649 ==> log prob of sentence so far: -18.421860417670707

3GRAM: van
ENGLISH: P(n|va) = -0.7970377341795746 ==> log prob of sentence so far: -18.18486873819506
FRENCH: P(n|va) = -0.6902728827534292 ==> log prob of sentence so far: -13.108626066577196
DUTCH: P(n|va) = -0.06720784495573275 ==> log prob of sentence so far: -6.341344524648588
PORTUGUESE: P(n|va) = -1.202903310588614 ==> log prob of sentence so far: -7.096697875447759
SPANISH: P(n|va) = -0.6677287341664176 ==> log prob of sentence so far: -19.089589151837124

3GRAM: anm
ENGLISH: P(m|an) = -2.4703693543115826 ==> log prob of sentence so far: -20.655238092506643
FRENCH: P(m|an) = -2.9043182087386907 ==> log prob of sentence so far: -16.01294427531589
DUTCH: P(m|an) = -1.6814658456145783 ==> log prob of sentence so far: -8.022810370263166
PORTUGUESE: P(m|an) = -7.372728419548177 ==> log prob of sentence so far: -14.469426294995937
SPANISH: P(m|an) = -1.9482862404083898 ==> log prob of sentence so far: -21.037875392245514

3GRAM: nma
ENGLISH: P(a|nm) = -0.44063433986481343 ==> log prob of sentence so far: -21.095872432371458
FRENCH: P(a|nm) = -0.5169790254977897 ==> log prob of sentence so far: -16.529923300813678
DUTCH: P(a|nm) = -0.532321092818032 ==> log prob of sentence so far: -8.555131463081198
PORTUGUESE: P(a|nm) = -4.001127700277035 ==> log prob of sentence so far: -18.470553995272972
SPANISH: P(a|nm) = -0.7133359527962763 ==> log prob of sentence so far: -21.75121134504179

3GRAM: mar
ENGLISH: P(r|ma) = -1.1127998434236388 ==> log prob of sentence so far: -22.208672275795095
FRENCH: P(r|ma) = -0.8404367288753473 ==> log prob of sentence so far: -17.370360029689024
DUTCH: P(r|ma) = -1.4195051507676555 ==> log prob of sentence so far: -9.974636613848853
PORTUGUESE: P(r|ma) = -0.7464228371683257 ==> log prob of sentence so far: -19.216976832441297
SPANISH: P(r|ma) = -0.8666469525178943 ==> log prob of sentence so far: -22.617858297559682

3GRAM: ary
ENGLISH: P(y|ar) = -1.5220784307256545 ==> log prob of sentence so far: -23.730750706520748
FRENCH: P(y|ar) = -3.7304968191993972 ==> log prob of sentence so far: -21.100856848888423
DUTCH: P(y|ar) = -7.572523400007856 ==> log prob of sentence so far: -17.547160013856708
PORTUGUESE: P(y|ar) = -7.467608490307706 ==> log prob of sentence so far: -26.684585322749
SPANISH: P(y|ar) = -2.1609262697286957 ==> log prob of sentence so far: -24.778784567288376

According to the 3gram model, the sentence is in Dutch
----------------
4GRAM MODEL:

4GRAM: ikho
ENGLISH: P(o|ikh) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
FRENCH: P(o|ikh) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
DUTCH: P(o|ikh) = -1.0414222936558335 ==> log prob of sentence so far: -1.0414222936558335
PORTUGUESE: P(o|ikh) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
SPANISH: P(o|ikh) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874

4GRAM: khou
ENGLISH: P(u|kho) = -5.000112901888685 ==> log prob of sentence so far: -6.431476666047672
FRENCH: P(u|kho) = -1.4313637641589874 ==> log prob of sentence so far: -2.8627275283179747
DUTCH: P(u|kho) = -0.6021396020318629 ==> log prob of sentence so far: -1.6435618956876965
PORTUGUESE: P(u|kho) = -1.4313637641589874 ==> log prob of sentence so far: -2.8627275283179747
SPANISH: P(u|kho) = -1.4313637641589874 ==> log prob of sentence so far: -2.8627275283179747

4GRAM: houv
ENGLISH: P(v|hou) = -7.1758023861269145 ==> log prob of sentence so far: -13.607279052174587
FRENCH: P(v|hou) = -5.812930728074705 ==> log prob of sentence so far: -8.67565825639268
DUTCH: P(v|hou) = -2.3364215100688486 ==> log prob of sentence so far: -3.979983405756545
PORTUGUESE: P(v|hou) = -0.5593191800720584 ==> log prob of sentence so far: -3.422046708390033
SPANISH: P(v|hou) = -5.000112901888685 ==> log prob of sentence so far: -7.86284043020666

4GRAM: ouva
ENGLISH: P(a|ouv) = -4.602342191036227 ==> log prob of sentence so far: -18.20962124321081
FRENCH: P(a|ouv) = -0.5481554989559713 ==> log prob of sentence so far: -9.223813755348651
DUTCH: P(a|ouv) = -1.3979417456727912 ==> log prob of sentence so far: -5.377925151429336
PORTUGUESE: P(a|ouv) = -1.7817429923185097 ==> log prob of sentence so far: -5.203789700708543
SPANISH: P(a|ouv) = -1.4313637641589874 ==> log prob of sentence so far: -9.294204194365648

4GRAM: uvan
ENGLISH: P(n|uva) = -1.4313637641589874 ==> log prob of sentence so far: -19.6409850073698
FRENCH: P(n|uva) = -1.0858339573967697 ==> log prob of sentence so far: -10.30964771274542
DUTCH: P(n|uva) = -3.617493019920266E-4 ==> log prob of sentence so far: -5.378286900731328
PORTUGUESE: P(n|uva) = -5.301086450277158 ==> log prob of sentence so far: -10.504876150985702
SPANISH: P(n|uva) = -0.6022987637593642 ==> log prob of sentence so far: -9.896502958125012

4GRAM: vanm
ENGLISH: P(m|van) = -2.0791472283826 ==> log prob of sentence so far: -21.7201322357524
FRENCH: P(m|van) = -6.6665204140947285 ==> log prob of sentence so far: -16.97616812684015
DUTCH: P(m|van) = -1.5462205214342017 ==> log prob of sentence so far: -6.924507422165529
PORTUGUESE: P(m|van) = -5.826091655592533 ==> log prob of sentence so far: -16.330967806578236
SPANISH: P(m|van) = -1.7455875016048021 ==> log prob of sentence so far: -11.642090459729815

4GRAM: anma
ENGLISH: P(a|anm) = -0.29261729770559114 ==> log prob of sentence so far: -22.01274953345799
FRENCH: P(a|anm) = -0.43942447589730466 ==> log prob of sentence so far: -17.415592602737455
DUTCH: P(a|anm) = -0.6627636042366006 ==> log prob of sentence so far: -7.58727102640213
PORTUGUESE: P(a|anm) = -1.4313637641589874 ==> log prob of sentence so far: -17.762331570737224
SPANISH: P(a|anm) = -0.5136540147583889 ==> log prob of sentence so far: -12.155744474488204

4GRAM: nmar
ENGLISH: P(r|nma) = -1.218680958875711 ==> log prob of sentence so far: -23.2314304923337
FRENCH: P(r|nma) = -0.7350718293798304 ==> log prob of sentence so far: -18.150664432117285
DUTCH: P(r|nma) = -1.414973347970818 ==> log prob of sentence so far: -9.002244374372948
PORTUGUESE: P(r|nma) = -1.4313637641589874 ==> log prob of sentence so far: -19.193695334896212
SPANISH: P(r|nma) = -0.7308670731869032 ==> log prob of sentence so far: -12.886611547675107

4GRAM: mary
ENGLISH: P(y|mar) = -1.1303353151190738 ==> log prob of sentence so far: -24.361765807452773
FRENCH: P(y|mar) = -6.749738324755967 ==> log prob of sentence so far: -24.90040275687325
DUTCH: P(y|mar) = -5.81956104377539 ==> log prob of sentence so far: -14.821805418148337
PORTUGUESE: P(y|mar) = -6.684847694625033 ==> log prob of sentence so far: -25.878543029521246
SPANISH: P(y|mar) = -2.041381630617385 ==> log prob of sentence so far: -14.927993178292493

According to the 4gram model, the sentence is in Dutch
----------------
