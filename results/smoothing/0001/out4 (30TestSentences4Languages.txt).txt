Birds build nests.

1GRAM MODEL:

1GRAM: b
ENGLISH: P(b) = -1.7519588996635036 ==> log prob of sentence so far: -1.7519588996635036
FRENCH: P(b) = -2.0519153276062747 ==> log prob of sentence so far: -2.0519153276062747
DUTCH: P(b) = -1.8074883611170944 ==> log prob of sentence so far: -1.8074883611170944
PORTUGUESE: P(b) = -2.0435293524217983 ==> log prob of sentence so far: -2.0435293524217983
SPANISH: P(b) = -1.8063459379660145 ==> log prob of sentence so far: -1.8063459379660145

1GRAM: i
ENGLISH: P(i) = -1.1625228822481477 ==> log prob of sentence so far: -2.9144817819116513
FRENCH: P(i) = -1.1352025755293589 ==> log prob of sentence so far: -3.1871179031356336
DUTCH: P(i) = -1.2262442278424301 ==> log prob of sentence so far: -3.0337325889595244
PORTUGUESE: P(i) = -1.2396906121447568 ==> log prob of sentence so far: -3.2832199645665554
SPANISH: P(i) = -1.2231489528257042 ==> log prob of sentence so far: -3.0294948907917187

1GRAM: r
ENGLISH: P(r) = -1.2612711446511347 ==> log prob of sentence so far: -4.175752926562786
FRENCH: P(r) = -1.1840252806546812 ==> log prob of sentence so far: -4.3711431837903145
DUTCH: P(r) = -1.254198637640079 ==> log prob of sentence so far: -4.2879312265996035
PORTUGUESE: P(r) = -1.194537098985067 ==> log prob of sentence so far: -4.477757063551622
SPANISH: P(r) = -1.191039284734038 ==> log prob of sentence so far: -4.220534175525756

1GRAM: d
ENGLISH: P(d) = -1.3961796679426524 ==> log prob of sentence so far: -5.571932594505438
FRENCH: P(d) = -1.4202617777502686 ==> log prob of sentence so far: -5.791404961540583
DUTCH: P(d) = -1.2011818240390888 ==> log prob of sentence so far: -5.489113050638692
PORTUGUESE: P(d) = -1.3354018349367311 ==> log prob of sentence so far: -5.8131588984883535
SPANISH: P(d) = -1.2907600057475275 ==> log prob of sentence so far: -5.511294181273284

1GRAM: s
ENGLISH: P(s) = -1.1711384807336087 ==> log prob of sentence so far: -6.743071075239047
FRENCH: P(s) = -1.064443204906708 ==> log prob of sentence so far: -6.8558481664472914
DUTCH: P(s) = -1.4604412026800553 ==> log prob of sentence so far: -6.949554253318747
PORTUGUESE: P(s) = -1.1128885199376846 ==> log prob of sentence so far: -6.926047418426038
SPANISH: P(s) = -1.1432550023265262 ==> log prob of sentence so far: -6.65454918359981

1GRAM: b
ENGLISH: P(b) = -1.7519588996635036 ==> log prob of sentence so far: -8.49502997490255
FRENCH: P(b) = -2.0519153276062747 ==> log prob of sentence so far: -8.907763494053565
DUTCH: P(b) = -1.8074883611170944 ==> log prob of sentence so far: -8.757042614435841
PORTUGUESE: P(b) = -2.0435293524217983 ==> log prob of sentence so far: -8.969576770847837
SPANISH: P(b) = -1.8063459379660145 ==> log prob of sentence so far: -8.460895121565825

1GRAM: u
ENGLISH: P(u) = -1.5524903643928334 ==> log prob of sentence so far: -10.047520339295383
FRENCH: P(u) = -1.2061319526021832 ==> log prob of sentence so far: -10.113895446655748
DUTCH: P(u) = -1.7453584472298034 ==> log prob of sentence so far: -10.502401061665644
PORTUGUESE: P(u) = -1.314188784008179 ==> log prob of sentence so far: -10.283765554856016
SPANISH: P(u) = -1.3656700797443349 ==> log prob of sentence so far: -9.82656520131016

1GRAM: i
ENGLISH: P(i) = -1.1625228822481477 ==> log prob of sentence so far: -11.21004322154353
FRENCH: P(i) = -1.1352025755293589 ==> log prob of sentence so far: -11.249098022185107
DUTCH: P(i) = -1.2262442278424301 ==> log prob of sentence so far: -11.728645289508075
PORTUGUESE: P(i) = -1.2396906121447568 ==> log prob of sentence so far: -11.523456167000774
SPANISH: P(i) = -1.2231489528257042 ==> log prob of sentence so far: -11.049714154135865

1GRAM: l
ENGLISH: P(l) = -1.3477680404759382 ==> log prob of sentence so far: -12.557811262019468
FRENCH: P(l) = -1.268210145276409 ==> log prob of sentence so far: -12.517308167461517
DUTCH: P(l) = -1.424888573378627 ==> log prob of sentence so far: -13.153533862886702
PORTUGUESE: P(l) = -1.4370345298164877 ==> log prob of sentence so far: -12.960490696817262
SPANISH: P(l) = -1.280216305870861 ==> log prob of sentence so far: -12.329930460006725

1GRAM: d
ENGLISH: P(d) = -1.3961796679426524 ==> log prob of sentence so far: -13.95399092996212
FRENCH: P(d) = -1.4202617777502686 ==> log prob of sentence so far: -13.937569945211784
DUTCH: P(d) = -1.2011818240390888 ==> log prob of sentence so far: -14.354715686925791
PORTUGUESE: P(d) = -1.3354018349367311 ==> log prob of sentence so far: -14.295892531753992
SPANISH: P(d) = -1.2907600057475275 ==> log prob of sentence so far: -13.620690465754253

1GRAM: n
ENGLISH: P(n) = -1.161594332338253 ==> log prob of sentence so far: -15.115585262300375
FRENCH: P(n) = -1.1226334875045167 ==> log prob of sentence so far: -15.060203432716301
DUTCH: P(n) = -0.9654176577239543 ==> log prob of sentence so far: -15.320133344649745
PORTUGUESE: P(n) = -1.3242807280507187 ==> log prob of sentence so far: -15.62017325980471
SPANISH: P(n) = -1.1498323276049258 ==> log prob of sentence so far: -14.770522793359179

1GRAM: e
ENGLISH: P(e) = -0.9100671672480583 ==> log prob of sentence so far: -16.025652429548433
FRENCH: P(e) = -0.7669595030100278 ==> log prob of sentence so far: -15.82716293572633
DUTCH: P(e) = -0.7296416717344202 ==> log prob of sentence so far: -16.049775016384167
PORTUGUESE: P(e) = -0.880437397131492 ==> log prob of sentence so far: -16.500610656936203
SPANISH: P(e) = -0.8816440718926534 ==> log prob of sentence so far: -15.652166865251832

1GRAM: s
ENGLISH: P(s) = -1.1711384807336087 ==> log prob of sentence so far: -17.196790910282044
FRENCH: P(s) = -1.064443204906708 ==> log prob of sentence so far: -16.891606140633037
DUTCH: P(s) = -1.4604412026800553 ==> log prob of sentence so far: -17.510216219064223
PORTUGUESE: P(s) = -1.1128885199376846 ==> log prob of sentence so far: -17.613499176873887
SPANISH: P(s) = -1.1432550023265262 ==> log prob of sentence so far: -16.79542186757836

1GRAM: t
ENGLISH: P(t) = -1.0342201967944733 ==> log prob of sentence so far: -18.231011107076515
FRENCH: P(t) = -1.1659631947814317 ==> log prob of sentence so far: -18.05756933541447
DUTCH: P(t) = -1.2498796929138756 ==> log prob of sentence so far: -18.760095911978098
PORTUGUESE: P(t) = -1.3798312063329417 ==> log prob of sentence so far: -18.99333038320683
SPANISH: P(t) = -1.403591206998156 ==> log prob of sentence so far: -18.199013074576513

1GRAM: s
ENGLISH: P(s) = -1.1711384807336087 ==> log prob of sentence so far: -19.402149587810126
FRENCH: P(s) = -1.064443204906708 ==> log prob of sentence so far: -19.12201254032118
DUTCH: P(s) = -1.4604412026800553 ==> log prob of sentence so far: -20.220537114658153
PORTUGUESE: P(s) = -1.1128885199376846 ==> log prob of sentence so far: -20.106218903144512
SPANISH: P(s) = -1.1432550023265262 ==> log prob of sentence so far: -19.34226807690304

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: bi
ENGLISH: P(i|b) = -1.4094982236216724 ==> log prob of sentence so far: -1.4094982236216724
FRENCH: P(i|b) = -0.8831499762805405 ==> log prob of sentence so far: -0.8831499762805405
DUTCH: P(i|b) = -0.971481529028593 ==> log prob of sentence so far: -0.971481529028593
PORTUGUESE: P(i|b) = -1.0753774818608943 ==> log prob of sentence so far: -1.0753774818608943
SPANISH: P(i|b) = -0.7881320918546834 ==> log prob of sentence so far: -0.7881320918546834

2GRAM: ir
ENGLISH: P(r|i) = -1.4088786466619074 ==> log prob of sentence so far: -2.81837687028358
FRENCH: P(r|i) = -1.2134667303092876 ==> log prob of sentence so far: -2.096616706589828
DUTCH: P(r|i) = -2.1935497297085504 ==> log prob of sentence so far: -3.1650312587371436
PORTUGUESE: P(r|i) = -1.1058037705746575 ==> log prob of sentence so far: -2.181181252435552
SPANISH: P(r|i) = -1.2824369823495172 ==> log prob of sentence so far: -2.0705690742042004

2GRAM: rd
ENGLISH: P(d|r) = -1.3854223448014262 ==> log prob of sentence so far: -4.203799215085006
FRENCH: P(d|r) = -1.3135760058227604 ==> log prob of sentence so far: -3.4101927124125884
DUTCH: P(d|r) = -0.8390733075578434 ==> log prob of sentence so far: -4.004104566294987
PORTUGUESE: P(d|r) = -1.4234155879550072 ==> log prob of sentence so far: -3.6045968403905593
SPANISH: P(d|r) = -1.317830662183974 ==> log prob of sentence so far: -3.388399736388174

2GRAM: ds
ENGLISH: P(s|d) = -1.0957662703914626 ==> log prob of sentence so far: -5.299565485476468
FRENCH: P(s|d) = -1.7451808669502606 ==> log prob of sentence so far: -5.155373579362849
DUTCH: P(s|d) = -1.780063484057608 ==> log prob of sentence so far: -5.784168050352594
PORTUGUESE: P(s|d) = -2.8722365488578028 ==> log prob of sentence so far: -6.4768333892483625
SPANISH: P(s|d) = -2.809070423587087 ==> log prob of sentence so far: -6.197470159975261

2GRAM: sb
ENGLISH: P(b|s) = -1.6901890740278063 ==> log prob of sentence so far: -6.9897545595042745
FRENCH: P(b|s) = -2.0221188760299156 ==> log prob of sentence so far: -7.177492455392764
DUTCH: P(b|s) = -1.8460731020560188 ==> log prob of sentence so far: -7.630241152408614
PORTUGUESE: P(b|s) = -2.0319660399924504 ==> log prob of sentence so far: -8.508799429240813
SPANISH: P(b|s) = -2.1005954801761355 ==> log prob of sentence so far: -8.298065640151396

2GRAM: bu
ENGLISH: P(u|b) = -0.801692476536701 ==> log prob of sentence so far: -7.791447036040975
FRENCH: P(u|b) = -1.71706353524589 ==> log prob of sentence so far: -8.894555990638654
DUTCH: P(u|b) = -0.9480670818610051 ==> log prob of sentence so far: -8.578308234269619
PORTUGUESE: P(u|b) = -1.5255756342968112 ==> log prob of sentence so far: -10.034375063537624
SPANISH: P(u|b) = -1.1426507293623787 ==> log prob of sentence so far: -9.440716369513774

2GRAM: ui
ENGLISH: P(i|u) = -1.6692502997536964 ==> log prob of sentence so far: -9.460697335794672
FRENCH: P(i|u) = -1.000736681615049 ==> log prob of sentence so far: -9.895292672253703
DUTCH: P(i|u) = -0.7018616974908801 ==> log prob of sentence so far: -9.280169931760499
PORTUGUESE: P(i|u) = -1.0653829765416187 ==> log prob of sentence so far: -11.099758040079243
SPANISH: P(i|u) = -1.1799549219531495 ==> log prob of sentence so far: -10.620671291466923

2GRAM: il
ENGLISH: P(l|i) = -1.2306471749559087 ==> log prob of sentence so far: -10.69134451075058
FRENCH: P(l|i) = -0.8981601157085628 ==> log prob of sentence so far: -10.793452787962266
DUTCH: P(l|i) = -1.4280285172080616 ==> log prob of sentence so far: -10.70819844896856
PORTUGUESE: P(l|i) = -1.5234380180857605 ==> log prob of sentence so far: -12.623196058165004
SPANISH: P(l|i) = -1.4534020646623096 ==> log prob of sentence so far: -12.074073356129233

2GRAM: ld
ENGLISH: P(d|l) = -1.2130509354517485 ==> log prob of sentence so far: -11.90439544620233
FRENCH: P(d|l) = -2.109551570504045 ==> log prob of sentence so far: -12.903004358466312
DUTCH: P(d|l) = -1.0107168340625885 ==> log prob of sentence so far: -11.718915283031148
PORTUGUESE: P(d|l) = -1.6455283830908023 ==> log prob of sentence so far: -14.268724441255806
SPANISH: P(d|l) = -1.4988298457317317 ==> log prob of sentence so far: -13.572903201860965

2GRAM: dn
ENGLISH: P(n|d) = -1.6483957799494247 ==> log prob of sentence so far: -13.552791226151754
FRENCH: P(n|d) = -2.4225662330074202 ==> log prob of sentence so far: -15.325570591473731
DUTCH: P(n|d) = -2.2541153038785953 ==> log prob of sentence so far: -13.973030586909744
PORTUGUESE: P(n|d) = -3.2402091980637135 ==> log prob of sentence so far: -17.50893363931952
SPANISH: P(n|d) = -2.8243103366594693 ==> log prob of sentence so far: -16.397213538520436

2GRAM: ne
ENGLISH: P(e|n) = -1.0532206253156708 ==> log prob of sentence so far: -14.606011851467425
FRENCH: P(e|n) = -0.7542226967076197 ==> log prob of sentence so far: -16.07979328818135
DUTCH: P(e|n) = -1.0854252599194454 ==> log prob of sentence so far: -15.058455846829188
PORTUGUESE: P(e|n) = -1.2165328027797635 ==> log prob of sentence so far: -18.725466442099282
SPANISH: P(e|n) = -1.0591609028952487 ==> log prob of sentence so far: -17.456374441415683

2GRAM: es
ENGLISH: P(s|e) = -0.9448348293174055 ==> log prob of sentence so far: -15.550846680784831
FRENCH: P(s|e) = -0.7261117562826892 ==> log prob of sentence so far: -16.805905044464037
DUTCH: P(s|e) = -1.4885357923574787 ==> log prob of sentence so far: -16.54699163918667
PORTUGUESE: P(s|e) = -0.8219917719022634 ==> log prob of sentence so far: -19.547458214001544
SPANISH: P(s|e) = -0.7772969067309046 ==> log prob of sentence so far: -18.233671348146586

2GRAM: st
ENGLISH: P(t|s) = -0.703615149506461 ==> log prob of sentence so far: -16.25446183029129
FRENCH: P(t|s) = -1.2372979453647666 ==> log prob of sentence so far: -18.043202989828803
DUTCH: P(t|s) = -0.5214462076050936 ==> log prob of sentence so far: -17.068437846791763
PORTUGUESE: P(t|s) = -0.9394210950183153 ==> log prob of sentence so far: -20.48687930901986
SPANISH: P(t|s) = -0.8813298145086538 ==> log prob of sentence so far: -19.11500116265524

2GRAM: ts
ENGLISH: P(s|t) = -1.3509262260280694 ==> log prob of sentence so far: -17.60538805631936
FRENCH: P(s|t) = -1.2204485268634018 ==> log prob of sentence so far: -19.263651516692203
DUTCH: P(s|t) = -1.336154869913733 ==> log prob of sentence so far: -18.404592716705498
PORTUGUESE: P(s|t) = -3.196353323914472 ==> log prob of sentence so far: -23.68323263293433
SPANISH: P(s|t) = -3.4612497451371644 ==> log prob of sentence so far: -22.576250907792403

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: bir
ENGLISH: P(r|bi) = -1.2041206446880766 ==> log prob of sentence so far: -1.2041206446880766
FRENCH: P(r|bi) = -2.0606930388790294 ==> log prob of sentence so far: -2.0606930388790294
DUTCH: P(r|bi) = -6.836325761717704 ==> log prob of sentence so far: -6.836325761717704
PORTUGUESE: P(r|bi) = -1.457376523209827 ==> log prob of sentence so far: -1.457376523209827
SPANISH: P(r|bi) = -1.4253478387135412 ==> log prob of sentence so far: -1.4253478387135412

3GRAM: ird
ENGLISH: P(d|ir) = -1.3640817933277172 ==> log prob of sentence so far: -2.5682024380157937
FRENCH: P(d|ir) = -1.3340829027623498 ==> log prob of sentence so far: -3.394775941641379
DUTCH: P(d|ir) = -6.195906844487599 ==> log prob of sentence so far: -13.032232606205302
PORTUGUESE: P(d|ir) = -1.5549595696847438 ==> log prob of sentence so far: -3.0123360928945706
SPANISH: P(d|ir) = -1.6108172465116954 ==> log prob of sentence so far: -3.0361650852252366

3GRAM: rds
ENGLISH: P(s|rd) = -0.7035294508980944 ==> log prob of sentence so far: -3.271731888913888
FRENCH: P(s|rd) = -1.2230010035250882 ==> log prob of sentence so far: -4.617776945166467
DUTCH: P(s|rd) = -2.013244029165754 ==> log prob of sentence so far: -15.045476635371056
PORTUGUESE: P(s|rd) = -2.4240449377200255 ==> log prob of sentence so far: -5.436381030614596
SPANISH: P(s|rd) = -7.04649617885792 ==> log prob of sentence so far: -10.082661264083157

3GRAM: dsb
ENGLISH: P(b|ds) = -1.7958022390269888 ==> log prob of sentence so far: -5.067534127940877
FRENCH: P(b|ds) = -2.0568964703435473 ==> log prob of sentence so far: -6.674673415510014
DUTCH: P(b|ds) = -1.7107491535900268 ==> log prob of sentence so far: -16.756225788961082
PORTUGUESE: P(b|ds) = -5.114030202518486 ==> log prob of sentence so far: -10.550411233133083
SPANISH: P(b|ds) = -5.462436932900274 ==> log prob of sentence so far: -15.545098196983432

3GRAM: sbu
ENGLISH: P(u|sb) = -0.5939174872664129 ==> log prob of sentence so far: -5.66145161520729
FRENCH: P(u|sb) = -1.5720958890261807 ==> log prob of sentence so far: -8.246769304536194
DUTCH: P(u|sb) = -1.5163134693644233 ==> log prob of sentence so far: -18.272539258325505
PORTUGUESE: P(u|sb) = -2.1818075893257767 ==> log prob of sentence so far: -12.73221882245886
SPANISH: P(u|sb) = -0.665389285006321 ==> log prob of sentence so far: -16.21048748198975

3GRAM: bui
ENGLISH: P(i|bu) = -2.2492774361163055 ==> log prob of sentence so far: -7.910729051323596
FRENCH: P(i|bu) = -1.0304949431292194 ==> log prob of sentence so far: -9.277264247665414
DUTCH: P(i|bu) = -1.1193766466955573 ==> log prob of sentence so far: -19.39191590502106
PORTUGUESE: P(i|bu) = -0.940244101351801 ==> log prob of sentence so far: -13.672462923810661
SPANISH: P(i|bu) = -2.3138482468725927 ==> log prob of sentence so far: -18.524335728862344

3GRAM: uil
ENGLISH: P(l|ui) = -1.0505855649286957 ==> log prob of sentence so far: -8.961314616252292
FRENCH: P(l|ui) = -0.7639372771459993 ==> log prob of sentence so far: -10.041201524811413
DUTCH: P(l|ui) = -1.7678966496044912 ==> log prob of sentence so far: -21.159812554625553
PORTUGUESE: P(l|ui) = -1.4476276975371638 ==> log prob of sentence so far: -15.120090621347824
SPANISH: P(l|ui) = -1.403977991508674 ==> log prob of sentence so far: -19.92831372037102

3GRAM: ild
ENGLISH: P(d|il) = -1.109079624261206 ==> log prob of sentence so far: -10.070394240513497
FRENCH: P(d|il) = -1.5999107763329516 ==> log prob of sentence so far: -11.641112301144364
DUTCH: P(d|il) = -0.5270266253162423 ==> log prob of sentence so far: -21.686839179941796
PORTUGUESE: P(d|il) = -1.466981246334113 ==> log prob of sentence so far: -16.58707186768194
SPANISH: P(d|il) = -1.5139117163006712 ==> log prob of sentence so far: -21.44222543667169

3GRAM: ldn
ENGLISH: P(n|ld) = -1.1851020318119172 ==> log prob of sentence so far: -11.255496272325415
FRENCH: P(n|ld) = -6.460901749886561 ==> log prob of sentence so far: -18.102014051030924
DUTCH: P(n|ld) = -2.2065220067642017 ==> log prob of sentence so far: -23.893361186706
PORTUGUESE: P(n|ld) = -1.4497348974511555 ==> log prob of sentence so far: -18.036806765133093
SPANISH: P(n|ld) = -6.775976222525004 ==> log prob of sentence so far: -28.218201659196694

3GRAM: dne
ENGLISH: P(e|dn) = -0.6432415063577275 ==> log prob of sentence so far: -11.898737778683142
FRENCH: P(e|dn) = -0.49049526467340526 ==> log prob of sentence so far: -18.59250931570433
DUTCH: P(e|dn) = -1.2399275080329886 ==> log prob of sentence so far: -25.133288694738987
PORTUGUESE: P(e|dn) = -0.7782959766186218 ==> log prob of sentence so far: -18.815102741751716
SPANISH: P(e|dn) = -1.146146646969516 ==> log prob of sentence so far: -29.36434830616621

3GRAM: nes
ENGLISH: P(s|ne) = -0.7262413463849692 ==> log prob of sentence so far: -12.62497912506811
FRENCH: P(s|ne) = -0.827577046179923 ==> log prob of sentence so far: -19.420086361884252
DUTCH: P(s|ne) = -2.1724236800034555 ==> log prob of sentence so far: -27.30571237474244
PORTUGUESE: P(s|ne) = -0.884163286425503 ==> log prob of sentence so far: -19.69926602817722
SPANISH: P(s|ne) = -0.5809749727067286 ==> log prob of sentence so far: -29.945323278872937

3GRAM: est
ENGLISH: P(t|es) = -0.7236055865133856 ==> log prob of sentence so far: -13.348584711581497
FRENCH: P(t|es) = -1.074125958649124 ==> log prob of sentence so far: -20.494212320533375
DUTCH: P(t|es) = -0.3788851366039736 ==> log prob of sentence so far: -27.684597511346414
PORTUGUESE: P(t|es) = -0.6333513548841391 ==> log prob of sentence so far: -20.332617383061358
SPANISH: P(t|es) = -0.6162774382277137 ==> log prob of sentence so far: -30.56160071710065

3GRAM: sts
ENGLISH: P(s|st) = -1.5081313109397916 ==> log prob of sentence so far: -14.856716022521288
FRENCH: P(s|st) = -1.9190773649371722 ==> log prob of sentence so far: -22.41328968547055
DUTCH: P(s|st) = -2.5674504669686535 ==> log prob of sentence so far: -30.25204797831507
PORTUGUESE: P(s|st) = -2.5754107057118842 ==> log prob of sentence so far: -22.908028088773243
SPANISH: P(s|st) = -7.518119289625776 ==> log prob of sentence so far: -38.079720006726426

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: bird
ENGLISH: P(d|bir) = -0.16565181408104415 ==> log prob of sentence so far: -0.16565181408104415
FRENCH: P(d|bir) = -4.845259319443209 ==> log prob of sentence so far: -4.845259319443209
DUTCH: P(d|bir) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
PORTUGUESE: P(d|bir) = -4.699195778770342 ==> log prob of sentence so far: -4.699195778770342
SPANISH: P(d|bir) = -5.531512126526995 ==> log prob of sentence so far: -5.531512126526995

4GRAM: irds
ENGLISH: P(s|ird) = -0.6734252373634899 ==> log prob of sentence so far: -0.839077051444534
FRENCH: P(s|ird) = -6.149227120848588 ==> log prob of sentence so far: -10.994486440291798
DUTCH: P(s|ird) = -1.4313637641589874 ==> log prob of sentence so far: -2.8627275283179747
PORTUGUESE: P(s|ird) = -5.431405583095624 ==> log prob of sentence so far: -10.130601361865967
SPANISH: P(s|ird) = -5.431405583095624 ==> log prob of sentence so far: -10.96291770962262

4GRAM: rdsb
ENGLISH: P(b|rds) = -1.721925182040974 ==> log prob of sentence so far: -2.561002233485508
FRENCH: P(b|rds) = -1.778138945814236 ==> log prob of sentence so far: -12.772625386106034
DUTCH: P(b|rds) = -5.491398117005353 ==> log prob of sentence so far: -8.354125645323329
PORTUGUESE: P(b|rds) = -4.001127700277035 ==> log prob of sentence so far: -14.131729062143002
SPANISH: P(b|rds) = -1.4313637641589874 ==> log prob of sentence so far: -12.394281473781607

4GRAM: dsbu
ENGLISH: P(u|dsb) = -0.7501421899815023 ==> log prob of sentence so far: -3.3111444234670104
FRENCH: P(u|dsb) = -4.602342191036227 ==> log prob of sentence so far: -17.37496757714226
DUTCH: P(u|dsb) = -4.903231109767353 ==> log prob of sentence so far: -13.257356755090683
PORTUGUESE: P(u|dsb) = -1.4313637641589874 ==> log prob of sentence so far: -15.56309282630199
SPANISH: P(u|dsb) = -1.4313637641589874 ==> log prob of sentence so far: -13.825645237940595

4GRAM: sbui
ENGLISH: P(i|sbu) = -6.509206018185927 ==> log prob of sentence so far: -9.820350441652938
FRENCH: P(i|sbu) = -0.33106228606573757 ==> log prob of sentence so far: -17.706029863208
DUTCH: P(i|sbu) = -0.07936071375668531 ==> log prob of sentence so far: -13.336717468847368
PORTUGUESE: P(i|sbu) = -4.001127700277035 ==> log prob of sentence so far: -19.564220526579025
SPANISH: P(i|sbu) = -5.633494714452069 ==> log prob of sentence so far: -19.459139952392665

4GRAM: buil
ENGLISH: P(l|bui) = -7.237589998703617E-5 ==> log prob of sentence so far: -9.820422817552926
FRENCH: P(l|bui) = -5.0414953244516 ==> log prob of sentence so far: -22.747525187659598
DUTCH: P(l|bui) = -5.740383219293597 ==> log prob of sentence so far: -19.077100688140966
PORTUGUESE: P(l|bui) = -4.845259319443209 ==> log prob of sentence so far: -24.409479846022233
SPANISH: P(l|bui) = -4.301594211829356 ==> log prob of sentence so far: -23.760734164222022

4GRAM: uild
ENGLISH: P(d|uil) = -0.862488071859763 ==> log prob of sentence so far: -10.682910889412689
FRENCH: P(d|uil) = -1.6896034439435939 ==> log prob of sentence so far: -24.43712863160319
DUTCH: P(d|uil) = -0.4948897573106154 ==> log prob of sentence so far: -19.571990445451583
PORTUGUESE: P(d|uil) = -5.5185481556710085 ==> log prob of sentence so far: -29.928028001693242
SPANISH: P(d|uil) = -5.602088219551879 ==> log prob of sentence so far: -29.362822383773903

4GRAM: ildn
ENGLISH: P(n|ild) = -1.5486950098515628 ==> log prob of sentence so far: -12.231605899264252
FRENCH: P(n|ild) = -6.195906844487599 ==> log prob of sentence so far: -30.63303547609079
DUTCH: P(n|ild) = -6.4329734575173445 ==> log prob of sentence so far: -26.004963902968928
PORTUGUESE: P(n|ild) = -5.114030202518486 ==> log prob of sentence so far: -35.04205820421173
SPANISH: P(n|ild) = -5.361776927401738 ==> log prob of sentence so far: -34.72459931117564

4GRAM: ldne
ENGLISH: P(e|ldn) = -0.8589795710496149 ==> log prob of sentence so far: -13.090585470313867
FRENCH: P(e|ldn) = -1.4313637641589874 ==> log prob of sentence so far: -32.06439924024978
DUTCH: P(e|ldn) = -4.903231109767353 ==> log prob of sentence so far: -30.908195012736282
PORTUGUESE: P(e|ldn) = -0.7782959766186218 ==> log prob of sentence so far: -35.82035418083035
SPANISH: P(e|ldn) = -1.4313637641589874 ==> log prob of sentence so far: -36.155963075334626

4GRAM: dnes
ENGLISH: P(s|dne) = -0.40028670834355223 ==> log prob of sentence so far: -13.490872178657419
FRENCH: P(s|dne) = -0.6600810191363014 ==> log prob of sentence so far: -32.72448025938608
DUTCH: P(s|dne) = -4.903231109767353 ==> log prob of sentence so far: -35.81142612250363
PORTUGUESE: P(s|dne) = -4.001127700277035 ==> log prob of sentence so far: -39.82148188110739
SPANISH: P(s|dne) = -4.301594211829356 ==> log prob of sentence so far: -40.45755728716398

4GRAM: nest
ENGLISH: P(t|nes) = -0.9133906988957421 ==> log prob of sentence so far: -14.404262877553162
FRENCH: P(t|nes) = -0.8871543761447211 ==> log prob of sentence so far: -33.6116346355308
DUTCH: P(t|nes) = -0.4586815117627002 ==> log prob of sentence so far: -36.27010763426633
PORTUGUESE: P(t|nes) = -0.27107924942897743 ==> log prob of sentence so far: -40.09256113053637
SPANISH: P(t|nes) = -0.5859151769402967 ==> log prob of sentence so far: -41.043472464104276

4GRAM: ests
ENGLISH: P(s|est) = -1.481920054878775 ==> log prob of sentence so far: -15.886182932431936
FRENCH: P(s|est) = -1.6472870924320424 ==> log prob of sentence so far: -35.25892172796284
DUTCH: P(s|est) = -2.6892882995069476 ==> log prob of sentence so far: -38.95939593377328
PORTUGUESE: P(s|est) = -6.976351171362411 ==> log prob of sentence so far: -47.06891230189878
SPANISH: P(s|est) = -7.259594499988724 ==> log prob of sentence so far: -48.303066964093

According to the 4gram model, the sentence is in English
----------------
