I hate AI.

1GRAM MODEL:

1GRAM: i
ENGLISH: P(i) = -1.1625228822481477 ==> log prob of sentence so far: -1.1625228822481477
FRENCH: P(i) = -1.1352025755293589 ==> log prob of sentence so far: -1.1352025755293589
DUTCH: P(i) = -1.2262442278424301 ==> log prob of sentence so far: -1.2262442278424301
PORTUGUESE: P(i) = -1.2396906121447568 ==> log prob of sentence so far: -1.2396906121447568
SPANISH: P(i) = -1.2231489528257042 ==> log prob of sentence so far: -1.2231489528257042

1GRAM: h
ENGLISH: P(h) = -1.1802717018481115 ==> log prob of sentence so far: -2.342794584096259
FRENCH: P(h) = -2.105661505277561 ==> log prob of sentence so far: -3.24086408080692
DUTCH: P(h) = -1.4907882132713295 ==> log prob of sentence so far: -2.7170324411137594
PORTUGUESE: P(h) = -1.8092516224324593 ==> log prob of sentence so far: -3.048942234577216
SPANISH: P(h) = -1.8922415939752475 ==> log prob of sentence so far: -3.1153905468009517

1GRAM: a
ENGLISH: P(a) = -1.0867855556195258 ==> log prob of sentence so far: -3.429580139715785
FRENCH: P(a) = -1.0763486860884999 ==> log prob of sentence so far: -4.31721276689542
DUTCH: P(a) = -1.1153867027045747 ==> log prob of sentence so far: -3.832419143818334
PORTUGUESE: P(a) = -0.8318864457343189 ==> log prob of sentence so far: -3.8808286803115353
SPANISH: P(a) = -0.9020151018641022 ==> log prob of sentence so far: -4.017405648665054

1GRAM: t
ENGLISH: P(t) = -1.0342201967944733 ==> log prob of sentence so far: -4.463800336510258
FRENCH: P(t) = -1.1659631947814317 ==> log prob of sentence so far: -5.483175961676851
DUTCH: P(t) = -1.2498796929138756 ==> log prob of sentence so far: -5.0822988367322095
PORTUGUESE: P(t) = -1.3798312063329417 ==> log prob of sentence so far: -5.260659886644477
SPANISH: P(t) = -1.403591206998156 ==> log prob of sentence so far: -5.42099685566321

1GRAM: e
ENGLISH: P(e) = -0.9100671672480583 ==> log prob of sentence so far: -5.373867503758316
FRENCH: P(e) = -0.7669595030100278 ==> log prob of sentence so far: -6.250135464686879
DUTCH: P(e) = -0.7296416717344202 ==> log prob of sentence so far: -5.81194050846663
PORTUGUESE: P(e) = -0.880437397131492 ==> log prob of sentence so far: -6.141097283775969
SPANISH: P(e) = -0.8816440718926534 ==> log prob of sentence so far: -6.3026409275558635

1GRAM: a
ENGLISH: P(a) = -1.0867855556195258 ==> log prob of sentence so far: -6.460653059377842
FRENCH: P(a) = -1.0763486860884999 ==> log prob of sentence so far: -7.3264841507753795
DUTCH: P(a) = -1.1153867027045747 ==> log prob of sentence so far: -6.927327211171204
PORTUGUESE: P(a) = -0.8318864457343189 ==> log prob of sentence so far: -6.972983729510288
SPANISH: P(a) = -0.9020151018641022 ==> log prob of sentence so far: -7.204656029419966

1GRAM: i
ENGLISH: P(i) = -1.1625228822481477 ==> log prob of sentence so far: -7.623175941625989
FRENCH: P(i) = -1.1352025755293589 ==> log prob of sentence so far: -8.461686726304738
DUTCH: P(i) = -1.2262442278424301 ==> log prob of sentence so far: -8.153571439013634
PORTUGUESE: P(i) = -1.2396906121447568 ==> log prob of sentence so far: -8.212674341655045
SPANISH: P(i) = -1.2231489528257042 ==> log prob of sentence so far: -8.42780498224567

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: ih
ENGLISH: P(h|i) = -2.463688053133791 ==> log prob of sentence so far: -2.463688053133791
FRENCH: P(h|i) = -3.257248367297974 ==> log prob of sentence so far: -3.257248367297974
DUTCH: P(h|i) = -3.1853269617504307 ==> log prob of sentence so far: -3.1853269617504307
PORTUGUESE: P(h|i) = -3.2091744399724824 ==> log prob of sentence so far: -3.2091744399724824
SPANISH: P(h|i) = -2.4631356427831514 ==> log prob of sentence so far: -2.4631356427831514

2GRAM: ha
ENGLISH: P(a|h) = -0.6935127499995246 ==> log prob of sentence so far: -3.157200803133316
FRENCH: P(a|h) = -0.5238394011778803 ==> log prob of sentence so far: -3.7810877684758544
DUTCH: P(a|h) = -0.8576400971532346 ==> log prob of sentence so far: -4.042967058903665
PORTUGUESE: P(a|h) = -0.4522154080917641 ==> log prob of sentence so far: -3.6613898480642466
SPANISH: P(a|h) = -0.34664319351750295 ==> log prob of sentence so far: -2.809778836300654

2GRAM: at
ENGLISH: P(t|a) = -0.8627002964232751 ==> log prob of sentence so far: -4.019901099556591
FRENCH: P(t|a) = -1.2479403766820554 ==> log prob of sentence so far: -5.02902814515791
DUTCH: P(t|a) = -1.109621860489706 ==> log prob of sentence so far: -5.152588919393371
PORTUGUESE: P(t|a) = -1.5397393675816842 ==> log prob of sentence so far: -5.201129215645931
SPANISH: P(t|a) = -1.5459743472234357 ==> log prob of sentence so far: -4.35575318352409

2GRAM: te
ENGLISH: P(e|t) = -1.0588721640199654 ==> log prob of sentence so far: -5.078773263576557
FRENCH: P(e|t) = -0.6740385697517565 ==> log prob of sentence so far: -5.703066714909666
DUTCH: P(e|t) = -0.5035716637055169 ==> log prob of sentence so far: -5.656160583098888
PORTUGUESE: P(e|t) = -0.5750111338550872 ==> log prob of sentence so far: -5.776140349501018
SPANISH: P(e|t) = -0.610612183505976 ==> log prob of sentence so far: -4.9663653670300665

2GRAM: ea
ENGLISH: P(a|e) = -1.0585327908587914 ==> log prob of sentence so far: -6.137306054435348
FRENCH: P(a|e) = -1.5190432916368146 ==> log prob of sentence so far: -7.222110006546481
DUTCH: P(a|e) = -2.1315290809356253 ==> log prob of sentence so far: -7.787689664034513
PORTUGUESE: P(a|e) = -1.281592048749021 ==> log prob of sentence so far: -7.057732398250039
SPANISH: P(a|e) = -1.4656543673665137 ==> log prob of sentence so far: -6.43201973439658

2GRAM: ai
ENGLISH: P(i|a) = -1.3482451351695797 ==> log prob of sentence so far: -7.485551189604928
FRENCH: P(i|a) = -0.696524586220923 ==> log prob of sentence so far: -7.918634592767404
DUTCH: P(i|a) = -2.256635077784397 ==> log prob of sentence so far: -10.04432474181891
PORTUGUESE: P(i|a) = -1.4740771803900676 ==> log prob of sentence so far: -8.531809578640107
SPANISH: P(i|a) = -1.9696512257535392 ==> log prob of sentence so far: -8.40167096015012

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: iha
ENGLISH: P(a|ih) = -0.1061399237010088 ==> log prob of sentence so far: -0.1061399237010088
FRENCH: P(a|ih) = -0.9700626258536191 ==> log prob of sentence so far: -0.9700626258536191
DUTCH: P(a|ih) = -0.5052118596370118 ==> log prob of sentence so far: -0.5052118596370118
PORTUGUESE: P(a|ih) = -0.3011602612130579 ==> log prob of sentence so far: -0.3011602612130579
SPANISH: P(a|ih) = -0.41267682320922966 ==> log prob of sentence so far: -0.41267682320922966

3GRAM: hat
ENGLISH: P(t|ha) = -0.49940134522269436 ==> log prob of sentence so far: -0.6055412689237032
FRENCH: P(t|ha) = -1.5480952843865214 ==> log prob of sentence so far: -2.5181579102401406
DUTCH: P(t|ha) = -1.7876957343205655 ==> log prob of sentence so far: -2.2929075939575774
PORTUGUESE: P(t|ha) = -1.8061785414208797 ==> log prob of sentence so far: -2.1073388026339375
SPANISH: P(t|ha) = -1.993539673621738 ==> log prob of sentence so far: -2.4062164968309676

3GRAM: ate
ENGLISH: P(e|at) = -0.8007955432295575 ==> log prob of sentence so far: -1.4063368121532607
FRENCH: P(e|at) = -0.6875770126209807 ==> log prob of sentence so far: -3.2057349228611214
DUTCH: P(e|at) = -0.7251486849261162 ==> log prob of sentence so far: -3.0180562788836935
PORTUGUESE: P(e|at) = -0.5922486929960163 ==> log prob of sentence so far: -2.6995874956299537
SPANISH: P(e|at) = -0.6706189998297142 ==> log prob of sentence so far: -3.076835496660682

3GRAM: tea
ENGLISH: P(a|te) = -1.438577238847763 ==> log prob of sentence so far: -2.8449140510010236
FRENCH: P(a|te) = -1.4473789137999828 ==> log prob of sentence so far: -4.653113836661104
DUTCH: P(a|te) = -2.132387079012415 ==> log prob of sentence so far: -5.150443357896108
PORTUGUESE: P(a|te) = -1.1507474682806074 ==> log prob of sentence so far: -3.8503349639105613
SPANISH: P(a|te) = -1.2063329159137826 ==> log prob of sentence so far: -4.283168412574464

3GRAM: eai
ENGLISH: P(i|ea) = -2.0001768989040967 ==> log prob of sentence so far: -4.845090949905121
FRENCH: P(i|ea) = -1.3770421112448266 ==> log prob of sentence so far: -6.03015594790593
DUTCH: P(i|ea) = -6.74741382785606 ==> log prob of sentence so far: -11.89785718575217
PORTUGUESE: P(i|ea) = -1.3418331261959722 ==> log prob of sentence so far: -5.1921680901065335
SPANISH: P(i|ea) = -2.1914473981600073 ==> log prob of sentence so far: -6.474615810734472

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: ihat
ENGLISH: P(t|iha) = -2.2479362185050555 ==> log prob of sentence so far: -2.2479362185050555
FRENCH: P(t|iha) = -4.477497480263115 ==> log prob of sentence so far: -4.477497480263115
DUTCH: P(t|iha) = -4.699195778770342 ==> log prob of sentence so far: -4.699195778770342
PORTUGUESE: P(t|iha) = -4.602342191036227 ==> log prob of sentence so far: -4.602342191036227
SPANISH: P(t|iha) = -5.462436932900274 ==> log prob of sentence so far: -5.462436932900274

4GRAM: hate
ENGLISH: P(e|hat) = -1.4558985170537928 ==> log prob of sentence so far: -3.703834735558848
FRENCH: P(e|hat) = -0.5488356850465186 ==> log prob of sentence so far: -5.026333165309634
DUTCH: P(e|hat) = -1.1761071820986115 ==> log prob of sentence so far: -5.875302960868954
PORTUGUESE: P(e|hat) = -0.6532643834440081 ==> log prob of sentence so far: -5.255606574480235
SPANISH: P(e|hat) = -0.4771688170217595 ==> log prob of sentence so far: -5.939605749922033

4GRAM: atea
ENGLISH: P(a|ate) = -1.4072376311208978 ==> log prob of sentence so far: -5.111072366679746
FRENCH: P(a|ate) = -0.9320323673373571 ==> log prob of sentence so far: -5.958365532646991
DUTCH: P(a|ate) = -2.6404405935889685 ==> log prob of sentence so far: -8.515743554457922
PORTUGUESE: P(a|ate) = -0.9126808307751055 ==> log prob of sentence so far: -6.168287405255341
SPANISH: P(a|ate) = -1.3222201564212843 ==> log prob of sentence so far: -7.261825906343318

4GRAM: teai
ENGLISH: P(i|tea) = -2.4345296280818074 ==> log prob of sentence so far: -7.545601994761554
FRENCH: P(i|tea) = -2.2442586236048454 ==> log prob of sentence so far: -8.202624156251836
DUTCH: P(i|tea) = -5.698992587061932 ==> log prob of sentence so far: -14.214736141519854
PORTUGUESE: P(i|tea) = -1.4419564016248234 ==> log prob of sentence so far: -7.610243806880164
SPANISH: P(i|tea) = -1.9934202480838878 ==> log prob of sentence so far: -9.255246154427205

According to the 4gram model, the sentence is in English
----------------
