What will the Japanese economy be like next year?

1GRAM MODEL:

1GRAM: w
ENGLISH: P(w) = -1.6313559002514257 ==> log prob of sentence so far: -1.6313559002514257
FRENCH: P(w) = -3.9204808901346184 ==> log prob of sentence so far: -3.9204808901346184

1GRAM: h
ENGLISH: P(h) = -1.1802729318845735 ==> log prob of sentence so far: -2.811628832135999
FRENCH: P(h) = -2.1056456076029977 ==> log prob of sentence so far: -6.026126497737616

1GRAM: a
ENGLISH: P(a) = -1.086787118026177 ==> log prob of sentence so far: -3.898415950162176
FRENCH: P(a) = -1.076350890171086 ==> log prob of sentence so far: -7.102477387908702

1GRAM: t
ENGLISH: P(t) = -1.0342219169534712 ==> log prob of sentence so far: -4.9326378671156474
FRENCH: P(t) = -1.1659649710967117 ==> log prob of sentence so far: -8.268442359005414

1GRAM: w
ENGLISH: P(w) = -1.6313559002514257 ==> log prob of sentence so far: -6.563993767367073
FRENCH: P(w) = -3.9204808901346184 ==> log prob of sentence so far: -12.188923249140032

1GRAM: i
ENGLISH: P(i) = -1.1625241810080462 ==> log prob of sentence so far: -7.726517948375119
FRENCH: P(i) = -1.1352045087261324 ==> log prob of sentence so far: -13.324127757866165

1GRAM: l
ENGLISH: P(l) = -1.3477684628666193 ==> log prob of sentence so far: -9.074286411241738
FRENCH: P(l) = -1.2682113125567938 ==> log prob of sentence so far: -14.592339070422959

1GRAM: l
ENGLISH: P(l) = -1.3477684628666193 ==> log prob of sentence so far: -10.422054874108358
FRENCH: P(l) = -1.2682113125567938 ==> log prob of sentence so far: -15.860550382979753

1GRAM: t
ENGLISH: P(t) = -1.0342219169534712 ==> log prob of sentence so far: -11.45627679106183
FRENCH: P(t) = -1.1659649710967117 ==> log prob of sentence so far: -17.026515354076466

1GRAM: h
ENGLISH: P(h) = -1.1802729318845735 ==> log prob of sentence so far: -12.636549722946404
FRENCH: P(h) = -2.1056456076029977 ==> log prob of sentence so far: -19.132160961679464

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -13.546618915204244
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -19.899123619832547

1GRAM: j
ENGLISH: P(j) = -2.9449302262255737 ==> log prob of sentence so far: -16.491549141429818
FRENCH: P(j) = -2.209598973291081 ==> log prob of sentence so far: -22.108722593123627

1GRAM: a
ENGLISH: P(a) = -1.086787118026177 ==> log prob of sentence so far: -17.578336259455995
FRENCH: P(a) = -1.076350890171086 ==> log prob of sentence so far: -23.185073483294712

1GRAM: p
ENGLISH: P(p) = -1.7418287685579088 ==> log prob of sentence so far: -19.320165028013903
FRENCH: P(p) = -1.5386370522359607 ==> log prob of sentence so far: -24.72371053553067

1GRAM: a
ENGLISH: P(a) = -1.086787118026177 ==> log prob of sentence so far: -20.40695214604008
FRENCH: P(a) = -1.076350890171086 ==> log prob of sentence so far: -25.800061425701756

1GRAM: n
ENGLISH: P(n) = -1.1615956346167673 ==> log prob of sentence so far: -21.568547780656846
FRENCH: P(n) = -1.1226354816746384 ==> log prob of sentence so far: -26.922696907376395

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -22.478616972914686
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -27.689659565529478

1GRAM: s
ENGLISH: P(s) = -1.1711397464845998 ==> log prob of sentence so far: -23.649756719399285
FRENCH: P(s) = -1.0644454594623793 ==> log prob of sentence so far: -28.75410502499186

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -24.559825911657125
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -29.521067683144942

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -25.469895103914965
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -30.288030341298025

1GRAM: c
ENGLISH: P(c) = -1.6265894821680524 ==> log prob of sentence so far: -27.09648458608302
FRENCH: P(c) = -1.4922244538487552 ==> log prob of sentence so far: -31.78025479514678

1GRAM: o
ENGLISH: P(o) = -1.1377823700508483 ==> log prob of sentence so far: -28.234266956133865
FRENCH: P(o) = -1.2743422038818786 ==> log prob of sentence so far: -33.05459699902866

1GRAM: n
ENGLISH: P(n) = -1.1615956346167673 ==> log prob of sentence so far: -29.39586259075063
FRENCH: P(n) = -1.1226354816746384 ==> log prob of sentence so far: -34.177232480703296

1GRAM: o
ENGLISH: P(o) = -1.1377823700508483 ==> log prob of sentence so far: -30.53364496080148
FRENCH: P(o) = -1.2743422038818786 ==> log prob of sentence so far: -35.451574684585175

1GRAM: m
ENGLISH: P(m) = -1.6111121655313874 ==> log prob of sentence so far: -32.14475712633287
FRENCH: P(m) = -1.512949635898697 ==> log prob of sentence so far: -36.964524320483875

1GRAM: y
ENGLISH: P(y) = -1.7506000117511689 ==> log prob of sentence so far: -33.89535713808404
FRENCH: P(y) = -2.6722939958631318 ==> log prob of sentence so far: -39.636818316347004

1GRAM: b
ENGLISH: P(b) = -1.7519554448616979 ==> log prob of sentence so far: -35.64731258294574
FRENCH: P(b) = -2.0519017542708387 ==> log prob of sentence so far: -41.68872007061784

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -36.55738177520358
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -42.455682728770924

1GRAM: l
ENGLISH: P(l) = -1.3477684628666193 ==> log prob of sentence so far: -37.9051502380702
FRENCH: P(l) = -1.2682113125567938 ==> log prob of sentence so far: -43.72389404132772

1GRAM: i
ENGLISH: P(i) = -1.1625241810080462 ==> log prob of sentence so far: -39.067674419078244
FRENCH: P(i) = -1.1352045087261324 ==> log prob of sentence so far: -44.85909855005385

1GRAM: k
ENGLISH: P(k) = -2.0727076081017395 ==> log prob of sentence so far: -41.14038202717998
FRENCH: P(k) = -3.53712924327307 ==> log prob of sentence so far: -48.39622779332692

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -42.050451219437825
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -49.163190451480006

1GRAM: n
ENGLISH: P(n) = -1.1615956346167673 ==> log prob of sentence so far: -43.21204685405459
FRENCH: P(n) = -1.1226354816746384 ==> log prob of sentence so far: -50.28582593315464

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -44.122116046312435
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -51.052788591307724

1GRAM: x
ENGLISH: P(x) = -2.9616241715616307 ==> log prob of sentence so far: -47.083740217874066
FRENCH: P(x) = -2.338677076650494 ==> log prob of sentence so far: -53.391465667958215

1GRAM: t
ENGLISH: P(t) = -1.0342219169534712 ==> log prob of sentence so far: -48.117962134827536
FRENCH: P(t) = -1.1659649710967117 ==> log prob of sentence so far: -54.55743063905493

1GRAM: y
ENGLISH: P(y) = -1.7506000117511689 ==> log prob of sentence so far: -49.868562146578704
FRENCH: P(y) = -2.6722939958631318 ==> log prob of sentence so far: -57.22972463491806

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -50.77863133883655
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -57.99668729307114

1GRAM: a
ENGLISH: P(a) = -1.086787118026177 ==> log prob of sentence so far: -51.86541845686273
FRENCH: P(a) = -1.076350890171086 ==> log prob of sentence so far: -59.073038183242225

1GRAM: r
ENGLISH: P(r) = -1.2612720228109857 ==> log prob of sentence so far: -53.12669047967371
FRENCH: P(r) = -1.1840269595391562 ==> log prob of sentence so far: -60.25706514278138

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: wh
ENGLISH: P(h|w) = -0.5698697104224215 ==> log prob of sentence so far: -0.5698697104224215
FRENCH: P(h|w) = -1.8549130223078556 ==> log prob of sentence so far: -1.8549130223078556

2GRAM: ha
ENGLISH: P(a|h) = -0.6935491986727692 ==> log prob of sentence so far: -1.2634189090951908
FRENCH: P(a|h) = -0.5242918232991061 ==> log prob of sentence so far: -2.379204845606962

2GRAM: at
ENGLISH: P(t|a) = -0.8627263979329755 ==> log prob of sentence so far: -2.126145307028166
FRENCH: P(t|a) = -1.2479558855108452 ==> log prob of sentence so far: -3.627160731117807

2GRAM: tw
ENGLISH: P(w|t) = -1.582846696654924 ==> log prob of sentence so far: -3.70899200368309
FRENCH: P(w|t) = -4.574924416268679 ==> log prob of sentence so far: -8.202085147386487

2GRAM: wi
ENGLISH: P(i|w) = -0.7710778623461667 ==> log prob of sentence so far: -4.480069866029257
FRENCH: P(i|w) = -0.9410991699241389 ==> log prob of sentence so far: -9.143184317310626

2GRAM: il
ENGLISH: P(l|i) = -1.230662020159717 ==> log prob of sentence so far: -5.710731886188974
FRENCH: P(l|i) = -0.8981988907257367 ==> log prob of sentence so far: -10.041383208036363

2GRAM: ll
ENGLISH: P(l|l) = -0.8383882133656653 ==> log prob of sentence so far: -6.549120099554639
FRENCH: P(l|l) = -1.0860476352293107 ==> log prob of sentence so far: -11.127430843265675

2GRAM: lt
ENGLISH: P(t|l) = -1.4624005582729764 ==> log prob of sentence so far: -8.011520657827615
FRENCH: P(t|l) = -2.334366197074892 ==> log prob of sentence so far: -13.461797040340567

2GRAM: th
ENGLISH: P(h|t) = -0.41925384309458535 ==> log prob of sentence so far: -8.430774500922201
FRENCH: P(h|t) = -2.1274562853189236 ==> log prob of sentence so far: -15.589253325659492

2GRAM: he
ENGLISH: P(e|h) = -0.3699866496853878 ==> log prob of sentence so far: -8.800761150607588
FRENCH: P(e|h) = -0.4575835390739619 ==> log prob of sentence so far: -16.046836864733454

2GRAM: ej
ENGLISH: P(j|e) = -2.8352260205874287 ==> log prob of sentence so far: -11.635987171195017
FRENCH: P(j|e) = -2.0397717130375423 ==> log prob of sentence so far: -18.086608577770996

2GRAM: ja
ENGLISH: P(a|j) = -0.6900273198130719 ==> log prob of sentence so far: -12.32601449100809
FRENCH: P(a|j) = -0.8115344621235662 ==> log prob of sentence so far: -18.898143039894563

2GRAM: ap
ENGLISH: P(p|a) = -1.5836468334372191 ==> log prob of sentence so far: -13.90966132444531
FRENCH: P(p|a) = -1.257318204721641 ==> log prob of sentence so far: -20.155461244616205

2GRAM: pa
ENGLISH: P(a|p) = -0.9078026863248108 ==> log prob of sentence so far: -14.81746401077012
FRENCH: P(a|p) = -0.6469438853738003 ==> log prob of sentence so far: -20.802405129990007

2GRAM: an
ENGLISH: P(n|a) = -0.7044992903650373 ==> log prob of sentence so far: -15.521963301135157
FRENCH: P(n|a) = -0.8167796096083183 ==> log prob of sentence so far: -21.619184739598325

2GRAM: ne
ENGLISH: P(e|n) = -1.053245365513592 ==> log prob of sentence so far: -16.57520866664875
FRENCH: P(e|n) = -0.754265128379799 ==> log prob of sentence so far: -22.373449867978124

2GRAM: es
ENGLISH: P(s|e) = -0.944851232165285 ==> log prob of sentence so far: -17.520059898814033
FRENCH: P(s|e) = -0.7261308722647525 ==> log prob of sentence so far: -23.099580740242878

2GRAM: se
ENGLISH: P(e|s) = -0.935704722053693 ==> log prob of sentence so far: -18.455764620867726
FRENCH: P(e|s) = -0.7710394179082264 ==> log prob of sentence so far: -23.870620158151105

2GRAM: ee
ENGLISH: P(e|e) = -1.3180598990871857 ==> log prob of sentence so far: -19.77382451995491
FRENCH: P(e|e) = -1.5122455828225252 ==> log prob of sentence so far: -25.38286574097363

2GRAM: ec
ENGLISH: P(c|e) = -1.4733278507402618 ==> log prob of sentence so far: -21.247152370695172
FRENCH: P(c|e) = -1.2462372285887275 ==> log prob of sentence so far: -26.62910296956236

2GRAM: co
ENGLISH: P(o|c) = -0.7819369014668373 ==> log prob of sentence so far: -22.02908927216201
FRENCH: P(o|c) = -0.6555829188501017 ==> log prob of sentence so far: -27.28468588841246

2GRAM: on
ENGLISH: P(n|o) = -0.8621791705270294 ==> log prob of sentence so far: -22.89126844268904
FRENCH: P(n|o) = -0.5204787696664979 ==> log prob of sentence so far: -27.805164658078958

2GRAM: no
ENGLISH: P(o|n) = -1.0865488228809994 ==> log prob of sentence so far: -23.97781726557004
FRENCH: P(o|n) = -1.2378153589422114 ==> log prob of sentence so far: -29.04298001702117

2GRAM: om
ENGLISH: P(m|o) = -1.1832474433615023 ==> log prob of sentence so far: -25.16106470893154
FRENCH: P(m|o) = -1.1432121524670795 ==> log prob of sentence so far: -30.186192169488248

2GRAM: my
ENGLISH: P(y|m) = -1.4424642268657086 ==> log prob of sentence so far: -26.60352893579725
FRENCH: P(y|m) = -2.5000854101934324 ==> log prob of sentence so far: -32.68627757968168

2GRAM: yb
ENGLISH: P(b|y) = -1.4117747653150854 ==> log prob of sentence so far: -28.015303701112337
FRENCH: P(b|y) = -2.3742349521525767 ==> log prob of sentence so far: -35.060512531834256

2GRAM: be
ENGLISH: P(e|b) = -0.6126236751075261 ==> log prob of sentence so far: -28.627927376219862
FRENCH: P(e|b) = -1.06003711531836 ==> log prob of sentence so far: -36.120549647152615

2GRAM: el
ENGLISH: P(l|e) = -1.3277759747230282 ==> log prob of sentence so far: -29.95570335094289
FRENCH: P(l|e) = -1.2044252837449716 ==> log prob of sentence so far: -37.32497493089759

2GRAM: li
ENGLISH: P(i|l) = -0.9280831904627344 ==> log prob of sentence so far: -30.883786541405623
FRENCH: P(i|l) = -1.202936578501793 ==> log prob of sentence so far: -38.52791150939938

2GRAM: ik
ENGLISH: P(k|i) = -1.8747162321111424 ==> log prob of sentence so far: -32.75850277351677
FRENCH: P(k|i) = -3.398038652017502 ==> log prob of sentence so far: -41.925950161416885

2GRAM: ke
ENGLISH: P(e|k) = -0.44619283348094124 ==> log prob of sentence so far: -33.20469560699771
FRENCH: P(e|k) = -0.9421414563352661 ==> log prob of sentence so far: -42.86809161775215

2GRAM: en
ENGLISH: P(n|e) = -1.0655045229859215 ==> log prob of sentence so far: -34.27020012998363
FRENCH: P(n|e) = -0.8688052128017848 ==> log prob of sentence so far: -43.73689683055393

2GRAM: ne
ENGLISH: P(e|n) = -1.053245365513592 ==> log prob of sentence so far: -35.32344549549722
FRENCH: P(e|n) = -0.754265128379799 ==> log prob of sentence so far: -44.49116195893373

2GRAM: ex
ENGLISH: P(x|e) = -2.163197644210439 ==> log prob of sentence so far: -37.48664313970766
FRENCH: P(x|e) = -2.26671787424639 ==> log prob of sentence so far: -46.75787983318012

2GRAM: xt
ENGLISH: P(t|x) = -0.6613995175068894 ==> log prob of sentence so far: -38.148042657214546
FRENCH: P(t|x) = -1.179444901063269 ==> log prob of sentence so far: -47.93732473424339

2GRAM: ty
ENGLISH: P(y|t) = -1.8603123207018948 ==> log prob of sentence so far: -40.00835497791644
FRENCH: P(y|t) = -3.0670685445728485 ==> log prob of sentence so far: -51.00439327881624

2GRAM: ye
ENGLISH: P(e|y) = -0.9196883579280865 ==> log prob of sentence so far: -40.92804333584453
FRENCH: P(e|y) = -0.5855010933248691 ==> log prob of sentence so far: -51.58989437214111

2GRAM: ea
ENGLISH: P(a|e) = -1.0585466791017586 ==> log prob of sentence so far: -41.98659001494629
FRENCH: P(a|e) = -1.5190367832599734 ==> log prob of sentence so far: -53.10893115540108

2GRAM: ar
ENGLISH: P(r|a) = -0.9854428440622539 ==> log prob of sentence so far: -42.97203285900854
FRENCH: P(r|a) = -1.0331636760797833 ==> log prob of sentence so far: -54.14209483148086

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: wha
ENGLISH: P(a|wh) = -0.36598838957048185 ==> log prob of sentence so far: -0.36598838957048185
FRENCH: P(a|wh) = -0.7781512503836436 ==> log prob of sentence so far: -0.7781512503836436

3GRAM: hat
ENGLISH: P(t|ha) = -0.49959744747047996 ==> log prob of sentence so far: -0.8655858370409618
FRENCH: P(t|ha) = -1.5474753410488424 ==> log prob of sentence so far: -2.325626591432486

3GRAM: atw
ENGLISH: P(w|at) = -1.4151436511863342 ==> log prob of sentence so far: -2.280729488227296
FRENCH: P(w|at) = -4.117867626566016 ==> log prob of sentence so far: -6.443494217998502

3GRAM: twi
ENGLISH: P(i|tw) = -0.7647785945025304 ==> log prob of sentence so far: -3.0455080827298264
FRENCH: P(i|tw) = -1.4771212547196624 ==> log prob of sentence so far: -7.920615472718165

3GRAM: wil
ENGLISH: P(l|wi) = -0.8313088527955881 ==> log prob of sentence so far: -3.8768169355254143
FRENCH: P(l|wi) = -1.1205739312058498 ==> log prob of sentence so far: -9.041189403924015

3GRAM: ill
ENGLISH: P(l|il) = -0.4563620586912566 ==> log prob of sentence so far: -4.333178994216671
FRENCH: P(l|il) = -0.6183649685089712 ==> log prob of sentence so far: -9.659554372432986

3GRAM: llt
ENGLISH: P(t|ll) = -0.9321012355053122 ==> log prob of sentence so far: -5.265280229721983
FRENCH: P(t|ll) = -4.087426457036285 ==> log prob of sentence so far: -13.74698082946927

3GRAM: lth
ENGLISH: P(h|lt) = -0.265563568636894 ==> log prob of sentence so far: -5.530843798358878
FRENCH: P(h|lt) = -2.8536982117761744 ==> log prob of sentence so far: -16.600679041245446

3GRAM: the
ENGLISH: P(e|th) = -0.2044551165913694 ==> log prob of sentence so far: -5.735298914950247
FRENCH: P(e|th) = -0.37814519438647765 ==> log prob of sentence so far: -16.978824235631922

3GRAM: hej
ENGLISH: P(j|he) = -2.517546325600808 ==> log prob of sentence so far: -8.252845240551055
FRENCH: P(j|he) = -2.7623899767795437 ==> log prob of sentence so far: -19.741214212411467

3GRAM: eja
ENGLISH: P(a|ej) = -0.7021285235808485 ==> log prob of sentence so far: -8.954973764131903
FRENCH: P(a|ej) = -0.6517538824921862 ==> log prob of sentence so far: -20.392968094903654

3GRAM: jap
ENGLISH: P(p|ja) = -1.0134526745563297 ==> log prob of sentence so far: -9.968426438688233
FRENCH: P(p|ja) = -0.9537548120473629 ==> log prob of sentence so far: -21.34672290695102

3GRAM: apa
ENGLISH: P(a|ap) = -1.1442016400420638 ==> log prob of sentence so far: -11.112628078730298
FRENCH: P(a|ap) = -1.10582233855604 ==> log prob of sentence so far: -22.45254524550706

3GRAM: pan
ENGLISH: P(n|pa) = -0.7999176104126836 ==> log prob of sentence so far: -11.91254568914298
FRENCH: P(n|pa) = -1.5279441646145029 ==> log prob of sentence so far: -23.980489410121564

3GRAM: ane
ENGLISH: P(e|an) = -1.6399939740645415 ==> log prob of sentence so far: -13.552539663207522
FRENCH: P(e|an) = -1.552898173002253 ==> log prob of sentence so far: -25.533387583123815

3GRAM: nes
ENGLISH: P(s|ne) = -0.726646327399672 ==> log prob of sentence so far: -14.279185990607195
FRENCH: P(s|ne) = -0.8278084722845006 ==> log prob of sentence so far: -26.361196055408318

3GRAM: ese
ENGLISH: P(e|es) = -0.9488648489844685 ==> log prob of sentence so far: -15.228050839591663
FRENCH: P(e|es) = -0.9179612970451427 ==> log prob of sentence so far: -27.27915735245346

3GRAM: see
ENGLISH: P(e|se) = -0.8028086577965827 ==> log prob of sentence so far: -16.030859497388246
FRENCH: P(e|se) = -1.4967218595154024 ==> log prob of sentence so far: -28.775879211968864

3GRAM: eec
ENGLISH: P(c|ee) = -1.9744976146966668 ==> log prob of sentence so far: -18.005357112084912
FRENCH: P(c|ee) = -1.6995791131061366 ==> log prob of sentence so far: -30.475458325075

3GRAM: eco
ENGLISH: P(o|ec) = -0.726979832011709 ==> log prob of sentence so far: -18.73233694409662
FRENCH: P(o|ec) = -0.6699858041006396 ==> log prob of sentence so far: -31.14544412917564

3GRAM: con
ENGLISH: P(n|co) = -0.536418951288437 ==> log prob of sentence so far: -19.268755895385056
FRENCH: P(n|co) = -0.43302409281470117 ==> log prob of sentence so far: -31.57846822199034

3GRAM: ono
ENGLISH: P(o|on) = -1.2618860129424725 ==> log prob of sentence so far: -20.530641908327528
FRENCH: P(o|on) = -1.8301604625083063 ==> log prob of sentence so far: -33.408628684498645

3GRAM: nom
ENGLISH: P(m|no) = -1.5121215490330553 ==> log prob of sentence so far: -22.042763457360582
FRENCH: P(m|no) = -1.0252265822231523 ==> log prob of sentence so far: -34.4338552667218

3GRAM: omy
ENGLISH: P(y|om) = -1.721959756901604 ==> log prob of sentence so far: -23.764723214262187
FRENCH: P(y|om) = -3.071309113343219 ==> log prob of sentence so far: -37.50516438006502

3GRAM: myb
ENGLISH: P(b|my) = -1.175384514421432 ==> log prob of sentence so far: -24.940107728683618
FRENCH: P(b|my) = -2.4683473304121573 ==> log prob of sentence so far: -39.973511710477176

3GRAM: ybe
ENGLISH: P(e|yb) = -0.4298416718431957 ==> log prob of sentence so far: -25.369949400526814
FRENCH: P(e|yb) = -1.6989700043360187 ==> log prob of sentence so far: -41.672481714813195

3GRAM: bel
ENGLISH: P(l|be) = -1.225103938986501 ==> log prob of sentence so far: -26.595053339513314
FRENCH: P(l|be) = -0.8572512515555468 ==> log prob of sentence so far: -42.52973296636874

3GRAM: eli
ENGLISH: P(i|el) = -0.8556611118407429 ==> log prob of sentence so far: -27.450714451354056
FRENCH: P(i|el) = -1.2613629379268763 ==> log prob of sentence so far: -43.79109590429562

3GRAM: lik
ENGLISH: P(k|li) = -0.8395589618346746 ==> log prob of sentence so far: -28.290273413188732
FRENCH: P(k|li) = -3.970439862951764 ==> log prob of sentence so far: -47.76153576724738

3GRAM: ike
ENGLISH: P(e|ik) = -0.04734430598044478 ==> log prob of sentence so far: -28.337617719169177
FRENCH: P(e|ik) = -2.0253058652647704 ==> log prob of sentence so far: -49.78684163251215

3GRAM: ken
ENGLISH: P(n|ke) = -1.0016159813320304 ==> log prob of sentence so far: -29.339233700501207
FRENCH: P(n|ke) = -0.553368067428238 ==> log prob of sentence so far: -50.34020969994039

3GRAM: ene
ENGLISH: P(e|en) = -1.0721460475221272 ==> log prob of sentence so far: -30.411379748023336
FRENCH: P(e|en) = -0.9132392354146238 ==> log prob of sentence so far: -51.25344893535501

3GRAM: nex
ENGLISH: P(x|ne) = -1.596881181936458 ==> log prob of sentence so far: -32.008260929959796
FRENCH: P(x|ne) = -2.010573803373797 ==> log prob of sentence so far: -53.264022738728805

3GRAM: ext
ENGLISH: P(t|ex) = -0.6129915040973003 ==> log prob of sentence so far: -32.6212524340571
FRENCH: P(t|ex) = -0.6511020300715198 ==> log prob of sentence so far: -53.915124768800325

3GRAM: xty
ENGLISH: P(y|xt) = -1.0957185322006378 ==> log prob of sentence so far: -33.71697096625773
FRENCH: P(y|xt) = -2.9253120914996495 ==> log prob of sentence so far: -56.840436860299974

3GRAM: tye
ENGLISH: P(e|ty) = -1.0038863300223915 ==> log prob of sentence so far: -34.72085729628012
FRENCH: P(e|ty) = -1.3152704347785915 ==> log prob of sentence so far: -58.155707295078564

3GRAM: yea
ENGLISH: P(a|ye) = -0.851217056721607 ==> log prob of sentence so far: -35.57207435300173
FRENCH: P(a|ye) = -1.672937074587008 ==> log prob of sentence so far: -59.828644369665575

3GRAM: ear
ENGLISH: P(r|ea) = -0.7831826042221113 ==> log prob of sentence so far: -36.355256957223844
FRENCH: P(r|ea) = -1.6004145882496386 ==> log prob of sentence so far: -61.42905895791522

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: what
ENGLISH: P(t|wha) = -0.5654942614785158 ==> log prob of sentence so far: -0.5654942614785158
FRENCH: P(t|wha) = -1.4771212547196624 ==> log prob of sentence so far: -1.4771212547196624

4GRAM: hatw
ENGLISH: P(w|hat) = -1.1292990221804053 ==> log prob of sentence so far: -1.694793283658921
FRENCH: P(w|hat) = -2.322219294733919 ==> log prob of sentence so far: -3.7993405494535812

4GRAM: atwi
ENGLISH: P(i|atw) = -0.7663256700193375 ==> log prob of sentence so far: -2.4611189536782585
FRENCH: P(i|atw) = -1.4313637641589874 ==> log prob of sentence so far: -5.230704313612568

4GRAM: twil
ENGLISH: P(l|twi) = -0.6992439207638335 ==> log prob of sentence so far: -3.160362874442092
FRENCH: P(l|twi) = -1.4313637641589874 ==> log prob of sentence so far: -6.662068077771556

4GRAM: will
ENGLISH: P(l|wil) = -0.12300595868794571 ==> log prob of sentence so far: -3.2833688331300377
FRENCH: P(l|wil) = -1.4771212547196624 ==> log prob of sentence so far: -8.139189332491219

4GRAM: illt
ENGLISH: P(t|ill) = -1.1157617547480565 ==> log prob of sentence so far: -4.399130587878094
FRENCH: P(t|ill) = -3.781755374652469 ==> log prob of sentence so far: -11.920944707143688

4GRAM: llth
ENGLISH: P(h|llt) = -0.07440666004028705 ==> log prob of sentence so far: -4.473537247918381
FRENCH: P(h|llt) = -1.4313637641589874 ==> log prob of sentence so far: -13.352308471302676

4GRAM: lthe
ENGLISH: P(e|lth) = -0.21588476081058178 ==> log prob of sentence so far: -4.689422008728963
FRENCH: P(e|lth) = -1.4313637641589874 ==> log prob of sentence so far: -14.783672235461664

4GRAM: thej
ENGLISH: P(j|the) = -2.419469797357145 ==> log prob of sentence so far: -7.108891806086108
FRENCH: P(j|the) = -2.790988475088816 ==> log prob of sentence so far: -17.57466071055048

4GRAM: heja
ENGLISH: P(a|hej) = -0.5317422056116426 ==> log prob of sentence so far: -7.640634011697751
FRENCH: P(a|hej) = -0.8808135922807914 ==> log prob of sentence so far: -18.45547430283127

4GRAM: ejap
ENGLISH: P(p|eja) = -0.979066093164357 ==> log prob of sentence so far: -8.619700104862108
FRENCH: P(p|eja) = -0.9726876158712732 ==> log prob of sentence so far: -19.428161918702543

4GRAM: japa
ENGLISH: P(a|jap) = -0.12748592562217986 ==> log prob of sentence so far: -8.747186030484288
FRENCH: P(a|jap) = -1.393912519388994 ==> log prob of sentence so far: -20.822074438091537

4GRAM: apan
ENGLISH: P(n|apa) = -0.7142867624201246 ==> log prob of sentence so far: -9.461472792904413
FRENCH: P(n|apa) = -1.7840716173796498 ==> log prob of sentence so far: -22.606146055471186

4GRAM: pane
ENGLISH: P(e|pan) = -1.1656941590228311 ==> log prob of sentence so far: -10.627166951927244
FRENCH: P(e|pan) = -2.04766419460156 ==> log prob of sentence so far: -24.653810250072745

4GRAM: anes
ENGLISH: P(s|ane) = -0.9361889242222454 ==> log prob of sentence so far: -11.563355876149489
FRENCH: P(s|ane) = -0.9361355120364809 ==> log prob of sentence so far: -25.589945762109224

4GRAM: nese
ENGLISH: P(e|nes) = -1.4783874202867873 ==> log prob of sentence so far: -13.041743296436277
FRENCH: P(e|nes) = -0.7010351270189887 ==> log prob of sentence so far: -26.290980889128214

4GRAM: esee
ENGLISH: P(e|ese) = -0.8065315130075286 ==> log prob of sentence so far: -13.848274809443804
FRENCH: P(e|ese) = -3.3226327116922234 ==> log prob of sentence so far: -29.613613600820436

4GRAM: seec
ENGLISH: P(c|see) = -2.2377447538751847 ==> log prob of sentence so far: -16.086019563318988
FRENCH: P(c|see) = -1.707229419327294 ==> log prob of sentence so far: -31.32084302014773

4GRAM: eeco
ENGLISH: P(o|eec) = -1.0068937079479003 ==> log prob of sentence so far: -17.092913271266887
FRENCH: P(o|eec) = -0.7670858242334837 ==> log prob of sentence so far: -32.08792884438122

4GRAM: econ
ENGLISH: P(n|eco) = -0.5131966201649422 ==> log prob of sentence so far: -17.606109891431828
FRENCH: P(n|eco) = -0.43813809171219376 ==> log prob of sentence so far: -32.526066936093414

4GRAM: cono
ENGLISH: P(o|con) = -2.4096348099948464 ==> log prob of sentence so far: -20.015744701426673
FRENCH: P(o|con) = -2.3988973956277397 ==> log prob of sentence so far: -34.924964331721156

4GRAM: onom
ENGLISH: P(m|ono) = -1.253560997540842 ==> log prob of sentence so far: -21.269305698967514
FRENCH: P(m|ono) = -0.7180701862360709 ==> log prob of sentence so far: -35.643034517957226

4GRAM: nomy
ENGLISH: P(y|nom) = -1.3636768048018704 ==> log prob of sentence so far: -22.632982503769384
FRENCH: P(y|nom) = -3.0622058088197126 ==> log prob of sentence so far: -38.70524032677694

4GRAM: omyb
ENGLISH: P(b|omy) = -1.57467419083833 ==> log prob of sentence so far: -24.207656694607714
FRENCH: P(b|omy) = -1.5314789170422551 ==> log prob of sentence so far: -40.236719243819195

4GRAM: mybe
ENGLISH: P(e|myb) = -0.7085153222422492 ==> log prob of sentence so far: -24.916172016849963
FRENCH: P(e|myb) = -1.4313637641589874 ==> log prob of sentence so far: -41.66808300797818

4GRAM: ybel
ENGLISH: P(l|ybe) = -1.1201923015838384 ==> log prob of sentence so far: -26.0363643184338
FRENCH: P(l|ybe) = -1.4313637641589874 ==> log prob of sentence so far: -43.09944677213717

4GRAM: beli
ENGLISH: P(i|bel) = -0.6247555667609307 ==> log prob of sentence so far: -26.661119885194733
FRENCH: P(i|bel) = -1.2719781986061587 ==> log prob of sentence so far: -44.37142497074333

4GRAM: elik
ENGLISH: P(k|eli) = -0.9578939054390323 ==> log prob of sentence so far: -27.619013790633765
FRENCH: P(k|eli) = -3.2121876044039577 ==> log prob of sentence so far: -47.583612575147285

4GRAM: like
ENGLISH: P(e|lik) = -0.004331777950295051 ==> log prob of sentence so far: -27.62334556858406
FRENCH: P(e|lik) = -1.4313637641589874 ==> log prob of sentence so far: -49.01497633930627

4GRAM: iken
ENGLISH: P(n|ike) = -1.602951461668836 ==> log prob of sentence so far: -29.226297030252898
FRENCH: P(n|ike) = -1.4313637641589874 ==> log prob of sentence so far: -50.44634010346526

4GRAM: kene
ENGLISH: P(e|ken) = -0.8202977011032053 ==> log prob of sentence so far: -30.0465947313561
FRENCH: P(e|ken) = -1.7634279935629373 ==> log prob of sentence so far: -52.209768097028196

4GRAM: enex
ENGLISH: P(x|ene) = -1.344053325348306 ==> log prob of sentence so far: -31.390648056704407
FRENCH: P(x|ene) = -2.275170847089066 ==> log prob of sentence so far: -54.48493894411726

4GRAM: next
ENGLISH: P(t|nex) = -0.3317349689773346 ==> log prob of sentence so far: -31.72238302568174
FRENCH: P(t|nex) = -0.6281017931923126 ==> log prob of sentence so far: -55.113040737309575

4GRAM: exty
ENGLISH: P(y|ext) = -2.187520720836463 ==> log prob of sentence so far: -33.909903746518204
FRENCH: P(y|ext) = -2.776701183988411 ==> log prob of sentence so far: -57.88974192129798

4GRAM: xtye
ENGLISH: P(e|xty) = -1.9731278535996988 ==> log prob of sentence so far: -35.8830316001179
FRENCH: P(e|xty) = -1.4313637641589874 ==> log prob of sentence so far: -59.32110568545697

4GRAM: tyea
ENGLISH: P(a|tye) = -1.2704459080179626 ==> log prob of sentence so far: -37.153477508135865
FRENCH: P(a|tye) = -1.5314789170422551 ==> log prob of sentence so far: -60.85258460249923

4GRAM: year
ENGLISH: P(r|yea) = -0.26494870358971884 ==> log prob of sentence so far: -37.41842621172558
FRENCH: P(r|yea) = -1.7634279935629373 ==> log prob of sentence so far: -62.61601259606216

According to the 4gram model, the sentence is in English
----------------
