They are bad hombres.

1GRAM MODEL:

1GRAM: t
ENGLISH: P(t) = -1.0342219169534712 ==> log prob of sentence so far: -1.0342219169534712
FRENCH: P(t) = -1.1659649710967117 ==> log prob of sentence so far: -1.1659649710967117
DUTCH: P(t) = -1.2498818524344808 ==> log prob of sentence so far: -1.2498818524344808
PORTUGUESE: P(t) = -1.3798321761018175 ==> log prob of sentence so far: -1.3798321761018175
SPANISH: P(t) = -1.4035914062065482 ==> log prob of sentence so far: -1.4035914062065482

1GRAM: h
ENGLISH: P(h) = -1.1802729318845735 ==> log prob of sentence so far: -2.2144948488380445
FRENCH: P(h) = -2.1056456076029977 ==> log prob of sentence so far: -3.2716105786997094
DUTCH: P(h) = -1.4907869107844225 ==> log prob of sentence so far: -2.7406687632189035
PORTUGUESE: P(h) = -1.8092331706505171 ==> log prob of sentence so far: -3.1890653467523347
SPANISH: P(h) = -1.8922261844491735 ==> log prob of sentence so far: -3.2958175906557217

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -3.124564041095885
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -4.038573236852793
DUTCH: P(e) = -0.7296470912483529 ==> log prob of sentence so far: -3.4703158544672563
PORTUGUESE: P(e) = -0.880446229569894 ==> log prob of sentence so far: -4.0695115763222285
SPANISH: P(e) = -0.881649517549016 ==> log prob of sentence so far: -4.177467108204738

1GRAM: y
ENGLISH: P(y) = -1.7506000117511689 ==> log prob of sentence so far: -4.875164052847054
FRENCH: P(y) = -2.6722939958631318 ==> log prob of sentence so far: -6.710867232715925
DUTCH: P(y) = -3.569878686206462 ==> log prob of sentence so far: -7.040194540673719
PORTUGUESE: P(y) = -3.344784162555018 ==> log prob of sentence so far: -7.414295738877247
SPANISH: P(y) = -1.9106442581412726 ==> log prob of sentence so far: -6.08811136634601

1GRAM: a
ENGLISH: P(a) = -1.086787118026177 ==> log prob of sentence so far: -5.961951170873231
FRENCH: P(a) = -1.076350890171086 ==> log prob of sentence so far: -7.787218122887011
DUTCH: P(a) = -1.1153901057578817 ==> log prob of sentence so far: -8.1555846464316
PORTUGUESE: P(a) = -0.8318956635743947 ==> log prob of sentence so far: -8.246191402451641
SPANISH: P(a) = -0.9020204392084481 ==> log prob of sentence so far: -6.990131805554459

1GRAM: r
ENGLISH: P(r) = -1.2612720228109857 ==> log prob of sentence so far: -7.223223193684217
FRENCH: P(r) = -1.1840269595391562 ==> log prob of sentence so far: -8.971245082426167
DUTCH: P(r) = -1.254200750493856 ==> log prob of sentence so far: -9.409785396925457
PORTUGUESE: P(r) = -1.1945420650398737 ==> log prob of sentence so far: -9.440733467491516
SPANISH: P(r) = -1.1910423872739788 ==> log prob of sentence so far: -8.181174192828438

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -8.133292385942058
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -9.73820774057925
DUTCH: P(e) = -0.7296470912483529 ==> log prob of sentence so far: -10.13943248817381
PORTUGUESE: P(e) = -0.880446229569894 ==> log prob of sentence so far: -10.32117969706141
SPANISH: P(e) = -0.881649517549016 ==> log prob of sentence so far: -9.062823710377454

1GRAM: b
ENGLISH: P(b) = -1.7519554448616979 ==> log prob of sentence so far: -9.885247830803756
FRENCH: P(b) = -2.0519017542708387 ==> log prob of sentence so far: -11.790109494850089
DUTCH: P(b) = -1.8074783299428059 ==> log prob of sentence so far: -11.946910818116615
PORTUGUESE: P(b) = -2.0434887868143994 ==> log prob of sentence so far: -12.36466848387581
SPANISH: P(b) = -1.8063346755744631 ==> log prob of sentence so far: -10.869158385951918

1GRAM: a
ENGLISH: P(a) = -1.086787118026177 ==> log prob of sentence so far: -10.972034948829933
FRENCH: P(a) = -1.076350890171086 ==> log prob of sentence so far: -12.866460385021176
DUTCH: P(a) = -1.1153901057578817 ==> log prob of sentence so far: -13.062300923874497
PORTUGUESE: P(a) = -0.8318956635743947 ==> log prob of sentence so far: -13.196564147450205
SPANISH: P(a) = -0.9020204392084481 ==> log prob of sentence so far: -11.771178825160366

1GRAM: d
ENGLISH: P(d) = -1.3961797927180317 ==> log prob of sentence so far: -12.368214741547964
FRENCH: P(d) = -1.4202617278792675 ==> log prob of sentence so far: -14.286722112900444
DUTCH: P(d) = -1.2011844788481099 ==> log prob of sentence so far: -14.263485402722607
PORTUGUESE: P(d) = -1.3354039236187203 ==> log prob of sentence so far: -14.531968071068926
SPANISH: P(d) = -1.2907619213439092 ==> log prob of sentence so far: -13.061940746504275

1GRAM: h
ENGLISH: P(h) = -1.1802729318845735 ==> log prob of sentence so far: -13.548487673432538
FRENCH: P(h) = -2.1056456076029977 ==> log prob of sentence so far: -16.39236772050344
DUTCH: P(h) = -1.4907869107844225 ==> log prob of sentence so far: -15.754272313507029
PORTUGUESE: P(h) = -1.8092331706505171 ==> log prob of sentence so far: -16.341201241719443
SPANISH: P(h) = -1.8922261844491735 ==> log prob of sentence so far: -14.954166930953448

1GRAM: o
ENGLISH: P(o) = -1.1377823700508483 ==> log prob of sentence so far: -14.686270043483386
FRENCH: P(o) = -1.2743422038818786 ==> log prob of sentence so far: -17.66670992438532
DUTCH: P(o) = -1.1998170018851302 ==> log prob of sentence so far: -16.95408931539216
PORTUGUESE: P(o) = -0.9821385704267878 ==> log prob of sentence so far: -17.32333981214623
SPANISH: P(o) = -0.9966304259011738 ==> log prob of sentence so far: -15.950797356854622

1GRAM: m
ENGLISH: P(m) = -1.6111121655313874 ==> log prob of sentence so far: -16.297382209014774
FRENCH: P(m) = -1.512949635898697 ==> log prob of sentence so far: -19.179659560284016
DUTCH: P(m) = -1.6188687884896773 ==> log prob of sentence so far: -18.572958103881838
PORTUGUESE: P(m) = -1.3143893368677781 ==> log prob of sentence so far: -18.637729149014007
SPANISH: P(m) = -1.497083341850591 ==> log prob of sentence so far: -17.447880698705212

1GRAM: b
ENGLISH: P(b) = -1.7519554448616979 ==> log prob of sentence so far: -18.04933765387647
FRENCH: P(b) = -2.0519017542708387 ==> log prob of sentence so far: -21.231561314554853
DUTCH: P(b) = -1.8074783299428059 ==> log prob of sentence so far: -20.380436433824645
PORTUGUESE: P(b) = -2.0434887868143994 ==> log prob of sentence so far: -20.681217935828407
SPANISH: P(b) = -1.8063346755744631 ==> log prob of sentence so far: -19.254215374279674

1GRAM: r
ENGLISH: P(r) = -1.2612720228109857 ==> log prob of sentence so far: -19.310609676687456
FRENCH: P(r) = -1.1840269595391562 ==> log prob of sentence so far: -22.41558827409401
DUTCH: P(r) = -1.254200750493856 ==> log prob of sentence so far: -21.6346371843185
PORTUGUESE: P(r) = -1.1945420650398737 ==> log prob of sentence so far: -21.87576000086828
SPANISH: P(r) = -1.1910423872739788 ==> log prob of sentence so far: -20.445257761553652

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -20.220678868945296
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -23.182550932247093
DUTCH: P(e) = -0.7296470912483529 ==> log prob of sentence so far: -22.364284275566853
PORTUGUESE: P(e) = -0.880446229569894 ==> log prob of sentence so far: -22.756206230438174
SPANISH: P(e) = -0.881649517549016 ==> log prob of sentence so far: -21.326907279102667

1GRAM: s
ENGLISH: P(s) = -1.1711397464845998 ==> log prob of sentence so far: -21.391818615429894
FRENCH: P(s) = -1.0644454594623793 ==> log prob of sentence so far: -24.246996391709473
DUTCH: P(s) = -1.4604404489822762 ==> log prob of sentence so far: -23.824724724549128
PORTUGUESE: P(s) = -1.112894773130837 ==> log prob of sentence so far: -23.86910100356901
SPANISH: P(s) = -1.1432585839855418 ==> log prob of sentence so far: -22.47016586308821

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: th
ENGLISH: P(h|t) = -0.41925384309458535 ==> log prob of sentence so far: -0.41925384309458535
FRENCH: P(h|t) = -2.1274562853189236 ==> log prob of sentence so far: -2.1274562853189236
DUTCH: P(h|t) = -1.3564343503377285 ==> log prob of sentence so far: -1.3564343503377285
PORTUGUESE: P(h|t) = -2.1877048512041175 ==> log prob of sentence so far: -2.1877048512041175
SPANISH: P(h|t) = -3.648540147646016 ==> log prob of sentence so far: -3.648540147646016

2GRAM: he
ENGLISH: P(e|h) = -0.3699866496853878 ==> log prob of sentence so far: -0.7892404927799732
FRENCH: P(e|h) = -0.4575835390739619 ==> log prob of sentence so far: -2.5850398243928856
DUTCH: P(e|h) = -0.4399480982151489 ==> log prob of sentence so far: -1.7963824485528774
PORTUGUESE: P(e|h) = -0.5748122410029949 ==> log prob of sentence so far: -2.7625170922071125
SPANISH: P(e|h) = -0.8486149909244649 ==> log prob of sentence so far: -4.497155138570481

2GRAM: ey
ENGLISH: P(y|e) = -1.831389874014664 ==> log prob of sentence so far: -2.620630366794637
FRENCH: P(y|e) = -3.4648966632649096 ==> log prob of sentence so far: -6.049936487657796
DUTCH: P(y|e) = -4.367094893123658 ==> log prob of sentence so far: -6.163477341676536
PORTUGUESE: P(y|e) = -3.2771092409427776 ==> log prob of sentence so far: -6.03962633314989
SPANISH: P(y|e) = -2.0103603744004968 ==> log prob of sentence so far: -6.507515512970977

2GRAM: ya
ENGLISH: P(a|y) = -1.0421822804971672 ==> log prob of sentence so far: -3.6628126472918043
FRENCH: P(a|y) = -0.6208051105771538 ==> log prob of sentence so far: -6.670741598234949
DUTCH: P(a|y) = -1.15358391805783 ==> log prob of sentence so far: -7.317061259734366
PORTUGUESE: P(a|y) = -1.5195251032727497 ==> log prob of sentence so far: -7.55915143642264
SPANISH: P(a|y) = -0.7714673278990829 ==> log prob of sentence so far: -7.278982840870061

2GRAM: ar
ENGLISH: P(r|a) = -0.9854428440622539 ==> log prob of sentence so far: -4.648255491354059
FRENCH: P(r|a) = -1.0331636760797833 ==> log prob of sentence so far: -7.7039052743147325
DUTCH: P(r|a) = -0.9128098519981566 ==> log prob of sentence so far: -8.229871111732523
PORTUGUESE: P(r|a) = -1.0106275820610928 ==> log prob of sentence so far: -8.569779018483732
SPANISH: P(r|a) = -0.9443875898070365 ==> log prob of sentence so far: -8.223370430677097

2GRAM: re
ENGLISH: P(e|r) = -0.628161706166691 ==> log prob of sentence so far: -5.2764171975207494
FRENCH: P(e|r) = -0.4909156061883572 ==> log prob of sentence so far: -8.194820880503089
DUTCH: P(e|r) = -0.7944662692235398 ==> log prob of sentence so far: -9.024337380956062
PORTUGUESE: P(e|r) = -0.7385662934125243 ==> log prob of sentence so far: -9.308345311896257
SPANISH: P(e|r) = -0.6818289630670133 ==> log prob of sentence so far: -8.90519939374411

2GRAM: eb
ENGLISH: P(b|e) = -1.6930812910594353 ==> log prob of sentence so far: -6.969498488580185
FRENCH: P(b|e) = -2.134533313053115 ==> log prob of sentence so far: -10.329354193556204
DUTCH: P(b|e) = -1.7346376009389344 ==> log prob of sentence so far: -10.758974981894998
PORTUGUESE: P(b|e) = -2.0925574322070446 ==> log prob of sentence so far: -11.400902744103302
SPANISH: P(b|e) = -2.101068184671782 ==> log prob of sentence so far: -11.006267578415892

2GRAM: ba
ENGLISH: P(a|b) = -1.2018226321960257 ==> log prob of sentence so far: -8.171321120776211
FRENCH: P(a|b) = -0.8259707819702409 ==> log prob of sentence so far: -11.155324975526446
DUTCH: P(a|b) = -1.1576943933269033 ==> log prob of sentence so far: -11.9166693752219
PORTUGUESE: P(a|b) = -0.6436643209747344 ==> log prob of sentence so far: -12.044567065078036
SPANISH: P(a|b) = -0.47085524786398186 ==> log prob of sentence so far: -11.477122826279874

2GRAM: ad
ENGLISH: P(d|a) = -1.3608787833534604 ==> log prob of sentence so far: -9.532199904129671
FRENCH: P(d|a) = -1.654802154739421 ==> log prob of sentence so far: -12.810127130265867
DUTCH: P(d|a) = -1.338319791130801 ==> log prob of sentence so far: -13.2549891663527
PORTUGUESE: P(d|a) = -1.065511854061316 ==> log prob of sentence so far: -13.110078919139353
SPANISH: P(d|a) = -1.0184030680609113 ==> log prob of sentence so far: -12.495525894340785

2GRAM: dh
ENGLISH: P(h|d) = -1.4165746116493925 ==> log prob of sentence so far: -10.948774515779064
FRENCH: P(h|d) = -2.32053184476401 ==> log prob of sentence so far: -15.130658975029878
DUTCH: P(h|d) = -1.722342770247731 ==> log prob of sentence so far: -14.977331936600432
PORTUGUESE: P(h|d) = -3.9217280825426397 ==> log prob of sentence so far: -17.031807001681994
SPANISH: P(h|d) = -3.1834848308878265 ==> log prob of sentence so far: -15.679010725228611

2GRAM: ho
ENGLISH: P(o|h) = -1.1036467362119045 ==> log prob of sentence so far: -12.05242125199097
FRENCH: P(o|h) = -0.8327964879250233 ==> log prob of sentence so far: -15.9634554629549
DUTCH: P(o|h) = -1.017442148638513 ==> log prob of sentence so far: -15.994774085238944
PORTUGUESE: P(o|h) = -0.5404521724327865 ==> log prob of sentence so far: -17.57225917411478
SPANISH: P(o|h) = -0.5331756089960642 ==> log prob of sentence so far: -16.212186334224675

2GRAM: om
ENGLISH: P(m|o) = -1.1832474433615023 ==> log prob of sentence so far: -13.235668695352471
FRENCH: P(m|o) = -1.1432121524670795 ==> log prob of sentence so far: -17.10666761542198
DUTCH: P(m|o) = -1.2691267463554436 ==> log prob of sentence so far: -17.263900831594388
PORTUGUESE: P(m|o) = -1.0086885328949597 ==> log prob of sentence so far: -18.58094770700974
SPANISH: P(m|o) = -1.1213246714538034 ==> log prob of sentence so far: -17.333511005678478

2GRAM: mb
ENGLISH: P(b|m) = -1.5584081865531085 ==> log prob of sentence so far: -14.79407688190558
FRENCH: P(b|m) = -1.4619337436730402 ==> log prob of sentence so far: -18.56860135909502
DUTCH: P(b|m) = -1.0952726514356332 ==> log prob of sentence so far: -18.35917348303002
PORTUGUESE: P(b|m) = -1.524806916494387 ==> log prob of sentence so far: -20.10575462350413
SPANISH: P(b|m) = -1.2483635488390668 ==> log prob of sentence so far: -18.581874554517544

2GRAM: br
ENGLISH: P(r|b) = -1.2407951036485254 ==> log prob of sentence so far: -16.034871985554105
FRENCH: P(r|b) = -0.7693540514778837 ==> log prob of sentence so far: -19.337955410572903
DUTCH: P(r|b) = -1.1240728798657234 ==> log prob of sentence so far: -19.48324636289574
PORTUGUESE: P(r|b) = -0.6502340008136325 ==> log prob of sentence so far: -20.755988624317762
SPANISH: P(r|b) = -0.8224077143895707 ==> log prob of sentence so far: -19.404282268907114

2GRAM: re
ENGLISH: P(e|r) = -0.628161706166691 ==> log prob of sentence so far: -16.663033691720795
FRENCH: P(e|r) = -0.4909156061883572 ==> log prob of sentence so far: -19.82887101676126
DUTCH: P(e|r) = -0.7944662692235398 ==> log prob of sentence so far: -20.277712632119282
PORTUGUESE: P(e|r) = -0.7385662934125243 ==> log prob of sentence so far: -21.494554917730287
SPANISH: P(e|r) = -0.6818289630670133 ==> log prob of sentence so far: -20.086111231974126

2GRAM: es
ENGLISH: P(s|e) = -0.944851232165285 ==> log prob of sentence so far: -17.60788492388608
FRENCH: P(s|e) = -0.7261308722647525 ==> log prob of sentence so far: -20.555001889026013
DUTCH: P(s|e) = -1.4885289103627197 ==> log prob of sentence so far: -21.766241542482003
PORTUGUESE: P(s|e) = -0.8220645789953112 ==> log prob of sentence so far: -22.316619496725597
SPANISH: P(s|e) = -0.7773433899855132 ==> log prob of sentence so far: -20.86345462195964

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: the
ENGLISH: P(e|th) = -0.2044551165913694 ==> log prob of sentence so far: -0.2044551165913694
FRENCH: P(e|th) = -0.37814519438647765 ==> log prob of sentence so far: -0.37814519438647765
DUTCH: P(e|th) = -0.4416682808051838 ==> log prob of sentence so far: -0.4416682808051838
PORTUGUESE: P(e|th) = -0.2581774957467133 ==> log prob of sentence so far: -0.2581774957467133
SPANISH: P(e|th) = -1.5797835966168101 ==> log prob of sentence so far: -1.5797835966168101

3GRAM: hey
ENGLISH: P(y|he) = -1.5298214672473125 ==> log prob of sentence so far: -1.7342765838386818
FRENCH: P(y|he) = -3.87633332908638 ==> log prob of sentence so far: -4.254478523472858
DUTCH: P(y|he) = -4.283346465356047 ==> log prob of sentence so far: -4.72501474616123
PORTUGUESE: P(y|he) = -3.555578072772955 ==> log prob of sentence so far: -3.8137555685196682
SPANISH: P(y|he) = -1.6444904176774475 ==> log prob of sentence so far: -3.2242740142942576

3GRAM: eya
ENGLISH: P(a|ey) = -1.0561625251047835 ==> log prob of sentence so far: -2.790439108943465
FRENCH: P(a|ey) = -0.8715729355458788 ==> log prob of sentence so far: -5.126051459018736
DUTCH: P(a|ey) = -1.5797835966168101 ==> log prob of sentence so far: -6.3047983427780405
PORTUGUESE: P(a|ey) = -1.9138138523837167 ==> log prob of sentence so far: -5.727569420903385
SPANISH: P(a|ey) = -0.9126685472329067 ==> log prob of sentence so far: -4.136942561527165

3GRAM: yar
ENGLISH: P(r|ya) = -0.8511443556788557 ==> log prob of sentence so far: -3.641583464622321
FRENCH: P(r|ya) = -1.696567262484454 ==> log prob of sentence so far: -6.82261872150319
DUTCH: P(r|ya) = -1.7634279935629373 ==> log prob of sentence so far: -8.068226336340977
PORTUGUESE: P(r|ya) = -1.5797835966168101 ==> log prob of sentence so far: -7.307353017520195
SPANISH: P(r|ya) = -1.514313315687304 ==> log prob of sentence so far: -5.651255877214469

3GRAM: are
ENGLISH: P(e|ar) = -0.8593398433782986 ==> log prob of sentence so far: -4.500923308000619
FRENCH: P(e|ar) = -1.0008882932254402 ==> log prob of sentence so far: -7.82350701472863
DUTCH: P(e|ar) = -0.7230912542715829 ==> log prob of sentence so far: -8.791317590612561
PORTUGUESE: P(e|ar) = -0.7943968861295941 ==> log prob of sentence so far: -8.10174990364979
SPANISH: P(e|ar) = -0.8955383674729839 ==> log prob of sentence so far: -6.546794244687453

3GRAM: reb
ENGLISH: P(b|re) = -1.652215559590858 ==> log prob of sentence so far: -6.153138867591477
FRENCH: P(b|re) = -2.3683362272745945 ==> log prob of sentence so far: -10.191843242003225
DUTCH: P(b|re) = -2.2926808092699424 ==> log prob of sentence so far: -11.083998399882503
PORTUGUESE: P(b|re) = -2.3160987779528717 ==> log prob of sentence so far: -10.417848681602662
SPANISH: P(b|re) = -2.2097898844770616 ==> log prob of sentence so far: -8.756584129164514

3GRAM: eba
ENGLISH: P(a|eb) = -1.1254084981272259 ==> log prob of sentence so far: -7.278547365718703
FRENCH: P(a|eb) = -0.5033015950350987 ==> log prob of sentence so far: -10.695144837038324
DUTCH: P(a|eb) = -1.2558967157488943 ==> log prob of sentence so far: -12.339895115631398
PORTUGUESE: P(a|eb) = -0.6369732353934338 ==> log prob of sentence so far: -11.054821916996096
SPANISH: P(a|eb) = -0.5703699486500132 ==> log prob of sentence so far: -9.326954077814527

3GRAM: bad
ENGLISH: P(d|ba) = -1.4021341464997363 ==> log prob of sentence so far: -8.68068151221844
FRENCH: P(d|ba) = -2.61419390497756 ==> log prob of sentence so far: -13.309338742015884
DUTCH: P(d|ba) = -1.2026763431596244 ==> log prob of sentence so far: -13.542571458791022
PORTUGUESE: P(d|ba) = -1.5521859345724551 ==> log prob of sentence so far: -12.60700785156855
SPANISH: P(d|ba) = -1.3421937432934976 ==> log prob of sentence so far: -10.669147821108025

3GRAM: adh
ENGLISH: P(h|ad) = -1.6379005882408186 ==> log prob of sentence so far: -10.318582100459258
FRENCH: P(h|ad) = -1.9281607080831755 ==> log prob of sentence so far: -15.23749945009906
DUTCH: P(h|ad) = -1.4445264046150195 ==> log prob of sentence so far: -14.987097863406042
PORTUGUESE: P(h|ad) = -3.341711435037339 ==> log prob of sentence so far: -15.94871928660589
SPANISH: P(h|ad) = -2.827110687466011 ==> log prob of sentence so far: -13.496258508574035

3GRAM: dho
ENGLISH: P(o|dh) = -0.9337399121491934 ==> log prob of sentence so far: -11.25232201260845
FRENCH: P(o|dh) = -0.6240757311456826 ==> log prob of sentence so far: -15.861575181244742
DUTCH: P(o|dh) = -1.0849868114352692 ==> log prob of sentence so far: -16.07208467484131
PORTUGUESE: P(o|dh) = -1.4771212547196624 ==> log prob of sentence so far: -17.425840541325552
SPANISH: P(o|dh) = -1.1702617153949573 ==> log prob of sentence so far: -14.666520223968993

3GRAM: hom
ENGLISH: P(m|ho) = -1.4924889570356539 ==> log prob of sentence so far: -12.744810969644105
FRENCH: P(m|ho) = -0.5182309723644262 ==> log prob of sentence so far: -16.379806153609167
DUTCH: P(m|ho) = -2.056227060357067 ==> log prob of sentence so far: -18.128311735198377
PORTUGUESE: P(m|ho) = -1.0364430933583295 ==> log prob of sentence so far: -18.46228363468388
SPANISH: P(m|ho) = -0.8564022889058689 ==> log prob of sentence so far: -15.522922512874862

3GRAM: omb
ENGLISH: P(b|om) = -1.7947447977644326 ==> log prob of sentence so far: -14.539555767408537
FRENCH: P(b|om) = -0.9388359588376614 ==> log prob of sentence so far: -17.318642112446827
DUTCH: P(b|om) = -1.531796151886093 ==> log prob of sentence so far: -19.66010788708447
PORTUGUESE: P(b|om) = -1.6190733497437642 ==> log prob of sentence so far: -20.081356984427646
SPANISH: P(b|om) = -1.0125484420785278 ==> log prob of sentence so far: -16.53547095495339

3GRAM: mbr
ENGLISH: P(r|mb) = -1.2959504701347062 ==> log prob of sentence so far: -15.835506237543242
FRENCH: P(r|mb) = -0.38062197758945804 ==> log prob of sentence so far: -17.699264090036287
DUTCH: P(r|mb) = -1.8893700361516252 ==> log prob of sentence so far: -21.549477923236097
PORTUGUESE: P(r|mb) = -0.5934989434327352 ==> log prob of sentence so far: -20.67485592786038
SPANISH: P(r|mb) = -0.3718610346817453 ==> log prob of sentence so far: -16.907331989635136

3GRAM: bre
ENGLISH: P(e|br) = -0.5805633996667262 ==> log prob of sentence so far: -16.41606963720997
FRENCH: P(e|br) = -0.37305631082386936 ==> log prob of sentence so far: -18.072320400860157
DUTCH: P(e|br) = -0.5783221443731765 ==> log prob of sentence so far: -22.12780006760927
PORTUGUESE: P(e|br) = -0.5272545453406255 ==> log prob of sentence so far: -21.202110473201007
SPANISH: P(e|br) = -0.28919159366286784 ==> log prob of sentence so far: -17.196523583298003

3GRAM: res
ENGLISH: P(s|re) = -0.8766546120147596 ==> log prob of sentence so far: -17.29272424922473
FRENCH: P(s|re) = -0.6177176473454268 ==> log prob of sentence so far: -18.690038048205583
DUTCH: P(s|re) = -1.9281169647767247 ==> log prob of sentence so far: -24.055917032385995
PORTUGUESE: P(s|re) = -0.6047220123049395 ==> log prob of sentence so far: -21.806832485505947
SPANISH: P(s|re) = -0.6500980516432519 ==> log prob of sentence so far: -17.846621634941254

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: they
ENGLISH: P(y|the) = -1.4142417696954939 ==> log prob of sentence so far: -1.4142417696954939
FRENCH: P(y|the) = -2.790988475088816 ==> log prob of sentence so far: -2.790988475088816
DUTCH: P(y|the) = -3.163757523981956 ==> log prob of sentence so far: -3.163757523981956
PORTUGUESE: P(y|the) = -2.2405492482826 ==> log prob of sentence so far: -2.2405492482826
SPANISH: P(y|the) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874

4GRAM: heya
ENGLISH: P(a|hey) = -0.8325089127062363 ==> log prob of sentence so far: -2.2467506824017303
FRENCH: P(a|hey) = -1.4313637641589874 ==> log prob of sentence so far: -4.222352239247803
DUTCH: P(a|hey) = -1.4313637641589874 ==> log prob of sentence so far: -4.595121288140943
PORTUGUESE: P(a|hey) = -1.4313637641589874 ==> log prob of sentence so far: -3.6719130124415873
SPANISH: P(a|hey) = -1.8920946026904804 ==> log prob of sentence so far: -3.3234583668494677

4GRAM: eyar
ENGLISH: P(r|eya) = -0.18090241031643642 ==> log prob of sentence so far: -2.4276530927181668
FRENCH: P(r|eya) = -1.6989700043360187 ==> log prob of sentence so far: -5.9213222435838215
DUTCH: P(r|eya) = -1.4313637641589874 ==> log prob of sentence so far: -6.026485052299931
PORTUGUESE: P(r|eya) = -1.4313637641589874 ==> log prob of sentence so far: -5.103276776600575
SPANISH: P(r|eya) = -1.655138434811382 ==> log prob of sentence so far: -4.978596801660849

4GRAM: yare
ENGLISH: P(e|yar) = -0.4125730599712018 ==> log prob of sentence so far: -2.8402261526893686
FRENCH: P(e|yar) = -1.7323937598229686 ==> log prob of sentence so far: -7.653716003406791
DUTCH: P(e|yar) = -1.4313637641589874 ==> log prob of sentence so far: -7.4578488164589185
PORTUGUESE: P(e|yar) = -1.4313637641589874 ==> log prob of sentence so far: -6.534640540759563
SPANISH: P(e|yar) = -0.6273658565927326 ==> log prob of sentence so far: -5.605962658253582

4GRAM: areb
ENGLISH: P(b|are) = -1.4688289840567128 ==> log prob of sentence so far: -4.309055136746082
FRENCH: P(b|are) = -2.636688447953283 ==> log prob of sentence so far: -10.290404451360073
DUTCH: P(b|are) = -2.054459837239404 ==> log prob of sentence so far: -9.512308653698323
PORTUGUESE: P(b|are) = -3.2773799746672547 ==> log prob of sentence so far: -9.812020515426818
SPANISH: P(b|are) = -2.7048366062114035 ==> log prob of sentence so far: -8.310799264464986

4GRAM: reba
ENGLISH: P(a|reb) = -1.703905222011531 ==> log prob of sentence so far: -6.012960358757613
FRENCH: P(a|reb) = -0.7781512503836436 ==> log prob of sentence so far: -11.068555701743717
DUTCH: P(a|reb) = -1.9912260756924949 ==> log prob of sentence so far: -11.503534729390818
PORTUGUESE: P(a|reb) = -0.9149892102916513 ==> log prob of sentence so far: -10.72700972571847
SPANISH: P(a|reb) = -0.49907583060771277 ==> log prob of sentence so far: -8.809875095072698

4GRAM: ebad
ENGLISH: P(d|eba) = -1.7421810919354637 ==> log prob of sentence so far: -7.755141450693076
FRENCH: P(d|eba) = -3.04688519083771 ==> log prob of sentence so far: -14.115440892581427
DUTCH: P(d|eba) = -2.5237464668115646 ==> log prob of sentence so far: -14.027281196202383
PORTUGUESE: P(d|eba) = -2.390935107103379 ==> log prob of sentence so far: -13.117944832821848
SPANISH: P(d|eba) = -1.3112491608456673 ==> log prob of sentence so far: -10.121124255918366

4GRAM: badh
ENGLISH: P(h|bad) = -2.2600713879850747 ==> log prob of sentence so far: -10.015212838678151
FRENCH: P(h|bad) = -1.5314789170422551 ==> log prob of sentence so far: -15.646919809623682
DUTCH: P(h|bad) = -2.1398790864012365 ==> log prob of sentence so far: -16.167160282603618
PORTUGUESE: P(h|bad) = -1.8920946026904804 ==> log prob of sentence so far: -15.01003943551233
SPANISH: P(h|bad) = -2.568201724066995 ==> log prob of sentence so far: -12.68932597998536

4GRAM: adho
ENGLISH: P(o|adh) = -0.9396541476288359 ==> log prob of sentence so far: -10.954866986306987
FRENCH: P(o|adh) = -1.235528446907549 ==> log prob of sentence so far: -16.88244825653123
DUTCH: P(o|adh) = -1.399865929708076 ==> log prob of sentence so far: -17.567026212311696
PORTUGUESE: P(o|adh) = -1.4771212547196624 ==> log prob of sentence so far: -16.487160690231992
SPANISH: P(o|adh) = -1.0 ==> log prob of sentence so far: -13.68932597998536

4GRAM: dhom
ENGLISH: P(m|dho) = -1.3101459566574323 ==> log prob of sentence so far: -12.26501294296442
FRENCH: P(m|dho) = -0.3907614240449142 ==> log prob of sentence so far: -17.273209680576144
DUTCH: P(m|dho) = -2.2695129442179165 ==> log prob of sentence so far: -19.83653915652961
PORTUGUESE: P(m|dho) = -1.4313637641589874 ==> log prob of sentence so far: -17.91852445439098
SPANISH: P(m|dho) = -1.4771212547196624 ==> log prob of sentence so far: -15.166447234705023

4GRAM: homb
ENGLISH: P(b|hom) = -2.8102325179950842 ==> log prob of sentence so far: -15.075245460959504
FRENCH: P(b|hom) = -2.046625212091902 ==> log prob of sentence so far: -19.319834892668048
DUTCH: P(b|hom) = -1.8195439355418686 ==> log prob of sentence so far: -21.65608309207148
PORTUGUESE: P(b|hom) = -1.01838558060476 ==> log prob of sentence so far: -18.93691003499574
SPANISH: P(b|hom) = -0.08141988235442114 ==> log prob of sentence so far: -15.247867117059444

4GRAM: ombr
ENGLISH: P(r|omb) = -2.4800069429571505 ==> log prob of sentence so far: -17.555252403916654
FRENCH: P(r|omb) = -0.26750297809917395 ==> log prob of sentence so far: -19.58733787076722
DUTCH: P(r|omb) = -1.146128035678238 ==> log prob of sentence so far: -22.802211127749718
PORTUGUESE: P(r|omb) = -0.28643048709369273 ==> log prob of sentence so far: -19.223340522089433
SPANISH: P(r|omb) = -0.0791812460476248 ==> log prob of sentence so far: -15.32704836310707

4GRAM: mbre
ENGLISH: P(e|mbr) = -0.8653014261025438 ==> log prob of sentence so far: -18.420553830019198
FRENCH: P(e|mbr) = -0.08558031958962954 ==> log prob of sentence so far: -19.67291819035685
DUTCH: P(e|mbr) = -0.8653014261025438 ==> log prob of sentence so far: -23.667512553852262
PORTUGUESE: P(e|mbr) = -0.9006705128685306 ==> log prob of sentence so far: -20.124011034957963
SPANISH: P(e|mbr) = -0.12318603237104617 ==> log prob of sentence so far: -15.450234395478116

4GRAM: bres
ENGLISH: P(s|bre) = -1.3679767852945943 ==> log prob of sentence so far: -19.788530615313793
FRENCH: P(s|bre) = -0.5034657042362025 ==> log prob of sentence so far: -20.17638389459305
DUTCH: P(s|bre) = -2.733999286538387 ==> log prob of sentence so far: -26.40151184039065
PORTUGUESE: P(s|bre) = -0.9015135987569886 ==> log prob of sentence so far: -21.02552463371495
SPANISH: P(s|bre) = -0.679310810719311 ==> log prob of sentence so far: -16.129545206197427

According to the 4gram model, the sentence is in Spanish
----------------
