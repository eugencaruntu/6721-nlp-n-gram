Ik hou van Mary.

1GRAM MODEL:

1GRAM: i
ENGLISH: P(i) = -1.1625241810080462 ==> log prob of sentence so far: -1.1625241810080462
FRENCH: P(i) = -1.1352045087261324 ==> log prob of sentence so far: -1.1352045087261324
DUTCH: P(i) = -1.2262466346889027 ==> log prob of sentence so far: -1.2262466346889027
PORTUGUESE: P(i) = -1.239694755342647 ==> log prob of sentence so far: -1.239694755342647
SPANISH: P(i) = -1.2231517024908503 ==> log prob of sentence so far: -1.2231517024908503

1GRAM: k
ENGLISH: P(k) = -2.0727076081017395 ==> log prob of sentence so far: -3.2352317891097857
FRENCH: P(k) = -3.53712924327307 ==> log prob of sentence so far: -4.6723337519992025
DUTCH: P(k) = -1.63254114436198 ==> log prob of sentence so far: -2.858787779050883
PORTUGUESE: P(k) = -4.726058549184086 ==> log prob of sentence so far: -5.965753304526733
SPANISH: P(k) = -3.821645110163466 ==> log prob of sentence so far: -5.044796812654316

1GRAM: h
ENGLISH: P(h) = -1.1802729318845735 ==> log prob of sentence so far: -4.41550472099436
FRENCH: P(h) = -2.1056456076029977 ==> log prob of sentence so far: -6.777979359602201
DUTCH: P(h) = -1.4907869107844225 ==> log prob of sentence so far: -4.349574689835306
PORTUGUESE: P(h) = -1.8092331706505171 ==> log prob of sentence so far: -7.774986475177251
SPANISH: P(h) = -1.8922261844491735 ==> log prob of sentence so far: -6.937022997103489

1GRAM: o
ENGLISH: P(o) = -1.1377823700508483 ==> log prob of sentence so far: -5.553287091045208
FRENCH: P(o) = -1.2743422038818786 ==> log prob of sentence so far: -8.05232156348408
DUTCH: P(o) = -1.1998170018851302 ==> log prob of sentence so far: -5.549391691720436
PORTUGUESE: P(o) = -0.9821385704267878 ==> log prob of sentence so far: -8.757125045604038
SPANISH: P(o) = -0.9966304259011738 ==> log prob of sentence so far: -7.933653423004663

1GRAM: u
ENGLISH: P(u) = -1.552489266886126 ==> log prob of sentence so far: -7.105776357931334
FRENCH: P(u) = -1.2061335065897725 ==> log prob of sentence so far: -9.258455070073852
DUTCH: P(u) = -1.745350663417757 ==> log prob of sentence so far: -7.294742355138192
PORTUGUESE: P(u) = -1.3141913678697503 ==> log prob of sentence so far: -10.071316413473788
SPANISH: P(u) = -1.3656709062006245 ==> log prob of sentence so far: -9.299324329205287

1GRAM: v
ENGLISH: P(v) = -2.043628928875231 ==> log prob of sentence so far: -9.149405286806566
FRENCH: P(v) = -1.8278487453889154 ==> log prob of sentence so far: -11.086303815462767
DUTCH: P(v) = -1.6100015762152238 ==> log prob of sentence so far: -8.904743931353416
PORTUGUESE: P(v) = -1.7546462542850974 ==> log prob of sentence so far: -11.825962667758885
SPANISH: P(v) = -1.9461124706597488 ==> log prob of sentence so far: -11.245436799865036

1GRAM: a
ENGLISH: P(a) = -1.086787118026177 ==> log prob of sentence so far: -10.236192404832742
FRENCH: P(a) = -1.076350890171086 ==> log prob of sentence so far: -12.162654705633853
DUTCH: P(a) = -1.1153901057578817 ==> log prob of sentence so far: -10.020134037111298
PORTUGUESE: P(a) = -0.8318956635743947 ==> log prob of sentence so far: -12.65785833133328
SPANISH: P(a) = -0.9020204392084481 ==> log prob of sentence so far: -12.147457239073484

1GRAM: n
ENGLISH: P(n) = -1.1615956346167673 ==> log prob of sentence so far: -11.397788039449509
FRENCH: P(n) = -1.1226354816746384 ==> log prob of sentence so far: -13.285290187308492
DUTCH: P(n) = -0.9654220611247865 ==> log prob of sentence so far: -10.985556098236085
PORTUGUESE: P(n) = -1.3242830793492986 ==> log prob of sentence so far: -13.982141410682578
SPANISH: P(n) = -1.1498358464017453 ==> log prob of sentence so far: -13.297293085475228

1GRAM: m
ENGLISH: P(m) = -1.6111121655313874 ==> log prob of sentence so far: -13.008900204980897
FRENCH: P(m) = -1.512949635898697 ==> log prob of sentence so far: -14.798239823207188
DUTCH: P(m) = -1.6188687884896773 ==> log prob of sentence so far: -12.604424886725763
PORTUGUESE: P(m) = -1.3143893368677781 ==> log prob of sentence so far: -15.296530747550356
SPANISH: P(m) = -1.497083341850591 ==> log prob of sentence so far: -14.794376427325819

1GRAM: a
ENGLISH: P(a) = -1.086787118026177 ==> log prob of sentence so far: -14.095687323007073
FRENCH: P(a) = -1.076350890171086 ==> log prob of sentence so far: -15.874590713378275
DUTCH: P(a) = -1.1153901057578817 ==> log prob of sentence so far: -13.719814992483645
PORTUGUESE: P(a) = -0.8318956635743947 ==> log prob of sentence so far: -16.12842641112475
SPANISH: P(a) = -0.9020204392084481 ==> log prob of sentence so far: -15.696396866534267

1GRAM: r
ENGLISH: P(r) = -1.2612720228109857 ==> log prob of sentence so far: -15.356959345818058
FRENCH: P(r) = -1.1840269595391562 ==> log prob of sentence so far: -17.058617672917432
DUTCH: P(r) = -1.254200750493856 ==> log prob of sentence so far: -14.974015742977501
PORTUGUESE: P(r) = -1.1945420650398737 ==> log prob of sentence so far: -17.322968476164622
SPANISH: P(r) = -1.1910423872739788 ==> log prob of sentence so far: -16.887439253808246

1GRAM: y
ENGLISH: P(y) = -1.7506000117511689 ==> log prob of sentence so far: -17.107559357569226
FRENCH: P(y) = -2.6722939958631318 ==> log prob of sentence so far: -19.730911668780564
DUTCH: P(y) = -3.569878686206462 ==> log prob of sentence so far: -18.543894429183965
PORTUGUESE: P(y) = -3.344784162555018 ==> log prob of sentence so far: -20.66775263871964
SPANISH: P(y) = -1.9106442581412726 ==> log prob of sentence so far: -18.798083511949518

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: ik
ENGLISH: P(k|i) = -1.8747162321111424 ==> log prob of sentence so far: -1.8747162321111424
FRENCH: P(k|i) = -3.398038652017502 ==> log prob of sentence so far: -3.398038652017502
DUTCH: P(k|i) = -1.645859041419517 ==> log prob of sentence so far: -1.645859041419517
PORTUGUESE: P(k|i) = -4.714547690703997 ==> log prob of sentence so far: -4.714547690703997
SPANISH: P(k|i) = -4.241416969813827 ==> log prob of sentence so far: -4.241416969813827

2GRAM: kh
ENGLISH: P(h|k) = -1.7341186985337669 ==> log prob of sentence so far: -3.6088349306449095
FRENCH: P(h|k) = -1.7966810525823644 ==> log prob of sentence so far: -5.1947197045998665
DUTCH: P(h|k) = -1.5631452354563338 ==> log prob of sentence so far: -3.209004276875851
PORTUGUESE: P(h|k) = -1.5797835966168101 ==> log prob of sentence so far: -6.294331287320807
SPANISH: P(h|k) = -1.3921104650113136 ==> log prob of sentence so far: -5.63352743482514

2GRAM: ho
ENGLISH: P(o|h) = -1.1036467362119045 ==> log prob of sentence so far: -4.712481666856814
FRENCH: P(o|h) = -0.8327964879250233 ==> log prob of sentence so far: -6.02751619252489
DUTCH: P(o|h) = -1.017442148638513 ==> log prob of sentence so far: -4.226446425514364
PORTUGUESE: P(o|h) = -0.5404521724327865 ==> log prob of sentence so far: -6.834783459753593
SPANISH: P(o|h) = -0.5331756089960642 ==> log prob of sentence so far: -6.1667030438212045

2GRAM: ou
ENGLISH: P(u|o) = -0.9046111932070653 ==> log prob of sentence so far: -5.617092860063879
FRENCH: P(u|o) = -0.6277251104704404 ==> log prob of sentence so far: -6.65524130299533
DUTCH: P(u|o) = -1.4239954767239777 ==> log prob of sentence so far: -5.650441902238342
PORTUGUESE: P(u|o) = -1.1236880746694284 ==> log prob of sentence so far: -7.958471534423022
SPANISH: P(u|o) = -2.007183725088882 ==> log prob of sentence so far: -8.173886768910087

2GRAM: uv
ENGLISH: P(v|u) = -2.9764920001191637 ==> log prob of sentence so far: -8.593584860183043
FRENCH: P(v|u) = -1.5222524803847453 ==> log prob of sentence so far: -8.177493783380076
DUTCH: P(v|u) = -2.185176503650779 ==> log prob of sentence so far: -7.835618405889121
PORTUGUESE: P(v|u) = -1.7525254600451066 ==> log prob of sentence so far: -9.710996994468129
SPANISH: P(v|u) = -2.1418752725385852 ==> log prob of sentence so far: -10.315762041448671

2GRAM: va
ENGLISH: P(a|v) = -1.0604286346750382 ==> log prob of sentence so far: -9.65401349485808
FRENCH: P(a|v) = -0.6558713438300514 ==> log prob of sentence so far: -8.833365127210127
DUTCH: P(a|v) = -0.5210709023865397 ==> log prob of sentence so far: -8.35668930827566
PORTUGUESE: P(a|v) = -0.5482519651782581 ==> log prob of sentence so far: -10.259248959646387
SPANISH: P(a|v) = -0.7125000248533472 ==> log prob of sentence so far: -11.028262066302018

2GRAM: an
ENGLISH: P(n|a) = -0.7044992903650373 ==> log prob of sentence so far: -10.358512785223118
FRENCH: P(n|a) = -0.8167796096083183 ==> log prob of sentence so far: -9.650144736818445
DUTCH: P(n|a) = -0.5909426718422803 ==> log prob of sentence so far: -8.94763198011794
PORTUGUESE: P(n|a) = -1.1303695606618018 ==> log prob of sentence so far: -11.389618520308188
SPANISH: P(n|a) = -0.8951102737403667 ==> log prob of sentence so far: -11.923372340042384

2GRAM: nm
ENGLISH: P(m|n) = -2.108447829244744 ==> log prob of sentence so far: -12.466960614467862
FRENCH: P(m|n) = -1.9864731474673378 ==> log prob of sentence so far: -11.636617884285783
DUTCH: P(m|n) = -1.5290745338092098 ==> log prob of sentence so far: -10.47670651392715
PORTUGUESE: P(m|n) = -3.9334670405477437 ==> log prob of sentence so far: -15.323085560855931
SPANISH: P(m|n) = -1.6356884602963113 ==> log prob of sentence so far: -13.559060800338695

2GRAM: ma
ENGLISH: P(a|m) = -0.703729378678151 ==> log prob of sentence so far: -13.170689993146013
FRENCH: P(a|m) = -0.7371308835829817 ==> log prob of sentence so far: -12.373748767868765
DUTCH: P(a|m) = -0.7533755388283183 ==> log prob of sentence so far: -11.230082052755469
PORTUGUESE: P(a|m) = -0.5415691182898056 ==> log prob of sentence so far: -15.864654679145737
SPANISH: P(a|m) = -0.6661732589450952 ==> log prob of sentence so far: -14.22523405928379

2GRAM: ar
ENGLISH: P(r|a) = -0.9854428440622539 ==> log prob of sentence so far: -14.156132837208267
FRENCH: P(r|a) = -1.0331636760797833 ==> log prob of sentence so far: -13.40691244394855
DUTCH: P(r|a) = -0.9128098519981566 ==> log prob of sentence so far: -12.142891904753625
PORTUGUESE: P(r|a) = -1.0106275820610928 ==> log prob of sentence so far: -16.87528226120683
SPANISH: P(r|a) = -0.9443875898070365 ==> log prob of sentence so far: -15.169621649090827

2GRAM: ry
ENGLISH: P(y|r) = -1.4566403431785555 ==> log prob of sentence so far: -15.612773180386823
FRENCH: P(y|r) = -3.1449577609125163 ==> log prob of sentence so far: -16.551870204861064
DUTCH: P(y|r) = -4.254973221361755 ==> log prob of sentence so far: -16.39786512611538
PORTUGUESE: P(y|r) = -3.288372591544311 ==> log prob of sentence so far: -20.163654852751144
SPANISH: P(y|r) = -2.199351928226504 ==> log prob of sentence so far: -17.36897357731733

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: ikh
ENGLISH: P(h|ik) = -3.5482665451707454 ==> log prob of sentence so far: -3.5482665451707454
FRENCH: P(h|ik) = -2.0253058652647704 ==> log prob of sentence so far: -2.0253058652647704
DUTCH: P(h|ik) = -1.3886678687748433 ==> log prob of sentence so far: -1.3886678687748433
PORTUGUESE: P(h|ik) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
SPANISH: P(h|ik) = -1.4771212547196624 ==> log prob of sentence so far: -1.4771212547196624

3GRAM: kho
ENGLISH: P(o|kh) = -1.1668126345380891 ==> log prob of sentence so far: -4.715079179708835
FRENCH: P(o|kh) = -1.5797835966168101 ==> log prob of sentence so far: -3.6050894618815805
DUTCH: P(o|kh) = -1.3309932190414244 ==> log prob of sentence so far: -2.7196610878162675
PORTUGUESE: P(o|kh) = -1.4313637641589874 ==> log prob of sentence so far: -2.8627275283179747
SPANISH: P(o|kh) = -1.5314789170422551 ==> log prob of sentence so far: -3.0086001717619175

3GRAM: hou
ENGLISH: P(u|ho) = -0.5113483830429988 ==> log prob of sentence so far: -5.226427562751834
FRENCH: P(u|ho) = -1.0914850287449183 ==> log prob of sentence so far: -4.696574490626499
DUTCH: P(u|ho) = -0.7704197976837444 ==> log prob of sentence so far: -3.490080885500012
PORTUGUESE: P(u|ho) = -1.0412820791517943 ==> log prob of sentence so far: -3.904009607469769
SPANISH: P(u|ho) = -2.120413408386834 ==> log prob of sentence so far: -5.129013580148751

3GRAM: ouv
ENGLISH: P(v|ou) = -3.3024327980291233 ==> log prob of sentence so far: -8.528860360780957
FRENCH: P(v|ou) = -0.9556083149917155 ==> log prob of sentence so far: -5.652182805618214
DUTCH: P(v|ou) = -1.5824908956607333 ==> log prob of sentence so far: -5.072571781160745
PORTUGUESE: P(v|ou) = -1.1262352057346903 ==> log prob of sentence so far: -5.030244813204459
SPANISH: P(v|ou) = -3.1492191126553797 ==> log prob of sentence so far: -8.278232692804131

3GRAM: uva
ENGLISH: P(a|uv) = -2.1398790864012365 ==> log prob of sentence so far: -10.668739447182194
FRENCH: P(a|uv) = -0.5965845728980701 ==> log prob of sentence so far: -6.248767378516284
DUTCH: P(a|uv) = -1.2245131412977681 ==> log prob of sentence so far: -6.297084922458513
PORTUGUESE: P(a|uv) = -0.8764377922205034 ==> log prob of sentence so far: -5.906682605424963
SPANISH: P(a|uv) = -1.448978975233845 ==> log prob of sentence so far: -9.727211668037976

3GRAM: van
ENGLISH: P(n|va) = -0.7998705000768799 ==> log prob of sentence so far: -11.468609947259074
FRENCH: P(n|va) = -0.6912781641651391 ==> log prob of sentence so far: -6.940045542681423
DUTCH: P(n|va) = -0.06808905529510015 ==> log prob of sentence so far: -6.365173977753614
PORTUGUESE: P(n|va) = -1.2039181260490357 ==> log prob of sentence so far: -7.1106007314739985
SPANISH: P(n|va) = -0.670643571144396 ==> log prob of sentence so far: -10.397855239182372

3GRAM: anm
ENGLISH: P(m|an) = -2.468433791587419 ==> log prob of sentence so far: -13.937043738846493
FRENCH: P(m|an) = -2.894881950824089 ==> log prob of sentence so far: -9.834927493505512
DUTCH: P(m|an) = -1.6811570636216842 ==> log prob of sentence so far: -8.046331041375298
PORTUGUESE: P(m|an) = -3.9759829437125465 ==> log prob of sentence so far: -11.086583675186546
SPANISH: P(m|an) = -1.9470516509586266 ==> log prob of sentence so far: -12.344906890140997

3GRAM: nma
ENGLISH: P(a|nm) = -0.4456234202687512 ==> log prob of sentence so far: -14.382667159115243
FRENCH: P(a|nm) = -0.5215465525734556 ==> log prob of sentence so far: -10.356474046078969
DUTCH: P(a|nm) = -0.5342927223977645 ==> log prob of sentence so far: -8.580623763773062
PORTUGUESE: P(a|nm) = -1.4771212547196624 ==> log prob of sentence so far: -12.563704929906208
SPANISH: P(a|nm) = -0.7171816912646641 ==> log prob of sentence so far: -13.06208858140566

3GRAM: mar
ENGLISH: P(r|ma) = -1.1131103444138777 ==> log prob of sentence so far: -15.495777503529121
FRENCH: P(r|ma) = -0.8409680744490241 ==> log prob of sentence so far: -11.197442120527993
DUTCH: P(r|ma) = -1.4194881450114618 ==> log prob of sentence so far: -10.000111908784524
PORTUGUESE: P(r|ma) = -0.7471619235284117 ==> log prob of sentence so far: -13.31086685343462
SPANISH: P(r|ma) = -0.8674618091310761 ==> log prob of sentence so far: -13.929550390536736

3GRAM: ary
ENGLISH: P(y|ar) = -1.5219792741644 ==> log prob of sentence so far: -17.01775677769352
FRENCH: P(y|ar) = -3.634154704380865 ==> log prob of sentence so far: -14.831596824908859
DUTCH: P(y|ar) = -4.17533782879202 ==> log prob of sentence so far: -14.175449737576542
PORTUGUESE: P(y|ar) = -4.070628844051428 ==> log prob of sentence so far: -17.38149569748605
SPANISH: P(y|ar) = -2.158318461631188 ==> log prob of sentence so far: -16.087868852167922

According to the 3gram model, the sentence is in Dutch
----------------
4GRAM MODEL:

4GRAM: ikho
ENGLISH: P(o|ikh) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
FRENCH: P(o|ikh) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
DUTCH: P(o|ikh) = -1.1026623418971477 ==> log prob of sentence so far: -1.1026623418971477
PORTUGUESE: P(o|ikh) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
SPANISH: P(o|ikh) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874

4GRAM: khou
ENGLISH: P(u|kho) = -1.8195439355418686 ==> log prob of sentence so far: -3.250907699700856
FRENCH: P(u|kho) = -1.4313637641589874 ==> log prob of sentence so far: -2.8627275283179747
DUTCH: P(u|kho) = -0.7552883674241394 ==> log prob of sentence so far: -1.8579507093212873
PORTUGUESE: P(u|kho) = -1.4313637641589874 ==> log prob of sentence so far: -2.8627275283179747
SPANISH: P(u|kho) = -1.4313637641589874 ==> log prob of sentence so far: -2.8627275283179747

4GRAM: houv
ENGLISH: P(v|hou) = -3.7797407511767407 ==> log prob of sentence so far: -7.030648450877597
FRENCH: P(v|hou) = -2.456366033129043 ==> log prob of sentence so far: -5.319093561447017
DUTCH: P(v|hou) = -2.2523675144598987 ==> log prob of sentence so far: -4.110318223781186
PORTUGUESE: P(v|hou) = -0.5860998679342353 ==> log prob of sentence so far: -3.4488273962522102
SPANISH: P(v|hou) = -1.8195439355418686 ==> log prob of sentence so far: -4.682271463859843

4GRAM: ouva
ENGLISH: P(a|ouv) = -1.6232492903979006 ==> log prob of sentence so far: -8.653897741275497
FRENCH: P(a|ouv) = -0.5506830852631365 ==> log prob of sentence so far: -5.869776646710154
DUTCH: P(a|ouv) = -1.4014005407815442 ==> log prob of sentence so far: -5.51171876456273
PORTUGUESE: P(a|ouv) = -1.7533276666586115 ==> log prob of sentence so far: -5.202155062910822
SPANISH: P(a|ouv) = -1.4313637641589874 ==> log prob of sentence so far: -6.113635228018831

4GRAM: uvan
ENGLISH: P(n|uva) = -1.4313637641589874 ==> log prob of sentence so far: -10.085261505434485
FRENCH: P(n|uva) = -1.0903260178923497 ==> log prob of sentence so far: -6.960102664602504
DUTCH: P(n|uva) = -0.4658402443099734 ==> log prob of sentence so far: -5.977559008872703
PORTUGUESE: P(n|uva) = -2.0253058652647704 ==> log prob of sentence so far: -7.227460928175592
SPANISH: P(n|uva) = -0.9242792860618817 ==> log prob of sentence so far: -7.037914514080713

4GRAM: vanm
ENGLISH: P(m|van) = -2.0051805125037805 ==> log prob of sentence so far: -12.090442017938265
FRENCH: P(m|van) = -3.274619619091238 ==> log prob of sentence so far: -10.234722283693742
DUTCH: P(m|van) = -1.545811572843039 ==> log prob of sentence so far: -7.523370581715742
PORTUGUESE: P(m|van) = -2.4683473304121573 ==> log prob of sentence so far: -9.69580825858775
SPANISH: P(m|van) = -1.727416118148018 ==> log prob of sentence so far: -8.76533063222873

4GRAM: anma
ENGLISH: P(a|anm) = -0.34053853694765485 ==> log prob of sentence so far: -12.43098055488592
FRENCH: P(a|anm) = -0.6146491186359829 ==> log prob of sentence so far: -10.849371402329725
DUTCH: P(a|anm) = -0.6768556900454465 ==> log prob of sentence so far: -8.200226271761188
PORTUGUESE: P(a|anm) = -1.4313637641589874 ==> log prob of sentence so far: -11.127172022746738
SPANISH: P(a|anm) = -0.5512598376479061 ==> log prob of sentence so far: -9.316590469876637

4GRAM: nmar
ENGLISH: P(r|nma) = -1.2241588320944303 ==> log prob of sentence so far: -13.65513938698035
FRENCH: P(r|nma) = -0.7484443235506133 ==> log prob of sentence so far: -11.597815725880338
DUTCH: P(r|nma) = -1.414973347970818 ==> log prob of sentence so far: -9.615199619732007
PORTUGUESE: P(r|nma) = -1.4313637641589874 ==> log prob of sentence so far: -12.558535786905725
SPANISH: P(r|nma) = -0.7500089708978261 ==> log prob of sentence so far: -10.066599440774462

4GRAM: mary
ENGLISH: P(y|mar) = -1.1341467383951238 ==> log prob of sentence so far: -14.789286125375474
FRENCH: P(y|mar) = -3.356790460351716 ==> log prob of sentence so far: -14.954606186232054
DUTCH: P(y|mar) = -2.462397997898956 ==> log prob of sentence so far: -12.077597617630962
PORTUGUESE: P(y|mar) = -3.29269900304393 ==> log prob of sentence so far: -15.851234789949656
SPANISH: P(y|mar) = -2.0151017075811213 ==> log prob of sentence so far: -12.081701148355585

According to the 4gram model, the sentence is in Dutch
----------------
