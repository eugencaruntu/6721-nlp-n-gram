Birds build nests.

1GRAM MODEL:

1GRAM: b
ENGLISH: P(b) = -1.7519554448616979 ==> log prob of sentence so far: -1.7519554448616979
FRENCH: P(b) = -2.0519017542708387 ==> log prob of sentence so far: -2.0519017542708387
DUTCH: P(b) = -1.8074783299428059 ==> log prob of sentence so far: -1.8074783299428059
PORTUGUESE: P(b) = -2.0434887868143994 ==> log prob of sentence so far: -2.0434887868143994
SPANISH: P(b) = -1.8063346755744631 ==> log prob of sentence so far: -1.8063346755744631

1GRAM: i
ENGLISH: P(i) = -1.1625241810080462 ==> log prob of sentence so far: -2.914479625869744
FRENCH: P(i) = -1.1352045087261324 ==> log prob of sentence so far: -3.187106262996971
DUTCH: P(i) = -1.2262466346889027 ==> log prob of sentence so far: -3.0337249646317086
PORTUGUESE: P(i) = -1.239694755342647 ==> log prob of sentence so far: -3.2831835421570466
SPANISH: P(i) = -1.2231517024908503 ==> log prob of sentence so far: -3.0294863780653136

1GRAM: r
ENGLISH: P(r) = -1.2612720228109857 ==> log prob of sentence so far: -4.17575164868073
FRENCH: P(r) = -1.1840269595391562 ==> log prob of sentence so far: -4.371133222536127
DUTCH: P(r) = -1.254200750493856 ==> log prob of sentence so far: -4.2879257151255645
PORTUGUESE: P(r) = -1.1945420650398737 ==> log prob of sentence so far: -4.47772560719692
SPANISH: P(r) = -1.1910423872739788 ==> log prob of sentence so far: -4.220528765339292

1GRAM: d
ENGLISH: P(d) = -1.3961797927180317 ==> log prob of sentence so far: -5.571931441398761
FRENCH: P(d) = -1.4202617278792675 ==> log prob of sentence so far: -5.791394950415395
DUTCH: P(d) = -1.2011844788481099 ==> log prob of sentence so far: -5.489110193973675
PORTUGUESE: P(d) = -1.3354039236187203 ==> log prob of sentence so far: -5.81312953081564
SPANISH: P(d) = -1.2907619213439092 ==> log prob of sentence so far: -5.511290686683202

1GRAM: s
ENGLISH: P(s) = -1.1711397464845998 ==> log prob of sentence so far: -6.743071187883361
FRENCH: P(s) = -1.0644454594623793 ==> log prob of sentence so far: -6.855840409877774
DUTCH: P(s) = -1.4604404489822762 ==> log prob of sentence so far: -6.949550642955951
PORTUGUESE: P(s) = -1.112894773130837 ==> log prob of sentence so far: -6.926024303946477
SPANISH: P(s) = -1.1432585839855418 ==> log prob of sentence so far: -6.654549270668744

1GRAM: b
ENGLISH: P(b) = -1.7519554448616979 ==> log prob of sentence so far: -8.49502663274506
FRENCH: P(b) = -2.0519017542708387 ==> log prob of sentence so far: -8.907742164148612
DUTCH: P(b) = -1.8074783299428059 ==> log prob of sentence so far: -8.757028972898757
PORTUGUESE: P(b) = -2.0434887868143994 ==> log prob of sentence so far: -8.969513090760877
SPANISH: P(b) = -1.8063346755744631 ==> log prob of sentence so far: -8.460883946243207

1GRAM: u
ENGLISH: P(u) = -1.552489266886126 ==> log prob of sentence so far: -10.047515899631186
FRENCH: P(u) = -1.2061335065897725 ==> log prob of sentence so far: -10.113875670738384
DUTCH: P(u) = -1.745350663417757 ==> log prob of sentence so far: -10.502379636316514
PORTUGUESE: P(u) = -1.3141913678697503 ==> log prob of sentence so far: -10.283704458630627
SPANISH: P(u) = -1.3656709062006245 ==> log prob of sentence so far: -9.82655485244383

1GRAM: i
ENGLISH: P(i) = -1.1625241810080462 ==> log prob of sentence so far: -11.210040080639232
FRENCH: P(i) = -1.1352045087261324 ==> log prob of sentence so far: -11.249080179464517
DUTCH: P(i) = -1.2262466346889027 ==> log prob of sentence so far: -11.728626271005416
PORTUGUESE: P(i) = -1.239694755342647 ==> log prob of sentence so far: -11.523399213973274
SPANISH: P(i) = -1.2231517024908503 ==> log prob of sentence so far: -11.049706554934682

1GRAM: l
ENGLISH: P(l) = -1.3477684628666193 ==> log prob of sentence so far: -12.557808543505852
FRENCH: P(l) = -1.2682113125567938 ==> log prob of sentence so far: -12.517291492021311
DUTCH: P(l) = -1.4248884156790762 ==> log prob of sentence so far: -13.153514686684492
PORTUGUESE: P(l) = -1.4370338796900037 ==> log prob of sentence so far: -12.960433093663278
SPANISH: P(l) = -1.2802183602331265 ==> log prob of sentence so far: -12.329924915167808

1GRAM: d
ENGLISH: P(d) = -1.3961797927180317 ==> log prob of sentence so far: -13.953988336223883
FRENCH: P(d) = -1.4202617278792675 ==> log prob of sentence so far: -13.937553219900579
DUTCH: P(d) = -1.2011844788481099 ==> log prob of sentence so far: -14.354699165532601
PORTUGUESE: P(d) = -1.3354039236187203 ==> log prob of sentence so far: -14.295837017281999
SPANISH: P(d) = -1.2907619213439092 ==> log prob of sentence so far: -13.620686836511718

1GRAM: n
ENGLISH: P(n) = -1.1615956346167673 ==> log prob of sentence so far: -15.11558397084065
FRENCH: P(n) = -1.1226354816746384 ==> log prob of sentence so far: -15.060188701575218
DUTCH: P(n) = -0.9654220611247865 ==> log prob of sentence so far: -15.320121226657388
PORTUGUESE: P(n) = -1.3242830793492986 ==> log prob of sentence so far: -15.620120096631297
SPANISH: P(n) = -1.1498358464017453 ==> log prob of sentence so far: -14.770522682913462

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -16.02565316309849
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -15.827151359728301
DUTCH: P(e) = -0.7296470912483529 ==> log prob of sentence so far: -16.04976831790574
PORTUGUESE: P(e) = -0.880446229569894 ==> log prob of sentence so far: -16.50056632620119
SPANISH: P(e) = -0.881649517549016 ==> log prob of sentence so far: -15.652172200462479

1GRAM: s
ENGLISH: P(s) = -1.1711397464845998 ==> log prob of sentence so far: -17.19679290958309
FRENCH: P(s) = -1.0644454594623793 ==> log prob of sentence so far: -16.89159681919068
DUTCH: P(s) = -1.4604404489822762 ==> log prob of sentence so far: -17.510208766888017
PORTUGUESE: P(s) = -1.112894773130837 ==> log prob of sentence so far: -17.613461099332028
SPANISH: P(s) = -1.1432585839855418 ==> log prob of sentence so far: -16.79543078444802

1GRAM: t
ENGLISH: P(t) = -1.0342219169534712 ==> log prob of sentence so far: -18.23101482653656
FRENCH: P(t) = -1.1659649710967117 ==> log prob of sentence so far: -18.057561790287394
DUTCH: P(t) = -1.2498818524344808 ==> log prob of sentence so far: -18.760090619322497
PORTUGUESE: P(t) = -1.3798321761018175 ==> log prob of sentence so far: -18.993293275433846
SPANISH: P(t) = -1.4035914062065482 ==> log prob of sentence so far: -18.199022190654567

1GRAM: s
ENGLISH: P(s) = -1.1711397464845998 ==> log prob of sentence so far: -19.402154573021157
FRENCH: P(s) = -1.0644454594623793 ==> log prob of sentence so far: -19.122007249749775
DUTCH: P(s) = -1.4604404489822762 ==> log prob of sentence so far: -20.220531068304773
PORTUGUESE: P(s) = -1.112894773130837 ==> log prob of sentence so far: -20.106188048564682
SPANISH: P(s) = -1.1432585839855418 ==> log prob of sentence so far: -19.34228077464011

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: bi
ENGLISH: P(i|b) = -1.4095003185493726 ==> log prob of sentence so far: -1.4095003185493726
FRENCH: P(i|b) = -0.8834736870194119 ==> log prob of sentence so far: -0.8834736870194119
DUTCH: P(i|b) = -0.9717623851081656 ==> log prob of sentence so far: -0.9717623851081656
PORTUGUESE: P(i|b) = -1.076123935211815 ==> log prob of sentence so far: -1.076123935211815
SPANISH: P(i|b) = -0.7885085253956543 ==> log prob of sentence so far: -0.7885085253956543

2GRAM: ir
ENGLISH: P(r|i) = -1.4088792448102916 ==> log prob of sentence so far: -2.818379563359664
FRENCH: P(r|i) = -1.2134874179509882 ==> log prob of sentence so far: -2.0969611049704002
DUTCH: P(r|i) = -2.1929740890887364 ==> log prob of sentence so far: -3.1647364741969017
PORTUGUESE: P(r|i) = -1.1059147012139598 ==> log prob of sentence so far: -2.1820386364257747
SPANISH: P(r|i) = -1.2824710373198902 ==> log prob of sentence so far: -2.0709795627155447

2GRAM: rd
ENGLISH: P(d|r) = -1.3854259677977665 ==> log prob of sentence so far: -4.20380553115743
FRENCH: P(d|r) = -1.3135890546809517 ==> log prob of sentence so far: -3.410550159651352
DUTCH: P(d|r) = -0.8391654937262119 ==> log prob of sentence so far: -4.003901967923113
PORTUGUESE: P(d|r) = -1.4234116550569367 ==> log prob of sentence so far: -3.605450291482711
SPANISH: P(d|r) = -1.3178550991774616 ==> log prob of sentence so far: -3.3888346618930063

2GRAM: ds
ENGLISH: P(s|d) = -1.09580661023457 ==> log prob of sentence so far: -5.299612141392
FRENCH: P(s|d) = -1.7450582160894443 ==> log prob of sentence so far: -5.155608375740796
DUTCH: P(s|d) = -1.7799176661673488 ==> log prob of sentence so far: -5.783819634090462
PORTUGUESE: P(s|d) = -2.864823231206167 ==> log prob of sentence so far: -6.470273522688878
SPANISH: P(s|d) = -2.8054950491701787 ==> log prob of sentence so far: -6.194329711063185

2GRAM: sb
ENGLISH: P(b|s) = -1.6901489051576137 ==> log prob of sentence so far: -6.989761046549614
FRENCH: P(b|s) = -2.0219729996649924 ==> log prob of sentence so far: -7.177581375405788
DUTCH: P(b|s) = -1.8457266552597453 ==> log prob of sentence so far: -7.629546289350207
PORTUGUESE: P(b|s) = -2.031425048289121 ==> log prob of sentence so far: -8.501698570977998
SPANISH: P(b|s) = -2.1001629101494514 ==> log prob of sentence so far: -8.294492621212637

2GRAM: bu
ENGLISH: P(u|b) = -0.8018189776344574 ==> log prob of sentence so far: -7.791580024184071
FRENCH: P(u|b) = -1.7166032711012917 ==> log prob of sentence so far: -8.89418464650708
DUTCH: P(u|b) = -0.9483562386905144 ==> log prob of sentence so far: -8.577902528040722
PORTUGUESE: P(u|b) = -1.5251770727549083 ==> log prob of sentence so far: -10.026875643732907
SPANISH: P(u|b) = -1.1428802531124427 ==> log prob of sentence so far: -9.43737287432508

2GRAM: ui
ENGLISH: P(i|u) = -1.6691663888726447 ==> log prob of sentence so far: -9.460746413056716
FRENCH: P(i|u) = -1.0007769495138459 ==> log prob of sentence so far: -9.894961596020925
DUTCH: P(i|u) = -0.7021697031449793 ==> log prob of sentence so far: -9.280072231185702
PORTUGUESE: P(i|u) = -1.0655272251771903 ==> log prob of sentence so far: -11.092402868910098
SPANISH: P(i|u) = -1.180030039722444 ==> log prob of sentence so far: -10.617402914047524

2GRAM: il
ENGLISH: P(l|i) = -1.230662020159717 ==> log prob of sentence so far: -10.691408433216433
FRENCH: P(l|i) = -0.8981988907257367 ==> log prob of sentence so far: -10.793160486746663
DUTCH: P(l|i) = -1.4280250056524744 ==> log prob of sentence so far: -10.708097236838176
PORTUGUESE: P(l|i) = -1.5233762349754383 ==> log prob of sentence so far: -12.615779103885536
SPANISH: P(l|i) = -1.4533900857180229 ==> log prob of sentence so far: -12.070792999765548

2GRAM: ld
ENGLISH: P(d|l) = -1.2130756787651809 ==> log prob of sentence so far: -11.904484111981613
FRENCH: P(d|l) = -2.1092520600307685 ==> log prob of sentence so far: -12.90241254677743
DUTCH: P(d|l) = -1.0108282042266286 ==> log prob of sentence so far: -11.718925441064805
PORTUGUESE: P(d|l) = -1.6452869961874614 ==> log prob of sentence so far: -14.261066100072998
SPANISH: P(d|l) = -1.4987979370426041 ==> log prob of sentence so far: -13.56959093680815

2GRAM: dn
ENGLISH: P(n|d) = -1.6483406288580484 ==> log prob of sentence so far: -13.552824740839661
FRENCH: P(n|d) = -2.421579063868141 ==> log prob of sentence so far: -15.323991610645571
DUTCH: P(n|d) = -2.253462357881213 ==> log prob of sentence so far: -13.972387798946018
PORTUGUESE: P(n|d) = -3.222758078206621 ==> log prob of sentence so far: -17.48382417827962
SPANISH: P(n|d) = -2.8206024674329204 ==> log prob of sentence so far: -16.39019340424107

2GRAM: ne
ENGLISH: P(e|n) = -1.053245365513592 ==> log prob of sentence so far: -14.606070106353252
FRENCH: P(e|n) = -0.754265128379799 ==> log prob of sentence so far: -16.07825673902537
DUTCH: P(e|n) = -1.0854610150426702 ==> log prob of sentence so far: -15.057848813988688
PORTUGUESE: P(e|n) = -1.216629317248219 ==> log prob of sentence so far: -18.70045349552784
SPANISH: P(e|n) = -1.059223402566419 ==> log prob of sentence so far: -17.44941680680749

2GRAM: es
ENGLISH: P(s|e) = -0.944851232165285 ==> log prob of sentence so far: -15.550921338518537
FRENCH: P(s|e) = -0.7261308722647525 ==> log prob of sentence so far: -16.804387611290124
DUTCH: P(s|e) = -1.4885289103627197 ==> log prob of sentence so far: -16.54637772435141
PORTUGUESE: P(s|e) = -0.8220645789953112 ==> log prob of sentence so far: -19.52251807452315
SPANISH: P(s|e) = -0.7773433899855132 ==> log prob of sentence so far: -18.226760196793006

2GRAM: st
ENGLISH: P(t|s) = -0.7036517360203784 ==> log prob of sentence so far: -16.254573074538914
FRENCH: P(t|s) = -1.237314022434334 ==> log prob of sentence so far: -18.041701633724458
DUTCH: P(t|s) = -0.5216242378706677 ==> log prob of sentence so far: -17.068001962222077
PORTUGUESE: P(t|s) = -0.9395358366561156 ==> log prob of sentence so far: -20.462053911179265
SPANISH: P(t|s) = -0.8814093632884903 ==> log prob of sentence so far: -19.108169560081496

2GRAM: ts
ENGLISH: P(s|t) = -1.3509306912217722 ==> log prob of sentence so far: -17.605503765760687
FRENCH: P(s|t) = -1.220470216535354 ==> log prob of sentence so far: -19.26217185025981
DUTCH: P(s|t) = -1.3361758122420142 ==> log prob of sentence so far: -18.40417777446409
PORTUGUESE: P(s|t) = -3.178930926896612 ==> log prob of sentence so far: -23.640984838075877
SPANISH: P(s|t) = -3.4402642052189334 ==> log prob of sentence so far: -22.54843376530043

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: bir
ENGLISH: P(r|bi) = -1.2057619297229016 ==> log prob of sentence so far: -1.2057619297229016
FRENCH: P(r|bi) = -2.048950517591257 ==> log prob of sentence so far: -2.048950517591257
DUTCH: P(r|bi) = -3.4424797690644486 ==> log prob of sentence so far: -3.4424797690644486
PORTUGUESE: P(r|bi) = -1.4557582031041367 ==> log prob of sentence so far: -1.4557582031041367
SPANISH: P(r|bi) = -1.4252751556299477 ==> log prob of sentence so far: -1.4252751556299477

3GRAM: ird
ENGLISH: P(d|ir) = -1.3642112026121063 ==> log prob of sentence so far: -2.569973132335008
FRENCH: P(d|ir) = -1.3342401812022437 ==> log prob of sentence so far: -3.3831906987935008
DUTCH: P(d|ir) = -2.815577748324267 ==> log prob of sentence so far: -6.258057517388716
PORTUGUESE: P(d|ir) = -1.5538607671178757 ==> log prob of sentence so far: -3.0096189702220126
SPANISH: P(d|ir) = -1.6093691908378458 ==> log prob of sentence so far: -3.0346443464677932

3GRAM: rds
ENGLISH: P(s|rd) = -0.7046577589209388 ==> log prob of sentence so far: -3.2746308912559465
FRENCH: P(s|rd) = -1.2234711450848368 ==> log prob of sentence so far: -4.606661843878338
DUTCH: P(s|rd) = -2.01063911673663 ==> log prob of sentence so far: -8.268696634125346
PORTUGUESE: P(s|rd) = -2.3781959504762806 ==> log prob of sentence so far: -5.387814920698293
SPANISH: P(s|rd) = -3.6510840892430116 ==> log prob of sentence so far: -6.685728435710805

3GRAM: dsb
ENGLISH: P(b|ds) = -1.7943994556149847 ==> log prob of sentence so far: -5.069030346870932
FRENCH: P(b|ds) = -2.03672280702474 ==> log prob of sentence so far: -6.643384650903078
DUTCH: P(b|ds) = -1.7042025312696958 ==> log prob of sentence so far: -9.972899165395042
PORTUGUESE: P(b|ds) = -1.8920946026904804 ==> log prob of sentence so far: -7.279909523388774
SPANISH: P(b|ds) = -2.1522883443830563 ==> log prob of sentence so far: -8.838016780093861

3GRAM: sbu
ENGLISH: P(u|sb) = -0.5958013062528547 ==> log prob of sentence so far: -5.664831653123787
FRENCH: P(u|sb) = -1.5699300705166115 ==> log prob of sentence so far: -8.213314721419689
DUTCH: P(u|sb) = -1.5126843962171637 ==> log prob of sentence so far: -11.485583561612206
PORTUGUESE: P(u|sb) = -2.1031192535457137 ==> log prob of sentence so far: -9.383028776934488
SPANISH: P(u|sb) = -0.676825714411255 ==> log prob of sentence so far: -9.514842494505116

3GRAM: bui
ENGLISH: P(i|bu) = -2.2431600815111232 ==> log prob of sentence so far: -7.90799173463491
FRENCH: P(i|bu) = -1.0440168289843739 ==> log prob of sentence so far: -9.257331550404063
DUTCH: P(i|bu) = -1.1212879379131673 ==> log prob of sentence so far: -12.606871499525372
PORTUGUESE: P(i|bu) = -0.9689657662600312 ==> log prob of sentence so far: -10.351994543194518
SPANISH: P(i|bu) = -2.2695129442179165 ==> log prob of sentence so far: -11.784355438723033

3GRAM: uil
ENGLISH: P(l|ui) = -1.0533595705718228 ==> log prob of sentence so far: -8.961351305206733
FRENCH: P(l|ui) = -0.764450288238158 ==> log prob of sentence so far: -10.021781838642221
DUTCH: P(l|ui) = -1.7654988839809496 ==> log prob of sentence so far: -14.372370383506322
PORTUGUESE: P(l|ui) = -1.447391209593138 ==> log prob of sentence so far: -11.799385752787657
SPANISH: P(l|ui) = -1.404047124348202 ==> log prob of sentence so far: -13.188402563071234

3GRAM: ild
ENGLISH: P(d|il) = -1.109452801967582 ==> log prob of sentence so far: -10.070804107174315
FRENCH: P(d|il) = -1.5996713743106084 ==> log prob of sentence so far: -11.62145321295283
DUTCH: P(d|il) = -0.5297094137852412 ==> log prob of sentence so far: -14.902079797291563
PORTUGUESE: P(d|il) = -1.4660558285695025 ==> log prob of sentence so far: -13.265441581357159
SPANISH: P(d|il) = -1.51295967994837 ==> log prob of sentence so far: -14.701362243019604

3GRAM: ldn
ENGLISH: P(n|ld) = -1.1855772876127542 ==> log prob of sentence so far: -11.256381394787068
FRENCH: P(n|ld) = -3.0726174765452368 ==> log prob of sentence so far: -14.694070689498066
DUTCH: P(n|ld) = -2.1965719928440453 ==> log prob of sentence so far: -17.09865179013561
PORTUGUESE: P(n|ld) = -1.4483971034577676 ==> log prob of sentence so far: -14.713838684814927
SPANISH: P(n|ld) = -3.3827372657613304 ==> log prob of sentence so far: -18.084099508780934

3GRAM: dne
ENGLISH: P(e|dn) = -0.6460943785602071 ==> log prob of sentence so far: -11.902475773347275
FRENCH: P(e|dn) = -0.514722740662425 ==> log prob of sentence so far: -15.20879343016049
DUTCH: P(e|dn) = -1.246409044772001 ==> log prob of sentence so far: -18.34506083490761
PORTUGUESE: P(e|dn) = -1.0 ==> log prob of sentence so far: -15.713838684814927
SPANISH: P(e|dn) = -1.1856365769619117 ==> log prob of sentence so far: -19.269736085742846

3GRAM: nes
ENGLISH: P(s|ne) = -0.726646327399672 ==> log prob of sentence so far: -12.629122100746947
FRENCH: P(s|ne) = -0.8278084722845006 ==> log prob of sentence so far: -16.036601902444993
DUTCH: P(s|ne) = -2.1685545062355778 ==> log prob of sentence so far: -20.513615341143186
PORTUGUESE: P(s|ne) = -0.8872013694972601 ==> log prob of sentence so far: -16.601040054312186
SPANISH: P(s|ne) = -0.5820722564051285 ==> log prob of sentence so far: -19.851808342147976

3GRAM: est
ENGLISH: P(t|es) = -0.7237854135100162 ==> log prob of sentence so far: -13.352907514256964
FRENCH: P(t|es) = -1.0741966768131124 ==> log prob of sentence so far: -17.110798579258105
DUTCH: P(t|es) = -0.3799427443417537 ==> log prob of sentence so far: -20.89355808548494
PORTUGUESE: P(t|es) = -0.6339293557652826 ==> log prob of sentence so far: -17.23496941007747
SPANISH: P(t|es) = -0.6165931193511333 ==> log prob of sentence so far: -20.46840146149911

3GRAM: sts
ENGLISH: P(s|st) = -1.508075020713721 ==> log prob of sentence so far: -14.860982534970685
FRENCH: P(s|st) = -1.9172667418947082 ==> log prob of sentence so far: -19.028065321152813
DUTCH: P(s|st) = -2.558388530369896 ==> log prob of sentence so far: -23.451946615854837
PORTUGUESE: P(s|st) = -2.555727656895269 ==> log prob of sentence so far: -19.790697066972736
SPANISH: P(s|st) = -4.1210343024722995 ==> log prob of sentence so far: -24.58943576397141

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: bird
ENGLISH: P(d|bir) = -0.22567515746940928 ==> log prob of sentence so far: -0.22567515746940928
FRENCH: P(d|bir) = -1.7323937598229686 ==> log prob of sentence so far: -1.7323937598229686
DUTCH: P(d|bir) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
PORTUGUESE: P(d|bir) = -1.662757831681574 ==> log prob of sentence so far: -1.662757831681574
SPANISH: P(d|bir) = -2.2095150145426308 ==> log prob of sentence so far: -2.2095150145426308

4GRAM: irds
ENGLISH: P(s|ird) = -0.6958935252473811 ==> log prob of sentence so far: -0.9215686827167904
FRENCH: P(s|ird) = -2.7708520116421442 ==> log prob of sentence so far: -4.503245771465113
DUTCH: P(s|ird) = -1.4313637641589874 ==> log prob of sentence so far: -2.8627275283179747
PORTUGUESE: P(s|ird) = -2.1271047983648077 ==> log prob of sentence so far: -3.7898626300463816
SPANISH: P(s|ird) = -2.1271047983648077 ==> log prob of sentence so far: -4.336619812907438

4GRAM: rdsb
ENGLISH: P(b|rds) = -1.7142719347691935 ==> log prob of sentence so far: -2.635840617485984
FRENCH: P(b|rds) = -1.7499080074004743 ==> log prob of sentence so far: -6.253153778865587
DUTCH: P(b|rds) = -2.1760912590556813 ==> log prob of sentence so far: -5.038818787373656
PORTUGUESE: P(b|rds) = -1.4771212547196624 ==> log prob of sentence so far: -5.266983884766044
SPANISH: P(b|rds) = -1.4313637641589874 ==> log prob of sentence so far: -5.767983577066426

4GRAM: dsbu
ENGLISH: P(u|dsb) = -0.7953532804912659 ==> log prob of sentence so far: -3.43119389797725
FRENCH: P(u|dsb) = -1.6232492903979006 ==> log prob of sentence so far: -7.876403069263488
DUTCH: P(u|dsb) = -1.7634279935629373 ==> log prob of sentence so far: -6.802246780936594
PORTUGUESE: P(u|dsb) = -1.4313637641589874 ==> log prob of sentence so far: -6.698347648925031
SPANISH: P(u|dsb) = -1.4313637641589874 ==> log prob of sentence so far: -7.199347341225414

4GRAM: sbui
ENGLISH: P(i|sbu) = -3.119915410257991 ==> log prob of sentence so far: -6.55110930823524
FRENCH: P(i|sbu) = -0.47210045334461165 ==> log prob of sentence so far: -8.3485035226081
DUTCH: P(i|sbu) = -0.37675070960209955 ==> log prob of sentence so far: -7.178997490538693
PORTUGUESE: P(i|sbu) = -1.4771212547196624 ==> log prob of sentence so far: -8.175468903644694
SPANISH: P(i|sbu) = -2.296665190261531 ==> log prob of sentence so far: -9.496012531486944

4GRAM: buil
ENGLISH: P(l|bui) = -0.14916861623280067 ==> log prob of sentence so far: -6.700277924468041
FRENCH: P(l|bui) = -1.845098040014257 ==> log prob of sentence so far: -10.193601562622357
DUTCH: P(l|bui) = -2.390935107103379 ==> log prob of sentence so far: -9.569932597642072
PORTUGUESE: P(l|bui) = -1.7323937598229686 ==> log prob of sentence so far: -9.907862663467663
SPANISH: P(l|bui) = -1.5314789170422551 ==> log prob of sentence so far: -11.027491448529199

4GRAM: uild
ENGLISH: P(d|uil) = -0.8993298381186368 ==> log prob of sentence so far: -7.599607762586677
FRENCH: P(d|uil) = -1.6862552191744227 ==> log prob of sentence so far: -11.879856781796779
DUTCH: P(d|uil) = -0.5818566052396754 ==> log prob of sentence so far: -10.151789202881748
PORTUGUESE: P(d|uil) = -2.1986570869544226 ==> log prob of sentence so far: -12.106519750422086
SPANISH: P(d|uil) = -2.2695129442179165 ==> log prob of sentence so far: -13.297004392747116

4GRAM: ildn
ENGLISH: P(n|ild) = -1.54519461951353 ==> log prob of sentence so far: -9.144802382100208
FRENCH: P(n|ild) = -2.815577748324267 ==> log prob of sentence so far: -14.695434530121046
DUTCH: P(n|ild) = -3.0453229787866576 ==> log prob of sentence so far: -13.197112181668405
PORTUGUESE: P(n|ild) = -1.8920946026904804 ==> log prob of sentence so far: -13.998614353112567
SPANISH: P(n|ild) = -2.0718820073061255 ==> log prob of sentence so far: -15.368886400053242

4GRAM: ldne
ENGLISH: P(e|ldn) = -0.8714679827947872 ==> log prob of sentence so far: -10.016270364894995
FRENCH: P(e|ldn) = -1.4313637641589874 ==> log prob of sentence so far: -16.126798294280032
DUTCH: P(e|ldn) = -1.7634279935629373 ==> log prob of sentence so far: -14.960540175231342
PORTUGUESE: P(e|ldn) = -1.0 ==> log prob of sentence so far: -14.998614353112567
SPANISH: P(e|ldn) = -1.4313637641589874 ==> log prob of sentence so far: -16.80025016421223

4GRAM: dnes
ENGLISH: P(s|dne) = -0.4137342758552695 ==> log prob of sentence so far: -10.430004640750264
FRENCH: P(s|dne) = -0.725122722937507 ==> log prob of sentence so far: -16.85192101721754
DUTCH: P(s|dne) = -1.7634279935629373 ==> log prob of sentence so far: -16.72396816879428
PORTUGUESE: P(s|dne) = -1.4771212547196624 ==> log prob of sentence so far: -16.475735607832227
SPANISH: P(s|dne) = -1.5314789170422551 ==> log prob of sentence so far: -18.331729081254483

4GRAM: nest
ENGLISH: P(t|nes) = -0.9152702615759484 ==> log prob of sentence so far: -11.345274902326212
FRENCH: P(t|nes) = -0.8886635032997615 ==> log prob of sentence so far: -17.7405845205173
DUTCH: P(t|nes) = -0.553368067428238 ==> log prob of sentence so far: -17.277336236222517
PORTUGUESE: P(t|nes) = -0.3010299956639812 ==> log prob of sentence so far: -16.77676560349621
SPANISH: P(t|nes) = -0.5904353564226673 ==> log prob of sentence so far: -18.92216443767715

4GRAM: ests
ENGLISH: P(s|est) = -1.4817139688427987 ==> log prob of sentence so far: -12.82698887116901
FRENCH: P(s|est) = -1.6461957236619915 ==> log prob of sentence so far: -19.386780244179292
DUTCH: P(s|est) = -2.6410332023627747 ==> log prob of sentence so far: -19.91836943858529
PORTUGUESE: P(s|est) = -3.5813806887099866 ==> log prob of sentence so far: -20.358146292206197
SPANISH: P(s|est) = -3.8632038590286295 ==> log prob of sentence so far: -22.78536829670578

According to the 4gram model, the sentence is in English
----------------
