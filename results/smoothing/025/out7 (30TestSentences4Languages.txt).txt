Woody Allen parle.

1GRAM MODEL:

1GRAM: w
ENGLISH: P(w) = -1.6313559002514257 ==> log prob of sentence so far: -1.6313559002514257
FRENCH: P(w) = -3.9204808901346184 ==> log prob of sentence so far: -3.9204808901346184
DUTCH: P(w) = -1.7339815774316287 ==> log prob of sentence so far: -1.7339815774316287
PORTUGUESE: P(w) = -3.3755824948867406 ==> log prob of sentence so far: -3.3755824948867406
SPANISH: P(w) = -6.166037383848577 ==> log prob of sentence so far: -6.166037383848577

1GRAM: o
ENGLISH: P(o) = -1.1377823700508483 ==> log prob of sentence so far: -2.7691382703022738
FRENCH: P(o) = -1.2743422038818786 ==> log prob of sentence so far: -5.194823094016497
DUTCH: P(o) = -1.1998170018851302 ==> log prob of sentence so far: -2.933798579316759
PORTUGUESE: P(o) = -0.9821385704267878 ==> log prob of sentence so far: -4.3577210653135285
SPANISH: P(o) = -0.9966304259011738 ==> log prob of sentence so far: -7.162667809749751

1GRAM: o
ENGLISH: P(o) = -1.1377823700508483 ==> log prob of sentence so far: -3.906920640353122
FRENCH: P(o) = -1.2743422038818786 ==> log prob of sentence so far: -6.469165297898376
DUTCH: P(o) = -1.1998170018851302 ==> log prob of sentence so far: -4.133615581201889
PORTUGUESE: P(o) = -0.9821385704267878 ==> log prob of sentence so far: -5.339859635740316
SPANISH: P(o) = -0.9966304259011738 ==> log prob of sentence so far: -8.159298235650924

1GRAM: d
ENGLISH: P(d) = -1.3961797927180317 ==> log prob of sentence so far: -5.303100433071154
FRENCH: P(d) = -1.4202617278792675 ==> log prob of sentence so far: -7.889427025777643
DUTCH: P(d) = -1.2011844788481099 ==> log prob of sentence so far: -5.334800060049999
PORTUGUESE: P(d) = -1.3354039236187203 ==> log prob of sentence so far: -6.675263559359037
SPANISH: P(d) = -1.2907619213439092 ==> log prob of sentence so far: -9.450060156994834

1GRAM: y
ENGLISH: P(y) = -1.7506000117511689 ==> log prob of sentence so far: -7.053700444822322
FRENCH: P(y) = -2.6722939958631318 ==> log prob of sentence so far: -10.561721021640775
DUTCH: P(y) = -3.569878686206462 ==> log prob of sentence so far: -8.904678746256462
PORTUGUESE: P(y) = -3.344784162555018 ==> log prob of sentence so far: -10.020047721914054
SPANISH: P(y) = -1.9106442581412726 ==> log prob of sentence so far: -11.360704415136107

1GRAM: a
ENGLISH: P(a) = -1.086787118026177 ==> log prob of sentence so far: -8.140487562848499
FRENCH: P(a) = -1.076350890171086 ==> log prob of sentence so far: -11.638071911811862
DUTCH: P(a) = -1.1153901057578817 ==> log prob of sentence so far: -10.020068852014344
PORTUGUESE: P(a) = -0.8318956635743947 ==> log prob of sentence so far: -10.851943385488449
SPANISH: P(a) = -0.9020204392084481 ==> log prob of sentence so far: -12.262724854344555

1GRAM: l
ENGLISH: P(l) = -1.3477684628666193 ==> log prob of sentence so far: -9.488256025715119
FRENCH: P(l) = -1.2682113125567938 ==> log prob of sentence so far: -12.906283224368655
DUTCH: P(l) = -1.4248884156790762 ==> log prob of sentence so far: -11.44495726769342
PORTUGUESE: P(l) = -1.4370338796900037 ==> log prob of sentence so far: -12.288977265178453
SPANISH: P(l) = -1.2802183602331265 ==> log prob of sentence so far: -13.542943214577681

1GRAM: l
ENGLISH: P(l) = -1.3477684628666193 ==> log prob of sentence so far: -10.836024488581739
FRENCH: P(l) = -1.2682113125567938 ==> log prob of sentence so far: -14.17449453692545
DUTCH: P(l) = -1.4248884156790762 ==> log prob of sentence so far: -12.869845683372496
PORTUGUESE: P(l) = -1.4370338796900037 ==> log prob of sentence so far: -13.726011144868457
SPANISH: P(l) = -1.2802183602331265 ==> log prob of sentence so far: -14.823161574810808

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -11.746093680839579
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -14.941457195078533
DUTCH: P(e) = -0.7296470912483529 ==> log prob of sentence so far: -13.599492774620849
PORTUGUESE: P(e) = -0.880446229569894 ==> log prob of sentence so far: -14.606457374438351
SPANISH: P(e) = -0.881649517549016 ==> log prob of sentence so far: -15.704811092359824

1GRAM: n
ENGLISH: P(n) = -1.1615956346167673 ==> log prob of sentence so far: -12.907689315456345
FRENCH: P(n) = -1.1226354816746384 ==> log prob of sentence so far: -16.06409267675317
DUTCH: P(n) = -0.9654220611247865 ==> log prob of sentence so far: -14.564914835745636
PORTUGUESE: P(n) = -1.3242830793492986 ==> log prob of sentence so far: -15.93074045378765
SPANISH: P(n) = -1.1498358464017453 ==> log prob of sentence so far: -16.85464693876157

1GRAM: p
ENGLISH: P(p) = -1.7418287685579088 ==> log prob of sentence so far: -14.649518084014254
FRENCH: P(p) = -1.5386370522359607 ==> log prob of sentence so far: -17.60272972898913
DUTCH: P(p) = -1.8900834352174465 ==> log prob of sentence so far: -16.45499827096308
PORTUGUESE: P(p) = -1.6180310556494373 ==> log prob of sentence so far: -17.54877150943709
SPANISH: P(p) = -1.6225529268704197 ==> log prob of sentence so far: -18.477199865631988

1GRAM: a
ENGLISH: P(a) = -1.086787118026177 ==> log prob of sentence so far: -15.73630520204043
FRENCH: P(a) = -1.076350890171086 ==> log prob of sentence so far: -18.679080619160214
DUTCH: P(a) = -1.1153901057578817 ==> log prob of sentence so far: -17.570388376720963
PORTUGUESE: P(a) = -0.8318956635743947 ==> log prob of sentence so far: -18.38066717301148
SPANISH: P(a) = -0.9020204392084481 ==> log prob of sentence so far: -19.379220304840437

1GRAM: r
ENGLISH: P(r) = -1.2612720228109857 ==> log prob of sentence so far: -16.997577224851415
FRENCH: P(r) = -1.1840269595391562 ==> log prob of sentence so far: -19.86310757869937
DUTCH: P(r) = -1.254200750493856 ==> log prob of sentence so far: -18.824589127214818
PORTUGUESE: P(r) = -1.1945420650398737 ==> log prob of sentence so far: -19.575209238051354
SPANISH: P(r) = -1.1910423872739788 ==> log prob of sentence so far: -20.570262692114415

1GRAM: l
ENGLISH: P(l) = -1.3477684628666193 ==> log prob of sentence so far: -18.345345687718034
FRENCH: P(l) = -1.2682113125567938 ==> log prob of sentence so far: -21.131318891256164
DUTCH: P(l) = -1.4248884156790762 ==> log prob of sentence so far: -20.249477542893892
PORTUGUESE: P(l) = -1.4370338796900037 ==> log prob of sentence so far: -21.012243117741356
SPANISH: P(l) = -1.2802183602331265 ==> log prob of sentence so far: -21.85048105234754

1GRAM: e
ENGLISH: P(e) = -0.9100691922578404 ==> log prob of sentence so far: -19.255414879975874
FRENCH: P(e) = -0.7669626581530837 ==> log prob of sentence so far: -21.898281549409248
DUTCH: P(e) = -0.7296470912483529 ==> log prob of sentence so far: -20.979124634142245
PORTUGUESE: P(e) = -0.880446229569894 ==> log prob of sentence so far: -21.89268934731125
SPANISH: P(e) = -0.881649517549016 ==> log prob of sentence so far: -22.732130569896555

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: wo
ENGLISH: P(o|w) = -1.0766125756617888 ==> log prob of sentence so far: -1.0766125756617888
FRENCH: P(o|w) = -1.8549130223078556 ==> log prob of sentence so far: -1.8549130223078556
DUTCH: P(o|w) = -1.003147075234186 ==> log prob of sentence so far: -1.003147075234186
PORTUGUESE: P(o|w) = -2.6085260335771943 ==> log prob of sentence so far: -2.6085260335771943
SPANISH: P(o|w) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874

2GRAM: oo
ENGLISH: P(o|o) = -1.3567931956279973 ==> log prob of sentence so far: -2.4334057712897863
FRENCH: P(o|o) = -2.758133977440803 ==> log prob of sentence so far: -4.613046999748659
DUTCH: P(o|o) = -0.6979739299764568 ==> log prob of sentence so far: -1.7011210052106427
PORTUGUESE: P(o|o) = -1.7209342893594897 ==> log prob of sentence so far: -4.329460322936684
SPANISH: P(o|o) = -2.4186592127897937 ==> log prob of sentence so far: -3.850022976948781

2GRAM: od
ENGLISH: P(d|o) = -1.5939461766620862 ==> log prob of sentence so far: -4.027351947951873
FRENCH: P(d|o) = -1.9550809160354292 ==> log prob of sentence so far: -6.568127915784088
DUTCH: P(d|o) = -1.748580415284404 ==> log prob of sentence so far: -3.449701420495047
PORTUGUESE: P(d|o) = -1.1315028511016285 ==> log prob of sentence so far: -5.460963174038312
SPANISH: P(d|o) = -1.1747209280696445 ==> log prob of sentence so far: -5.0247439050184255

2GRAM: dy
ENGLISH: P(y|d) = -1.7555559061397175 ==> log prob of sentence so far: -5.78290785409159
FRENCH: P(y|d) = -3.1570467105108 ==> log prob of sentence so far: -9.725174626294889
DUTCH: P(y|d) = -5.008574624276284 ==> log prob of sentence so far: -8.458276044771331
PORTUGUESE: P(y|d) = -3.9217280825426397 ==> log prob of sentence so far: -9.382691256580951
SPANISH: P(y|d) = -2.597219106743096 ==> log prob of sentence so far: -7.621963011761522

2GRAM: ya
ENGLISH: P(a|y) = -1.0421822804971672 ==> log prob of sentence so far: -6.825090134588757
FRENCH: P(a|y) = -0.6208051105771538 ==> log prob of sentence so far: -10.345979736872042
DUTCH: P(a|y) = -1.15358391805783 ==> log prob of sentence so far: -9.611859962829161
PORTUGUESE: P(a|y) = -1.5195251032727497 ==> log prob of sentence so far: -10.902216359853702
SPANISH: P(a|y) = -0.7714673278990829 ==> log prob of sentence so far: -8.393430339660604

2GRAM: al
ENGLISH: P(l|a) = -0.9619084802847181 ==> log prob of sentence so far: -7.786998614873475
FRENCH: P(l|a) = -1.1918078680888855 ==> log prob of sentence so far: -11.537787604960927
DUTCH: P(l|a) = -1.1010849828243647 ==> log prob of sentence so far: -10.712944945653526
PORTUGUESE: P(l|a) = -1.1792214153832388 ==> log prob of sentence so far: -12.08143777523694
SPANISH: P(l|a) = -1.0149394919983374 ==> log prob of sentence so far: -9.408369831658941

2GRAM: ll
ENGLISH: P(l|l) = -0.8383882133656653 ==> log prob of sentence so far: -8.62538682823914
FRENCH: P(l|l) = -1.0860476352293107 ==> log prob of sentence so far: -12.623835240190239
DUTCH: P(l|l) = -1.1375836191381803 ==> log prob of sentence so far: -11.850528564791706
PORTUGUESE: P(l|l) = -0.8237584400319679 ==> log prob of sentence so far: -12.905196215268909
SPANISH: P(l|l) = -1.1343674968537458 ==> log prob of sentence so far: -10.542737328512686

2GRAM: le
ENGLISH: P(e|l) = -0.693770426122538 ==> log prob of sentence so far: -9.319157254361679
FRENCH: P(e|l) = -0.4012258003874435 ==> log prob of sentence so far: -13.025061040577683
DUTCH: P(e|l) = -0.7537546122854343 ==> log prob of sentence so far: -12.60428317707714
PORTUGUESE: P(e|l) = -0.8507453296156485 ==> log prob of sentence so far: -13.755941544884557
SPANISH: P(e|l) = -0.8392291450388445 ==> log prob of sentence so far: -11.38196647355153

2GRAM: en
ENGLISH: P(n|e) = -1.0655045229859215 ==> log prob of sentence so far: -10.3846617773476
FRENCH: P(n|e) = -0.8688052128017848 ==> log prob of sentence so far: -13.893866253379468
DUTCH: P(n|e) = -0.5269453520814099 ==> log prob of sentence so far: -13.13122852915855
PORTUGUESE: P(n|e) = -0.9799050230808308 ==> log prob of sentence so far: -14.735846567965387
SPANISH: P(n|e) = -0.7295824946628534 ==> log prob of sentence so far: -12.111548968214384

2GRAM: np
ENGLISH: P(p|n) = -2.2281579223101886 ==> log prob of sentence so far: -12.61281969965779
FRENCH: P(p|n) = -1.8909014852315436 ==> log prob of sentence so far: -15.784767738611011
DUTCH: P(p|n) = -2.10938327496631 ==> log prob of sentence so far: -15.24061180412486
PORTUGUESE: P(p|n) = -3.9334670405477437 ==> log prob of sentence so far: -18.669313608513132
SPANISH: P(p|n) = -1.7615573632098618 ==> log prob of sentence so far: -13.873106331424246

2GRAM: pa
ENGLISH: P(a|p) = -0.9078026863248108 ==> log prob of sentence so far: -13.5206223859826
FRENCH: P(a|p) = -0.6469438853738003 ==> log prob of sentence so far: -16.43171162398481
DUTCH: P(a|p) = -0.854928978503816 ==> log prob of sentence so far: -16.095540782628678
PORTUGUESE: P(a|p) = -0.6222197934343825 ==> log prob of sentence so far: -19.291533401947515
SPANISH: P(a|p) = -0.5957244918971365 ==> log prob of sentence so far: -14.468830823321383

2GRAM: ar
ENGLISH: P(r|a) = -0.9854428440622539 ==> log prob of sentence so far: -14.506065230044854
FRENCH: P(r|a) = -1.0331636760797833 ==> log prob of sentence so far: -17.464875300064595
DUTCH: P(r|a) = -0.9128098519981566 ==> log prob of sentence so far: -17.008350634626833
PORTUGUESE: P(r|a) = -1.0106275820610928 ==> log prob of sentence so far: -20.30216098400861
SPANISH: P(r|a) = -0.9443875898070365 ==> log prob of sentence so far: -15.41321841312842

2GRAM: rl
ENGLISH: P(l|r) = -1.772412417610845 ==> log prob of sentence so far: -16.2784776476557
FRENCH: P(l|r) = -1.3539257071184079 ==> log prob of sentence so far: -18.818801007183
DUTCH: P(l|r) = -1.5706665752901237 ==> log prob of sentence so far: -18.579017209916955
PORTUGUESE: P(l|r) = -2.0509328635760213 ==> log prob of sentence so far: -22.35309384758463
SPANISH: P(l|r) = -1.5553933757577798 ==> log prob of sentence so far: -16.9686117888862

2GRAM: le
ENGLISH: P(e|l) = -0.693770426122538 ==> log prob of sentence so far: -16.972248073778236
FRENCH: P(e|l) = -0.4012258003874435 ==> log prob of sentence so far: -19.220026807570445
DUTCH: P(e|l) = -0.7537546122854343 ==> log prob of sentence so far: -19.33277182220239
PORTUGUESE: P(e|l) = -0.8507453296156485 ==> log prob of sentence so far: -23.20383917720028
SPANISH: P(e|l) = -0.8392291450388445 ==> log prob of sentence so far: -17.807840933925043

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: woo
ENGLISH: P(o|wo) = -1.1062377831831987 ==> log prob of sentence so far: -1.1062377831831987
FRENCH: P(o|wo) = -1.4771212547196624 ==> log prob of sentence so far: -1.4771212547196624
DUTCH: P(o|wo) = -0.6640983798611456 ==> log prob of sentence so far: -0.6640983798611456
PORTUGUESE: P(o|wo) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
SPANISH: P(o|wo) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874

3GRAM: ood
ENGLISH: P(d|oo) = -0.711708324466352 ==> log prob of sentence so far: -1.8179461076495507
FRENCH: P(d|oo) = -1.7512791039833422 ==> log prob of sentence so far: -3.228400358703005
DUTCH: P(d|oo) = -1.238923950641701 ==> log prob of sentence so far: -1.9030223305028464
PORTUGUESE: P(d|oo) = -1.5715794427173377 ==> log prob of sentence so far: -3.002943206876325
SPANISH: P(d|oo) = -1.172160965427249 ==> log prob of sentence so far: -2.603524729586236

3GRAM: ody
ENGLISH: P(y|od) = -1.012945652427145 ==> log prob of sentence so far: -2.8308917600766956
FRENCH: P(y|od) = -2.520614521878236 ==> log prob of sentence so far: -5.749014880581241
DUTCH: P(y|od) = -3.2615007731982804 ==> log prob of sentence so far: -5.164523103701127
PORTUGUESE: P(y|od) = -3.8217754671834636 ==> log prob of sentence so far: -6.824718674059788
SPANISH: P(y|od) = -3.975063996095236 ==> log prob of sentence so far: -6.578588725681472

3GRAM: dya
ENGLISH: P(a|dy) = -1.1525357754216399 ==> log prob of sentence so far: -3.9834275354983353
FRENCH: P(a|dy) = -1.03698356625317 ==> log prob of sentence so far: -6.785998446834411
DUTCH: P(a|dy) = -1.4313637641589874 ==> log prob of sentence so far: -6.595886867860115
PORTUGUESE: P(a|dy) = -1.4771212547196624 ==> log prob of sentence so far: -8.30183992877945
SPANISH: P(a|dy) = -0.8163556030538729 ==> log prob of sentence so far: -7.394944328735345

3GRAM: yal
ENGLISH: P(l|ya) = -0.9780657640439847 ==> log prob of sentence so far: -4.96149329954232
FRENCH: P(l|ya) = -1.3460519037405547 ==> log prob of sentence so far: -8.132050350574966
DUTCH: P(l|ya) = -1.0644579892269184 ==> log prob of sentence so far: -7.660344857087033
PORTUGUESE: P(l|ya) = -1.5797835966168101 ==> log prob of sentence so far: -9.881623525396261
SPANISH: P(l|ya) = -0.832008933394255 ==> log prob of sentence so far: -8.2269532621296

3GRAM: all
ENGLISH: P(l|al) = -0.4470956782501075 ==> log prob of sentence so far: -5.408588977792427
FRENCH: P(l|al) = -0.9313325386430092 ==> log prob of sentence so far: -9.063382889217975
DUTCH: P(l|al) = -0.6350602514308877 ==> log prob of sentence so far: -8.29540510851792
PORTUGUESE: P(l|al) = -1.1484549965792867 ==> log prob of sentence so far: -11.030078521975547
SPANISH: P(l|al) = -0.961208614527359 ==> log prob of sentence so far: -9.18816187665696

3GRAM: lle
ENGLISH: P(e|ll) = -0.9437304921356443 ==> log prob of sentence so far: -6.352319469928072
FRENCH: P(e|ll) = -0.15877461038259083 ==> log prob of sentence so far: -9.222157499600565
DUTCH: P(e|ll) = -0.15045578199225 ==> log prob of sentence so far: -8.44586089051017
PORTUGUESE: P(e|ll) = -0.44976916950211854 ==> log prob of sentence so far: -11.479847691477666
SPANISH: P(e|ll) = -0.5169762258432301 ==> log prob of sentence so far: -9.70513810250019

3GRAM: len
ENGLISH: P(n|le) = -1.2612464613135668 ==> log prob of sentence so far: -7.613565931241639
FRENCH: P(n|le) = -1.164295034756445 ==> log prob of sentence so far: -10.386452534357009
DUTCH: P(n|le) = -0.4522637790722851 ==> log prob of sentence so far: -8.898124669582455
PORTUGUESE: P(n|le) = -0.9769981285091374 ==> log prob of sentence so far: -12.456845819986803
SPANISH: P(n|le) = -1.0986374943549442 ==> log prob of sentence so far: -10.803775596855134

3GRAM: enp
ENGLISH: P(p|en) = -2.0714690489720913 ==> log prob of sentence so far: -9.68503498021373
FRENCH: P(p|en) = -1.9245675665118318 ==> log prob of sentence so far: -12.311020100868841
DUTCH: P(p|en) = -2.0572447663207054 ==> log prob of sentence so far: -10.95536943590316
PORTUGUESE: P(p|en) = -3.384460943824492 ==> log prob of sentence so far: -15.841306763811296
SPANISH: P(p|en) = -1.8999148413831533 ==> log prob of sentence so far: -12.703690438238286

3GRAM: npa
ENGLISH: P(a|np) = -0.780398574838997 ==> log prob of sentence so far: -10.465433555052728
FRENCH: P(a|np) = -0.6668658129455547 ==> log prob of sentence so far: -12.977885913814395
DUTCH: P(a|np) = -0.561558776510611 ==> log prob of sentence so far: -11.51692821241377
PORTUGUESE: P(a|np) = -1.4771212547196624 ==> log prob of sentence so far: -17.31842801853096
SPANISH: P(a|np) = -0.5890374154258573 ==> log prob of sentence so far: -13.292727853664143

3GRAM: par
ENGLISH: P(r|pa) = -0.484481045818538 ==> log prob of sentence so far: -10.949914600871267
FRENCH: P(r|pa) = -0.3229657424962003 ==> log prob of sentence so far: -13.300851656310595
DUTCH: P(r|pa) = -1.1437467028748534 ==> log prob of sentence so far: -12.660674915288624
PORTUGUESE: P(r|pa) = -0.3233151852330615 ==> log prob of sentence so far: -17.64174320376402
SPANISH: P(r|pa) = -0.3734739456259632 ==> log prob of sentence so far: -13.666201799290107

3GRAM: arl
ENGLISH: P(l|ar) = -1.6014328905451314 ==> log prob of sentence so far: -12.551347491416399
FRENCH: P(l|ar) = -1.0505025958388208 ==> log prob of sentence so far: -14.351354252149417
DUTCH: P(l|ar) = -1.8635839677362658 ==> log prob of sentence so far: -14.52425888302489
PORTUGUESE: P(l|ar) = -1.726236570366317 ==> log prob of sentence so far: -19.367979774130337
SPANISH: P(l|ar) = -1.2928729670310868 ==> log prob of sentence so far: -14.959074766321194

3GRAM: rle
ENGLISH: P(e|rl) = -0.839036004655313 ==> log prob of sentence so far: -13.390383496071712
FRENCH: P(e|rl) = -0.2025372987697513 ==> log prob of sentence so far: -14.553891550919168
DUTCH: P(e|rl) = -1.0355631414975013 ==> log prob of sentence so far: -15.559822024522392
PORTUGUESE: P(e|rl) = -1.2024718042758515 ==> log prob of sentence so far: -20.57045157840619
SPANISH: P(e|rl) = -0.636432820222828 ==> log prob of sentence so far: -15.595507586544022

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: wood
ENGLISH: P(d|woo) = -0.24394680499591315 ==> log prob of sentence so far: -0.24394680499591315
FRENCH: P(d|woo) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
DUTCH: P(d|woo) = -2.8363241157067516 ==> log prob of sentence so far: -2.8363241157067516
PORTUGUESE: P(d|woo) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
SPANISH: P(d|woo) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874

4GRAM: oody
ENGLISH: P(y|ood) = -1.3463142344763117 ==> log prob of sentence so far: -1.590261039472225
FRENCH: P(y|ood) = -1.4771212547196624 ==> log prob of sentence so far: -2.9084850188786495
DUTCH: P(y|ood) = -3.0711452904510828 ==> log prob of sentence so far: -5.907469406157834
PORTUGUESE: P(y|ood) = -1.845098040014257 ==> log prob of sentence so far: -3.2764618041732443
SPANISH: P(y|ood) = -1.792391689498254 ==> log prob of sentence so far: -3.2237554536572413

4GRAM: odya
ENGLISH: P(a|ody) = -0.8781161023545606 ==> log prob of sentence so far: -2.4683771418267857
FRENCH: P(a|ody) = -1.4771212547196624 ==> log prob of sentence so far: -4.385606273598312
DUTCH: P(a|ody) = -1.4313637641589874 ==> log prob of sentence so far: -7.338833170316821
PORTUGUESE: P(a|ody) = -1.4313637641589874 ==> log prob of sentence so far: -4.707825568332232
SPANISH: P(a|ody) = -1.4313637641589874 ==> log prob of sentence so far: -4.655119217816228

4GRAM: dyal
ENGLISH: P(l|dya) = -1.5976951859255124 ==> log prob of sentence so far: -4.066072327752298
FRENCH: P(l|dya) = -1.5314789170422551 ==> log prob of sentence so far: -5.9170851906405675
DUTCH: P(l|dya) = -1.4313637641589874 ==> log prob of sentence so far: -8.770196934475809
PORTUGUESE: P(l|dya) = -1.4313637641589874 ==> log prob of sentence so far: -6.139189332491219
SPANISH: P(l|dya) = -1.0334237554869496 ==> log prob of sentence so far: -5.688542973303178

4GRAM: yall
ENGLISH: P(l|yal) = -0.3743479677505349 ==> log prob of sentence so far: -4.440420295502833
FRENCH: P(l|yal) = -1.0 ==> log prob of sentence so far: -6.9170851906405675
DUTCH: P(l|yal) = -1.4771212547196624 ==> log prob of sentence so far: -10.247318189195472
PORTUGUESE: P(l|yal) = -1.4313637641589874 ==> log prob of sentence so far: -7.570553096650206
SPANISH: P(l|yal) = -1.0487986170539039 ==> log prob of sentence so far: -6.737341590357081

4GRAM: alle
ENGLISH: P(e|all) = -1.0187823758754024 ==> log prob of sentence so far: -5.459202671378236
FRENCH: P(e|all) = -0.5412554840250479 ==> log prob of sentence so far: -7.458340674665616
DUTCH: P(e|all) = -0.03665017365331037 ==> log prob of sentence so far: -10.283968362848782
PORTUGUESE: P(e|all) = -1.1725459782910315 ==> log prob of sentence so far: -8.743099074941238
SPANISH: P(e|all) = -0.4873572246871537 ==> log prob of sentence so far: -7.224698815044235

4GRAM: llen
ENGLISH: P(n|lle) = -1.017228279594671 ==> log prob of sentence so far: -6.476430950972907
FRENCH: P(n|lle) = -1.2787017665573621 ==> log prob of sentence so far: -8.737042441222977
DUTCH: P(n|lle) = -0.38552977105908537 ==> log prob of sentence so far: -10.669498133907867
PORTUGUESE: P(n|lle) = -1.022635852807294 ==> log prob of sentence so far: -9.765734927748532
SPANISH: P(n|lle) = -1.1299475572806672 ==> log prob of sentence so far: -8.354646372324902

4GRAM: lenp
ENGLISH: P(p|len) = -3.2576785748691846 ==> log prob of sentence so far: -9.734109525842092
FRENCH: P(p|len) = -2.6519238051682956 ==> log prob of sentence so far: -11.388966246391274
DUTCH: P(p|len) = -2.5754444443335918 ==> log prob of sentence so far: -13.244942578241458
PORTUGUESE: P(p|len) = -2.6972293427597176 ==> log prob of sentence so far: -12.46296427050825
SPANISH: P(p|len) = -1.9710695820603246 ==> log prob of sentence so far: -10.325715954385227

4GRAM: enpa
ENGLISH: P(a|enp) = -0.726162742149921 ==> log prob of sentence so far: -10.460272267992012
FRENCH: P(a|enp) = -0.6430063306278823 ==> log prob of sentence so far: -12.031972577019156
DUTCH: P(a|enp) = -0.6224306389172205 ==> log prob of sentence so far: -13.86737321715868
PORTUGUESE: P(a|enp) = -1.4771212547196624 ==> log prob of sentence so far: -13.940085525227913
SPANISH: P(a|enp) = -0.5742704825406538 ==> log prob of sentence so far: -10.89998643692588

4GRAM: npar
ENGLISH: P(r|npa) = -0.41282261037873746 ==> log prob of sentence so far: -10.87309487837075
FRENCH: P(r|npa) = -0.35922675022947836 ==> log prob of sentence so far: -12.391199327248634
DUTCH: P(r|npa) = -0.8351897513540077 ==> log prob of sentence so far: -14.702562968512687
PORTUGUESE: P(r|npa) = -1.4313637641589874 ==> log prob of sentence so far: -15.3714492893869
SPANISH: P(r|npa) = -0.3798509808183607 ==> log prob of sentence so far: -11.279837417744242

4GRAM: parl
ENGLISH: P(l|par) = -1.7954938061297363 ==> log prob of sentence so far: -12.668588684500486
FRENCH: P(l|par) = -0.6914402828249538 ==> log prob of sentence so far: -13.082639610073588
DUTCH: P(l|par) = -2.369215857410143 ==> log prob of sentence so far: -17.07177882592283
PORTUGUESE: P(l|par) = -2.2822559947888994 ==> log prob of sentence so far: -17.6537052841758
SPANISH: P(l|par) = -2.346352974450639 ==> log prob of sentence so far: -13.62619039219488

4GRAM: arle
ENGLISH: P(e|arl) = -0.8466859557938888 ==> log prob of sentence so far: -13.515274640294376
FRENCH: P(e|arl) = -0.17799630821858303 ==> log prob of sentence so far: -13.260635918292172
DUTCH: P(e|arl) = -0.9637878273455552 ==> log prob of sentence so far: -18.035566653268386
PORTUGUESE: P(e|arl) = -0.9929950984313415 ==> log prob of sentence so far: -18.64670038260714
SPANISH: P(e|arl) = -0.5519393819486244 ==> log prob of sentence so far: -14.178129774143505

According to the 4gram model, the sentence is in French
----------------
