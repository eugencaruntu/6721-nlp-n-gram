Etienne loves Veronique.

1GRAM MODEL:

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -0.9100712180599071
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -0.7669658145224565
DUTCH: P(e) = -0.7296525128280946 ==> log prob of sentence so far: -0.7296525128280946
PORTUGUESE: P(e) = -0.8804550652146593 ==> log prob of sentence so far: -0.8804550652146593
SPANISH: P(e) = -0.8816549652595953 ==> log prob of sentence so far: -0.8816549652595953

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -1.9442948558441795
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -1.93293256261921
DUTCH: P(t) = -1.2498840127620319 ==> log prob of sentence so far: -1.9795365255901265
PORTUGUESE: P(t) = -1.3798331462051736 ==> log prob of sentence so far: -2.260288211419833
SPANISH: P(t) = -1.4035916054876783 ==> log prob of sentence so far: -2.2852465707472733

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -3.1068203361180906
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -3.0681390052880535
DUTCH: P(i) = -1.2262490424361108 ==> log prob of sentence so far: -3.2057855680262373
PORTUGUESE: P(i) = -1.2396988999998464 ==> log prob of sentence so far: -3.499987111419679
SPANISH: P(i) = -1.2231544531761491 ==> log prob of sentence so far: -3.5084010239234225

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -4.016891554177998
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -3.83510481981051
DUTCH: P(e) = -0.7296525128280946 ==> log prob of sentence so far: -3.9354380808543317
PORTUGUESE: P(e) = -0.8804550652146593 ==> log prob of sentence so far: -4.380442176634339
SPANISH: P(e) = -0.8816549652595953 ==> log prob of sentence so far: -4.3900559891830175

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -5.178488491580627
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -4.957742296425015
DUTCH: P(n) = -0.9654264661937975 ==> log prob of sentence so far: -4.900864547048129
PORTUGUESE: P(n) = -1.3242854314663415 ==> log prob of sentence so far: -5.70472760810068
SPANISH: P(n) = -1.1498393665103084 ==> log prob of sentence so far: -5.539895355693326

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -6.340085428983256
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -6.080379773039519
DUTCH: P(n) = -0.9654264661937975 ==> log prob of sentence so far: -5.866291013241926
PORTUGUESE: P(n) = -1.3242854314663415 ==> log prob of sentence so far: -7.029013039567022
SPANISH: P(n) = -1.1498393665103084 ==> log prob of sentence so far: -6.689734722203634

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -7.2501566470431635
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -6.847345587561976
DUTCH: P(e) = -0.7296525128280946 ==> log prob of sentence so far: -6.5959435260700205
PORTUGUESE: P(e) = -0.8804550652146593 ==> log prob of sentence so far: -7.909468104781681
SPANISH: P(e) = -0.8816549652595953 ==> log prob of sentence so far: -7.571389687463229

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -8.597925532464163
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -8.115558067847498
DUTCH: P(l) = -1.4248882579214401 ==> log prob of sentence so far: -8.020831783991461
PORTUGUESE: P(l) = -1.4370332293417125 ==> log prob of sentence so far: -9.346501334123394
SPANISH: P(l) = -1.2802204153542895 ==> log prob of sentence so far: -8.851610102817519

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -9.735709293049657
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -9.389901398165152
DUTCH: P(o) = -1.1998196708165811 ==> log prob of sentence so far: -9.220651454808042
PORTUGUESE: P(o) = -0.982146444324746 ==> log prob of sentence so far: -10.32864777844814
SPANISH: P(o) = -0.9966351897105246 ==> log prob of sentence so far: -9.848245292528043

1GRAM: v
ENGLISH: P(v) = -2.043619342325931 ==> log prob of sentence so far: -11.779328635375588
FRENCH: P(v) = -1.82784228094936 ==> log prob of sentence so far: -11.217743679114513
DUTCH: P(v) = -1.6099977038119904 ==> log prob of sentence so far: -10.830649158620032
PORTUGUESE: P(v) = -1.7546314510401964 ==> log prob of sentence so far: -12.083279229488337
SPANISH: P(v) = -1.9460940020959083 ==> log prob of sentence so far: -11.794339294623951

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -12.689399853435495
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -11.98470949363697
DUTCH: P(e) = -0.7296525128280946 ==> log prob of sentence so far: -11.560301671448126
PORTUGUESE: P(e) = -0.8804550652146593 ==> log prob of sentence so far: -12.963734294702997
SPANISH: P(e) = -0.8816549652595953 ==> log prob of sentence so far: -12.675994259883547

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -13.860540866164095
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -13.049157208526626
DUTCH: P(s) = -1.460439695007922 ==> log prob of sentence so far: -13.020741366456049
PORTUGUESE: P(s) = -1.1129010285568741 ==> log prob of sentence so far: -14.076635323259872
SPANISH: P(s) = -1.1432621669802536 ==> log prob of sentence so far: -13.8192564268638

1GRAM: v
ENGLISH: P(v) = -2.043619342325931 ==> log prob of sentence so far: -15.904160208490026
FRENCH: P(v) = -1.82784228094936 ==> log prob of sentence so far: -14.876999489475986
DUTCH: P(v) = -1.6099977038119904 ==> log prob of sentence so far: -14.630739070268039
PORTUGUESE: P(v) = -1.7546314510401964 ==> log prob of sentence so far: -15.831266774300069
SPANISH: P(v) = -1.9460940020959083 ==> log prob of sentence so far: -15.765350428959708

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -16.814231426549934
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -15.643965303998442
DUTCH: P(e) = -0.7296525128280946 ==> log prob of sentence so far: -15.360391583096133
PORTUGUESE: P(e) = -0.8804550652146593 ==> log prob of sentence so far: -16.711721839514727
SPANISH: P(e) = -0.8816549652595953 ==> log prob of sentence so far: -16.647005394219303

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -18.07550432786203
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -16.8279939430689
DUTCH: P(r) = -1.2542028641369134 ==> log prob of sentence so far: -16.614594447233046
PORTUGUESE: P(r) = -1.1945470328532288 ==> log prob of sentence so far: -17.906268872367956
SPANISH: P(r) = -1.191045490967515 ==> log prob of sentence so far: -17.838050885186817

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -19.213288088447527
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -18.102337273386556
DUTCH: P(o) = -1.1998196708165811 ==> log prob of sentence so far: -17.814414118049626
PORTUGUESE: P(o) = -0.982146444324746 ==> log prob of sentence so far: -18.8884153166927
SPANISH: P(o) = -0.9966351897105246 ==> log prob of sentence so far: -18.83468607489734

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -20.374885025850155
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -19.22497475000106
DUTCH: P(n) = -0.9654264661937975 ==> log prob of sentence so far: -18.779840584243424
PORTUGUESE: P(n) = -1.3242854314663415 ==> log prob of sentence so far: -20.212700748159044
SPANISH: P(n) = -1.1498393665103084 ==> log prob of sentence so far: -19.984525441407648

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -21.537410506124065
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -20.360181192669902
DUTCH: P(i) = -1.2262490424361108 ==> log prob of sentence so far: -20.006089626679533
PORTUGUESE: P(i) = -1.2396988999998464 ==> log prob of sentence so far: -21.45239964815889
SPANISH: P(i) = -1.2231544531761491 ==> log prob of sentence so far: -21.207679894583798

1GRAM: q
ENGLISH: P(q) = -2.7879988169444423 ==> log prob of sentence so far: -24.325409323068506
FRENCH: P(q) = -1.9394381295606844 ==> log prob of sentence so far: -22.299619322230587
DUTCH: P(q) = -4.065957184296058 ==> log prob of sentence so far: -24.07204681097559
PORTUGUESE: P(q) = -1.8683838627056177 ==> log prob of sentence so far: -23.32078351086451
SPANISH: P(q) = -1.8372986299782537 ==> log prob of sentence so far: -23.04497852456205

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -25.87789749202642
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -23.50575438340621
DUTCH: P(u) = -1.74534287687545 ==> log prob of sentence so far: -25.81738968785104
PORTUGUESE: P(u) = -1.3141939526321222 ==> log prob of sentence so far: -24.634977463496632
SPANISH: P(u) = -1.3656717329598758 ==> log prob of sentence so far: -24.410650257521926

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -26.78796871008633
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -24.272720197928667
DUTCH: P(e) = -0.7296525128280946 ==> log prob of sentence so far: -26.547042200679137
PORTUGUESE: P(e) = -0.8804550652146593 ==> log prob of sentence so far: -25.51543252871129
SPANISH: P(e) = -0.8816549652595953 ==> log prob of sentence so far: -25.292305222781522

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: et
ENGLISH: P(t|e) = -1.1805749996736075 ==> log prob of sentence so far: -1.1805749996736075
FRENCH: P(t|e) = -1.081730001747917 ==> log prob of sentence so far: -1.081730001747917
DUTCH: P(t|e) = -1.1837528305322884 ==> log prob of sentence so far: -1.1837528305322884
PORTUGUESE: P(t|e) = -1.551723849866917 ==> log prob of sentence so far: -1.551723849866917
SPANISH: P(t|e) = -1.5942592148484573 ==> log prob of sentence so far: -1.5942592148484573

2GRAM: ti
ENGLISH: P(i|t) = -1.0855982270227826 ==> log prob of sentence so far: -2.26617322669639
FRENCH: P(i|t) = -0.9860184878476054 ==> log prob of sentence so far: -2.0677484895955223
DUTCH: P(i|t) = -1.2223991657777313 ==> log prob of sentence so far: -2.4061519963100197
PORTUGUESE: P(i|t) = -0.9912582706946893 ==> log prob of sentence so far: -2.5429821205616063
SPANISH: P(i|t) = -0.948297769257498 ==> log prob of sentence so far: -2.542556984105955

2GRAM: ie
ENGLISH: P(e|i) = -1.5107075344419896 ==> log prob of sentence so far: -3.7768807611383792
FRENCH: P(e|i) = -0.9021220228745312 ==> log prob of sentence so far: -2.9698705124700533
DUTCH: P(e|i) = -0.7078508965711553 ==> log prob of sentence so far: -3.114002892881175
PORTUGUESE: P(e|i) = -1.8490694524427844 ==> log prob of sentence so far: -4.392051573004391
SPANISH: P(e|i) = -0.8666515620295678 ==> log prob of sentence so far: -3.409208546135523

2GRAM: en
ENGLISH: P(n|e) = -1.0655182392646758 ==> log prob of sentence so far: -4.842399000403055
FRENCH: P(n|e) = -0.8688224207402516 ==> log prob of sentence so far: -3.838692933210305
DUTCH: P(n|e) = -0.5269778242584016 ==> log prob of sentence so far: -3.6409807171395765
PORTUGUESE: P(n|e) = -0.9799668961094903 ==> log prob of sentence so far: -5.372018469113881
SPANISH: P(n|e) = -0.7296304377265809 ==> log prob of sentence so far: -4.138838983862104

2GRAM: nn
ENGLISH: P(n|n) = -1.9288837797167968 ==> log prob of sentence so far: -6.771282780119852
FRENCH: P(n|n) = -1.4789797928128527 ==> log prob of sentence so far: -5.317672726023158
DUTCH: P(n|n) = -1.5183432601814861 ==> log prob of sentence so far: -5.159323977321063
PORTUGUESE: P(n|n) = -2.152693243239444 ==> log prob of sentence so far: -7.524711712353325
SPANISH: P(n|n) = -2.2609491855127652 ==> log prob of sentence so far: -6.399788169374869

2GRAM: ne
ENGLISH: P(e|n) = -1.0532701120323171 ==> log prob of sentence so far: -7.824552892152169
FRENCH: P(e|n) = -0.7543075705657655 ==> log prob of sentence so far: -6.071980296588923
DUTCH: P(e|n) = -1.0854967763423953 ==> log prob of sentence so far: -6.244820753663458
PORTUGUESE: P(e|n) = -1.2167257748200284 ==> log prob of sentence so far: -8.741437487173354
SPANISH: P(e|n) = -1.0592859040672948 ==> log prob of sentence so far: -7.459074073442164

2GRAM: el
ENGLISH: P(l|e) = -1.3277804884685662 ==> log prob of sentence so far: -9.152333380620735
FRENCH: P(l|e) = -1.2044345212229748 ==> log prob of sentence so far: -7.276414817811898
DUTCH: P(l|e) = -1.1637226950598198 ==> log prob of sentence so far: -7.408543448723277
PORTUGUESE: P(l|e) = -1.1287459762359933 ==> log prob of sentence so far: -9.870183463409347
SPANISH: P(l|e) = -0.9033967676475738 ==> log prob of sentence so far: -8.362470841089738

2GRAM: lo
ENGLISH: P(o|l) = -1.0313867050170902 ==> log prob of sentence so far: -10.183720085637825
FRENCH: P(o|l) = -1.1639759810063668 ==> log prob of sentence so far: -8.440390798818266
DUTCH: P(o|l) = -1.1567197126394506 ==> log prob of sentence so far: -8.565263161362727
PORTUGUESE: P(o|l) = -1.1089097545257627 ==> log prob of sentence so far: -10.97909321793511
SPANISH: P(o|l) = -0.7251815454396849 ==> log prob of sentence so far: -9.087652386529424

2GRAM: ov
ENGLISH: P(v|o) = -1.7957558593393887 ==> log prob of sentence so far: -11.979475944977214
FRENCH: P(v|o) = -2.526669664991859 ==> log prob of sentence so far: -10.967060463810125
DUTCH: P(v|o) = -1.600195206145255 ==> log prob of sentence so far: -10.165458367507982
PORTUGUESE: P(v|o) = -1.8175876944074065 ==> log prob of sentence so far: -12.796680912342516
SPANISH: P(v|o) = -1.8968322272631362 ==> log prob of sentence so far: -10.98448461379256

2GRAM: ve
ENGLISH: P(e|v) = -0.17428026499063828 ==> log prob of sentence so far: -12.153756209967852
FRENCH: P(e|v) = -0.4815039345822809 ==> log prob of sentence so far: -11.448564398392406
DUTCH: P(e|v) = -0.4368964994892093 ==> log prob of sentence so far: -10.602354866997192
PORTUGUESE: P(e|v) = -0.4548584898508671 ==> log prob of sentence so far: -13.251539402193384
SPANISH: P(e|v) = -0.5270799338521787 ==> log prob of sentence so far: -11.511564547644738

2GRAM: es
ENGLISH: P(s|e) = -0.944867640322014 ==> log prob of sentence so far: -13.098623850289867
FRENCH: P(s|e) = -0.7261499946209886 ==> log prob of sentence so far: -12.174714393013396
DUTCH: P(s|e) = -1.4885220269054555 ==> log prob of sentence so far: -12.090876893902648
PORTUGUESE: P(s|e) = -0.8221373946407721 ==> log prob of sentence so far: -14.073676796834155
SPANISH: P(s|e) = -0.7773898838843721 ==> log prob of sentence so far: -12.28895443152911

2GRAM: sv
ENGLISH: P(v|s) = -2.539334750977554 ==> log prob of sentence so far: -15.637958601267421
FRENCH: P(v|s) = -1.956488342986067 ==> log prob of sentence so far: -14.131202735999462
DUTCH: P(v|s) = -1.4862900166931376 ==> log prob of sentence so far: -13.577166910595786
PORTUGUESE: P(v|s) = -1.9528919172610466 ==> log prob of sentence so far: -16.026568714095202
SPANISH: P(v|s) = -1.9805444137390624 ==> log prob of sentence so far: -14.269498845268172

2GRAM: ve
ENGLISH: P(e|v) = -0.17428026499063828 ==> log prob of sentence so far: -15.81223886625806
FRENCH: P(e|v) = -0.4815039345822809 ==> log prob of sentence so far: -14.612706670581744
DUTCH: P(e|v) = -0.4368964994892093 ==> log prob of sentence so far: -14.014063410084995
PORTUGUESE: P(e|v) = -0.4548584898508671 ==> log prob of sentence so far: -16.48142720394607
SPANISH: P(e|v) = -0.5270799338521787 ==> log prob of sentence so far: -14.79657877912035

2GRAM: er
ENGLISH: P(r|e) = -0.8561613604222861 ==> log prob of sentence so far: -16.668400226680347
FRENCH: P(r|e) = -1.0504664166531121 ==> log prob of sentence so far: -15.663173087234856
DUTCH: P(r|e) = -0.8727420982068449 ==> log prob of sentence so far: -14.88680550829184
PORTUGUESE: P(r|e) = -0.9762482313577309 ==> log prob of sentence so far: -17.4576754353038
SPANISH: P(r|e) = -0.9099180534364051 ==> log prob of sentence so far: -15.706496832556756

2GRAM: ro
ENGLISH: P(o|r) = -0.9679454985579867 ==> log prob of sentence so far: -17.636345725238336
FRENCH: P(o|r) = -1.1139096014103482 ==> log prob of sentence so far: -16.777082688645205
DUTCH: P(o|r) = -1.030099792018266 ==> log prob of sentence so far: -15.916905300310106
PORTUGUESE: P(o|r) = -0.9761621541266129 ==> log prob of sentence so far: -18.43383758943041
SPANISH: P(o|r) = -0.8581323749493194 ==> log prob of sentence so far: -16.564629207506076

2GRAM: on
ENGLISH: P(n|o) = -0.8622085952026249 ==> log prob of sentence so far: -18.49855432044096
FRENCH: P(n|o) = -0.5205456677173079 ==> log prob of sentence so far: -17.297628356362512
DUTCH: P(n|o) = -0.8623470116246322 ==> log prob of sentence so far: -16.779252311934737
PORTUGUESE: P(n|o) = -1.1339351022385673 ==> log prob of sentence so far: -19.567772691668978
SPANISH: P(n|o) = -0.8457337450674146 ==> log prob of sentence so far: -17.41036295257349

2GRAM: ni
ENGLISH: P(i|n) = -1.3332322954338982 ==> log prob of sentence so far: -19.831786615874858
FRENCH: P(i|n) = -1.4845526185364621 ==> log prob of sentence so far: -18.782180974898974
DUTCH: P(i|n) = -1.2579813083737479 ==> log prob of sentence so far: -18.037233620308484
PORTUGUESE: P(i|n) = -1.526169332374213 ==> log prob of sentence so far: -21.09394202404319
SPANISH: P(i|n) = -1.403921655876726 ==> log prob of sentence so far: -18.814284608450215

2GRAM: iq
ENGLISH: P(q|i) = -3.179393289527697 ==> log prob of sentence so far: -23.011179905402557
FRENCH: P(q|i) = -1.7047366261391819 ==> log prob of sentence so far: -20.486917601038158
DUTCH: P(q|i) = -3.018611980947513 ==> log prob of sentence so far: -21.055845601255996
PORTUGUESE: P(q|i) = -2.1281782076871 ==> log prob of sentence so far: -23.22212023173029
SPANISH: P(q|i) = -2.239812767787548 ==> log prob of sentence so far: -21.054097376237763

2GRAM: qu
ENGLISH: P(u|q) = -0.0037433435301242783 ==> log prob of sentence so far: -23.014923248932682
FRENCH: P(u|q) = -0.00832433543958775 ==> log prob of sentence so far: -20.495241936477747
DUTCH: P(u|q) = -0.1310128843204931 ==> log prob of sentence so far: -21.18685848557649
PORTUGUESE: P(u|q) = -0.0017690227388466463 ==> log prob of sentence so far: -23.223889254469135
SPANISH: P(u|q) = -0.0010989135717912205 ==> log prob of sentence so far: -21.055196289809555

2GRAM: ue
ENGLISH: P(e|u) = -1.3246679556129457 ==> log prob of sentence so far: -24.339591204545627
FRENCH: P(e|u) = -0.8812512722642089 ==> log prob of sentence so far: -21.376493208741955
DUTCH: P(e|u) = -1.9473691432183058 ==> log prob of sentence so far: -23.134227628794797
PORTUGUESE: P(e|u) = -0.567594281246031 ==> log prob of sentence so far: -23.791483535715166
SPANISH: P(e|u) = -0.3957542761522447 ==> log prob of sentence so far: -21.4509505659618

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: eti
ENGLISH: P(i|et) = -1.1129191759155594 ==> log prob of sentence so far: -1.1129191759155594
FRENCH: P(i|et) = -1.2501654504528017 ==> log prob of sentence so far: -1.2501654504528017
DUTCH: P(i|et) = -1.5058955484290315 ==> log prob of sentence so far: -1.5058955484290315
PORTUGUESE: P(i|et) = -0.9989458866623447 ==> log prob of sentence so far: -0.9989458866623447
SPANISH: P(i|et) = -0.8377475761077073 ==> log prob of sentence so far: -0.8377475761077073

3GRAM: tie
ENGLISH: P(e|ti) = -1.5553852998310138 ==> log prob of sentence so far: -2.6683044757465733
FRENCH: P(e|ti) = -1.1287066528767673 ==> log prob of sentence so far: -2.378872103329569
DUTCH: P(e|ti) = -0.866375881083796 ==> log prob of sentence so far: -2.3722714295128275
PORTUGUESE: P(e|ti) = -2.591064607026499 ==> log prob of sentence so far: -3.590010493688844
SPANISH: P(e|ti) = -0.69540748974884 ==> log prob of sentence so far: -1.5331550658565474

3GRAM: ien
ENGLISH: P(n|ie) = -0.8538414328807916 ==> log prob of sentence so far: -3.522145908627365
FRENCH: P(n|ie) = -0.407374576625151 ==> log prob of sentence so far: -2.7862466799547203
DUTCH: P(n|ie) = -0.7640480639971381 ==> log prob of sentence so far: -3.1363194935099656
PORTUGUESE: P(n|ie) = -0.634941217722618 ==> log prob of sentence so far: -4.224951711411462
SPANISH: P(n|ie) = -0.29868956840655564 ==> log prob of sentence so far: -1.831844634263103

3GRAM: enn
ENGLISH: P(n|en) = -2.0813427203721244 ==> log prob of sentence so far: -5.603488628999489
FRENCH: P(n|en) = -1.8006731846590913 ==> log prob of sentence so far: -4.586919864613812
DUTCH: P(n|en) = -1.5056810435648615 ==> log prob of sentence so far: -4.642000537074827
PORTUGUESE: P(n|en) = -2.6693884105805874 ==> log prob of sentence so far: -6.89434012199205
SPANISH: P(n|en) = -2.227352062923503 ==> log prob of sentence so far: -4.059196697186606

3GRAM: nne
ENGLISH: P(e|nn) = -0.5507814951358535 ==> log prob of sentence so far: -6.154270124135343
FRENCH: P(e|nn) = -0.25126792865900743 ==> log prob of sentence so far: -4.8381877932728194
DUTCH: P(e|nn) = -0.4424373926696349 ==> log prob of sentence so far: -5.084437929744461
PORTUGUESE: P(e|nn) = -1.400414627799893 ==> log prob of sentence so far: -8.294754749791943
SPANISH: P(e|nn) = -1.082066934285113 ==> log prob of sentence so far: -5.14126363147172

3GRAM: nel
ENGLISH: P(l|ne) = -1.6422024576108396 ==> log prob of sentence so far: -7.796472581746183
FRENCH: P(l|ne) = -1.4206738468176074 ==> log prob of sentence so far: -6.2588616400904264
DUTCH: P(l|ne) = -1.603834176096725 ==> log prob of sentence so far: -6.688272105841186
PORTUGUESE: P(l|ne) = -1.0332929635926007 ==> log prob of sentence so far: -9.328047713384544
SPANISH: P(l|ne) = -0.6914491427996365 ==> log prob of sentence so far: -5.832712774271356

3GRAM: elo
ENGLISH: P(o|el) = -1.0284913035250804 ==> log prob of sentence so far: -8.824963885271263
FRENCH: P(o|el) = -1.1623020489331568 ==> log prob of sentence so far: -7.421163689023583
DUTCH: P(o|el) = -1.2660453186330543 ==> log prob of sentence so far: -7.95431742447424
PORTUGUESE: P(o|el) = -1.1363051246133695 ==> log prob of sentence so far: -10.464352837997914
SPANISH: P(o|el) = -0.8746936353304455 ==> log prob of sentence so far: -6.707406409601802

3GRAM: lov
ENGLISH: P(v|lo) = -1.5902409874322145 ==> log prob of sentence so far: -10.415204872703477
FRENCH: P(v|lo) = -3.0108085975122063 ==> log prob of sentence so far: -10.431972286535789
DUTCH: P(v|lo) = -1.7017888008549453 ==> log prob of sentence so far: -9.656106225329186
PORTUGUESE: P(v|lo) = -2.2426833777952857 ==> log prob of sentence so far: -12.7070362157932
SPANISH: P(v|lo) = -1.975022408655535 ==> log prob of sentence so far: -8.682428818257337

3GRAM: ove
ENGLISH: P(e|ov) = -0.054785174934942456 ==> log prob of sentence so far: -10.469990047638419
FRENCH: P(e|ov) = -0.5880492768851477 ==> log prob of sentence so far: -11.020021563420936
DUTCH: P(e|ov) = -0.06342890120449048 ==> log prob of sentence so far: -9.719535126533676
PORTUGUESE: P(e|ov) = -0.4321641937135228 ==> log prob of sentence so far: -13.139200409506724
SPANISH: P(e|ov) = -0.5622217521868123 ==> log prob of sentence so far: -9.244650570444149

3GRAM: ves
ENGLISH: P(s|ve) = -0.985340464767245 ==> log prob of sentence so far: -11.455330512405663
FRENCH: P(s|ve) = -1.2677150959949515 ==> log prob of sentence so far: -12.287736659415888
DUTCH: P(s|ve) = -2.100954352816585 ==> log prob of sentence so far: -11.820489479350261
PORTUGUESE: P(s|ve) = -0.7562032801869624 ==> log prob of sentence so far: -13.895403689693687
SPANISH: P(s|ve) = -1.1450752000549715 ==> log prob of sentence so far: -10.38972577049912

3GRAM: esv
ENGLISH: P(v|es) = -3.075963850776022 ==> log prob of sentence so far: -14.531294363181685
FRENCH: P(v|es) = -1.8501693273971493 ==> log prob of sentence so far: -14.137905986813037
DUTCH: P(v|es) = -1.9161415546225344 ==> log prob of sentence so far: -13.736631033972795
PORTUGUESE: P(v|es) = -1.9343921237900061 ==> log prob of sentence so far: -15.829795813483694
SPANISH: P(v|es) = -1.9986749644050847 ==> log prob of sentence so far: -12.388400734904206

3GRAM: sve
ENGLISH: P(e|sv) = -0.44761065721112403 ==> log prob of sentence so far: -14.978905020392808
FRENCH: P(e|sv) = -0.5468567248262058 ==> log prob of sentence so far: -14.684762711639243
DUTCH: P(e|sv) = -0.5927996302087581 ==> log prob of sentence so far: -14.329430664181553
PORTUGUESE: P(e|sv) = -0.34802655834020474 ==> log prob of sentence so far: -16.177822371823897
SPANISH: P(e|sv) = -0.47319096109123004 ==> log prob of sentence so far: -12.861591695995436

3GRAM: ver
ENGLISH: P(r|ve) = -0.3855967161664635 ==> log prob of sentence so far: -15.364501736559273
FRENCH: P(r|ve) = -0.5174611042546211 ==> log prob of sentence so far: -15.202223815893865
DUTCH: P(r|ve) = -0.21522907306261535 ==> log prob of sentence so far: -14.544659737244167
PORTUGUESE: P(r|ve) = -0.6222187129968236 ==> log prob of sentence so far: -16.80004108482072
SPANISH: P(r|ve) = -0.48295792082271005 ==> log prob of sentence so far: -13.344549616818146

3GRAM: ero
ENGLISH: P(o|er) = -1.332688208238226 ==> log prob of sentence so far: -16.6971899447975
FRENCH: P(o|er) = -1.5189151162273014 ==> log prob of sentence so far: -16.721138932121164
DUTCH: P(o|er) = -1.4498930589572172 ==> log prob of sentence so far: -15.994552796201384
PORTUGUESE: P(o|er) = -1.3527284571925613 ==> log prob of sentence so far: -18.15276954201328
SPANISH: P(o|er) = -0.6379830917379468 ==> log prob of sentence so far: -13.982532708556093

3GRAM: ron
ENGLISH: P(n|ro) = -1.1323618515057796 ==> log prob of sentence so far: -17.82955179630328
FRENCH: P(n|ro) = -0.8429145538567797 ==> log prob of sentence so far: -17.564053485977944
DUTCH: P(n|ro) = -0.9504513102238623 ==> log prob of sentence so far: -16.945004106425248
PORTUGUESE: P(n|ro) = -0.7963016438986097 ==> log prob of sentence so far: -18.94907118591189
SPANISH: P(n|ro) = -0.6115171237762681 ==> log prob of sentence so far: -14.594049832332361

3GRAM: oni
ENGLISH: P(i|on) = -1.4217738275625775 ==> log prob of sentence so far: -19.25132562386586
FRENCH: P(i|on) = -1.7769662715259649 ==> log prob of sentence so far: -19.341019757503908
DUTCH: P(i|on) = -1.1149879946488386 ==> log prob of sentence so far: -18.059992101074087
PORTUGUESE: P(i|on) = -1.5069907690705466 ==> log prob of sentence so far: -20.456061954982438
SPANISH: P(i|on) = -1.4165097154439321 ==> log prob of sentence so far: -16.010559547776293

3GRAM: niq
ENGLISH: P(q|ni) = -3.0784568180532927 ==> log prob of sentence so far: -22.329782441919154
FRENCH: P(q|ni) = -1.3178101758288643 ==> log prob of sentence so far: -20.658829933332772
DUTCH: P(q|ni) = -3.1912646619703375 ==> log prob of sentence so far: -21.251256763044424
PORTUGUESE: P(q|ni) = -2.345046824648355 ==> log prob of sentence so far: -22.801108779630795
SPANISH: P(q|ni) = -1.8692317197309762 ==> log prob of sentence so far: -17.87979126750727

3GRAM: iqu
ENGLISH: P(u|iq) = -0.10969877005156307 ==> log prob of sentence so far: -22.439481211970715
FRENCH: P(u|iq) = -0.005397714049644559 ==> log prob of sentence so far: -20.664227647382415
DUTCH: P(u|iq) = -0.185234638495551 ==> log prob of sentence so far: -21.436491401539975
PORTUGUESE: P(u|iq) = -0.05289918459683106 ==> log prob of sentence so far: -22.854007964227627
SPANISH: P(u|iq) = -0.04123536058417955 ==> log prob of sentence so far: -17.921026628091447

3GRAM: que
ENGLISH: P(e|qu) = -0.3018595937804336 ==> log prob of sentence so far: -22.74134080575115
FRENCH: P(e|qu) = -0.2536416357055235 ==> log prob of sentence so far: -20.91786928308794
DUTCH: P(e|qu) = -0.31017337510385096 ==> log prob of sentence so far: -21.746664776643826
PORTUGUESE: P(e|qu) = -0.07412635505035695 ==> log prob of sentence so far: -22.928134319277984
SPANISH: P(e|qu) = -0.08958282370969425 ==> log prob of sentence so far: -18.01060945180114

According to the 3gram model, the sentence is in Spanish
----------------
4GRAM MODEL:

4GRAM: etie
ENGLISH: P(e|eti) = -1.5730962953926457 ==> log prob of sentence so far: -1.5730962953926457
FRENCH: P(e|eti) = -1.2223670008495782 ==> log prob of sentence so far: -1.2223670008495782
DUTCH: P(e|eti) = -1.137959246600811 ==> log prob of sentence so far: -1.137959246600811
PORTUGUESE: P(e|eti) = -1.7970365945440174 ==> log prob of sentence so far: -1.7970365945440174
SPANISH: P(e|eti) = -0.5493536408119594 ==> log prob of sentence so far: -0.5493536408119594

4GRAM: tien
ENGLISH: P(n|tie) = -0.951151432462183 ==> log prob of sentence so far: -2.5242477278548288
FRENCH: P(n|tie) = -0.599706085934485 ==> log prob of sentence so far: -1.822073086784063
DUTCH: P(n|tie) = -0.5926184350951209 ==> log prob of sentence so far: -1.730577681695932
PORTUGUESE: P(n|tie) = -1.4471580313422192 ==> log prob of sentence so far: -3.2441946258862364
SPANISH: P(n|tie) = -0.274031251250277 ==> log prob of sentence so far: -0.8233848920622364

4GRAM: ienn
ENGLISH: P(n|ien) = -2.7723217067229196 ==> log prob of sentence so far: -5.296569434577748
FRENCH: P(n|ien) = -1.1792367061973916 ==> log prob of sentence so far: -3.0013097929814547
DUTCH: P(n|ien) = -1.6159216301726762 ==> log prob of sentence so far: -3.346499311868608
PORTUGUESE: P(n|ien) = -2.0569048513364727 ==> log prob of sentence so far: -5.301099477222709
SPANISH: P(n|ien) = -2.028932562598488 ==> log prob of sentence so far: -2.8523174546607244

4GRAM: enne
ENGLISH: P(e|enn) = -0.6690067809585756 ==> log prob of sentence so far: -5.9655762155363234
FRENCH: P(e|enn) = -0.12001284846699084 ==> log prob of sentence so far: -3.1213226414484456
DUTCH: P(e|enn) = -0.9285832616467516 ==> log prob of sentence so far: -4.27508257351536
PORTUGUESE: P(e|enn) = -1.1026623418971477 ==> log prob of sentence so far: -6.403761819119857
SPANISH: P(e|enn) = -0.9242792860618817 ==> log prob of sentence so far: -3.776596740722606

4GRAM: nnel
ENGLISH: P(l|nne) = -1.4285159212861611 ==> log prob of sentence so far: -7.394092136822485
FRENCH: P(l|nne) = -1.133360223125194 ==> log prob of sentence so far: -4.25468286457364
DUTCH: P(l|nne) = -1.9344984512435677 ==> log prob of sentence so far: -6.209581024758927
PORTUGUESE: P(l|nne) = -1.0280287236002434 ==> log prob of sentence so far: -7.4317905427201
SPANISH: P(l|nne) = -1.6989700043360187 ==> log prob of sentence so far: -5.475566745058625

4GRAM: nelo
ENGLISH: P(o|nel) = -0.8968410377149421 ==> log prob of sentence so far: -8.290933174537427
FRENCH: P(o|nel) = -1.1561410321012917 ==> log prob of sentence so far: -5.410823896674932
DUTCH: P(o|nel) = -1.169357876396713 ==> log prob of sentence so far: -7.3789389011556406
PORTUGUESE: P(o|nel) = -2.164352855784437 ==> log prob of sentence so far: -9.596143398504537
SPANISH: P(o|nel) = -1.4411120708428806 ==> log prob of sentence so far: -6.916678815901506

4GRAM: elov
ENGLISH: P(v|elo) = -1.4653828514484184 ==> log prob of sentence so far: -9.756316025985846
FRENCH: P(v|elo) = -3.0145205387579237 ==> log prob of sentence so far: -8.425344435432855
DUTCH: P(v|elo) = -1.2705661445862564 ==> log prob of sentence so far: -8.649505045741897
PORTUGUESE: P(v|elo) = -2.0028856882374884 ==> log prob of sentence so far: -11.599029086742025
SPANISH: P(v|elo) = -1.806179973983887 ==> log prob of sentence so far: -8.722858789885393

4GRAM: love
ENGLISH: P(e|lov) = -0.09076311859744 ==> log prob of sentence so far: -9.847079144583287
FRENCH: P(e|lov) = -1.4771212547196624 ==> log prob of sentence so far: -9.902465690152518
DUTCH: P(e|lov) = -0.1990404571266498 ==> log prob of sentence so far: -8.848545502868546
PORTUGUESE: P(e|lov) = -0.8061799739838872 ==> log prob of sentence so far: -12.405209060725912
SPANISH: P(e|lov) = -0.49090953920529573 ==> log prob of sentence so far: -9.213768329090689

4GRAM: oves
ENGLISH: P(s|ove) = -1.420285884941918 ==> log prob of sentence so far: -11.267365029525205
FRENCH: P(s|ove) = -1.9444826721501687 ==> log prob of sentence so far: -11.846948362302687
DUTCH: P(s|ove) = -2.593286067020457 ==> log prob of sentence so far: -11.441831569889004
PORTUGUESE: P(s|ove) = -1.177612432176214 ==> log prob of sentence so far: -13.582821492902127
SPANISH: P(s|ove) = -1.741939077729199 ==> log prob of sentence so far: -10.955707406819888

4GRAM: vesv
ENGLISH: P(v|ves) = -2.582063362911709 ==> log prob of sentence so far: -13.849428392436915
FRENCH: P(v|ves) = -2.575187844927661 ==> log prob of sentence so far: -14.422136207230349
DUTCH: P(v|ves) = -1.9242792860618816 ==> log prob of sentence so far: -13.366110855950886
PORTUGUESE: P(v|ves) = -1.7288045287995246 ==> log prob of sentence so far: -15.311626021701652
SPANISH: P(v|ves) = -1.8239087409443189 ==> log prob of sentence so far: -12.779616147764207

4GRAM: esve
ENGLISH: P(e|esv) = -0.7085153222422492 ==> log prob of sentence so far: -14.557943714679164
FRENCH: P(e|esv) = -0.493066817665408 ==> log prob of sentence so far: -14.915203024895757
DUTCH: P(e|esv) = -0.9700367766225568 ==> log prob of sentence so far: -14.336147632573443
PORTUGUESE: P(e|esv) = -0.6167832481486687 ==> log prob of sentence so far: -15.928409269850322
SPANISH: P(e|esv) = -0.47466065617200565 ==> log prob of sentence so far: -13.254276803936213

4GRAM: sver
ENGLISH: P(r|sve) = -0.18293068358598671 ==> log prob of sentence so far: -14.74087439826515
FRENCH: P(r|sve) = -0.24061407319232575 ==> log prob of sentence so far: -15.155817098088082
DUTCH: P(r|sve) = -0.11510523389925659 ==> log prob of sentence so far: -14.4512528664727
PORTUGUESE: P(r|sve) = -0.6478174818886375 ==> log prob of sentence so far: -16.57622675173896
SPANISH: P(r|sve) = -0.6887508391543328 ==> log prob of sentence so far: -13.943027643090545

4GRAM: vero
ENGLISH: P(o|ver) = -1.768839832836859 ==> log prob of sentence so far: -16.50971423110201
FRENCH: P(o|ver) = -2.0388919422683296 ==> log prob of sentence so far: -17.19470904035641
DUTCH: P(o|ver) = -1.7047865352330478 ==> log prob of sentence so far: -16.156039401705748
PORTUGUESE: P(o|ver) = -1.5877769722356816 ==> log prob of sentence so far: -18.16400372397464
SPANISH: P(o|ver) = -1.5446072057874978 ==> log prob of sentence so far: -15.487634848878043

4GRAM: eron
ENGLISH: P(n|ero) = -0.9253663817030958 ==> log prob of sentence so far: -17.435080612805105
FRENCH: P(n|ero) = -0.4818529145563464 ==> log prob of sentence so far: -17.676561954912756
DUTCH: P(n|ero) = -0.6595394963796423 ==> log prob of sentence so far: -16.81557889808539
PORTUGUESE: P(n|ero) = -1.4866665726258927 ==> log prob of sentence so far: -19.650670296600534
SPANISH: P(n|ero) = -0.6213651465233491 ==> log prob of sentence so far: -16.108999995401394

4GRAM: roni
ENGLISH: P(i|ron) = -1.1408381471117168 ==> log prob of sentence so far: -18.57591875991682
FRENCH: P(i|ron) = -1.4636797336504643 ==> log prob of sentence so far: -19.140241688563222
DUTCH: P(i|ron) = -2.6954816764901977 ==> log prob of sentence so far: -19.511060574575588
PORTUGUESE: P(i|ron) = -1.6362142675622682 ==> log prob of sentence so far: -21.286884564162804
SPANISH: P(i|ron) = -1.5840866203160615 ==> log prob of sentence so far: -17.693086615717455

4GRAM: oniq
ENGLISH: P(q|oni) = -2.8561244442423 ==> log prob of sentence so far: -21.432043204159122
FRENCH: P(q|oni) = -0.8857158909225209 ==> log prob of sentence so far: -20.02595757948574
DUTCH: P(q|oni) = -2.754348335711019 ==> log prob of sentence so far: -22.265408910286606
PORTUGUESE: P(q|oni) = -2.1072099696478683 ==> log prob of sentence so far: -23.394094533810673
SPANISH: P(q|oni) = -1.7481880270062005 ==> log prob of sentence so far: -19.441274642723656

4GRAM: niqu
ENGLISH: P(u|niq) = -0.7781512503836436 ==> log prob of sentence so far: -22.210194454542766
FRENCH: P(u|niq) = -0.06126965673892266 ==> log prob of sentence so far: -20.087227236224663
DUTCH: P(u|niq) = -0.9700367766225568 ==> log prob of sentence so far: -23.235445686909163
PORTUGUESE: P(u|niq) = -0.9700367766225568 ==> log prob of sentence so far: -24.36413131043323
SPANISH: P(u|niq) = -0.2846395794758118 ==> log prob of sentence so far: -19.725914222199467

4GRAM: ique
ENGLISH: P(e|iqu) = -0.39600550889483793 ==> log prob of sentence so far: -22.606199963437604
FRENCH: P(e|iqu) = -0.09911496774854345 ==> log prob of sentence so far: -20.186342203973208
DUTCH: P(e|iqu) = -0.2891307723642735 ==> log prob of sentence so far: -23.524576459273437
PORTUGUESE: P(e|iqu) = -0.0954184449183104 ==> log prob of sentence so far: -24.45954975535154
SPANISH: P(e|iqu) = -0.21302237745154412 ==> log prob of sentence so far: -19.93893659965101

According to the 4gram model, the sentence is in Spanish
----------------
