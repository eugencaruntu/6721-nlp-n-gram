Trump visits Place des Etats-Unis in Paris.

1GRAM MODEL:

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -1.0342236377842724
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -1.1659667480967535

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -2.2954965390963684
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -2.34999538716721

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -3.847984708054283
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -3.5561304483428313

1GRAM: m
ENGLISH: P(m) = -1.6111104829831362 ==> log prob of sentence so far: -5.459095191037419
FRENCH: P(m) = -1.5129486053385504 ==> log prob of sentence so far: -5.0690790536813815

1GRAM: p
ENGLISH: P(p) = -1.7418254600332643 ==> log prob of sentence so far: -7.200920651070684
FRENCH: P(p) = -1.538635710757223 ==> log prob of sentence so far: -6.6077147644386045

1GRAM: v
ENGLISH: P(v) = -2.043619342325931 ==> log prob of sentence so far: -9.244539993396614
FRENCH: P(v) = -1.82784228094936 ==> log prob of sentence so far: -8.435557045387965

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -10.407065473670524
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -9.570763488056809

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -11.578206486399125
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -10.635211202946465

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -12.740731966673035
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -11.770417645615309

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -13.774955604457308
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -12.936384393712062

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -14.946096617185908
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -14.000832108601719

1GRAM: p
ENGLISH: P(p) = -1.7418254600332643 ==> log prob of sentence so far: -16.687922077219174
FRENCH: P(p) = -1.538635710757223 ==> log prob of sentence so far: -15.539467819358942

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -18.035690962640174
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -16.807680299644463

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -19.122479643682627
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -17.884033394749974

1GRAM: c
ENGLISH: P(c) = -1.626587631644617 ==> log prob of sentence so far: -20.749067275327246
FRENCH: P(c) = -1.492223661080204 ==> log prob of sentence so far: -19.376257055830177

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -21.659138493387154
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -20.143222870352634

1GRAM: d
ENGLISH: P(d) = -1.396179917541683 ==> log prob of sentence so far: -23.055318410928837
FRENCH: P(d) = -1.4202616779892512 ==> log prob of sentence so far: -21.563484548341886

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -23.965389628988746
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -22.330450362864344

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -25.136530641717346
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -23.394898077754

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -26.046601859777255
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -24.161863892276457

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -27.080825497561527
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -25.32783064037321

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -28.16761417860398
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -26.40418373547872

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -29.201837816388252
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -27.570150483575475

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -30.372978829116853
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -28.63459819846513

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -31.925466998074768
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -29.840733259640754

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -33.087063935477396
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -30.963370736255257

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -34.24958941575131
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -32.0985771789241

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -35.420730428479914
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -33.163024893813756

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -36.58325590875383
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -34.2982313364826

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -37.744852846156455
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -35.42086881309711

1GRAM: p
ENGLISH: P(p) = -1.7418254600332643 ==> log prob of sentence so far: -39.48667830618972
FRENCH: P(p) = -1.538635710757223 ==> log prob of sentence so far: -36.959504523854335

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -40.57346698723217
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -38.035857618959845

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -41.83473988854427
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -39.2198862580303

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -42.997265368818184
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -40.355092700699146

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -44.16840638154679
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -41.4195404155888

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: tr
ENGLISH: P(r|t) = -1.5560569993606441 ==> log prob of sentence so far: -1.5560569993606441
FRENCH: P(r|t) = -0.9624973465112773 ==> log prob of sentence so far: -0.9624973465112773

2GRAM: ru
ENGLISH: P(u|r) = -1.7203818473421117 ==> log prob of sentence so far: -3.2764388467027556
FRENCH: P(u|r) = -1.6687978889643627 ==> log prob of sentence so far: -2.63129523547564

2GRAM: um
ENGLISH: P(m|u) = -1.4654834543200839 ==> log prob of sentence so far: -4.741922301022839
FRENCH: P(m|u) = -1.6406795484953514 ==> log prob of sentence so far: -4.271974783970991

2GRAM: mp
ENGLISH: P(p|m) = -1.2993194696217338 ==> log prob of sentence so far: -6.041241770644573
FRENCH: P(p|m) = -1.1678922975743729 ==> log prob of sentence so far: -5.439867081545364

2GRAM: pv
ENGLISH: P(v|p) = -3.691308878051961 ==> log prob of sentence so far: -9.732550648696535
FRENCH: P(v|p) = -3.205529151061801 ==> log prob of sentence so far: -8.645396232607165

2GRAM: vi
ENGLISH: P(i|v) = -0.7631927622867011 ==> log prob of sentence so far: -10.495743410983236
FRENCH: P(i|v) = -0.7216945576421682 ==> log prob of sentence so far: -9.367090790249334

2GRAM: is
ENGLISH: P(s|i) = -0.8699344462536623 ==> log prob of sentence so far: -11.365677857236898
FRENCH: P(s|i) = -0.8503045030811127 ==> log prob of sentence so far: -10.217395293330446

2GRAM: si
ENGLISH: P(i|s) = -1.1208857310233835 ==> log prob of sentence so far: -12.486563588260282
FRENCH: P(i|s) = -1.184885635307546 ==> log prob of sentence so far: -11.402280928637992

2GRAM: it
ENGLISH: P(t|i) = -0.9082997757934181 ==> log prob of sentence so far: -13.3948633640537
FRENCH: P(t|i) = -0.7742072823534025 ==> log prob of sentence so far: -12.176488210991394

2GRAM: ts
ENGLISH: P(s|t) = -1.3509351575782058 ==> log prob of sentence so far: -14.745798521631906
FRENCH: P(s|t) = -1.2204919099668523 ==> log prob of sentence so far: -13.396980120958247

2GRAM: sp
ENGLISH: P(p|s) = -1.4078823004928671 ==> log prob of sentence so far: -16.153680822124773
FRENCH: P(p|s) = -1.237330102985858 ==> log prob of sentence so far: -14.634310223944105

2GRAM: pl
ENGLISH: P(l|p) = -1.1219349684369153 ==> log prob of sentence so far: -17.275615790561687
FRENCH: P(l|p) = -1.0311785535824212 ==> log prob of sentence so far: -15.665488777526527

2GRAM: la
ENGLISH: P(a|l) = -1.002845754477944 ==> log prob of sentence so far: -18.278461545039633
FRENCH: P(a|l) = -0.6764237550347996 ==> log prob of sentence so far: -16.341912532561327

2GRAM: ac
ENGLISH: P(c|a) = -1.4314000466535202 ==> log prob of sentence so far: -19.709861591693155
FRENCH: P(c|a) = -1.3380717042938233 ==> log prob of sentence so far: -17.67998423685515

2GRAM: ce
ENGLISH: P(e|c) = -0.8165379480901575 ==> log prob of sentence so far: -20.526399539783313
FRENCH: P(e|c) = -0.522619213200064 ==> log prob of sentence so far: -18.202603450055214

2GRAM: ed
ENGLISH: P(d|e) = -1.0374576111626355 ==> log prob of sentence so far: -21.56385715094595
FRENCH: P(d|e) = -1.2790847979127375 ==> log prob of sentence so far: -19.481688247967952

2GRAM: de
ENGLISH: P(e|d) = -0.8320988657918726 ==> log prob of sentence so far: -22.395956016737824
FRENCH: P(e|d) = -0.290519189469436 ==> log prob of sentence so far: -19.772207437437388

2GRAM: es
ENGLISH: P(s|e) = -0.944867640322014 ==> log prob of sentence so far: -23.340823657059836
FRENCH: P(s|e) = -0.7261499946209886 ==> log prob of sentence so far: -20.498357432058377

2GRAM: se
ENGLISH: P(e|s) = -0.9357350813780119 ==> log prob of sentence so far: -24.27655873843785
FRENCH: P(e|s) = -0.7710764417175806 ==> log prob of sentence so far: -21.26943387377596

2GRAM: et
ENGLISH: P(t|e) = -1.1805749996736075 ==> log prob of sentence so far: -25.457133738111455
FRENCH: P(t|e) = -1.081730001747917 ==> log prob of sentence so far: -22.351163875523877

2GRAM: ta
ENGLISH: P(a|t) = -1.1697948806890075 ==> log prob of sentence so far: -26.626928618800463
FRENCH: P(a|t) = -0.8827723186309387 ==> log prob of sentence so far: -23.233936194154815

2GRAM: at
ENGLISH: P(t|a) = -0.8627525070949107 ==> log prob of sentence so far: -27.489681125895373
FRENCH: P(t|a) = -1.2479713976288138 ==> log prob of sentence so far: -24.481907591783628

2GRAM: ts
ENGLISH: P(s|t) = -1.3509351575782058 ==> log prob of sentence so far: -28.84061628347358
FRENCH: P(s|t) = -1.2204919099668523 ==> log prob of sentence so far: -25.70239950175048

2GRAM: su
ENGLISH: P(u|s) = -1.4481235192711146 ==> log prob of sentence so far: -30.288739802744693
FRENCH: P(u|s) = -1.3202306641102985 ==> log prob of sentence so far: -27.02263016586078

2GRAM: un
ENGLISH: P(n|u) = -0.9099582558407868 ==> log prob of sentence so far: -31.19869805858548
FRENCH: P(n|u) = -0.9913278927203765 ==> log prob of sentence so far: -28.013958058581157

2GRAM: ni
ENGLISH: P(i|n) = -1.3332322954338982 ==> log prob of sentence so far: -32.53193035401938
FRENCH: P(i|n) = -1.4845526185364621 ==> log prob of sentence so far: -29.49851067711762

2GRAM: is
ENGLISH: P(s|i) = -0.8699344462536623 ==> log prob of sentence so far: -33.401864800273046
FRENCH: P(s|i) = -0.8503045030811127 ==> log prob of sentence so far: -30.34881518019873

2GRAM: si
ENGLISH: P(i|s) = -1.1208857310233835 ==> log prob of sentence so far: -34.52275053129643
FRENCH: P(i|s) = -1.184885635307546 ==> log prob of sentence so far: -31.533700815506275

2GRAM: in
ENGLISH: P(n|i) = -0.5177328887875342 ==> log prob of sentence so far: -35.040483420083966
FRENCH: P(n|i) = -0.8962746820270037 ==> log prob of sentence so far: -32.42997549753328

2GRAM: np
ENGLISH: P(p|n) = -2.2279170156247026 ==> log prob of sentence so far: -37.26840043570867
FRENCH: P(p|n) = -1.8907933448423553 ==> log prob of sentence so far: -34.320768842375635

2GRAM: pa
ENGLISH: P(a|p) = -0.9079158130990952 ==> log prob of sentence so far: -38.176316248807765
FRENCH: P(a|p) = -0.6470605885380062 ==> log prob of sentence so far: -34.96782943091364

2GRAM: ar
ENGLISH: P(r|a) = -0.9854656313970852 ==> log prob of sentence so far: -39.16178188020485
FRENCH: P(r|a) = -1.0331920938948058 ==> log prob of sentence so far: -36.00102152480844

2GRAM: ri
ENGLISH: P(i|r) = -1.011900400480812 ==> log prob of sentence so far: -40.17368228068566
FRENCH: P(i|r) = -1.089298477748147 ==> log prob of sentence so far: -37.09032000255659

2GRAM: is
ENGLISH: P(s|i) = -0.8699344462536623 ==> log prob of sentence so far: -41.04361672693933
FRENCH: P(s|i) = -0.8503045030811127 ==> log prob of sentence so far: -37.9406245056377

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: tru
ENGLISH: P(u|tr) = -0.8531550108252123 ==> log prob of sentence so far: -0.8531550108252123
FRENCH: P(u|tr) = -1.695515514542488 ==> log prob of sentence so far: -1.695515514542488

3GRAM: rum
ENGLISH: P(m|ru) = -1.3363057560125244 ==> log prob of sentence so far: -2.189460766837737
FRENCH: P(m|ru) = -1.1605907908877482 ==> log prob of sentence so far: -2.8561063054302362

3GRAM: ump
ENGLISH: P(p|um) = -0.7049111979734239 ==> log prob of sentence so far: -2.8943719648111608
FRENCH: P(p|um) = -2.256148982659935 ==> log prob of sentence so far: -5.1122552880901715

3GRAM: mpv
ENGLISH: P(v|mp) = -3.3647385550553985 ==> log prob of sentence so far: -6.259110519866559
FRENCH: P(v|mp) = -2.9876662649262746 ==> log prob of sentence so far: -8.099921553016447

3GRAM: pvi
ENGLISH: P(i|pv) = -1.0280287236002434 ==> log prob of sentence so far: -7.287139243466803
FRENCH: P(i|pv) = -0.5228787452803376 ==> log prob of sentence so far: -8.622800298296784

3GRAM: vis
ENGLISH: P(s|vi) = -1.0870222821799054 ==> log prob of sentence so far: -8.37416152564671
FRENCH: P(s|vi) = -0.8651538073105368 ==> log prob of sentence so far: -9.48795410560732

3GRAM: isi
ENGLISH: P(i|is) = -1.1763261609717497 ==> log prob of sentence so far: -9.550487686618458
FRENCH: P(i|is) = -1.2011987083102265 ==> log prob of sentence so far: -10.689152813917547

3GRAM: sit
ENGLISH: P(t|si) = -0.9193597135638597 ==> log prob of sentence so far: -10.469847400182319
FRENCH: P(t|si) = -1.0643453755916699 ==> log prob of sentence so far: -11.753498189509216

3GRAM: its
ENGLISH: P(s|it) = -0.9558176939772731 ==> log prob of sentence so far: -11.425665094159593
FRENCH: P(s|it) = -1.2340318679540005 ==> log prob of sentence so far: -12.987530057463216

3GRAM: tsp
ENGLISH: P(p|ts) = -1.4289589941875929 ==> log prob of sentence so far: -12.854624088347185
FRENCH: P(p|ts) = -1.4769647239982335 ==> log prob of sentence so far: -14.46449478146145

3GRAM: spl
ENGLISH: P(l|sp) = -1.2368113395568796 ==> log prob of sentence so far: -14.091435427904065
FRENCH: P(l|sp) = -0.9577436198632373 ==> log prob of sentence so far: -15.422238401324687

3GRAM: pla
ENGLISH: P(a|pl) = -0.3800125254043627 ==> log prob of sentence so far: -14.471447953308427
FRENCH: P(a|pl) = -0.6214281531944322 ==> log prob of sentence so far: -16.04366655451912

3GRAM: lac
ENGLISH: P(c|la) = -1.015840052274403 ==> log prob of sentence so far: -15.48728800558283
FRENCH: P(c|la) = -1.0811271376556668 ==> log prob of sentence so far: -17.124793692174787

3GRAM: ace
ENGLISH: P(e|ac) = -0.6750618735632957 ==> log prob of sentence so far: -16.162349879146124
FRENCH: P(e|ac) = -0.508669718735406 ==> log prob of sentence so far: -17.633463410910192

3GRAM: ced
ENGLISH: P(d|ce) = -1.050873881628948 ==> log prob of sentence so far: -17.213223760775072
FRENCH: P(d|ce) = -1.1561837417583283 ==> log prob of sentence so far: -18.789647152668522

3GRAM: ede
ENGLISH: P(e|ed) = -1.203762979362576 ==> log prob of sentence so far: -18.41698674013765
FRENCH: P(e|ed) = -0.26826572538935256 ==> log prob of sentence so far: -19.057912878057873

3GRAM: des
ENGLISH: P(s|de) = -0.9611523087474174 ==> log prob of sentence so far: -19.378139048885068
FRENCH: P(s|de) = -0.5850790411375858 ==> log prob of sentence so far: -19.64299191919546

3GRAM: ese
ENGLISH: P(e|es) = -0.9490134133447401 ==> log prob of sentence so far: -20.327152462229808
FRENCH: P(e|es) = -0.9180499442018412 ==> log prob of sentence so far: -20.5610418633973

3GRAM: set
ENGLISH: P(t|se) = -1.1697780566749647 ==> log prob of sentence so far: -21.496930518904772
FRENCH: P(t|se) = -0.7895369875752213 ==> log prob of sentence so far: -21.35057885097252

3GRAM: eta
ENGLISH: P(a|et) = -1.2738884031014208 ==> log prob of sentence so far: -22.770818922006193
FRENCH: P(a|et) = -0.8006581161550461 ==> log prob of sentence so far: -22.151236967127566

3GRAM: tat
ENGLISH: P(t|ta) = -1.1668290094491693 ==> log prob of sentence so far: -23.937647931455363
FRENCH: P(t|ta) = -1.4643408370823485 ==> log prob of sentence so far: -23.615577804209916

3GRAM: ats
ENGLISH: P(s|at) = -1.0875568628002064 ==> log prob of sentence so far: -25.02520479425557
FRENCH: P(s|at) = -1.7883137770778146 ==> log prob of sentence so far: -25.40389158128773

3GRAM: tsu
ENGLISH: P(u|ts) = -1.3205169826300331 ==> log prob of sentence so far: -26.3457217768856
FRENCH: P(u|ts) = -0.8984184344345886 ==> log prob of sentence so far: -26.30231001572232

3GRAM: sun
ENGLISH: P(n|su) = -0.8815943466158389 ==> log prob of sentence so far: -27.22731612350144
FRENCH: P(n|su) = -0.7160953649259377 ==> log prob of sentence so far: -27.018405380648257

3GRAM: uni
ENGLISH: P(i|un) = -1.4510019569323358 ==> log prob of sentence so far: -28.678318080433776
FRENCH: P(i|un) = -1.286865909306583 ==> log prob of sentence so far: -28.30527128995484

3GRAM: nis
ENGLISH: P(s|ni) = -1.0117582676302972 ==> log prob of sentence so far: -29.690076348064075
FRENCH: P(s|ni) = -1.1869892569946099 ==> log prob of sentence so far: -29.492260546949453

3GRAM: isi
ENGLISH: P(i|is) = -1.1763261609717497 ==> log prob of sentence so far: -30.866402509035826
FRENCH: P(i|is) = -1.2011987083102265 ==> log prob of sentence so far: -30.69345925525968

3GRAM: sin
ENGLISH: P(n|si) = -0.5098459021801978 ==> log prob of sentence so far: -31.376248411216025
FRENCH: P(n|si) = -0.9099008084674817 ==> log prob of sentence so far: -31.60336006372716

3GRAM: inp
ENGLISH: P(p|in) = -2.1580062209662434 ==> log prob of sentence so far: -33.53425463218227
FRENCH: P(p|in) = -2.2556121957595785 ==> log prob of sentence so far: -33.858972259486734

3GRAM: npa
ENGLISH: P(a|np) = -0.7859365071903064 ==> log prob of sentence so far: -34.320191139372575
FRENCH: P(a|np) = -0.6702841185908591 ==> log prob of sentence so far: -34.52925637807759

3GRAM: par
ENGLISH: P(r|pa) = -0.48565020314281593 ==> log prob of sentence so far: -34.80584134251539
FRENCH: P(r|pa) = -0.3235383556648062 ==> log prob of sentence so far: -34.8527947337424

3GRAM: ari
ENGLISH: P(i|ar) = -1.1814402876075736 ==> log prob of sentence so far: -35.987281630122965
FRENCH: P(i|ar) = -1.1151132512981732 ==> log prob of sentence so far: -35.967907985040576

3GRAM: ris
ENGLISH: P(s|ri) = -1.066497711756112 ==> log prob of sentence so far: -37.05377934187908
FRENCH: P(s|ri) = -0.8417457604585549 ==> log prob of sentence so far: -36.80965374549913

According to the 3gram model, the sentence is in French
----------------
4GRAM MODEL:

4GRAM: trum
ENGLISH: P(m|tru) = -1.3035046147918365 ==> log prob of sentence so far: -1.3035046147918365
FRENCH: P(m|tru) = -0.5394131821900733 ==> log prob of sentence so far: -0.5394131821900733

4GRAM: rump
ENGLISH: P(p|rum) = -0.5730962953926458 ==> log prob of sentence so far: -1.8766009101844823
FRENCH: P(p|rum) = -2.2041199826559246 ==> log prob of sentence so far: -2.743533164845998

4GRAM: umpv
ENGLISH: P(v|ump) = -2.57978359661681 ==> log prob of sentence so far: -4.456384506801292
FRENCH: P(v|ump) = -1.5563025007672873 ==> log prob of sentence so far: -4.299835665613285

4GRAM: mpvi
ENGLISH: P(i|mpv) = -1.4313637641589874 ==> log prob of sentence so far: -5.88774827096028
FRENCH: P(i|mpv) = -0.9700367766225568 ==> log prob of sentence so far: -5.269872442235842

4GRAM: pvis
ENGLISH: P(s|pvi) = -0.9700367766225568 ==> log prob of sentence so far: -6.857785047582837
FRENCH: P(s|pvi) = -1.1249387366083 ==> log prob of sentence so far: -6.394811178844142

4GRAM: visi
ENGLISH: P(i|vis) = -0.1401026778726058 ==> log prob of sentence so far: -6.997887725455443
FRENCH: P(i|vis) = -0.4656008316898512 ==> log prob of sentence so far: -6.860412010533993

4GRAM: isit
ENGLISH: P(t|isi) = -0.72956397155456 ==> log prob of sentence so far: -7.727451697010003
FRENCH: P(t|isi) = -0.7955253464530664 ==> log prob of sentence so far: -7.65593735698706

4GRAM: sits
ENGLISH: P(s|sit) = -0.8602374870746757 ==> log prob of sentence so far: -8.58768918408468
FRENCH: P(s|sit) = -1.8846065812979305 ==> log prob of sentence so far: -9.54054393828499

4GRAM: itsp
ENGLISH: P(p|its) = -1.4024597961405665 ==> log prob of sentence so far: -9.990148980225246
FRENCH: P(p|its) = -1.5695039573722396 ==> log prob of sentence so far: -11.11004789565723

4GRAM: tspl
ENGLISH: P(l|tsp) = -1.1153934187020695 ==> log prob of sentence so far: -11.105542398927316
FRENCH: P(l|tsp) = -1.146128035678238 ==> log prob of sentence so far: -12.256175931335468

4GRAM: spla
ENGLISH: P(a|spl) = -0.29821901833015274 ==> log prob of sentence so far: -11.403761417257469
FRENCH: P(a|spl) = -0.6020599913279624 ==> log prob of sentence so far: -12.85823592266343

4GRAM: plac
ENGLISH: P(c|pla) = -0.44098678033573596 ==> log prob of sentence so far: -11.844748197593205
FRENCH: P(c|pla) = -0.6624759137996179 ==> log prob of sentence so far: -13.520711836463049

4GRAM: lace
ENGLISH: P(e|lac) = -0.31152071062293474 ==> log prob of sentence so far: -12.15626890821614
FRENCH: P(e|lac) = -0.5290546977537546 ==> log prob of sentence so far: -14.049766534216802

4GRAM: aced
ENGLISH: P(d|ace) = -0.9038279557029067 ==> log prob of sentence so far: -13.060096863919046
FRENCH: P(d|ace) = -0.7368066571895786 ==> log prob of sentence so far: -14.786573191406381

4GRAM: cede
ENGLISH: P(e|ced) = -1.192220759356217 ==> log prob of sentence so far: -14.252317623275264
FRENCH: P(e|ced) = -0.1663922515841051 ==> log prob of sentence so far: -14.952965442990486

4GRAM: edes
ENGLISH: P(s|ede) = -0.9828319748629861 ==> log prob of sentence so far: -15.23514959813825
FRENCH: P(s|ede) = -0.5666952840536525 ==> log prob of sentence so far: -15.519660727044139

4GRAM: dese
ENGLISH: P(e|des) = -1.2837610429147437 ==> log prob of sentence so far: -16.518910641052994
FRENCH: P(e|des) = -0.9139763994443542 ==> log prob of sentence so far: -16.43363712648849

4GRAM: eset
ENGLISH: P(t|ese) = -1.1984345918806996 ==> log prob of sentence so far: -17.717345232933692
FRENCH: P(t|ese) = -0.5684408662054619 ==> log prob of sentence so far: -17.002077992693955

4GRAM: seta
ENGLISH: P(a|set) = -1.273795332980608 ==> log prob of sentence so far: -18.9911405659143
FRENCH: P(a|set) = -0.7816417348695821 ==> log prob of sentence so far: -17.783719727563536

4GRAM: etat
ENGLISH: P(t|eta) = -1.5446072057874978 ==> log prob of sentence so far: -20.535747771701796
FRENCH: P(t|eta) = -1.5317595596897613 ==> log prob of sentence so far: -19.315479287253297

4GRAM: tats
ENGLISH: P(s|tat) = -1.4319180647854823 ==> log prob of sentence so far: -21.967665836487278
FRENCH: P(s|tat) = -1.2120889123272 ==> log prob of sentence so far: -20.527568199580497

4GRAM: atsu
ENGLISH: P(u|ats) = -1.2463608994937627 ==> log prob of sentence so far: -23.21402673598104
FRENCH: P(u|ats) = -1.26884531229258 ==> log prob of sentence so far: -21.796413511873077

4GRAM: tsun
ENGLISH: P(n|tsu) = -0.7921619648264583 ==> log prob of sentence so far: -24.0061887008075
FRENCH: P(n|tsu) = -1.5821830197472648 ==> log prob of sentence so far: -23.378596531620342

4GRAM: suni
ENGLISH: P(i|sun) = -1.3758464363091558 ==> log prob of sentence so far: -25.382035137116656
FRENCH: P(i|sun) = -1.4341013341179747 ==> log prob of sentence so far: -24.81269786573832

4GRAM: unis
ENGLISH: P(s|uni) = -1.0894004112293108 ==> log prob of sentence so far: -26.471435548345966
FRENCH: P(s|uni) = -0.9408785478813434 ==> log prob of sentence so far: -25.753576413619662

4GRAM: nisi
ENGLISH: P(i|nis) = -1.541872785344646 ==> log prob of sentence so far: -28.01330833369061
FRENCH: P(i|nis) = -2.3944516808262164 ==> log prob of sentence so far: -28.148028094445877

4GRAM: isin
ENGLISH: P(n|isi) = -0.4337103718784951 ==> log prob of sentence so far: -28.447018705569104
FRENCH: P(n|isi) = -1.0437645888251685 ==> log prob of sentence so far: -29.191792683271046

4GRAM: sinp
ENGLISH: P(p|sin) = -2.169024408341792 ==> log prob of sentence so far: -30.616043113910894
FRENCH: P(p|sin) = -2.9858753573083936 ==> log prob of sentence so far: -32.17766804057944

4GRAM: inpa
ENGLISH: P(a|inp) = -0.8585078543392031 ==> log prob of sentence so far: -31.474550968250096
FRENCH: P(a|inp) = -0.8683278807327317 ==> log prob of sentence so far: -33.04599592131217

4GRAM: npar
ENGLISH: P(r|npa) = -0.4471580313422192 ==> log prob of sentence so far: -31.921708999592315
FRENCH: P(r|npa) = -0.375835441651318 ==> log prob of sentence so far: -33.42183136296349

4GRAM: pari
ENGLISH: P(i|par) = -1.4274003612462587 ==> log prob of sentence so far: -33.349109360838575
FRENCH: P(i|par) = -1.738860805062924 ==> log prob of sentence so far: -35.160692168026415

4GRAM: aris
ENGLISH: P(s|ari) = -1.2601522697213927 ==> log prob of sentence so far: -34.60926163055997
FRENCH: P(s|ari) = -1.1043647920658737 ==> log prob of sentence so far: -36.26505696009229

According to the 4gram model, the sentence is in English
----------------
