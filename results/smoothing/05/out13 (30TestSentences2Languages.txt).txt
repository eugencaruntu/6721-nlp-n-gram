Protesters chant "Ne vous trumpez pas!".

1GRAM MODEL:

1GRAM: p
ENGLISH: P(p) = -1.7418254600332643 ==> log prob of sentence so far: -1.7418254600332643
FRENCH: P(p) = -1.538635710757223 ==> log prob of sentence so far: -1.538635710757223

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -3.0030983613453603
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -2.7226643498276797

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -4.140882121930855
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -3.9970076801453347

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -5.1751057597151275
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -5.162974428242088

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -6.085176977775035
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -5.929940242764545

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -7.256317990503636
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -6.994387957654201

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -8.290541628287908
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -8.160354705750954

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -9.200612846347815
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -8.927320520273412

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -10.46188574765991
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -10.111349159343868

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -11.63302676038851
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -11.175796874233525

1GRAM: c
ENGLISH: P(c) = -1.626587631644617 ==> log prob of sentence so far: -13.259614392033127
FRENCH: P(c) = -1.492223661080204 ==> log prob of sentence so far: -12.66802053531373

1GRAM: h
ENGLISH: P(h) = -1.180274162400034 ==> log prob of sentence so far: -14.43988855443316
FRENCH: P(h) = -2.1056297044472685 ==> log prob of sentence so far: -14.773650239760999

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -15.526677235475615
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -15.850003334866507

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -16.688274172878245
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -16.97264081148101

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -17.722497810662517
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -18.138607559577764

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -18.884094748065145
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -19.261245036192268

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -19.794165966125053
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -20.028210850714725

1GRAM: v
ENGLISH: P(v) = -2.043619342325931 ==> log prob of sentence so far: -21.837785308450982
FRENCH: P(v) = -1.82784228094936 ==> log prob of sentence so far: -21.856053131664083

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -22.975569069036478
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -23.13039646198174

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -24.528057237994393
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -24.336531523157362

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -25.699198250722993
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -25.400979238047018

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -26.733421888507266
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -26.56694598614377

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -27.994694789819363
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -27.75097462521423

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -29.547182958777277
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -28.957109686389852

1GRAM: m
ENGLISH: P(m) = -1.6111104829831362 ==> log prob of sentence so far: -31.158293441760414
FRENCH: P(m) = -1.5129486053385504 ==> log prob of sentence so far: -30.470058291728403

1GRAM: p
ENGLISH: P(p) = -1.7418254600332643 ==> log prob of sentence so far: -32.900118901793675
FRENCH: P(p) = -1.538635710757223 ==> log prob of sentence so far: -32.008694002485626

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -33.81019011985358
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -32.77565981700808

1GRAM: z
ENGLISH: P(z) = -3.1788309635150704 ==> log prob of sentence so far: -36.98902108336865
FRENCH: P(z) = -2.82237675112463 ==> log prob of sentence so far: -35.59803656813271

1GRAM: p
ENGLISH: P(p) = -1.7418254600332643 ==> log prob of sentence so far: -38.73084654340191
FRENCH: P(p) = -1.538635710757223 ==> log prob of sentence so far: -37.13667227888993

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -39.817635224444366
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -38.21302537399544

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -40.98877623717297
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -39.277473088885095

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: pr
ENGLISH: P(r|p) = -0.9816755413902514 ==> log prob of sentence so far: -0.9816755413902514
FRENCH: P(r|p) = -0.8315144107698896 ==> log prob of sentence so far: -0.8315144107698896

2GRAM: ro
ENGLISH: P(o|r) = -0.9679454985579867 ==> log prob of sentence so far: -1.949621039948238
FRENCH: P(o|r) = -1.1139096014103482 ==> log prob of sentence so far: -1.9454240121802377

2GRAM: ot
ENGLISH: P(t|o) = -1.1641322248007255 ==> log prob of sentence so far: -3.1137532647489636
FRENCH: P(t|o) = -1.4142544440361249 ==> log prob of sentence so far: -3.3596784562163626

2GRAM: te
ENGLISH: P(e|t) = -1.0589086136007413 ==> log prob of sentence so far: -4.172661878349705
FRENCH: P(e|t) = -0.674136919284091 ==> log prob of sentence so far: -4.0338153755004535

2GRAM: es
ENGLISH: P(s|e) = -0.944867640322014 ==> log prob of sentence so far: -5.117529518671718
FRENCH: P(s|e) = -0.7261499946209886 ==> log prob of sentence so far: -4.759965370121442

2GRAM: st
ENGLISH: P(t|s) = -0.7036883326029575 ==> log prob of sentence so far: -5.8212178512746755
FRENCH: P(t|s) = -1.237330102985858 ==> log prob of sentence so far: -5.9972954731073

2GRAM: te
ENGLISH: P(e|t) = -1.0589086136007413 ==> log prob of sentence so far: -6.880126464875417
FRENCH: P(e|t) = -0.674136919284091 ==> log prob of sentence so far: -6.671432392391392

2GRAM: er
ENGLISH: P(r|e) = -0.8561613604222861 ==> log prob of sentence so far: -7.736287825297703
FRENCH: P(r|e) = -1.0504664166531121 ==> log prob of sentence so far: -7.721898809044504

2GRAM: rs
ENGLISH: P(s|r) = -1.1637455734200046 ==> log prob of sentence so far: -8.900033398717708
FRENCH: P(s|r) = -1.1996969344915898 ==> log prob of sentence so far: -8.921595743536093

2GRAM: sc
ENGLISH: P(c|s) = -1.566928188574753 ==> log prob of sentence so far: -10.466961587292461
FRENCH: P(c|s) = -1.3370330161780848 ==> log prob of sentence so far: -10.258628759714178

2GRAM: ch
ENGLISH: P(h|c) = -0.7747616737579107 ==> log prob of sentence so far: -11.241723261050373
FRENCH: P(h|c) = -0.9481233380135414 ==> log prob of sentence so far: -11.206752097727719

2GRAM: ha
ENGLISH: P(a|h) = -0.693585657435816 ==> log prob of sentence so far: -11.93530891848619
FRENCH: P(a|h) = -0.5247438164739214 ==> log prob of sentence so far: -11.731495914201641

2GRAM: an
ENGLISH: P(n|a) = -0.704528505817304 ==> log prob of sentence so far: -12.639837424303494
FRENCH: P(n|a) = -0.8168159428818009 ==> log prob of sentence so far: -12.548311857083442

2GRAM: nt
ENGLISH: P(t|n) = -0.8262590589426815 ==> log prob of sentence so far: -13.466096483246176
FRENCH: P(t|n) = -0.6409503859281479 ==> log prob of sentence so far: -13.18926224301159

2GRAM: tn
ENGLISH: P(n|t) = -2.1962581332348874 ==> log prob of sentence so far: -15.662354616481064
FRENCH: P(n|t) = -1.9480285591208415 ==> log prob of sentence so far: -15.137290802132432

2GRAM: ne
ENGLISH: P(e|n) = -1.0532701120323171 ==> log prob of sentence so far: -16.715624728513383
FRENCH: P(e|n) = -0.7543075705657655 ==> log prob of sentence so far: -15.891598372698198

2GRAM: ev
ENGLISH: P(v|e) = -1.6835607871993943 ==> log prob of sentence so far: -18.399185515712777
FRENCH: P(v|e) = -1.6625507306840055 ==> log prob of sentence so far: -17.554149103382205

2GRAM: vo
ENGLISH: P(o|v) = -1.222683931569715 ==> log prob of sentence so far: -19.621869447282492
FRENCH: P(o|v) = -0.7098703324399372 ==> log prob of sentence so far: -18.264019435822142

2GRAM: ou
ENGLISH: P(u|o) = -0.9046394432126483 ==> log prob of sentence so far: -20.52650889049514
FRENCH: P(u|o) = -0.6277892700441801 ==> log prob of sentence so far: -18.89180870586632

2GRAM: us
ENGLISH: P(s|u) = -0.9123383198503607 ==> log prob of sentence so far: -21.4388472103455
FRENCH: P(s|u) = -0.8445087767932609 ==> log prob of sentence so far: -19.736317482659583

2GRAM: st
ENGLISH: P(t|s) = -0.7036883326029575 ==> log prob of sentence so far: -22.14253554294846
FRENCH: P(t|s) = -1.237330102985858 ==> log prob of sentence so far: -20.97364758564544

2GRAM: tr
ENGLISH: P(r|t) = -1.5560569993606441 ==> log prob of sentence so far: -23.698592542309104
FRENCH: P(r|t) = -0.9624973465112773 ==> log prob of sentence so far: -21.93614493215672

2GRAM: ru
ENGLISH: P(u|r) = -1.7203818473421117 ==> log prob of sentence so far: -25.418974389651215
FRENCH: P(u|r) = -1.6687978889643627 ==> log prob of sentence so far: -23.60494282112108

2GRAM: um
ENGLISH: P(m|u) = -1.4654834543200839 ==> log prob of sentence so far: -26.884457843971298
FRENCH: P(m|u) = -1.6406795484953514 ==> log prob of sentence so far: -25.245622369616434

2GRAM: mp
ENGLISH: P(p|m) = -1.2993194696217338 ==> log prob of sentence so far: -28.18377731359303
FRENCH: P(p|m) = -1.1678922975743729 ==> log prob of sentence so far: -26.413514667190807

2GRAM: pe
ENGLISH: P(e|p) = -0.6898842496499311 ==> log prob of sentence so far: -28.873661563242962
FRENCH: P(e|p) = -0.8022713252746899 ==> log prob of sentence so far: -27.215785992465495

2GRAM: ez
ENGLISH: P(z|e) = -3.2331908433056555 ==> log prob of sentence so far: -32.10685240654862
FRENCH: P(z|e) = -2.2452353228389517 ==> log prob of sentence so far: -29.461021315304446

2GRAM: zp
ENGLISH: P(p|z) = -3.1112625136590655 ==> log prob of sentence so far: -35.21811492020768
FRENCH: P(p|z) = -1.1247986190133104 ==> log prob of sentence so far: -30.585819934317758

2GRAM: pa
ENGLISH: P(a|p) = -0.9079158130990952 ==> log prob of sentence so far: -36.12603073330678
FRENCH: P(a|p) = -0.6470605885380062 ==> log prob of sentence so far: -31.232880522855766

2GRAM: as
ENGLISH: P(s|a) = -0.9959554224973615 ==> log prob of sentence so far: -37.12198615580414
FRENCH: P(s|a) = -1.2507587495318242 ==> log prob of sentence so far: -32.48363927238759

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: pro
ENGLISH: P(o|pr) = -0.4522375568714942 ==> log prob of sentence so far: -0.4522375568714942
FRENCH: P(o|pr) = -0.46779056221038884 ==> log prob of sentence so far: -0.46779056221038884

3GRAM: rot
ENGLISH: P(t|ro) = -1.7403234028252674 ==> log prob of sentence so far: -2.1925609596967615
FRENCH: P(t|ro) = -1.8041832142278118 ==> log prob of sentence so far: -2.271973776438201

3GRAM: ote
ENGLISH: P(e|ot) = -1.3850973410776328 ==> log prob of sentence so far: -3.5776583007743943
FRENCH: P(e|ot) = -0.7169866059354433 ==> log prob of sentence so far: -2.9889603823736444

3GRAM: tes
ENGLISH: P(s|te) = -1.256377062978169 ==> log prob of sentence so far: -4.834035363752563
FRENCH: P(s|te) = -0.8101324027353374 ==> log prob of sentence so far: -3.7990927851089817

3GRAM: est
ENGLISH: P(t|es) = -0.723965199933241 ==> log prob of sentence so far: -5.5580005636858045
FRENCH: P(t|es) = -1.074267392428957 ==> log prob of sentence so far: -4.873360177537939

3GRAM: ste
ENGLISH: P(e|st) = -1.0419178187504745 ==> log prob of sentence so far: -6.599918382436279
FRENCH: P(e|st) = -0.630699393934714 ==> log prob of sentence so far: -5.504059571472653

3GRAM: ter
ENGLISH: P(r|te) = -0.4558490196269567 ==> log prob of sentence so far: -7.0557674020632355
FRENCH: P(r|te) = -0.8798178774714153 ==> log prob of sentence so far: -6.383877448944069

3GRAM: ers
ENGLISH: P(s|er) = -0.9536294514539049 ==> log prob of sentence so far: -8.00939685351714
FRENCH: P(s|er) = -0.957568667001535 ==> log prob of sentence so far: -7.341446115945604

3GRAM: rsc
ENGLISH: P(c|rs) = -1.752726150659539 ==> log prob of sentence so far: -9.76212300417668
FRENCH: P(c|rs) = -1.3540928788929465 ==> log prob of sentence so far: -8.69553899483855

3GRAM: sch
ENGLISH: P(h|sc) = -1.0550406496572273 ==> log prob of sentence so far: -10.817163653833907
FRENCH: P(h|sc) = -1.131091456135533 ==> log prob of sentence so far: -9.826630450974083

3GRAM: cha
ENGLISH: P(a|ch) = -0.5465900006062943 ==> log prob of sentence so far: -11.3637536544402
FRENCH: P(a|ch) = -0.44568077937626416 ==> log prob of sentence so far: -10.272311230350347

3GRAM: han
ENGLISH: P(n|ha) = -0.9385162923034742 ==> log prob of sentence so far: -12.302269946743674
FRENCH: P(n|ha) = -0.8931298701220856 ==> log prob of sentence so far: -11.165441100472432

3GRAM: ant
ENGLISH: P(t|an) = -1.0401701906526726 ==> log prob of sentence so far: -13.342440137396347
FRENCH: P(t|an) = -0.49067183272548415 ==> log prob of sentence so far: -11.656112933197916

3GRAM: ntn
ENGLISH: P(n|nt) = -2.665191889503488 ==> log prob of sentence so far: -16.007632026899834
FRENCH: P(n|nt) = -1.8426240520896953 ==> log prob of sentence so far: -13.49873698528761

3GRAM: tne
ENGLISH: P(e|tn) = -0.6775607536372196 ==> log prob of sentence so far: -16.685192780537054
FRENCH: P(e|tn) = -0.39256476196024676 ==> log prob of sentence so far: -13.891301747247857

3GRAM: nev
ENGLISH: P(v|ne) = -1.1790356397024626 ==> log prob of sentence so far: -17.864228420239517
FRENCH: P(v|ne) = -1.4964547707214264 ==> log prob of sentence so far: -15.387756517969283

3GRAM: evo
ENGLISH: P(o|ev) = -1.3353342268339667 ==> log prob of sentence so far: -19.199562647073485
FRENCH: P(o|ev) = -0.5848053820022968 ==> log prob of sentence so far: -15.97256189997158

3GRAM: vou
ENGLISH: P(u|vo) = -1.0849664150805485 ==> log prob of sentence so far: -20.284529062154032
FRENCH: P(u|vo) = -0.3438634134725692 ==> log prob of sentence so far: -16.31642531344415

3GRAM: ous
ENGLISH: P(s|ou) = -0.7842497685950943 ==> log prob of sentence so far: -21.068778830749128
FRENCH: P(s|ou) = -0.5120146442641926 ==> log prob of sentence so far: -16.82843995770834

3GRAM: ust
ENGLISH: P(t|us) = -0.6069140437324048 ==> log prob of sentence so far: -21.675692874481534
FRENCH: P(t|us) = -1.4064505335181856 ==> log prob of sentence so far: -18.234890491226526

3GRAM: str
ENGLISH: P(r|st) = -1.0443161376769121 ==> log prob of sentence so far: -22.720009012158446
FRENCH: P(r|st) = -0.742178158051828 ==> log prob of sentence so far: -18.977068649278355

3GRAM: tru
ENGLISH: P(u|tr) = -0.8531550108252123 ==> log prob of sentence so far: -23.573164022983658
FRENCH: P(u|tr) = -1.695515514542488 ==> log prob of sentence so far: -20.672584163820844

3GRAM: rum
ENGLISH: P(m|ru) = -1.3363057560125244 ==> log prob of sentence so far: -24.909469778996183
FRENCH: P(m|ru) = -1.1605907908877482 ==> log prob of sentence so far: -21.833174954708593

3GRAM: ump
ENGLISH: P(p|um) = -0.7049111979734239 ==> log prob of sentence so far: -25.614380976969606
FRENCH: P(p|um) = -2.256148982659935 ==> log prob of sentence so far: -24.089323937368526

3GRAM: mpe
ENGLISH: P(e|mp) = -0.8501908023951125 ==> log prob of sentence so far: -26.464571779364718
FRENCH: P(e|mp) = -0.8703949692705103 ==> log prob of sentence so far: -24.959718906639036

3GRAM: pez
ENGLISH: P(z|pe) = -3.8444771757456815 ==> log prob of sentence so far: -30.3090489551104
FRENCH: P(z|pe) = -3.324556804316231 ==> log prob of sentence so far: -28.284275710955267

3GRAM: ezp
ENGLISH: P(p|ez) = -2.1986570869544226 ==> log prob of sentence so far: -32.50770604206482
FRENCH: P(p|ez) = -0.9345644584603379 ==> log prob of sentence so far: -29.218840169415603

3GRAM: zpa
ENGLISH: P(a|zp) = -1.4313637641589874 ==> log prob of sentence so far: -33.939069806223806
FRENCH: P(a|zp) = -0.31575325248468755 ==> log prob of sentence so far: -29.53459342190029

3GRAM: pas
ENGLISH: P(s|pa) = -0.8797160159010861 ==> log prob of sentence so far: -34.818785822124894
FRENCH: P(s|pa) = -0.5285672214189053 ==> log prob of sentence so far: -30.063160643319197

According to the 3gram model, the sentence is in French
----------------
4GRAM MODEL:

4GRAM: prot
ENGLISH: P(t|pro) = -1.569875307956561 ==> log prob of sentence so far: -1.569875307956561
FRENCH: P(t|pro) = -1.6211338722752795 ==> log prob of sentence so far: -1.6211338722752795

4GRAM: rote
ENGLISH: P(e|rot) = -0.8061799739838872 ==> log prob of sentence so far: -2.3760552819404483
FRENCH: P(e|rot) = -0.4550069404290901 ==> log prob of sentence so far: -2.0761408127043697

4GRAM: otes
ENGLISH: P(s|ote) = -0.8638633635902263 ==> log prob of sentence so far: -3.2399186455306745
FRENCH: P(s|ote) = -0.528175085090286 ==> log prob of sentence so far: -2.6043158977946557

4GRAM: test
ENGLISH: P(t|tes) = -0.4241851795318639 ==> log prob of sentence so far: -3.6641038250625386
FRENCH: P(t|tes) = -1.287657278080598 ==> log prob of sentence so far: -3.8919731758752536

4GRAM: este
ENGLISH: P(e|est) = -1.0727750507660563 ==> log prob of sentence so far: -4.7368788758285945
FRENCH: P(e|est) = -0.7711008604496177 ==> log prob of sentence so far: -4.663074036324871

4GRAM: ster
ENGLISH: P(r|ste) = -0.48328438352767544 ==> log prob of sentence so far: -5.22016325935627
FRENCH: P(r|ste) = -0.6746106584765741 ==> log prob of sentence so far: -5.337684694801445

4GRAM: ters
ENGLISH: P(s|ter) = -0.8424634665180117 ==> log prob of sentence so far: -6.062626725874281
FRENCH: P(s|ter) = -1.7027007881542482 ==> log prob of sentence so far: -7.040385482955694

4GRAM: ersc
ENGLISH: P(c|ers) = -1.6823202304322267 ==> log prob of sentence so far: -7.744946956306508
FRENCH: P(c|ers) = -1.4227635923970698 ==> log prob of sentence so far: -8.463149075352764

4GRAM: rsch
ENGLISH: P(h|rsc) = -1.1169698069370246 ==> log prob of sentence so far: -8.861916763243533
FRENCH: P(h|rsc) = -1.036628895362161 ==> log prob of sentence so far: -9.499777970714925

4GRAM: scha
ENGLISH: P(a|sch) = -0.5811260844923194 ==> log prob of sentence so far: -9.443042847735853
FRENCH: P(a|sch) = -0.27160828667328407 ==> log prob of sentence so far: -9.77138625738821

4GRAM: chan
ENGLISH: P(n|cha) = -0.5978719696556493 ==> log prob of sentence so far: -10.040914817391503
FRENCH: P(n|cha) = -0.7537229470007286 ==> log prob of sentence so far: -10.525109204388938

4GRAM: hant
ENGLISH: P(t|han) = -0.8093544768810672 ==> log prob of sentence so far: -10.85026929427257
FRENCH: P(t|han) = -0.3272366840035658 ==> log prob of sentence so far: -10.852345888392504

4GRAM: antn
ENGLISH: P(n|ant) = -2.3890049062287417 ==> log prob of sentence so far: -13.239274200501312
FRENCH: P(n|ant) = -1.675115684741525 ==> log prob of sentence so far: -12.52746157313403

4GRAM: ntne
ENGLISH: P(e|ntn) = -0.7781512503836436 ==> log prob of sentence so far: -14.017425450884955
FRENCH: P(e|ntn) = -0.5375192714632276 ==> log prob of sentence so far: -13.064980844597258

4GRAM: tnev
ENGLISH: P(v|tne) = -0.6940254217189564 ==> log prob of sentence so far: -14.711450872603912
FRENCH: P(v|tne) = -1.625125295396656 ==> log prob of sentence so far: -14.690106139993913

4GRAM: nevo
ENGLISH: P(o|nev) = -1.449449828121804 ==> log prob of sentence so far: -16.160900700725715
FRENCH: P(o|nev) = -0.4312916282096639 ==> log prob of sentence so far: -15.121397768203577

4GRAM: evou
ENGLISH: P(u|evo) = -0.9249918284397733 ==> log prob of sentence so far: -17.085892529165488
FRENCH: P(u|evo) = -0.30941307445366856 ==> log prob of sentence so far: -15.430810842657246

4GRAM: vous
ENGLISH: P(s|vou) = -0.5578563288359089 ==> log prob of sentence so far: -17.643748858001395
FRENCH: P(s|vou) = -0.11279951926027851 ==> log prob of sentence so far: -15.543610361917525

4GRAM: oust
ENGLISH: P(t|ous) = -1.1347285759341441 ==> log prob of sentence so far: -18.77847743393554
FRENCH: P(t|ous) = -1.7559567748431395 ==> log prob of sentence so far: -17.299567136760665

4GRAM: ustr
ENGLISH: P(r|ust) = -1.284216496654331 ==> log prob of sentence so far: -20.06269393058987
FRENCH: P(r|ust) = -0.5559552040819237 ==> log prob of sentence so far: -17.855522340842587

4GRAM: stru
ENGLISH: P(u|str) = -0.8890541142880414 ==> log prob of sentence so far: -20.95174804487791
FRENCH: P(u|str) = -0.8680632445159616 ==> log prob of sentence so far: -18.72358558535855

4GRAM: trum
ENGLISH: P(m|tru) = -1.3035046147918365 ==> log prob of sentence so far: -22.255252659669747
FRENCH: P(m|tru) = -0.5394131821900733 ==> log prob of sentence so far: -19.262998767548623

4GRAM: rump
ENGLISH: P(p|rum) = -0.5730962953926458 ==> log prob of sentence so far: -22.828348955062392
FRENCH: P(p|rum) = -2.2041199826559246 ==> log prob of sentence so far: -21.46711875020455

4GRAM: umpe
ENGLISH: P(e|ump) = -0.8239087409443188 ==> log prob of sentence so far: -23.65225769600671
FRENCH: P(e|ump) = -1.0791812460476249 ==> log prob of sentence so far: -22.546299996252174

4GRAM: mpez
ENGLISH: P(z|mpe) = -2.546542663478131 ==> log prob of sentence so far: -26.198800359484842
FRENCH: P(z|mpe) = -2.6190933306267428 ==> log prob of sentence so far: -25.165393326878917

4GRAM: pezp
ENGLISH: P(p|pez) = -1.4313637641589874 ==> log prob of sentence so far: -27.63016412364383
FRENCH: P(p|pez) = -1.4471580313422192 ==> log prob of sentence so far: -26.612551358221136

4GRAM: ezpa
ENGLISH: P(a|ezp) = -1.4313637641589874 ==> log prob of sentence so far: -29.061527887802818
FRENCH: P(a|ezp) = -0.3109007496902755 ==> log prob of sentence so far: -26.923452107911412

4GRAM: zpas
ENGLISH: P(s|zpa) = -1.4313637641589874 ==> log prob of sentence so far: -30.492891651961806
FRENCH: P(s|zpa) = -0.1741567592784816 ==> log prob of sentence so far: -27.097608867189894

According to the 4gram model, the sentence is in French
----------------
