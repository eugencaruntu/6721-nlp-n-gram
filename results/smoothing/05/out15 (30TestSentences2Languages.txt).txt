He deserves a covfefe.

1GRAM MODEL:

1GRAM: h
ENGLISH: P(h) = -1.180274162400034 ==> log prob of sentence so far: -1.180274162400034
FRENCH: P(h) = -2.1056297044472685 ==> log prob of sentence so far: -2.1056297044472685

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -2.090345380459941
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -2.872595518969725

1GRAM: d
ENGLISH: P(d) = -1.396179917541683 ==> log prob of sentence so far: -3.4865252980016237
FRENCH: P(d) = -1.4202616779892512 ==> log prob of sentence so far: -4.292857196958976

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -4.396596516061531
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -5.059823011481432

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -5.567737528790133
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -6.124270726371089

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -6.47780874685004
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -6.891236540893545

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -7.739081648162136
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -8.075265179964003

1GRAM: v
ENGLISH: P(v) = -2.043619342325931 ==> log prob of sentence so far: -9.782700990488067
FRENCH: P(v) = -1.82784228094936 ==> log prob of sentence so far: -9.903107460913363

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -10.692772208547973
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -10.670073275435819

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -11.863913221276574
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -11.734520990325475

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -12.950701902319029
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -12.810874085430983

1GRAM: c
ENGLISH: P(c) = -1.626587631644617 ==> log prob of sentence so far: -14.577289533963645
FRENCH: P(c) = -1.492223661080204 ==> log prob of sentence so far: -14.303097746511188

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -15.71507329454914
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -15.577441076828842

1GRAM: v
ENGLISH: P(v) = -2.043619342325931 ==> log prob of sentence so far: -17.758692636875068
FRENCH: P(v) = -1.82784228094936 ==> log prob of sentence so far: -17.405283357778202

1GRAM: f
ENGLISH: P(f) = -1.660275542753472 ==> log prob of sentence so far: -19.41896817962854
FRENCH: P(f) = -1.9917263037969526 ==> log prob of sentence so far: -19.397009661575154

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -20.329039397688447
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -20.16397547609761

1GRAM: f
ENGLISH: P(f) = -1.660275542753472 ==> log prob of sentence so far: -21.98931494044192
FRENCH: P(f) = -1.9917263037969526 ==> log prob of sentence so far: -22.155701779894564

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -22.899386158501827
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -22.92266759441702

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: he
ENGLISH: P(e|h) = -0.37002759861537876 ==> log prob of sentence so far: -0.37002759861537876
FRENCH: P(e|h) = -0.45804503705298444 ==> log prob of sentence so far: -0.45804503705298444

2GRAM: ed
ENGLISH: P(d|e) = -1.0374576111626355 ==> log prob of sentence so far: -1.4074852097780142
FRENCH: P(d|e) = -1.2790847979127375 ==> log prob of sentence so far: -1.737129834965722

2GRAM: de
ENGLISH: P(e|d) = -0.8320988657918726 ==> log prob of sentence so far: -2.239584075569887
FRENCH: P(e|d) = -0.290519189469436 ==> log prob of sentence so far: -2.027649024435158

2GRAM: es
ENGLISH: P(s|e) = -0.944867640322014 ==> log prob of sentence so far: -3.184451715891901
FRENCH: P(s|e) = -0.7261499946209886 ==> log prob of sentence so far: -2.7537990190561468

2GRAM: se
ENGLISH: P(e|s) = -0.9357350813780119 ==> log prob of sentence so far: -4.120186797269913
FRENCH: P(e|s) = -0.7710764417175806 ==> log prob of sentence so far: -3.5248754607737274

2GRAM: er
ENGLISH: P(r|e) = -0.8561613604222861 ==> log prob of sentence so far: -4.976348157692199
FRENCH: P(r|e) = -1.0504664166531121 ==> log prob of sentence so far: -4.5753418774268395

2GRAM: rv
ENGLISH: P(v|r) = -2.1037655820145593 ==> log prob of sentence so far: -7.080113739706758
FRENCH: P(v|r) = -1.8220003150840203 ==> log prob of sentence so far: -6.39734219251086

2GRAM: ve
ENGLISH: P(e|v) = -0.17428026499063828 ==> log prob of sentence so far: -7.254394004697396
FRENCH: P(e|v) = -0.4815039345822809 ==> log prob of sentence so far: -6.878846127093141

2GRAM: es
ENGLISH: P(s|e) = -0.944867640322014 ==> log prob of sentence so far: -8.19926164501941
FRENCH: P(s|e) = -0.7261499946209886 ==> log prob of sentence so far: -7.604996121714129

2GRAM: sa
ENGLISH: P(a|s) = -1.0355099388369886 ==> log prob of sentence so far: -9.234771583856398
FRENCH: P(a|s) = -0.9925773557188845 ==> log prob of sentence so far: -8.597573477433015

2GRAM: ac
ENGLISH: P(c|a) = -1.4314000466535202 ==> log prob of sentence so far: -10.666171630509918
FRENCH: P(c|a) = -1.3380717042938233 ==> log prob of sentence so far: -9.935645181726837

2GRAM: co
ENGLISH: P(o|c) = -0.7820327849639489 ==> log prob of sentence so far: -11.448204415473867
FRENCH: P(o|c) = -0.6556874666871543 ==> log prob of sentence so far: -10.591332648413992

2GRAM: ov
ENGLISH: P(v|o) = -1.7957558593393887 ==> log prob of sentence so far: -13.243960274813256
FRENCH: P(v|o) = -2.526669664991859 ==> log prob of sentence so far: -13.118002313405851

2GRAM: vf
ENGLISH: P(f|v) = -4.239299479126893 ==> log prob of sentence so far: -17.48325975394015
FRENCH: P(f|v) = -4.314204405542848 ==> log prob of sentence so far: -17.4322067189487

2GRAM: fe
ENGLISH: P(e|f) = -1.129301522702586 ==> log prob of sentence so far: -18.612561276642737
FRENCH: P(e|f) = -0.9025987035889845 ==> log prob of sentence so far: -18.334805422537684

2GRAM: ef
ENGLISH: P(f|e) = -1.6422937235476818 ==> log prob of sentence so far: -20.254855000190418
FRENCH: P(f|e) = -1.8201213810502177 ==> log prob of sentence so far: -20.1549268035879

2GRAM: fe
ENGLISH: P(e|f) = -1.129301522702586 ==> log prob of sentence so far: -21.384156522893004
FRENCH: P(e|f) = -0.9025987035889845 ==> log prob of sentence so far: -21.057525507176887

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: hed
ENGLISH: P(d|he) = -1.378502539906489 ==> log prob of sentence so far: -1.378502539906489
FRENCH: P(d|he) = -1.4400813286725058 ==> log prob of sentence so far: -1.4400813286725058

3GRAM: ede
ENGLISH: P(e|ed) = -1.203762979362576 ==> log prob of sentence so far: -2.5822655192690647
FRENCH: P(e|ed) = -0.26826572538935256 ==> log prob of sentence so far: -1.7083470540618584

3GRAM: des
ENGLISH: P(s|de) = -0.9611523087474174 ==> log prob of sentence so far: -3.543417828016482
FRENCH: P(s|de) = -0.5850790411375858 ==> log prob of sentence so far: -2.293426095199444

3GRAM: ese
ENGLISH: P(e|es) = -0.9490134133447401 ==> log prob of sentence so far: -4.492431241361222
FRENCH: P(e|es) = -0.9180499442018412 ==> log prob of sentence so far: -3.211476039401285

3GRAM: ser
ENGLISH: P(r|se) = -1.2967086218813386 ==> log prob of sentence so far: -5.789139863242561
FRENCH: P(r|se) = -0.9361500878778645 ==> log prob of sentence so far: -4.14762612727915

3GRAM: erv
ENGLISH: P(v|er) = -1.7682772902500774 ==> log prob of sentence so far: -7.557417153492638
FRENCH: P(v|er) = -1.3472614301474384 ==> log prob of sentence so far: -5.494887557426588

3GRAM: rve
ENGLISH: P(e|rv) = -0.32573077591529725 ==> log prob of sentence so far: -7.883147929407936
FRENCH: P(e|rv) = -0.4476611237609496 ==> log prob of sentence so far: -5.942548681187537

3GRAM: ves
ENGLISH: P(s|ve) = -0.985340464767245 ==> log prob of sentence so far: -8.86848839417518
FRENCH: P(s|ve) = -1.2677150959949515 ==> log prob of sentence so far: -7.2102637771824885

3GRAM: esa
ENGLISH: P(a|es) = -1.0053104001078617 ==> log prob of sentence so far: -9.873798794283042
FRENCH: P(a|es) = -1.0604828170880394 ==> log prob of sentence so far: -8.270746594270527

3GRAM: sac
ENGLISH: P(c|sa) = -1.7012032817061193 ==> log prob of sentence so far: -11.575002075989161
FRENCH: P(c|sa) = -1.3064883140386434 ==> log prob of sentence so far: -9.57723490830917

3GRAM: aco
ENGLISH: P(o|ac) = -1.1831383275401057 ==> log prob of sentence so far: -12.758140403529268
FRENCH: P(o|ac) = -0.8026885301350244 ==> log prob of sentence so far: -10.379923438444195

3GRAM: cov
ENGLISH: P(v|co) = -1.6462470742142987 ==> log prob of sentence so far: -14.404387477743567
FRENCH: P(v|co) = -3.9947569445876283 ==> log prob of sentence so far: -14.374680383031823

3GRAM: ovf
ENGLISH: P(f|ov) = -3.3494717992143856 ==> log prob of sentence so far: -17.753859276957954
FRENCH: P(f|ov) = -2.387389826338729 ==> log prob of sentence so far: -16.762070209370552

3GRAM: vfe
ENGLISH: P(e|vf) = -1.4313637641589874 ==> log prob of sentence so far: -19.18522304111694
FRENCH: P(e|vf) = -1.4313637641589874 ==> log prob of sentence so far: -18.19343397352954

3GRAM: fef
ENGLISH: P(f|fe) = -2.078601800355391 ==> log prob of sentence so far: -21.26382484147233
FRENCH: P(f|fe) = -2.776216750606444 ==> log prob of sentence so far: -20.969650724135985

3GRAM: efe
ENGLISH: P(e|ef) = -1.1690226432794089 ==> log prob of sentence so far: -22.43284748475174
FRENCH: P(e|ef) = -0.9772989860255613 ==> log prob of sentence so far: -21.946949710161547

According to the 3gram model, the sentence is in French
----------------
4GRAM MODEL:

4GRAM: hede
ENGLISH: P(e|hed) = -0.5507668066989779 ==> log prob of sentence so far: -0.5507668066989779
FRENCH: P(e|hed) = -0.290436922166557 ==> log prob of sentence so far: -0.290436922166557

4GRAM: edes
ENGLISH: P(s|ede) = -0.9828319748629861 ==> log prob of sentence so far: -1.533598781561964
FRENCH: P(s|ede) = -0.5666952840536525 ==> log prob of sentence so far: -0.8571322062202096

4GRAM: dese
ENGLISH: P(e|des) = -1.2837610429147437 ==> log prob of sentence so far: -2.817359824476708
FRENCH: P(e|des) = -0.9139763994443542 ==> log prob of sentence so far: -1.7711086056645637

4GRAM: eser
ENGLISH: P(r|ese) = -1.1934426463232006 ==> log prob of sentence so far: -4.010802470799908
FRENCH: P(r|ese) = -0.9604699531340147 ==> log prob of sentence so far: -2.7315785587985784

4GRAM: serv
ENGLISH: P(v|ser) = -0.4379998196938166 ==> log prob of sentence so far: -4.448802290493725
FRENCH: P(v|ser) = -0.5331787019201495 ==> log prob of sentence so far: -3.264757260718728

4GRAM: erve
ENGLISH: P(e|erv) = -0.3757634173582919 ==> log prob of sentence so far: -4.824565707852017
FRENCH: P(e|erv) = -0.3900126402699363 ==> log prob of sentence so far: -3.654769900988664

4GRAM: rves
ENGLISH: P(s|rve) = -0.9288972505982291 ==> log prob of sentence so far: -5.753462958450246
FRENCH: P(s|rve) = -1.281285937468224 ==> log prob of sentence so far: -4.936055838456888

4GRAM: vesa
ENGLISH: P(a|ves) = -1.037995318561433 ==> log prob of sentence so far: -6.791458277011679
FRENCH: P(a|ves) = -0.8676176688297247 ==> log prob of sentence so far: -5.803673507286613

4GRAM: esac
ENGLISH: P(c|esa) = -1.9313526424491243 ==> log prob of sentence so far: -8.722810919460803
FRENCH: P(c|esa) = -1.2598658264183704 ==> log prob of sentence so far: -7.063539333704983

4GRAM: saco
ENGLISH: P(o|sac) = -0.8297382846050426 ==> log prob of sentence so far: -9.552549204065846
FRENCH: P(o|sac) = -0.7852593383817829 ==> log prob of sentence so far: -7.848798672086766

4GRAM: acov
ENGLISH: P(v|aco) = -2.1249387366083 ==> log prob of sentence so far: -11.677487940674146
FRENCH: P(v|aco) = -2.938519725176492 ==> log prob of sentence so far: -10.787318397263258

4GRAM: covf
ENGLISH: P(f|cov) = -2.287801729930226 ==> log prob of sentence so far: -13.965289670604372
FRENCH: P(f|cov) = -1.4313637641589874 ==> log prob of sentence so far: -12.218682161422246

4GRAM: ovfe
ENGLISH: P(e|ovf) = -1.4313637641589874 ==> log prob of sentence so far: -15.39665343476336
FRENCH: P(e|ovf) = -1.4313637641589874 ==> log prob of sentence so far: -13.650045925581233

4GRAM: vfef
ENGLISH: P(f|vfe) = -1.4313637641589874 ==> log prob of sentence so far: -16.828017198922346
FRENCH: P(f|vfe) = -1.4313637641589874 ==> log prob of sentence so far: -15.081409689740221

4GRAM: fefe
ENGLISH: P(e|fef) = -1.6989700043360187 ==> log prob of sentence so far: -18.526987203258365
FRENCH: P(e|fef) = -1.4471580313422192 ==> log prob of sentence so far: -16.52856772108244

According to the 4gram model, the sentence is in French
----------------
