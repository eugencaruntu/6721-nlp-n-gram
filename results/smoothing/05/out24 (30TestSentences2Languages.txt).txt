He had Boeuf bourguignon, Escargots de Bourgogne, and Coq au vin.

1GRAM MODEL:

1GRAM: h
ENGLISH: P(h) = -1.180274162400034 ==> log prob of sentence so far: -1.180274162400034
FRENCH: P(h) = -2.1056297044472685 ==> log prob of sentence so far: -2.1056297044472685

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -2.090345380459941
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -2.872595518969725

1GRAM: h
ENGLISH: P(h) = -1.180274162400034 ==> log prob of sentence so far: -3.2706195428599747
FRENCH: P(h) = -2.1056297044472685 ==> log prob of sentence so far: -4.9782252234169935

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -4.3574082239024285
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -6.054578318522502

1GRAM: d
ENGLISH: P(d) = -1.396179917541683 ==> log prob of sentence so far: -5.753588141444111
FRENCH: P(d) = -1.4202616779892512 ==> log prob of sentence so far: -7.474839996511753

1GRAM: b
ENGLISH: P(b) = -1.7519519887518187 ==> log prob of sentence so far: -7.50554013019593
FRENCH: P(b) = -2.0518881761829366 ==> log prob of sentence so far: -9.526728172694689

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -8.643323890781424
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -10.801071503012343

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -9.55339510884133
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -11.568037317534799

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -11.105883277799245
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -12.77417237871042

1GRAM: f
ENGLISH: P(f) = -1.660275542753472 ==> log prob of sentence so far: -12.766158820552718
FRENCH: P(f) = -1.9917263037969526 ==> log prob of sentence so far: -14.765898682507371

1GRAM: b
ENGLISH: P(b) = -1.7519519887518187 ==> log prob of sentence so far: -14.518110809304536
FRENCH: P(b) = -2.0518881761829366 ==> log prob of sentence so far: -16.817786858690308

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -15.65589456989003
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -18.092130189007964

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -17.208382738847945
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -19.298265250183587

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -18.469655640160042
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -20.482293889254045

1GRAM: g
ENGLISH: P(g) = -1.660068197743731 ==> log prob of sentence so far: -20.12972383790377
FRENCH: P(g) = -2.0284572462247623 ==> log prob of sentence so far: -22.510751135478806

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -21.682212006861686
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -23.71688619665443

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -22.844737487135596
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -24.85209263932327

1GRAM: g
ENGLISH: P(g) = -1.660068197743731 ==> log prob of sentence so far: -24.504805684879326
FRENCH: P(g) = -2.0284572462247623 ==> log prob of sentence so far: -26.88054988554803

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -25.666402622281954
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -28.003187362162535

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -26.80418638286745
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -29.27753069248019

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -27.965783320270077
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -30.400168169094695

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -28.875854538329985
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -31.167133983617152

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -30.046995551058586
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -32.23158169850681

1GRAM: c
ENGLISH: P(c) = -1.626587631644617 ==> log prob of sentence so far: -31.673583182703204
FRENCH: P(c) = -1.492223661080204 ==> log prob of sentence so far: -33.72380535958701

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -32.76037186374566
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -34.80015845469252

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -34.02164476505776
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -35.984187093762976

1GRAM: g
ENGLISH: P(g) = -1.660068197743731 ==> log prob of sentence so far: -35.68171296280149
FRENCH: P(g) = -2.0284572462247623 ==> log prob of sentence so far: -38.01264433998774

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -36.81949672338698
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -39.28698767030539

1GRAM: t
ENGLISH: P(t) = -1.0342236377842724 ==> log prob of sentence so far: -37.853720361171256
FRENCH: P(t) = -1.1659667480967535 ==> log prob of sentence so far: -40.45295441840214

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -39.02486137389986
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -41.517402133291796

1GRAM: d
ENGLISH: P(d) = -1.396179917541683 ==> log prob of sentence so far: -40.42104129144154
FRENCH: P(d) = -1.4202616779892512 ==> log prob of sentence so far: -42.93766381128105

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -41.331112509501445
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -43.704629625803506

1GRAM: b
ENGLISH: P(b) = -1.7519519887518187 ==> log prob of sentence so far: -43.083064498253265
FRENCH: P(b) = -2.0518881761829366 ==> log prob of sentence so far: -45.75651780198644

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -44.22084825883876
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -47.030861132304096

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -45.77333642779667
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -48.23699619347972

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -47.03460932910877
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -49.421024832550174

1GRAM: g
ENGLISH: P(g) = -1.660068197743731 ==> log prob of sentence so far: -48.6946775268525
FRENCH: P(g) = -2.0284572462247623 ==> log prob of sentence so far: -51.449482078774935

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -49.832461287437994
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -52.72382540909259

1GRAM: g
ENGLISH: P(g) = -1.660068197743731 ==> log prob of sentence so far: -51.49252948518173
FRENCH: P(g) = -2.0284572462247623 ==> log prob of sentence so far: -54.75228265531735

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -52.654126422584355
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -55.87492013193186

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -53.56419764064426
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -56.641885946454316

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -54.650986321686716
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -57.718239041559826

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -55.812583259089344
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -58.84087651817433

1GRAM: d
ENGLISH: P(d) = -1.396179917541683 ==> log prob of sentence so far: -57.208763176631024
FRENCH: P(d) = -1.4202616779892512 ==> log prob of sentence so far: -60.261138196163586

1GRAM: c
ENGLISH: P(c) = -1.626587631644617 ==> log prob of sentence so far: -58.83535080827564
FRENCH: P(c) = -1.492223661080204 ==> log prob of sentence so far: -61.75336185724379

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -59.973134568861134
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -63.027705187561445

1GRAM: q
ENGLISH: P(q) = -2.7879988169444423 ==> log prob of sentence so far: -62.76113338580558
FRENCH: P(q) = -1.9394381295606844 ==> log prob of sentence so far: -64.96714331712212

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -63.847922066848035
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -66.04349641222763

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -65.40041023580595
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -67.24963147340324

1GRAM: v
ENGLISH: P(v) = -2.043619342325931 ==> log prob of sentence so far: -67.44402957813188
FRENCH: P(v) = -1.82784228094936 ==> log prob of sentence so far: -69.0774737543526

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -68.60655505840579
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -70.21268019702144

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -69.76815199580842
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -71.33531767363594

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: he
ENGLISH: P(e|h) = -0.37002759861537876 ==> log prob of sentence so far: -0.37002759861537876
FRENCH: P(e|h) = -0.45804503705298444 ==> log prob of sentence so far: -0.45804503705298444

2GRAM: eh
ENGLISH: P(h|e) = -1.6767976472301338 ==> log prob of sentence so far: -2.0468252458455125
FRENCH: P(h|e) = -2.3081346043113826 ==> log prob of sentence so far: -2.766179641364367

2GRAM: ha
ENGLISH: P(a|h) = -0.693585657435816 ==> log prob of sentence so far: -2.7404109032813286
FRENCH: P(a|h) = -0.5247438164739214 ==> log prob of sentence so far: -3.2909234578382884

2GRAM: ad
ENGLISH: P(d|a) = -1.3608830323254337 ==> log prob of sentence so far: -4.101293935606762
FRENCH: P(d|a) = -1.6547663418439653 ==> log prob of sentence so far: -4.9456897996822535

2GRAM: db
ENGLISH: P(b|d) = -1.3787468648352232 ==> log prob of sentence so far: -5.480040800441985
FRENCH: P(b|d) = -3.6055039520559196 ==> log prob of sentence so far: -8.551193751738174

2GRAM: bo
ENGLISH: P(o|b) = -0.8314605247936132 ==> log prob of sentence so far: -6.311501325235598
FRENCH: P(o|b) = -0.9110536976871761 ==> log prob of sentence so far: -9.46224744942535

2GRAM: oe
ENGLISH: P(e|o) = -2.1228938724075666 ==> log prob of sentence so far: -8.434395197643164
FRENCH: P(e|o) = -2.1436581077967913 ==> log prob of sentence so far: -11.60590555722214

2GRAM: eu
ENGLISH: P(u|e) = -2.3662597924696036 ==> log prob of sentence so far: -10.800654990112768
FRENCH: P(u|e) = -1.2864621294283165 ==> log prob of sentence so far: -12.892367686650458

2GRAM: uf
ENGLISH: P(f|u) = -2.1908268242090263 ==> log prob of sentence so far: -12.991481814321794
FRENCH: P(f|u) = -2.078637282391912 ==> log prob of sentence so far: -14.971004969042369

2GRAM: fb
ENGLISH: P(b|f) = -1.9878762232415879 ==> log prob of sentence so far: -14.979358037563383
FRENCH: P(b|f) = -4.150326536498707 ==> log prob of sentence so far: -19.121331505541075

2GRAM: bo
ENGLISH: P(o|b) = -0.8314605247936132 ==> log prob of sentence so far: -15.810818562356996
FRENCH: P(o|b) = -0.9110536976871761 ==> log prob of sentence so far: -20.03238520322825

2GRAM: ou
ENGLISH: P(u|o) = -0.9046394432126483 ==> log prob of sentence so far: -16.715458005569644
FRENCH: P(u|o) = -0.6277892700441801 ==> log prob of sentence so far: -20.66017447327243

2GRAM: ur
ENGLISH: P(r|u) = -0.9178087681418121 ==> log prob of sentence so far: -17.633266773711455
FRENCH: P(r|u) = -0.7887730706784898 ==> log prob of sentence so far: -21.448947543950922

2GRAM: rg
ENGLISH: P(g|r) = -1.9696414243131393 ==> log prob of sentence so far: -19.602908198024593
FRENCH: P(g|r) = -2.0675257846928004 ==> log prob of sentence so far: -23.516473328643723

2GRAM: gu
ENGLISH: P(u|g) = -1.5278201021313254 ==> log prob of sentence so far: -21.13072830015592
FRENCH: P(u|g) = -1.0064610319587752 ==> log prob of sentence so far: -24.5229343606025

2GRAM: ui
ENGLISH: P(i|u) = -1.6690825013810329 ==> log prob of sentence so far: -22.799810801536953
FRENCH: P(i|u) = -1.0008172251086478 ==> log prob of sentence so far: -25.523751585711146

2GRAM: ig
ENGLISH: P(g|i) = -1.4583316297190163 ==> log prob of sentence so far: -24.25814243125597
FRENCH: P(g|i) = -1.7738250369287822 ==> log prob of sentence so far: -27.29757662263993

2GRAM: gn
ENGLISH: P(n|g) = -1.601888129639173 ==> log prob of sentence so far: -25.86003056089514
FRENCH: P(n|g) = -0.9950844088776849 ==> log prob of sentence so far: -28.292661031517614

2GRAM: no
ENGLISH: P(o|n) = -1.086572052191251 ==> log prob of sentence so far: -26.94660261308639
FRENCH: P(o|n) = -1.2378335482588405 ==> log prob of sentence so far: -29.530494579776455

2GRAM: on
ENGLISH: P(n|o) = -0.8622085952026249 ==> log prob of sentence so far: -27.808811208289015
FRENCH: P(n|o) = -0.5205456677173079 ==> log prob of sentence so far: -30.051040247493763

2GRAM: ne
ENGLISH: P(e|n) = -1.0532701120323171 ==> log prob of sentence so far: -28.86208132032133
FRENCH: P(e|n) = -0.7543075705657655 ==> log prob of sentence so far: -30.805347818059527

2GRAM: es
ENGLISH: P(s|e) = -0.944867640322014 ==> log prob of sentence so far: -29.806948960643343
FRENCH: P(s|e) = -0.7261499946209886 ==> log prob of sentence so far: -31.531497812680517

2GRAM: sc
ENGLISH: P(c|s) = -1.566928188574753 ==> log prob of sentence so far: -31.373877149218096
FRENCH: P(c|s) = -1.3370330161780848 ==> log prob of sentence so far: -32.8685308288586

2GRAM: ca
ENGLISH: P(a|c) = -0.84612780149222 ==> log prob of sentence so far: -32.22000495071032
FRENCH: P(a|c) = -0.9172798051764028 ==> log prob of sentence so far: -33.78581063403501

2GRAM: ar
ENGLISH: P(r|a) = -0.9854656313970852 ==> log prob of sentence so far: -33.2054705821074
FRENCH: P(r|a) = -1.0331920938948058 ==> log prob of sentence so far: -34.81900272792981

2GRAM: rg
ENGLISH: P(g|r) = -1.9696414243131393 ==> log prob of sentence so far: -35.17511200642054
FRENCH: P(g|r) = -2.0675257846928004 ==> log prob of sentence so far: -36.88652851262261

2GRAM: go
ENGLISH: P(o|g) = -1.0244622475417653 ==> log prob of sentence so far: -36.199574253962304
FRENCH: P(o|g) = -1.3057991887792395 ==> log prob of sentence so far: -38.19232770140185

2GRAM: ot
ENGLISH: P(t|o) = -1.1641322248007255 ==> log prob of sentence so far: -37.36370647876303
FRENCH: P(t|o) = -1.4142544440361249 ==> log prob of sentence so far: -39.606582145437976

2GRAM: ts
ENGLISH: P(s|t) = -1.3509351575782058 ==> log prob of sentence so far: -38.714641636341234
FRENCH: P(s|t) = -1.2204919099668523 ==> log prob of sentence so far: -40.82707405540483

2GRAM: sd
ENGLISH: P(d|s) = -2.063829477589922 ==> log prob of sentence so far: -40.778471113931154
FRENCH: P(d|s) = -1.0845267273179306 ==> log prob of sentence so far: -41.91160078272276

2GRAM: de
ENGLISH: P(e|d) = -0.8320988657918726 ==> log prob of sentence so far: -41.610569979723024
FRENCH: P(e|d) = -0.290519189469436 ==> log prob of sentence so far: -42.20211997219219

2GRAM: eb
ENGLISH: P(b|e) = -1.6930590296644747 ==> log prob of sentence so far: -43.3036290093875
FRENCH: P(b|e) = -2.134431311091517 ==> log prob of sentence so far: -44.33655128328371

2GRAM: bo
ENGLISH: P(o|b) = -0.8314605247936132 ==> log prob of sentence so far: -44.13508953418111
FRENCH: P(o|b) = -0.9110536976871761 ==> log prob of sentence so far: -45.247604980970884

2GRAM: ou
ENGLISH: P(u|o) = -0.9046394432126483 ==> log prob of sentence so far: -45.039728977393764
FRENCH: P(u|o) = -0.6277892700441801 ==> log prob of sentence so far: -45.87539425101507

2GRAM: ur
ENGLISH: P(r|u) = -0.9178087681418121 ==> log prob of sentence so far: -45.957537745535575
FRENCH: P(r|u) = -0.7887730706784898 ==> log prob of sentence so far: -46.664167321693554

2GRAM: rg
ENGLISH: P(g|r) = -1.9696414243131393 ==> log prob of sentence so far: -47.92717916984871
FRENCH: P(g|r) = -2.0675257846928004 ==> log prob of sentence so far: -48.73169310638635

2GRAM: go
ENGLISH: P(o|g) = -1.0244622475417653 ==> log prob of sentence so far: -48.951641417390476
FRENCH: P(o|g) = -1.3057991887792395 ==> log prob of sentence so far: -50.03749229516559

2GRAM: og
ENGLISH: P(g|o) = -2.1407791136921204 ==> log prob of sentence so far: -51.092420531082595
FRENCH: P(g|o) = -2.412268919823467 ==> log prob of sentence so far: -52.44976121498906

2GRAM: gn
ENGLISH: P(n|g) = -1.601888129639173 ==> log prob of sentence so far: -52.69430866072177
FRENCH: P(n|g) = -0.9950844088776849 ==> log prob of sentence so far: -53.44484562386675

2GRAM: ne
ENGLISH: P(e|n) = -1.0532701120323171 ==> log prob of sentence so far: -53.74757877275408
FRENCH: P(e|n) = -0.7543075705657655 ==> log prob of sentence so far: -54.19915319443251

2GRAM: ea
ENGLISH: P(a|e) = -1.0585605717592588 ==> log prob of sentence so far: -54.80613934451334
FRENCH: P(a|e) = -1.5190302730971181 ==> log prob of sentence so far: -55.71818346752963

2GRAM: an
ENGLISH: P(n|a) = -0.704528505817304 ==> log prob of sentence so far: -55.51066785033064
FRENCH: P(n|a) = -0.8168159428818009 ==> log prob of sentence so far: -56.53499941041143

2GRAM: nd
ENGLISH: P(d|n) = -0.7555075870204362 ==> log prob of sentence so far: -56.26617543735108
FRENCH: P(d|n) = -1.0271635643855614 ==> log prob of sentence so far: -57.56216297479699

2GRAM: dc
ENGLISH: P(c|d) = -1.7453945152533614 ==> log prob of sentence so far: -58.01156995260444
FRENCH: P(c|d) = -2.5347558735451576 ==> log prob of sentence so far: -60.09691884834215

2GRAM: co
ENGLISH: P(o|c) = -0.7820327849639489 ==> log prob of sentence so far: -58.793602737568385
FRENCH: P(o|c) = -0.6556874666871543 ==> log prob of sentence so far: -60.752606315029304

2GRAM: oq
ENGLISH: P(q|o) = -3.153572867651858 ==> log prob of sentence so far: -61.947175605220245
FRENCH: P(q|o) = -2.2129372379540166 ==> log prob of sentence so far: -62.96554355298332

2GRAM: qa
ENGLISH: P(a|q) = -3.0206374635676054 ==> log prob of sentence so far: -64.96781306878785
FRENCH: P(a|q) = -3.2487903775753857 ==> log prob of sentence so far: -66.21433393055871

2GRAM: au
ENGLISH: P(u|a) = -2.1220107996271214 ==> log prob of sentence so far: -67.08982386841497
FRENCH: P(u|a) = -1.079016828317488 ==> log prob of sentence so far: -67.2933507588762

2GRAM: uv
ENGLISH: P(v|u) = -2.9727710636098093 ==> log prob of sentence so far: -70.06259493202478
FRENCH: P(v|u) = -1.5222341223399225 ==> log prob of sentence so far: -68.81558488121613

2GRAM: vi
ENGLISH: P(i|v) = -0.7631927622867011 ==> log prob of sentence so far: -70.82578769431149
FRENCH: P(i|v) = -0.7216945576421682 ==> log prob of sentence so far: -69.53727943885829

2GRAM: in
ENGLISH: P(n|i) = -0.5177328887875342 ==> log prob of sentence so far: -71.34352058309902
FRENCH: P(n|i) = -0.8962746820270037 ==> log prob of sentence so far: -70.43355412088529

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: heh
ENGLISH: P(h|he) = -1.526287399651993 ==> log prob of sentence so far: -1.526287399651993
FRENCH: P(h|he) = -2.7317038558146556 ==> log prob of sentence so far: -2.7317038558146556

3GRAM: eha
ENGLISH: P(a|eh) = -0.5495652287140947 ==> log prob of sentence so far: -2.075852628366088
FRENCH: P(a|eh) = -0.4496679843398301 ==> log prob of sentence so far: -3.1813718401544855

3GRAM: had
ENGLISH: P(d|ha) = -1.1593729353047122 ==> log prob of sentence so far: -3.2352255636708
FRENCH: P(d|ha) = -1.9025600363686455 ==> log prob of sentence so far: -5.083931876523131

3GRAM: adb
ENGLISH: P(b|ad) = -1.2030463245518612 ==> log prob of sentence so far: -4.438271888222661
FRENCH: P(b|ad) = -3.414639146737009 ==> log prob of sentence so far: -8.49857102326014

3GRAM: dbo
ENGLISH: P(o|db) = -0.9833476924200054 ==> log prob of sentence so far: -5.421619580642666
FRENCH: P(o|db) = -1.5797835966168101 ==> log prob of sentence so far: -10.07835461987695

3GRAM: boe
ENGLISH: P(e|bo) = -3.699143687394484 ==> log prob of sentence so far: -9.12076326803715
FRENCH: P(e|bo) = -2.4879863311293935 ==> log prob of sentence so far: -12.566340951006344

3GRAM: oeu
ENGLISH: P(u|oe) = -2.0694214087584686 ==> log prob of sentence so far: -11.190184676795619
FRENCH: P(u|oe) = -0.3335766413971352 ==> log prob of sentence so far: -12.89991759240348

3GRAM: euf
ENGLISH: P(f|eu) = -3.0017337128090005 ==> log prob of sentence so far: -14.19191838960462
FRENCH: P(f|eu) = -1.987594770238171 ==> log prob of sentence so far: -14.887512362641651

3GRAM: ufb
ENGLISH: P(b|uf) = -2.568201724066995 ==> log prob of sentence so far: -16.760120113671615
FRENCH: P(b|uf) = -2.870403905279027 ==> log prob of sentence so far: -17.757916267920677

3GRAM: fbo
ENGLISH: P(o|fb) = -0.7941639206300166 ==> log prob of sentence so far: -17.55428403430163
FRENCH: P(o|fb) = -1.4313637641589874 ==> log prob of sentence so far: -19.189280032079665

3GRAM: bou
ENGLISH: P(u|bo) = -0.7692141273098959 ==> log prob of sentence so far: -18.32349816161153
FRENCH: P(u|bo) = -0.6222902712133229 ==> log prob of sentence so far: -19.81157030329299

3GRAM: our
ENGLISH: P(r|ou) = -0.9331113741598649 ==> log prob of sentence so far: -19.256609535771393
FRENCH: P(r|ou) = -0.6948640525655961 ==> log prob of sentence so far: -20.506434355858584

3GRAM: urg
ENGLISH: P(g|ur) = -1.6935497220361015 ==> log prob of sentence so far: -20.950159257807496
FRENCH: P(g|ur) = -2.38431882290614 ==> log prob of sentence so far: -22.890753178764726

3GRAM: rgu
ENGLISH: P(u|rg) = -1.2925669478779132 ==> log prob of sentence so far: -22.242726205685408
FRENCH: P(u|rg) = -1.0748382650368427 ==> log prob of sentence so far: -23.965591443801568

3GRAM: gui
ENGLISH: P(i|gu) = -1.0195955560633807 ==> log prob of sentence so far: -23.262321761748787
FRENCH: P(i|gu) = -1.1203079823415052 ==> log prob of sentence so far: -25.085899426143072

3GRAM: uig
ENGLISH: P(g|ui) = -2.5918063569624095 ==> log prob of sentence so far: -25.854128118711195
FRENCH: P(g|ui) = -2.570847387480698 ==> log prob of sentence so far: -27.65674681362377

3GRAM: ign
ENGLISH: P(n|ig) = -0.9908290366748339 ==> log prob of sentence so far: -26.84495715538603
FRENCH: P(n|ig) = -0.44635545410867744 ==> log prob of sentence so far: -28.103102267732446

3GRAM: gno
ENGLISH: P(o|gn) = -0.5162099849683232 ==> log prob of sentence so far: -27.361167140354354
FRENCH: P(o|gn) = -0.7005935370002324 ==> log prob of sentence so far: -28.80369580473268

3GRAM: non
ENGLISH: P(n|no) = -1.207608310501746 ==> log prob of sentence so far: -28.5687754508561
FRENCH: P(n|no) = -0.8686710521546634 ==> log prob of sentence so far: -29.672366856887344

3GRAM: one
ENGLISH: P(e|on) = -0.7088247590076757 ==> log prob of sentence so far: -29.277600209863778
FRENCH: P(e|on) = -1.4137489254473474 ==> log prob of sentence so far: -31.08611578233469

3GRAM: nes
ENGLISH: P(s|ne) = -0.7270508985750305 ==> log prob of sentence so far: -30.004651108438807
FRENCH: P(s|ne) = -0.8280397816538563 ==> log prob of sentence so far: -31.914155563988547

3GRAM: esc
ENGLISH: P(c|es) = -1.5675944768247967 ==> log prob of sentence so far: -31.572245585263605
FRENCH: P(c|es) = -1.2064269173590478 ==> log prob of sentence so far: -33.1205824813476

3GRAM: sca
ENGLISH: P(a|sc) = -0.6776223081297914 ==> log prob of sentence so far: -32.2498678933934
FRENCH: P(a|sc) = -0.9461113382434008 ==> log prob of sentence so far: -34.066693819590995

3GRAM: car
ENGLISH: P(r|ca) = -0.9665961829698767 ==> log prob of sentence so far: -33.21646407636328
FRENCH: P(r|ca) = -0.9577043245477188 ==> log prob of sentence so far: -35.024398144138715

3GRAM: arg
ENGLISH: P(g|ar) = -1.6122661744754239 ==> log prob of sentence so far: -34.8287302508387
FRENCH: P(g|ar) = -1.4124827058769625 ==> log prob of sentence so far: -36.436880850015676

3GRAM: rgo
ENGLISH: P(o|rg) = -0.9728948421578737 ==> log prob of sentence so far: -35.801625092996574
FRENCH: P(o|rg) = -1.3327113436706741 ==> log prob of sentence so far: -37.76959219368635

3GRAM: got
ENGLISH: P(t|go) = -1.1516693482632028 ==> log prob of sentence so far: -36.95329444125978
FRENCH: P(t|go) = -1.6421346345582744 ==> log prob of sentence so far: -39.411726828244625

3GRAM: ots
ENGLISH: P(s|ot) = -1.365151742426073 ==> log prob of sentence so far: -38.31844618368585
FRENCH: P(s|ot) = -0.7349487122247658 ==> log prob of sentence so far: -40.14667554046939

3GRAM: tsd
ENGLISH: P(d|ts) = -1.8726564934203058 ==> log prob of sentence so far: -40.19110267710616
FRENCH: P(d|ts) = -0.9097157487197147 ==> log prob of sentence so far: -41.0563912891891

3GRAM: sde
ENGLISH: P(e|sd) = -0.5918960202089932 ==> log prob of sentence so far: -40.78299869731515
FRENCH: P(e|sd) = -0.1968243154297844 ==> log prob of sentence so far: -41.25321560461889

3GRAM: deb
ENGLISH: P(b|de) = -1.934580556259769 ==> log prob of sentence so far: -42.71757925357492
FRENCH: P(b|de) = -1.7280377104459204 ==> log prob of sentence so far: -42.981253315064805

3GRAM: ebo
ENGLISH: P(o|eb) = -0.6136368023770007 ==> log prob of sentence so far: -43.33121605595192
FRENCH: P(o|eb) = -0.7969372250247866 ==> log prob of sentence so far: -43.77819054008959

3GRAM: bou
ENGLISH: P(u|bo) = -0.7692141273098959 ==> log prob of sentence so far: -44.100430183261814
FRENCH: P(u|bo) = -0.6222902712133229 ==> log prob of sentence so far: -44.40048081130291

3GRAM: our
ENGLISH: P(r|ou) = -0.9331113741598649 ==> log prob of sentence so far: -45.03354155742168
FRENCH: P(r|ou) = -0.6948640525655961 ==> log prob of sentence so far: -45.09534486386851

3GRAM: urg
ENGLISH: P(g|ur) = -1.6935497220361015 ==> log prob of sentence so far: -46.72709127945778
FRENCH: P(g|ur) = -2.38431882290614 ==> log prob of sentence so far: -47.47966368677465

3GRAM: rgo
ENGLISH: P(o|rg) = -0.9728948421578737 ==> log prob of sentence so far: -47.69998612161565
FRENCH: P(o|rg) = -1.3327113436706741 ==> log prob of sentence so far: -48.812375030445324

3GRAM: gog
ENGLISH: P(g|go) = -2.879210605291759 ==> log prob of sentence so far: -50.57919672690741
FRENCH: P(g|go) = -2.341104638894293 ==> log prob of sentence so far: -51.15347966933962

3GRAM: ogn
ENGLISH: P(n|og) = -1.2084072285471583 ==> log prob of sentence so far: -51.78760395545457
FRENCH: P(n|og) = -1.2609127724559988 ==> log prob of sentence so far: -52.41439244179561

3GRAM: gne
ENGLISH: P(e|gn) = -0.7764744356291283 ==> log prob of sentence so far: -52.5640783910837
FRENCH: P(e|gn) = -0.3298372575340912 ==> log prob of sentence so far: -52.7442296993297

3GRAM: nea
ENGLISH: P(a|ne) = -1.0581037488786527 ==> log prob of sentence so far: -53.62218213996235
FRENCH: P(a|ne) = -1.4348089385740532 ==> log prob of sentence so far: -54.17903863790375

3GRAM: ean
ENGLISH: P(n|ea) = -0.8001087755498615 ==> log prob of sentence so far: -54.42229091551221
FRENCH: P(n|ea) = -0.9961169276321141 ==> log prob of sentence so far: -55.17515556553587

3GRAM: and
ENGLISH: P(d|an) = -0.27601282081443484 ==> log prob of sentence so far: -54.69830373632664
FRENCH: P(d|an) = -0.8454667423726812 ==> log prob of sentence so far: -56.020622307908546

3GRAM: ndc
ENGLISH: P(c|nd) = -1.5556443540500782 ==> log prob of sentence so far: -56.253948090376724
FRENCH: P(c|nd) = -2.14720569003244 ==> log prob of sentence so far: -58.16782799794099

3GRAM: dco
ENGLISH: P(o|dc) = -0.45583894901441796 ==> log prob of sentence so far: -56.709787039391145
FRENCH: P(o|dc) = -0.5261441327081049 ==> log prob of sentence so far: -58.693972130649094

3GRAM: coq
ENGLISH: P(q|co) = -3.8741337788279724 ==> log prob of sentence so far: -60.58392081821912
FRENCH: P(q|co) = -1.683003083531874 ==> log prob of sentence so far: -60.376975214180966

3GRAM: oqa
ENGLISH: P(a|oq) = -2.0863598306747484 ==> log prob of sentence so far: -62.670280648893865
FRENCH: P(a|oq) = -2.6757783416740852 ==> log prob of sentence so far: -63.05275355585505

3GRAM: qau
ENGLISH: P(u|qa) = -1.4471580313422192 ==> log prob of sentence so far: -64.11743868023608
FRENCH: P(u|qa) = -1.5314789170422551 ==> log prob of sentence so far: -64.5842324728973

3GRAM: auv
ENGLISH: P(v|au) = -3.0784568180532927 ==> log prob of sentence so far: -67.19589549828937
FRENCH: P(v|au) = -1.6107679418426117 ==> log prob of sentence so far: -66.19500041473991

3GRAM: uvi
ENGLISH: P(i|uv) = -0.2803453968041301 ==> log prob of sentence so far: -67.47624089509351
FRENCH: P(i|uv) = -1.458264190923432 ==> log prob of sentence so far: -67.65326460566334

3GRAM: vin
ENGLISH: P(n|vi) = -0.4960929483150365 ==> log prob of sentence so far: -67.97233384340855
FRENCH: P(n|vi) = -0.8700519172965795 ==> log prob of sentence so far: -68.52331652295992

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: heha
ENGLISH: P(a|heh) = -0.37093930727607216 ==> log prob of sentence so far: -0.37093930727607216
FRENCH: P(a|heh) = -1.505149978319906 ==> log prob of sentence so far: -1.505149978319906

4GRAM: ehad
ENGLISH: P(d|eha) = -0.5224599463260869 ==> log prob of sentence so far: -0.8933992536021591
FRENCH: P(d|eha) = -2.1702617153949575 ==> log prob of sentence so far: -3.6754116937148638

4GRAM: hadb
ENGLISH: P(b|had) = -0.7613445299700574 ==> log prob of sentence so far: -1.6547437835722165
FRENCH: P(b|had) = -1.8195439355418686 ==> log prob of sentence so far: -5.494955629256732

4GRAM: adbo
ENGLISH: P(o|adb) = -1.0894004112293108 ==> log prob of sentence so far: -2.7441441948015273
FRENCH: P(o|adb) = -1.4313637641589874 ==> log prob of sentence so far: -6.926319393415719

4GRAM: dboe
ENGLISH: P(e|dbo) = -2.53655844257153 ==> log prob of sentence so far: -5.280702637373057
FRENCH: P(e|dbo) = -1.4313637641589874 ==> log prob of sentence so far: -8.357683157574707

4GRAM: boeu
ENGLISH: P(u|boe) = -1.4313637641589874 ==> log prob of sentence so far: -6.712066401532045
FRENCH: P(u|boe) = -0.7781512503836436 ==> log prob of sentence so far: -9.13583440795835

4GRAM: oeuf
ENGLISH: P(f|oeu) = -1.5314789170422551 ==> log prob of sentence so far: -8.2435453185743
FRENCH: P(f|oeu) = -0.9878511104204051 ==> log prob of sentence so far: -10.123685518378755

4GRAM: eufb
ENGLISH: P(b|euf) = -1.4313637641589874 ==> log prob of sentence so far: -9.674909082733288
FRENCH: P(b|euf) = -2.1702617153949575 ==> log prob of sentence so far: -12.293947233773713

4GRAM: ufbo
ENGLISH: P(o|ufb) = -1.4313637641589874 ==> log prob of sentence so far: -11.106272846892276
FRENCH: P(o|ufb) = -1.4313637641589874 ==> log prob of sentence so far: -13.7253109979327

4GRAM: fbou
ENGLISH: P(u|fbo) = -1.9822712330395684 ==> log prob of sentence so far: -13.088544079931845
FRENCH: P(u|fbo) = -1.4313637641589874 ==> log prob of sentence so far: -15.156674762091688

4GRAM: bour
ENGLISH: P(r|bou) = -1.4239901662901933 ==> log prob of sentence so far: -14.512534246222039
FRENCH: P(r|bou) = -1.1019243731861845 ==> log prob of sentence so far: -16.258599135277873

4GRAM: ourg
ENGLISH: P(g|our) = -1.7588660347893672 ==> log prob of sentence so far: -16.271400281011406
FRENCH: P(g|our) = -2.4301246920434387 ==> log prob of sentence so far: -18.68872382732131

4GRAM: urgu
ENGLISH: P(u|urg) = -1.4828735836087539 ==> log prob of sentence so far: -17.75427386462016
FRENCH: P(u|urg) = -1.0687158123694598 ==> log prob of sentence so far: -19.757439639690773

4GRAM: rgui
ENGLISH: P(i|rgu) = -1.214843848047698 ==> log prob of sentence so far: -18.96911771266786
FRENCH: P(i|rgu) = -1.4866665726258927 ==> log prob of sentence so far: -21.244106212316666

4GRAM: guig
ENGLISH: P(g|gui) = -2.1522883443830563 ==> log prob of sentence so far: -21.121406057050915
FRENCH: P(g|gui) = -2.093421685162235 ==> log prob of sentence so far: -23.3375278974789

4GRAM: uign
ENGLISH: P(n|uig) = -1.4471580313422192 ==> log prob of sentence so far: -22.568564088393135
FRENCH: P(n|uig) = -1.2041199826559248 ==> log prob of sentence so far: -24.541647880134825

4GRAM: igno
ENGLISH: P(o|ign) = -0.6388866592839406 ==> log prob of sentence so far: -23.207450747677075
FRENCH: P(o|ign) = -1.0685231778655684 ==> log prob of sentence so far: -25.610171058000393

4GRAM: gnon
ENGLISH: P(n|gno) = -1.8325089127062364 ==> log prob of sentence so far: -25.03995966038331
FRENCH: P(n|gno) = -0.17982554244064455 ==> log prob of sentence so far: -25.78999660044104

4GRAM: none
ENGLISH: P(e|non) = -0.29065829123661774 ==> log prob of sentence so far: -25.330617951619928
FRENCH: P(e|non) = -1.5594382146201458 ==> log prob of sentence so far: -27.349434815061187

4GRAM: ones
ENGLISH: P(s|one) = -0.8644551778186058 ==> log prob of sentence so far: -26.195073129438533
FRENCH: P(s|one) = -0.7106330205628184 ==> log prob of sentence so far: -28.060067835624004

4GRAM: nesc
ENGLISH: P(c|nes) = -1.9188687433809846 ==> log prob of sentence so far: -28.113941872819517
FRENCH: P(c|nes) = -1.4934990550533696 ==> log prob of sentence so far: -29.553566890677374

4GRAM: esca
ENGLISH: P(a|esc) = -0.6082910090010099 ==> log prob of sentence so far: -28.722232881820528
FRENCH: P(a|esc) = -0.8474961382218361 ==> log prob of sentence so far: -30.40106302889921

4GRAM: scar
ENGLISH: P(r|sca) = -0.7007390270748655 ==> log prob of sentence so far: -29.42297190889539
FRENCH: P(r|sca) = -0.5939923695799293 ==> log prob of sentence so far: -30.99505539847914

4GRAM: carg
ENGLISH: P(g|car) = -1.8147317590840752 ==> log prob of sentence so far: -31.237703667979467
FRENCH: P(g|car) = -2.316669129971156 ==> log prob of sentence so far: -33.31172452845029

4GRAM: argo
ENGLISH: P(o|arg) = -1.2947810463869796 ==> log prob of sentence so far: -32.53248471436645
FRENCH: P(o|arg) = -1.283694433331499 ==> log prob of sentence so far: -34.59541896178179

4GRAM: rgot
ENGLISH: P(t|rgo) = -0.5612237373565573 ==> log prob of sentence so far: -33.093708451723
FRENCH: P(t|rgo) = -1.792391689498254 ==> log prob of sentence so far: -36.38781065128004

4GRAM: gots
ENGLISH: P(s|got) = -1.5081554884596313 ==> log prob of sentence so far: -34.60186394018263
FRENCH: P(s|got) = -0.4881166390211256 ==> log prob of sentence so far: -36.87592729030117

4GRAM: otsd
ENGLISH: P(d|ots) = -2.135662602000073 ==> log prob of sentence so far: -36.73752654218271
FRENCH: P(d|ots) = -0.7833524440694514 ==> log prob of sentence so far: -37.65927973437062

4GRAM: tsde
ENGLISH: P(e|tsd) = -0.6954816764901975 ==> log prob of sentence so far: -37.4330082186729
FRENCH: P(e|tsd) = -0.16250429647513467 ==> log prob of sentence so far: -37.821784030845755

4GRAM: sdeb
ENGLISH: P(b|sde) = -2.482873583608754 ==> log prob of sentence so far: -39.91588180228166
FRENCH: P(b|sde) = -1.6559512219851606 ==> log prob of sentence so far: -39.47773525283092

4GRAM: debo
ENGLISH: P(o|deb) = -0.9398127940166834 ==> log prob of sentence so far: -40.85569459629834
FRENCH: P(o|deb) = -0.699796445083801 ==> log prob of sentence so far: -40.177531697914716

4GRAM: ebou
ENGLISH: P(u|ebo) = -1.7399865140857933 ==> log prob of sentence so far: -42.595681110384135
FRENCH: P(u|ebo) = -0.533483576963841 ==> log prob of sentence so far: -40.711015274878555

4GRAM: bour
ENGLISH: P(r|bou) = -1.4239901662901933 ==> log prob of sentence so far: -44.01967127667433
FRENCH: P(r|bou) = -1.1019243731861845 ==> log prob of sentence so far: -41.81293964806474

4GRAM: ourg
ENGLISH: P(g|our) = -1.7588660347893672 ==> log prob of sentence so far: -45.778537311463694
FRENCH: P(g|our) = -2.4301246920434387 ==> log prob of sentence so far: -44.24306434010818

4GRAM: urgo
ENGLISH: P(o|urg) = -1.4828735836087539 ==> log prob of sentence so far: -47.261410895072444
FRENCH: P(o|urg) = -1.4366925976640543 ==> log prob of sentence so far: -45.67975693777223

4GRAM: rgog
ENGLISH: P(g|rgo) = -2.1522883443830563 ==> log prob of sentence so far: -49.413699239455504
FRENCH: P(g|rgo) = -1.792391689498254 ==> log prob of sentence so far: -47.472148627270485

4GRAM: gogn
ENGLISH: P(n|gog) = -1.4771212547196624 ==> log prob of sentence so far: -50.890820494175166
FRENCH: P(n|gog) = -1.4471580313422192 ==> log prob of sentence so far: -48.919306658612705

4GRAM: ogne
ENGLISH: P(e|ogn) = -0.9030899869919435 ==> log prob of sentence so far: -51.79391048116711
FRENCH: P(e|ogn) = -0.6690067809585756 ==> log prob of sentence so far: -49.58831343957128

4GRAM: gnea
ENGLISH: P(a|gne) = -0.8298580734575199 ==> log prob of sentence so far: -52.62376855462463
FRENCH: P(a|gne) = -1.343781976084931 ==> log prob of sentence so far: -50.932095415656214

4GRAM: nean
ENGLISH: P(n|nea) = -0.7314258252107818 ==> log prob of sentence so far: -53.35519437983542
FRENCH: P(n|nea) = -1.1486530107087416 ==> log prob of sentence so far: -52.080748426364956

4GRAM: eand
ENGLISH: P(d|ean) = -0.21552907409835442 ==> log prob of sentence so far: -53.57072345393377
FRENCH: P(d|ean) = -1.3228455305769675 ==> log prob of sentence so far: -53.403593956941926

4GRAM: andc
ENGLISH: P(c|and) = -1.4331208983531403 ==> log prob of sentence so far: -55.00384435228691
FRENCH: P(c|and) = -1.6488033948702887 ==> log prob of sentence so far: -55.05239735181222

4GRAM: ndco
ENGLISH: P(o|ndc) = -0.43844655909740055 ==> log prob of sentence so far: -55.44229091138431
FRENCH: P(o|ndc) = -0.5107298557007426 ==> log prob of sentence so far: -55.56312720751296

4GRAM: dcoq
ENGLISH: P(q|dco) = -2.69196510276736 ==> log prob of sentence so far: -58.134256014151674
FRENCH: P(q|dco) = -1.8920946026904804 ==> log prob of sentence so far: -57.45522181020344

4GRAM: coqa
ENGLISH: P(a|coq) = -1.4313637641589874 ==> log prob of sentence so far: -59.56561977831066
FRENCH: P(a|coq) = -2.357934847000454 ==> log prob of sentence so far: -59.81315665720389

4GRAM: oqau
ENGLISH: P(u|oqa) = -1.4313637641589874 ==> log prob of sentence so far: -60.99698354246965
FRENCH: P(u|oqa) = -1.4313637641589874 ==> log prob of sentence so far: -61.24452042136288

4GRAM: qauv
ENGLISH: P(v|qau) = -1.4313637641589874 ==> log prob of sentence so far: -62.42834730662864
FRENCH: P(v|qau) = -1.4313637641589874 ==> log prob of sentence so far: -62.675884185521866

4GRAM: auvi
ENGLISH: P(i|auv) = -1.4313637641589874 ==> log prob of sentence so far: -63.859711070787625
FRENCH: P(i|auv) = -1.941180036600083 ==> log prob of sentence so far: -64.61706422212195

4GRAM: uvin
ENGLISH: P(n|uvi) = -1.8325089127062364 ==> log prob of sentence so far: -65.69221998349386
FRENCH: P(n|uvi) = -1.3654879848908996 ==> log prob of sentence so far: -65.98255220701286

According to the 4gram model, the sentence is in English
----------------
