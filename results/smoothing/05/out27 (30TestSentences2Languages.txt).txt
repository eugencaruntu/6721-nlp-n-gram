J’call malade.

1GRAM MODEL:

1GRAM: j
ENGLISH: P(j) = -2.944833324319119 ==> log prob of sentence so far: -2.944833324319119
FRENCH: P(j) = -2.2095776682007906 ==> log prob of sentence so far: -2.2095776682007906

1GRAM: c
ENGLISH: P(c) = -1.626587631644617 ==> log prob of sentence so far: -4.571420955963736
FRENCH: P(c) = -1.492223661080204 ==> log prob of sentence so far: -3.7018013292809946

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -5.65820963700619
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -4.7781544243865035

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -7.005978522427191
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -6.046366904672025

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -8.353747407848193
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -7.314579384957547

1GRAM: m
ENGLISH: P(m) = -1.6111104829831362 ==> log prob of sentence so far: -9.964857890831329
FRENCH: P(m) = -1.5129486053385504 ==> log prob of sentence so far: -8.827527990296097

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -11.051646571873784
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -9.903881085401606

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -12.399415457294785
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -11.172093565687128

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -13.48620413833724
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -12.248446660792636

1GRAM: d
ENGLISH: P(d) = -1.396179917541683 ==> log prob of sentence so far: -14.882384055878923
FRENCH: P(d) = -1.4202616779892512 ==> log prob of sentence so far: -13.668708338781887

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -15.79245527393883
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -14.435674153304344

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: jc
ENGLISH: P(c|j) = -3.342422680822206 ==> log prob of sentence so far: -3.342422680822206
FRENCH: P(c|j) = -3.9335885101966532 ==> log prob of sentence so far: -3.9335885101966532

2GRAM: ca
ENGLISH: P(a|c) = -0.84612780149222 ==> log prob of sentence so far: -4.188550482314426
FRENCH: P(a|c) = -0.9172798051764028 ==> log prob of sentence so far: -4.850868315373056

2GRAM: al
ENGLISH: P(l|a) = -0.961931979448657 ==> log prob of sentence so far: -5.150482461763083
FRENCH: P(l|a) = -1.191827391645171 ==> log prob of sentence so far: -6.042695707018227

2GRAM: ll
ENGLISH: P(l|l) = -0.8384371303603633 ==> log prob of sentence so far: -5.988919592123446
FRENCH: P(l|l) = -1.0860879345809604 ==> log prob of sentence so far: -7.128783641599187

2GRAM: lm
ENGLISH: P(m|l) = -1.8223278511357694 ==> log prob of sentence so far: -7.811247443259216
FRENCH: P(m|l) = -2.143277522516325 ==> log prob of sentence so far: -9.272061164115513

2GRAM: ma
ENGLISH: P(a|m) = -0.7038280092488644 ==> log prob of sentence so far: -8.51507545250808
FRENCH: P(a|m) = -0.7372357130058481 ==> log prob of sentence so far: -10.009296877121361

2GRAM: al
ENGLISH: P(l|a) = -0.961931979448657 ==> log prob of sentence so far: -9.477007431956737
FRENCH: P(l|a) = -1.191827391645171 ==> log prob of sentence so far: -11.201124268766533

2GRAM: la
ENGLISH: P(a|l) = -1.002845754477944 ==> log prob of sentence so far: -10.479853186434681
FRENCH: P(a|l) = -0.6764237550347996 ==> log prob of sentence so far: -11.877548023801332

2GRAM: ad
ENGLISH: P(d|a) = -1.3608830323254337 ==> log prob of sentence so far: -11.840736218760116
FRENCH: P(d|a) = -1.6547663418439653 ==> log prob of sentence so far: -13.532314365645297

2GRAM: de
ENGLISH: P(e|d) = -0.8320988657918726 ==> log prob of sentence so far: -12.672835084551988
FRENCH: P(e|d) = -0.290519189469436 ==> log prob of sentence so far: -13.822833555114732

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: jca
ENGLISH: P(a|jc) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
FRENCH: P(a|jc) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874

3GRAM: cal
ENGLISH: P(l|ca) = -0.7231132971230037 ==> log prob of sentence so far: -2.154477061281991
FRENCH: P(l|ca) = -1.164304746062042 ==> log prob of sentence so far: -2.5956685102210293

3GRAM: all
ENGLISH: P(l|al) = -0.4473969486278356 ==> log prob of sentence so far: -2.6018740099098268
FRENCH: P(l|al) = -0.9318410009992406 ==> log prob of sentence so far: -3.52750951122027

3GRAM: llm
ENGLISH: P(m|ll) = -1.5577194497628937 ==> log prob of sentence so far: -4.15959345967272
FRENCH: P(m|ll) = -3.7873187566245474 ==> log prob of sentence so far: -7.314828267844817

3GRAM: lma
ENGLISH: P(a|lm) = -0.8230077147250607 ==> log prob of sentence so far: -4.982601174397781
FRENCH: P(a|lm) = -0.49533499602630726 ==> log prob of sentence so far: -7.810163263871124

3GRAM: mal
ENGLISH: P(l|ma) = -1.1323605671861814 ==> log prob of sentence so far: -6.114961741583963
FRENCH: P(l|ma) = -1.164297251856072 ==> log prob of sentence so far: -8.974460515727197

3GRAM: ala
ENGLISH: P(a|al) = -1.5184360604762794 ==> log prob of sentence so far: -7.633397802060243
FRENCH: P(a|al) = -0.6884477592924901 ==> log prob of sentence so far: -9.662908275019687

3GRAM: lad
ENGLISH: P(d|la) = -1.4444879693661663 ==> log prob of sentence so far: -9.07788577142641
FRENCH: P(d|la) = -1.5787862370722936 ==> log prob of sentence so far: -11.24169451209198

3GRAM: ade
ENGLISH: P(e|ad) = -0.7947225913774888 ==> log prob of sentence so far: -9.872608362803899
FRENCH: P(e|ad) = -0.47263109371469586 ==> log prob of sentence so far: -11.714325605806676

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: jcal
ENGLISH: P(l|jca) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
FRENCH: P(l|jca) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874

4GRAM: call
ENGLISH: P(l|cal) = -0.2867191627601086 ==> log prob of sentence so far: -1.718082926919096
FRENCH: P(l|cal) = -1.554103536667349 ==> log prob of sentence so far: -2.985467300826336

4GRAM: allm
ENGLISH: P(m|all) = -1.6827851226911572 ==> log prob of sentence so far: -3.4008680496102532
FRENCH: P(m|all) = -2.9532763366673045 ==> log prob of sentence so far: -5.938743637493641

4GRAM: llma
ENGLISH: P(a|llm) = -0.7422386944929992 ==> log prob of sentence so far: -4.143106744103252
FRENCH: P(a|llm) = -1.4313637641589874 ==> log prob of sentence so far: -7.3701074016526285

4GRAM: lmal
ENGLISH: P(l|lma) = -1.493358453590348 ==> log prob of sentence so far: -5.6364651976936
FRENCH: P(l|lma) = -1.4645321274116418 ==> log prob of sentence so far: -8.83463952906427

4GRAM: mala
ENGLISH: P(a|mal) = -1.319074498357624 ==> log prob of sentence so far: -6.955539696051225
FRENCH: P(a|mal) = -0.7781512503836436 ==> log prob of sentence so far: -9.612790779447915

4GRAM: alad
ENGLISH: P(d|ala) = -1.3230457354817016 ==> log prob of sentence so far: -8.278585431532926
FRENCH: P(d|ala) = -1.4505290274279259 ==> log prob of sentence so far: -11.06331980687584

4GRAM: lade
ENGLISH: P(e|lad) = -0.7085153222422492 ==> log prob of sentence so far: -8.987100753775175
FRENCH: P(e|lad) = -0.37430238210108574 ==> log prob of sentence so far: -11.437622188976926

According to the 4gram model, the sentence is in English
----------------
