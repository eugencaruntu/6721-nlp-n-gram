L'oiseau vole.

1GRAM MODEL:

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -1.3477688854210008
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -1.2682124802855221
DUTCH: P(l) = -1.4248882579214401 ==> log prob of sentence so far: -1.4248882579214401
PORTUGUESE: P(l) = -1.4370332293417125 ==> log prob of sentence so far: -1.4370332293417125
SPANISH: P(l) = -1.2802204153542895 ==> log prob of sentence so far: -1.2802204153542895

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -2.4855526460064956
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -2.5425558106031767
DUTCH: P(o) = -1.1998196708165811 ==> log prob of sentence so far: -2.6247079287380215
PORTUGUESE: P(o) = -0.982146444324746 ==> log prob of sentence so far: -2.4191796736664584
SPANISH: P(o) = -0.9966351897105246 ==> log prob of sentence so far: -2.276855605064814

1GRAM: i
ENGLISH: P(i) = -1.162525480273911 ==> log prob of sentence so far: -3.6480781262804065
FRENCH: P(i) = -1.1352064426688435 ==> log prob of sentence so far: -3.67776225327202
DUTCH: P(i) = -1.2262490424361108 ==> log prob of sentence so far: -3.8509569711741323
PORTUGUESE: P(i) = -1.2396988999998464 ==> log prob of sentence so far: -3.6588785736663048
SPANISH: P(i) = -1.2231544531761491 ==> log prob of sentence so far: -3.5000100582409632

1GRAM: s
ENGLISH: P(s) = -1.1711410127286013 ==> log prob of sentence so far: -4.819219139009007
FRENCH: P(s) = -1.064447714889656 ==> log prob of sentence so far: -4.742209968161676
DUTCH: P(s) = -1.460439695007922 ==> log prob of sentence so far: -5.311396666182054
PORTUGUESE: P(s) = -1.1129010285568741 ==> log prob of sentence so far: -4.771779602223178
SPANISH: P(s) = -1.1432621669802536 ==> log prob of sentence so far: -4.643272225221217

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -5.729290357068915
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -5.5091757826841325
DUTCH: P(e) = -0.7296525128280946 ==> log prob of sentence so far: -6.041049179010148
PORTUGUESE: P(e) = -0.8804550652146593 ==> log prob of sentence so far: -5.652234667437837
SPANISH: P(e) = -0.8816549652595953 ==> log prob of sentence so far: -5.524927190480812

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -6.8160790381113685
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -6.585528877789642
DUTCH: P(a) = -1.115393510092554 ==> log prob of sentence so far: -7.156442689102702
PORTUGUESE: P(a) = -0.8319048847689273 ==> log prob of sentence so far: -6.484139552206765
SPANISH: P(a) = -0.902025778564821 ==> log prob of sentence so far: -6.426952969045633

1GRAM: u
ENGLISH: P(u) = -1.5524881689579149 ==> log prob of sentence so far: -8.368567207069283
FRENCH: P(u) = -1.2061350611756212 ==> log prob of sentence so far: -7.791663938965263
DUTCH: P(u) = -1.74534287687545 ==> log prob of sentence so far: -8.901785565978152
PORTUGUESE: P(u) = -1.3141939526321222 ==> log prob of sentence so far: -7.798333504838887
SPANISH: P(u) = -1.3656717329598758 ==> log prob of sentence so far: -7.792624702005509

1GRAM: v
ENGLISH: P(v) = -2.043619342325931 ==> log prob of sentence so far: -10.412186549395214
FRENCH: P(v) = -1.82784228094936 ==> log prob of sentence so far: -9.619506219914623
DUTCH: P(v) = -1.6099977038119904 ==> log prob of sentence so far: -10.511783269790142
PORTUGUESE: P(v) = -1.7546314510401964 ==> log prob of sentence so far: -9.552964955879084
SPANISH: P(v) = -1.9460940020959083 ==> log prob of sentence so far: -9.738718704101418

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -11.549970309980708
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -10.893849550232277
DUTCH: P(o) = -1.1998196708165811 ==> log prob of sentence so far: -11.711602940606722
PORTUGUESE: P(o) = -0.982146444324746 ==> log prob of sentence so far: -10.53511140020383
SPANISH: P(o) = -0.9966351897105246 ==> log prob of sentence so far: -10.735353893811942

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -12.897739195401709
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -12.162062030517799
DUTCH: P(l) = -1.4248882579214401 ==> log prob of sentence so far: -13.136491198528162
PORTUGUESE: P(l) = -1.4370332293417125 ==> log prob of sentence so far: -11.972144629545543
SPANISH: P(l) = -1.2802204153542895 ==> log prob of sentence so far: -12.015574309166231

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -13.807810413461615
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -12.929027845040256
DUTCH: P(e) = -0.7296525128280946 ==> log prob of sentence so far: -13.866143711356257
PORTUGUESE: P(e) = -0.8804550652146593 ==> log prob of sentence so far: -12.852599694760203
SPANISH: P(e) = -0.8816549652595953 ==> log prob of sentence so far: -12.897229274425827

According to the 1gram model, the sentence is in Portuguese
----------------
2GRAM MODEL:

2GRAM: lo
ENGLISH: P(o|l) = -1.0313867050170902 ==> log prob of sentence so far: -1.0313867050170902
FRENCH: P(o|l) = -1.1639759810063668 ==> log prob of sentence so far: -1.1639759810063668
DUTCH: P(o|l) = -1.1567197126394506 ==> log prob of sentence so far: -1.1567197126394506
PORTUGUESE: P(o|l) = -1.1089097545257627 ==> log prob of sentence so far: -1.1089097545257627
SPANISH: P(o|l) = -0.7251815454396849 ==> log prob of sentence so far: -0.7251815454396849

2GRAM: oi
ENGLISH: P(i|o) = -1.769461585140497 ==> log prob of sentence so far: -2.800848290157587
FRENCH: P(i|o) = -1.0544009130666083 ==> log prob of sentence so far: -2.218376894072975
DUTCH: P(i|o) = -2.1242940363299323 ==> log prob of sentence so far: -3.281013748969383
PORTUGUESE: P(i|o) = -1.6233851985454353 ==> log prob of sentence so far: -2.732294953071198
SPANISH: P(i|o) = -2.219294756101371 ==> log prob of sentence so far: -2.944476301541056

2GRAM: is
ENGLISH: P(s|i) = -0.8699344462536623 ==> log prob of sentence so far: -3.6707827364112493
FRENCH: P(s|i) = -0.8503045030811127 ==> log prob of sentence so far: -3.0686813971540876
DUTCH: P(s|i) = -1.2809457346168842 ==> log prob of sentence so far: -4.561959483586268
PORTUGUESE: P(s|i) = -0.9633324305395075 ==> log prob of sentence so far: -3.6956273836107054
SPANISH: P(s|i) = -1.1736576739111495 ==> log prob of sentence so far: -4.1181339754522055

2GRAM: se
ENGLISH: P(e|s) = -0.9357350813780119 ==> log prob of sentence so far: -4.606517817789261
FRENCH: P(e|s) = -0.7710764417175806 ==> log prob of sentence so far: -3.839757838871668
DUTCH: P(e|s) = -1.2731857466924617 ==> log prob of sentence so far: -5.835145230278729
PORTUGUESE: P(e|s) = -0.7112499318536796 ==> log prob of sentence so far: -4.406877315464385
SPANISH: P(e|s) = -0.8023625514175101 ==> log prob of sentence so far: -4.920496526869716

2GRAM: ea
ENGLISH: P(a|e) = -1.0585605717592588 ==> log prob of sentence so far: -5.66507838954852
FRENCH: P(a|e) = -1.5190302730971181 ==> log prob of sentence so far: -5.358788111968787
DUTCH: P(a|e) = -2.13121546254526 ==> log prob of sentence so far: -7.966360692823989
PORTUGUESE: P(a|e) = -1.2816437516923342 ==> log prob of sentence so far: -5.688521067156719
SPANISH: P(a|e) = -1.4656394168016165 ==> log prob of sentence so far: -6.3861359436713325

2GRAM: au
ENGLISH: P(u|a) = -2.1220107996271214 ==> log prob of sentence so far: -7.787089189175641
FRENCH: P(u|a) = -1.079016828317488 ==> log prob of sentence so far: -6.4378049402862745
DUTCH: P(u|a) = -2.4976859115106795 ==> log prob of sentence so far: -10.46404660433467
PORTUGUESE: P(u|a) = -1.9415346072575186 ==> log prob of sentence so far: -7.630055674414238
SPANISH: P(u|a) = -1.8963498184286804 ==> log prob of sentence so far: -8.282485762100013

2GRAM: uv
ENGLISH: P(v|u) = -2.9727710636098093 ==> log prob of sentence so far: -10.759860252785451
FRENCH: P(v|u) = -1.5222341223399225 ==> log prob of sentence so far: -7.960039062626197
DUTCH: P(v|u) = -2.1833138800996443 ==> log prob of sentence so far: -12.647360484434314
PORTUGUESE: P(v|u) = -1.752219005284925 ==> log prob of sentence so far: -9.382274679699163
SPANISH: P(v|u) = -2.141097382208373 ==> log prob of sentence so far: -10.423583144308386

2GRAM: vo
ENGLISH: P(o|v) = -1.222683931569715 ==> log prob of sentence so far: -11.982544184355167
FRENCH: P(o|v) = -0.7098703324399372 ==> log prob of sentence so far: -8.669909395066133
DUTCH: P(o|v) = -0.7282693262650096 ==> log prob of sentence so far: -13.375629810699323
PORTUGUESE: P(o|v) = -0.9176140149487747 ==> log prob of sentence so far: -10.299888694647938
SPANISH: P(o|v) = -0.9157098575472005 ==> log prob of sentence so far: -11.339293001855587

2GRAM: ol
ENGLISH: P(l|o) = -1.416314628982505 ==> log prob of sentence so far: -13.398858813337672
FRENCH: P(l|o) = -1.4870835318641464 ==> log prob of sentence so far: -10.156992926930279
DUTCH: P(l|o) = -1.2575309318354733 ==> log prob of sentence so far: -14.633160742534796
PORTUGUESE: P(l|o) = -1.3486204293639907 ==> log prob of sentence so far: -11.648509124011929
SPANISH: P(l|o) = -1.247037450489288 ==> log prob of sentence so far: -12.586330452344875

2GRAM: le
ENGLISH: P(e|l) = -0.6938243412569666 ==> log prob of sentence so far: -14.092683154594638
FRENCH: P(e|l) = -0.40129432934375414 ==> log prob of sentence so far: -10.558287256274033
DUTCH: P(e|l) = -0.7538983528428277 ==> log prob of sentence so far: -15.387059095377625
PORTUGUESE: P(e|l) = -0.8509960063785604 ==> log prob of sentence so far: -12.499505130390489
SPANISH: P(e|l) = -0.8393391903088171 ==> log prob of sentence so far: -13.425669642653691

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: loi
ENGLISH: P(i|lo) = -2.4353390274464712 ==> log prob of sentence so far: -2.4353390274464712
FRENCH: P(i|lo) = -1.3170816485885595 ==> log prob of sentence so far: -1.3170816485885595
DUTCH: P(i|lo) = -2.293864571276307 ==> log prob of sentence so far: -2.293864571276307
PORTUGUESE: P(i|lo) = -1.911690158753861 ==> log prob of sentence so far: -1.911690158753861
SPANISH: P(i|lo) = -1.975022408655535 ==> log prob of sentence so far: -1.975022408655535

3GRAM: ois
ENGLISH: P(s|oi) = -0.8290745815850581 ==> log prob of sentence so far: -3.2644136090315294
FRENCH: P(s|oi) = -0.5633961757799035 ==> log prob of sentence so far: -1.880477824368463
DUTCH: P(s|oi) = -1.2510560207021426 ==> log prob of sentence so far: -3.5449205919784497
PORTUGUESE: P(s|oi) = -0.5649446509762609 ==> log prob of sentence so far: -2.476634809730122
SPANISH: P(s|oi) = -1.539269161468507 ==> log prob of sentence so far: -3.5142915701240423

3GRAM: ise
ENGLISH: P(e|is) = -1.1253912536423671 ==> log prob of sentence so far: -4.389804862673897
FRENCH: P(e|is) = -0.9067694313268164 ==> log prob of sentence so far: -2.7872472556952794
DUTCH: P(e|is) = -1.275777170761014 ==> log prob of sentence so far: -4.820697762739464
PORTUGUESE: P(e|is) = -1.2386692516249649 ==> log prob of sentence so far: -3.715304061355087
SPANISH: P(e|is) = -1.0791812460476249 ==> log prob of sentence so far: -4.593472816171667

3GRAM: sea
ENGLISH: P(a|se) = -0.8532077360782319 ==> log prob of sentence so far: -5.243012598752128
FRENCH: P(a|se) = -1.3610505990816937 ==> log prob of sentence so far: -4.148297854776973
DUTCH: P(a|se) = -3.17435059747938 ==> log prob of sentence so far: -7.995048360218844
PORTUGUESE: P(a|se) = -1.1534485698022627 ==> log prob of sentence so far: -4.868752631157349
SPANISH: P(a|se) = -1.0680318839101566 ==> log prob of sentence so far: -5.661504700081824

3GRAM: eau
ENGLISH: P(u|ea) = -2.3739306942824907 ==> log prob of sentence so far: -7.616943293034619
FRENCH: P(u|ea) = -0.4024427318596305 ==> log prob of sentence so far: -4.5507405866366035
DUTCH: P(u|ea) = -1.8279771030787315 ==> log prob of sentence so far: -9.823025463297576
PORTUGUESE: P(u|ea) = -1.9813947554829396 ==> log prob of sentence so far: -6.850147386640289
SPANISH: P(u|ea) = -1.6210377287408713 ==> log prob of sentence so far: -7.282542428822696

3GRAM: auv
ENGLISH: P(v|au) = -3.0784568180532927 ==> log prob of sentence so far: -10.695400111087912
FRENCH: P(v|au) = -1.6107679418426117 ==> log prob of sentence so far: -6.1615085284792155
DUTCH: P(v|au) = -1.8731267636145004 ==> log prob of sentence so far: -11.696152226912076
PORTUGUESE: P(v|au) = -2.3979400086720375 ==> log prob of sentence so far: -9.248087395312327
SPANISH: P(v|au) = -3.060697840353612 ==> log prob of sentence so far: -10.343240269176308

3GRAM: uvo
ENGLISH: P(o|uv) = -1.9138138523837167 ==> log prob of sentence so far: -12.609213963471628
FRENCH: P(o|uv) = -1.3961162841745873 ==> log prob of sentence so far: -7.557624812653803
DUTCH: P(o|uv) = -0.910268571619067 ==> log prob of sentence so far: -12.606420798531143
PORTUGUESE: P(o|uv) = -1.3802112417116061 ==> log prob of sentence so far: -10.628298637023933
SPANISH: P(o|uv) = -0.5884871841386885 ==> log prob of sentence so far: -10.931727453314997

3GRAM: vol
ENGLISH: P(l|vo) = -0.7434523004514394 ==> log prob of sentence so far: -13.352666263923068
FRENCH: P(l|vo) = -1.187070129945028 ==> log prob of sentence so far: -8.74469494259883
DUTCH: P(l|vo) = -0.665836734169069 ==> log prob of sentence so far: -13.272257532700213
PORTUGUESE: P(l|vo) = -0.6940927307170226 ==> log prob of sentence so far: -11.322391367740956
SPANISH: P(l|vo) = -0.5310457055530639 ==> log prob of sentence so far: -11.46277315886806

3GRAM: ole
ENGLISH: P(e|ol) = -0.7622341797266956 ==> log prob of sentence so far: -14.114900443649763
FRENCH: P(e|ol) = -0.4569096911365881 ==> log prob of sentence so far: -9.201604633735418
DUTCH: P(e|ol) = -1.8265268004678048 ==> log prob of sentence so far: -15.098784333168018
PORTUGUESE: P(e|ol) = -0.9893810239897061 ==> log prob of sentence so far: -12.311772391730662
SPANISH: P(e|ol) = -0.709408957450662 ==> log prob of sentence so far: -12.172182116318723

According to the 3gram model, the sentence is in French
----------------
4GRAM MODEL:

4GRAM: lois
ENGLISH: P(s|loi) = -1.255272505103306 ==> log prob of sentence so far: -1.255272505103306
FRENCH: P(s|loi) = -0.7978953085794007 ==> log prob of sentence so far: -0.7978953085794007
DUTCH: P(s|loi) = -1.5563025007672873 ==> log prob of sentence so far: -1.5563025007672873
PORTUGUESE: P(s|loi) = -1.6020599913279623 ==> log prob of sentence so far: -1.6020599913279623
SPANISH: P(s|loi) = -1.9912260756924949 ==> log prob of sentence so far: -1.9912260756924949

4GRAM: oise
ENGLISH: P(e|ois) = -0.5000158623208334 ==> log prob of sentence so far: -1.7552883674241393
FRENCH: P(e|ois) = -0.8533089508163634 ==> log prob of sentence so far: -1.651204259395764
DUTCH: P(e|ois) = -0.9637878273455552 ==> log prob of sentence so far: -2.5200903281128424
PORTUGUESE: P(e|ois) = -1.0791812460476249 ==> log prob of sentence so far: -2.681241237375587
SPANISH: P(e|ois) = -1.1026623418971477 ==> log prob of sentence so far: -3.0938884175896426

4GRAM: isea
ENGLISH: P(a|ise) = -1.0749441931043702 ==> log prob of sentence so far: -2.8302325605285095
FRENCH: P(a|ise) = -1.0049919455574992 ==> log prob of sentence so far: -2.6561962049532633
DUTCH: P(a|ise) = -2.187520720836463 ==> log prob of sentence so far: -4.707611048949305
PORTUGUESE: P(a|ise) = -1.7732987475892314 ==> log prob of sentence so far: -4.454539984964819
SPANISH: P(a|ise) = -1.72916478969277 ==> log prob of sentence so far: -4.823053207282412

4GRAM: seau
ENGLISH: P(u|sea) = -2.1513462735539934 ==> log prob of sentence so far: -4.981578834082503
FRENCH: P(u|sea) = -0.131824637085132 ==> log prob of sentence so far: -2.7880208420383954
DUTCH: P(u|sea) = -1.4313637641589874 ==> log prob of sentence so far: -6.138974813108293
PORTUGUESE: P(u|sea) = -2.1818435879447726 ==> log prob of sentence so far: -6.636383572909591
SPANISH: P(u|sea) = -1.7728549105736953 ==> log prob of sentence so far: -6.595908117856108

4GRAM: eauv
ENGLISH: P(v|eau) = -2.03342375548695 ==> log prob of sentence so far: -7.015002589569454
FRENCH: P(v|eau) = -2.0863598306747484 ==> log prob of sentence so far: -4.874380672713144
DUTCH: P(v|eau) = -1.1249387366083 ==> log prob of sentence so far: -7.263913549716593
PORTUGUESE: P(v|eau) = -1.7481880270062005 ==> log prob of sentence so far: -8.384571599915791
SPANISH: P(v|eau) = -2.0 ==> log prob of sentence so far: -8.595908117856109

4GRAM: auvo
ENGLISH: P(o|auv) = -1.4313637641589874 ==> log prob of sentence so far: -8.446366353728441
FRENCH: P(o|auv) = -1.5732032513054885 ==> log prob of sentence so far: -6.447583924018633
DUTCH: P(o|auv) = -0.9700367766225568 ==> log prob of sentence so far: -8.23395032633915
PORTUGUESE: P(o|auv) = -1.4471580313422192 ==> log prob of sentence so far: -9.83172963125801
SPANISH: P(o|auv) = -1.4313637641589874 ==> log prob of sentence so far: -10.027271882015096

4GRAM: uvol
ENGLISH: P(l|uvo) = -1.4313637641589874 ==> log prob of sentence so far: -9.87773011788743
FRENCH: P(l|uvo) = -0.9378520932511555 ==> log prob of sentence so far: -7.385436017269789
DUTCH: P(l|uvo) = -0.9030899869919435 ==> log prob of sentence so far: -9.137040313331093
PORTUGUESE: P(l|uvo) = -0.7781512503836436 ==> log prob of sentence so far: -10.609880881641654
SPANISH: P(l|uvo) = -0.9802559418042428 ==> log prob of sentence so far: -11.00752782381934

4GRAM: vole
ENGLISH: P(e|vol) = -1.1583624920952496 ==> log prob of sentence so far: -11.03609260998268
FRENCH: P(e|vol) = -1.097664651741638 ==> log prob of sentence so far: -8.483100669011426
DUTCH: P(e|vol) = -1.8103359337550449 ==> log prob of sentence so far: -10.947376247086138
PORTUGUESE: P(e|vol) = -1.6473829701146199 ==> log prob of sentence so far: -12.257263851756274
SPANISH: P(e|vol) = -1.463757293161681 ==> log prob of sentence so far: -12.47128511698102

According to the 4gram model, the sentence is in French
----------------
