Woody Allen parle.

1GRAM MODEL:

1GRAM: w
ENGLISH: P(w) = -1.6313539967808455 ==> log prob of sentence so far: -1.6313539967808455
FRENCH: P(w) = -3.9191827290425008 ==> log prob of sentence so far: -3.9191827290425008

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -2.76913775736634
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -5.193526059360155

1GRAM: o
ENGLISH: P(o) = -1.1377837605854946 ==> log prob of sentence so far: -3.906921517951835
FRENCH: P(o) = -1.2743433303176548 ==> log prob of sentence so far: -6.46786938967781

1GRAM: d
ENGLISH: P(d) = -1.396179917541683 ==> log prob of sentence so far: -5.3031014354935175
FRENCH: P(d) = -1.4202616779892512 ==> log prob of sentence so far: -7.888131067667062

1GRAM: y
ENGLISH: P(y) = -1.7505965755952897 ==> log prob of sentence so far: -7.053698011088807
FRENCH: P(y) = -2.672224427638286 ==> log prob of sentence so far: -10.560355495305348

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -8.140486692131262
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -11.636708590410857

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -9.488255577552263
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -12.904921070696378

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -10.836024462973263
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -14.1731335509819

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -11.74609568103317
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -14.940099365504356

1GRAM: n
ENGLISH: P(n) = -1.1615969374026291 ==> log prob of sentence so far: -12.9076926184358
FRENCH: P(n) = -1.1226374766145046 ==> log prob of sentence so far: -16.06273684211886

1GRAM: p
ENGLISH: P(p) = -1.7418254600332643 ==> log prob of sentence so far: -14.649518078469065
FRENCH: P(p) = -1.538635710757223 ==> log prob of sentence so far: -17.601372552876082

1GRAM: a
ENGLISH: P(a) = -1.086788681042454 ==> log prob of sentence so far: -15.73630675951152
FRENCH: P(a) = -1.076353095105509 ==> log prob of sentence so far: -18.677725647981593

1GRAM: r
ENGLISH: P(r) = -1.261272901312096 ==> log prob of sentence so far: -16.997579660823614
FRENCH: P(r) = -1.1840286390704569 ==> log prob of sentence so far: -19.86175428705205

1GRAM: l
ENGLISH: P(l) = -1.3477688854210008 ==> log prob of sentence so far: -18.345348546244615
FRENCH: P(l) = -1.2682124802855221 ==> log prob of sentence so far: -21.129966767337574

1GRAM: e
ENGLISH: P(e) = -0.9100712180599071 ==> log prob of sentence so far: -19.255419764304524
FRENCH: P(e) = -0.7669658145224565 ==> log prob of sentence so far: -21.89693258186003

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: wo
ENGLISH: P(o|w) = -1.0766814777608746 ==> log prob of sentence so far: -1.0766814777608746
FRENCH: P(o|w) = -1.806179973983887 ==> log prob of sentence so far: -1.806179973983887

2GRAM: oo
ENGLISH: P(o|o) = -1.3567983196477529 ==> log prob of sentence so far: -2.4334797974086273
FRENCH: P(o|o) = -2.756524069532728 ==> log prob of sentence so far: -4.562704043516615

2GRAM: od
ENGLISH: P(d|o) = -1.593925335082911 ==> log prob of sentence so far: -4.0274051324915385
FRENCH: P(d|o) = -1.9548917232995615 ==> log prob of sentence so far: -6.517595766816177

2GRAM: dy
ENGLISH: P(y|d) = -1.7554636173254634 ==> log prob of sentence so far: -5.782868749817002
FRENCH: P(y|d) = -3.151245580295761 ==> log prob of sentence so far: -9.668841347111938

2GRAM: ya
ENGLISH: P(a|y) = -1.042285010275727 ==> log prob of sentence so far: -6.825153760092729
FRENCH: P(a|y) = -0.6224025699054712 ==> log prob of sentence so far: -10.29124391701741

2GRAM: al
ENGLISH: P(l|a) = -0.961931979448657 ==> log prob of sentence so far: -7.787085739541386
FRENCH: P(l|a) = -1.191827391645171 ==> log prob of sentence so far: -11.48307130866258

2GRAM: ll
ENGLISH: P(l|l) = -0.8384371303603633 ==> log prob of sentence so far: -8.625522869901749
FRENCH: P(l|l) = -1.0860879345809604 ==> log prob of sentence so far: -12.569159243243542

2GRAM: le
ENGLISH: P(e|l) = -0.6938243412569666 ==> log prob of sentence so far: -9.319347211158716
FRENCH: P(e|l) = -0.40129432934375414 ==> log prob of sentence so far: -12.970453572587296

2GRAM: en
ENGLISH: P(n|e) = -1.0655182392646758 ==> log prob of sentence so far: -10.384865450423392
FRENCH: P(n|e) = -0.8688224207402516 ==> log prob of sentence so far: -13.839275993327547

2GRAM: np
ENGLISH: P(p|n) = -2.2279170156247026 ==> log prob of sentence so far: -12.612782466048095
FRENCH: P(p|n) = -1.8907933448423553 ==> log prob of sentence so far: -15.730069338169903

2GRAM: pa
ENGLISH: P(a|p) = -0.9079158130990952 ==> log prob of sentence so far: -13.52069827914719
FRENCH: P(a|p) = -0.6470605885380062 ==> log prob of sentence so far: -16.37712992670791

2GRAM: ar
ENGLISH: P(r|a) = -0.9854656313970852 ==> log prob of sentence so far: -14.506163910544275
FRENCH: P(r|a) = -1.0331920938948058 ==> log prob of sentence so far: -17.410322020602713

2GRAM: rl
ENGLISH: P(l|r) = -1.772342048861236 ==> log prob of sentence so far: -16.27850595940551
FRENCH: P(l|r) = -1.3539339270813628 ==> log prob of sentence so far: -18.764255947684077

2GRAM: le
ENGLISH: P(e|l) = -0.6938243412569666 ==> log prob of sentence so far: -16.972330300662477
FRENCH: P(e|l) = -0.40129432934375414 ==> log prob of sentence so far: -19.165550277027833

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: woo
ENGLISH: P(o|wo) = -1.1070207757952126 ==> log prob of sentence so far: -1.1070207757952126
FRENCH: P(o|wo) = -1.4471580313422192 ==> log prob of sentence so far: -1.4471580313422192

3GRAM: ood
ENGLISH: P(d|oo) = -0.7124573625369396 ==> log prob of sentence so far: -1.819478138332152
FRENCH: P(d|oo) = -1.7103994661168007 ==> log prob of sentence so far: -3.15755749745902

3GRAM: ody
ENGLISH: P(y|od) = -1.0139440646089992 ==> log prob of sentence so far: -2.8334222029411515
FRENCH: P(y|od) = -2.448190836779987 ==> log prob of sentence so far: -5.605748334239006

3GRAM: dya
ENGLISH: P(a|dy) = -1.1546023432219428 ==> log prob of sentence so far: -3.9880245461630945
FRENCH: P(a|dy) = -1.0934216851622351 ==> log prob of sentence so far: -6.699170019401242

3GRAM: yal
ENGLISH: P(l|ya) = -0.979315401140149 ==> log prob of sentence so far: -4.967339947303244
FRENCH: P(l|ya) = -1.347182120038183 ==> log prob of sentence so far: -8.046352139439424

3GRAM: all
ENGLISH: P(l|al) = -0.4473969486278356 ==> log prob of sentence so far: -5.414736895931079
FRENCH: P(l|al) = -0.9318410009992406 ==> log prob of sentence so far: -8.978193140438664

3GRAM: lle
ENGLISH: P(e|ll) = -0.9440510492295612 ==> log prob of sentence so far: -6.35878794516064
FRENCH: P(e|ll) = -0.1596457248579317 ==> log prob of sentence so far: -9.137838865296596

3GRAM: len
ENGLISH: P(n|le) = -1.2613481306686272 ==> log prob of sentence so far: -7.620136075829267
FRENCH: P(n|le) = -1.164379533169003 ==> log prob of sentence so far: -10.302218398465598

3GRAM: enp
ENGLISH: P(p|en) = -2.0704164967495085 ==> log prob of sentence so far: -9.690552572578776
FRENCH: P(p|en) = -1.9241695603205442 ==> log prob of sentence so far: -12.226387958786143

3GRAM: npa
ENGLISH: P(a|np) = -0.7859365071903064 ==> log prob of sentence so far: -10.476489079769083
FRENCH: P(a|np) = -0.6702841185908591 ==> log prob of sentence so far: -12.896672077377001

3GRAM: par
ENGLISH: P(r|pa) = -0.48565020314281593 ==> log prob of sentence so far: -10.962139282911899
FRENCH: P(r|pa) = -0.3235383556648062 ==> log prob of sentence so far: -13.220210433041808

3GRAM: arl
ENGLISH: P(l|ar) = -1.6012428801143832 ==> log prob of sentence so far: -12.563382163026283
FRENCH: P(l|ar) = -1.0508001536800562 ==> log prob of sentence so far: -14.271010586721864

3GRAM: rle
ENGLISH: P(e|rl) = -0.8414039903942658 ==> log prob of sentence so far: -13.404786153420549
FRENCH: P(e|rl) = -0.20386026303376995 ==> log prob of sentence so far: -14.474870849755634

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: wood
ENGLISH: P(d|woo) = -0.2611584836262889 ==> log prob of sentence so far: -0.2611584836262889
FRENCH: P(d|woo) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874

4GRAM: oody
ENGLISH: P(y|ood) = -1.3470428784589514 ==> log prob of sentence so far: -1.6082013620852402
FRENCH: P(y|ood) = -1.4471580313422192 ==> log prob of sentence so far: -2.8785217955012063

4GRAM: odya
ENGLISH: P(a|ody) = -0.890278011231978 ==> log prob of sentence so far: -2.498479373317218
FRENCH: P(a|ody) = -1.4471580313422192 ==> log prob of sentence so far: -4.325679826843426

4GRAM: dyal
ENGLISH: P(l|dya) = -1.5720967679505191 ==> log prob of sentence so far: -4.070576141267737
FRENCH: P(l|dya) = -1.4771212547196624 ==> log prob of sentence so far: -5.802801081563088

4GRAM: yall
ENGLISH: P(l|yal) = -0.39058457604006663 ==> log prob of sentence so far: -4.461160717307804
FRENCH: P(l|yal) = -1.0644579892269184 ==> log prob of sentence so far: -6.8672590707900065

4GRAM: alle
ENGLISH: P(e|all) = -1.0193822192505801 ==> log prob of sentence so far: -5.480542936558384
FRENCH: P(e|all) = -0.5467361562333491 ==> log prob of sentence so far: -7.413995227023356

4GRAM: llen
ENGLISH: P(n|lle) = -1.019744058195757 ==> log prob of sentence so far: -6.500286994754141
FRENCH: P(n|lle) = -1.279063774384823 ==> log prob of sentence so far: -8.693059001408178

4GRAM: lenp
ENGLISH: P(p|len) = -2.9628426812012423 ==> log prob of sentence so far: -9.463129675955383
FRENCH: P(p|len) = -2.6089536992758626 ==> log prob of sentence so far: -11.302012700684042

4GRAM: enpa
ENGLISH: P(a|enp) = -0.7509990043400289 ==> log prob of sentence so far: -10.214128680295412
FRENCH: P(a|enp) = -0.6548360464395573 ==> log prob of sentence so far: -11.9568487471236

4GRAM: npar
ENGLISH: P(r|npa) = -0.4471580313422192 ==> log prob of sentence so far: -10.661286711637631
FRENCH: P(r|npa) = -0.375835441651318 ==> log prob of sentence so far: -12.332684188774918

4GRAM: parl
ENGLISH: P(l|par) = -1.7899483948294548 ==> log prob of sentence so far: -12.451235106467086
FRENCH: P(l|par) = -0.6924990212795935 ==> log prob of sentence so far: -13.02518321005451

4GRAM: arle
ENGLISH: P(e|arl) = -0.8565138449505885 ==> log prob of sentence so far: -13.307748951417675
FRENCH: P(e|arl) = -0.18343538379607374 ==> log prob of sentence so far: -13.208618593850584

According to the 4gram model, the sentence is in French
----------------
