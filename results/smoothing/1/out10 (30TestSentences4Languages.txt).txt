J'aime l'IA.

1GRAM MODEL:

1GRAM: j
ENGLISH: P(j) = -2.944639589289319 ==> log prob of sentence so far: -2.944639589289319
FRENCH: P(j) = -2.2095350623538743 ==> log prob of sentence so far: -2.2095350623538743
DUTCH: P(j) = -1.7474959977336848 ==> log prob of sentence so far: -1.7474959977336848
PORTUGUESE: P(j) = -2.514378826233088 ==> log prob of sentence so far: -2.514378826233088
SPANISH: P(j) = -2.3397264898243275 ==> log prob of sentence so far: -2.3397264898243275

1GRAM: a
ENGLISH: P(a) = -1.086791807028239 ==> log prob of sentence so far: -4.031431396317558
FRENCH: P(a) = -1.0763575048838907 ==> log prob of sentence so far: -3.285892567237765
DUTCH: P(a) = -1.1154003185206602 ==> log prob of sentence so far: -2.862896316254345
PORTUGUESE: P(a) = -0.831923326155409 ==> log prob of sentence so far: -3.346302152388497
SPANISH: P(a) = -0.9020364569062174 ==> log prob of sentence so far: -3.241762946730545

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -5.193959475081954
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -4.421102877709065
DUTCH: P(i) = -1.2262538577433346 ==> log prob of sentence so far: -4.089150173997679
PORTUGUESE: P(i) = -1.2397071887182836 ==> log prob of sentence so far: -4.586009341106781
SPANISH: P(i) = -1.2231599543062535 ==> log prob of sentence so far: -4.464922901036799

1GRAM: m
ENGLISH: P(m) = -1.6111071179746999 ==> log prob of sentence so far: -6.805066593056654
FRENCH: P(m) = -1.5129465442835708 ==> log prob of sentence so far: -5.934049421992635
DUTCH: P(m) = -1.618856509410599 ==> log prob of sentence so far: -5.7080066834082785
PORTUGUESE: P(m) = -1.314397077219965 ==> log prob of sentence so far: -5.900406418326746
SPANISH: P(m) = -1.4970785320245885 ==> log prob of sentence so far: -5.962001433061387

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -7.715141862666553
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -6.701021549145082
DUTCH: P(e) = -0.7296633556789381 ==> log prob of sentence so far: -6.437670039087217
PORTUGUESE: P(e) = -0.8804727355199955 ==> log prob of sentence so far: -6.780879153846741
SPANISH: P(e) = -0.8816658603059454 ==> log prob of sentence so far: -6.843667293367332

1GRAM: l
ENGLISH: P(l) = -1.3477697305137912 ==> log prob of sentence so far: -9.062911593180345
FRENCH: P(l) = -1.2682148156867032 ==> log prob of sentence so far: -7.969236364831786
DUTCH: P(l) = -1.4248879424212282 ==> log prob of sentence so far: -7.862557981508445
PORTUGUESE: P(l) = -1.437031928760182 ==> log prob of sentence so far: -8.217911082606923
SPANISH: P(l) = -1.280224525407062 ==> log prob of sentence so far: -8.123891818774394

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -10.225439671944741
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -9.104446675303086
DUTCH: P(i) = -1.2262538577433346 ==> log prob of sentence so far: -9.088811839251779
PORTUGUESE: P(i) = -1.2397071887182836 ==> log prob of sentence so far: -9.457618271325206
SPANISH: P(i) = -1.2231599543062535 ==> log prob of sentence so far: -9.347051773080647

1GRAM: a
ENGLISH: P(a) = -1.086791807028239 ==> log prob of sentence so far: -11.312231478972981
FRENCH: P(a) = -1.0763575048838907 ==> log prob of sentence so far: -10.180804180186977
DUTCH: P(a) = -1.1154003185206602 ==> log prob of sentence so far: -10.204212157772439
PORTUGUESE: P(a) = -0.831923326155409 ==> log prob of sentence so far: -10.289541597480614
SPANISH: P(a) = -0.9020364569062174 ==> log prob of sentence so far: -10.249088229986864

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: ja
ENGLISH: P(a|j) = -0.6962471460005455 ==> log prob of sentence so far: -0.6962471460005455
FRENCH: P(a|j) = -0.8130142732186328 ==> log prob of sentence so far: -0.8130142732186328
DUTCH: P(a|j) = -1.2162858897109594 ==> log prob of sentence so far: -1.2162858897109594
PORTUGUESE: P(a|j) = -0.3886515353136907 ==> log prob of sentence so far: -0.3886515353136907
SPANISH: P(a|j) = -0.5784579726092458 ==> log prob of sentence so far: -0.5784579726092458

2GRAM: ai
ENGLISH: P(i|a) = -1.3482657995085439 ==> log prob of sentence so far: -2.0445129455090894
FRENCH: P(i|a) = -0.6966817475436522 ==> log prob of sentence so far: -1.5096960207622852
DUTCH: P(i|a) = -2.2545178663697243 ==> log prob of sentence so far: -3.4708037560806835
PORTUGUESE: P(i|a) = -1.474025539058395 ==> log prob of sentence so far: -1.8626770743720855
SPANISH: P(i|a) = -1.9689927989998801 ==> log prob of sentence so far: -2.547450771609126

2GRAM: im
ENGLISH: P(m|i) = -1.3479991992166425 ==> log prob of sentence so far: -3.392512144725732
FRENCH: P(m|i) = -1.5426642768181869 ==> log prob of sentence so far: -3.052360297580472
DUTCH: P(m|i) = -2.1518638486432375 ==> log prob of sentence so far: -5.622667604723921
PORTUGUESE: P(m|i) = -1.1493530095215254 ==> log prob of sentence so far: -3.012030083893611
SPANISH: P(m|i) = -1.459046193731474 ==> log prob of sentence so far: -4.0064969653406

2GRAM: me
ENGLISH: P(e|m) = -0.5947431843948017 ==> log prob of sentence so far: -3.9872553291205337
FRENCH: P(e|m) = -0.4308235270940095 ==> log prob of sentence so far: -3.4831838246744815
DUTCH: P(e|m) = -0.4602117516446211 ==> log prob of sentence so far: -6.082879356368542
PORTUGUESE: P(e|m) = -0.7099011207371668 ==> log prob of sentence so far: -3.7219312046307778
SPANISH: P(e|m) = -0.5851782567080399 ==> log prob of sentence so far: -4.591675222048639

2GRAM: el
ENGLISH: P(l|e) = -1.3277895145531675 ==> log prob of sentence so far: -5.315044843673701
FRENCH: P(l|e) = -1.2044529937000865 ==> log prob of sentence so far: -4.687636818374568
DUTCH: P(l|e) = -1.1637554586225491 ==> log prob of sentence so far: -7.246634814991091
PORTUGUESE: P(l|e) = -1.128840327441183 ==> log prob of sentence so far: -4.850771532071961
SPANISH: P(l|e) = -0.9034803640806133 ==> log prob of sentence so far: -5.495155586129252

2GRAM: li
ENGLISH: P(i|l) = -0.9282177691743759 ==> log prob of sentence so far: -6.243262612848077
FRENCH: P(i|l) = -1.2030244854616337 ==> log prob of sentence so far: -5.890661303836201
DUTCH: P(i|l) = -0.7915222759952025 ==> log prob of sentence so far: -8.038157090986294
PORTUGUESE: P(i|l) = -1.0733453507773052 ==> log prob of sentence so far: -5.924116882849265
SPANISH: P(i|l) = -1.253800786717162 ==> log prob of sentence so far: -6.748956372846415

2GRAM: ia
ENGLISH: P(a|i) = -1.7531361889780281 ==> log prob of sentence so far: -7.996398801826105
FRENCH: P(a|i) = -1.7741912984155617 ==> log prob of sentence so far: -7.664852602251763
DUTCH: P(a|i) = -2.0494658369319145 ==> log prob of sentence so far: -10.087622927918208
PORTUGUESE: P(a|i) = -0.8378294823252691 ==> log prob of sentence so far: -6.761946365174534
SPANISH: P(a|i) = -0.8675704342028947 ==> log prob of sentence so far: -7.6165268070493095

According to the 2gram model, the sentence is in Portuguese
----------------
3GRAM MODEL:

3GRAM: jai
ENGLISH: P(i|ja) = -1.794139355767774 ==> log prob of sentence so far: -1.794139355767774
FRENCH: P(i|ja) = -0.7435350518973153 ==> log prob of sentence so far: -0.7435350518973153
DUTCH: P(i|ja) = -2.6646419755561257 ==> log prob of sentence so far: -2.6646419755561257
PORTUGUESE: P(i|ja) = -1.8864907251724818 ==> log prob of sentence so far: -1.8864907251724818
SPANISH: P(i|ja) = -2.3541084391474008 ==> log prob of sentence so far: -2.3541084391474008

3GRAM: aim
ENGLISH: P(m|ai) = -1.9438653380278803 ==> log prob of sentence so far: -3.7380046937956544
FRENCH: P(m|ai) = -1.9998514978676236 ==> log prob of sentence so far: -2.743386549764939
DUTCH: P(m|ai) = -2.0 ==> log prob of sentence so far: -4.664641975556126
PORTUGUESE: P(m|ai) = -1.3939738214544362 ==> log prob of sentence so far: -3.280464546626918
SPANISH: P(m|ai) = -1.1813237859893582 ==> log prob of sentence so far: -3.535432225136759

3GRAM: ime
ENGLISH: P(e|im) = -0.5956798946901868 ==> log prob of sentence so far: -4.333684588485841
FRENCH: P(e|im) = -0.48936412370206295 ==> log prob of sentence so far: -3.232750673467002
DUTCH: P(e|im) = -0.6488033948702886 ==> log prob of sentence so far: -5.313445370426415
PORTUGUESE: P(e|im) = -0.5035105834138677 ==> log prob of sentence so far: -3.783975130040786
SPANISH: P(e|im) = -0.7176705030022621 ==> log prob of sentence so far: -4.253102728139021

3GRAM: mel
ENGLISH: P(l|me) = -1.5995294433538227 ==> log prob of sentence so far: -5.933214031839664
FRENCH: P(l|me) = -1.505591110386176 ==> log prob of sentence so far: -4.738341783853178
DUTCH: P(l|me) = -1.5123285629470293 ==> log prob of sentence so far: -6.825773933373444
PORTUGUESE: P(l|me) = -1.2285756137351194 ==> log prob of sentence so far: -5.012550743775906
SPANISH: P(l|me) = -0.7104187813998611 ==> log prob of sentence so far: -4.963521509538882

3GRAM: eli
ENGLISH: P(i|el) = -0.8568119156294655 ==> log prob of sentence so far: -6.790025947469129
FRENCH: P(i|el) = -1.261706615108716 ==> log prob of sentence so far: -6.000048398961894
DUTCH: P(i|el) = -0.6939052882552424 ==> log prob of sentence so far: -7.519679221628687
PORTUGUESE: P(i|el) = -1.1081379927708876 ==> log prob of sentence so far: -6.120688736546794
SPANISH: P(i|el) = -1.4510377648617152 ==> log prob of sentence so far: -6.414559274400597

3GRAM: lia
ENGLISH: P(a|li) = -1.4732493614788529 ==> log prob of sentence so far: -8.263275308947982
FRENCH: P(a|li) = -1.6085629179019776 ==> log prob of sentence so far: -7.608611316863872
DUTCH: P(a|li) = -2.621349410930967 ==> log prob of sentence so far: -10.141028632559653
PORTUGUESE: P(a|li) = -0.8675343075348357 ==> log prob of sentence so far: -6.9882230440816295
SPANISH: P(a|li) = -1.0219995502040335 ==> log prob of sentence so far: -7.436558824604631

According to the 3gram model, the sentence is in Portuguese
----------------
4GRAM MODEL:

4GRAM: jaim
ENGLISH: P(m|jai) = -1.462397997898956 ==> log prob of sentence so far: -1.462397997898956
FRENCH: P(m|jai) = -1.173186268412274 ==> log prob of sentence so far: -1.173186268412274
DUTCH: P(m|jai) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874
PORTUGUESE: P(m|jai) = -1.462397997898956 ==> log prob of sentence so far: -1.462397997898956
SPANISH: P(m|jai) = -1.130333768495006 ==> log prob of sentence so far: -1.130333768495006

4GRAM: aime
ENGLISH: P(e|aim) = -0.43270211493124955 ==> log prob of sentence so far: -1.8951001128302056
FRENCH: P(e|aim) = -0.5228787452803376 ==> log prob of sentence so far: -1.6960650136926116
DUTCH: P(e|aim) = -1.4313637641589874 ==> log prob of sentence so far: -2.8627275283179747
PORTUGUESE: P(e|aim) = -1.5378190950732742 ==> log prob of sentence so far: -3.00021709297223
SPANISH: P(e|aim) = -1.7634279935629373 ==> log prob of sentence so far: -2.8937617620579434

4GRAM: imel
ENGLISH: P(l|ime) = -1.5490032620257879 ==> log prob of sentence so far: -3.4441033748559935
FRENCH: P(l|ime) = -1.853871964321762 ==> log prob of sentence so far: -3.5499369780143737
DUTCH: P(l|ime) = -0.9294189257142927 ==> log prob of sentence so far: -3.7921464540322676
PORTUGUESE: P(l|ime) = -2.0086001717619175 ==> log prob of sentence so far: -5.008817264734148
SPANISH: P(l|ime) = -1.5314789170422551 ==> log prob of sentence so far: -4.425240679100199

4GRAM: meli
ENGLISH: P(i|mel) = -0.9413544870872261 ==> log prob of sentence so far: -4.38545786194322
FRENCH: P(i|mel) = -1.587871250860149 ==> log prob of sentence so far: -5.137808228874523
DUTCH: P(i|mel) = -0.6515453544078806 ==> log prob of sentence so far: -4.443691808440148
PORTUGUESE: P(i|mel) = -1.3251636753807006 ==> log prob of sentence so far: -6.333980940114849
SPANISH: P(i|mel) = -2.0856472882968564 ==> log prob of sentence so far: -6.510887967397055

4GRAM: elia
ENGLISH: P(a|eli) = -1.738100733954366 ==> log prob of sentence so far: -6.1235585958975856
FRENCH: P(a|eli) = -1.676185365585699 ==> log prob of sentence so far: -6.8139935944602215
DUTCH: P(a|eli) = -3.0253058652647704 ==> log prob of sentence so far: -7.468997673704918
PORTUGUESE: P(a|eli) = -1.6834973176798114 ==> log prob of sentence so far: -8.01747825779466
SPANISH: P(a|eli) = -1.4471580313422192 ==> log prob of sentence so far: -7.958045998739275

According to the 4gram model, the sentence is in English
----------------
