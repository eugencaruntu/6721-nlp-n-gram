Etienne loves Veronique.

1GRAM MODEL:

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -0.9100752696098996
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -0.766972127152447
DUTCH: P(e) = -0.7296633556789381 ==> log prob of sentence so far: -0.7296633556789381
PORTUGUESE: P(e) = -0.8804727355199955 ==> log prob of sentence so far: -0.8804727355199955
SPANISH: P(e) = -0.8816658603059454 ==> log prob of sentence so far: -0.8816658603059454

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -1.9443023490061595
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -1.9329424291711248
DUTCH: P(t) = -1.2498883332454858 ==> log prob of sentence so far: -1.979551688924424
PORTUGUESE: P(t) = -1.3798350862511235 ==> log prob of sentence so far: -2.2603078217711188
SPANISH: P(t) = -1.403592004029003 ==> log prob of sentence so far: -2.2852578643349486

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -3.1068304277705554
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -3.068152739642425
DUTCH: P(i) = -1.2262538577433346 ==> log prob of sentence so far: -3.205805546667759
PORTUGUESE: P(i) = -1.2397071887182836 ==> log prob of sentence so far: -3.5000150104894026
SPANISH: P(i) = -1.2231599543062535 ==> log prob of sentence so far: -3.508417818641202

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -4.016905697380455
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -3.835124866794872
DUTCH: P(e) = -0.7296633556789381 ==> log prob of sentence so far: -3.935468902346697
PORTUGUESE: P(e) = -0.8804727355199955 ==> log prob of sentence so far: -4.380487746009398
SPANISH: P(e) = -0.8816658603059454 ==> log prob of sentence so far: -4.390083678947147

1GRAM: n
ENGLISH: P(n) = -1.1615995429330288 ==> log prob of sentence so far: -5.178505240313484
FRENCH: P(n) = -1.1226414664094957 ==> log prob of sentence so far: -4.9577663332043675
DUTCH: P(n) = -0.9654352760501171 ==> log prob of sentence so far: -4.900904178396814
PORTUGUESE: P(n) = -1.3242901353330945 ==> log prob of sentence so far: -5.704777881342492
SPANISH: P(n) = -1.1498464064383778 ==> log prob of sentence so far: -5.5399300853855244

1GRAM: n
ENGLISH: P(n) = -1.1615995429330288 ==> log prob of sentence so far: -6.340104783246513
FRENCH: P(n) = -1.1226414664094957 ==> log prob of sentence so far: -6.080407799613863
DUTCH: P(n) = -0.9654352760501171 ==> log prob of sentence so far: -5.866339454446932
PORTUGUESE: P(n) = -1.3242901353330945 ==> log prob of sentence so far: -7.029068016675587
SPANISH: P(n) = -1.1498464064383778 ==> log prob of sentence so far: -6.689776491823903

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -7.250180052856413
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -6.84737992676631
DUTCH: P(e) = -0.7296633556789381 ==> log prob of sentence so far: -6.59600281012587
PORTUGUESE: P(e) = -0.8804727355199955 ==> log prob of sentence so far: -7.909540752195582
SPANISH: P(e) = -0.8816658603059454 ==> log prob of sentence so far: -7.571442352129848

1GRAM: l
ENGLISH: P(l) = -1.3477697305137912 ==> log prob of sentence so far: -8.597949783370204
FRENCH: P(l) = -1.2682148156867032 ==> log prob of sentence so far: -8.115594742453013
DUTCH: P(l) = -1.4248879424212282 ==> log prob of sentence so far: -8.020890752547098
PORTUGUESE: P(l) = -1.437031928760182 ==> log prob of sentence so far: -9.346572680955765
SPANISH: P(l) = -1.280224525407062 ==> log prob of sentence so far: -8.85166687753691

1GRAM: o
ENGLISH: P(o) = -1.1377865416115225 ==> log prob of sentence so far: -9.735736324981726
FRENCH: P(o) = -1.2743455831346002 ==> log prob of sentence so far: -9.389940325587613
DUTCH: P(o) = -1.1998250084767994 ==> log prob of sentence so far: -9.220715761023897
PORTUGUESE: P(o) = -0.9821621911912899 ==> log prob of sentence so far: -10.328734872147054
SPANISH: P(o) = -0.9966447169789665 ==> log prob of sentence so far: -9.848311594515877

1GRAM: v
ENGLISH: P(v) = -2.043600170252475 ==> log prob of sentence so far: -11.779336495234201
FRENCH: P(v) = -1.8278293527225755 ==> log prob of sentence so far: -11.217769678310189
DUTCH: P(v) = -1.609989959474564 ==> log prob of sentence so far: -10.830705720498461
PORTUGUESE: P(v) = -1.7546018486161579 ==> log prob of sentence so far: -12.083336720763212
SPANISH: P(v) = -1.9460570692896122 ==> log prob of sentence so far: -11.79436866380549

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -12.6894117648441
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -11.984741805462637
DUTCH: P(e) = -0.7296633556789381 ==> log prob of sentence so far: -11.560369076177398
PORTUGUESE: P(e) = -0.8804727355199955 ==> log prob of sentence so far: -12.963809456283208
SPANISH: P(e) = -0.8816658603059454 ==> log prob of sentence so far: -12.676034524111435

1GRAM: s
ENGLISH: P(s) = -1.1711435451761196 ==> log prob of sentence so far: -13.86055531002022
FRENCH: P(s) = -1.0644522256524611 ==> log prob of sentence so far: -13.049194031115098
DUTCH: P(s) = -1.460438187134298 ==> log prob of sentence so far: -13.020807263311696
PORTUGUESE: P(s) = -1.112913538600678 ==> log prob of sentence so far: -14.076722994883886
SPANISH: P(s) = -1.1432693326770131 ==> log prob of sentence so far: -13.819303856788448

1GRAM: v
ENGLISH: P(v) = -2.043600170252475 ==> log prob of sentence so far: -15.904155480272696
FRENCH: P(v) = -1.8278293527225755 ==> log prob of sentence so far: -14.877023383837674
DUTCH: P(v) = -1.609989959474564 ==> log prob of sentence so far: -14.63079722278626
PORTUGUESE: P(v) = -1.7546018486161579 ==> log prob of sentence so far: -15.831324843500044
SPANISH: P(v) = -1.9460570692896122 ==> log prob of sentence so far: -15.76536092607806

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -16.814230749882594
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -15.643995510990122
DUTCH: P(e) = -0.7296633556789381 ==> log prob of sentence so far: -15.360460578465197
PORTUGUESE: P(e) = -0.8804727355199955 ==> log prob of sentence so far: -16.71179757902004
SPANISH: P(e) = -0.8816658603059454 ==> log prob of sentence so far: -16.647026786384004

1GRAM: r
ENGLISH: P(r) = -1.2612746582838763 ==> log prob of sentence so far: -18.07550540816647
FRENCH: P(r) = -1.1840319980580551 ==> log prob of sentence so far: -16.828027509048177
DUTCH: P(r) = -1.2542070912544077 ==> log prob of sentence so far: -16.614667669719605
PORTUGUESE: P(r) = -1.194556967793861 ==> log prob of sentence so far: -17.9063545468139
SPANISH: P(r) = -1.1910516980907977 ==> log prob of sentence so far: -17.838078484474803

1GRAM: o
ENGLISH: P(o) = -1.1377865416115225 ==> log prob of sentence so far: -19.213291949777993
FRENCH: P(o) = -1.2743455831346002 ==> log prob of sentence so far: -18.102373092182777
DUTCH: P(o) = -1.1998250084767994 ==> log prob of sentence so far: -17.814492678196405
PORTUGUESE: P(o) = -0.9821621911912899 ==> log prob of sentence so far: -18.88851673800519
SPANISH: P(o) = -0.9966447169789665 ==> log prob of sentence so far: -18.83472320145377

1GRAM: n
ENGLISH: P(n) = -1.1615995429330288 ==> log prob of sentence so far: -20.374891492711022
FRENCH: P(n) = -1.1226414664094957 ==> log prob of sentence so far: -19.22501455859227
DUTCH: P(n) = -0.9654352760501171 ==> log prob of sentence so far: -18.77992795424652
PORTUGUESE: P(n) = -1.3242901353330945 ==> log prob of sentence so far: -20.212806873338284
SPANISH: P(n) = -1.1498464064383778 ==> log prob of sentence so far: -19.984569607892148

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -21.537419571475418
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -20.360224869063572
DUTCH: P(i) = -1.2262538577433346 ==> log prob of sentence so far: -20.006181811989855
PORTUGUESE: P(i) = -1.2397071887182836 ==> log prob of sentence so far: -21.45251406205657
SPANISH: P(i) = -1.2231599543062535 ==> log prob of sentence so far: -21.207729562198402

1GRAM: q
ENGLISH: P(q) = -2.7878655815890627 ==> log prob of sentence so far: -24.32528515306448
FRENCH: P(q) = -1.939419027441695 ==> log prob of sentence so far: -22.299643896505266
DUTCH: P(q) = -4.059896699244934 ==> log prob of sentence so far: -24.06607851123479
PORTUGUESE: P(q) = -1.868337923883273 ==> log prob of sentence so far: -23.320851985939843
SPANISH: P(q) = -1.8372732950920128 ==> log prob of sentence so far: -23.045002857290413

1GRAM: u
ENGLISH: P(u) = -1.5524859731545257 ==> log prob of sentence so far: -25.877771126219006
FRENCH: P(u) = -1.2061381702765537 ==> log prob of sentence so far: -23.50578206678182
DUTCH: P(u) = -1.7453273049444786 ==> log prob of sentence so far: -25.811405816179267
PORTUGUESE: P(u) = -1.3141991217573536 ==> log prob of sentence so far: -24.635051107697198
SPANISH: P(u) = -1.3656733863951074 ==> log prob of sentence so far: -24.410676243685522

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -26.787846395828904
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -24.272754193934265
DUTCH: P(e) = -0.7296633556789381 ==> log prob of sentence so far: -26.541069171858204
PORTUGUESE: P(e) = -0.8804727355199955 ==> log prob of sentence so far: -25.515523843217192
SPANISH: P(e) = -0.8816658603059454 ==> log prob of sentence so far: -25.292342103991466

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: et
ENGLISH: P(t|e) = -1.180595696581799 ==> log prob of sentence so far: -1.180595696581799
FRENCH: P(t|e) = -1.081755762820215 ==> log prob of sentence so far: -1.081755762820215
DUTCH: P(t|e) = -1.1837836199830594 ==> log prob of sentence so far: -1.1837836199830594
PORTUGUESE: P(t|e) = -1.5516515176662755 ==> log prob of sentence so far: -1.5516515176662755
SPANISH: P(t|e) = -1.594197493340078 ==> log prob of sentence so far: -1.594197493340078

2GRAM: ti
ENGLISH: P(i|t) = -1.085632857151972 ==> log prob of sentence so far: -2.2662285537337707
FRENCH: P(i|t) = -0.9860938950046418 ==> log prob of sentence so far: -2.0678496578248566
DUTCH: P(i|t) = -1.222489516857662 ==> log prob of sentence so far: -2.4062731368407215
PORTUGUESE: P(i|t) = -0.9916304296343493 ==> log prob of sentence so far: -2.543281947300625
SPANISH: P(i|t) = -0.9485544762657262 ==> log prob of sentence so far: -2.542751969605804

2GRAM: ie
ENGLISH: P(e|i) = -1.510686361683256 ==> log prob of sentence so far: -3.7769149154170267
FRENCH: P(e|i) = -0.9021992667515383 ==> log prob of sentence so far: -2.970048924576395
DUTCH: P(e|i) = -0.7080358296433861 ==> log prob of sentence so far: -3.1143089664841077
PORTUGUESE: P(e|i) = -1.8483230138575444 ==> log prob of sentence so far: -4.391604961158169
SPANISH: P(e|i) = -0.8668372000562372 ==> log prob of sentence so far: -3.4095891696620413

2GRAM: en
ENGLISH: P(n|e) = -1.0655456684199553 ==> log prob of sentence so far: -4.842460583836982
FRENCH: P(n|e) = -0.8688568329466715 ==> log prob of sentence so far: -3.838905757523066
DUTCH: P(n|e) = -0.5270427591643824 ==> log prob of sentence so far: -3.64135172564849
PORTUGUESE: P(n|e) = -0.9800905850482625 ==> log prob of sentence so far: -5.371695546206432
SPANISH: P(n|e) = -0.7297262997249456 ==> log prob of sentence so far: -4.139315469386987

2GRAM: nn
ENGLISH: P(n|n) = -1.9286855188961969 ==> log prob of sentence so far: -6.771146102733179
FRENCH: P(n|n) = -1.4789625522556082 ==> log prob of sentence so far: -5.3178683097786745
DUTCH: P(n|n) = -1.518307128053744 ==> log prob of sentence so far: -5.159658853702234
PORTUGUESE: P(n|n) = -2.1503524073798674 ==> log prob of sentence so far: -7.522047953586299
SPANISH: P(n|n) = -2.2596076274944323 ==> log prob of sentence so far: -6.398923096881419

2GRAM: ne
ENGLISH: P(e|n) = -1.053319594333179 ==> log prob of sentence so far: -7.824465697066358
FRENCH: P(e|n) = -0.7543924355435184 ==> log prob of sentence so far: -6.072260745322193
DUTCH: P(e|n) = -1.0855682745547242 ==> log prob of sentence so far: -6.245227128256958
PORTUGUESE: P(e|n) = -1.2169184039287388 ==> log prob of sentence so far: -8.738966357515038
SPANISH: P(e|n) = -1.05941083756878 ==> log prob of sentence so far: -7.4583339344501995

2GRAM: el
ENGLISH: P(l|e) = -1.3277895145531675 ==> log prob of sentence so far: -9.152255211619526
FRENCH: P(l|e) = -1.2044529937000865 ==> log prob of sentence so far: -7.27671373902228
DUTCH: P(l|e) = -1.1637554586225491 ==> log prob of sentence so far: -7.408982586879507
PORTUGUESE: P(l|e) = -1.128840327441183 ==> log prob of sentence so far: -9.867806684956221
SPANISH: P(l|e) = -0.9034803640806133 ==> log prob of sentence so far: -8.361814298530813

2GRAM: lo
ENGLISH: P(o|l) = -1.031464770073541 ==> log prob of sentence so far: -10.183719981693066
FRENCH: P(o|l) = -1.16404257293882 ==> log prob of sentence so far: -8.440756311961099
DUTCH: P(o|l) = -1.1568843966678894 ==> log prob of sentence so far: -8.565866983547396
PORTUGUESE: P(o|l) = -1.1092579066679498 ==> log prob of sentence so far: -10.97706459162417
SPANISH: P(o|l) = -0.7254198915857156 ==> log prob of sentence so far: -9.08723419011653

2GRAM: ov
ENGLISH: P(v|o) = -1.7956412075341 ==> log prob of sentence so far: -11.979361189227166
FRENCH: P(v|o) = -2.5248444078586534 ==> log prob of sentence so far: -10.965600719819752
DUTCH: P(v|o) = -1.6000799092271532 ==> log prob of sentence so far: -10.16594689277455
PORTUGUESE: P(v|o) = -1.8172027197269662 ==> log prob of sentence so far: -12.794267311351136
SPANISH: P(v|o) = -1.8965064896095694 ==> log prob of sentence so far: -10.983740679726099

2GRAM: ve
ENGLISH: P(e|v) = -0.17489320455273447 ==> log prob of sentence so far: -12.154254393779901
FRENCH: P(e|v) = -0.4819874687745128 ==> log prob of sentence so far: -11.447588188594265
DUTCH: P(e|v) = -0.4373936381421902 ==> log prob of sentence so far: -10.60334053091674
PORTUGUESE: P(e|v) = -0.4561183081442821 ==> log prob of sentence so far: -13.250385619495418
SPANISH: P(e|v) = -0.5282607526957835 ==> log prob of sentence so far: -11.512001432421883

2GRAM: es
ENGLISH: P(s|e) = -0.9449004528705965 ==> log prob of sentence so far: -13.099154846650498
FRENCH: P(s|e) = -0.7261882355074406 ==> log prob of sentence so far: -12.173776424101705
DUTCH: P(s|e) = -1.4885082638644267 ==> log prob of sentence so far: -12.091848794781166
PORTUGUESE: P(s|e) = -0.8222829642120955 ==> log prob of sentence so far: -14.072668583707515
SPANISH: P(s|e) = -0.7774828478170737 ==> log prob of sentence so far: -12.289484280238957

2GRAM: sv
ENGLISH: P(v|s) = -2.538217540025347 ==> log prob of sentence so far: -15.637372386675844
FRENCH: P(v|s) = -1.9562509477828147 ==> log prob of sentence so far: -14.13002737188452
DUTCH: P(v|s) = -1.4862172573925077 ==> log prob of sentence so far: -13.578066052173673
PORTUGUESE: P(v|s) = -1.9520483639804518 ==> log prob of sentence so far: -16.024716947687967
SPANISH: P(v|s) = -1.9799428481587025 ==> log prob of sentence so far: -14.269427128397659

2GRAM: ve
ENGLISH: P(e|v) = -0.17489320455273447 ==> log prob of sentence so far: -15.812265591228579
FRENCH: P(e|v) = -0.4819874687745128 ==> log prob of sentence so far: -14.612014840659032
DUTCH: P(e|v) = -0.4373936381421902 ==> log prob of sentence so far: -14.015459690315863
PORTUGUESE: P(e|v) = -0.4561183081442821 ==> log prob of sentence so far: -16.48083525583225
SPANISH: P(e|v) = -0.5282607526957835 ==> log prob of sentence so far: -14.797687881093443

2GRAM: er
ENGLISH: P(r|e) = -0.8561972787159292 ==> log prob of sentence so far: -16.66846286994451
FRENCH: P(r|e) = -1.050493728299846 ==> log prob of sentence so far: -15.662508568958879
DUTCH: P(r|e) = -0.8727952844334766 ==> log prob of sentence so far: -14.888254974749339
PORTUGUESE: P(r|e) = -0.9763725325306456 ==> log prob of sentence so far: -17.457207788362897
SPANISH: P(r|e) = -0.9100010871348838 ==> log prob of sentence so far: -15.707688968228327

2GRAM: ro
ENGLISH: P(o|r) = -0.9680163071420329 ==> log prob of sentence so far: -17.636479177086542
FRENCH: P(o|r) = -1.1139722753851267 ==> log prob of sentence so far: -16.776480844344007
DUTCH: P(o|r) = -1.030247280574744 ==> log prob of sentence so far: -15.918502255324082
PORTUGUESE: P(o|r) = -0.9764168081557388 ==> log prob of sentence so far: -18.433624596518637
SPANISH: P(o|r) = -0.8583085238564643 ==> log prob of sentence so far: -16.56599749208479

2GRAM: on
ENGLISH: P(n|o) = -0.862267433922097 ==> log prob of sentence so far: -18.498746611008638
FRENCH: P(n|o) = -0.5206794238803306 ==> log prob of sentence so far: -17.29716026822434
DUTCH: P(n|o) = -0.862503109381795 ==> log prob of sentence so far: -16.781005364705877
PORTUGUESE: P(n|o) = -1.1340552855154322 ==> log prob of sentence so far: -19.56767988203407
SPANISH: P(n|o) = -0.8458508345819566 ==> log prob of sentence so far: -17.411848326666746

2GRAM: ni
ENGLISH: P(i|n) = -1.3332473151699546 ==> log prob of sentence so far: -19.831993926178594
FRENCH: P(i|n) = -1.4845337532041178 ==> log prob of sentence so far: -18.781694021428457
DUTCH: P(i|n) = -1.2580221000798222 ==> log prob of sentence so far: -18.039027464785697
PORTUGUESE: P(i|n) = -1.526016021340753 ==> log prob of sentence so far: -21.093695903374822
SPANISH: P(i|n) = -1.4039272694368718 ==> log prob of sentence so far: -18.815775596103617

2GRAM: iq
ENGLISH: P(q|i) = -3.1745157322304145 ==> log prob of sentence so far: -23.00650965840901
FRENCH: P(q|i) = -1.7046308931822618 ==> log prob of sentence so far: -20.48632491461072
DUTCH: P(q|i) = -3.009698710060427 ==> log prob of sentence so far: -21.048726174846124
PORTUGUESE: P(q|i) = -2.126369102600836 ==> log prob of sentence so far: -23.22006500597566
SPANISH: P(q|i) = -2.2383448542459647 ==> log prob of sentence so far: -21.054120450349583

2GRAM: qu
ENGLISH: P(u|q) = -0.00717858462712341 ==> log prob of sentence so far: -23.013688243036132
FRENCH: P(u|q) = -0.009003519905163586 ==> log prob of sentence so far: -20.495328434515883
DUTCH: P(u|q) = -0.22902733424347976 ==> log prob of sentence so far: -21.277753509089603
PORTUGUESE: P(u|q) = -0.0035302948571258696 ==> log prob of sentence so far: -23.223595300832784
SPANISH: P(u|q) = -0.002113754926962629 ==> log prob of sentence so far: -21.056234205276546

2GRAM: ue
ENGLISH: P(e|u) = -1.3247075359478098 ==> log prob of sentence so far: -24.33839577898394
FRENCH: P(e|u) = -0.8813439489240259 ==> log prob of sentence so far: -21.37667238343991
DUTCH: P(e|u) = -1.9455389219174464 ==> log prob of sentence so far: -23.22329243100705
PORTUGUESE: P(e|u) = -0.5680414530136387 ==> log prob of sentence so far: -23.791636753846422
SPANISH: P(e|u) = -0.3960791868120396 ==> log prob of sentence so far: -21.452313392088584

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: eti
ENGLISH: P(i|et) = -1.1133004291962256 ==> log prob of sentence so far: -1.1133004291962256
FRENCH: P(i|et) = -1.2503486295226196 ==> log prob of sentence so far: -1.2503486295226196
DUTCH: P(i|et) = -1.505614514124067 ==> log prob of sentence so far: -1.505614514124067
PORTUGUESE: P(i|et) = -1.0031281850629425 ==> log prob of sentence so far: -1.0031281850629425
SPANISH: P(i|et) = -0.8411806380529443 ==> log prob of sentence so far: -0.8411806380529443

3GRAM: tie
ENGLISH: P(e|ti) = -1.5550822288717328 ==> log prob of sentence so far: -2.6683826580679586
FRENCH: P(e|ti) = -1.12926604950139 ==> log prob of sentence so far: -2.3796146790240096
DUTCH: P(e|ti) = -0.8693592845672299 ==> log prob of sentence so far: -2.374973798691297
PORTUGUESE: P(e|ti) = -2.5176356898679657 ==> log prob of sentence so far: -3.5207638749309083
SPANISH: P(e|ti) = -0.6981820492086447 ==> log prob of sentence so far: -1.539362687261589

3GRAM: ien
ENGLISH: P(n|ie) = -0.8558484523348504 ==> log prob of sentence so far: -3.524231110402809
FRENCH: P(n|ie) = -0.40817959365605777 ==> log prob of sentence so far: -2.7877942726800673
DUTCH: P(n|ie) = -0.7649801688304502 ==> log prob of sentence so far: -3.139953967521747
PORTUGUESE: P(n|ie) = -0.6585413472804106 ==> log prob of sentence so far: -4.179305222211319
SPANISH: P(n|ie) = -0.30044626643375927 ==> log prob of sentence so far: -1.8398089536953484

3GRAM: enn
ENGLISH: P(n|en) = -2.0791812460476247 ==> log prob of sentence so far: -5.603412356450434
FRENCH: P(n|en) = -1.8001637007271192 ==> log prob of sentence so far: -4.587957973407186
DUTCH: P(n|en) = -1.5056169845041054 ==> log prob of sentence so far: -4.645570952025852
PORTUGUESE: P(n|en) = -2.639059384351124 ==> log prob of sentence so far: -6.818364606562443
SPANISH: P(n|en) = -2.223735105323737 ==> log prob of sentence so far: -4.063544059019085

3GRAM: nne
ENGLISH: P(e|nn) = -0.5570837065216335 ==> log prob of sentence so far: -6.160496062972068
FRENCH: P(e|nn) = -0.2542815303849093 ==> log prob of sentence so far: -4.842239503792095
DUTCH: P(e|nn) = -0.4463532853147284 ==> log prob of sentence so far: -5.091924237340581
PORTUGUESE: P(e|nn) = -1.4022613824546801 ==> log prob of sentence so far: -8.220625989017122
SPANISH: P(e|nn) = -1.100900495740861 ==> log prob of sentence so far: -5.164444554759946

3GRAM: nel
ENGLISH: P(l|ne) = -1.641505338984871 ==> log prob of sentence so far: -7.802001401956939
FRENCH: P(l|ne) = -1.4206656152716368 ==> log prob of sentence so far: -6.262905119063732
DUTCH: P(l|ne) = -1.60294283216438 ==> log prob of sentence so far: -6.69486706950496
PORTUGUESE: P(l|ne) = -1.0381969791868906 ==> log prob of sentence so far: -9.258822968204013
SPANISH: P(l|ne) = -0.693519352111165 ==> log prob of sentence so far: -5.857963906871111

3GRAM: elo
ENGLISH: P(o|el) = -1.0291152752851027 ==> log prob of sentence so far: -8.831116677242042
FRENCH: P(o|el) = -1.1626412900745247 ==> log prob of sentence so far: -7.425546409138256
DUTCH: P(o|el) = -1.2663658689837305 ==> log prob of sentence so far: -7.961232938488691
PORTUGUESE: P(o|el) = -1.1375476220875167 ==> log prob of sentence so far: -10.39637059029153
SPANISH: P(o|el) = -0.8754090235719554 ==> log prob of sentence so far: -6.733372930443067

3GRAM: lov
ENGLISH: P(v|lo) = -1.58953347405132 ==> log prob of sentence so far: -10.420650151293362
FRENCH: P(v|lo) = -2.933824603968112 ==> log prob of sentence so far: -10.359371013106369
DUTCH: P(v|lo) = -1.6969914383539308 ==> log prob of sentence so far: -9.658224376842622
PORTUGUESE: P(v|lo) = -2.193820026016113 ==> log prob of sentence so far: -12.590190616307643
SPANISH: P(v|lo) = -1.9707488379473665 ==> log prob of sentence so far: -8.704121768390433

3GRAM: ove
ENGLISH: P(e|ov) = -0.05958568998424408 ==> log prob of sentence so far: -10.480235841277606
FRENCH: P(e|ov) = -0.6251837901751002 ==> log prob of sentence so far: -10.984554803281469
DUTCH: P(e|ov) = -0.07144729085728964 ==> log prob of sentence so far: -9.729671667699911
PORTUGUESE: P(e|ov) = -0.4462097897386464 ==> log prob of sentence so far: -13.036400406046289
SPANISH: P(e|ov) = -0.5726503620113332 ==> log prob of sentence so far: -9.276772130401767

3GRAM: ves
ENGLISH: P(s|ve) = -0.9859638577354091 ==> log prob of sentence so far: -11.466199699013016
FRENCH: P(s|ve) = -1.268191199485949 ==> log prob of sentence so far: -12.252746002767418
DUTCH: P(s|ve) = -2.095169351431755 ==> log prob of sentence so far: -11.824841019131666
PORTUGUESE: P(s|ve) = -0.7593581872919104 ==> log prob of sentence so far: -13.7957585933382
SPANISH: P(s|ve) = -1.1471724317292227 ==> log prob of sentence so far: -10.42394456213099

3GRAM: esv
ENGLISH: P(v|es) = -3.056211643716186 ==> log prob of sentence so far: -14.522411342729201
FRENCH: P(v|es) = -1.849721445439623 ==> log prob of sentence so far: -14.102467448207042
DUTCH: P(v|es) = -1.9111576087399766 ==> log prob of sentence so far: -13.735998627871643
PORTUGUESE: P(v|es) = -1.931224726577555 ==> log prob of sentence so far: -15.726983319915755
SPANISH: P(v|es) = -1.9965577535889827 ==> log prob of sentence so far: -12.420502315719972

3GRAM: sve
ENGLISH: P(e|sv) = -0.472904770318499 ==> log prob of sentence so far: -14.9953161130477
FRENCH: P(e|sv) = -0.5541268645119002 ==> log prob of sentence so far: -14.656594312718942
DUTCH: P(e|sv) = -0.6029714176475832 ==> log prob of sentence so far: -14.338970045519225
PORTUGUESE: P(e|sv) = -0.37358066281259295 ==> log prob of sentence so far: -16.10056398272835
SPANISH: P(e|sv) = -0.49090953920529573 ==> log prob of sentence so far: -12.911411854925268

3GRAM: ver
ENGLISH: P(r|ve) = -0.38649667698572626 ==> log prob of sentence so far: -15.381812790033425
FRENCH: P(r|ve) = -0.5189087009758648 ==> log prob of sentence so far: -15.175503013694808
DUTCH: P(r|ve) = -0.21664755593054863 ==> log prob of sentence so far: -14.555617601449773
PORTUGUESE: P(r|ve) = -0.6256099299266036 ==> log prob of sentence so far: -16.726173912654954
SPANISH: P(r|ve) = -0.4869680291489155 ==> log prob of sentence so far: -13.398379884074183

3GRAM: ero
ENGLISH: P(o|er) = -1.3327521843949144 ==> log prob of sentence so far: -16.714564974428338
FRENCH: P(o|er) = -1.5187669515261173 ==> log prob of sentence so far: -16.694269965220926
DUTCH: P(o|er) = -1.4498452604256298 ==> log prob of sentence so far: -16.005462861875404
PORTUGUESE: P(o|er) = -1.352979987545175 ==> log prob of sentence so far: -18.07915390020013
SPANISH: P(o|er) = -0.6388097188226879 ==> log prob of sentence so far: -14.037189602896872

3GRAM: ron
ENGLISH: P(n|ro) = -1.1328496016385499 ==> log prob of sentence so far: -17.84741457606689
FRENCH: P(n|ro) = -0.8441022368106383 ==> log prob of sentence so far: -17.538372202031564
DUTCH: P(n|ro) = -0.9522016481558949 ==> log prob of sentence so far: -16.9576645100313
PORTUGUESE: P(n|ro) = -0.7993101802719391 ==> log prob of sentence so far: -18.878464080472067
SPANISH: P(n|ro) = -0.6130662328557583 ==> log prob of sentence so far: -14.65025583575263

3GRAM: oni
ENGLISH: P(i|on) = -1.4217641605307234 ==> log prob of sentence so far: -19.269178736597613
FRENCH: P(i|on) = -1.7762984049599486 ==> log prob of sentence so far: -19.314670606991513
DUTCH: P(i|on) = -1.1157817682674167 ==> log prob of sentence so far: -18.073446278298718
PORTUGUESE: P(i|on) = -1.5061927026669208 ==> log prob of sentence so far: -20.38465678313899
SPANISH: P(i|on) = -1.4165055704156782 ==> log prob of sentence so far: -16.066761406168308

3GRAM: niq
ENGLISH: P(q|ni) = -3.0011565771999424 ==> log prob of sentence so far: -22.270335313797556
FRENCH: P(q|ni) = -1.3184656457668007 ==> log prob of sentence so far: -20.633136252758312
DUTCH: P(q|ni) = -3.0687422929329817 ==> log prob of sentence so far: -21.1421885712317
PORTUGUESE: P(q|ni) = -2.2367890994092927 ==> log prob of sentence so far: -22.621445882548283
SPANISH: P(q|ni) = -1.8590524768255423 ==> log prob of sentence so far: -17.92581388299385

3GRAM: iqu
ENGLISH: P(u|iq) = -0.19539641425106793 ==> log prob of sentence so far: -22.465731728048624
FRENCH: P(u|iq) = -0.010723865391773113 ==> log prob of sentence so far: -20.643860118150087
DUTCH: P(u|iq) = -0.30998483831690765 ==> log prob of sentence so far: -21.452173409548607
PORTUGUESE: P(u|iq) = -0.09958809640850338 ==> log prob of sentence so far: -22.721033978956786
SPANISH: P(u|iq) = -0.07860640217560651 ==> log prob of sentence so far: -18.004420285169456

3GRAM: que
ENGLISH: P(e|qu) = -0.3051596098085178 ==> log prob of sentence so far: -22.77089133785714
FRENCH: P(e|qu) = -0.2543114195895424 ==> log prob of sentence so far: -20.89817153773963
DUTCH: P(e|qu) = -0.405118593299161 ==> log prob of sentence so far: -21.857292002847768
PORTUGUESE: P(e|qu) = -0.07587477489434706 ==> log prob of sentence so far: -22.796908753851135
SPANISH: P(e|qu) = -0.0905886448664081 ==> log prob of sentence so far: -18.095008930035863

According to the 3gram model, the sentence is in Spanish
----------------
4GRAM MODEL:

4GRAM: etie
ENGLISH: P(e|eti) = -1.5689347107083378 ==> log prob of sentence so far: -1.5689347107083378
FRENCH: P(e|eti) = -1.225917111750769 ==> log prob of sentence so far: -1.225917111750769
DUTCH: P(e|eti) = -1.153814864344529 ==> log prob of sentence so far: -1.153814864344529
PORTUGUESE: P(e|eti) = -1.7283537820212285 ==> log prob of sentence so far: -1.7283537820212285
SPANISH: P(e|eti) = -0.5745772068089177 ==> log prob of sentence so far: -0.5745772068089177

4GRAM: tien
ENGLISH: P(n|tie) = -0.9680936213365546 ==> log prob of sentence so far: -2.5370283320448923
FRENCH: P(n|tie) = -0.6124420271216842 ==> log prob of sentence so far: -1.8383591388724532
DUTCH: P(n|tie) = -0.6179367534268005 ==> log prob of sentence so far: -1.7717516177713295
PORTUGUESE: P(n|tie) = -1.4313637641589874 ==> log prob of sentence so far: -3.159717546180216
SPANISH: P(n|tie) = -0.28899890824244573 ==> log prob of sentence so far: -0.8635761150513634

4GRAM: ienn
ENGLISH: P(n|ien) = -2.4899584794248346 ==> log prob of sentence so far: -5.026986811469727
FRENCH: P(n|ien) = -1.1802078252505135 ==> log prob of sentence so far: -3.0185669641229667
DUTCH: P(n|ien) = -1.6116674178480854 ==> log prob of sentence so far: -3.383419035619415
PORTUGUESE: P(n|ien) = -1.845098040014257 ==> log prob of sentence so far: -5.004815586194473
SPANISH: P(n|ien) = -2.0170333392987803 ==> log prob of sentence so far: -2.880609454350144

4GRAM: enne
ENGLISH: P(e|enn) = -0.7160033436347991 ==> log prob of sentence so far: -5.742990155104526
FRENCH: P(e|enn) = -0.1398790864012365 ==> log prob of sentence so far: -3.158446050524203
DUTCH: P(e|enn) = -0.9343672642672181 ==> log prob of sentence so far: -4.317786299886633
PORTUGUESE: P(e|enn) = -1.2041199826559248 ==> log prob of sentence so far: -6.208935568850398
SPANISH: P(e|enn) = -0.9777236052888478 ==> log prob of sentence so far: -3.8583330596389915

4GRAM: nnel
ENGLISH: P(l|nne) = -1.4277745331355436 ==> log prob of sentence so far: -7.1707646882400695
FRENCH: P(l|nne) = -1.1361037340663431 ==> log prob of sentence so far: -4.294549784590546
DUTCH: P(l|nne) = -1.9084850188786497 ==> log prob of sentence so far: -6.226271318765282
PORTUGUESE: P(l|nne) = -1.1613680022349748 ==> log prob of sentence so far: -7.370303571085373
SPANISH: P(l|nne) = -1.5797835966168101 ==> log prob of sentence so far: -5.438116656255802

4GRAM: nelo
ENGLISH: P(o|nel) = -0.9237044421898634 ==> log prob of sentence so far: -8.094469130429934
FRENCH: P(o|nel) = -1.1631613749770184 ==> log prob of sentence so far: -5.457711159567564
DUTCH: P(o|nel) = -1.1923284579263669 ==> log prob of sentence so far: -7.418599776691649
PORTUGUESE: P(o|nel) = -1.9344984512435677 ==> log prob of sentence so far: -9.304802022328941
SPANISH: P(o|nel) = -1.4403186068119138 ==> log prob of sentence so far: -6.878435263067716

4GRAM: elov
ENGLISH: P(v|elo) = -1.4640587818804205 ==> log prob of sentence so far: -9.558527912310353
FRENCH: P(v|elo) = -2.724275869600789 ==> log prob of sentence so far: -8.181987029168354
DUTCH: P(v|elo) = -1.275886960301226 ==> log prob of sentence so far: -8.694486736992875
PORTUGUESE: P(v|elo) = -1.9138138523837167 ==> log prob of sentence so far: -11.218615874712658
SPANISH: P(v|elo) = -1.7953005716518415 ==> log prob of sentence so far: -8.673735834719558

4GRAM: love
ENGLISH: P(e|lov) = -0.13532077240202173 ==> log prob of sentence so far: -9.693848684712375
FRENCH: P(e|lov) = -1.4471580313422192 ==> log prob of sentence so far: -9.629145060510574
DUTCH: P(e|lov) = -0.32967517711351124 ==> log prob of sentence so far: -9.024161914106386
PORTUGUESE: P(e|lov) = -0.9852767431792937 ==> log prob of sentence so far: -12.20389261789195
SPANISH: P(e|lov) = -0.5812098523548422 ==> log prob of sentence so far: -9.2549456870744

4GRAM: oves
ENGLISH: P(s|ove) = -1.4202164033831899 ==> log prob of sentence so far: -11.114065088095565
FRENCH: P(s|ove) = -1.7558748556724915 ==> log prob of sentence so far: -11.385019916183065
DUTCH: P(s|ove) = -2.4778444763387584 ==> log prob of sentence so far: -11.502006390445144
PORTUGUESE: P(s|ove) = -1.1931245983544616 ==> log prob of sentence so far: -13.397017216246413
SPANISH: P(s|ove) = -1.7018556925735069 ==> log prob of sentence so far: -10.956801379647906

4GRAM: vesv
ENGLISH: P(v|ves) = -2.4668676203541096 ==> log prob of sentence so far: -13.580932708449675
FRENCH: P(v|ves) = -2.303196057420489 ==> log prob of sentence so far: -13.688215973603555
DUTCH: P(v|ves) = -1.7403626894942439 ==> log prob of sentence so far: -13.242369079939388
PORTUGUESE: P(v|ves) = -1.7058637122839193 ==> log prob of sentence so far: -15.102880928530332
SPANISH: P(v|ves) = -1.7520484478194385 ==> log prob of sentence so far: -12.708849827467345

4GRAM: esve
ENGLISH: P(e|esv) = -0.8573324964312684 ==> log prob of sentence so far: -14.438265204880944
FRENCH: P(e|esv) = -0.508300858998864 ==> log prob of sentence so far: -14.196516832602418
DUTCH: P(e|esv) = -1.041392685158225 ==> log prob of sentence so far: -14.283761765097614
PORTUGUESE: P(e|esv) = -0.6872316010647747 ==> log prob of sentence so far: -15.790112529595108
SPANISH: P(e|esv) = -0.5272001190629801 ==> log prob of sentence so far: -13.236049946530326

4GRAM: sver
ENGLISH: P(r|sve) = -0.24420707895314603 ==> log prob of sentence so far: -14.68247228383409
FRENCH: P(r|sve) = -0.2659557841222724 ==> log prob of sentence so far: -14.46247261672469
DUTCH: P(r|sve) = -0.1540411183126071 ==> log prob of sentence so far: -14.437802883410221
PORTUGUESE: P(r|sve) = -0.6913506074658268 ==> log prob of sentence so far: -16.481463137060935
SPANISH: P(r|sve) = -0.7294593264839191 ==> log prob of sentence so far: -13.965509273014245

4GRAM: vero
ENGLISH: P(o|ver) = -1.7657258533252167 ==> log prob of sentence so far: -16.448198137159306
FRENCH: P(o|ver) = -2.02201573981772 ==> log prob of sentence so far: -16.48448835654241
DUTCH: P(o|ver) = -1.7024305364455252 ==> log prob of sentence so far: -16.140233419855747
PORTUGUESE: P(o|ver) = -1.5797835966168101 ==> log prob of sentence so far: -18.061246733677745
SPANISH: P(o|ver) = -1.5399120845791179 ==> log prob of sentence so far: -15.505421357593363

4GRAM: eron
ENGLISH: P(n|ero) = -0.9306055233975602 ==> log prob of sentence so far: -17.378803660556866
FRENCH: P(n|ero) = -0.49695549970463443 ==> log prob of sentence so far: -16.981443856247044
DUTCH: P(n|ero) = -0.6720978579357174 ==> log prob of sentence so far: -16.812331277791465
PORTUGUESE: P(n|ero) = -1.4800069429571505 ==> log prob of sentence so far: -19.541253676634895
SPANISH: P(n|ero) = -0.6252278563799883 ==> log prob of sentence so far: -16.130649213973353

4GRAM: roni
ENGLISH: P(i|ron) = -1.147160841116006 ==> log prob of sentence so far: -18.52596450167287
FRENCH: P(i|ron) = -1.462397997898956 ==> log prob of sentence so far: -18.443841854146
DUTCH: P(i|ron) = -2.416640507338281 ==> log prob of sentence so far: -19.228971785129744
PORTUGUESE: P(i|ron) = -1.6215224710973946 ==> log prob of sentence so far: -21.162776147732288
SPANISH: P(i|ron) = -1.5803846963417019 ==> log prob of sentence so far: -17.711033910315056

4GRAM: oniq
ENGLISH: P(q|oni) = -2.5705429398818973 ==> log prob of sentence so far: -21.096507441554767
FRENCH: P(q|oni) = -0.905172938140236 ==> log prob of sentence so far: -19.349014792286237
DUTCH: P(q|oni) = -2.4727564493172123 ==> log prob of sentence so far: -21.701728234446957
PORTUGUESE: P(q|oni) = -1.8864907251724818 ==> log prob of sentence so far: -23.04926687290477
SPANISH: P(q|oni) = -1.7180862947830917 ==> log prob of sentence so far: -19.429120205098148

4GRAM: niqu
ENGLISH: P(u|niq) = -0.9700367766225568 ==> log prob of sentence so far: -22.066544218177324
FRENCH: P(u|niq) = -0.11434566311087581 ==> log prob of sentence so far: -19.463360455397112
DUTCH: P(u|niq) = -1.130333768495006 ==> log prob of sentence so far: -22.832062002941964
PORTUGUESE: P(u|niq) = -1.130333768495006 ==> log prob of sentence so far: -24.179600641399777
SPANISH: P(u|niq) = -0.4449365713482612 ==> log prob of sentence so far: -19.87405677644641

4GRAM: ique
ENGLISH: P(e|iqu) = -0.47712125471966244 ==> log prob of sentence so far: -22.543665472896986
FRENCH: P(e|iqu) = -0.10438882358668244 ==> log prob of sentence so far: -19.567749278983793
DUTCH: P(e|iqu) = -0.4114424790756847 ==> log prob of sentence so far: -23.24350448201765
PORTUGUESE: P(e|iqu) = -0.14187715852457958 ==> log prob of sentence so far: -24.321477799924356
SPANISH: P(e|iqu) = -0.24955802157887672 ==> log prob of sentence so far: -20.123614798025287

According to the 4gram model, the sentence is in French
----------------
