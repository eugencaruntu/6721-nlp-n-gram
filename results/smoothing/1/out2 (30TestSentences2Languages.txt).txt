She asked him if he was a student at this school.

1GRAM MODEL:

1GRAM: s
ENGLISH: P(s) = -1.1711435451761196 ==> log prob of sentence so far: -1.1711435451761196
FRENCH: P(s) = -1.0644522256524611 ==> log prob of sentence so far: -1.0644522256524611

1GRAM: h
ENGLISH: P(h) = -1.1802766233913087 ==> log prob of sentence so far: -2.3514201685674285
FRENCH: P(h) = -2.1055979007773504 ==> log prob of sentence so far: -3.1700501264298113

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -3.2614954381773282
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -3.937022253582258

1GRAM: a
ENGLISH: P(a) = -1.086791807028239 ==> log prob of sentence so far: -4.348287245205567
FRENCH: P(a) = -1.0763575048838907 ==> log prob of sentence so far: -5.013379758466149

1GRAM: s
ENGLISH: P(s) = -1.1711435451761196 ==> log prob of sentence so far: -5.519430790381687
FRENCH: P(s) = -1.0644522256524611 ==> log prob of sentence so far: -6.07783198411861

1GRAM: k
ENGLISH: P(k) = -2.072676245803911 ==> log prob of sentence so far: -7.592107036185598
FRENCH: P(k) = -3.5355259793911835 ==> log prob of sentence so far: -9.613357963509793

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -8.502182305795497
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -10.380330090662241

1GRAM: d
ENGLISH: P(d) = -1.3961801671840104 ==> log prob of sentence so far: -9.898362472979507
FRENCH: P(d) = -1.4202615782120422 ==> log prob of sentence so far: -11.800591668874283

1GRAM: h
ENGLISH: P(h) = -1.1802766233913087 ==> log prob of sentence so far: -11.078639096370816
FRENCH: P(h) = -2.1055979007773504 ==> log prob of sentence so far: -13.906189569651634

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -12.241167175135212
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -15.041399880122935

1GRAM: m
ENGLISH: P(m) = -1.6111071179746999 ==> log prob of sentence so far: -13.852274293109911
FRENCH: P(m) = -1.5129465442835708 ==> log prob of sentence so far: -16.554346424406507

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -15.014802371874307
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -17.689556734877808

1GRAM: f
ENGLISH: P(f) = -1.6602710678572594 ==> log prob of sentence so far: -16.675073439731566
FRENCH: P(f) = -1.9917037157389805 ==> log prob of sentence so far: -19.68126045061679

1GRAM: h
ENGLISH: P(h) = -1.1802766233913087 ==> log prob of sentence so far: -17.855350063122874
FRENCH: P(h) = -2.1055979007773504 ==> log prob of sentence so far: -21.78685835139414

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -18.76542533273277
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -22.553830478546587

1GRAM: w
ENGLISH: P(w) = -1.6313501899422194 ==> log prob of sentence so far: -20.39677552267499
FRENCH: P(w) = -3.916598062775926 ==> log prob of sentence so far: -26.470428541322512

1GRAM: a
ENGLISH: P(a) = -1.086791807028239 ==> log prob of sentence so far: -21.483567329703227
FRENCH: P(a) = -1.0763575048838907 ==> log prob of sentence so far: -27.546786046206403

1GRAM: s
ENGLISH: P(s) = -1.1711435451761196 ==> log prob of sentence so far: -22.654710874879346
FRENCH: P(s) = -1.0644522256524611 ==> log prob of sentence so far: -28.611238271858863

1GRAM: a
ENGLISH: P(a) = -1.086791807028239 ==> log prob of sentence so far: -23.741502681907583
FRENCH: P(a) = -1.0763575048838907 ==> log prob of sentence so far: -29.687595776742754

1GRAM: s
ENGLISH: P(s) = -1.1711435451761196 ==> log prob of sentence so far: -24.9126462270837
FRENCH: P(s) = -1.0644522256524611 ==> log prob of sentence so far: -30.752048002395213

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -25.94687330647996
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -31.91801830441389

1GRAM: u
ENGLISH: P(u) = -1.5524859731545257 ==> log prob of sentence so far: -27.499359279634486
FRENCH: P(u) = -1.2061381702765537 ==> log prob of sentence so far: -33.124156474690444

1GRAM: d
ENGLISH: P(d) = -1.3961801671840104 ==> log prob of sentence so far: -28.895539446818496
FRENCH: P(d) = -1.4202615782120422 ==> log prob of sentence so far: -34.544418052902486

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -29.805614716428394
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -35.311390180054936

1GRAM: n
ENGLISH: P(n) = -1.1615995429330288 ==> log prob of sentence so far: -30.967214259361423
FRENCH: P(n) = -1.1226414664094957 ==> log prob of sentence so far: -36.43403164646443

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -32.00144133875769
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -37.60000194848311

1GRAM: a
ENGLISH: P(a) = -1.086791807028239 ==> log prob of sentence so far: -33.08823314578593
FRENCH: P(a) = -1.0763575048838907 ==> log prob of sentence so far: -38.676359453367

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -34.12246022518219
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -39.84232975538568

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -35.15668730457845
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -41.008300057404355

1GRAM: h
ENGLISH: P(h) = -1.1802766233913087 ==> log prob of sentence so far: -36.336963927969755
FRENCH: P(h) = -2.1055979007773504 ==> log prob of sentence so far: -43.1138979581817

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -37.49949200673415
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -44.249108268653

1GRAM: s
ENGLISH: P(s) = -1.1711435451761196 ==> log prob of sentence so far: -38.67063555191027
FRENCH: P(s) = -1.0644522256524611 ==> log prob of sentence so far: -45.31356049430546

1GRAM: s
ENGLISH: P(s) = -1.1711435451761196 ==> log prob of sentence so far: -39.841779097086395
FRENCH: P(s) = -1.0644522256524611 ==> log prob of sentence so far: -46.37801271995792

1GRAM: c
ENGLISH: P(c) = -1.6265839306967516 ==> log prob of sentence so far: -41.46836302778315
FRENCH: P(c) = -1.4922220755920426 ==> log prob of sentence so far: -47.87023479554996

1GRAM: h
ENGLISH: P(h) = -1.1802766233913087 ==> log prob of sentence so far: -42.648639651174456
FRENCH: P(h) = -2.1055979007773504 ==> log prob of sentence so far: -49.97583269632731

1GRAM: o
ENGLISH: P(o) = -1.1377865416115225 ==> log prob of sentence so far: -43.78642619278598
FRENCH: P(o) = -1.2743455831346002 ==> log prob of sentence so far: -51.250178279461906

1GRAM: o
ENGLISH: P(o) = -1.1377865416115225 ==> log prob of sentence so far: -44.92421273439751
FRENCH: P(o) = -1.2743455831346002 ==> log prob of sentence so far: -52.5245238625965

1GRAM: l
ENGLISH: P(l) = -1.3477697305137912 ==> log prob of sentence so far: -46.2719824649113
FRENCH: P(l) = -1.2682148156867032 ==> log prob of sentence so far: -53.79273867828321

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: sh
ENGLISH: P(h|s) = -1.1032939651001397 ==> log prob of sentence so far: -1.1032939651001397
FRENCH: P(h|s) = -2.159838380424855 ==> log prob of sentence so far: -2.159838380424855

2GRAM: he
ENGLISH: P(e|h) = -0.37010948259900806 ==> log prob of sentence so far: -1.4734034476991478
FRENCH: P(e|h) = -0.45896620044521513 ==> log prob of sentence so far: -2.61880458087007

2GRAM: ea
ENGLISH: P(a|e) = -1.058588353645204 ==> log prob of sentence so far: -2.531991801344352
FRENCH: P(a|e) = -1.5190172552264145 ==> log prob of sentence so far: -4.137821836096485

2GRAM: as
ENGLISH: P(s|a) = -0.9960003295333243 ==> log prob of sentence so far: -3.527992130877676
FRENCH: P(s|a) = -1.2507893391409073 ==> log prob of sentence so far: -5.388611175237393

2GRAM: sk
ENGLISH: P(k|s) = -2.042210941145311 ==> log prob of sentence so far: -5.570203072022987
FRENCH: P(k|s) = -3.3555251955439167 ==> log prob of sentence so far: -8.744136370781309

2GRAM: ke
ENGLISH: P(e|k) = -0.4471580313422192 ==> log prob of sentence so far: -6.017361103365206
FRENCH: P(e|k) = -0.9680936213365546 ==> log prob of sentence so far: -9.712229992117864

2GRAM: ed
ENGLISH: P(d|e) = -1.0374864289809331 ==> log prob of sentence so far: -7.054847532346139
FRENCH: P(d|e) = -1.2790977167639939 ==> log prob of sentence so far: -10.991327708881858

2GRAM: dh
ENGLISH: P(h|d) = -1.4165737529547737 ==> log prob of sentence so far: -8.471421285300913
FRENCH: P(h|d) = -2.3182621422365024 ==> log prob of sentence so far: -13.30958985111836

2GRAM: hi
ENGLISH: P(i|h) = -0.792994352028281 ==> log prob of sentence so far: -9.264415637329193
FRENCH: P(i|h) = -1.0752208457966166 ==> log prob of sentence so far: -14.384810696914977

2GRAM: im
ENGLISH: P(m|i) = -1.3479991992166425 ==> log prob of sentence so far: -10.612414836545836
FRENCH: P(m|i) = -1.5426642768181869 ==> log prob of sentence so far: -15.927474973733164

2GRAM: mi
ENGLISH: P(i|m) = -1.0706076493715508 ==> log prob of sentence so far: -11.683022485917387
FRENCH: P(i|m) = -1.043744927593803 ==> log prob of sentence so far: -16.97121990132697

2GRAM: if
ENGLISH: P(f|i) = -1.676832318595863 ==> log prob of sentence so far: -13.359854804513251
FRENCH: P(f|i) = -1.8970958651134087 ==> log prob of sentence so far: -18.868315766440375

2GRAM: fh
ENGLISH: P(h|f) = -1.499049669220783 ==> log prob of sentence so far: -14.858904473734034
FRENCH: P(h|f) = -3.0049965543724437 ==> log prob of sentence so far: -21.87331232081282

2GRAM: he
ENGLISH: P(e|h) = -0.37010948259900806 ==> log prob of sentence so far: -15.229013956333041
FRENCH: P(e|h) = -0.45896620044521513 ==> log prob of sentence so far: -22.332278521258033

2GRAM: ew
ENGLISH: P(w|e) = -1.446574422594899 ==> log prob of sentence so far: -16.67558837892794
FRENCH: P(w|e) = -3.406976847014123 ==> log prob of sentence so far: -25.739255368272154

2GRAM: wa
ENGLISH: P(a|w) = -0.7202718700852793 ==> log prob of sentence so far: -17.39586024901322
FRENCH: P(a|w) = -0.49335845359034797 ==> log prob of sentence so far: -26.2326138218625

2GRAM: as
ENGLISH: P(s|a) = -0.9960003295333243 ==> log prob of sentence so far: -18.391860578546545
FRENCH: P(s|a) = -1.2507893391409073 ==> log prob of sentence so far: -27.48340316100341

2GRAM: sa
ENGLISH: P(a|s) = -1.035562862015324 ==> log prob of sentence so far: -19.42742344056187
FRENCH: P(a|s) = -0.992636919338493 ==> log prob of sentence so far: -28.476040080341903

2GRAM: as
ENGLISH: P(s|a) = -0.9960003295333243 ==> log prob of sentence so far: -20.423423770095194
FRENCH: P(s|a) = -1.2507893391409073 ==> log prob of sentence so far: -29.72682941948281

2GRAM: st
ENGLISH: P(t|s) = -0.7037615120539172 ==> log prob of sentence so far: -21.12718528214911
FRENCH: P(t|s) = -1.2373622552360959 ==> log prob of sentence so far: -30.964191674718908

2GRAM: tu
ENGLISH: P(u|t) = -1.6185544924733473 ==> log prob of sentence so far: -22.745739774622457
FRENCH: P(u|t) = -1.495633994955907 ==> log prob of sentence so far: -32.459825669674814

2GRAM: ud
ENGLISH: P(d|u) = -1.634036404802705 ==> log prob of sentence so far: -24.37977617942516
FRENCH: P(d|u) = -1.585249170935557 ==> log prob of sentence so far: -34.04507484061037

2GRAM: de
ENGLISH: P(e|d) = -0.8322133640166092 ==> log prob of sentence so far: -25.21198954344177
FRENCH: P(e|d) = -0.29071839298247226 ==> log prob of sentence so far: -34.33579323359284

2GRAM: en
ENGLISH: P(n|e) = -1.0655456684199553 ==> log prob of sentence so far: -26.277535211861725
FRENCH: P(n|e) = -0.8688568329466715 ==> log prob of sentence so far: -35.20465006653951

2GRAM: nt
ENGLISH: P(t|n) = -0.8263240391391752 ==> log prob of sentence so far: -27.1038592510009
FRENCH: P(t|n) = -0.6410407005031727 ==> log prob of sentence so far: -35.84569076704268

2GRAM: ta
ENGLISH: P(a|t) = -1.1698229824614428 ==> log prob of sentence so far: -28.273682233462345
FRENCH: P(a|t) = -0.8828571952877377 ==> log prob of sentence so far: -36.728547962330424

2GRAM: at
ENGLISH: P(t|a) = -0.8628047170414462 ==> log prob of sentence so far: -29.13648695050379
FRENCH: P(t|a) = -1.2480024131158243 ==> log prob of sentence so far: -37.97655037544625

2GRAM: tt
ENGLISH: P(t|t) = -1.2609009822691009 ==> log prob of sentence so far: -30.39738793277289
FRENCH: P(t|t) = -1.3533254172514313 ==> log prob of sentence so far: -39.329875792697685

2GRAM: th
ENGLISH: P(h|t) = -0.4193416964465229 ==> log prob of sentence so far: -30.816729629219413
FRENCH: P(h|t) = -2.126707570733353 ==> log prob of sentence so far: -41.45658336343104

2GRAM: hi
ENGLISH: P(i|h) = -0.792994352028281 ==> log prob of sentence so far: -31.609723981247694
FRENCH: P(i|h) = -1.0752208457966166 ==> log prob of sentence so far: -42.53180420922765

2GRAM: is
ENGLISH: P(s|i) = -0.8699958294708219 ==> log prob of sentence so far: -32.479719810718514
FRENCH: P(s|i) = -0.8503855961161433 ==> log prob of sentence so far: -43.38218980534379

2GRAM: ss
ENGLISH: P(s|s) = -1.1761751118303596 ==> log prob of sentence so far: -33.655894922548875
FRENCH: P(s|s) = -1.0372210095821532 ==> log prob of sentence so far: -44.419410814925946

2GRAM: sc
ENGLISH: P(c|s) = -1.5668901399212958 ==> log prob of sentence so far: -35.22278506247017
FRENCH: P(c|s) = -1.3370487497531387 ==> log prob of sentence so far: -45.75645956467908

2GRAM: ch
ENGLISH: P(h|c) = -0.7749543047999193 ==> log prob of sentence so far: -35.99773936727009
FRENCH: P(h|c) = -0.9482899998958259 ==> log prob of sentence so far: -46.70474956457491

2GRAM: ho
ENGLISH: P(o|h) = -1.103715815990187 ==> log prob of sentence so far: -37.10145518326028
FRENCH: P(o|h) = -0.8339435443419342 ==> log prob of sentence so far: -47.538693108916846

2GRAM: oo
ENGLISH: P(o|o) = -1.3568085649760118 ==> log prob of sentence so far: -38.45826374823629
FRENCH: P(o|o) = -2.753323736374023 ==> log prob of sentence so far: -50.29201684529087

2GRAM: ol
ENGLISH: P(l|o) = -1.4163143762055483 ==> log prob of sentence so far: -39.87457812444184
FRENCH: P(l|o) = -1.4870558469692536 ==> log prob of sentence so far: -51.77907269226012

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: she
ENGLISH: P(e|sh) = -0.604571407969852 ==> log prob of sentence so far: -0.604571407969852
FRENCH: P(e|sh) = -0.654216663313797 ==> log prob of sentence so far: -0.654216663313797

3GRAM: hea
ENGLISH: P(a|he) = -1.1573335459584044 ==> log prob of sentence so far: -1.7619049539282563
FRENCH: P(a|he) = -1.4526787982520024 ==> log prob of sentence so far: -2.1068954615657995

3GRAM: eas
ENGLISH: P(s|ea) = -0.9886623515952251 ==> log prob of sentence so far: -2.7505673055234814
FRENCH: P(s|ea) = -1.5621714344313853 ==> log prob of sentence so far: -3.669066895997185

3GRAM: ask
ENGLISH: P(k|as) = -1.5424142389278186 ==> log prob of sentence so far: -4.2929815444513
FRENCH: P(k|as) = -3.0330214446829107 ==> log prob of sentence so far: -6.7020883406800955

3GRAM: ske
ENGLISH: P(e|sk) = -0.6452851453732424 ==> log prob of sentence so far: -4.938266689824543
FRENCH: P(e|sk) = -1.7075701760979363 ==> log prob of sentence so far: -8.409658516778032

3GRAM: ked
ENGLISH: P(d|ke) = -0.8400036181745685 ==> log prob of sentence so far: -5.778270307999112
FRENCH: P(d|ke) = -1.6901960800285136 ==> log prob of sentence so far: -10.099854596806546

3GRAM: edh
ENGLISH: P(h|ed) = -1.274273783600431 ==> log prob of sentence so far: -7.052544091599542
FRENCH: P(h|ed) = -2.391922621374344 ==> log prob of sentence so far: -12.49177721818089

3GRAM: dhi
ENGLISH: P(i|dh) = -0.4917285999236005 ==> log prob of sentence so far: -7.544272691523143
FRENCH: P(i|dh) = -1.0028856882374881 ==> log prob of sentence so far: -13.494662906418377

3GRAM: him
ENGLISH: P(m|hi) = -0.885236014433036 ==> log prob of sentence so far: -8.429508705956179
FRENCH: P(m|hi) = -1.5387173259661744 ==> log prob of sentence so far: -15.033380232384552

3GRAM: imi
ENGLISH: P(i|im) = -1.1349490561586937 ==> log prob of sentence so far: -9.564457762114873
FRENCH: P(i|im) = -1.3694514708606 ==> log prob of sentence so far: -16.402831703245152

3GRAM: mif
ENGLISH: P(f|mi) = -2.343299158378835 ==> log prob of sentence so far: -11.907756920493709
FRENCH: P(f|mi) = -1.873722912619438 ==> log prob of sentence so far: -18.27655461586459

3GRAM: ifh
ENGLISH: P(h|if) = -1.4586378490256493 ==> log prob of sentence so far: -13.366394769519358
FRENCH: P(h|if) = -2.82020145948564 ==> log prob of sentence so far: -21.096756075350232

3GRAM: fhe
ENGLISH: P(e|fh) = -0.7603184732486272 ==> log prob of sentence so far: -14.126713242767986
FRENCH: P(e|fh) = -0.6600519383056491 ==> log prob of sentence so far: -21.75680801365588

3GRAM: hew
ENGLISH: P(w|he) = -1.200642440574725 ==> log prob of sentence so far: -15.327355683342711
FRENCH: P(w|he) = -3.278753600952829 ==> log prob of sentence so far: -25.03556161460871

3GRAM: ewa
ENGLISH: P(a|ew) = -0.6593305824721628 ==> log prob of sentence so far: -15.986686265814875
FRENCH: P(a|ew) = -0.4895305127014824 ==> log prob of sentence so far: -25.52509212731019

3GRAM: was
ENGLISH: P(s|wa) = -0.38583345521042056 ==> log prob of sentence so far: -16.372519721025295
FRENCH: P(s|wa) = -1.1760912590556813 ==> log prob of sentence so far: -26.701183386365873

3GRAM: asa
ENGLISH: P(a|as) = -1.117442738969486 ==> log prob of sentence so far: -17.489962459994782
FRENCH: P(a|as) = -1.0330214446829107 ==> log prob of sentence so far: -27.734204831048785

3GRAM: sas
ENGLISH: P(s|sa) = -1.1946446421083303 ==> log prob of sentence so far: -18.68460710210311
FRENCH: P(s|sa) = -1.5919314625675987 ==> log prob of sentence so far: -29.326136293616383

3GRAM: ast
ENGLISH: P(t|as) = -0.5424142389278186 ==> log prob of sentence so far: -19.22702134103093
FRENCH: P(t|as) = -1.2069466419820842 ==> log prob of sentence so far: -30.53308293559847

3GRAM: stu
ENGLISH: P(u|st) = -1.4065944163980473 ==> log prob of sentence so far: -20.633615757428977
FRENCH: P(u|st) = -1.4279575154670758 ==> log prob of sentence so far: -31.961040451065543

3GRAM: tud
ENGLISH: P(d|tu) = -1.1601353775232193 ==> log prob of sentence so far: -21.793751134952196
FRENCH: P(d|tu) = -0.9002533049153121 ==> log prob of sentence so far: -32.86129375598085

3GRAM: ude
ENGLISH: P(e|ud) = -0.43462302509874673 ==> log prob of sentence so far: -22.228374160050944
FRENCH: P(e|ud) = -0.20078040709417228 ==> log prob of sentence so far: -33.062074163075025

3GRAM: den
ENGLISH: P(n|de) = -1.0212302489999012 ==> log prob of sentence so far: -23.249604409050846
FRENCH: P(n|de) = -1.3293071745817022 ==> log prob of sentence so far: -34.391381337656725

3GRAM: ent
ENGLISH: P(t|en) = -0.5251753898810755 ==> log prob of sentence so far: -23.77477979893192
FRENCH: P(t|en) = -0.3834635334557952 ==> log prob of sentence so far: -34.77484487111252

3GRAM: nta
ENGLISH: P(a|nt) = -1.2524461848650656 ==> log prob of sentence so far: -25.027225983796985
FRENCH: P(a|nt) = -0.962834543573197 ==> log prob of sentence so far: -35.737679414685715

3GRAM: tat
ENGLISH: P(t|ta) = -1.167250353618274 ==> log prob of sentence so far: -26.19447633741526
FRENCH: P(t|ta) = -1.4642307642173251 ==> log prob of sentence so far: -37.20191017890304

3GRAM: att
ENGLISH: P(t|at) = -0.8572475322361617 ==> log prob of sentence so far: -27.051723869651422
FRENCH: P(t|at) = -0.931795010873589 ==> log prob of sentence so far: -38.13370518977663

3GRAM: tth
ENGLISH: P(h|tt) = -0.28945769307696806 ==> log prob of sentence so far: -27.34118156272839
FRENCH: P(h|tt) = -3.022840610876528 ==> log prob of sentence so far: -41.156545800653156

3GRAM: thi
ENGLISH: P(i|th) = -1.0333814105972134 ==> log prob of sentence so far: -28.374562973325602
FRENCH: P(i|th) = -1.2964342439748322 ==> log prob of sentence so far: -42.452980044627985

3GRAM: his
ENGLISH: P(s|hi) = -0.38764206226239784 ==> log prob of sentence so far: -28.762205035588
FRENCH: P(s|hi) = -0.6892101670468626 ==> log prob of sentence so far: -43.142190211674844

3GRAM: iss
ENGLISH: P(s|is) = -1.1478509107997614 ==> log prob of sentence so far: -29.91005594638776
FRENCH: P(s|is) = -0.752414323562985 ==> log prob of sentence so far: -43.89460453523783

3GRAM: ssc
ENGLISH: P(c|ss) = -1.793252957964765 ==> log prob of sentence so far: -31.703308904352525
FRENCH: P(c|ss) = -2.2301294697405507 ==> log prob of sentence so far: -46.124734004978386

3GRAM: sch
ENGLISH: P(h|sc) = -1.0569048513364727 ==> log prob of sentence so far: -32.760213755688994
FRENCH: P(h|sc) = -1.1320820743513476 ==> log prob of sentence so far: -47.25681607932973

3GRAM: cho
ENGLISH: P(o|ch) = -1.0847590903526079 ==> log prob of sentence so far: -33.8449728460416
FRENCH: P(o|ch) = -1.1418693774913011 ==> log prob of sentence so far: -48.398685456821035

3GRAM: hoo
ENGLISH: P(o|ho) = -1.4656643809822398 ==> log prob of sentence so far: -35.31063722702384
FRENCH: P(o|ho) = -2.6154239528859438 ==> log prob of sentence so far: -51.014109409706975

3GRAM: ool
ENGLISH: P(l|oo) = -1.3094015011144935 ==> log prob of sentence so far: -36.62003872813833
FRENCH: P(l|oo) = -1.0511525224473812 ==> log prob of sentence so far: -52.06526193215436

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: shea
ENGLISH: P(a|she) = -0.6942592128293492 ==> log prob of sentence so far: -0.6942592128293492
FRENCH: P(a|she) = -2.0827853703164503 ==> log prob of sentence so far: -2.0827853703164503

4GRAM: heas
ENGLISH: P(s|hea) = -1.8339802675248897 ==> log prob of sentence so far: -2.528239480354239
FRENCH: P(s|hea) = -1.1186897873312984 ==> log prob of sentence so far: -3.2014751576477485

4GRAM: eask
ENGLISH: P(k|eas) = -2.5276299008713385 ==> log prob of sentence so far: -5.055869381225578
FRENCH: P(k|eas) = -2.089905111439398 ==> log prob of sentence so far: -5.2913802690871465

4GRAM: aske
ENGLISH: P(e|ask) = -0.7878241331261278 ==> log prob of sentence so far: -5.843693514351705
FRENCH: P(e|ask) = -1.4471580313422192 ==> log prob of sentence so far: -6.738538300429366

4GRAM: sked
ENGLISH: P(d|ske) = -0.7725473728656451 ==> log prob of sentence so far: -6.616240887217351
FRENCH: P(d|ske) = -1.4313637641589874 ==> log prob of sentence so far: -8.169902064588353

4GRAM: kedh
ENGLISH: P(h|ked) = -1.287801729930226 ==> log prob of sentence so far: -7.904042617147576
FRENCH: P(h|ked) = -1.4313637641589874 ==> log prob of sentence so far: -9.60126582874734

4GRAM: edhi
ENGLISH: P(i|edh) = -0.2876417982466277 ==> log prob of sentence so far: -8.191684415394203
FRENCH: P(i|edh) = -1.2218487496163564 ==> log prob of sentence so far: -10.823114578363697

4GRAM: dhim
ENGLISH: P(m|dhi) = -0.4359738643897055 ==> log prob of sentence so far: -8.627658279783908
FRENCH: P(m|dhi) = -1.6020599913279623 ==> log prob of sentence so far: -12.42517456969166

4GRAM: himi
ENGLISH: P(i|him) = -1.1211457475911608 ==> log prob of sentence so far: -9.74880402737507
FRENCH: P(i|him) = -0.8920946026904805 ==> log prob of sentence so far: -13.31726917238214

4GRAM: imif
ENGLISH: P(f|imi) = -1.6683859166900001 ==> log prob of sentence so far: -11.417189944065068
FRENCH: P(f|imi) = -1.9444826721501687 ==> log prob of sentence so far: -15.26175184453231

4GRAM: mifh
ENGLISH: P(h|mif) = -1.0543576623225928 ==> log prob of sentence so far: -12.471547606387661
FRENCH: P(h|mif) = -1.7075701760979363 ==> log prob of sentence so far: -16.969322020630244

4GRAM: ifhe
ENGLISH: P(e|ifh) = -0.22985440454086936 ==> log prob of sentence so far: -12.70140201092853
FRENCH: P(e|ifh) = -1.4313637641589874 ==> log prob of sentence so far: -18.400685784789232

4GRAM: fhew
ENGLISH: P(w|fhe) = -1.0290714479472582 ==> log prob of sentence so far: -13.730473458875789
FRENCH: P(w|fhe) = -1.505149978319906 ==> log prob of sentence so far: -19.90583576310914

4GRAM: hewa
ENGLISH: P(a|hew) = -0.635472936881447 ==> log prob of sentence so far: -14.365946395757236
FRENCH: P(a|hew) = -1.4313637641589874 ==> log prob of sentence so far: -21.337199527268126

4GRAM: ewas
ENGLISH: P(s|ewa) = -0.3558970197009113 ==> log prob of sentence so far: -14.721843415458148
FRENCH: P(s|ewa) = -1.0791812460476249 ==> log prob of sentence so far: -22.41638077331575

4GRAM: wasa
ENGLISH: P(a|was) = -0.837239662486924 ==> log prob of sentence so far: -15.559083077945072
FRENCH: P(a|was) = -1.462397997898956 ==> log prob of sentence so far: -23.87877877121471

4GRAM: asas
ENGLISH: P(s|asa) = -1.0023203678451458 ==> log prob of sentence so far: -16.56140344579022
FRENCH: P(s|asa) = -1.0494853630799184 ==> log prob of sentence so far: -24.928264134294626

4GRAM: sast
ENGLISH: P(t|sas) = -0.6752445109100387 ==> log prob of sentence so far: -17.236647956700256
FRENCH: P(t|sas) = -1.076761771924212 ==> log prob of sentence so far: -26.005025906218837

4GRAM: astu
ENGLISH: P(u|ast) = -1.9324737646771533 ==> log prob of sentence so far: -19.169121721377408
FRENCH: P(u|ast) = -1.3127157539891758 ==> log prob of sentence so far: -27.317741660208014

4GRAM: stud
ENGLISH: P(d|stu) = -1.3152704347785915 ==> log prob of sentence so far: -20.484392156155998
FRENCH: P(d|stu) = -1.8836614351536176 ==> log prob of sentence so far: -29.20140309536163

4GRAM: tude
ENGLISH: P(e|tud) = -0.15021074007570376 ==> log prob of sentence so far: -20.6346028962317
FRENCH: P(e|tud) = -0.12161588580083166 ==> log prob of sentence so far: -29.323018981162463

4GRAM: uden
ENGLISH: P(n|ude) = -1.105510184769974 ==> log prob of sentence so far: -21.740113081001674
FRENCH: P(n|ude) = -1.2876838912422413 ==> log prob of sentence so far: -30.610702872404705

4GRAM: dent
ENGLISH: P(t|den) = -0.5427825146048242 ==> log prob of sentence so far: -22.282895595606497
FRENCH: P(t|den) = -0.5293558722673002 ==> log prob of sentence so far: -31.140058744672004

4GRAM: enta
ENGLISH: P(a|ent) = -1.1208871620033467 ==> log prob of sentence so far: -23.403782757609843
FRENCH: P(a|ent) = -0.9488400205091703 ==> log prob of sentence so far: -32.08889876518118

4GRAM: ntat
ENGLISH: P(t|nta) = -1.172160965427249 ==> log prob of sentence so far: -24.575943723037092
FRENCH: P(t|nta) = -1.3992742477611022 ==> log prob of sentence so far: -33.488173012942276

4GRAM: tatt
ENGLISH: P(t|tat) = -0.7686059324774133 ==> log prob of sentence so far: -25.344549655514506
FRENCH: P(t|tat) = -1.005628085008426 ==> log prob of sentence so far: -34.4938010979507

4GRAM: atth
ENGLISH: P(h|att) = -0.2183564144581636 ==> log prob of sentence so far: -25.56290606997267
FRENCH: P(h|att) = -2.6138418218760693 ==> log prob of sentence so far: -37.10764291982677

4GRAM: tthi
ENGLISH: P(i|tth) = -1.0267453810140257 ==> log prob of sentence so far: -26.589651450986693
FRENCH: P(i|tth) = -1.4313637641589874 ==> log prob of sentence so far: -38.53900668398576

4GRAM: this
ENGLISH: P(s|thi) = -0.27490120802370754 ==> log prob of sentence so far: -26.8645526590104
FRENCH: P(s|thi) = -0.7983546364719306 ==> log prob of sentence so far: -39.33736132045769

4GRAM: hiss
ENGLISH: P(s|his) = -0.9958660181384091 ==> log prob of sentence so far: -27.860418677148807
FRENCH: P(s|his) = -0.4178072535036804 ==> log prob of sentence so far: -39.75516857396137

4GRAM: issc
ENGLISH: P(c|iss) = -1.6141939049775604 ==> log prob of sentence so far: -29.47461258212637
FRENCH: P(c|iss) = -3.1089031276673134 ==> log prob of sentence so far: -42.86407170162868

4GRAM: ssch
ENGLISH: P(h|ssc) = -1.0511525224473812 ==> log prob of sentence so far: -30.52576510457375
FRENCH: P(h|ssc) = -1.7558748556724915 ==> log prob of sentence so far: -44.619946557301176

4GRAM: scho
ENGLISH: P(o|sch) = -0.599585372200107 ==> log prob of sentence so far: -31.125350476773857
FRENCH: P(o|sch) = -0.9410525092223048 ==> log prob of sentence so far: -45.56099906652348

4GRAM: choo
ENGLISH: P(o|cho) = -0.9030899869919435 ==> log prob of sentence so far: -32.0284404637658
FRENCH: P(o|cho) = -2.3180633349627615 ==> log prob of sentence so far: -47.87906240148624

4GRAM: hool
ENGLISH: P(l|hoo) = -0.8254037320312699 ==> log prob of sentence so far: -32.853844195797066
FRENCH: P(l|hoo) = -1.4313637641589874 ==> log prob of sentence so far: -49.310426165645225

According to the 4gram model, the sentence is in English
----------------
