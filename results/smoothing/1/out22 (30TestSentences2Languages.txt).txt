Tinctures constitute limited palette of colours.

1GRAM MODEL:

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -1.0342270793962598
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -1.1659703020186778

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -2.1967551581606557
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -2.301180612489978

1GRAM: n
ENGLISH: P(n) = -1.1615995429330288 ==> log prob of sentence so far: -3.3583547010936847
FRENCH: P(n) = -1.1226414664094957 ==> log prob of sentence so far: -3.4238220788994735

1GRAM: c
ENGLISH: P(c) = -1.6265839306967516 ==> log prob of sentence so far: -4.9849386317904365
FRENCH: P(c) = -1.4922220755920426 ==> log prob of sentence so far: -4.916044154491516

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -6.019165711186696
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -6.082014456510194

1GRAM: u
ENGLISH: P(u) = -1.5524859731545257 ==> log prob of sentence so far: -7.571651684341222
FRENCH: P(u) = -1.2061381702765537 ==> log prob of sentence so far: -7.288152626786748

1GRAM: r
ENGLISH: P(r) = -1.2612746582838763 ==> log prob of sentence so far: -8.832926342625099
FRENCH: P(r) = -1.1840319980580551 ==> log prob of sentence so far: -8.472184624844804

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -9.743001612234998
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -9.239156751997252

1GRAM: s
ENGLISH: P(s) = -1.1711435451761196 ==> log prob of sentence so far: -10.914145157411118
FRENCH: P(s) = -1.0644522256524611 ==> log prob of sentence so far: -10.303608977649713

1GRAM: c
ENGLISH: P(c) = -1.6265839306967516 ==> log prob of sentence so far: -12.54072908810787
FRENCH: P(c) = -1.4922220755920426 ==> log prob of sentence so far: -11.795831053241756

1GRAM: o
ENGLISH: P(o) = -1.1377865416115225 ==> log prob of sentence so far: -13.678515629719392
FRENCH: P(o) = -1.2743455831346002 ==> log prob of sentence so far: -13.070176636376356

1GRAM: n
ENGLISH: P(n) = -1.1615995429330288 ==> log prob of sentence so far: -14.840115172652421
FRENCH: P(n) = -1.1226414664094957 ==> log prob of sentence so far: -14.192818102785852

1GRAM: s
ENGLISH: P(s) = -1.1711435451761196 ==> log prob of sentence so far: -16.01125871782854
FRENCH: P(s) = -1.0644522256524611 ==> log prob of sentence so far: -15.257270328438313

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -17.0454857972248
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -16.42324063045699

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -18.208013875989195
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -17.558450940928292

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -19.242240955385455
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -18.72442124294697

1GRAM: u
ENGLISH: P(u) = -1.5524859731545257 ==> log prob of sentence so far: -20.79472692853998
FRENCH: P(u) = -1.2061381702765537 ==> log prob of sentence so far: -19.930559413223524

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -21.82895400793624
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -21.0965297152422

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -22.739029277546138
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -21.863501842394648

1GRAM: l
ENGLISH: P(l) = -1.3477697305137912 ==> log prob of sentence so far: -24.086799008059927
FRENCH: P(l) = -1.2682148156867032 ==> log prob of sentence so far: -23.131716658081352

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -25.249327086824323
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -24.266926968552653

1GRAM: m
ENGLISH: P(m) = -1.6111071179746999 ==> log prob of sentence so far: -26.860434204799024
FRENCH: P(m) = -1.5129465442835708 ==> log prob of sentence so far: -25.779873512836225

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -28.02296228356342
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -26.915083823307526

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -29.05718936295968
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -28.081054125326204

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -29.967264632569577
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -28.84802625247865

1GRAM: d
ENGLISH: P(d) = -1.3961801671840104 ==> log prob of sentence so far: -31.363444799753587
FRENCH: P(d) = -1.4202615782120422 ==> log prob of sentence so far: -30.268287830690692

1GRAM: p
ENGLISH: P(p) = -1.7418188431943058 ==> log prob of sentence so far: -33.10526364294789
FRENCH: P(p) = -1.5386330278876472 ==> log prob of sentence so far: -31.80692085857834

1GRAM: a
ENGLISH: P(a) = -1.086791807028239 ==> log prob of sentence so far: -34.19205544997613
FRENCH: P(a) = -1.0763575048838907 ==> log prob of sentence so far: -32.88327836346223

1GRAM: l
ENGLISH: P(l) = -1.3477697305137912 ==> log prob of sentence so far: -35.53982518048992
FRENCH: P(l) = -1.2682148156867032 ==> log prob of sentence so far: -34.151493179148936

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -36.44990045009982
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -34.918465306301385

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -37.48412752949608
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -36.084435608320064

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -38.51835460889234
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -37.25040591033874

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -39.428429878502236
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -38.01737803749119

1GRAM: o
ENGLISH: P(o) = -1.1377865416115225 ==> log prob of sentence so far: -40.56621642011376
FRENCH: P(o) = -1.2743455831346002 ==> log prob of sentence so far: -39.29172362062579

1GRAM: f
ENGLISH: P(f) = -1.6602710678572594 ==> log prob of sentence so far: -42.22648748797102
FRENCH: P(f) = -1.9917037157389805 ==> log prob of sentence so far: -41.28342733636477

1GRAM: c
ENGLISH: P(c) = -1.6265839306967516 ==> log prob of sentence so far: -43.853071418667774
FRENCH: P(c) = -1.4922220755920426 ==> log prob of sentence so far: -42.775649411956806

1GRAM: o
ENGLISH: P(o) = -1.1377865416115225 ==> log prob of sentence so far: -44.9908579602793
FRENCH: P(o) = -1.2743455831346002 ==> log prob of sentence so far: -44.0499949950914

1GRAM: l
ENGLISH: P(l) = -1.3477697305137912 ==> log prob of sentence so far: -46.33862769079309
FRENCH: P(l) = -1.2682148156867032 ==> log prob of sentence so far: -45.31820981077811

1GRAM: o
ENGLISH: P(o) = -1.1377865416115225 ==> log prob of sentence so far: -47.476414232404615
FRENCH: P(o) = -1.2743455831346002 ==> log prob of sentence so far: -46.592555393912704

1GRAM: u
ENGLISH: P(u) = -1.5524859731545257 ==> log prob of sentence so far: -49.02890020555914
FRENCH: P(u) = -1.2061381702765537 ==> log prob of sentence so far: -47.79869356418926

1GRAM: r
ENGLISH: P(r) = -1.2612746582838763 ==> log prob of sentence so far: -50.29017486384301
FRENCH: P(r) = -1.1840319980580551 ==> log prob of sentence so far: -48.98272556224731

1GRAM: s
ENGLISH: P(s) = -1.1711435451761196 ==> log prob of sentence so far: -51.461318409019135
FRENCH: P(s) = -1.0644522256524611 ==> log prob of sentence so far: -50.04717778789977

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: ti
ENGLISH: P(i|t) = -1.085632857151972 ==> log prob of sentence so far: -1.085632857151972
FRENCH: P(i|t) = -0.9860938950046418 ==> log prob of sentence so far: -0.9860938950046418

2GRAM: in
ENGLISH: P(n|i) = -0.5178078717652497 ==> log prob of sentence so far: -1.6034407289172217
FRENCH: P(n|i) = -0.8963523835994941 ==> log prob of sentence so far: -1.882446278604136

2GRAM: nc
ENGLISH: P(c|n) = -1.3671761554314137 ==> log prob of sentence so far: -2.9706168843486354
FRENCH: P(c|n) = -1.237004211961442 ==> log prob of sentence so far: -3.119450490565578

2GRAM: ct
ENGLISH: P(t|c) = -1.2537955399138556 ==> log prob of sentence so far: -4.224412424262491
FRENCH: P(t|c) = -1.5424752992618775 ==> log prob of sentence so far: -4.661925789827455

2GRAM: tu
ENGLISH: P(u|t) = -1.6185544924733473 ==> log prob of sentence so far: -5.842966916735838
FRENCH: P(u|t) = -1.495633994955907 ==> log prob of sentence so far: -6.157559784783362

2GRAM: ur
ENGLISH: P(r|u) = -0.9179525044888044 ==> log prob of sentence so far: -6.760919421224642
FRENCH: P(r|u) = -0.7888731004457142 ==> log prob of sentence so far: -6.946432885229076

2GRAM: re
ENGLISH: P(e|r) = -0.6282999669648388 ==> log prob of sentence so far: -7.389219388189481
FRENCH: P(e|r) = -0.49108123989053065 ==> log prob of sentence so far: -7.437514125119607

2GRAM: es
ENGLISH: P(s|e) = -0.9449004528705965 ==> log prob of sentence so far: -8.334119841060078
FRENCH: P(s|e) = -0.7261882355074406 ==> log prob of sentence so far: -8.163702360627047

2GRAM: sc
ENGLISH: P(c|s) = -1.5668901399212958 ==> log prob of sentence so far: -9.901009980981375
FRENCH: P(c|s) = -1.3370487497531387 ==> log prob of sentence so far: -9.500751110380186

2GRAM: co
ENGLISH: P(o|c) = -0.7822244499569562 ==> log prob of sentence so far: -10.683234430938331
FRENCH: P(o|c) = -0.6558964550910407 ==> log prob of sentence so far: -10.156647565471227

2GRAM: on
ENGLISH: P(n|o) = -0.862267433922097 ==> log prob of sentence so far: -11.545501864860428
FRENCH: P(n|o) = -0.5206794238803306 ==> log prob of sentence so far: -10.677326989351558

2GRAM: ns
ENGLISH: P(s|n) = -1.2472062467205134 ==> log prob of sentence so far: -12.792708111580941
FRENCH: P(s|n) = -0.9212365456939782 ==> log prob of sentence so far: -11.598563535045535

2GRAM: st
ENGLISH: P(t|s) = -0.7037615120539172 ==> log prob of sentence so far: -13.496469623634859
FRENCH: P(t|s) = -1.2373622552360959 ==> log prob of sentence so far: -12.835925790281632

2GRAM: ti
ENGLISH: P(i|t) = -1.085632857151972 ==> log prob of sentence so far: -14.58210248078683
FRENCH: P(i|t) = -0.9860938950046418 ==> log prob of sentence so far: -13.822019685286273

2GRAM: it
ENGLISH: P(t|i) = -0.9083588982264331 ==> log prob of sentence so far: -15.490461379013263
FRENCH: P(t|i) = -0.7742932574445318 ==> log prob of sentence so far: -14.596312942730805

2GRAM: tu
ENGLISH: P(u|t) = -1.6185544924733473 ==> log prob of sentence so far: -17.109015871486612
FRENCH: P(u|t) = -1.495633994955907 ==> log prob of sentence so far: -16.09194693768671

2GRAM: ut
ENGLISH: P(t|u) = -0.8003585170338139 ==> log prob of sentence so far: -17.909374388520426
FRENCH: P(t|u) = -1.1017128146132409 ==> log prob of sentence so far: -17.19365975229995

2GRAM: te
ENGLISH: P(e|t) = -1.0589450625962922 ==> log prob of sentence so far: -18.96831945111672
FRENCH: P(e|t) = -0.6742352563335733 ==> log prob of sentence so far: -17.867895008633525

2GRAM: el
ENGLISH: P(l|e) = -1.3277895145531675 ==> log prob of sentence so far: -20.296108965669887
FRENCH: P(l|e) = -1.2044529937000865 ==> log prob of sentence so far: -19.072348002333612

2GRAM: li
ENGLISH: P(i|l) = -0.9282177691743759 ==> log prob of sentence so far: -21.224326734844265
FRENCH: P(i|l) = -1.2030244854616337 ==> log prob of sentence so far: -20.275372487795245

2GRAM: im
ENGLISH: P(m|i) = -1.3479991992166425 ==> log prob of sentence so far: -22.572325934060906
FRENCH: P(m|i) = -1.5426642768181869 ==> log prob of sentence so far: -21.81803676461343

2GRAM: mi
ENGLISH: P(i|m) = -1.0706076493715508 ==> log prob of sentence so far: -23.642933583432455
FRENCH: P(i|m) = -1.043744927593803 ==> log prob of sentence so far: -22.861781692207234

2GRAM: it
ENGLISH: P(t|i) = -0.9083588982264331 ==> log prob of sentence so far: -24.551292481658887
FRENCH: P(t|i) = -0.7742932574445318 ==> log prob of sentence so far: -23.636074949651768

2GRAM: te
ENGLISH: P(e|t) = -1.0589450625962922 ==> log prob of sentence so far: -25.610237544255178
FRENCH: P(e|t) = -0.6742352563335733 ==> log prob of sentence so far: -24.310310205985342

2GRAM: ed
ENGLISH: P(d|e) = -1.0374864289809331 ==> log prob of sentence so far: -26.64772397323611
FRENCH: P(d|e) = -1.2790977167639939 ==> log prob of sentence so far: -25.589407922749334

2GRAM: dp
ENGLISH: P(p|d) = -1.8996464857046547 ==> log prob of sentence so far: -28.547370458940765
FRENCH: P(p|d) = -2.5435714239623652 ==> log prob of sentence so far: -28.1329793467117

2GRAM: pa
ENGLISH: P(a|p) = -0.9081418985181764 ==> log prob of sentence so far: -29.455512357458943
FRENCH: P(a|p) = -0.6472938621502154 ==> log prob of sentence so far: -28.780273208861914

2GRAM: al
ENGLISH: P(l|a) = -0.9619789698130631 ==> log prob of sentence so far: -30.417491327272007
FRENCH: P(l|a) = -1.1918664282871085 ==> log prob of sentence so far: -29.972139637149024

2GRAM: le
ENGLISH: P(e|l) = -0.6939321420318187 ==> log prob of sentence so far: -31.111423469303826
FRENCH: P(e|l) = -0.40143134786677326 ==> log prob of sentence so far: -30.373570985015796

2GRAM: et
ENGLISH: P(t|e) = -1.180595696581799 ==> log prob of sentence so far: -32.29201916588563
FRENCH: P(t|e) = -1.081755762820215 ==> log prob of sentence so far: -31.45532674783601

2GRAM: tt
ENGLISH: P(t|t) = -1.2609009822691009 ==> log prob of sentence so far: -33.552920148154726
FRENCH: P(t|t) = -1.3533254172514313 ==> log prob of sentence so far: -32.808652165087445

2GRAM: te
ENGLISH: P(e|t) = -1.0589450625962922 ==> log prob of sentence so far: -34.61186521075102
FRENCH: P(e|t) = -0.6742352563335733 ==> log prob of sentence so far: -33.48288742142102

2GRAM: eo
ENGLISH: P(o|e) = -1.6145240737460236 ==> log prob of sentence so far: -36.226389284497046
FRENCH: P(o|e) = -2.314622412300626 ==> log prob of sentence so far: -35.79750983372165

2GRAM: of
ENGLISH: P(f|o) = -0.9586970452840777 ==> log prob of sentence so far: -37.18508632978112
FRENCH: P(f|o) = -1.933779800832154 ==> log prob of sentence so far: -37.731289634553804

2GRAM: fc
ENGLISH: P(c|f) = -2.054407611165112 ==> log prob of sentence so far: -39.23949394094623
FRENCH: P(c|f) = -2.736151242079864 ==> log prob of sentence so far: -40.46744087663367

2GRAM: co
ENGLISH: P(o|c) = -0.7822244499569562 ==> log prob of sentence so far: -40.02171839090319
FRENCH: P(o|c) = -0.6558964550910407 ==> log prob of sentence so far: -41.123337331724706

2GRAM: ol
ENGLISH: P(l|o) = -1.4163143762055483 ==> log prob of sentence so far: -41.43803276710874
FRENCH: P(l|o) = -1.4870558469692536 ==> log prob of sentence so far: -42.61039317869396

2GRAM: lo
ENGLISH: P(o|l) = -1.031464770073541 ==> log prob of sentence so far: -42.46949753718228
FRENCH: P(o|l) = -1.16404257293882 ==> log prob of sentence so far: -43.77443575163278

2GRAM: ou
ENGLISH: P(u|o) = -0.9046959327873543 ==> log prob of sentence so far: -43.37419346996963
FRENCH: P(u|o) = -0.6279175496747227 ==> log prob of sentence so far: -44.4023533013075

2GRAM: ur
ENGLISH: P(r|u) = -0.9179525044888044 ==> log prob of sentence so far: -44.292145974458435
FRENCH: P(r|u) = -0.7888731004457142 ==> log prob of sentence so far: -45.19122640175321

2GRAM: rs
ENGLISH: P(s|r) = -1.1637939617439301 ==> log prob of sentence so far: -45.45593993620236
FRENCH: P(s|r) = -1.1997459222752236 ==> log prob of sentence so far: -46.39097232402844

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: tin
ENGLISH: P(n|ti) = -0.5916760311481891 ==> log prob of sentence so far: -0.5916760311481891
FRENCH: P(n|ti) = -1.0186577394684435 ==> log prob of sentence so far: -1.0186577394684435

3GRAM: inc
ENGLISH: P(c|in) = -1.5522476823941302 ==> log prob of sentence so far: -2.1439237135423195
FRENCH: P(c|in) = -1.2408746844026635 ==> log prob of sentence so far: -2.259532423871107

3GRAM: nct
ENGLISH: P(t|nc) = -1.4594546675119182 ==> log prob of sentence so far: -3.6033783810542377
FRENCH: P(t|nc) = -1.8283734225922785 ==> log prob of sentence so far: -4.087905846463386

3GRAM: ctu
ENGLISH: P(u|ct) = -1.0 ==> log prob of sentence so far: -4.603378381054238
FRENCH: P(u|ct) = -1.2533118043377782 ==> log prob of sentence so far: -5.341217650801164

3GRAM: tur
ENGLISH: P(r|tu) = -0.42521504599005255 ==> log prob of sentence so far: -5.0285934270442905
FRENCH: P(r|tu) = -0.6333261805637675 ==> log prob of sentence so far: -5.974543831364932

3GRAM: ure
ENGLISH: P(e|ur) = -0.6409610824523555 ==> log prob of sentence so far: -5.6695545094966455
FRENCH: P(e|ur) = -0.6876922132952862 ==> log prob of sentence so far: -6.662236044660219

3GRAM: res
ENGLISH: P(s|re) = -0.8771709886013095 ==> log prob of sentence so far: -6.546725498097955
FRENCH: P(s|re) = -0.6182148968503466 ==> log prob of sentence so far: -7.280450941510566

3GRAM: esc
ENGLISH: P(c|es) = -1.5674046306713287 ==> log prob of sentence so far: -8.114130128769284
FRENCH: P(c|es) = -1.206526052421803 ==> log prob of sentence so far: -8.48697699393237

3GRAM: sco
ENGLISH: P(o|sc) = -0.6046071803418424 ==> log prob of sentence so far: -8.718737309111127
FRENCH: P(o|sc) = -0.4745907625967287 ==> log prob of sentence so far: -8.961567756529098

3GRAM: con
ENGLISH: P(n|co) = -0.5383803972538926 ==> log prob of sentence so far: -9.257117706365019
FRENCH: P(n|co) = -0.43455838218753606 ==> log prob of sentence so far: -9.396126138716633

3GRAM: ons
ENGLISH: P(s|on) = -0.9348001166580476 ==> log prob of sentence so far: -10.191917823023067
FRENCH: P(s|on) = -0.5666085888525093 ==> log prob of sentence so far: -9.962734727569142

3GRAM: nst
ENGLISH: P(t|ns) = -0.6496910237764953 ==> log prob of sentence so far: -10.841608846799563
FRENCH: P(t|ns) = -1.2227867510218209 ==> log prob of sentence so far: -11.185521478590964

3GRAM: sti
ENGLISH: P(i|st) = -0.9939680352300869 ==> log prob of sentence so far: -11.835576882029649
FRENCH: P(i|st) = -1.003688568072689 ==> log prob of sentence so far: -12.189210046663653

3GRAM: tit
ENGLISH: P(t|ti) = -1.1082376544048342 ==> log prob of sentence so far: -12.943814536434484
FRENCH: P(t|ti) = -1.2598398260057668 ==> log prob of sentence so far: -13.44904987266942

3GRAM: itu
ENGLISH: P(u|it) = -1.591450328218298 ==> log prob of sentence so far: -14.535264864652781
FRENCH: P(u|it) = -1.2246945689807671 ==> log prob of sentence so far: -14.673744441650188

3GRAM: tut
ENGLISH: P(t|tu) = -1.9095148853368384 ==> log prob of sentence so far: -16.44477974998962
FRENCH: P(t|tu) = -2.2293120241795368 ==> log prob of sentence so far: -16.903056465829724

3GRAM: ute
ENGLISH: P(e|ut) = -1.2517662553789457 ==> log prob of sentence so far: -17.696546005368567
FRENCH: P(e|ut) = -0.5486773450896669 ==> log prob of sentence so far: -17.451733810919393

3GRAM: tel
ENGLISH: P(l|te) = -1.3219875164859078 ==> log prob of sentence so far: -19.018533521854476
FRENCH: P(l|te) = -1.3089875049695736 ==> log prob of sentence so far: -18.760721315888965

3GRAM: eli
ENGLISH: P(i|el) = -0.8568119156294655 ==> log prob of sentence so far: -19.87534543748394
FRENCH: P(i|el) = -1.261706615108716 ==> log prob of sentence so far: -20.02242793099768

3GRAM: lim
ENGLISH: P(m|li) = -1.5905463557932775 ==> log prob of sentence so far: -21.46589179327722
FRENCH: P(m|li) = -1.2892055411484649 ==> log prob of sentence so far: -21.311633472146145

3GRAM: imi
ENGLISH: P(i|im) = -1.1349490561586937 ==> log prob of sentence so far: -22.60084084943591
FRENCH: P(i|im) = -1.3694514708606 ==> log prob of sentence so far: -22.681084943006745

3GRAM: mit
ENGLISH: P(t|mi) = -0.9264738055464236 ==> log prob of sentence so far: -23.527314654982334
FRENCH: P(t|mi) = -1.3442135884400872 ==> log prob of sentence so far: -24.025298531446833

3GRAM: ite
ENGLISH: P(e|it) = -1.0759437082252217 ==> log prob of sentence so far: -24.603258363207555
FRENCH: P(e|it) = -0.7274090203445804 ==> log prob of sentence so far: -24.752707551791413

3GRAM: ted
ENGLISH: P(d|te) = -0.6948168045749143 ==> log prob of sentence so far: -25.298075167782468
FRENCH: P(d|te) = -1.2185866068737496 ==> log prob of sentence so far: -25.971294158665163

3GRAM: edp
ENGLISH: P(p|ed) = -1.8813935736977956 ==> log prob of sentence so far: -27.179468741480264
FRENCH: P(p|ed) = -2.835620120607057 ==> log prob of sentence so far: -28.80691427927222

3GRAM: dpa
ENGLISH: P(a|dp) = -0.7763603427657627 ==> log prob of sentence so far: -27.955829084246027
FRENCH: P(a|dp) = -0.5376020021010439 ==> log prob of sentence so far: -29.344516281373263

3GRAM: pal
ENGLISH: P(l|pa) = -1.1982931360660312 ==> log prob of sentence so far: -29.154122220312058
FRENCH: P(l|pa) = -1.7439111018213915 ==> log prob of sentence so far: -31.088427383194656

3GRAM: ale
ENGLISH: P(e|al) = -0.6268622281812386 ==> log prob of sentence so far: -29.780984448493296
FRENCH: P(e|al) = -0.5910646070264992 ==> log prob of sentence so far: -31.679491990221155

3GRAM: let
ENGLISH: P(t|le) = -1.030427421226657 ==> log prob of sentence so far: -30.811411869719954
FRENCH: P(t|le) = -1.3255488327047642 ==> log prob of sentence so far: -33.00504082292592

3GRAM: ett
ENGLISH: P(t|et) = -1.2505604030154924 ==> log prob of sentence so far: -32.061972272735446
FRENCH: P(t|et) = -0.9780359992162385 ==> log prob of sentence so far: -33.98307682214216

3GRAM: tte
ENGLISH: P(e|tt) = -0.8924038191377927 ==> log prob of sentence so far: -32.954376091873236
FRENCH: P(e|tt) = -0.2283525642173582 ==> log prob of sentence so far: -34.21142938635952

3GRAM: teo
ENGLISH: P(o|te) = -2.005424012867125 ==> log prob of sentence so far: -34.95980010474036
FRENCH: P(o|te) = -2.085347847735336 ==> log prob of sentence so far: -36.296777234094854

3GRAM: eof
ENGLISH: P(f|eo) = -0.34576787315679974 ==> log prob of sentence so far: -35.30556797789716
FRENCH: P(f|eo) = -1.99563519459755 ==> log prob of sentence so far: -38.2924124286924

3GRAM: ofc
ENGLISH: P(c|of) = -1.6948812043333277 ==> log prob of sentence so far: -37.00044918223049
FRENCH: P(c|of) = -2.6570558528571038 ==> log prob of sentence so far: -40.94946828154951

3GRAM: fco
ENGLISH: P(o|fc) = -0.39055207504982486 ==> log prob of sentence so far: -37.391001257280315
FRENCH: P(o|fc) = -1.5797835966168101 ==> log prob of sentence so far: -42.52925187816632

3GRAM: col
ENGLISH: P(l|co) = -1.351893470192604 ==> log prob of sentence so far: -38.74289472747292
FRENCH: P(l|co) = -1.4518302792961617 ==> log prob of sentence so far: -43.98108215746248

3GRAM: olo
ENGLISH: P(o|ol) = -1.0677623663621372 ==> log prob of sentence so far: -39.81065709383506
FRENCH: P(o|ol) = -0.9045162427299757 ==> log prob of sentence so far: -44.88559840019246

3GRAM: lou
ENGLISH: P(u|lo) = -1.2270657835415015 ==> log prob of sentence so far: -41.03772287737657
FRENCH: P(u|lo) = -1.162972592325968 ==> log prob of sentence so far: -46.04857099251843

3GRAM: our
ENGLISH: P(r|ou) = -0.9335543655354406 ==> log prob of sentence so far: -41.97127724291201
FRENCH: P(r|ou) = -0.6953898142992955 ==> log prob of sentence so far: -46.743960806817725

3GRAM: urs
ENGLISH: P(s|ur) = -0.928537109204423 ==> log prob of sentence so far: -42.89981435211643
FRENCH: P(s|ur) = -0.825460018920419 ==> log prob of sentence so far: -47.56942082573814

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: tinc
ENGLISH: P(c|tin) = -1.3751821344416106 ==> log prob of sentence so far: -1.3751821344416106
FRENCH: P(c|tin) = -0.8142475957319202 ==> log prob of sentence so far: -0.8142475957319202

4GRAM: inct
ENGLISH: P(t|inc) = -1.001521173120533 ==> log prob of sentence so far: -2.3767033075621438
FRENCH: P(t|inc) = -1.146128035678238 ==> log prob of sentence so far: -1.9603756314101581

4GRAM: nctu
ENGLISH: P(u|nct) = -0.7138191253749379 ==> log prob of sentence so far: -3.0905224329370817
FRENCH: P(u|nct) = -1.3617278360175928 ==> log prob of sentence so far: -3.322103467427751

4GRAM: ctur
ENGLISH: P(r|ctu) = -0.31276839893522534 ==> log prob of sentence so far: -3.403290831872307
FRENCH: P(r|ctu) = -0.56194276811998 ==> log prob of sentence so far: -3.884046235547731

4GRAM: ture
ENGLISH: P(e|tur) = -0.3499291920759408 ==> log prob of sentence so far: -3.753220023948248
FRENCH: P(e|tur) = -0.12341756348776714 ==> log prob of sentence so far: -4.007463799035499

4GRAM: ures
ENGLISH: P(s|ure) = -0.6433315512638423 ==> log prob of sentence so far: -4.39655157521209
FRENCH: P(s|ure) = -0.6038963343679596 ==> log prob of sentence so far: -4.611360133403458

4GRAM: resc
ENGLISH: P(c|res) = -1.711807229041191 ==> log prob of sentence so far: -6.108358804253281
FRENCH: P(c|res) = -1.3260797155471573 ==> log prob of sentence so far: -5.937439848950616

4GRAM: esco
ENGLISH: P(o|esc) = -0.992899659582061 ==> log prob of sentence so far: -7.101258463835341
FRENCH: P(o|esc) = -0.3893398369101201 ==> log prob of sentence so far: -6.326779685860736

4GRAM: scon
ENGLISH: P(n|sco) = -0.619788758288394 ==> log prob of sentence so far: -7.721047222123735
FRENCH: P(n|sco) = -0.507644072383219 ==> log prob of sentence so far: -6.834423758243956

4GRAM: cons
ENGLISH: P(s|con) = -0.5671475633470243 ==> log prob of sentence so far: -8.28819478547076
FRENCH: P(s|con) = -0.4053354792955845 ==> log prob of sentence so far: -7.23975923753954

4GRAM: onst
ENGLISH: P(t|ons) = -0.6960788273500153 ==> log prob of sentence so far: -8.984273612820775
FRENCH: P(t|ons) = -1.2084002347956881 ==> log prob of sentence so far: -8.44815947233523

4GRAM: nsti
ENGLISH: P(i|nst) = -1.1697278560998243 ==> log prob of sentence so far: -10.1540014689206
FRENCH: P(i|nst) = -1.295567099962479 ==> log prob of sentence so far: -9.743726572297708

4GRAM: stit
ENGLISH: P(t|sti) = -1.165052202927352 ==> log prob of sentence so far: -11.319053671847952
FRENCH: P(t|sti) = -1.5209001792982497 ==> log prob of sentence so far: -11.264626751595959

4GRAM: titu
ENGLISH: P(u|tit) = -0.8287076238260875 ==> log prob of sentence so far: -12.147761295674039
FRENCH: P(u|tit) = -0.5209001792982497 ==> log prob of sentence so far: -11.785526930894209

4GRAM: itut
ENGLISH: P(t|itu) = -1.151905874537198 ==> log prob of sentence so far: -13.299667170211237
FRENCH: P(t|itu) = -2.1222158782728267 ==> log prob of sentence so far: -13.907742809167036

4GRAM: tute
ENGLISH: P(e|tut) = -0.7533276666586115 ==> log prob of sentence so far: -14.052994836869848
FRENCH: P(e|tut) = -1.2304489213782739 ==> log prob of sentence so far: -15.13819173054531

4GRAM: utel
ENGLISH: P(l|ute) = -1.1409268419924303 ==> log prob of sentence so far: -15.193921678862278
FRENCH: P(l|ute) = -1.2909245593827543 ==> log prob of sentence so far: -16.429116289928064

4GRAM: teli
ENGLISH: P(i|tel) = -1.627820853472384 ==> log prob of sentence so far: -16.82174253233466
FRENCH: P(i|tel) = -1.2736441951743487 ==> log prob of sentence so far: -17.702760485102413

4GRAM: elim
ENGLISH: P(m|eli) = -1.5418060888103977 ==> log prob of sentence so far: -18.363548621145057
FRENCH: P(m|eli) = -1.3751553699217178 ==> log prob of sentence so far: -19.07791585502413

4GRAM: limi
ENGLISH: P(i|lim) = -0.7725473728656451 ==> log prob of sentence so far: -19.136095994010702
FRENCH: P(i|lim) = -0.8855992548316082 ==> log prob of sentence so far: -19.963515109855738

4GRAM: imit
ENGLISH: P(t|imi) = -0.9049579231270629 ==> log prob of sentence so far: -20.041053917137766
FRENCH: P(t|imi) = -0.48208467425121254 ==> log prob of sentence so far: -20.44559978410695

4GRAM: mite
ENGLISH: P(e|mit) = -1.2976763536563933 ==> log prob of sentence so far: -21.33873027079416
FRENCH: P(e|mit) = -0.3288025738826307 ==> log prob of sentence so far: -20.77440235798958

4GRAM: ited
ENGLISH: P(d|ite) = -0.9711432599775263 ==> log prob of sentence so far: -22.309873530771686
FRENCH: P(d|ite) = -0.9910058172970532 ==> log prob of sentence so far: -21.765408175286634

4GRAM: tedp
ENGLISH: P(p|ted) = -1.6307051727774924 ==> log prob of sentence so far: -23.94057870354918
FRENCH: P(p|ted) = -2.79309160017658 ==> log prob of sentence so far: -24.558499775463215

4GRAM: edpa
ENGLISH: P(a|edp) = -0.8422687499953926 ==> log prob of sentence so far: -24.78284745354457
FRENCH: P(a|edp) = -1.2304489213782739 ==> log prob of sentence so far: -25.788948696841487

4GRAM: dpal
ENGLISH: P(l|dpa) = -1.0253058652647702 ==> log prob of sentence so far: -25.80815331880934
FRENCH: P(l|dpa) = -1.7323937598229686 ==> log prob of sentence so far: -27.521342456664456

4GRAM: pale
ENGLISH: P(e|pal) = -0.8239087409443188 ==> log prob of sentence so far: -26.632062059753657
FRENCH: P(e|pal) = -0.41659992096547416 ==> log prob of sentence so far: -27.93794237762993

4GRAM: alet
ENGLISH: P(t|ale) = -1.208505667098878 ==> log prob of sentence so far: -27.840567726852534
FRENCH: P(t|ale) = -1.2032291218678486 ==> log prob of sentence so far: -29.14117149949778

4GRAM: lett
ENGLISH: P(t|let) = -1.1297787594969106 ==> log prob of sentence so far: -28.970346486349445
FRENCH: P(t|let) = -1.251638220448212 ==> log prob of sentence so far: -30.39280971994599

4GRAM: ette
ENGLISH: P(e|ett) = -0.595220566797657 ==> log prob of sentence so far: -29.565567053147102
FRENCH: P(e|ett) = -0.09856921075454148 ==> log prob of sentence so far: -30.49137893070053

4GRAM: tteo
ENGLISH: P(o|tte) = -2.8041394323353503 ==> log prob of sentence so far: -32.36970648548245
FRENCH: P(o|tte) = -1.6874603577105183 ==> log prob of sentence so far: -32.17883928841105

4GRAM: teof
ENGLISH: P(f|teo) = -0.31439395722196267 ==> log prob of sentence so far: -32.68410044270441
FRENCH: P(f|teo) = -1.5481846105451078 ==> log prob of sentence so far: -33.727023898956155

4GRAM: eofc
ENGLISH: P(c|eof) = -1.518148218187304 ==> log prob of sentence so far: -34.20224866089172
FRENCH: P(c|eof) = -1.4913616938342726 ==> log prob of sentence so far: -35.218385592790426

4GRAM: ofco
ENGLISH: P(o|ofc) = -0.3992344790250364 ==> log prob of sentence so far: -34.60148313991675
FRENCH: P(o|ofc) = -1.4313637641589874 ==> log prob of sentence so far: -36.649749356949414

4GRAM: fcol
ENGLISH: P(l|fco) = -1.188325715472693 ==> log prob of sentence so far: -35.789808855389445
FRENCH: P(l|fco) = -1.4313637641589874 ==> log prob of sentence so far: -38.0811131211084

4GRAM: colo
ENGLISH: P(o|col) = -0.5124492170614053 ==> log prob of sentence so far: -36.30225807245085
FRENCH: P(o|col) = -0.6478174818886375 ==> log prob of sentence so far: -38.72893060299704

4GRAM: olou
ENGLISH: P(u|olo) = -0.808609114454539 ==> log prob of sentence so far: -37.11086718690539
FRENCH: P(u|olo) = -1.7708520116421442 ==> log prob of sentence so far: -40.49978261463918

4GRAM: lour
ENGLISH: P(r|lou) = -0.6906974783700289 ==> log prob of sentence so far: -37.801564665275414
FRENCH: P(r|lou) = -0.7868374295687363 ==> log prob of sentence so far: -41.286620044207915

4GRAM: ours
ENGLISH: P(s|our) = -0.6320232147054056 ==> log prob of sentence so far: -38.43358787998082
FRENCH: P(s|our) = -0.7605308858177421 ==> log prob of sentence so far: -42.04715093002566

According to the 4gram model, the sentence is in English
----------------
