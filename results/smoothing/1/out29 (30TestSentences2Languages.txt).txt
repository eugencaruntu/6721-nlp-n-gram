It was magnifique.

1GRAM MODEL:

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -1.1625280787643961
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -1.1352103104713

1GRAM: t
ENGLISH: P(t) = -1.0342270793962598 ==> log prob of sentence so far: -2.1967551581606557
FRENCH: P(t) = -1.1659703020186778 ==> log prob of sentence so far: -2.301180612489978

1GRAM: w
ENGLISH: P(w) = -1.6313501899422194 ==> log prob of sentence so far: -3.8281053481028753
FRENCH: P(w) = -3.916598062775926 ==> log prob of sentence so far: -6.217778675265904

1GRAM: a
ENGLISH: P(a) = -1.086791807028239 ==> log prob of sentence so far: -4.914897155131114
FRENCH: P(a) = -1.0763575048838907 ==> log prob of sentence so far: -7.294136180149795

1GRAM: s
ENGLISH: P(s) = -1.1711435451761196 ==> log prob of sentence so far: -6.086040700307233
FRENCH: P(s) = -1.0644522256524611 ==> log prob of sentence so far: -8.358588405802255

1GRAM: m
ENGLISH: P(m) = -1.6111071179746999 ==> log prob of sentence so far: -7.697147818281933
FRENCH: P(m) = -1.5129465442835708 ==> log prob of sentence so far: -9.871534950085826

1GRAM: a
ENGLISH: P(a) = -1.086791807028239 ==> log prob of sentence so far: -8.783939625310172
FRENCH: P(a) = -1.0763575048838907 ==> log prob of sentence so far: -10.947892454969717

1GRAM: g
ENGLISH: P(g) = -1.6600637277970318 ==> log prob of sentence so far: -10.444003353107204
FRENCH: P(g) = -2.0284319459650515 ==> log prob of sentence so far: -12.976324400934768

1GRAM: n
ENGLISH: P(n) = -1.1615995429330288 ==> log prob of sentence so far: -11.605602896040233
FRENCH: P(n) = -1.1226414664094957 ==> log prob of sentence so far: -14.098965867344264

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -12.76813097480463
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -15.234176177815563

1GRAM: f
ENGLISH: P(f) = -1.6602710678572594 ==> log prob of sentence so far: -14.428402042661888
FRENCH: P(f) = -1.9917037157389805 ==> log prob of sentence so far: -17.225879893554545

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -15.590930121426284
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -18.361090204025846

1GRAM: q
ENGLISH: P(q) = -2.7878655815890627 ==> log prob of sentence so far: -18.378795703015346
FRENCH: P(q) = -1.939419027441695 ==> log prob of sentence so far: -20.30050923146754

1GRAM: u
ENGLISH: P(u) = -1.5524859731545257 ==> log prob of sentence so far: -19.93128167616987
FRENCH: P(u) = -1.2061381702765537 ==> log prob of sentence so far: -21.506647401744093

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -20.84135694577977
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -22.27361952889654

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: it
ENGLISH: P(t|i) = -0.9083588982264331 ==> log prob of sentence so far: -0.9083588982264331
FRENCH: P(t|i) = -0.7742932574445318 ==> log prob of sentence so far: -0.7742932574445318

2GRAM: tw
ENGLISH: P(w|t) = -1.5828005902150086 ==> log prob of sentence so far: -2.4911594884414416
FRENCH: P(w|t) = -4.370984691535196 ==> log prob of sentence so far: -5.145277948979728

2GRAM: wa
ENGLISH: P(a|w) = -0.7202718700852793 ==> log prob of sentence so far: -3.211431358526721
FRENCH: P(a|w) = -0.49335845359034797 ==> log prob of sentence so far: -5.638636402570076

2GRAM: as
ENGLISH: P(s|a) = -0.9960003295333243 ==> log prob of sentence so far: -4.207431688060045
FRENCH: P(s|a) = -1.2507893391409073 ==> log prob of sentence so far: -6.889425741710983

2GRAM: sm
ENGLISH: P(m|s) = -1.6762187494728888 ==> log prob of sentence so far: -5.883650437532934
FRENCH: P(m|s) = -1.4881030387722092 ==> log prob of sentence so far: -8.377528780483193

2GRAM: ma
ENGLISH: P(a|m) = -0.7040251707983967 ==> log prob of sentence so far: -6.58767560833133
FRENCH: P(a|m) = -0.7374452556456322 ==> log prob of sentence so far: -9.114974036128825

2GRAM: ag
ENGLISH: P(g|a) = -1.654050435088533 ==> log prob of sentence so far: -8.241726043419863
FRENCH: P(g|a) = -1.6041050028866524 ==> log prob of sentence so far: -10.719079039015476

2GRAM: gn
ENGLISH: P(n|g) = -1.6017380134445107 ==> log prob of sentence so far: -9.843464056864374
FRENCH: P(n|g) = -0.9956218606148988 ==> log prob of sentence so far: -11.714700899630374

2GRAM: ni
ENGLISH: P(i|n) = -1.3332473151699546 ==> log prob of sentence so far: -11.176711372034328
FRENCH: P(i|n) = -1.4845337532041178 ==> log prob of sentence so far: -13.199234652834493

2GRAM: if
ENGLISH: P(f|i) = -1.676832318595863 ==> log prob of sentence so far: -12.853543690630191
FRENCH: P(f|i) = -1.8970958651134087 ==> log prob of sentence so far: -15.096330517947901

2GRAM: fi
ENGLISH: P(i|f) = -0.9636551340408334 ==> log prob of sentence so far: -13.817198824671024
FRENCH: P(i|f) = -0.8774290021205898 ==> log prob of sentence so far: -15.97375952006849

2GRAM: iq
ENGLISH: P(q|i) = -3.1745157322304145 ==> log prob of sentence so far: -16.99171455690144
FRENCH: P(q|i) = -1.7046308931822618 ==> log prob of sentence so far: -17.678390413250753

2GRAM: qu
ENGLISH: P(u|q) = -0.00717858462712341 ==> log prob of sentence so far: -16.998893141528562
FRENCH: P(u|q) = -0.009003519905163586 ==> log prob of sentence so far: -17.687393933155917

2GRAM: ue
ENGLISH: P(e|u) = -1.3247075359478098 ==> log prob of sentence so far: -18.32360067747637
FRENCH: P(e|u) = -0.8813439489240259 ==> log prob of sentence so far: -18.568737882079944

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: itw
ENGLISH: P(w|it) = -1.22194055692711 ==> log prob of sentence so far: -1.22194055692711
FRENCH: P(w|it) = -3.9279859470994287 ==> log prob of sentence so far: -3.9279859470994287

3GRAM: twa
ENGLISH: P(a|tw) = -0.5964074887769507 ==> log prob of sentence so far: -1.8183480457040608
FRENCH: P(a|tw) = -1.4313637641589874 ==> log prob of sentence so far: -5.359349711258416

3GRAM: was
ENGLISH: P(s|wa) = -0.38583345521042056 ==> log prob of sentence so far: -2.2041815009144816
FRENCH: P(s|wa) = -1.1760912590556813 ==> log prob of sentence so far: -6.535440970314097

3GRAM: asm
ENGLISH: P(m|as) = -1.7255346950673616 ==> log prob of sentence so far: -3.929716195981843
FRENCH: P(m|as) = -1.5058213256199307 ==> log prob of sentence so far: -8.041262295934027

3GRAM: sma
ENGLISH: P(a|sm) = -0.4177864424975235 ==> log prob of sentence so far: -4.347502638479367
FRENCH: P(a|sm) = -0.4542411607318839 ==> log prob of sentence so far: -8.495503456665912

3GRAM: mag
ENGLISH: P(g|ma) = -1.478737324482527 ==> log prob of sentence so far: -5.8262399629618935
FRENCH: P(g|ma) = -1.5974292370611676 ==> log prob of sentence so far: -10.092932693727079

3GRAM: agn
ENGLISH: P(n|ag) = -1.4489056976117394 ==> log prob of sentence so far: -7.275145660573633
FRENCH: P(n|ag) = -0.6992653423153729 ==> log prob of sentence so far: -10.792198036042452

3GRAM: gni
ENGLISH: P(i|gn) = -0.6037019383949391 ==> log prob of sentence so far: -7.878847598968573
FRENCH: P(i|gn) = -0.8659377551275974 ==> log prob of sentence so far: -11.658135791170048

3GRAM: nif
ENGLISH: P(f|ni) = -1.3577039007137548 ==> log prob of sentence so far: -9.236551499682328
FRENCH: P(f|ni) = -1.4666917265007302 ==> log prob of sentence so far: -13.124827517670779

3GRAM: ifi
ENGLISH: P(i|if) = -0.7101268063988285 ==> log prob of sentence so far: -9.946678306081157
FRENCH: P(i|if) = -0.4877629995700349 ==> log prob of sentence so far: -13.612590517240815

3GRAM: fiq
ENGLISH: P(q|fi) = -3.348499570283838 ==> log prob of sentence so far: -13.295177876364995
FRENCH: P(q|fi) = -1.0250356415817372 ==> log prob of sentence so far: -14.637626158822552

3GRAM: iqu
ENGLISH: P(u|iq) = -0.19539641425106793 ==> log prob of sentence so far: -13.490574290616063
FRENCH: P(u|iq) = -0.010723865391773113 ==> log prob of sentence so far: -14.648350024214325

3GRAM: que
ENGLISH: P(e|qu) = -0.3051596098085178 ==> log prob of sentence so far: -13.795733900424581
FRENCH: P(e|qu) = -0.2543114195895424 ==> log prob of sentence so far: -14.902661443803868

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: itwa
ENGLISH: P(a|itw) = -0.2186628471658742 ==> log prob of sentence so far: -0.2186628471658742
FRENCH: P(a|itw) = -1.4313637641589874 ==> log prob of sentence so far: -1.4313637641589874

4GRAM: twas
ENGLISH: P(s|twa) = -0.13535164842939934 ==> log prob of sentence so far: -0.35401449559527354
FRENCH: P(s|twa) = -1.4313637641589874 ==> log prob of sentence so far: -2.8627275283179747

4GRAM: wasm
ENGLISH: P(m|was) = -1.5923882730371612 ==> log prob of sentence so far: -1.9464027686324348
FRENCH: P(m|was) = -1.462397997898956 ==> log prob of sentence so far: -4.325125526216931

4GRAM: asma
ENGLISH: P(a|asm) = -0.5351132016973492 ==> log prob of sentence so far: -2.481515970329784
FRENCH: P(a|asm) = -0.6090088512832903 ==> log prob of sentence so far: -4.9341343775002215

4GRAM: smag
ENGLISH: P(g|sma) = -1.9493900066449128 ==> log prob of sentence so far: -4.430905976974697
FRENCH: P(g|sma) = -1.6727134419961225 ==> log prob of sentence so far: -6.606847819496344

4GRAM: magn
ENGLISH: P(n|mag) = -0.4845452727988694 ==> log prob of sentence so far: -4.915451249773566
FRENCH: P(n|mag) = -0.5023570781357358 ==> log prob of sentence so far: -7.10920489763208

4GRAM: agni
ENGLISH: P(i|agn) = -0.38321675185133125 ==> log prob of sentence so far: -5.298668001624898
FRENCH: P(i|agn) = -0.8135946030286675 ==> log prob of sentence so far: -7.922799500660747

4GRAM: gnif
ENGLISH: P(f|gni) = -0.5524469759230464 ==> log prob of sentence so far: -5.851114977547944
FRENCH: P(f|gni) = -0.46982201597816303 ==> log prob of sentence so far: -8.39262151663891

4GRAM: nifi
ENGLISH: P(i|nif) = -0.5303667666728875 ==> log prob of sentence so far: -6.381481744220832
FRENCH: P(i|nif) = -0.3222192947339193 ==> log prob of sentence so far: -8.714840811372829

4GRAM: ifiq
ENGLISH: P(q|ifi) = -2.4683473304121573 ==> log prob of sentence so far: -8.84982907463299
FRENCH: P(q|ifi) = -0.42116984939051244 ==> log prob of sentence so far: -9.136010660763342

4GRAM: fiqu
ENGLISH: P(u|fiq) = -1.4313637641589874 ==> log prob of sentence so far: -10.281192838791977
FRENCH: P(u|fiq) = -0.1054165969058249 ==> log prob of sentence so far: -9.241427257669166

4GRAM: ique
ENGLISH: P(e|iqu) = -0.47712125471966244 ==> log prob of sentence so far: -10.75831409351164
FRENCH: P(e|iqu) = -0.10438882358668244 ==> log prob of sentence so far: -9.345816081255848

According to the 4gram model, the sentence is in French
----------------
