L'oiseau vole.

1GRAM MODEL:

1GRAM: l
ENGLISH: P(l) = -1.3477697305137912 ==> log prob of sentence so far: -1.3477697305137912
FRENCH: P(l) = -1.2682148156867032 ==> log prob of sentence so far: -1.2682148156867032
DUTCH: P(l) = -1.4248879424212282 ==> log prob of sentence so far: -1.4248879424212282
PORTUGUESE: P(l) = -1.437031928760182 ==> log prob of sentence so far: -1.437031928760182
SPANISH: P(l) = -1.280224525407062 ==> log prob of sentence so far: -1.280224525407062

1GRAM: o
ENGLISH: P(o) = -1.1377865416115225 ==> log prob of sentence so far: -2.4855562721253137
FRENCH: P(o) = -1.2743455831346002 ==> log prob of sentence so far: -2.5425603988213035
DUTCH: P(o) = -1.1998250084767994 ==> log prob of sentence so far: -2.6247129508980276
PORTUGUESE: P(o) = -0.9821621911912899 ==> log prob of sentence so far: -2.419194119951472
SPANISH: P(o) = -0.9966447169789665 ==> log prob of sentence so far: -2.2768692423860286

1GRAM: i
ENGLISH: P(i) = -1.1625280787643961 ==> log prob of sentence so far: -3.64808435088971
FRENCH: P(i) = -1.1352103104713 ==> log prob of sentence so far: -3.6777707092926035
DUTCH: P(i) = -1.2262538577433346 ==> log prob of sentence so far: -3.8509668086413624
PORTUGUESE: P(i) = -1.2397071887182836 ==> log prob of sentence so far: -3.6589013086697557
SPANISH: P(i) = -1.2231599543062535 ==> log prob of sentence so far: -3.500029196692282

1GRAM: s
ENGLISH: P(s) = -1.1711435451761196 ==> log prob of sentence so far: -4.819227896065829
FRENCH: P(s) = -1.0644522256524611 ==> log prob of sentence so far: -4.742222934945064
DUTCH: P(s) = -1.460438187134298 ==> log prob of sentence so far: -5.311404995775661
PORTUGUESE: P(s) = -1.112913538600678 ==> log prob of sentence so far: -4.771814847270434
SPANISH: P(s) = -1.1432693326770131 ==> log prob of sentence so far: -4.643298529369295

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -5.729303165675729
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -5.509195062097511
DUTCH: P(e) = -0.7296633556789381 ==> log prob of sentence so far: -6.041068351454599
PORTUGUESE: P(e) = -0.8804727355199955 ==> log prob of sentence so far: -5.6522875827904295
SPANISH: P(e) = -0.8816658603059454 ==> log prob of sentence so far: -5.52496438967524

1GRAM: a
ENGLISH: P(a) = -1.086791807028239 ==> log prob of sentence so far: -6.816094972703968
FRENCH: P(a) = -1.0763575048838907 ==> log prob of sentence so far: -6.585552566981402
DUTCH: P(a) = -1.1154003185206602 ==> log prob of sentence so far: -7.1564686699752595
PORTUGUESE: P(a) = -0.831923326155409 ==> log prob of sentence so far: -6.484210908945839
SPANISH: P(a) = -0.9020364569062174 ==> log prob of sentence so far: -6.427000846581458

1GRAM: u
ENGLISH: P(u) = -1.5524859731545257 ==> log prob of sentence so far: -8.368580945858493
FRENCH: P(u) = -1.2061381702765537 ==> log prob of sentence so far: -7.791690737257956
DUTCH: P(u) = -1.7453273049444786 ==> log prob of sentence so far: -8.901795974919738
PORTUGUESE: P(u) = -1.3141991217573536 ==> log prob of sentence so far: -7.7984100307031925
SPANISH: P(u) = -1.3656733863951074 ==> log prob of sentence so far: -7.792674232976565

1GRAM: v
ENGLISH: P(v) = -2.043600170252475 ==> log prob of sentence so far: -10.412181116110968
FRENCH: P(v) = -1.8278293527225755 ==> log prob of sentence so far: -9.619520089980531
DUTCH: P(v) = -1.609989959474564 ==> log prob of sentence so far: -10.511785934394302
PORTUGUESE: P(v) = -1.7546018486161579 ==> log prob of sentence so far: -9.55301187931935
SPANISH: P(v) = -1.9460570692896122 ==> log prob of sentence so far: -9.738731302266178

1GRAM: o
ENGLISH: P(o) = -1.1377865416115225 ==> log prob of sentence so far: -11.54996765772249
FRENCH: P(o) = -1.2743455831346002 ==> log prob of sentence so far: -10.893865673115132
DUTCH: P(o) = -1.1998250084767994 ==> log prob of sentence so far: -11.7116109428711
PORTUGUESE: P(o) = -0.9821621911912899 ==> log prob of sentence so far: -10.535174070510639
SPANISH: P(o) = -0.9966447169789665 ==> log prob of sentence so far: -10.735376019245145

1GRAM: l
ENGLISH: P(l) = -1.3477697305137912 ==> log prob of sentence so far: -12.897737388236282
FRENCH: P(l) = -1.2682148156867032 ==> log prob of sentence so far: -12.162080488801834
DUTCH: P(l) = -1.4248879424212282 ==> log prob of sentence so far: -13.13649888529233
PORTUGUESE: P(l) = -1.437031928760182 ==> log prob of sentence so far: -11.972205999270821
SPANISH: P(l) = -1.280224525407062 ==> log prob of sentence so far: -12.015600544652207

1GRAM: e
ENGLISH: P(e) = -0.9100752696098996 ==> log prob of sentence so far: -13.807812657846181
FRENCH: P(e) = -0.766972127152447 ==> log prob of sentence so far: -12.929052615954282
DUTCH: P(e) = -0.7296633556789381 ==> log prob of sentence so far: -13.866162240971267
PORTUGUESE: P(e) = -0.8804727355199955 ==> log prob of sentence so far: -12.852678734790818
SPANISH: P(e) = -0.8816658603059454 ==> log prob of sentence so far: -12.897266404958152

According to the 1gram model, the sentence is in Portuguese
----------------
2GRAM MODEL:

2GRAM: lo
ENGLISH: P(o|l) = -1.031464770073541 ==> log prob of sentence so far: -1.031464770073541
FRENCH: P(o|l) = -1.16404257293882 ==> log prob of sentence so far: -1.16404257293882
DUTCH: P(o|l) = -1.1568843966678894 ==> log prob of sentence so far: -1.1568843966678894
PORTUGUESE: P(o|l) = -1.1092579066679498 ==> log prob of sentence so far: -1.1092579066679498
SPANISH: P(o|l) = -0.7254198915857156 ==> log prob of sentence so far: -0.7254198915857156

2GRAM: oi
ENGLISH: P(i|o) = -1.7693584678950243 ==> log prob of sentence so far: -2.800823237968565
FRENCH: P(i|o) = -1.0544873816718956 ==> log prob of sentence so far: -2.2185299546107156
DUTCH: P(i|o) = -2.1234015861949445 ==> log prob of sentence so far: -3.280285982862834
PORTUGUESE: P(i|o) = -1.6232298896928916 ==> log prob of sentence so far: -2.732487796360841
SPANISH: P(i|o) = -2.2184344093027684 ==> log prob of sentence so far: -2.943854300888484

2GRAM: is
ENGLISH: P(s|i) = -0.8699958294708219 ==> log prob of sentence so far: -3.6708190674393872
FRENCH: P(s|i) = -0.8503855961161433 ==> log prob of sentence so far: -3.068915550726859
DUTCH: P(s|i) = -1.2810068241047197 ==> log prob of sentence so far: -4.561292806967554
PORTUGUESE: P(s|i) = -0.963613823112733 ==> log prob of sentence so far: -3.6961016194735743
SPANISH: P(s|i) = -1.1737680267416861 ==> log prob of sentence so far: -4.11762232763017

2GRAM: se
ENGLISH: P(e|s) = -0.9357957873421036 ==> log prob of sentence so far: -4.6066148547814905
FRENCH: P(e|s) = -0.7711504743080135 ==> log prob of sentence so far: -3.840066025034872
DUTCH: P(e|s) = -1.2732993303808482 ==> log prob of sentence so far: -5.834592137348402
PORTUGUESE: P(e|s) = -0.7115263946452497 ==> log prob of sentence so far: -4.407628014118824
SPANISH: P(e|s) = -0.8025325463824325 ==> log prob of sentence so far: -4.920154874012603

2GRAM: ea
ENGLISH: P(a|e) = -1.058588353645204 ==> log prob of sentence so far: -5.665203208426695
FRENCH: P(a|e) = -1.5190172552264145 ==> log prob of sentence so far: -5.359083280261286
DUTCH: P(a|e) = -2.1309021154086616 ==> log prob of sentence so far: -7.965494252757063
PORTUGUESE: P(a|e) = -1.281695424584262 ==> log prob of sentence so far: -5.689323438703086
SPANISH: P(a|e) = -1.465624472076684 ==> log prob of sentence so far: -6.385779346089286

2GRAM: au
ENGLISH: P(u|a) = -2.121713900391585 ==> log prob of sentence so far: -7.78691710881828
FRENCH: P(u|a) = -1.0790691613741499 ==> log prob of sentence so far: -6.438152441635436
DUTCH: P(u|a) = -2.4957091604012316 ==> log prob of sentence so far: -10.461203413158294
PORTUGUESE: P(u|a) = -1.94111645228713 ==> log prob of sentence so far: -7.630439890990216
SPANISH: P(u|a) = -1.8960913568690272 ==> log prob of sentence so far: -8.281870702958313

2GRAM: uv
ENGLISH: P(v|u) = -2.9654287915945674 ==> log prob of sentence so far: -10.752345900412847
FRENCH: P(v|u) = -1.522197425188468 ==> log prob of sentence so far: -7.960349866823904
DUTCH: P(v|u) = -2.1796221279508146 ==> log prob of sentence so far: -12.640825541109109
PORTUGUESE: P(v|u) = -1.7516078446641985 ==> log prob of sentence so far: -9.382047735654414
SPANISH: P(v|u) = -2.1395476903669164 ==> log prob of sentence so far: -10.42141839332523

2GRAM: vo
ENGLISH: P(o|v) = -1.2229164686099725 ==> log prob of sentence so far: -11.975262369022818
FRENCH: P(o|v) = -0.7103097074543211 ==> log prob of sentence so far: -8.670659574278226
DUTCH: P(o|v) = -0.7287105728089343 ==> log prob of sentence so far: -13.369536113918043
PORTUGUESE: P(o|v) = -0.9185784413150951 ==> log prob of sentence so far: -10.300626176969509
SPANISH: P(o|v) = -0.9166363321728067 ==> log prob of sentence so far: -11.338054725498036

2GRAM: ol
ENGLISH: P(l|o) = -1.4163143762055483 ==> log prob of sentence so far: -13.391576745228367
FRENCH: P(l|o) = -1.4870558469692536 ==> log prob of sentence so far: -10.15771542124748
DUTCH: P(l|o) = -1.2575968636148152 ==> log prob of sentence so far: -14.627132977532858
PORTUGUESE: P(l|o) = -1.3486561656990457 ==> log prob of sentence so far: -11.649282342668554
SPANISH: P(l|o) = -1.247088858765869 ==> log prob of sentence so far: -12.585143584263905

2GRAM: le
ENGLISH: P(e|l) = -0.6939321420318187 ==> log prob of sentence so far: -14.085508887260186
FRENCH: P(e|l) = -0.40143134786677326 ==> log prob of sentence so far: -10.559146769114253
DUTCH: P(e|l) = -0.7541856117292387 ==> log prob of sentence so far: -15.381318589262097
PORTUGUESE: P(e|l) = -0.8514966011609488 ==> log prob of sentence so far: -12.500778943829504
SPANISH: P(e|l) = -0.8395591367609632 ==> log prob of sentence so far: -13.424702721024868

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: loi
ENGLISH: P(i|lo) = -2.4220423867575565 ==> log prob of sentence so far: -2.4220423867575565
FRENCH: P(i|lo) = -1.3175241735255394 ==> log prob of sentence so far: -1.3175241735255394
DUTCH: P(i|lo) = -2.2612628687924934 ==> log prob of sentence so far: -2.2612628687924934
PORTUGUESE: P(i|lo) = -1.8927900303521317 ==> log prob of sentence so far: -1.8927900303521317
SPANISH: P(i|lo) = -1.9707488379473665 ==> log prob of sentence so far: -1.9707488379473665

3GRAM: ois
ENGLISH: P(s|oi) = -0.8325814825922608 ==> log prob of sentence so far: -3.2546238693498175
FRENCH: P(s|oi) = -0.5648983765926713 ==> log prob of sentence so far: -1.8824225501182106
DUTCH: P(s|oi) = -1.25927524755698 ==> log prob of sentence so far: -3.5205381163494733
PORTUGUESE: P(s|oi) = -0.5738342641036089 ==> log prob of sentence so far: -2.4666242944557406
SPANISH: P(s|oi) = -1.5314789170422551 ==> log prob of sentence so far: -3.5022277549896215

3GRAM: ise
ENGLISH: P(e|is) = -1.1257210144816001 ==> log prob of sentence so far: -4.380344883831418
FRENCH: P(e|is) = -0.9073162835487281 ==> log prob of sentence so far: -2.789738833666939
DUTCH: P(e|is) = -1.277018502832267 ==> log prob of sentence so far: -4.797556619181741
PORTUGUESE: P(e|is) = -1.2400434459463137 ==> log prob of sentence so far: -3.7066677404020543
SPANISH: P(e|is) = -1.0812528420527818 ==> log prob of sentence so far: -4.583480597042403

3GRAM: sea
ENGLISH: P(a|se) = -0.8537923511118328 ==> log prob of sentence so far: -5.234137234943251
FRENCH: P(a|se) = -1.361116891107089 ==> log prob of sentence so far: -4.150855724774027
DUTCH: P(a|se) = -2.8808135922807914 ==> log prob of sentence so far: -7.678370211462532
PORTUGUESE: P(a|se) = -1.1542669103651444 ==> log prob of sentence so far: -4.860934650767199
SPANISH: P(a|se) = -1.0688457628437698 ==> log prob of sentence so far: -5.652326359886173

3GRAM: eau
ENGLISH: P(u|ea) = -2.369304227434235 ==> log prob of sentence so far: -7.603441462377486
FRENCH: P(u|ea) = -0.4038704963695322 ==> log prob of sentence so far: -4.55472622114356
DUTCH: P(u|ea) = -1.8129133566428555 ==> log prob of sentence so far: -9.491283568105388
PORTUGUESE: P(u|ea) = -1.9713918307075229 ==> log prob of sentence so far: -6.8323264814747215
SPANISH: P(u|ea) = -1.6188734903376125 ==> log prob of sentence so far: -7.271199850223786

3GRAM: auv
ENGLISH: P(v|au) = -2.7867514221455614 ==> log prob of sentence so far: -10.390192884523048
FRENCH: P(v|au) = -1.6101052227229935 ==> log prob of sentence so far: -6.164831443866554
DUTCH: P(v|au) = -1.7958800173440752 ==> log prob of sentence so far: -11.287163585449463
PORTUGUESE: P(v|au) = -2.287801729930226 ==> log prob of sentence so far: -9.120128211404948
SPANISH: P(v|au) = -2.7693773260761385 ==> log prob of sentence so far: -10.040577176299925

3GRAM: uvo
ENGLISH: P(o|uv) = -1.7323937598229686 ==> log prob of sentence so far: -12.122586644346017
FRENCH: P(o|uv) = -1.3962980616050609 ==> log prob of sentence so far: -7.561129505471614
DUTCH: P(o|uv) = -0.9661417327390326 ==> log prob of sentence so far: -12.253305318188495
PORTUGUESE: P(o|uv) = -1.3822172244092046 ==> log prob of sentence so far: -10.502345435814153
SPANISH: P(o|uv) = -0.6245008603762077 ==> log prob of sentence so far: -10.665078036676132

3GRAM: vol
ENGLISH: P(l|vo) = -0.7517280534988185 ==> log prob of sentence so far: -12.874314697844834
FRENCH: P(l|vo) = -1.1882038424588712 ==> log prob of sentence so far: -8.749333347930484
DUTCH: P(l|vo) = -0.6682550160453932 ==> log prob of sentence so far: -12.921560334233888
PORTUGUESE: P(l|vo) = -0.7033348097384688 ==> log prob of sentence so far: -11.205680245552621
SPANISH: P(l|vo) = -0.5407014036003184 ==> log prob of sentence so far: -11.20577944027645

3GRAM: ole
ENGLISH: P(e|ol) = -0.7638845278253299 ==> log prob of sentence so far: -13.638199225670164
FRENCH: P(e|ol) = -0.4610473797130744 ==> log prob of sentence so far: -9.210380727643559
DUTCH: P(e|ol) = -1.82044031249972 ==> log prob of sentence so far: -14.742000646733608
PORTUGUESE: P(e|ol) = -0.9928422944133798 ==> log prob of sentence so far: -12.198522539966001
SPANISH: P(e|ol) = -0.7116670174927877 ==> log prob of sentence so far: -11.917446457769238

According to the 3gram model, the sentence is in French
----------------
4GRAM MODEL:

4GRAM: lois
ENGLISH: P(s|loi) = -1.3010299956639813 ==> log prob of sentence so far: -1.3010299956639813
FRENCH: P(s|loi) = -0.8278390345727511 ==> log prob of sentence so far: -0.8278390345727511
DUTCH: P(s|loi) = -1.4913616938342726 ==> log prob of sentence so far: -1.4913616938342726
PORTUGUESE: P(s|loi) = -1.5185139398778875 ==> log prob of sentence so far: -1.5185139398778875
SPANISH: P(s|loi) = -1.792391689498254 ==> log prob of sentence so far: -1.792391689498254

4GRAM: oise
ENGLISH: P(e|ois) = -0.5258131786193869 ==> log prob of sentence so far: -1.8268431742833682
FRENCH: P(e|ois) = -0.8579158336336825 ==> log prob of sentence so far: -1.6857548682064336
DUTCH: P(e|ois) = -1.0791812460476249 ==> log prob of sentence so far: -2.5705429398818973
PORTUGUESE: P(e|ois) = -1.098244252097121 ==> log prob of sentence so far: -2.6167581919750083
SPANISH: P(e|ois) = -1.2041199826559248 ==> log prob of sentence so far: -2.9965116721541785

4GRAM: isea
ENGLISH: P(a|ise) = -1.079876673709276 ==> log prob of sentence so far: -2.906719847992644
FRENCH: P(a|ise) = -1.0088937131599824 ==> log prob of sentence so far: -2.6946485813664163
DUTCH: P(a|ise) = -1.9542425094393248 ==> log prob of sentence so far: -4.524785449321222
PORTUGUESE: P(a|ise) = -1.7075701760979363 ==> log prob of sentence so far: -4.324328368072944
SPANISH: P(a|ise) = -1.6901960800285136 ==> log prob of sentence so far: -4.686707752182692

4GRAM: seau
ENGLISH: P(u|sea) = -2.1252488362158366 ==> log prob of sentence so far: -5.031968684208481
FRENCH: P(u|sea) = -0.14366958005389494 ==> log prob of sentence so far: -2.838318161420311
DUTCH: P(u|sea) = -1.4313637641589874 ==> log prob of sentence so far: -5.956149213480209
PORTUGUESE: P(u|sea) = -2.080987046910887 ==> log prob of sentence so far: -6.405315414983831
SPANISH: P(u|sea) = -1.7520484478194385 ==> log prob of sentence so far: -6.438756200002131

4GRAM: eauv
ENGLISH: P(v|eau) = -1.8260748027008264 ==> log prob of sentence so far: -6.858043486909308
FRENCH: P(v|eau) = -2.0718820073061255 ==> log prob of sentence so far: -4.910200168726437
DUTCH: P(v|eau) = -1.2174839442139063 ==> log prob of sentence so far: -7.173633157694116
PORTUGUESE: P(v|eau) = -1.6127838567197355 ==> log prob of sentence so far: -8.018099271703566
SPANISH: P(v|eau) = -1.7993405494535817 ==> log prob of sentence so far: -8.238096749455712

4GRAM: auvo
ENGLISH: P(o|auv) = -1.4313637641589874 ==> log prob of sentence so far: -8.289407251068296
FRENCH: P(o|auv) = -1.5563025007672873 ==> log prob of sentence so far: -6.466502669493725
DUTCH: P(o|auv) = -1.130333768495006 ==> log prob of sentence so far: -8.303966926189123
PORTUGUESE: P(o|auv) = -1.4313637641589874 ==> log prob of sentence so far: -9.449463035862554
SPANISH: P(o|auv) = -1.4313637641589874 ==> log prob of sentence so far: -9.6694605136147

4GRAM: uvol
ENGLISH: P(l|uvo) = -1.4313637641589874 ==> log prob of sentence so far: -9.720771015227283
FRENCH: P(l|uvo) = -0.9890046156985368 ==> log prob of sentence so far: -7.455507285192262
DUTCH: P(l|uvo) = -1.041392685158225 ==> log prob of sentence so far: -9.345359611347348
PORTUGUESE: P(l|uvo) = -0.9294189257142927 ==> log prob of sentence so far: -10.378881961576846
SPANISH: P(l|uvo) = -1.0492180226701815 ==> log prob of sentence so far: -10.718678536284882

4GRAM: vole
ENGLISH: P(e|vol) = -1.1796953833245065 ==> log prob of sentence so far: -10.90046639855179
FRENCH: P(e|vol) = -1.116718406361609 ==> log prob of sentence so far: -8.572225691553871
DUTCH: P(e|vol) = -1.7913898563391086 ==> log prob of sentence so far: -11.136749467686457
PORTUGUESE: P(e|vol) = -1.6163004304425725 ==> log prob of sentence so far: -11.99518239201942
SPANISH: P(e|vol) = -1.4598948527451518 ==> log prob of sentence so far: -12.178573389030033

According to the 4gram model, the sentence is in French
----------------
