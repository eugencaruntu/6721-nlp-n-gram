What will the Japanese economy be like next year?

1GRAM MODEL:

1GRAM: w
ENGLISH: P(w) = -1.7134936393550348 ==> log prob of sentence so far: -1.7134936393550348
FRENCH: P(w) = -4.003509805907437 ==> log prob of sentence so far: -4.003509805907437

1GRAM: h
ENGLISH: P(h) = -1.2624138049742233 ==> log prob of sentence so far: -2.975907444329258
FRENCH: P(h) = -2.189956781312205 ==> log prob of sentence so far: -6.193466587219643

1GRAM: a
ENGLISH: P(a) = -1.1689283236166432 ==> log prob of sentence so far: -4.144835767945901
FRENCH: P(a) = -1.1606801719704456 ==> log prob of sentence so far: -7.354146759190089

1GRAM: t
ENGLISH: P(t) = -1.1163632803584618 ==> log prob of sentence so far: -5.261199048304363
FRENCH: P(t) = -1.2502938249616902 ==> log prob of sentence so far: -8.604440584151778

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -6.024853258383305
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -9.357739359874445

1GRAM: w
ENGLISH: P(w) = -1.7134936393550348 ==> log prob of sentence so far: -7.7383468977383405
FRENCH: P(w) = -4.003509805907437 ==> log prob of sentence so far: -13.361249165781881

1GRAM: i
ENGLISH: P(i) = -1.2446651228481003 ==> log prob of sentence so far: -8.983012020586441
FRENCH: P(i) = -1.21953351953378 ==> log prob of sentence so far: -14.580782685315661

1GRAM: l
ENGLISH: P(l) = -1.4299085279951902 ==> log prob of sentence so far: -10.412920548581631
FRENCH: P(l) = -1.3525395571504588 ==> log prob of sentence so far: -15.93332224246612

1GRAM: l
ENGLISH: P(l) = -1.4299085279951902 ==> log prob of sentence so far: -11.842829076576821
FRENCH: P(l) = -1.3525395571504588 ==> log prob of sentence so far: -17.28586179961658

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -12.606483286655763
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -18.03916057533925

1GRAM: t
ENGLISH: P(t) = -1.1163632803584618 ==> log prob of sentence so far: -13.722846567014225
FRENCH: P(t) = -1.2502938249616902 ==> log prob of sentence so far: -19.28945440030094

1GRAM: h
ENGLISH: P(h) = -1.2624138049742233 ==> log prob of sentence so far: -14.985260371988447
FRENCH: P(h) = -2.189956781312205 ==> log prob of sentence so far: -21.479411181613145

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -15.977471232622543
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -22.33070407300054

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -16.741125442701485
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -23.08400284872321

1GRAM: j
ENGLISH: P(j) = -3.0269729668933083 ==> log prob of sentence so far: -19.768098409594792
FRENCH: P(j) = -2.2939047450657273 ==> log prob of sentence so far: -25.377907593788937

1GRAM: a
ENGLISH: P(a) = -1.1689283236166432 ==> log prob of sentence so far: -20.937026733211436
FRENCH: P(a) = -1.1606801719704456 ==> log prob of sentence so far: -26.53858776575938

1GRAM: p
ENGLISH: P(p) = -1.8239651026074535 ==> log prob of sentence so far: -22.76099183581889
FRENCH: P(p) = -1.6229627876221597 ==> log prob of sentence so far: -28.16155055338154

1GRAM: a
ENGLISH: P(a) = -1.1689283236166432 ==> log prob of sentence so far: -23.929920159435532
FRENCH: P(a) = -1.1606801719704456 ==> log prob of sentence so far: -29.322230725351986

1GRAM: n
ENGLISH: P(n) = -1.2437365799768185 ==> log prob of sentence so far: -25.17365673941235
FRENCH: P(n) = -1.2069645534794413 ==> log prob of sentence so far: -30.529195278831427

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -26.165867600046447
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -31.38048817021882

1GRAM: s
ENGLISH: P(s) = -1.2532806553027906 ==> log prob of sentence so far: -27.41914825534924
FRENCH: P(s) = -1.1487747917545927 ==> log prob of sentence so far: -32.52926296197341

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -28.411359115983334
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -33.380555853360804

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -29.175013326062277
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -34.13385462908347

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -30.167224186696373
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -34.98514752047087

1GRAM: c
ENGLISH: P(c) = -1.7087272742188062 ==> log prob of sentence so far: -31.87595146091518
FRENCH: P(c) = -1.5765507379451407 ==> log prob of sentence so far: -36.56169825841601

1GRAM: o
ENGLISH: P(o) = -1.2199234031596837 ==> log prob of sentence so far: -33.095874864074865
FRENCH: P(o) = -1.3586704071825915 ==> log prob of sentence so far: -37.9203686655986

1GRAM: n
ENGLISH: P(n) = -1.2437365799768185 ==> log prob of sentence so far: -34.339611444051684
FRENCH: P(n) = -1.2069645534794413 ==> log prob of sentence so far: -39.12733321907805

1GRAM: o
ENGLISH: P(o) = -1.2199234031596837 ==> log prob of sentence so far: -35.55953484721137
FRENCH: P(o) = -1.3586704071825915 ==> log prob of sentence so far: -40.48600362626064

1GRAM: m
ENGLISH: P(m) = -1.6932501255573253 ==> log prob of sentence so far: -37.252784972768694
FRENCH: P(m) = -1.597275682203487 ==> log prob of sentence so far: -42.08327930846413

1GRAM: y
ENGLISH: P(y) = -1.832736218169479 ==> log prob of sentence so far: -39.085521190938174
FRENCH: P(y) = -2.7565515045032223 ==> log prob of sentence so far: -44.83983081296735

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -39.84917540101711
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -45.59312958869002

1GRAM: b
ENGLISH: P(b) = -1.834091631326008 ==> log prob of sentence so far: -41.683267032343124
FRENCH: P(b) = -2.1362152530478733 ==> log prob of sentence so far: -47.72934484173789

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -42.67547789297722
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -48.580637733125286

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -43.43913210305616
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -49.333936508847955

1GRAM: l
ENGLISH: P(l) = -1.4299085279951902 ==> log prob of sentence so far: -44.86904063105135
FRENCH: P(l) = -1.3525395571504588 ==> log prob of sentence so far: -50.68647606599841

1GRAM: i
ENGLISH: P(i) = -1.2446651228481003 ==> log prob of sentence so far: -46.113705753899445
FRENCH: P(i) = -1.21953351953378 ==> log prob of sentence so far: -51.906009585532196

1GRAM: k
ENGLISH: P(k) = -2.1548367961831083 ==> log prob of sentence so far: -48.26854255008256
FRENCH: P(k) = -3.620921230913911 ==> log prob of sentence so far: -55.52693081644611

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -49.26075341071665
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -56.378223707833506

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -50.02440762079559
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -57.131522483556175

1GRAM: n
ENGLISH: P(n) = -1.2437365799768185 ==> log prob of sentence so far: -51.26814420077241
FRENCH: P(n) = -1.2069645534794413 ==> log prob of sentence so far: -58.33848703703562

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -52.26035506140651
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -59.189779928423015

1GRAM: x
ENGLISH: P(x) = -3.043662999845278 ==> log prob of sentence so far: -55.30401806125178
FRENCH: P(x) = -2.4229740656245635 ==> log prob of sentence so far: -61.61275399404758

1GRAM: t
ENGLISH: P(t) = -1.1163632803584618 ==> log prob of sentence so far: -56.420381341610245
FRENCH: P(t) = -1.2502938249616902 ==> log prob of sentence so far: -62.86304781900927

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -57.184035551689185
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -63.61634659473194

1GRAM: y
ENGLISH: P(y) = -1.832736218169479 ==> log prob of sentence so far: -59.016771769858664
FRENCH: P(y) = -2.7565515045032223 ==> log prob of sentence so far: -66.37289809923516

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -60.00898263049276
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -67.22419099062255

1GRAM: a
ENGLISH: P(a) = -1.1689283236166432 ==> log prob of sentence so far: -61.1779109541094
FRENCH: P(a) = -1.1606801719704456 ==> log prob of sentence so far: -68.38487116259299

1GRAM: r
ENGLISH: P(r) = -1.3434125438862854 ==> log prob of sentence so far: -62.52132349799569
FRENCH: P(r) = -1.2683557159353935 ==> log prob of sentence so far: -69.65322687852839

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: wh
ENGLISH: P(h|w) = -0.5807443387518778 ==> log prob of sentence so far: -0.5807443387518778
FRENCH: P(h|w) = -1.8084360542881113 ==> log prob of sentence so far: -1.8084360542881113

2GRAM: ha
ENGLISH: P(a|h) = -0.7300942559060702 ==> log prob of sentence so far: -1.3108385946579482
FRENCH: P(a|h) = -0.5264684726003448 ==> log prob of sentence so far: -2.3349045268884563

2GRAM: at
ENGLISH: P(t|a) = -0.875598830073791 ==> log prob of sentence so far: -2.1864374247317393
FRENCH: P(t|a) = -1.3177070457577422 ==> log prob of sentence so far: -3.6526115726461983

2GRAM: t 
ENGLISH: P( |t) = -0.6224172047938763 ==> log prob of sentence so far: -2.808854629525616
FRENCH: P( |t) = -0.3948074145715058 ==> log prob of sentence so far: -4.047418987217704

2GRAM:  w
ENGLISH: P(w| ) = -1.177685462873727 ==> log prob of sentence so far: -3.986540092399343
FRENCH: P(w| ) = -3.9507350183453407 ==> log prob of sentence so far: -7.998154005563045

2GRAM: wi
ENGLISH: P(i|w) = -0.7969119192937272 ==> log prob of sentence so far: -4.78345201169307
FRENCH: P(i|w) = -0.9633380142738545 ==> log prob of sentence so far: -8.9614920198369

2GRAM: il
ENGLISH: P(l|i) = -1.2371318892604701 ==> log prob of sentence so far: -6.0205839009535405
FRENCH: P(l|i) = -0.9316413302830511 ==> log prob of sentence so far: -9.893133350119951

2GRAM: ll
ENGLISH: P(l|l) = -0.8462874532283543 ==> log prob of sentence so far: -6.866871354181895
FRENCH: P(l|l) = -1.1108399903349184 ==> log prob of sentence so far: -11.00397334045487

2GRAM: l 
ENGLISH: P( |l) = -0.8692358155639522 ==> log prob of sentence so far: -7.736107169745847
FRENCH: P( |l) = -1.062582859081955 ==> log prob of sentence so far: -12.066556199536825

2GRAM:  t
ENGLISH: P(t| ) = -0.787363253747808 ==> log prob of sentence so far: -8.523470423493656
FRENCH: P(t| ) = -1.579219372059811 ==> log prob of sentence so far: -13.645775571596635

2GRAM: th
ENGLISH: P(h|t) = -0.43709402370664463 ==> log prob of sentence so far: -8.9605644472003
FRENCH: P(h|t) = -2.3749883004661787 ==> log prob of sentence so far: -16.020763872062815

2GRAM: he
ENGLISH: P(e|h) = -0.37152697512569327 ==> log prob of sentence so far: -9.332091422325993
FRENCH: P(e|h) = -0.4600005188050297 ==> log prob of sentence so far: -16.480764390867844

2GRAM: e 
ENGLISH: P( |e) = -0.4805818326341819 ==> log prob of sentence so far: -9.812673254960174
FRENCH: P( |e) = -0.45464594686088583 ==> log prob of sentence so far: -16.93541033772873

2GRAM:  j
ENGLISH: P(j| ) = -2.427495482498343 ==> log prob of sentence so far: -12.240168737458518
FRENCH: P(j| ) = -1.6338755057532193 ==> log prob of sentence so far: -18.56928584348195

2GRAM: ja
ENGLISH: P(a|j) = -0.6923125194214115 ==> log prob of sentence so far: -12.93248125687993
FRENCH: P(a|j) = -0.8120792683374242 ==> log prob of sentence so far: -19.381365111819374

2GRAM: ap
ENGLISH: P(p|a) = -1.6594244558415556 ==> log prob of sentence so far: -14.591905712721486
FRENCH: P(p|a) = -1.4297675987747605 ==> log prob of sentence so far: -20.811132710594133

2GRAM: pa
ENGLISH: P(a|p) = -0.9679921054989932 ==> log prob of sentence so far: -15.55989781822048
FRENCH: P(a|p) = -0.6479365393291892 ==> log prob of sentence so far: -21.459069249923324

2GRAM: an
ENGLISH: P(n|a) = -0.7075096193827597 ==> log prob of sentence so far: -16.26740743760324
FRENCH: P(n|a) = -0.8347752288172496 ==> log prob of sentence so far: -22.293844478740574

2GRAM: ne
ENGLISH: P(e|n) = -1.077410169937069 ==> log prob of sentence so far: -17.344817607540307
FRENCH: P(e|n) = -0.7933706565270517 ==> log prob of sentence so far: -23.087215135267627

2GRAM: es
ENGLISH: P(s|e) = -1.1166581324228657 ==> log prob of sentence so far: -18.461475739963173
FRENCH: P(s|e) = -0.7950741387552865 ==> log prob of sentence so far: -23.882289274022913

2GRAM: se
ENGLISH: P(e|s) = -0.9631237646383787 ==> log prob of sentence so far: -19.424599504601552
FRENCH: P(e|s) = -0.9202517403414495 ==> log prob of sentence so far: -24.802541014364362

2GRAM: e 
ENGLISH: P( |e) = -0.4805818326341819 ==> log prob of sentence so far: -19.905181337235735
FRENCH: P( |e) = -0.45464594686088583 ==> log prob of sentence so far: -25.257186961225248

2GRAM:  e
ENGLISH: P(e| ) = -1.762612410696786 ==> log prob of sentence so far: -21.66779374793252
FRENCH: P(e| ) = -1.1704177062051893 ==> log prob of sentence so far: -26.427604667430437

2GRAM: ec
ENGLISH: P(c|e) = -1.7514543434988548 ==> log prob of sentence so far: -23.419248091431374
FRENCH: P(c|e) = -1.6729583738455893 ==> log prob of sentence so far: -28.100563041276025

2GRAM: co
ENGLISH: P(o|c) = -0.7856846590325642 ==> log prob of sentence so far: -24.20493275046394
FRENCH: P(o|c) = -0.6562250182835844 ==> log prob of sentence so far: -28.75678805955961

2GRAM: on
ENGLISH: P(n|o) = -0.8688546434110379 ==> log prob of sentence so far: -25.073787393874976
FRENCH: P(n|o) = -0.5227156749929506 ==> log prob of sentence so far: -29.279503734552563

2GRAM: no
ENGLISH: P(o|n) = -1.1802872101766229 ==> log prob of sentence so far: -26.2540746040516
FRENCH: P(o|n) = -1.2599912972556513 ==> log prob of sentence so far: -30.539495031808215

2GRAM: om
ENGLISH: P(m|o) = -1.2349296768941522 ==> log prob of sentence so far: -27.48900428094575
FRENCH: P(m|o) = -1.1522011039864875 ==> log prob of sentence so far: -31.691696135794704

2GRAM: my
ENGLISH: P(y|m) = -1.4551698619948752 ==> log prob of sentence so far: -28.944174142940625
FRENCH: P(y|m) = -2.4987392749319217 ==> log prob of sentence so far: -34.19043541072663

2GRAM: y 
ENGLISH: P( |y) = -0.16273103710113346 ==> log prob of sentence so far: -29.106905180041757
FRENCH: P( |y) = -0.7165588009590896 ==> log prob of sentence so far: -34.90699421168572

2GRAM:  b
ENGLISH: P(b| ) = -1.3041858497871541 ==> log prob of sentence so far: -30.41109102982891
FRENCH: P(b| ) = -1.7871941811494207 ==> log prob of sentence so far: -36.694188392835144

2GRAM: be
ENGLISH: P(e|b) = -0.6137277417845066 ==> log prob of sentence so far: -31.02481877161342
FRENCH: P(e|b) = -1.0668528652741813 ==> log prob of sentence so far: -37.76104125810932

2GRAM: e 
ENGLISH: P( |e) = -0.4805818326341819 ==> log prob of sentence so far: -31.5054006042476
FRENCH: P( |e) = -0.45464594686088583 ==> log prob of sentence so far: -38.21568720497021

2GRAM:  l
ENGLISH: P(l| ) = -1.5795732932216122 ==> log prob of sentence so far: -33.08497389746921
FRENCH: P(l| ) = -0.9697405778171274 ==> log prob of sentence so far: -39.185427782787336

2GRAM: li
ENGLISH: P(i|l) = -0.9631912724114013 ==> log prob of sentence so far: -34.04816516988061
FRENCH: P(i|l) = -1.2145977924784346 ==> log prob of sentence so far: -40.40002557526577

2GRAM: ik
ENGLISH: P(k|i) = -1.9043368912833558 ==> log prob of sentence so far: -35.95250206116397
FRENCH: P(k|i) = -3.393198415928393 ==> log prob of sentence so far: -43.79322399119417

2GRAM: ke
ENGLISH: P(e|k) = -0.4484636798549245 ==> log prob of sentence so far: -36.400965741018894
FRENCH: P(e|k) = -1.1618840979367122 ==> log prob of sentence so far: -44.95510808913088

2GRAM: e 
ENGLISH: P( |e) = -0.4805818326341819 ==> log prob of sentence so far: -36.88154757365307
FRENCH: P( |e) = -0.45464594686088583 ==> log prob of sentence so far: -45.40975403599177

2GRAM:  n
ENGLISH: P(n| ) = -1.6694920658404147 ==> log prob of sentence so far: -38.55103963949349
FRENCH: P(n| ) = -1.3337664668875164 ==> log prob of sentence so far: -46.743520502879285

2GRAM: ne
ENGLISH: P(e|n) = -1.077410169937069 ==> log prob of sentence so far: -39.628449809430556
FRENCH: P(e|n) = -0.7933706565270517 ==> log prob of sentence so far: -47.53689115940634

2GRAM: ex
ENGLISH: P(x|e) = -2.1631235874872865 ==> log prob of sentence so far: -41.79157339691784
FRENCH: P(x|e) = -2.2683798885034747 ==> log prob of sentence so far: -49.80527104790981

2GRAM: xt
ENGLISH: P(t|x) = -0.6754998505124701 ==> log prob of sentence so far: -42.467073247430314
FRENCH: P(t|x) = -1.3342171428738154 ==> log prob of sentence so far: -51.13948819078363

2GRAM: t 
ENGLISH: P( |t) = -0.6224172047938763 ==> log prob of sentence so far: -43.08949045222419
FRENCH: P( |t) = -0.3948074145715058 ==> log prob of sentence so far: -51.53429560535513

2GRAM:  y
ENGLISH: P(y| ) = -1.9409493983802015 ==> log prob of sentence so far: -45.030439850604395
FRENCH: P(y| ) = -2.745793286188042 ==> log prob of sentence so far: -54.28008889154317

2GRAM: ye
ENGLISH: P(e|y) = -0.9791588595021232 ==> log prob of sentence so far: -46.009598710106516
FRENCH: P(e|y) = -0.6201053793530722 ==> log prob of sentence so far: -54.90019427089624

2GRAM: ea
ENGLISH: P(a|e) = -1.2399107379922065 ==> log prob of sentence so far: -47.24950944809872
FRENCH: P(a|e) = -1.882586342228919 ==> log prob of sentence so far: -56.78278061312516

2GRAM: ar
ENGLISH: P(r|a) = -0.9946330495604285 ==> log prob of sentence so far: -48.24414249765915
FRENCH: P(r|a) = -1.0579882107737122 ==> log prob of sentence so far: -57.84076882389888

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: wha
ENGLISH: P(a|wh) = -0.3615009056549607 ==> log prob of sentence so far: -0.3615009056549607
FRENCH: P(a|wh) = -0.9852767431792937 ==> log prob of sentence so far: -0.9852767431792937

3GRAM: hat
ENGLISH: P(t|ha) = -0.47290530889949667 ==> log prob of sentence so far: -0.8344062145544573
FRENCH: P(t|ha) = -1.5456661859215024 ==> log prob of sentence so far: -2.530942929100796

3GRAM: at 
ENGLISH: P( |at) = -0.26604692890376985 ==> log prob of sentence so far: -1.1004531434582272
FRENCH: P( |at) = -1.3289296915197588 ==> log prob of sentence so far: -3.8598726206205547

3GRAM: t w
ENGLISH: P(w|t ) = -1.1156897908036227 ==> log prob of sentence so far: -2.2161429342618497
FRENCH: P(w|t ) = -4.577273448787888 ==> log prob of sentence so far: -8.437146069408442

3GRAM:  wi
ENGLISH: P(i| w) = -0.6877718064837165 ==> log prob of sentence so far: -2.903914740745566
FRENCH: P(i| w) = -1.2937307569224816 ==> log prob of sentence so far: -9.730876826330924

3GRAM: wil
ENGLISH: P(l|wi) = -0.8097767632917368 ==> log prob of sentence so far: -3.713691504037303
FRENCH: P(l|wi) = -1.1949766032160551 ==> log prob of sentence so far: -10.92585342954698

3GRAM: ill
ENGLISH: P(l|il) = -0.4541521753891548 ==> log prob of sentence so far: -4.167843679426458
FRENCH: P(l|il) = -0.6179303923628956 ==> log prob of sentence so far: -11.543783821909875

3GRAM: ll 
ENGLISH: P( |ll) = -0.23405611190568498 ==> log prob of sentence so far: -4.401899791332143
FRENCH: P( |ll) = -2.80881093086029 ==> log prob of sentence so far: -14.352594752770164

3GRAM: l t
ENGLISH: P(t|l ) = -0.7961730731325376 ==> log prob of sentence so far: -5.198072864464681
FRENCH: P(t|l ) = -1.820966187747856 ==> log prob of sentence so far: -16.17356094051802

3GRAM:  th
ENGLISH: P(h| t) = -0.1344910283641496 ==> log prob of sentence so far: -5.332563892828831
FRENCH: P(h| t) = -1.922998755285585 ==> log prob of sentence so far: -18.096559695803606

3GRAM: the
ENGLISH: P(e|th) = -0.19703627885635827 ==> log prob of sentence so far: -5.529600171685189
FRENCH: P(e|th) = -0.30619408126398834 ==> log prob of sentence so far: -18.402753777067595

3GRAM: he 
ENGLISH: P( |he) = -0.22900636188657594 ==> log prob of sentence so far: -5.758606533571765
FRENCH: P( |he) = -0.7336841778671976 ==> log prob of sentence so far: -19.13643795493479

3GRAM: e j
ENGLISH: P(j|e ) = -2.440384254832884 ==> log prob of sentence so far: -8.198990788404648
FRENCH: P(j|e ) = -1.6783064302227964 ==> log prob of sentence so far: -20.81474438515759

3GRAM:  ja
ENGLISH: P(a| j) = -0.6476582538458261 ==> log prob of sentence so far: -8.846649042250474
FRENCH: P(a| j) = -0.7812594838531737 ==> log prob of sentence so far: -21.59600386901076

3GRAM: jap
ENGLISH: P(p|ja) = -1.0216486269624678 ==> log prob of sentence so far: -9.868297669212943
FRENCH: P(p|ja) = -1.0061603087048185 ==> log prob of sentence so far: -22.60216417771558

3GRAM: apa
ENGLISH: P(a|ap) = -1.4158649926228746 ==> log prob of sentence so far: -11.284162661835818
FRENCH: P(a|ap) = -1.7405625489003955 ==> log prob of sentence so far: -24.342726726615975

3GRAM: pan
ENGLISH: P(n|pa) = -0.9596360847464979 ==> log prob of sentence so far: -12.243798746582316
FRENCH: P(n|pa) = -1.5301853035300756 ==> log prob of sentence so far: -25.87291203014605

3GRAM: ane
ENGLISH: P(e|an) = -1.8886423575761018 ==> log prob of sentence so far: -14.132441104158417
FRENCH: P(e|an) = -1.855317205195943 ==> log prob of sentence so far: -27.728229235341992

3GRAM: nes
ENGLISH: P(s|ne) = -0.7679580942339896 ==> log prob of sentence so far: -14.900399198392407
FRENCH: P(s|ne) = -1.02941779617884 ==> log prob of sentence so far: -28.757647031520833

3GRAM: ese
ENGLISH: P(e|es) = -1.0756072151232605 ==> log prob of sentence so far: -15.976006413515668
FRENCH: P(e|es) = -1.8475190127357808 ==> log prob of sentence so far: -30.605166044256613

3GRAM: se 
ENGLISH: P( |se) = -0.5588986014572302 ==> log prob of sentence so far: -16.5349050149729
FRENCH: P( |se) = -0.4943251435190449 ==> log prob of sentence so far: -31.09949118777566

3GRAM: e e
ENGLISH: P(e|e ) = -1.7007452690253608 ==> log prob of sentence so far: -18.23565028399826
FRENCH: P(e|e ) = -1.2690711927649507 ==> log prob of sentence so far: -32.36856238054061

3GRAM:  ec
ENGLISH: P(c| e) = -2.477561619988554 ==> log prob of sentence so far: -20.713211903986814
FRENCH: P(c| e) = -1.5458152711752433 ==> log prob of sentence so far: -33.91437765171585

3GRAM: eco
ENGLISH: P(o|ec) = -1.055536791592133 ==> log prob of sentence so far: -21.768748695578946
FRENCH: P(o|ec) = -0.8366998187241965 ==> log prob of sentence so far: -34.75107747044005

3GRAM: con
ENGLISH: P(n|co) = -0.5347205492328668 ==> log prob of sentence so far: -22.30346924481181
FRENCH: P(n|co) = -0.4334670814278006 ==> log prob of sentence so far: -35.18454455186785

3GRAM: ono
ENGLISH: P(o|on) = -1.9910104319630841 ==> log prob of sentence so far: -24.294479676774895
FRENCH: P(o|on) = -2.093005950241829 ==> log prob of sentence so far: -37.27755050210968

3GRAM: nom
ENGLISH: P(m|no) = -1.816318391406187 ==> log prob of sentence so far: -26.110798068181083
FRENCH: P(m|no) = -1.005858138240791 ==> log prob of sentence so far: -38.28340864035047

3GRAM: omy
ENGLISH: P(y|om) = -2.378067387627845 ==> log prob of sentence so far: -28.488865455808927
FRENCH: P(y|om) = -3.717087724927019 ==> log prob of sentence so far: -42.00049636527749

3GRAM: my 
ENGLISH: P( |my) = -0.10046893076049287 ==> log prob of sentence so far: -28.58933438656942
FRENCH: P( |my) = -0.6386241519648548 ==> log prob of sentence so far: -42.63912051724235

3GRAM: y b
ENGLISH: P(b|y ) = -1.299776562246559 ==> log prob of sentence so far: -29.88911094881598
FRENCH: P(b|y ) = -1.8217318216900442 ==> log prob of sentence so far: -44.46085233893239

3GRAM:  be
ENGLISH: P(e| b) = -0.48246064653285214 ==> log prob of sentence so far: -30.371571595348833
FRENCH: P(e| b) = -1.0900039280435938 ==> log prob of sentence so far: -45.550856266975984

3GRAM: be 
ENGLISH: P( |be) = -0.604826725883828 ==> log prob of sentence so far: -30.97639832123266
FRENCH: P( |be) = -0.6973510109162503 ==> log prob of sentence so far: -46.248207277892234

3GRAM: e l
ENGLISH: P(l|e ) = -1.490404792942822 ==> log prob of sentence so far: -32.466803114175484
FRENCH: P(l|e ) = -0.961363736110971 ==> log prob of sentence so far: -47.2095710140032

3GRAM:  li
ENGLISH: P(i| l) = -0.4925898761311285 ==> log prob of sentence so far: -32.95939299030661
FRENCH: P(i| l) = -1.2661083483635807 ==> log prob of sentence so far: -48.47567936236678

3GRAM: lik
ENGLISH: P(k|li) = -0.8066024166367178 ==> log prob of sentence so far: -33.76599540694333
FRENCH: P(k|li) = -3.659440781870318 ==> log prob of sentence so far: -52.1351201442371

3GRAM: ike
ENGLISH: P(e|ik) = -0.02248236791012965 ==> log prob of sentence so far: -33.78847777485346
FRENCH: P(e|ik) = -1.8260748027008264 ==> log prob of sentence so far: -53.96119494693792

3GRAM: ke 
ENGLISH: P( |ke) = -0.39092702728120227 ==> log prob of sentence so far: -34.17940480213466
FRENCH: P( |ke) = -1.2632414347745815 ==> log prob of sentence so far: -55.22443638171251

3GRAM: e n
ENGLISH: P(n|e ) = -1.7001662096303698 ==> log prob of sentence so far: -35.87957101176503
FRENCH: P(n|e ) = -1.1750984768892099 ==> log prob of sentence so far: -56.39953485860172

3GRAM:  ne
ENGLISH: P(e| n) = -0.7882688974531556 ==> log prob of sentence so far: -36.66783990921819
FRENCH: P(e| n) = -0.3964986682344542 ==> log prob of sentence so far: -56.796033526836176

3GRAM: nex
ENGLISH: P(x|ne) = -1.7533967063983489 ==> log prob of sentence so far: -38.421236615616536
FRENCH: P(x|ne) = -2.4071162373495896 ==> log prob of sentence so far: -59.20314976418577

3GRAM: ext
ENGLISH: P(t|ex) = -0.6162821408937332 ==> log prob of sentence so far: -39.037518756510266
FRENCH: P(t|ex) = -0.6543878716832783 ==> log prob of sentence so far: -59.85753763586904

3GRAM: xt 
ENGLISH: P( |xt) = -0.6068377565864305 ==> log prob of sentence so far: -39.644356513096696
FRENCH: P( |xt) = -2.4983105537896004 ==> log prob of sentence so far: -62.355848189658644

3GRAM: t y
ENGLISH: P(y|t ) = -1.958600633336811 ==> log prob of sentence so far: -41.6029571464335
FRENCH: P(y|t ) = -3.2550541540539686 ==> log prob of sentence so far: -65.61090234371261

3GRAM:  ye
ENGLISH: P(e| y) = -0.38429807846192965 ==> log prob of sentence so far: -41.987255224895435
FRENCH: P(e| y) = -0.35868670952436404 ==> log prob of sentence so far: -65.96958905323697

3GRAM: yea
ENGLISH: P(a|ye) = -1.0454718743389562 ==> log prob of sentence so far: -43.03272709923439
FRENCH: P(a|ye) = -2.8674674878590514 ==> log prob of sentence so far: -68.83705654109602

3GRAM: ear
ENGLISH: P(r|ea) = -0.6731672479398241 ==> log prob of sentence so far: -43.705894347174215
FRENCH: P(r|ea) = -2.378677369736355 ==> log prob of sentence so far: -71.21573391083237

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: what
ENGLISH: P(t|wha) = -0.5607678973038716 ==> log prob of sentence so far: -0.5607678973038716
FRENCH: P(t|wha) = -1.462397997898956 ==> log prob of sentence so far: -1.462397997898956

4GRAM: hat 
ENGLISH: P( |hat) = -0.03826839966313622 ==> log prob of sentence so far: -0.5990362969670078
FRENCH: P( |hat) = -1.2304489213782739 ==> log prob of sentence so far: -2.6928469192772297

4GRAM: at w
ENGLISH: P(w|at ) = -1.1593733730551299 ==> log prob of sentence so far: -1.7584096700221377
FRENCH: P(w|at ) = -2.45178643552429 ==> log prob of sentence so far: -5.14463335480152

4GRAM: t wi
ENGLISH: P(i|t w) = -0.7113792623178388 ==> log prob of sentence so far: -2.469788932339976
FRENCH: P(i|t w) = -1.4471580313422192 ==> log prob of sentence so far: -6.59179138614374

4GRAM:  wil
ENGLISH: P(l| wi) = -0.7431140478449457 ==> log prob of sentence so far: -3.212902980184922
FRENCH: P(l| wi) = -0.9852767431792937 ==> log prob of sentence so far: -7.577068129323034

4GRAM: will
ENGLISH: P(l|wil) = -0.12712792024819436 ==> log prob of sentence so far: -3.340030900433116
FRENCH: P(l|wil) = -1.462397997898956 ==> log prob of sentence so far: -9.03946612722199

4GRAM: ill 
ENGLISH: P( |ill) = -0.1384977844083445 ==> log prob of sentence so far: -3.4785286848414607
FRENCH: P( |ill) = -3.4513258084895195 ==> log prob of sentence so far: -12.49079193571151

4GRAM: ll t
ENGLISH: P(t|ll ) = -0.7054959645730905 ==> log prob of sentence so far: -4.184024649414551
FRENCH: P(t|ll ) = -1.5440680443502757 ==> log prob of sentence so far: -14.034859980061785

4GRAM: l th
ENGLISH: P(h|l t) = -0.11457322113646629 ==> log prob of sentence so far: -4.298597870551018
FRENCH: P(h|l t) = -2.089905111439398 ==> log prob of sentence so far: -16.12476509150118

4GRAM:  the
ENGLISH: P(e| th) = -0.14996514927326823 ==> log prob of sentence so far: -4.448563019824286
FRENCH: P(e| th) = -0.21222410127207483 ==> log prob of sentence so far: -16.336989192773256

4GRAM: the 
ENGLISH: P( |the) = -0.1532721157123939 ==> log prob of sentence so far: -4.60183513553668
FRENCH: P( |the) = -1.3296751771135114 ==> log prob of sentence so far: -17.666664369886767

4GRAM: he j
ENGLISH: P(j|he ) = -2.2860276906215655 ==> log prob of sentence so far: -6.887862826158246
FRENCH: P(j|he ) = -2.0055481951688097 ==> log prob of sentence so far: -19.672212565055577

4GRAM: e ja
ENGLISH: P(a|e j) = -0.676330638870848 ==> log prob of sentence so far: -7.564193465029094
FRENCH: P(a|e j) = -0.7910033739543864 ==> log prob of sentence so far: -20.463215939009963

4GRAM:  jap
ENGLISH: P(p| ja) = -0.9518822075323539 ==> log prob of sentence so far: -8.516075672561447
FRENCH: P(p| ja) = -0.9417280737613626 ==> log prob of sentence so far: -21.404944012771324

4GRAM: japa
ENGLISH: P(a|jap) = -0.2177898931394888 ==> log prob of sentence so far: -8.733865565700937
FRENCH: P(a|jap) = -2.2013971243204513 ==> log prob of sentence so far: -23.606341137091775

4GRAM: apan
ENGLISH: P(n|apa) = -0.5624311968296473 ==> log prob of sentence so far: -9.296296762530584
FRENCH: P(n|apa) = -1.3222192947339193 ==> log prob of sentence so far: -24.928560431825694

4GRAM: pane
ENGLISH: P(e|pan) = -0.9603594342490068 ==> log prob of sentence so far: -10.256656196779591
FRENCH: P(e|pan) = -2.4638929889859074 ==> log prob of sentence so far: -27.392453420811602

4GRAM: anes
ENGLISH: P(s|ane) = -0.8366030115725244 ==> log prob of sentence so far: -11.093259208352116
FRENCH: P(s|ane) = -0.7831720517586944 ==> log prob of sentence so far: -28.175625472570296

4GRAM: nese
ENGLISH: P(e|nes) = -1.7326374032974672 ==> log prob of sentence so far: -12.825896611649583
FRENCH: P(e|nes) = -2.3393094453980634 ==> log prob of sentence so far: -30.51493491796836

4GRAM: ese 
ENGLISH: P( |ese) = -0.2323383862880483 ==> log prob of sentence so far: -13.058234997937632
FRENCH: P( |ese) = -1.2529312892884035 ==> log prob of sentence so far: -31.767866207256766

4GRAM: se e
ENGLISH: P(e|se ) = -1.6756831381921664 ==> log prob of sentence so far: -14.733918136129798
FRENCH: P(e|se ) = -1.2162858897109594 ==> log prob of sentence so far: -32.984152096967726

4GRAM: e ec
ENGLISH: P(c|e e) = -2.1418771585245797 ==> log prob of sentence so far: -16.875795294654377
FRENCH: P(c|e e) = -1.7504400381584933 ==> log prob of sentence so far: -34.73459213512622

4GRAM:  eco
ENGLISH: P(o| ec) = -0.8450980400142568 ==> log prob of sentence so far: -17.720893334668634
FRENCH: P(o| ec) = -1.0326920038700187 ==> log prob of sentence so far: -35.76728413899624

4GRAM: econ
ENGLISH: P(n|eco) = -0.42786273650356094 ==> log prob of sentence so far: -18.148756071172194
FRENCH: P(n|eco) = -0.3451925726366905 ==> log prob of sentence so far: -36.11247671163293

4GRAM: cono
ENGLISH: P(o|con) = -2.3863998681213805 ==> log prob of sentence so far: -20.535155939293574
FRENCH: P(o|con) = -2.5212569870536914 ==> log prob of sentence so far: -38.63373369868662

4GRAM: onom
ENGLISH: P(m|ono) = -0.5839197658034487 ==> log prob of sentence so far: -21.11907570509702
FRENCH: P(m|ono) = -0.49458268127035737 ==> log prob of sentence so far: -39.12831637995698

4GRAM: nomy
ENGLISH: P(y|nom) = -1.0763883458634547 ==> log prob of sentence so far: -22.195464050960474
FRENCH: P(y|nom) = -2.7715874808812555 ==> log prob of sentence so far: -41.899903860838236

4GRAM: omy 
ENGLISH: P( |omy) = -0.39414701965489857 ==> log prob of sentence so far: -22.589611070615373
FRENCH: P( |omy) = -1.4471580313422192 ==> log prob of sentence so far: -43.347061892180456

4GRAM: my b
ENGLISH: P(b|my ) = -1.0724533535248928 ==> log prob of sentence so far: -23.662064424140265
FRENCH: P(b|my ) = -1.7993405494535817 ==> log prob of sentence so far: -45.14640244163404

4GRAM: y be
ENGLISH: P(e|y b) = -0.39273188451551816 ==> log prob of sentence so far: -24.054796308655785
FRENCH: P(e|y b) = -1.5440680443502757 ==> log prob of sentence so far: -46.690470485984314

4GRAM:  be 
ENGLISH: P( | be) = -0.5498832694862302 ==> log prob of sentence so far: -24.604679578142015
FRENCH: P( | be) = -2.622214022966295 ==> log prob of sentence so far: -49.31268450895061

4GRAM: be l
ENGLISH: P(l|be ) = -1.8431683156163479 ==> log prob of sentence so far: -26.447847893758365
FRENCH: P(l|be ) = -0.9470341367891504 ==> log prob of sentence so far: -50.25971864573976

4GRAM: e li
ENGLISH: P(i|e l) = -0.5388889280089508 ==> log prob of sentence so far: -26.986736821767316
FRENCH: P(i|e l) = -1.242676488114361 ==> log prob of sentence so far: -51.50239513385412

4GRAM:  lik
ENGLISH: P(k| li) = -0.4822472449674334 ==> log prob of sentence so far: -27.46898406673475
FRENCH: P(k| li) = -3.2402995820027125 ==> log prob of sentence so far: -54.742694715856835

4GRAM: like
ENGLISH: P(e|lik) = -0.007744825634963438 ==> log prob of sentence so far: -27.47672889236971
FRENCH: P(e|lik) = -1.4471580313422192 ==> log prob of sentence so far: -56.189852747199055

4GRAM: ike 
ENGLISH: P( |ike) = -0.05259223516833116 ==> log prob of sentence so far: -27.52932112753804
FRENCH: P( |ike) = -1.4471580313422192 ==> log prob of sentence so far: -57.637010778541274

4GRAM: ke n
ENGLISH: P(n|ke ) = -1.8532270487444413 ==> log prob of sentence so far: -29.38254817628248
FRENCH: P(n|ke ) = -1.462397997898956 ==> log prob of sentence so far: -59.09940877644023

4GRAM: e ne
ENGLISH: P(e|e n) = -0.6855284832668207 ==> log prob of sentence so far: -30.068076659549302
FRENCH: P(e|e n) = -0.3201061265561597 ==> log prob of sentence so far: -59.41951490299639

4GRAM:  nex
ENGLISH: P(x| ne) = -1.2124631428388555 ==> log prob of sentence so far: -31.28053980238816
FRENCH: P(x| ne) = -2.462949383004911 ==> log prob of sentence so far: -61.8824642860013

4GRAM: next
ENGLISH: P(t|nex) = -0.2560965935515311 ==> log prob of sentence so far: -31.53663639593969
FRENCH: P(t|nex) = -0.9176487071628685 ==> log prob of sentence so far: -62.80011299316417

4GRAM: ext 
ENGLISH: P( |ext) = -0.5478944522033286 ==> log prob of sentence so far: -32.08453084814302
FRENCH: P( |ext) = -2.4955443375464483 ==> log prob of sentence so far: -65.29565733071061

4GRAM: xt y
ENGLISH: P(y|xt ) = -2.143014800254095 ==> log prob of sentence so far: -34.22754564839711
FRENCH: P(y|xt ) = -1.4471580313422192 ==> log prob of sentence so far: -66.74281536205282

4GRAM: t ye
ENGLISH: P(e|t y) = -0.4341718100120251 ==> log prob of sentence so far: -34.661717458409136
FRENCH: P(e|t y) = -1.6720978579357175 ==> log prob of sentence so far: -68.41491321998853

4GRAM:  yea
ENGLISH: P(a| ye) = -0.8465219576239357 ==> log prob of sentence so far: -35.50823941603307
FRENCH: P(a| ye) = -2.429752280002408 ==> log prob of sentence so far: -70.84466549999094

4GRAM: year
ENGLISH: P(r|yea) = -0.09177037335564533 ==> log prob of sentence so far: -35.600009789388714
FRENCH: P(r|yea) = -1.4471580313422192 ==> log prob of sentence so far: -72.29182353133315

According to the 4gram model, the sentence is in English
----------------
