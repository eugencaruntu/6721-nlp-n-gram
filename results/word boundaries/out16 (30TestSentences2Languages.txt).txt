They are bad hombres.

1GRAM MODEL:

1GRAM: t
ENGLISH: P(t) = -1.1163632803584618 ==> log prob of sentence so far: -1.1163632803584618
FRENCH: P(t) = -1.2502938249616902 ==> log prob of sentence so far: -1.2502938249616902

1GRAM: h
ENGLISH: P(h) = -1.2624138049742233 ==> log prob of sentence so far: -2.3787770853326853
FRENCH: P(h) = -2.189956781312205 ==> log prob of sentence so far: -3.4402506062738953

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -3.3709879459667818
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -4.291543497661289

1GRAM: y
ENGLISH: P(y) = -1.832736218169479 ==> log prob of sentence so far: -5.203724164136261
FRENCH: P(y) = -2.7565515045032223 ==> log prob of sentence so far: -7.048095002164511

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -5.967378374215204
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -7.80139377788718

1GRAM: a
ENGLISH: P(a) = -1.1689283236166432 ==> log prob of sentence so far: -7.136306697831847
FRENCH: P(a) = -1.1606801719704456 ==> log prob of sentence so far: -8.962073949857626

1GRAM: r
ENGLISH: P(r) = -1.3434125438862854 ==> log prob of sentence so far: -8.479719241718133
FRENCH: P(r) = -1.2683557159353935 ==> log prob of sentence so far: -10.23042966579302

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -9.471930102352228
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -11.081722557180413

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -10.23558431243117
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -11.83502133290308

1GRAM: b
ENGLISH: P(b) = -1.834091631326008 ==> log prob of sentence so far: -12.069675943757177
FRENCH: P(b) = -2.1362152530478733 ==> log prob of sentence so far: -13.971236585950955

1GRAM: a
ENGLISH: P(a) = -1.1689283236166432 ==> log prob of sentence so far: -13.238604267373821
FRENCH: P(a) = -1.1606801719704456 ==> log prob of sentence so far: -15.1319167579214

1GRAM: d
ENGLISH: P(d) = -1.478319560115872 ==> log prob of sentence so far: -14.716923827489694
FRENCH: P(d) = -1.5045887548541879 ==> log prob of sentence so far: -16.63650551277559

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -15.480578037568636
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -17.389804288498258

1GRAM: h
ENGLISH: P(h) = -1.2624138049742233 ==> log prob of sentence so far: -16.74299184254286
FRENCH: P(h) = -2.189956781312205 ==> log prob of sentence so far: -19.579761069810463

1GRAM: o
ENGLISH: P(o) = -1.2199234031596837 ==> log prob of sentence so far: -17.962915245702543
FRENCH: P(o) = -1.3586704071825915 ==> log prob of sentence so far: -20.938431476993053

1GRAM: m
ENGLISH: P(m) = -1.6932501255573253 ==> log prob of sentence so far: -19.656165371259867
FRENCH: P(m) = -1.597275682203487 ==> log prob of sentence so far: -22.53570715919654

1GRAM: b
ENGLISH: P(b) = -1.834091631326008 ==> log prob of sentence so far: -21.490257002585874
FRENCH: P(b) = -2.1362152530478733 ==> log prob of sentence so far: -24.671922412244413

1GRAM: r
ENGLISH: P(r) = -1.3434125438862854 ==> log prob of sentence so far: -22.83366954647216
FRENCH: P(r) = -1.2683557159353935 ==> log prob of sentence so far: -25.940278128179806

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -23.825880407106254
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -26.7915710195672

1GRAM: s
ENGLISH: P(s) = -1.2532806553027906 ==> log prob of sentence so far: -25.079161062409046
FRENCH: P(s) = -1.1487747917545927 ==> log prob of sentence so far: -27.940345811321794

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: th
ENGLISH: P(h|t) = -0.43709402370664463 ==> log prob of sentence so far: -0.43709402370664463
FRENCH: P(h|t) = -2.3749883004661787 ==> log prob of sentence so far: -2.3749883004661787

2GRAM: he
ENGLISH: P(e|h) = -0.37152697512569327 ==> log prob of sentence so far: -0.8086209988323378
FRENCH: P(e|h) = -0.4600005188050297 ==> log prob of sentence so far: -2.8349888192712083

2GRAM: ey
ENGLISH: P(y|e) = -1.9527620982847427 ==> log prob of sentence so far: -2.7613830971170805
FRENCH: P(y|e) = -3.572500961144598 ==> log prob of sentence so far: -6.407489780415807

2GRAM: y 
ENGLISH: P( |y) = -0.16273103710113346 ==> log prob of sentence so far: -2.9241141342182138
FRENCH: P( |y) = -0.7165588009590896 ==> log prob of sentence so far: -7.124048581374896

2GRAM:  a
ENGLISH: P(a| ) = -0.9570855839437185 ==> log prob of sentence so far: -3.881199718161932
FRENCH: P(a| ) = -1.1850664635863264 ==> log prob of sentence so far: -8.309115044961223

2GRAM: ar
ENGLISH: P(r|a) = -0.9946330495604285 ==> log prob of sentence so far: -4.875832767722361
FRENCH: P(r|a) = -1.0579882107737122 ==> log prob of sentence so far: -9.367103255734936

2GRAM: re
ENGLISH: P(e|r) = -0.6381624788810675 ==> log prob of sentence so far: -5.513995246603429
FRENCH: P(e|r) = -0.5077793986557104 ==> log prob of sentence so far: -9.874882654390646

2GRAM: e 
ENGLISH: P( |e) = -0.4805818326341819 ==> log prob of sentence so far: -5.994577079237611
FRENCH: P( |e) = -0.45464594686088583 ==> log prob of sentence so far: -10.329528601251532

2GRAM:  b
ENGLISH: P(b| ) = -1.3041858497871541 ==> log prob of sentence so far: -7.298762929024765
FRENCH: P(b| ) = -1.7871941811494207 ==> log prob of sentence so far: -12.116722782400952

2GRAM: ba
ENGLISH: P(a|b) = -1.2435593141026775 ==> log prob of sentence so far: -8.542322243127442
FRENCH: P(a|b) = -0.8272917839934996 ==> log prob of sentence so far: -12.944014566394452

2GRAM: ad
ENGLISH: P(d|a) = -1.3872988549199865 ==> log prob of sentence so far: -9.929621098047429
FRENCH: P(d|a) = -1.9201554589718086 ==> log prob of sentence so far: -14.864170025366262

2GRAM: d 
ENGLISH: P( |d) = -0.23939150572394285 ==> log prob of sentence so far: -10.169012603771371
FRENCH: P( |d) = -1.1815706002329907 ==> log prob of sentence so far: -16.04574062559925

2GRAM:  h
ENGLISH: P(h| ) = -1.2041385116702426 ==> log prob of sentence so far: -11.373151115441614
FRENCH: P(h| ) = -2.0130348028652394 ==> log prob of sentence so far: -18.05877542846449

2GRAM: ho
ENGLISH: P(o|h) = -1.1381460769997287 ==> log prob of sentence so far: -12.511297192441342
FRENCH: P(o|h) = -0.8349317278314232 ==> log prob of sentence so far: -18.893707156295914

2GRAM: om
ENGLISH: P(m|o) = -1.2349296768941522 ==> log prob of sentence so far: -13.746226869335494
FRENCH: P(m|o) = -1.1522011039864875 ==> log prob of sentence so far: -20.045908260282403

2GRAM: mb
ENGLISH: P(b|m) = -1.6820612747306127 ==> log prob of sentence so far: -15.428288144066107
FRENCH: P(b|m) = -1.4620512476366716 ==> log prob of sentence so far: -21.507959507919075

2GRAM: br
ENGLISH: P(r|b) = -1.2485460487464923 ==> log prob of sentence so far: -16.6768341928126
FRENCH: P(r|b) = -0.7697438252792935 ==> log prob of sentence so far: -22.277703333198367

2GRAM: re
ENGLISH: P(e|r) = -0.6381624788810675 ==> log prob of sentence so far: -17.314996671693667
FRENCH: P(e|r) = -0.5077793986557104 ==> log prob of sentence so far: -22.785482731854078

2GRAM: es
ENGLISH: P(s|e) = -1.1166581324228657 ==> log prob of sentence so far: -18.431654804116533
FRENCH: P(s|e) = -0.7950741387552865 ==> log prob of sentence so far: -23.580556870609364

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: the
ENGLISH: P(e|th) = -0.19703627885635827 ==> log prob of sentence so far: -0.19703627885635827
FRENCH: P(e|th) = -0.30619408126398834 ==> log prob of sentence so far: -0.30619408126398834

3GRAM: hey
ENGLISH: P(y|he) = -1.5654852118770406 ==> log prob of sentence so far: -1.7625214907333988
FRENCH: P(y|he) = -3.576916955965207 ==> log prob of sentence so far: -3.8831110372291957

3GRAM: ey 
ENGLISH: P( |ey) = -0.20178457753912984 ==> log prob of sentence so far: -1.9643060682725286
FRENCH: P( |ey) = -0.7732987475892316 ==> log prob of sentence so far: -4.656409784818427

3GRAM: y a
ENGLISH: P(a|y ) = -0.9918481656477878 ==> log prob of sentence so far: -2.9561542339203166
FRENCH: P(a|y ) = -0.4904170221215953 ==> log prob of sentence so far: -5.146826806940022

3GRAM:  ar
ENGLISH: P(r| a) = -1.347595273441145 ==> log prob of sentence so far: -4.303749507361461
FRENCH: P(r| a) = -1.3193869293780283 ==> log prob of sentence so far: -6.466213736318051

3GRAM: are
ENGLISH: P(e|ar) = -0.8731583180919611 ==> log prob of sentence so far: -5.176907825453422
FRENCH: P(e|ar) = -1.1891608915643406 ==> log prob of sentence so far: -7.6553746278823915

3GRAM: re 
ENGLISH: P( |re) = -0.43929938301734384 ==> log prob of sentence so far: -5.616207208470766
FRENCH: P( |re) = -0.4674585123211922 ==> log prob of sentence so far: -8.122833140203584

3GRAM: e b
ENGLISH: P(b|e ) = -1.2876032591336253 ==> log prob of sentence so far: -6.903810467604392
FRENCH: P(b|e ) = -1.7936888900562977 ==> log prob of sentence so far: -9.916522030259882

3GRAM:  ba
ENGLISH: P(a| b) = -1.231942627001738 ==> log prob of sentence so far: -8.13575309460613
FRENCH: P(a| b) = -0.5941384254877041 ==> log prob of sentence so far: -10.510660455747587

3GRAM: bad
ENGLISH: P(d|ba) = -1.4156409798961542 ==> log prob of sentence so far: -9.551394074502284
FRENCH: P(d|ba) = -2.570776368794748 ==> log prob of sentence so far: -13.081436824542335

3GRAM: ad 
ENGLISH: P( |ad) = -0.3028209032818621 ==> log prob of sentence so far: -9.854214977784146
FRENCH: P( |ad) = -2.3081068600700276 ==> log prob of sentence so far: -15.389543684612363

3GRAM: d h
ENGLISH: P(h|d ) = -1.1997464895890706 ==> log prob of sentence so far: -11.053961467373217
FRENCH: P(h|d ) = -2.5791481985782263 ==> log prob of sentence so far: -17.96869188319059

3GRAM:  ho
ENGLISH: P(o| h) = -1.0225065996684566 ==> log prob of sentence so far: -12.076468067041674
FRENCH: P(o| h) = -0.6712707017163335 ==> log prob of sentence so far: -18.639962584906925

3GRAM: hom
ENGLISH: P(m|ho) = -1.483227006386342 ==> log prob of sentence so far: -13.559695073428015
FRENCH: P(m|ho) = -0.5199379896297536 ==> log prob of sentence so far: -19.159900574536678

3GRAM: omb
ENGLISH: P(b|om) = -1.9375399351846392 ==> log prob of sentence so far: -15.497235008612655
FRENCH: P(b|om) = -0.9310465146844649 ==> log prob of sentence so far: -20.090947089221142

3GRAM: mbr
ENGLISH: P(r|mb) = -1.1887722908147702 ==> log prob of sentence so far: -16.686007299427427
FRENCH: P(r|mb) = -0.3843560286272068 ==> log prob of sentence so far: -20.475303117848348

3GRAM: bre
ENGLISH: P(e|br) = -0.5947007816564263 ==> log prob of sentence so far: -17.28070808108385
FRENCH: P(e|br) = -0.3756892096280154 ==> log prob of sentence so far: -20.850992327476362

3GRAM: res
ENGLISH: P(s|re) = -0.9843893280348494 ==> log prob of sentence so far: -18.265097409118702
FRENCH: P(s|re) = -0.6483310433228557 ==> log prob of sentence so far: -21.499323370799218

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: they
ENGLISH: P(y|the) = -1.4400073147800023 ==> log prob of sentence so far: -1.4400073147800023
FRENCH: P(y|the) = -2.3710678622717363 ==> log prob of sentence so far: -2.3710678622717363

4GRAM: hey 
ENGLISH: P( |hey) = -0.013203569163388489 ==> log prob of sentence so far: -1.4532108839433908
FRENCH: P( |hey) = -1.4471580313422192 ==> log prob of sentence so far: -3.8182258936139553

4GRAM: ey a
ENGLISH: P(a|ey ) = -0.8846065812979305 ==> log prob of sentence so far: -2.3378174652413213
FRENCH: P(a|ey ) = -1.135662602000073 ==> log prob of sentence so far: -4.953888495614028

4GRAM: y ar
ENGLISH: P(r|y a) = -0.9982436955003133 ==> log prob of sentence so far: -3.3360611607416346
FRENCH: P(r|y a) = -1.4953460748258616 ==> log prob of sentence so far: -6.44923457043989

4GRAM:  are
ENGLISH: P(e| ar) = -0.23202740775673347 ==> log prob of sentence so far: -3.568088568498368
FRENCH: P(e| ar) = -2.132168172413036 ==> log prob of sentence so far: -8.581402742852926

4GRAM: are 
ENGLISH: P( |are) = -0.16663907359068666 ==> log prob of sentence so far: -3.7347276420890547
FRENCH: P( |are) = -0.8741057195916915 ==> log prob of sentence so far: -9.455508462444618

4GRAM: re b
ENGLISH: P(b|re ) = -1.3772456973383442 ==> log prob of sentence so far: -5.111973339427399
FRENCH: P(b|re ) = -2.0314316817268057 ==> log prob of sentence so far: -11.486940144171424

4GRAM: e ba
ENGLISH: P(a|e b) = -1.099083022197947 ==> log prob of sentence so far: -6.211056361625346
FRENCH: P(a|e b) = -0.4508492166828717 ==> log prob of sentence so far: -11.937789360854296

4GRAM:  bad
ENGLISH: P(d| ba) = -1.3501748493538022 ==> log prob of sentence so far: -7.561231210979148
FRENCH: P(d| ba) = -3.0993352776859577 ==> log prob of sentence so far: -15.037124638540254

4GRAM: bad 
ENGLISH: P( |bad) = -0.5153256073898916 ==> log prob of sentence so far: -8.07655681836904
FRENCH: P( |bad) = -1.0142404391146103 ==> log prob of sentence so far: -16.051365077654864

4GRAM: ad h
ENGLISH: P(h|ad ) = -1.3931113087047342 ==> log prob of sentence so far: -9.469668127073774
FRENCH: P(h|ad ) = -1.041392685158225 ==> log prob of sentence so far: -17.09275776281309

4GRAM: d ho
ENGLISH: P(o|d h) = -0.9578263552087697 ==> log prob of sentence so far: -10.427494482282544
FRENCH: P(o|d h) = -0.8450980400142568 ==> log prob of sentence so far: -17.937855802827347

4GRAM:  hom
ENGLISH: P(m| ho) = -1.2172115738807998 ==> log prob of sentence so far: -11.644706056163344
FRENCH: P(m| ho) = -0.24935082761315946 ==> log prob of sentence so far: -18.187206630440507

4GRAM: homb
ENGLISH: P(b|hom) = -2.503790683057181 ==> log prob of sentence so far: -14.148496739220525
FRENCH: P(b|hom) = -2.0128372247051725 ==> log prob of sentence so far: -20.20004385514568

4GRAM: ombr
ENGLISH: P(r|omb) = -2.05307844348342 ==> log prob of sentence so far: -16.201575182703944
FRENCH: P(r|omb) = -0.2765095314640118 ==> log prob of sentence so far: -20.47655338660969

4GRAM: mbre
ENGLISH: P(e|mbr) = -0.9951474972055879 ==> log prob of sentence so far: -17.19672267990953
FRENCH: P(e|mbr) = -0.09472042983027956 ==> log prob of sentence so far: -20.57127381643997

4GRAM: bres
ENGLISH: P(s|bre) = -1.4347369421411136 ==> log prob of sentence so far: -18.631459622050645
FRENCH: P(s|bre) = -0.5481846105451078 ==> log prob of sentence so far: -21.119458426985076

According to the 4gram model, the sentence is in English
----------------
