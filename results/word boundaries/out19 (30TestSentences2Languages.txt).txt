John aime Brook.

1GRAM MODEL:

1GRAM: j
ENGLISH: P(j) = -3.0269729668933083 ==> log prob of sentence so far: -3.0269729668933083
FRENCH: P(j) = -2.2939047450657273 ==> log prob of sentence so far: -2.2939047450657273

1GRAM: o
ENGLISH: P(o) = -1.2199234031596837 ==> log prob of sentence so far: -4.246896370052992
FRENCH: P(o) = -1.3586704071825915 ==> log prob of sentence so far: -3.6525751522483185

1GRAM: h
ENGLISH: P(h) = -1.2624138049742233 ==> log prob of sentence so far: -5.509310175027215
FRENCH: P(h) = -2.189956781312205 ==> log prob of sentence so far: -5.842531933560524

1GRAM: n
ENGLISH: P(n) = -1.2437365799768185 ==> log prob of sentence so far: -6.753046755004034
FRENCH: P(n) = -1.2069645534794413 ==> log prob of sentence so far: -7.049496487039965

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -7.516700965082976
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -7.802795262762633

1GRAM: a
ENGLISH: P(a) = -1.1689283236166432 ==> log prob of sentence so far: -8.68562928869962
FRENCH: P(a) = -1.1606801719704456 ==> log prob of sentence so far: -8.963475434733079

1GRAM: i
ENGLISH: P(i) = -1.2446651228481003 ==> log prob of sentence so far: -9.93029441154772
FRENCH: P(i) = -1.21953351953378 ==> log prob of sentence so far: -10.183008954266858

1GRAM: m
ENGLISH: P(m) = -1.6932501255573253 ==> log prob of sentence so far: -11.623544537105046
FRENCH: P(m) = -1.597275682203487 ==> log prob of sentence so far: -11.780284636470345

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -12.615755397739141
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -12.631577527857738

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -13.379409607818083
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -13.384876303580405

1GRAM: b
ENGLISH: P(b) = -1.834091631326008 ==> log prob of sentence so far: -15.21350123914409
FRENCH: P(b) = -2.1362152530478733 ==> log prob of sentence so far: -15.521091556628278

1GRAM: r
ENGLISH: P(r) = -1.3434125438862854 ==> log prob of sentence so far: -16.556913783030375
FRENCH: P(r) = -1.2683557159353935 ==> log prob of sentence so far: -16.78944727256367

1GRAM: o
ENGLISH: P(o) = -1.2199234031596837 ==> log prob of sentence so far: -17.776837186190058
FRENCH: P(o) = -1.3586704071825915 ==> log prob of sentence so far: -18.14811767974626

1GRAM: o
ENGLISH: P(o) = -1.2199234031596837 ==> log prob of sentence so far: -18.99676058934974
FRENCH: P(o) = -1.3586704071825915 ==> log prob of sentence so far: -19.50678808692885

1GRAM: k
ENGLISH: P(k) = -2.1548367961831083 ==> log prob of sentence so far: -21.15159738553285
FRENCH: P(k) = -3.620921230913911 ==> log prob of sentence so far: -23.12770931784276

According to the 1gram model, the sentence is in English
----------------
2GRAM MODEL:

2GRAM: jo
ENGLISH: P(o|j) = -0.5263787425615649 ==> log prob of sentence so far: -0.5263787425615649
FRENCH: P(o|j) = -0.8910636000847345 ==> log prob of sentence so far: -0.8910636000847345

2GRAM: oh
ENGLISH: P(h|o) = -2.535042699455585 ==> log prob of sentence so far: -3.0614214420171497
FRENCH: P(h|o) = -3.4048867767824587 ==> log prob of sentence so far: -4.295950376867193

2GRAM: hn
ENGLISH: P(n|h) = -3.0856369028246995 ==> log prob of sentence so far: -6.147058344841849
FRENCH: P(n|h) = -2.99575481847433 ==> log prob of sentence so far: -7.291705195341523

2GRAM: n 
ENGLISH: P( |n) = -0.6289289135021393 ==> log prob of sentence so far: -6.775987258343988
FRENCH: P( |n) = -0.7920247282158231 ==> log prob of sentence so far: -8.083729923557346

2GRAM:  a
ENGLISH: P(a| ) = -0.9570855839437185 ==> log prob of sentence so far: -7.733072842287706
FRENCH: P(a| ) = -1.1850664635863264 ==> log prob of sentence so far: -9.268796387143672

2GRAM: ai
ENGLISH: P(i|a) = -1.3535171865585782 ==> log prob of sentence so far: -9.086590028846285
FRENCH: P(i|a) = -0.6982383219083534 ==> log prob of sentence so far: -9.967034709052026

2GRAM: im
ENGLISH: P(m|i) = -1.3632696253971748 ==> log prob of sentence so far: -10.44985965424346
FRENCH: P(m|i) = -1.6317840147190457 ==> log prob of sentence so far: -11.598818723771071

2GRAM: me
ENGLISH: P(e|m) = -0.5972185796005993 ==> log prob of sentence so far: -11.047078233844058
FRENCH: P(e|m) = -0.43132397580431553 ==> log prob of sentence so far: -12.030142699575388

2GRAM: e 
ENGLISH: P( |e) = -0.4805818326341819 ==> log prob of sentence so far: -11.527660066478239
FRENCH: P( |e) = -0.45464594686088583 ==> log prob of sentence so far: -12.484788646436273

2GRAM:  b
ENGLISH: P(b| ) = -1.3041858497871541 ==> log prob of sentence so far: -12.831845916265394
FRENCH: P(b| ) = -1.7871941811494207 ==> log prob of sentence so far: -14.271982827585694

2GRAM: br
ENGLISH: P(r|b) = -1.2485460487464923 ==> log prob of sentence so far: -14.080391965011886
FRENCH: P(r|b) = -0.7697438252792935 ==> log prob of sentence so far: -15.041726652864988

2GRAM: ro
ENGLISH: P(o|r) = -1.0281569390125158 ==> log prob of sentence so far: -15.108548904024403
FRENCH: P(o|r) = -1.1244692090918036 ==> log prob of sentence so far: -16.16619586195679

2GRAM: oo
ENGLISH: P(o|o) = -1.3914623055871775 ==> log prob of sentence so far: -16.50001120961158
FRENCH: P(o|o) = -2.821961795894757 ==> log prob of sentence so far: -18.98815765785155

2GRAM: ok
ENGLISH: P(k|o) = -1.876711677008586 ==> log prob of sentence so far: -18.376722886620165
FRENCH: P(k|o) = -3.4048867767824587 ==> log prob of sentence so far: -22.393044434634007

According to the 2gram model, the sentence is in English
----------------
3GRAM MODEL:

3GRAM: joh
ENGLISH: P(h|jo) = -1.242082504886286 ==> log prob of sentence so far: -1.242082504886286
FRENCH: P(h|jo) = -2.353723937588949 ==> log prob of sentence so far: -2.353723937588949

3GRAM: ohn
ENGLISH: P(n|oh) = -1.0185298021987208 ==> log prob of sentence so far: -2.260612307085007
FRENCH: P(n|oh) = -1.041392685158225 ==> log prob of sentence so far: -3.395116622747174

3GRAM: hn 
ENGLISH: P( |hn) = -0.6724399567969696 ==> log prob of sentence so far: -2.9330522638819763
FRENCH: P( |hn) = -0.8692317197309761 ==> log prob of sentence so far: -4.26434834247815

3GRAM: n a
ENGLISH: P(a|n ) = -0.8884039672384039 ==> log prob of sentence so far: -3.82145623112038
FRENCH: P(a|n ) = -1.1134758662747186 ==> log prob of sentence so far: -5.377824208752869

3GRAM:  ai
ENGLISH: P(i| a) = -2.1109333137142197 ==> log prob of sentence so far: -5.932389544834599
FRENCH: P(i| a) = -1.5234128172482089 ==> log prob of sentence so far: -6.901237026001078

3GRAM: aim
ENGLISH: P(m|ai) = -1.9425417579503146 ==> log prob of sentence so far: -7.874931302784914
FRENCH: P(m|ai) = -2.2703527017761833 ==> log prob of sentence so far: -9.171589727777262

3GRAM: ime
ENGLISH: P(e|im) = -0.5959636966945008 ==> log prob of sentence so far: -8.470894999479414
FRENCH: P(e|im) = -0.48481781405783136 ==> log prob of sentence so far: -9.656407541835092

3GRAM: me 
ENGLISH: P( |me) = -0.4001366105597906 ==> log prob of sentence so far: -8.871031610039205
FRENCH: P( |me) = -0.5753788826686089 ==> log prob of sentence so far: -10.231786424503701

3GRAM: e b
ENGLISH: P(b|e ) = -1.2876032591336253 ==> log prob of sentence so far: -10.15863486917283
FRENCH: P(b|e ) = -1.7936888900562977 ==> log prob of sentence so far: -12.02547531456

3GRAM:  br
ENGLISH: P(r| b) = -1.149098522499213 ==> log prob of sentence so far: -11.307733391672043
FRENCH: P(r| b) = -0.7679425298690954 ==> log prob of sentence so far: -12.793417844429095

3GRAM: bro
ENGLISH: P(o|br) = -0.5399270786056658 ==> log prob of sentence so far: -11.84766047027771
FRENCH: P(o|br) = -1.5136273118737062 ==> log prob of sentence so far: -14.307045156302802

3GRAM: roo
ENGLISH: P(o|ro) = -1.545329612359844 ==> log prob of sentence so far: -13.392990082637553
FRENCH: P(o|ro) = -2.8784579615212427 ==> log prob of sentence so far: -17.185503117824044

3GRAM: ook
ENGLISH: P(k|oo) = -0.6364078673494847 ==> log prob of sentence so far: -14.029397949987038
FRENCH: P(k|oo) = -0.7053568029974194 ==> log prob of sentence so far: -17.890859920821462

According to the 3gram model, the sentence is in English
----------------
4GRAM MODEL:

4GRAM: john
ENGLISH: P(n|joh) = -0.2218487496163564 ==> log prob of sentence so far: -0.2218487496163564
FRENCH: P(n|joh) = -0.7923916894982539 ==> log prob of sentence so far: -0.7923916894982539

4GRAM: ohn 
ENGLISH: P( |ohn) = -0.36797678529459443 ==> log prob of sentence so far: -0.5898255349109508
FRENCH: P( |ohn) = -0.7923916894982539 ==> log prob of sentence so far: -1.5847833789965078

4GRAM: hn a
ENGLISH: P(a|hn ) = -1.7242758696007892 ==> log prob of sentence so far: -2.31410140451174
FRENCH: P(a|hn ) = -1.4913616938342726 ==> log prob of sentence so far: -3.0761450728307804

4GRAM: n ai
ENGLISH: P(i|n a) = -2.1209141540841214 ==> log prob of sentence so far: -4.435015558595861
FRENCH: P(i|n a) = -1.2794156353522241 ==> log prob of sentence so far: -4.355560708183004

4GRAM:  aim
ENGLISH: P(m| ai) = -1.5137017634200942 ==> log prob of sentence so far: -5.9487173220159555
FRENCH: P(m| ai) = -1.2106727479357444 ==> log prob of sentence so far: -5.5662334561187485

4GRAM: aime
ENGLISH: P(e|aim) = -0.3490914411342206 ==> log prob of sentence so far: -6.297808763150176
FRENCH: P(e|aim) = -0.4173114127397826 ==> log prob of sentence so far: -5.983544868858531

4GRAM: ime 
ENGLISH: P( |ime) = -0.28224848760258464 ==> log prob of sentence so far: -6.580057250752761
FRENCH: P( |ime) = -0.8753317675934713 ==> log prob of sentence so far: -6.858876636452003

4GRAM: me b
ENGLISH: P(b|me ) = -1.5083601436589482 ==> log prob of sentence so far: -8.088417394411708
FRENCH: P(b|me ) = -2.190953896674857 ==> log prob of sentence so far: -9.04983053312686

4GRAM: e br
ENGLISH: P(r|e b) = -1.1104007827224796 ==> log prob of sentence so far: -9.198818177134187
FRENCH: P(r|e b) = -0.7437613221246735 ==> log prob of sentence so far: -9.793591855251533

4GRAM:  bro
ENGLISH: P(o| br) = -0.5072796227788448 ==> log prob of sentence so far: -9.706097799913032
FRENCH: P(o| br) = -1.1895668705903442 ==> log prob of sentence so far: -10.983158725841877

4GRAM: broo
ENGLISH: P(o|bro) = -1.3343047906000267 ==> log prob of sentence so far: -11.040402590513057
FRENCH: P(o|bro) = -1.0047988828817687 ==> log prob of sentence so far: -11.987957608723645

4GRAM: rook
ENGLISH: P(k|roo) = -1.0161684976948873 ==> log prob of sentence so far: -12.056571088207944
FRENCH: P(k|roo) = -0.5898255349109508 ==> log prob of sentence so far: -12.577783143634596

According to the 4gram model, the sentence is in English
----------------
