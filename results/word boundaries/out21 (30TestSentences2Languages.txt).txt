NTSB exhorte l'installation d'altimetres radar.

1GRAM MODEL:

1GRAM: n
ENGLISH: P(n) = -1.2437365799768185 ==> log prob of sentence so far: -1.2437365799768185
FRENCH: P(n) = -1.2069645534794413 ==> log prob of sentence so far: -1.2069645534794413

1GRAM: t
ENGLISH: P(t) = -1.1163632803584618 ==> log prob of sentence so far: -2.3600998603352803
FRENCH: P(t) = -1.2502938249616902 ==> log prob of sentence so far: -2.4572583784411313

1GRAM: s
ENGLISH: P(s) = -1.2532806553027906 ==> log prob of sentence so far: -3.613380515638071
FRENCH: P(s) = -1.1487747917545927 ==> log prob of sentence so far: -3.6060331701957242

1GRAM: b
ENGLISH: P(b) = -1.834091631326008 ==> log prob of sentence so far: -5.447472146964079
FRENCH: P(b) = -2.1362152530478733 ==> log prob of sentence so far: -5.7422484232435975

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -6.211126357043022
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -6.495547198966266

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -7.203337217677118
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -7.346840090353659

1GRAM: x
ENGLISH: P(x) = -3.043662999845278 ==> log prob of sentence so far: -10.247000217522396
FRENCH: P(x) = -2.4229740656245635 ==> log prob of sentence so far: -9.769814155978223

1GRAM: h
ENGLISH: P(h) = -1.2624138049742233 ==> log prob of sentence so far: -11.50941402249662
FRENCH: P(h) = -2.189956781312205 ==> log prob of sentence so far: -11.959770937290427

1GRAM: o
ENGLISH: P(o) = -1.2199234031596837 ==> log prob of sentence so far: -12.729337425656302
FRENCH: P(o) = -1.3586704071825915 ==> log prob of sentence so far: -13.318441344473019

1GRAM: r
ENGLISH: P(r) = -1.3434125438862854 ==> log prob of sentence so far: -14.072749969542588
FRENCH: P(r) = -1.2683557159353935 ==> log prob of sentence so far: -14.586797060408413

1GRAM: t
ENGLISH: P(t) = -1.1163632803584618 ==> log prob of sentence so far: -15.18911324990105
FRENCH: P(t) = -1.2502938249616902 ==> log prob of sentence so far: -15.837090885370102

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -16.181324110535147
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -16.688383776757497

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -16.94497832061409
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -17.441682552480167

1GRAM: l
ENGLISH: P(l) = -1.4299085279951902 ==> log prob of sentence so far: -18.374886848609282
FRENCH: P(l) = -1.3525395571504588 ==> log prob of sentence so far: -18.794222109630624

1GRAM: i
ENGLISH: P(i) = -1.2446651228481003 ==> log prob of sentence so far: -19.619551971457383
FRENCH: P(i) = -1.21953351953378 ==> log prob of sentence so far: -20.013755629164404

1GRAM: n
ENGLISH: P(n) = -1.2437365799768185 ==> log prob of sentence so far: -20.863288551434202
FRENCH: P(n) = -1.2069645534794413 ==> log prob of sentence so far: -21.220720182643845

1GRAM: s
ENGLISH: P(s) = -1.2532806553027906 ==> log prob of sentence so far: -22.116569206736994
FRENCH: P(s) = -1.1487747917545927 ==> log prob of sentence so far: -22.36949497439844

1GRAM: t
ENGLISH: P(t) = -1.1163632803584618 ==> log prob of sentence so far: -23.232932487095457
FRENCH: P(t) = -1.2502938249616902 ==> log prob of sentence so far: -23.61978879936013

1GRAM: a
ENGLISH: P(a) = -1.1689283236166432 ==> log prob of sentence so far: -24.4018608107121
FRENCH: P(a) = -1.1606801719704456 ==> log prob of sentence so far: -24.780468971330574

1GRAM: l
ENGLISH: P(l) = -1.4299085279951902 ==> log prob of sentence so far: -25.831769338707293
FRENCH: P(l) = -1.3525395571504588 ==> log prob of sentence so far: -26.13300852848103

1GRAM: l
ENGLISH: P(l) = -1.4299085279951902 ==> log prob of sentence so far: -27.261677866702485
FRENCH: P(l) = -1.3525395571504588 ==> log prob of sentence so far: -27.48554808563149

1GRAM: a
ENGLISH: P(a) = -1.1689283236166432 ==> log prob of sentence so far: -28.43060619031913
FRENCH: P(a) = -1.1606801719704456 ==> log prob of sentence so far: -28.646228257601933

1GRAM: t
ENGLISH: P(t) = -1.1163632803584618 ==> log prob of sentence so far: -29.546969470677592
FRENCH: P(t) = -1.2502938249616902 ==> log prob of sentence so far: -29.896522082563624

1GRAM: i
ENGLISH: P(i) = -1.2446651228481003 ==> log prob of sentence so far: -30.791634593525693
FRENCH: P(i) = -1.21953351953378 ==> log prob of sentence so far: -31.116055602097404

1GRAM: o
ENGLISH: P(o) = -1.2199234031596837 ==> log prob of sentence so far: -32.011557996685376
FRENCH: P(o) = -1.3586704071825915 ==> log prob of sentence so far: -32.47472600928

1GRAM: n
ENGLISH: P(n) = -1.2437365799768185 ==> log prob of sentence so far: -33.255294576662195
FRENCH: P(n) = -1.2069645534794413 ==> log prob of sentence so far: -33.68169056275944

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -34.018948786741134
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -34.43498933848211

1GRAM: d
ENGLISH: P(d) = -1.478319560115872 ==> log prob of sentence so far: -35.497268346857005
FRENCH: P(d) = -1.5045887548541879 ==> log prob of sentence so far: -35.9395780933363

1GRAM: a
ENGLISH: P(a) = -1.1689283236166432 ==> log prob of sentence so far: -36.666196670473646
FRENCH: P(a) = -1.1606801719704456 ==> log prob of sentence so far: -37.10025826530675

1GRAM: l
ENGLISH: P(l) = -1.4299085279951902 ==> log prob of sentence so far: -38.096105198468834
FRENCH: P(l) = -1.3525395571504588 ==> log prob of sentence so far: -38.45279782245721

1GRAM: t
ENGLISH: P(t) = -1.1163632803584618 ==> log prob of sentence so far: -39.2124684788273
FRENCH: P(t) = -1.2502938249616902 ==> log prob of sentence so far: -39.703091647418894

1GRAM: i
ENGLISH: P(i) = -1.2446651228481003 ==> log prob of sentence so far: -40.457133601675395
FRENCH: P(i) = -1.21953351953378 ==> log prob of sentence so far: -40.92262516695268

1GRAM: m
ENGLISH: P(m) = -1.6932501255573253 ==> log prob of sentence so far: -42.15038372723272
FRENCH: P(m) = -1.597275682203487 ==> log prob of sentence so far: -42.519900849156166

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -43.14259458786682
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -43.37119374054356

1GRAM: t
ENGLISH: P(t) = -1.1163632803584618 ==> log prob of sentence so far: -44.25895786822528
FRENCH: P(t) = -1.2502938249616902 ==> log prob of sentence so far: -44.62148756550525

1GRAM: r
ENGLISH: P(r) = -1.3434125438862854 ==> log prob of sentence so far: -45.60237041211157
FRENCH: P(r) = -1.2683557159353935 ==> log prob of sentence so far: -45.88984328144064

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -46.594581272745664
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -46.741136172828035

1GRAM: s
ENGLISH: P(s) = -1.2532806553027906 ==> log prob of sentence so far: -47.84786192804845
FRENCH: P(s) = -1.1487747917545927 ==> log prob of sentence so far: -47.889910964582626

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -48.61151613812739
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -48.643209740305295

1GRAM: r
ENGLISH: P(r) = -1.3434125438862854 ==> log prob of sentence so far: -49.95492868201368
FRENCH: P(r) = -1.2683557159353935 ==> log prob of sentence so far: -49.91156545624069

1GRAM: a
ENGLISH: P(a) = -1.1689283236166432 ==> log prob of sentence so far: -51.12385700563032
FRENCH: P(a) = -1.1606801719704456 ==> log prob of sentence so far: -51.072245628211135

1GRAM: d
ENGLISH: P(d) = -1.478319560115872 ==> log prob of sentence so far: -52.60217656574619
FRENCH: P(d) = -1.5045887548541879 ==> log prob of sentence so far: -52.576834383065325

1GRAM: a
ENGLISH: P(a) = -1.1689283236166432 ==> log prob of sentence so far: -53.77110488936283
FRENCH: P(a) = -1.1606801719704456 ==> log prob of sentence so far: -53.73751455503577

1GRAM: r
ENGLISH: P(r) = -1.3434125438862854 ==> log prob of sentence so far: -55.11451743324912
FRENCH: P(r) = -1.2683557159353935 ==> log prob of sentence so far: -55.005870270971165

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: nt
ENGLISH: P(t|n) = -1.0490570841981695 ==> log prob of sentence so far: -1.0490570841981695
FRENCH: P(t|n) = -0.6508722063321496 ==> log prob of sentence so far: -0.6508722063321496

2GRAM: ts
ENGLISH: P(s|t) = -1.581136254665701 ==> log prob of sentence so far: -2.6301933388638705
FRENCH: P(s|t) = -1.5193913400823387 ==> log prob of sentence so far: -2.170263546414488

2GRAM: sb
ENGLISH: P(b|s) = -3.1753755663051146 ==> log prob of sentence so far: -5.805568905168985
FRENCH: P(b|s) = -3.8428550940302846 ==> log prob of sentence so far: -6.013118640444773

2GRAM: b 
ENGLISH: P( |b) = -1.3813833577297705 ==> log prob of sentence so far: -7.186952262898756
FRENCH: P( |b) = -2.3200758409394635 ==> log prob of sentence so far: -8.333194481384236

2GRAM:  e
ENGLISH: P(e| ) = -1.762612410696786 ==> log prob of sentence so far: -8.949564673595543
FRENCH: P(e| ) = -1.1704177062051893 ==> log prob of sentence so far: -9.503612187589425

2GRAM: ex
ENGLISH: P(x|e) = -2.1631235874872865 ==> log prob of sentence so far: -11.112688261082829
FRENCH: P(x|e) = -2.2683798885034747 ==> log prob of sentence so far: -11.7719920760929

2GRAM: xh
ENGLISH: P(h|x) = -1.4566155798598526 ==> log prob of sentence so far: -12.56930384094268
FRENCH: P(h|x) = -2.3971749769583255 ==> log prob of sentence so far: -14.169167053051225

2GRAM: ho
ENGLISH: P(o|h) = -1.1381460769997287 ==> log prob of sentence so far: -13.707449917942409
FRENCH: P(o|h) = -0.8349317278314232 ==> log prob of sentence so far: -15.004098780882648

2GRAM: or
ENGLISH: P(r|o) = -0.9387048637584691 ==> log prob of sentence so far: -14.646154781700877
FRENCH: P(r|o) = -1.0282469012931088 ==> log prob of sentence so far: -16.03234568217576

2GRAM: rt
ENGLISH: P(t|r) = -1.3279711164723658 ==> log prob of sentence so far: -15.974125898173243
FRENCH: P(t|r) = -1.4364633791795869 ==> log prob of sentence so far: -17.468809061355344

2GRAM: te
ENGLISH: P(e|t) = -1.0798658320225 ==> log prob of sentence so far: -17.053991730195744
FRENCH: P(e|t) = -0.7438666988991465 ==> log prob of sentence so far: -18.21267576025449

2GRAM: e 
ENGLISH: P( |e) = -0.4805818326341819 ==> log prob of sentence so far: -17.534573562829927
FRENCH: P( |e) = -0.45464594686088583 ==> log prob of sentence so far: -18.667321707115377

2GRAM:  l
ENGLISH: P(l| ) = -1.5795732932216122 ==> log prob of sentence so far: -19.11414685605154
FRENCH: P(l| ) = -0.9697405778171274 ==> log prob of sentence so far: -19.637062284932504

2GRAM: li
ENGLISH: P(i|l) = -0.9631912724114013 ==> log prob of sentence so far: -20.07733812846294
FRENCH: P(i|l) = -1.2145977924784346 ==> log prob of sentence so far: -20.85166007741094

2GRAM: in
ENGLISH: P(n|i) = -0.5190218609955932 ==> log prob of sentence so far: -20.596359989458534
FRENCH: P(n|i) = -0.9179525548054146 ==> log prob of sentence so far: -21.769612632216354

2GRAM: ns
ENGLISH: P(s|n) = -1.3738363647884215 ==> log prob of sentence so far: -21.970196354246955
FRENCH: P(s|n) = -0.9696823202801229 ==> log prob of sentence so far: -22.739294952496476

2GRAM: st
ENGLISH: P(t|s) = -0.8485217043425864 ==> log prob of sentence so far: -22.818718058589543
FRENCH: P(t|s) = -1.3538898557059649 ==> log prob of sentence so far: -24.09318480820244

2GRAM: ta
ENGLISH: P(a|t) = -1.3876640326966487 ==> log prob of sentence so far: -24.20638209128619
FRENCH: P(a|t) = -1.0416603352001712 ==> log prob of sentence so far: -25.13484514340261

2GRAM: al
ENGLISH: P(l|a) = -0.9816526684182028 ==> log prob of sentence so far: -25.188034759704394
FRENCH: P(l|a) = -1.3822147336029835 ==> log prob of sentence so far: -26.517059877005593

2GRAM: ll
ENGLISH: P(l|l) = -0.8462874532283543 ==> log prob of sentence so far: -26.034322212932747
FRENCH: P(l|l) = -1.1108399903349184 ==> log prob of sentence so far: -27.62789986734051

2GRAM: la
ENGLISH: P(a|l) = -1.0647950006268063 ==> log prob of sentence so far: -27.099117213559552
FRENCH: P(a|l) = -0.6901255142935863 ==> log prob of sentence so far: -28.318025381634097

2GRAM: at
ENGLISH: P(t|a) = -0.875598830073791 ==> log prob of sentence so far: -27.974716043633343
FRENCH: P(t|a) = -1.3177070457577422 ==> log prob of sentence so far: -29.63573242739184

2GRAM: ti
ENGLISH: P(i|t) = -1.2348409356175678 ==> log prob of sentence so far: -29.209556979250912
FRENCH: P(i|t) = -1.0337107934899412 ==> log prob of sentence so far: -30.66944322088178

2GRAM: io
ENGLISH: P(o|i) = -1.4900162804332482 ==> log prob of sentence so far: -30.69957325968416
FRENCH: P(o|i) = -1.3847021050977135 ==> log prob of sentence so far: -32.0541453259795

2GRAM: on
ENGLISH: P(n|o) = -0.8688546434110379 ==> log prob of sentence so far: -31.5684279030952
FRENCH: P(n|o) = -0.5227156749929506 ==> log prob of sentence so far: -32.57686100097245

2GRAM: n 
ENGLISH: P( |n) = -0.6289289135021393 ==> log prob of sentence so far: -32.197356816597335
FRENCH: P( |n) = -0.7920247282158231 ==> log prob of sentence so far: -33.36888572918827

2GRAM:  d
ENGLISH: P(d| ) = -1.5865842529099268 ==> log prob of sentence so far: -33.783941069507264
FRENCH: P(d| ) = -0.9158393035688763 ==> log prob of sentence so far: -34.284725032757144

2GRAM: da
ENGLISH: P(a|d) = -1.5027778019014362 ==> log prob of sentence so far: -35.2867188714087
FRENCH: P(a|d) = -0.9867411180106263 ==> log prob of sentence so far: -35.27146615076777

2GRAM: al
ENGLISH: P(l|a) = -0.9816526684182028 ==> log prob of sentence so far: -36.2683715398269
FRENCH: P(l|a) = -1.3822147336029835 ==> log prob of sentence so far: -36.65368088437076

2GRAM: lt
ENGLISH: P(t|l) = -1.913593758179087 ==> log prob of sentence so far: -38.18196529800599
FRENCH: P(t|l) = -2.4757411821973245 ==> log prob of sentence so far: -39.12942206656808

2GRAM: ti
ENGLISH: P(i|t) = -1.2348409356175678 ==> log prob of sentence so far: -39.41680623362356
FRENCH: P(i|t) = -1.0337107934899412 ==> log prob of sentence so far: -40.16313286005802

2GRAM: im
ENGLISH: P(m|i) = -1.3632696253971748 ==> log prob of sentence so far: -40.780075859020734
FRENCH: P(m|i) = -1.6317840147190457 ==> log prob of sentence so far: -41.794916874777066

2GRAM: me
ENGLISH: P(e|m) = -0.5972185796005993 ==> log prob of sentence so far: -41.377294438621334
FRENCH: P(e|m) = -0.43132397580431553 ==> log prob of sentence so far: -42.226240850581384

2GRAM: et
ENGLISH: P(t|e) = -1.5419699073166608 ==> log prob of sentence so far: -42.919264345938
FRENCH: P(t|e) = -1.1437235864613344 ==> log prob of sentence so far: -43.36996443704272

2GRAM: tr
ENGLISH: P(r|t) = -1.6024736230576526 ==> log prob of sentence so far: -44.52173796899565
FRENCH: P(r|t) = -1.0017148912212714 ==> log prob of sentence so far: -44.37167932826399

2GRAM: re
ENGLISH: P(e|r) = -0.6381624788810675 ==> log prob of sentence so far: -45.159900447876716
FRENCH: P(e|r) = -0.5077793986557104 ==> log prob of sentence so far: -44.879458726919694

2GRAM: es
ENGLISH: P(s|e) = -1.1166581324228657 ==> log prob of sentence so far: -46.27655858029958
FRENCH: P(s|e) = -0.7950741387552865 ==> log prob of sentence so far: -45.67453286567498

2GRAM: s 
ENGLISH: P( |s) = -0.40550908468327607 ==> log prob of sentence so far: -46.68206766498285
FRENCH: P( |s) = -0.2671715449815274 ==> log prob of sentence so far: -45.94170441065651

2GRAM:  r
ENGLISH: P(r| ) = -1.7712794700952248 ==> log prob of sentence so far: -48.453347135078076
FRENCH: P(r| ) = -1.5209827383429326 ==> log prob of sentence so far: -47.46268714899944

2GRAM: ra
ENGLISH: P(a|r) = -1.1658006465083335 ==> log prob of sentence so far: -49.619147781586406
FRENCH: P(a|r) = -0.9589043275660933 ==> log prob of sentence so far: -48.42159147656554

2GRAM: ad
ENGLISH: P(d|a) = -1.3872988549199865 ==> log prob of sentence so far: -51.00644663650639
FRENCH: P(d|a) = -1.9201554589718086 ==> log prob of sentence so far: -50.341746935537344

2GRAM: da
ENGLISH: P(a|d) = -1.5027778019014362 ==> log prob of sentence so far: -52.50922443840783
FRENCH: P(a|d) = -0.9867411180106263 ==> log prob of sentence so far: -51.32848805354797

2GRAM: ar
ENGLISH: P(r|a) = -0.9946330495604285 ==> log prob of sentence so far: -53.503857487968254
FRENCH: P(r|a) = -1.0579882107737122 ==> log prob of sentence so far: -52.386476264321686

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: nts
ENGLISH: P(s|nt) = -1.2347953951494302 ==> log prob of sentence so far: -1.2347953951494302
FRENCH: P(s|nt) = -1.243311608138322 ==> log prob of sentence so far: -1.243311608138322

3GRAM: tsb
ENGLISH: P(b|ts) = -2.9250025076809774 ==> log prob of sentence so far: -4.159797902830407
FRENCH: P(b|ts) = -3.44544851426605 ==> log prob of sentence so far: -4.688760122404372

3GRAM: sb 
ENGLISH: P( |sb) = -2.037426497940624 ==> log prob of sentence so far: -6.197224400771031
FRENCH: P( |sb) = -1.6334684555795866 ==> log prob of sentence so far: -6.322228577983958

3GRAM: b e
ENGLISH: P(e|b ) = -1.8762786278381414 ==> log prob of sentence so far: -8.073503028609172
FRENCH: P(e|b ) = -0.6989700043360187 ==> log prob of sentence so far: -7.021198582319977

3GRAM:  ex
ENGLISH: P(x| e) = -0.8168610848206604 ==> log prob of sentence so far: -8.890364113429833
FRENCH: P(x| e) = -1.3664175081449519 ==> log prob of sentence so far: -8.38761609046493

3GRAM: exh
ENGLISH: P(h|ex) = -1.3482225661432807 ==> log prob of sentence so far: -10.238586679573114
FRENCH: P(h|ex) = -2.413299764081252 ==> log prob of sentence so far: -10.800915854546181

3GRAM: xho
ENGLISH: P(o|xh) = -1.5185139398778875 ==> log prob of sentence so far: -11.757100619451002
FRENCH: P(o|xh) = -1.7075701760979363 ==> log prob of sentence so far: -12.508486030644118

3GRAM: hor
ENGLISH: P(r|ho) = -1.044555037591331 ==> log prob of sentence so far: -12.801655657042334
FRENCH: P(r|ho) = -0.6614721433655512 ==> log prob of sentence so far: -13.169958174009668

3GRAM: ort
ENGLISH: P(t|or) = -1.0102784532431723 ==> log prob of sentence so far: -13.811934110285506
FRENCH: P(t|or) = -0.7231036840527382 ==> log prob of sentence so far: -13.893061858062406

3GRAM: rte
ENGLISH: P(e|rt) = -0.9192895464502105 ==> log prob of sentence so far: -14.731223656735716
FRENCH: P(e|rt) = -0.4821619925273595 ==> log prob of sentence so far: -14.375223850589766

3GRAM: te 
ENGLISH: P( |te) = -0.9124869993593766 ==> log prob of sentence so far: -15.643710656095093
FRENCH: P( |te) = -0.4052895328427899 ==> log prob of sentence so far: -14.780513383432556

3GRAM: e l
ENGLISH: P(l|e ) = -1.490404792942822 ==> log prob of sentence so far: -17.134115449037914
FRENCH: P(l|e ) = -0.961363736110971 ==> log prob of sentence so far: -15.741877119543528

3GRAM:  li
ENGLISH: P(i| l) = -0.4925898761311285 ==> log prob of sentence so far: -17.62670532516904
FRENCH: P(i| l) = -1.2661083483635807 ==> log prob of sentence so far: -17.00798546790711

3GRAM: lin
ENGLISH: P(n|li) = -0.590806649768399 ==> log prob of sentence so far: -18.21751197493744
FRENCH: P(n|li) = -0.8967622181428816 ==> log prob of sentence so far: -17.90474768604999

3GRAM: ins
ENGLISH: P(s|in) = -1.4122009263062918 ==> log prob of sentence so far: -19.62971290124373
FRENCH: P(s|in) = -0.9077061873486602 ==> log prob of sentence so far: -18.81245387339865

3GRAM: nst
ENGLISH: P(t|ns) = -0.7096835354113354 ==> log prob of sentence so far: -20.33939643665507
FRENCH: P(t|ns) = -1.2830110376116322 ==> log prob of sentence so far: -20.095464911010282

3GRAM: sta
ENGLISH: P(a|st) = -0.8224915427608313 ==> log prob of sentence so far: -21.1618879794159
FRENCH: P(a|st) = -0.7964800574583567 ==> log prob of sentence so far: -20.89194496846864

3GRAM: tal
ENGLISH: P(l|ta) = -1.0207931700640676 ==> log prob of sentence so far: -22.18268114947997
FRENCH: P(l|ta) = -1.5033805437243264 ==> log prob of sentence so far: -22.395325512192965

3GRAM: all
ENGLISH: P(l|al) = -0.4316329878313321 ==> log prob of sentence so far: -22.6143141373113
FRENCH: P(l|al) = -0.7654514167486618 ==> log prob of sentence so far: -23.160776928941626

3GRAM: lla
ENGLISH: P(a|ll) = -1.5495650330393165 ==> log prob of sentence so far: -24.16387917035062
FRENCH: P(a|ll) = -0.9798647492243572 ==> log prob of sentence so far: -24.140641678165984

3GRAM: lat
ENGLISH: P(t|la) = -1.069164704349024 ==> log prob of sentence so far: -25.233043874699643
FRENCH: P(t|la) = -1.2302776245780371 ==> log prob of sentence so far: -25.37091930274402

3GRAM: ati
ENGLISH: P(i|at) = -1.0583900558970079 ==> log prob of sentence so far: -26.29143393059665
FRENCH: P(i|at) = -0.4476383513733053 ==> log prob of sentence so far: -25.818557654117328

3GRAM: tio
ENGLISH: P(o|ti) = -0.6769407066416073 ==> log prob of sentence so far: -26.968374637238256
FRENCH: P(o|ti) = -0.5631461859618833 ==> log prob of sentence so far: -26.38170384007921

3GRAM: ion
ENGLISH: P(n|io) = -0.1808477556165817 ==> log prob of sentence so far: -27.14922239285484
FRENCH: P(n|io) = -0.0417954743285875 ==> log prob of sentence so far: -26.4234993144078

3GRAM: on 
ENGLISH: P( |on) = -0.4688130067746161 ==> log prob of sentence so far: -27.618035399629456
FRENCH: P( |on) = -0.5370443667397936 ==> log prob of sentence so far: -26.96054368114759

3GRAM: n d
ENGLISH: P(d|n ) = -1.7550736005120382 ==> log prob of sentence so far: -29.373109000141493
FRENCH: P(d|n ) = -0.8649291279024568 ==> log prob of sentence so far: -27.825472809050048

3GRAM:  da
ENGLISH: P(a| d) = -0.8797657355694308 ==> log prob of sentence so far: -30.252874735710925
FRENCH: P(a| d) = -1.0409075347384003 ==> log prob of sentence so far: -28.86638034378845

3GRAM: dal
ENGLISH: P(l|da) = -1.4040062022 ==> log prob of sentence so far: -31.656880937910927
FRENCH: P(l|da) = -1.8599784416420209 ==> log prob of sentence so far: -30.72635878543047

3GRAM: alt
ENGLISH: P(t|al) = -1.7078965292839894 ==> log prob of sentence so far: -33.36477746719492
FRENCH: P(t|al) = -1.8700435696583528 ==> log prob of sentence so far: -32.59640235508882

3GRAM: lti
ENGLISH: P(i|lt) = -0.8826144012821459 ==> log prob of sentence so far: -34.24739186847707
FRENCH: P(i|lt) = -0.8952646494799871 ==> log prob of sentence so far: -33.491667004568804

3GRAM: tim
ENGLISH: P(m|ti) = -0.8890976848700366 ==> log prob of sentence so far: -35.136489553347104
FRENCH: P(m|ti) = -1.4294789747328869 ==> log prob of sentence so far: -34.92114597930169

3GRAM: ime
ENGLISH: P(e|im) = -0.5959636966945008 ==> log prob of sentence so far: -35.73245325004161
FRENCH: P(e|im) = -0.48481781405783136 ==> log prob of sentence so far: -35.405963793359525

3GRAM: met
ENGLISH: P(t|me) = -1.2369345478455978 ==> log prob of sentence so far: -36.96938779788721
FRENCH: P(t|me) = -1.0913565624792652 ==> log prob of sentence so far: -36.49732035583879

3GRAM: etr
ENGLISH: P(r|et) = -1.6918419161314746 ==> log prob of sentence so far: -38.66122971401868
FRENCH: P(r|et) = -0.9725528398384369 ==> log prob of sentence so far: -37.46987319567722

3GRAM: tre
ENGLISH: P(e|tr) = -0.8240749780686081 ==> log prob of sentence so far: -39.48530469208729
FRENCH: P(e|tr) = -0.2243809960990119 ==> log prob of sentence so far: -37.69425419177624

3GRAM: res
ENGLISH: P(s|re) = -0.9843893280348494 ==> log prob of sentence so far: -40.46969402012214
FRENCH: P(s|re) = -0.6483310433228557 ==> log prob of sentence so far: -38.342585235099094

3GRAM: es 
ENGLISH: P( |es) = -0.30877389675438693 ==> log prob of sentence so far: -40.77846791687653
FRENCH: P( |es) = -0.0917593965695952 ==> log prob of sentence so far: -38.43434463166869

3GRAM: s r
ENGLISH: P(r|s ) = -1.8287843056609587 ==> log prob of sentence so far: -42.60725222253749
FRENCH: P(r|s ) = -1.4711045996366758 ==> log prob of sentence so far: -39.90544923130536

3GRAM:  ra
ENGLISH: P(a| r) = -0.9143271270564362 ==> log prob of sentence so far: -43.52157934959393
FRENCH: P(a| r) = -0.8247062239622833 ==> log prob of sentence so far: -40.73015545526764

3GRAM: rad
ENGLISH: P(d|ra) = -1.4611636799389067 ==> log prob of sentence so far: -44.982743029532834
FRENCH: P(d|ra) = -1.9600035758568475 ==> log prob of sentence so far: -42.69015903112449

3GRAM: ada
ENGLISH: P(a|ad) = -1.854127356556057 ==> log prob of sentence so far: -46.83687038608889
FRENCH: P(a|ad) = -1.3973300444117929 ==> log prob of sentence so far: -44.08748907553628

3GRAM: dar
ENGLISH: P(r|da) = -0.6632098686538478 ==> log prob of sentence so far: -47.50008025474274
FRENCH: P(r|da) = -1.7307183312510783 ==> log prob of sentence so far: -45.81820740678736

According to the 3gram model, the sentence is in French
----------------
4GRAM MODEL:

4GRAM: ntsb
ENGLISH: P(b|nts) = -2.7902851640332416 ==> log prob of sentence so far: -2.7902851640332416
FRENCH: P(b|nts) = -3.116275587580544 ==> log prob of sentence so far: -3.116275587580544

4GRAM: tsb 
ENGLISH: P( |tsb) = -1.4913616938342726 ==> log prob of sentence so far: -4.281646857867514
FRENCH: P( |tsb) = -1.4471580313422192 ==> log prob of sentence so far: -4.563433618922764

4GRAM: sb e
ENGLISH: P(e|sb ) = -1.4471580313422192 ==> log prob of sentence so far: -5.728804889209734
FRENCH: P(e|sb ) = -1.4471580313422192 ==> log prob of sentence so far: -6.010591650264983

4GRAM: b ex
ENGLISH: P(x|b e) = -0.9542425094393249 ==> log prob of sentence so far: -6.683047398649059
FRENCH: P(x|b e) = -1.6334684555795866 ==> log prob of sentence so far: -7.64406010584457

4GRAM:  exh
ENGLISH: P(h| ex) = -1.2597477103238068 ==> log prob of sentence so far: -7.942795108972866
FRENCH: P(h| ex) = -2.468839448857906 ==> log prob of sentence so far: -10.112899554702476

4GRAM: exho
ENGLISH: P(o|exh) = -1.5096504795465824 ==> log prob of sentence so far: -9.452445588519447
FRENCH: P(o|exh) = -1.4913616938342726 ==> log prob of sentence so far: -11.60426124853675

4GRAM: xhor
ENGLISH: P(r|xho) = -0.9852767431792937 ==> log prob of sentence so far: -10.43772233169874
FRENCH: P(r|xho) = -1.4471580313422192 ==> log prob of sentence so far: -13.051419279878969

4GRAM: hort
ENGLISH: P(t|hor) = -0.7354958764544509 ==> log prob of sentence so far: -11.173218208153191
FRENCH: P(t|hor) = -2.578639209968072 ==> log prob of sentence so far: -15.630058489847041

4GRAM: orte
ENGLISH: P(e|ort) = -1.2211650518762958 ==> log prob of sentence so far: -12.394383260029487
FRENCH: P(e|ort) = -0.33783040030931494 ==> log prob of sentence so far: -15.967888890156356

4GRAM: rte 
ENGLISH: P( |rte) = -2.7745169657285493 ==> log prob of sentence so far: -15.168900225758037
FRENCH: P( |rte) = -0.29430274845107673 ==> log prob of sentence so far: -16.262191638607433

4GRAM: te l
ENGLISH: P(l|te ) = -1.8497878242376855 ==> log prob of sentence so far: -17.018688049995724
FRENCH: P(l|te ) = -1.1867575076976598 ==> log prob of sentence so far: -17.448949146305093

4GRAM: e li
ENGLISH: P(i|e l) = -0.5388889280089508 ==> log prob of sentence so far: -17.557576978004676
FRENCH: P(i|e l) = -1.242676488114361 ==> log prob of sentence so far: -18.691625634419456

4GRAM:  lin
ENGLISH: P(n| li) = -0.9517020255532719 ==> log prob of sentence so far: -18.509279003557946
FRENCH: P(n| li) = -0.603811685649347 ==> log prob of sentence so far: -19.295437320068803

4GRAM: lins
ENGLISH: P(s|lin) = -2.204300901001346 ==> log prob of sentence so far: -20.713579904559293
FRENCH: P(s|lin) = -1.0574795050516799 ==> log prob of sentence so far: -20.352916825120484

4GRAM: inst
ENGLISH: P(t|ins) = -0.34323358033048523 ==> log prob of sentence so far: -21.056813484889776
FRENCH: P(t|ins) = -0.7500866480207787 ==> log prob of sentence so far: -21.103003473141264

4GRAM: nsta
ENGLISH: P(a|nst) = -0.46795154881769246 ==> log prob of sentence so far: -21.524765033707467
FRENCH: P(a|nst) = -0.36701754805889947 ==> log prob of sentence so far: -21.470021021200164

4GRAM: stal
ENGLISH: P(l|sta) = -1.8003481937151367 ==> log prob of sentence so far: -23.325113227422605
FRENCH: P(l|sta) = -1.3035476518852276 ==> log prob of sentence so far: -22.773568673085393

4GRAM: tall
ENGLISH: P(l|tal) = -0.7067370277180586 ==> log prob of sentence so far: -24.031850255140665
FRENCH: P(l|tal) = -0.9168297984062722 ==> log prob of sentence so far: -23.690398471491665

4GRAM: alla
ENGLISH: P(a|all) = -1.6936335790332406 ==> log prob of sentence so far: -25.725483834173904
FRENCH: P(a|all) = -0.4791625956432328 ==> log prob of sentence so far: -24.169561067134897

4GRAM: llat
ENGLISH: P(t|lla) = -0.996261430440598 ==> log prob of sentence so far: -26.7217452646145
FRENCH: P(t|lla) = -1.4383015232265415 ==> log prob of sentence so far: -25.60786259036144

4GRAM: lati
ENGLISH: P(i|lat) = -0.4486327169082249 ==> log prob of sentence so far: -27.170377981522726
FRENCH: P(i|lat) = -0.49254731969185805 ==> log prob of sentence so far: -26.100409910053298

4GRAM: atio
ENGLISH: P(o|ati) = -0.29392030260674856 ==> log prob of sentence so far: -27.464298284129477
FRENCH: P(o|ati) = -0.2595788813088292 ==> log prob of sentence so far: -26.359988791362127

4GRAM: tion
ENGLISH: P(n|tio) = -0.024906651185448582 ==> log prob of sentence so far: -27.489204935314927
FRENCH: P(n|tio) = -0.007253521126645573 ==> log prob of sentence so far: -26.367242312488774

4GRAM: ion 
ENGLISH: P( |ion) = -0.2256536067106657 ==> log prob of sentence so far: -27.714858542025592
FRENCH: P( |ion) = -0.24735855688154174 ==> log prob of sentence so far: -26.614600869370314

4GRAM: on d
ENGLISH: P(d|on ) = -1.6873955255429185 ==> log prob of sentence so far: -29.40225406756851
FRENCH: P(d|on ) = -0.7783570283936073 ==> log prob of sentence so far: -27.39295789776392

4GRAM: n da
ENGLISH: P(a|n d) = -1.0572157278579095 ==> log prob of sentence so far: -30.45946979542642
FRENCH: P(a|n d) = -1.2327138260260875 ==> log prob of sentence so far: -28.62567172379001

4GRAM:  dal
ENGLISH: P(l| da) = -2.2982290899777897 ==> log prob of sentence so far: -32.75769888540421
FRENCH: P(l| da) = -1.8087007066314038 ==> log prob of sentence so far: -30.434372430421412

4GRAM: dalt
ENGLISH: P(t|dal) = -2.060697840353612 ==> log prob of sentence so far: -34.81839672575782
FRENCH: P(t|dal) = -2.0043213737826426 ==> log prob of sentence so far: -32.43869380420406

4GRAM: alti
ENGLISH: P(i|alt) = -0.8889757779267333 ==> log prob of sentence so far: -35.70737250368455
FRENCH: P(i|alt) = -1.260071387985075 ==> log prob of sentence so far: -33.69876519218913

4GRAM: ltim
ENGLISH: P(m|lti) = -0.8034571156484139 ==> log prob of sentence so far: -36.510829619332966
FRENCH: P(m|lti) = -1.785329835010767 ==> log prob of sentence so far: -35.48409502719989

4GRAM: time
ENGLISH: P(e|tim) = -0.05192815786116031 ==> log prob of sentence so far: -36.56275777719413
FRENCH: P(e|tim) = -0.1597008428675119 ==> log prob of sentence so far: -35.64379587006741

4GRAM: imet
ENGLISH: P(t|ime) = -2.171238756261269 ==> log prob of sentence so far: -38.7339965334554
FRENCH: P(t|ime) = -0.8291685838861501 ==> log prob of sentence so far: -36.47296445395356

4GRAM: metr
ENGLISH: P(r|met) = -1.363028119868867 ==> log prob of sentence so far: -40.09702465332426
FRENCH: P(r|met) = -0.24798995747739133 ==> log prob of sentence so far: -36.72095441143095

4GRAM: etre
ENGLISH: P(e|etr) = -0.6462636538200158 ==> log prob of sentence so far: -40.743288307144276
FRENCH: P(e|etr) = -0.11344444991779212 ==> log prob of sentence so far: -36.834398861348745

4GRAM: tres
ENGLISH: P(s|tre) = -1.2165199161112894 ==> log prob of sentence so far: -41.959808223255564
FRENCH: P(s|tre) = -0.580718736720241 ==> log prob of sentence so far: -37.41511759806899

4GRAM: res 
ENGLISH: P( |res) = -0.5926843812628577 ==> log prob of sentence so far: -42.55249260451842
FRENCH: P( |res) = -0.15551948117686032 ==> log prob of sentence so far: -37.57063707924585

4GRAM: es r
ENGLISH: P(r|es ) = -2.0487396954680466 ==> log prob of sentence so far: -44.601232299986464
FRENCH: P(r|es ) = -1.4546494519143685 ==> log prob of sentence so far: -39.02528653116022

4GRAM: s ra
ENGLISH: P(a|s r) = -0.8888682027379236 ==> log prob of sentence so far: -45.49010050272439
FRENCH: P(a|s r) = -0.7173232063168986 ==> log prob of sentence so far: -39.74260973747712

4GRAM:  rad
ENGLISH: P(d| ra) = -1.1740547044120966 ==> log prob of sentence so far: -46.66415520713649
FRENCH: P(d| ra) = -1.7714916207149014 ==> log prob of sentence so far: -41.51410135819202

4GRAM: rada
ENGLISH: P(a|rad) = -1.9526310252827455 ==> log prob of sentence so far: -48.61678623241924
FRENCH: P(a|rad) = -0.8998848471167322 ==> log prob of sentence so far: -42.41398620530875

4GRAM: adar
ENGLISH: P(r|ada) = -2.021189299069938 ==> log prob of sentence so far: -50.637975531489175
FRENCH: P(r|ada) = -1.8976270912904414 ==> log prob of sentence so far: -44.31161329659919

According to the 4gram model, the sentence is in French
----------------
