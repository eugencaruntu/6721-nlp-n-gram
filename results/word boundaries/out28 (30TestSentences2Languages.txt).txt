Je ne pas de job.

1GRAM MODEL:

1GRAM: j
ENGLISH: P(j) = -3.0269729668933083 ==> log prob of sentence so far: -3.0269729668933083
FRENCH: P(j) = -2.2939047450657273 ==> log prob of sentence so far: -2.2939047450657273

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -4.019183827527405
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -3.1451976364531204

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -4.782838037606347
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -3.8984964121757884

1GRAM: n
ENGLISH: P(n) = -1.2437365799768185 ==> log prob of sentence so far: -6.026574617583166
FRENCH: P(n) = -1.2069645534794413 ==> log prob of sentence so far: -5.105460965655229

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -7.0187854782172625
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -5.956753857042623

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -7.782439688296205
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -6.71005263276529

1GRAM: p
ENGLISH: P(p) = -1.8239651026074535 ==> log prob of sentence so far: -9.606404790903659
FRENCH: P(p) = -1.6229627876221597 ==> log prob of sentence so far: -8.33301542038745

1GRAM: a
ENGLISH: P(a) = -1.1689283236166432 ==> log prob of sentence so far: -10.775333114520302
FRENCH: P(a) = -1.1606801719704456 ==> log prob of sentence so far: -9.493695592357897

1GRAM: s
ENGLISH: P(s) = -1.2532806553027906 ==> log prob of sentence so far: -12.028613769823092
FRENCH: P(s) = -1.1487747917545927 ==> log prob of sentence so far: -10.642470384112489

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -12.792267979902034
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -11.395769159835156

1GRAM: d
ENGLISH: P(d) = -1.478319560115872 ==> log prob of sentence so far: -14.270587540017907
FRENCH: P(d) = -1.5045887548541879 ==> log prob of sentence so far: -12.900357914689344

1GRAM: e
ENGLISH: P(e) = -0.9922108606340964 ==> log prob of sentence so far: -15.262798400652002
FRENCH: P(e) = -0.8512928913873932 ==> log prob of sentence so far: -13.751650806076738

1GRAM:  
ENGLISH: P( ) = -0.7636542100789422 ==> log prob of sentence so far: -16.026452610730946
FRENCH: P( ) = -0.7532987757226679 ==> log prob of sentence so far: -14.504949581799405

1GRAM: j
ENGLISH: P(j) = -3.0269729668933083 ==> log prob of sentence so far: -19.053425577624253
FRENCH: P(j) = -2.2939047450657273 ==> log prob of sentence so far: -16.798854326865133

1GRAM: o
ENGLISH: P(o) = -1.2199234031596837 ==> log prob of sentence so far: -20.273348980783936
FRENCH: P(o) = -1.3586704071825915 ==> log prob of sentence so far: -18.157524734047723

1GRAM: b
ENGLISH: P(b) = -1.834091631326008 ==> log prob of sentence so far: -22.107440612109944
FRENCH: P(b) = -2.1362152530478733 ==> log prob of sentence so far: -20.293739987095595

According to the 1gram model, the sentence is in French
----------------
2GRAM MODEL:

2GRAM: je
ENGLISH: P(e|j) = -0.6568783039510844 ==> log prob of sentence so far: -0.6568783039510844
FRENCH: P(e|j) = -0.18958094664608957 ==> log prob of sentence so far: -0.18958094664608957

2GRAM: e 
ENGLISH: P( |e) = -0.4805818326341819 ==> log prob of sentence so far: -1.1374601365852663
FRENCH: P( |e) = -0.45464594686088583 ==> log prob of sentence so far: -0.6442268935069754

2GRAM:  n
ENGLISH: P(n| ) = -1.6694920658404147 ==> log prob of sentence so far: -2.806952202425681
FRENCH: P(n| ) = -1.3337664668875164 ==> log prob of sentence so far: -1.9779933603944917

2GRAM: ne
ENGLISH: P(e|n) = -1.077410169937069 ==> log prob of sentence so far: -3.88436237236275
FRENCH: P(e|n) = -0.7933706565270517 ==> log prob of sentence so far: -2.7713640169215434

2GRAM: e 
ENGLISH: P( |e) = -0.4805818326341819 ==> log prob of sentence so far: -4.364944204996932
FRENCH: P( |e) = -0.45464594686088583 ==> log prob of sentence so far: -3.2260099637824293

2GRAM:  p
ENGLISH: P(p| ) = -1.5773435961750142 ==> log prob of sentence so far: -5.942287801171946
FRENCH: P(p| ) = -1.1203018922976302 ==> log prob of sentence so far: -4.346311856080059

2GRAM: pa
ENGLISH: P(a|p) = -0.9679921054989932 ==> log prob of sentence so far: -6.910279906670939
FRENCH: P(a|p) = -0.6479365393291892 ==> log prob of sentence so far: -4.994248395409248

2GRAM: as
ENGLISH: P(s|a) = -1.0391213206477214 ==> log prob of sentence so far: -7.949401227318661
FRENCH: P(s|a) = -1.361115527777203 ==> log prob of sentence so far: -6.355363923186451

2GRAM: s 
ENGLISH: P( |s) = -0.40550908468327607 ==> log prob of sentence so far: -8.354910312001937
FRENCH: P( |s) = -0.2671715449815274 ==> log prob of sentence so far: -6.622535468167979

2GRAM:  d
ENGLISH: P(d| ) = -1.5865842529099268 ==> log prob of sentence so far: -9.941494564911864
FRENCH: P(d| ) = -0.9158393035688763 ==> log prob of sentence so far: -7.538374771736855

2GRAM: de
ENGLISH: P(e|d) = -0.861685920801946 ==> log prob of sentence so far: -10.80318048571381
FRENCH: P(e|d) = -0.2969202992977406 ==> log prob of sentence so far: -7.835295071034595

2GRAM: e 
ENGLISH: P( |e) = -0.4805818326341819 ==> log prob of sentence so far: -11.283762318347991
FRENCH: P( |e) = -0.45464594686088583 ==> log prob of sentence so far: -8.289941017895481

2GRAM:  j
ENGLISH: P(j| ) = -2.427495482498343 ==> log prob of sentence so far: -13.711257800846333
FRENCH: P(j| ) = -1.6338755057532193 ==> log prob of sentence so far: -9.9238165236487

2GRAM: jo
ENGLISH: P(o|j) = -0.5263787425615649 ==> log prob of sentence so far: -14.237636543407898
FRENCH: P(o|j) = -0.8910636000847345 ==> log prob of sentence so far: -10.814880123733435

2GRAM: ob
ENGLISH: P(b|o) = -2.0792724219669023 ==> log prob of sentence so far: -16.3169089653748
FRENCH: P(b|o) = -1.837495303849559 ==> log prob of sentence so far: -12.652375427582994

According to the 2gram model, the sentence is in French
----------------
3GRAM MODEL:

3GRAM: je 
ENGLISH: P( |je) = -2.708420900134713 ==> log prob of sentence so far: -2.708420900134713
FRENCH: P( |je) = -0.08228353490238077 ==> log prob of sentence so far: -0.08228353490238077

3GRAM: e n
ENGLISH: P(n|e ) = -1.7001662096303698 ==> log prob of sentence so far: -4.408587109765083
FRENCH: P(n|e ) = -1.1750984768892099 ==> log prob of sentence so far: -1.2573820117915906

3GRAM:  ne
ENGLISH: P(e| n) = -0.7882688974531556 ==> log prob of sentence so far: -5.196856007218239
FRENCH: P(e| n) = -0.3964986682344542 ==> log prob of sentence so far: -1.6538806800260448

3GRAM: ne 
ENGLISH: P( |ne) = -0.5050919934189868 ==> log prob of sentence so far: -5.701948000637225
FRENCH: P( |ne) = -0.24244315598859387 ==> log prob of sentence so far: -1.8963238360146386

3GRAM: e p
ENGLISH: P(p|e ) = -1.4176070399833143 ==> log prob of sentence so far: -7.11955504062054
FRENCH: P(p|e ) = -1.0918353747194343 ==> log prob of sentence so far: -2.988159210734073

3GRAM:  pa
ENGLISH: P(a| p) = -0.7311957732267642 ==> log prob of sentence so far: -7.850750813847304
FRENCH: P(a| p) = -0.5234423981588127 ==> log prob of sentence so far: -3.5116016088928856

3GRAM: pas
ENGLISH: P(s|pa) = -0.8802779006308712 ==> log prob of sentence so far: -8.731028714478175
FRENCH: P(s|pa) = -0.5280758922227119 ==> log prob of sentence so far: -4.039677501115597

3GRAM: as 
ENGLISH: P( |as) = -0.2759174929966362 ==> log prob of sentence so far: -9.006946207474812
FRENCH: P( |as) = -0.3243957793370807 ==> log prob of sentence so far: -4.364073280452678

3GRAM: s d
ENGLISH: P(d|s ) = -1.6895568240059702 ==> log prob of sentence so far: -10.696503031480782
FRENCH: P(d|s ) = -0.822746859351899 ==> log prob of sentence so far: -5.186820139804578

3GRAM:  de
ENGLISH: P(e| d) = -0.5779410562767562 ==> log prob of sentence so far: -11.274444087757539
FRENCH: P(e| d) = -0.20563470908611803 ==> log prob of sentence so far: -5.392454848890695

3GRAM: de 
ENGLISH: P( |de) = -0.8324473711335487 ==> log prob of sentence so far: -12.106891458891088
FRENCH: P( |de) = -0.2704858464890902 ==> log prob of sentence so far: -5.662940695379786

3GRAM: e j
ENGLISH: P(j|e ) = -2.440384254832884 ==> log prob of sentence so far: -14.547275713723971
FRENCH: P(j|e ) = -1.6783064302227964 ==> log prob of sentence so far: -7.341247125602582

3GRAM:  jo
ENGLISH: P(o| j) = -0.48933114071535533 ==> log prob of sentence so far: -15.036606854439327
FRENCH: P(o| j) = -1.0155064201361352 ==> log prob of sentence so far: -8.356753545738718

3GRAM: job
ENGLISH: P(b|jo) = -1.2890790675625095 ==> log prob of sentence so far: -16.32568592200184
FRENCH: P(b|jo) = -1.3284180723241787 ==> log prob of sentence so far: -9.685171618062896

According to the 3gram model, the sentence is in French
----------------
4GRAM MODEL:

4GRAM: je n
ENGLISH: P(n|je ) = -1.4471580313422192 ==> log prob of sentence so far: -1.4471580313422192
FRENCH: P(n|je ) = -0.683653722647628 ==> log prob of sentence so far: -0.683653722647628

4GRAM: e ne
ENGLISH: P(e|e n) = -0.6855284832668207 ==> log prob of sentence so far: -2.13268651460904
FRENCH: P(e|e n) = -0.3201061265561597 ==> log prob of sentence so far: -1.0037598492037878

4GRAM:  ne 
ENGLISH: P( | ne) = -2.6748611407378116 ==> log prob of sentence so far: -4.807547655346852
FRENCH: P( | ne) = -0.32323596301113944 ==> log prob of sentence so far: -1.3269958122149272

4GRAM: ne p
ENGLISH: P(p|ne ) = -1.5145871999087717 ==> log prob of sentence so far: -6.322134855255624
FRENCH: P(p|ne ) = -0.9248386072213541 ==> log prob of sentence so far: -2.2518344194362814

4GRAM: e pa
ENGLISH: P(a|e p) = -0.7717655437087052 ==> log prob of sentence so far: -7.093900398964329
FRENCH: P(a|e p) = -0.5762327088465381 ==> log prob of sentence so far: -2.8280671282828194

4GRAM:  pas
ENGLISH: P(s| pa) = -0.7583053083335619 ==> log prob of sentence so far: -7.852205707297891
FRENCH: P(s| pa) = -0.42405709888389764 ==> log prob of sentence so far: -3.252124227166717

4GRAM: pas 
ENGLISH: P( |pas) = -2.0145205387579237 ==> log prob of sentence so far: -9.866726246055816
FRENCH: P( |pas) = -0.11177158961396287 ==> log prob of sentence so far: -3.36389581678068

4GRAM: as d
ENGLISH: P(d|as ) = -1.7007037171450194 ==> log prob of sentence so far: -11.567429963200835
FRENCH: P(d|as ) = -0.8733333592892749 ==> log prob of sentence so far: -4.237229176069954

4GRAM: s de
ENGLISH: P(e|s d) = -0.5713911715615104 ==> log prob of sentence so far: -12.138821134762345
FRENCH: P(e|s d) = -0.19637229363535968 ==> log prob of sentence so far: -4.433601469705314

4GRAM:  de 
ENGLISH: P( | de) = -1.6842467475153124 ==> log prob of sentence so far: -13.823067882277657
FRENCH: P( | de) = -0.2446327043839627 ==> log prob of sentence so far: -4.678234174089277

4GRAM: de j
ENGLISH: P(j|de ) = -3.1693804953119495 ==> log prob of sentence so far: -16.992448377589607
FRENCH: P(j|de ) = -2.277074133470176 ==> log prob of sentence so far: -6.955308307559453

4GRAM: e jo
ENGLISH: P(o|e j) = -0.516629796003336 ==> log prob of sentence so far: -17.509078173592943
FRENCH: P(o|e j) = -0.8829543271387885 ==> log prob of sentence so far: -7.838262634698242

4GRAM:  job
ENGLISH: P(b| jo) = -1.1949766032160551 ==> log prob of sentence so far: -18.704054776809
FRENCH: P(b| jo) = -1.1139433523068367 ==> log prob of sentence so far: -8.952205987005078

According to the 4gram model, the sentence is in French
----------------
