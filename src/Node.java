
public class Node {
    private double freq;
    private Node[] children;

    public Node(double smoothing) {
        freq = smoothing;
    }

    public void populateChildren(double smoothing) {
        children = new Node[driver.alphabet.length];
        for (int i = 0; i < children.length; i++) {
            children[i] = new Node(smoothing);
        }
    }

    public double getCounts() {
        return freq;
    }

    public void setCounts(double counts) {
        freq = counts;
    }

    public Node[] getChildren() {
        return children;
    }

    public boolean hasChildren() {
        return children != null;
    }

    public Node getChildrenNodeAt(int idx) {
        return children[idx];
    }

    public double getCountsForChildrenNodeAt(int idx) {
        return children[idx].freq;
    }
}
