import java.util.Arrays;
import java.util.Vector;

public class Trie {
    private int n;
    private String div;
    private Node root;
    private Vector<Character> vocabulary;
    private int bins;
    private double smoothing;

    public Trie(int n, Character[] alphabet, double smoothing) {
        this.n = n;
        div = (n == 1) ? "" : "|";   // if n is one (unigram) scrap the divider string
        this.smoothing = smoothing;
        root = new Node(0); // there is no smooting for root at this stage
        if (n == 1) {
            root.populateChildren(smoothing);   // default all child nodes to smoothing value
        } else { // if we have more than 1 levels
            root.populateChildren(0);
        }
        vocabulary = new Vector<>(Arrays.asList(alphabet));
        bins = vocabulary.size();
    }

    public void insert(String gram) {
        Node node = root;
        for (int i = 1; i <= gram.length(); i++) {
            int currentIdx = vocabulary.indexOf(gram.charAt(i - 1)); // -1 since we start at 1 for clarity
            node = node.getChildrenNodeAt(currentIdx);   // set the node to children node
            node.setCounts(node.getCounts() + 1);

            // if not last level, ensure children with zero smoothing
            if (i < n - 1 && !node.hasChildren()) {
                node.populateChildren(0);
            } else if (i == n - 1 && !node.hasChildren()) {   // only for the parent of the last level (that means for level n-1), ensure children have smoothing
                node.populateChildren(smoothing);
            }
        }
    }

    public double search(String ngram) {
        Node current = root;
        double priors, count;

        int i = 0; // start at first position of ngram
        // while there there is a continuation, navigate the trie without doing anything, if not we report entire ngram with smoothed counts
        while (current.hasChildren() && i < ngram.length() - 1) {
            int idx = vocabulary.indexOf(ngram.charAt(i)); // get the position in vocabulary of the character in ngram
            current = current.getChildrenNodeAt(idx);
            i++;
        }

        // we are now at n-2, readAndTrain the priors from the parent which is still the current node
        priors = (current.getCounts() == 0) ? smoothing : current.getCounts();

        // see if leaf exists, then if it has value, or add smoothing
        int idx = vocabulary.indexOf(ngram.charAt(i));
        if (current.hasChildren()) {
            count = (current.getCountsForChildrenNodeAt(idx) == 0) ? smoothing : current.getCountsForChildrenNodeAt(idx);
        } else {
            count = smoothing;
        }

        // report the probabilities
        return probs(priors, count);
    }

    public void DFS(Node current, String gram, double priors) {
        gram = (current == root) ? "" : gram;    // if root, scrap the parent string
        // for each of its children, set current node and recurse
        if (current.hasChildren()) {
/*            // test the last level for smoothing and probability summing up to 1
            if (gram.length() == n - 1) {
                System.out.print(test(gram, current));
            }*/
            for (int i = 0; i < current.getChildren().length; i++) {
                char ch = vocabulary.elementAt(i);
                DFS(current.getChildrenNodeAt(i), gram + ch, current.getCounts());
            }
        } else {
            if (gram.length() == n) { // we have a full n-gram, so we calculate probabilities now
                Util.writeProbs(n, gram, div, probs(priors, current.getCounts()));
            } else {  // this is when we do not have a full ngram and we need to generate

                // if we are missing one (for 3-gram)
                if (gram.length() == n - 1) {
                    for (char c : vocabulary) {
                        Util.writeProbs(n, gram + c, div, probs(smoothing, smoothing));
                    }
                }

                // if we are missing 2 (for 4-gram)
                if (gram.length() == n - 2) {
                    for (char ch : vocabulary) {
                        String tmp = gram + ch;
                        for (char ch1 : vocabulary) {
                            Util.writeProbs(n, tmp + ch1, div, probs(smoothing, smoothing));
                        }
                    }
                }

            }
        }

    }

    private double probs(double priors, double count) {
        return Math.log10(count / (priors + smoothing * bins));
    }

    private String test(String gram, Node node) {
        double priors = node.getCounts();
        double sumProbs = 0d;
        for (int i = 0; i < node.getChildren().length; i++) {
//            System.writer.print(vocabulary.elementAt(i) + ":" + node.getChildren[i].getCounts() + "\t");
            sumProbs += node.getCountsForChildrenNodeAt(i) / (priors + smoothing * bins);
        }
        System.out.println();
        return "given [" + gram + "]: " + priors + ", the sum of probs: " + sumProbs;
    }

    public Node getRoot() {
        return root;
    }

    public String getDiv() {
        return div;
    }
}
