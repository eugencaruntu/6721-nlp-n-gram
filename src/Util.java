import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.util.*;
import java.util.regex.Pattern;

class Util {

    static void readAndTrain(File fileName, Trie model, int n) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)))) {
            while (reader.ready()) {

                String line = cleanText(reader.readLine()); // CLEAN THE LINE, KEEP ONLY ALPHABET CHARS
                List<String> grams = makeGrams(line, n);
                model.getRoot().setCounts(model.getRoot().getCounts() + line.length()); // increase the corpus size each time we add something

                // train the model with all grams from the line
                for (String gram : grams) {
                    model.insert(gram);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static PrintWriter makeWriter(String fileName, Boolean append) {
        FileWriter fw = null;
        try {
            fw = new FileWriter(fileName, append);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert fw != null;

        return new PrintWriter(fw);
    }

    static File[] getListOfFiles(String folderName, String prefix) {
        File folder = new File(folderName);
        return folder.listFiles((dir1, name) -> name.startsWith(prefix));
    }

    static void writeProbs(int n, String gram, String divider, double probs) {
        driver.writer.println("(" + gram.charAt(n - 1) + divider + gram.substring(0, n - 1) + ") = " + probs);
    }

    static String getMax(HashMap<String, Double> langProb) {
        Iterator<Map.Entry<String, Double>> it = langProb.entrySet().iterator();
        Map.Entry<String, Double> current = it.next();
        double max = current.getValue();
        String maxLang = current.getKey();
        while (it.hasNext()) {
            current = it.next();
            if (current.getValue() > max) {
                max = current.getValue();
                maxLang = current.getKey();
            }
        }
        return maxLang;
    }

    static List<String> makeGrams(String text, int n) {
        List<String> grams = new LinkedList<>();
        // make n-grams and call insert for each
        for (int i = 0; i <= text.length() - n; i++) {
//            int end = (i + n > text.length()) ? text.length() - 1 : i + n; // TODO: do we really need this or we can only use i+n
            int end = i + n;
            grams.add(text.substring(i, end));
        }
        return grams;
    }

    /**
     * Removes characters other than those in alphabet and makes the text lowercase
     *
     * @param text the text
     * @return the cleaned text
     */
    public static String cleanText(String text) {
        StringBuilder alphabet = new StringBuilder();
        for (char c : driver.alphabet) {
            alphabet.append(c);
        }
        text = text.toLowerCase().replaceAll("[^" + alphabet + "]", "");
        return text;
    }

    public static void removeAccents(String folderName) {
        File[] testFiles = Util.getListOfFiles(folderName, "");
        for (File file : testFiles) {
            Charset charset = StandardCharsets.UTF_8;
            String content = null;
            try {
                content = new String(Files.readAllBytes(file.toPath()), charset);
                String nfdNormalizedString = Normalizer.normalize(content, Normalizer.Form.NFD);
                Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
                content = pattern.matcher(nfdNormalizedString).replaceAll("");
                Files.write(file.toPath(), content.getBytes(charset));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
