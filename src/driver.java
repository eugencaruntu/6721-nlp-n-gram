import javafx.util.Pair;

import java.io.*;
import java.util.*;

public class driver {
    // TODO: make that n is taken as argument at command line, same for smoothing and alphabet
    static int[] gramSizes = {1, 2, 3, 4};
    static double smoothing = 0.5;
    static Character[] alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        static String[] langs = {"English", "French", "Dutch", "Portuguese", "Spanish"};
//        static String[] langs = {"Portuguese", "Spanish"};
//    static String[] langs = {"English", "French"};
    static String trainingFolder = "TrainingCorpus";
//    static String trainingFolder = "TrainingCorpus_larger";
//    static String trainingFolder = "TrainingCorpus_hellicopter";
    static String testFolder = "TestFiles";
    static PrintWriter writer;

    private static HashMap<Integer, ArrayList<Pair<String, Trie>>> models = new HashMap<>();

    public static void main(String[] args) {
        Util.removeAccents(testFolder);
        train();
        test();
    }

    private static void train() {
        for (int n : gramSizes) {
            ArrayList<Pair<String, Trie>> modelPairList = new ArrayList<>();
            for (String lang : langs) {
                String langId = lang.substring(0, 2).toLowerCase();
                Trie model = new Trie(n, alphabet, smoothing);
                File[] languageFiles = Util.getListOfFiles(trainingFolder, langId + "-");
                for (File file : languageFiles) {
                    Util.readAndTrain(file, model, n);
                }
                writer = Util.makeWriter(n + "-gram" + langId.toUpperCase() + ".txt", false);
                model.DFS(model.getRoot(), "", model.getRoot().getCounts());
                writer.close();
                // TODO: make a methond to dump the model to file and one that loads the model from file
                modelPairList.add(new Pair<>(lang, model));
                System.out.println(lang + " corpus:\t" + model.getRoot().getCounts());
            }
            models.put(n, modelPairList);  // add the model to array of models hopefully fits in memory
        }
    }

    private static void test() {
        File[] testFiles = Util.getListOfFiles(testFolder, "");
        for (File file : testFiles) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
                String line;
                int lineCnt = 0;
                // for each line in testFile
                while ((line = reader.readLine()) != null) {
                    writer = Util.makeWriter("out" + (++lineCnt) + " (" + file.getName() + ").txt", false);
                    writer.println(line + "\n");
                    // for each n in models keys
                    for (int n : gramSizes) {
                        HashMap<String, Double> langProbs = new HashMap<>();
                        for (String lang : langs) {
                            langProbs.put(lang, 0.0);
                        }
                        writer.println(n + "GRAM MODEL:");

                        line = Util.cleanText(line); // CLEAN THE LINE, KEEP ONLY ALPHABET CHARS
                        List<String> grams = Util.makeGrams(line, n);

                        // for each gram
                        for (String gram : grams) {
                            writer.println("\n" + n + "GRAM: " + gram);
                            for (int i = 0; i < langs.length; i++) {
                                Pair<String, Trie> modelPair = models.get(n).get(i);
                                String lang = modelPair.getKey();
                                Trie model = modelPair.getValue();
                                double prob = model.search(gram);
                                langProbs.put(lang, langProbs.get(lang) + prob);
                                writer.println(lang.toUpperCase() + ": P(" + gram.charAt(n - 1) + model.getDiv() + gram.substring(0, n - 1) + ") = " + prob +
                                        " ==> log prob of sentence so far: " + langProbs.get(lang));
                            }
                        }
                        writer.println("\nAccording to the " + n + "gram model, the sentence is in " + Util.getMax(langProbs));
                        writer.println("----------------");
                    }
                    writer.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
